'use strict';
IDB.ReportBucket = function(graphContext, states, groupValue, valueAxisId, groupIndexes, rdpRows) {
	this._graphContext = graphContext;
	this._reportData = graphContext.reportData;
	this._statesStr = states;
	this._groupValue = groupValue;
	this._valueAxisId = valueAxisId;
	this._groupIndexes = (groupIndexes ? groupIndexes.slice() : null); //copy
	this._rdpRows = rdpRows;

	this._build();
};

IDB.ReportBucket.NoGrouping = -1;
IDB.ReportBucket.NextRowId = 0;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype._build = function() {
	var ss = this._statesStr;
	this._isExpanded = (ss && (ss == '-' || ss.length > 1));

	var gi = this._groupIndexes;
	this._groupLevel = (gi ? gi.length+1 : 0);
	this._groupIndex = (gi ? gi.shift() : IDB.ReportBucket.NoGrouping);
	
	if ( !IDB.isUndef(this._groupIndex) && this._groupIndex != IDB.ReportBucket.NoGrouping ) {
		this._buildSubBuckets();
	}

	this._calcAggregates();
	this._buildRows();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype._calcAggregates = function() {
	var rows = this._rdpRows;
	var rowLen = rows.length;
	var cols = this._reportData.getAllAxes();
	var colLen = cols.length;
	var sums = [];
	var avgs = [];
	var mins = [];
	var maxs = [];
	var counts = [];
	var ci, ri, n, col, isNum, rdp;

	// Set up initial aggregate values for each axis

	for ( ci = 0 ; ci < colLen ; ++ci ) {
		isNum = cols[ci].dataTypeIsNumber;
		
		if ( isNum ) {
			sums[ci] = 0;
			avgs[ci] = 0;
			mins[ci] = Number.POSITIVE_INFINITY;
			maxs[ci] = Number.NEGATIVE_INFINITY;
		}
		else {
			sums[ci] = avgs[ci] = mins[ci] = maxs[ci] = null;
		}
		
		counts[ci] = 0;
	}
	
	// Calculate aggregate values for each axis

	for ( ci = 0 ; ci < colLen ; ++ci ) {
		col = cols[ci];
		isNum = col.dataTypeIsNumber;
		
		for ( ri = 0 ; ri < rowLen ; ++ri ) {
			rdp = rows[ri][ci];

			if ( rdp.getValue().isNothing ) {
				continue;
			}
			
			if ( !isNum ) {
				counts[ci]++;
				continue;
			}
			
			n = rdp.getValue().numberValue;
			sums[ci] += n;
			mins[ci] = Math.min(mins[ci], n);
			maxs[ci] = Math.max(maxs[ci], n);
			counts[ci]++;
		}
		
		avgs[ci] = (isNum && counts[ci] > 0 ? sums[ci]/counts[ci] : null);
	}

	this._aggregates = {
		sums: sums,
		averages: avgs,
		mins: mins,
		maxes: maxs,
		counts: counts
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype._buildSubBuckets = function() {
	var gc = this._graphContext;
	var rows = this._rdpRows;
	var rowLen = rows.length;
	var grpIndex = this._groupIndex;
	var states = this._parseExpandString(this._statesStr);
	var dataVals = {};
	var dptMap = {};
	var vals = []; //to preserve sorting
	var i, sv, dv, rb, state;
	
	this._buckets = [];
	
	for ( i = 0 ; i < rowLen ; ++i ) {
		dv = rows[i][grpIndex].getValue();
		sv = dv.stringValue;
		
		if ( !dptMap[sv] ) {
			vals.push(sv);
			dptMap[sv] = [];
		}
		
		dptMap[sv].push(rows[i]);
		dataVals[sv] = dv;
	}
	
	for ( i = 0 ; i < vals.length ; ++i ) {
		sv = vals[i];
		state = (states && i < states.length ? states[i] : null);
		rb = new IDB.ReportBucket(gc, state, dataVals[sv],
			this._groupIndex, this._groupIndexes, dptMap[sv]);
		this._buckets.push(rb);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype._parseExpandString = function(s) {
	if ( !s ) {
		return null;
	}
	
	var states = [];
	var open = 0;
	var i, seg;
	
	for ( i = 0 ; i < s.length ; ++i ) {
		if ( s[i] == '[' ) {
			++open;
			continue;
		}

		if ( s[i] == ']' ) {
			--open;
			continue;
		}

		if ( open != 0 ) {
			continue;
		}
		
		if ( s[i] == ',' ) {
			seg = s.slice(0, i);

			if ( seg[0] == '[' ) {
				seg = seg.substring(1, seg.length-1);
			}
			
			states.push(seg);
			s = s.substring(i+1, s.length);
			i = -1;
			continue;
		}
	}
	
	if ( s.length ) {
		if ( s[0] == '[' ) {
			s = s.substring(1, s.length-1); //the rest is a segment
		}

		states.push(s);
	}
	
	return states;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype._buildRows = function() {
	var gc = this._graphContext;
	var rd = this._reportData;
	var rdpRows = this._rdpRows;
	var cols = rdpRows[0].length;
	var isAlt = gc.settings.getBool('CellBgAlternate');
	var rows = [];
	var i, j, row, c;

	if ( this._groupValue ) {
		this._buildGroupRow();
	}
	
	for ( i = 0 ; i < rdpRows.length ; ++i ) {
		row = new Array(cols);
		row.bucketRowId = IDB.ReportBucket.NextRowId++;
		
		for ( j = 0 ; j < cols ; ++j ) {
			if ( rd.isAxisGrouped(j) ) {
				continue;
			}
			
			c = new IDB.ReportBucketCell(gc, j, 1, null);
			c.buildAsRow(rdpRows[i][j]);
			c.setOddRow(isAlt && i%2 == 1);

			row[rd.getColumnIndexByAxis(j)] = c;
		}
		
		rows.push(row);
	}
	
	this._dataRows = rows;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype._buildGroupRow = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var rd = this._reportData;
	var axes = rd.getAllAxes();
	var agg = this._aggregates;
	var isExp = this._isExpanded;
	var valAxisId = this._valueAxisId;
	var groupVal = this._groupValue;
	var depth = 1-this._groupLevel/rd.getGroupIndexes().length;
	var cols = this._rdpRows[0].length;
	var pivotIndex = rd.getColumnIndexByAxis(valAxisId);
	var row = [];
	var me = this;
	var i, axis, colIndex, c, summType, aggVal;
	
	var expCallback = function(expand) {
		me._isExpanded = expand;
	};
	
	for ( i = 0 ; i < cols ; ++i ) {
		axis = axes[i];
		colIndex = rd.getColumnIndexByAxis(i);
		
		if ( colIndex < pivotIndex ) {
			continue;
		}

		c = new IDB.ReportBucketCell(gc, i, depth, expCallback);
		row[colIndex] = c;
		
		if ( i == valAxisId ) {
			c.buildAsPivot(new IDB.ReportDatapoint(axis, groupVal), isExp);
			continue;
		}

		aggVal = null;
		summType = rd.getSummaryType(i);
		
		switch ( summType ) {
			case IDB.ReportData.SummarySum: aggVal = agg.sums[i]; break;
			case IDB.ReportData.SummaryAvg: aggVal = agg.averages[i]; break;
			case IDB.ReportData.SummaryMax: aggVal = agg.maxes[i]; break;
			case IDB.ReportData.SummaryMin: aggVal = agg.mins[i]; break;
			case IDB.ReportData.SummaryCount: aggVal = agg.counts[i]; break;
		}
		
		if ( aggVal == null ) {
			c.buildAsEmpty(new IDB.ReportDatapoint(axis, null));
			continue;
		}

		aggVal = new IDB.DataValue('NUMBER', {
			n: aggVal,
			f: sett.formatNumber(aggVal, axis.numFmt)
		});

		c.buildAsSummary(new IDB.ReportDatapoint(axis, aggVal), summType);
	}
	
	row.bucketRowId = IDB.ReportBucket.NextRowId++;
	this._groupedRow = row;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype.getRows = function() {
	var rows = [];

	if ( this._groupedRow ) {
		if ( !this._isExpanded ) {
			return [this._groupedRow];
		}

		rows.push(this._groupedRow);
	}

	if ( this._buckets ) {
		var buckets = this._buckets;

		for ( var i in buckets ) {
			rows = rows.concat(buckets[i].getRows());
		}

		return rows;
	}

	return rows.concat(this._dataRows);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype.getNumShownRows = function() {
	var n = 1;

	if ( this._groupValue && !this._isExpanded ) {
		return n;
	}
	
	var buckets = this._buckets;
	
	if ( buckets ) {
		for ( var i in buckets ) {
			n += buckets[i].getNumShownRows();
		}
	}
	else {
		n += this._rdpRows.length;
	}
	
	return n;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype.getDatapoints = function() {
	var datapoints = [];
	var rdpRows = this._rdpRows;
	var i, j, rdpRow, cell, graphDp;

	for ( i in rdpRows ) {
		rdpRow = rdpRows[i];

		for ( j in rdpRow ) {
			cell = rdpRow[j];
			graphDp = (cell ? cell.getGraphDatapoint() : null);

			if ( graphDp ) {
				datapoints.push(graphDp);
			}
		}
	}
	
	return datapoints;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype.getBucketCount = function() {
	return this._buckets.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype.getBucket = function(index) {
	return this._buckets[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype.getAggregateValue = function(index) {
	var agg = this._aggregates;

	switch ( this._reportData.getSummaryType(index) ) {
		case IDB.ReportData.SummarySum: return agg.sums[index];
		case IDB.ReportData.SummaryAvg: return agg.averages[index];
		case IDB.ReportData.SummaryMax: return agg.maxes[index];
		case IDB.ReportData.SummaryMin: return agg.mins[index];
		case IDB.ReportData.SummaryCount: return agg.counts[index];
	}

	return null;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype.doExpandAll = function(expand) {
	var buckets = this._buckets;
	var grpRow = this._groupedRow;
	var i, cell;

	this._isExpanded = expand;
	
	if ( grpRow ) {
		var grpLen = grpRow.length;

		for ( i = 0 ; i < grpLen ; ++i ) {
			cell = grpRow[i];

			if ( cell && cell.isPivot() ) {
				cell.setExpandedDuringExpandAll(expand);
			}
		}
	}

	if ( buckets ) {
		var bucketLen = buckets.length;

		for ( i = 0 ; i < bucketLen ; ++i ) {
			buckets[i].doExpandAll(expand);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype.sort = function(col, dir) {
	var rowCompare = function(rowA, rowB) {
		var a = rowA[col].getDatapoint();
		var b = rowB[col].getDatapoint();
		var valA, valB;

		if ( a.getAxis().dataTypeIsNumber ) {
			valA = a.getValue().numberValue;
			valB = b.getValue().numberValue;
		}
		else {
			valA = a.getValue().stringValue;
			valB = b.getValue().stringValue;
		}

		if ( valA == valB ) {
			return 0;
		}
		
		return (valA > valB ? 1 : -1)*dir;
	};

	var bucketCompare = function(bucketA, bucketB) {
		return rowCompare(bucketA._groupedRow, bucketB._groupedRow);
	};

	////

	var buckets = this._buckets;

	if ( !buckets ) {
		this._dataRows.sort(rowCompare);
		this._updateAlternatingRows();
		return;
	}

	var child = buckets[0];
	var rd = this._graphContext.reportData;
	var childPivotIndex = rd.getColumnIndexByAxis(child._valueAxisId);
	
	if ( col == childPivotIndex ) {
		buckets.sort(bucketCompare);
		return;
	}
	
	for ( var i in buckets ) {
		buckets[i].sort(col, dir);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucket.prototype._updateAlternatingRows = function() {
	var gc = this._graphContext;

	if ( !gc.settings.getBool('CellBgAlternate') ) {
		return;
	}

	var dataRows = this._dataRows;
	var rowLen = dataRows.length;
	var colLen = dataRows[0].length;
	var ri, ci, row, odd, cell;

	for ( ri = 0 ; ri < rowLen ; ++ri ) {
		row = dataRows[ri];
		odd = (ri%2 == 1);
		
		for ( ci = 0 ; ci < colLen ; ++ci ) {
			cell = row[ci];
			
			if ( cell ) {
				cell.setOddRow(odd);
			}
		}
	}
};

/*====================================================================================================*/
IDB.ReportBucketCell = function(graphContext, columnIndex, groupDepth, expandCallback) {
	this._graphContext = graphContext;
	this._columnIndex = columnIndex;
	this._groupDepth = groupDepth;
	this._expandCallback = expandCallback;

	this._mode = null;
	this._rdp = null;
	this._isExpanded = false;
	this._isOddRow = false;
	this._text = '';
	this._swatchShape = null;

	var rd = graphContext.reportData;

	this._hasWordWrap = rd.hasWordWrap(columnIndex);
	this._alignH = rd.getAlignment(columnIndex);
	this._alignV = rd.getVerticalAlignment(columnIndex);
	this._swatchShape = rd.getSwatchShape(columnIndex);
};

IDB.ReportBucketCell.ModeRow = 1;
IDB.ReportBucketCell.ModePivot = 2;
IDB.ReportBucketCell.ModeSummary = 3;
IDB.ReportBucketCell.ModeEmpty = 4;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.buildAsRow = function(reportDatapoint) {
	this._mode = IDB.ReportBucketCell.ModeRow;
	this._rdp = reportDatapoint;
	this._buildShared();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.buildAsPivot = function(reportDatapoint, isExpanded) {
	this._mode = IDB.ReportBucketCell.ModePivot;
	this._rdp = reportDatapoint;
	this._isExpanded = isExpanded;
	this._buildShared();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.buildAsSummary = function(reportDatapoint, summaryType) {
	this._mode = IDB.ReportBucketCell.ModeSummary;
	this._rdp = reportDatapoint;
	this._summType = summaryType;
	this._buildShared();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.buildAsEmpty = function(reportDatapoint) {
	this._mode = IDB.ReportBucketCell.ModeEmpty;
	this._rdp = reportDatapoint;
	this._buildShared();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype._buildShared = function() {
	var rdp = this._rdp;
	var axis = rdp.getAxis();
	var val = rdp.getValue();

	if ( axis && axis.dataTypeIsNumber && val ) {
		var nv = val.numberValue;
		this._swatchDir = (nv >= 0 ? 1 : -1);
		this._isSwatchZero = (IDB.isUndef(nv) || nv == 0 || nv == null);
	}

	this._text = this._buildText();
	this._textColor = this._buildTextColor();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype._buildTextColor = function() {
	var sett = this._graphContext.settings;
	var rdp = this._rdp;
	var axis = rdp.getAxis();
	var val = rdp.getValue();
	
	if ( axis && axis.dataTypeIsNumber && val ) {
		var nv = val.numberValue;

		if ( sett.getBool('UpperLimitShow') && nv >= sett.get('UpperLimitValue') ) {
			return sett.get('UpperLimitColor');
		}
		else if ( sett.getBool('LowerLimitShow') && nv <= sett.get('LowerLimitValue') ) {
			return sett.get('LowerLimitColor');
		}
	}

	return sett.get('CellLabelOptions').color;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype._buildText = function() {
	var rdp = this._rdp;
	var val = rdp.getValue();
	
	if ( !val || val.stringValue == '' ) {
		return '';
	}
	
	return val.formattedValue;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.toggleExpanded = function() {
	this._isExpanded = !this._isExpanded;
	
	//The bucket (not just the cell) needs to know about the expansion before the "table refresh"
	//so that it can provide the correct bucket rows.
	this._expandCallback(this._isExpanded);

	this._graphContext.notifyTableRefresh();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.setExpandedDuringExpandAll = function(expanded) {
	this._isExpanded = expanded;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.setOddRow = function(isOddRow) {
	this._isOddRow = isOddRow;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getDatapoint = function() {
	return this._rdp;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getColumnIndex = function() {
	return this._columnIndex;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getText = function() {
	return this._text;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getTextColor = function() {
	return this._textColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getAlign = function() {
	return this._alignH;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getVAlign = function() {
	return this._alignV;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.hasWordWrap = function() {
	return this._hasWordWrap;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getSwatchShape = function() {
	return this._swatchShape;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getSwatchShapeDir = function() {
	return this._swatchDir;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.isSwatchShapeZero = function() {
	return this._isSwatchZero;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.isOddRow = function() {
	return this._isOddRow;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.isCount = function() {
	return (this._summType == IDB.ReportData.SummaryCount);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.isSummation = function() {
	return (this._summType == IDB.ReportData.SummarySum);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.isExpanded = function() {
	return this._isExpanded;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.isPivot = function() {
	return (this._mode == IDB.ReportBucketCell.ModePivot);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.getGroupDepth = function() {
	return this._groupDepth;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.isDataRow = function() {
	return (this._mode == IDB.ReportBucketCell.ModeRow);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketCell.prototype.isXAxis = function() {
	return (this._rdp.axis.axisId == 0);
};

/*====================================================================================================*/
IDB.ReportBucketSet = function(graphContext) {
	this._graphContext = graphContext;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketSet.prototype._build = function() {
	var gc = this._graphContext;
	var rd = gc.reportData;
	var sett = gc.settings;

	var s = sett.get('HiddenExpandState');
	s = s.substring(1, s.length-1);
	
	this._bucket = new IDB.ReportBucket(gc, s, null, -1, rd.getGroupIndexes(), rd.getRdpRows());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketSet.prototype.doExpandAll = function(expand) {
	this._bucket.doExpandAll(expand);
	this._graphContext.notifyTableRefresh();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketSet.prototype.sort = function(columnIndex, direction) {
	this._bucket.sort(columnIndex, direction);
	this._graphContext.notifyTableRefresh(); //EvtSortUpdate
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketSet.prototype.getBucket = function() {
	return this._bucket;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportBucketSet.prototype.print = function() {
	var rootBucket = this.getBucket();
	var rows = rootBucket.getRows();
	var row = rows[0];
	var str = ' # ';
	var i, j, cell, cellStr;

	for ( j = 0 ; j < row.length ; ++j ) {
		cell = row[j];
		cellStr = cell.getDatapoint().getAxis().axisName;
		str += ('              '+cellStr).slice(-14)+' # ';
	}

	console.log(str);

	for ( i = 0 ; i < rows.length ; ++i ) {
		row = rows[i];
		str = ' | ';

		for ( j = 0 ; j < row.length ; ++j ) {
			cell = row[j];
			cellStr = (cell ? cell.getText() : '::::::::::::::');
			str += ('              '+cellStr).slice(-14)+' | ';
		}

		console.log(str);
	}
};

/*====================================================================================================*/
IDB.ReportContext = function(graphContext) {
	this._graphContext = graphContext;

	// Wrap properties of the IDB.GraphContext object
	this.settings = graphContext.settings;
	this.datapointGrid = graphContext.datapointGrid;
	this.refreshDisplay = graphContext.refreshDisplay;
	this.onDatapointClicked = graphContext.onDatapointClicked;
	this.onDatapointOver = graphContext.onDatapointOver;
	this.onDatapointOut = graphContext.onDatapointOut;

	this.reportData = new IDB.ReportData(graphContext);
	
	this._datapointsUpdatedListeners = [];
	this._tableRefreshListeners = [];
	this._columnDragListeners = [];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
Object.defineProperties(IDB.ReportContext.prototype, {
	
	isOutgoingDatapointEvent: {
		get: function() {
			return this._graphContext.isOutgoingDatapointEvent;
		}
	}

});


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportContext.prototype.notifyDatapointsUpdated = function() {
	this._graphContext.notifyDatapointsUpdated();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportContext.prototype.notifyTableRefresh = function() {
	var list = this._tableRefreshListeners;

	for ( var i in list ) {
		list[i].listener();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportContext.prototype.notifyColumnDrag = function() {
	var list = this._columnDragListeners;

	for ( var i in list ) {
		list[i]();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportContext.prototype.addDatapointsUpdatedListener = function(listener) {
	this._graphContext.addDatapointsUpdatedListener(listener);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportContext.prototype.addTableRefreshListener = function(listener, priority) {
	var list = this._tableRefreshListeners;
	var len = list.length;
	var added = false;

	var obj = {
		listener: listener,
		priority: priority
	};

	for ( var i = 0 ; i < len ; ++i ) {
		if ( list[i].priority < priority ) {
			list.splice(i, 0, obj);
			added = true;
		}
	}

	if ( !added ) {
		list.push(obj);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportContext.prototype.addColumnDragListener = function(listener) {
	this._columnDragListeners.push(listener);
};
/*global IDB */
// "use strict";
/*====================================================================================================*/
IDB.ReportData = function(graphContext) {
	this._graphContext = graphContext;
	this._buildAxesAndDatapoints();
	this._buildGroupingIndexes();
	this._buildColumnIndexes();
	this._buildSwatchShapes();
	this._buildSummaryTypes();
	this._buildAlignments();
	this._buildWordWraps();
	this._buildHeaderWordWraps();
	this._buildColumnWidths();
};

IDB.ReportData.SummarySum = 1;
IDB.ReportData.SummaryAvg = 2;
IDB.ReportData.SummaryMax = 3;
IDB.ReportData.SummaryMin = 4;
IDB.ReportData.SummaryCount = 5;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildAxesAndDatapoints = function() {
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var sett = gc.settings;
	var hideX = sett.getBool('HideXAxis');
	var numYCols = dpGrid.getNumYCols(false);
	var numXRows = dpGrid.getNumXRows();
	var axes = [];
	var rows = [];
	var dpRows = [];
	var ri, ci, dp, newDp, rdp;

	for ( ri = 0 ; ri < numXRows ; ++ri ) {
		rows[ri] = [];
		dpRows[ri] = [];
	}

	if ( !hideX ) {
		axes.push(dpGrid.getXAxis());
		
		for ( ri = 0 ; ri < numXRows ; ++ri ) {
			dp = dpGrid.getDatapointAt(ri, 0, false);
			newDp = new IDB.Datapoint(dp.xAxis, dp.xAxis, dp.dataRow, dpRows[ri]);

			rdp = new IDB.ReportDatapoint(dp.xAxis, dp.xValue, dp.rangeColor, newDp);
			rows[ri].push(rdp);
			dpRows[ri].push(newDp);
		}
	}

	for ( ci = 0 ; ci < numYCols ; ++ci ) {
		axes.push(dpGrid.getYAxis(ci, false));
		
		for ( ri = 0 ; ri < numXRows ; ++ri ) {
			dp = dpGrid.getDatapointAt(ri, ci, false);
			newDp = new IDB.Datapoint(dp.xAxis, dp.yAxis, dp.dataRow, dpRows[ri]);
			
			rdp = new IDB.ReportDatapoint(dp.yAxis, dp.yValue, dp.rangeColor, newDp);
			rows[ri].push(rdp);
			dpRows[ri].push(newDp);
		}
	}

	this._allAxes = axes;
	this._rdpRows = rows;
	this._defaultRangeList = dpGrid.getXAxis().getRangeList();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildGroupingIndexes = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var axes = this._allAxes;
	var nameStr = sett.get('GroupingIndexes');
	var names = IDB.StringUtils.parseTokens(nameStr, '|', '/');
	var indexes = [];
	var axisNameMap = {};
	var i, axisIndex;

	if ( !names.length ) {
		this._groupIndexes = indexes;
		return;
	}
	
	for ( i = 0 ; i < axes.length ; ++i ) {
		axisNameMap[axes[i].axisName] = i;
	}
	
	for ( i = 0 ; i < names.length ; ++i ) {
		axisIndex = axisNameMap[names[i]];

		if ( !IDB.isNil(axisIndex) ) {
			indexes.push(axisIndex);
		}
	}

	this._groupIndexes = indexes;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildColumnIndexes = function() {
	//Create a colIndexes list that starts with the indexes for grouped columns, then includes the
	//remaining (non-grouped) column indexes. The order of indexes is important.

	var axes = this._allAxes;
	var grpIndexes = this._groupIndexes;
	var colIndexes = grpIndexes.slice(); //copy
	var groupMap = {};

	for ( var i = 0 ; i < grpIndexes.length ; ++i ) {
		groupMap[grpIndexes[i]] = true;
	}

	for ( i = 0 ; i < axes.length ; ++i ) {
		if ( !groupMap[i] ) {
			colIndexes.push(i);
		}
	}
	
	this._columnIndexes = colIndexes;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildSwatchShapes = function() {
	var map = this._getAxisNameMap('CellShowRangeShapes');
	var shapes = [];

	if ( map ) {
		var axes = this._allAxes;
		var i, shapeVal;

		for ( i = 0 ; i < axes.length ; ++i ) {
			shapeVal = map[axes[i].axisName];
			shapes.push(shapeVal ? Number(shapeVal) : 0);
		}
	}

	this._swatchShapes = shapes;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildSummaryTypes = function() {
	var map = this._getAxisNameMap('PivotSummaryType');
	var summaries = [];

	if ( map ) {
		var axes = this._allAxes;
		var i, typeVal;
	
		for ( i = 0 ; i < axes.length ; ++i ) {
			typeVal = map[axes[i].axisName];
			summaries.push(typeVal ? Number(typeVal) : 0);
		}
	}

	this._summaryTypes = summaries;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildAlignments = function() {
	var map = this._getAxisNameMap('ColumnAlignments');
	var aligns = [];

	if ( map ) {
		var axes = this._allAxes;
		var i, alignVal;
	
		for ( i = 0 ; i < axes.length ; ++i ) {
			alignVal = map[axes[i].axisName];
			aligns.push(alignVal ? alignVal : 'left');
		}
	}

	this._alignments = aligns;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildWordWraps = function() {
	var map = this._getAxisNameMap('CellWordWrap');
	var wraps = [];

	if ( map ) {
		var axes = this._allAxes;
		var i, wrapVal;
	
		for ( i = 0 ; i < axes.length ; ++i ) {
			wrapVal = map[axes[i].axisName];
			wraps.push(wrapVal ? wrapVal : '0.mid');
		}
	}
	
	this._wordWraps = wraps;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildHeaderWordWraps = function() {
    this._hasWrapEnabledHeaderColumn = false;
	var map = this._getAxisNameMap('HeaderWordWrap');
	var wraps = [];

	if ( map ) {
		var axes = this._allAxes;
		var i, wrapVal;
	
		for ( i = 0 ; i < axes.length ; ++i ) {
			wrapVal = map[axes[i].axisName];
            if(wrapVal) {
                this._hasWrapEnabledHeaderColumn = this._hasWrapEnabledHeaderColumn || (wrapVal.indexOf("1.") === 0);
            }
			wraps.push(wrapVal ? wrapVal : '0.mid');
		}
	}
	
	this._headerWordWraps = wraps;
};

/**
 * Returns true if any of the header columns have word wrap
 * enabled.
 */
IDB.ReportData.prototype.hasWrapEnabledHeaderColumn = function() {
	return this._hasWrapEnabledHeaderColumn;
};




/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._buildColumnWidths = function() {
	var map = this._getAxisNameMap('HiddenColWidths');
	var widths = [];

	if ( map ) {
		var axes = this._allAxes;
		var i, widthVal;
	
		for ( i = 0 ; i < axes.length ; ++i ) {
			widthVal = map[axes[i].axisName];
			widths.push(widthVal ? Number(widthVal) : 80);
		}
	}
	
	this._columnWidths = widths;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype._getAxisNameMap = function(settingName) {
	var pairsStr = this._graphContext.settings.get(settingName);
	var pairs = IDB.StringUtils.parseTokens(pairsStr, '|', '/');

	if ( !pairs.length ) {
		return null;
	}
	
	var axisNameMap = {};
	var i, pair;
	
	for ( i = 0 ; i < pairs.length ; ++i ) {
		pair = IDB.StringUtils.parseTokens(pairs[i], '=', '$');
		axisNameMap[pair[0]] = pair[1];
	}

	return axisNameMap;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getAllAxes = function() {
	return this._allAxes;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getRdpRows = function() {
	return this._rdpRows;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getGroupIndexes = function() {
	return this._groupIndexes;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.isAxisGrouped = function(axisId) {
	var grpInds = this._groupIndexes;
	
	for ( var i in grpInds ) {
		if ( grpInds[i] == axisId ) {
			return true;
		}
	}

	return false;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getColumnIndexByAxis = function(axisId) {
	var colInds = this._columnIndexes;

	for ( var i in colInds ) {
		if ( colInds[i] == axisId ) {
			return i;
		}
	}

	return -1;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getColumnIndexByDisplayOrder = function(orderIndex) {
	return this._columnIndexes[orderIndex];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getSwatchShape = function(axisId) {
	var ss = this._swatchShapes;
	return (ss && axisId < ss.length ? ss[axisId] : 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getSummaryType = function(axisId) {
	return this._summaryTypes[axisId];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getAlignment = function(axisId) {
	var a = this._alignments;
	return (a && axisId < a.length ? a[axisId] : 'left');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.hasWordWrap = function(axisId) {
	var ww = this._wordWraps;

	if ( !ww || !ww[axisId] ) {
		return false;
	}

	var wrap = ww[axisId].split('.')[0];
	return (wrap == '1');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.hasHeaderWordWrap = function(axisId) {
	var ww = this._headerWordWraps;

	if ( !ww || !ww[axisId] ) {
		return false;
	}

	var wrap = ww[axisId].split('.')[0];
	return (wrap == '1');
};


/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getVerticalAlignment = function(axisId) {
	var ww = this._wordWraps;

	if ( !ww || !ww[axisId] ) {
		return -1;
	}

	var align = ww[axisId].split('.')[1];
	return (align == 'mid' ? 0 : (align == 'bot' ? 1 : -1));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getHeaderVerticalAlignment = function(axisId) {
	var ww = this._headerWordWraps;

	if ( !ww || !ww[axisId] ) {
		return -1;
	}

	var align = ww[axisId].split('.')[1];
	return (align == 'mid' ? 0 : (align == 'bot' ? 1 : -1));
};


/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getColumnWidth = function(axisId) {
	var cw = this._columnWidths;
	return (cw && axisId < cw.length ? cw[axisId] : 80);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.getAxisRangeList = function(axis) {
	var rl = axis.getRangeList();
	
	if ( rl && rl.length ) {
		return rl;
	}

	return this._defaultRangeList;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportData.prototype.setColumnWidth = function(axisId, width) {
	if ( !this._columnWidths ) {
		this._columnWidths = [];
	}

	this._columnWidths[axisId] = width;
};

/*====================================================================================================*/
IDB.ReportDatapoint = function(axis, value, rangeColor, graphDatapoint) {
	this._axis = axis;
	this._value = value;
	this._axisColor = axis.axisColor;
	this._rangeColor = rangeColor;
	this._graphDatapoint = graphDatapoint;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportDatapoint.prototype.getAxis = function() {
	return this._axis;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportDatapoint.prototype.getValue = function() {
	return this._value;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportDatapoint.prototype.getAxisColor = function() {
	return this._axisColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportDatapoint.prototype.getRangeColor = function() {
	return this._rangeColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportDatapoint.prototype.getGraphDatapoint = function() {
	return this._graphDatapoint;
};

/*====================================================================================================*/
IDB.ReportGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);

	var gc = this._graphContext;
	var isTab = (gc.settings.get('GraphTypeCode') == 'Tabular');
	var noNumAxis = (gc.datapointGrid.getNumYCols(true) == 0);

	this._info.numericYOnly = false;
	this._info.legendStyle = (isTab || noNumAxis ? IDB.LS.NONE : IDB.LS.RANGES);
	this._info.calloutStyle = 'none';
};

IDB.ReportGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.ReportGraph.prototype.constructor = IDB.ReportGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportGraph.prototype._build = function() {
	var gc = this._graphContext;
	var rc = new IDB.ReportContext(gc);

	this._bucketSet = new IDB.ReportBucketSet(rc);
	
	var scrollModel = new IDB.ReportScroll(rc, this._bucketSet);

	this._scroll = new IDB.ReportScrollView(scrollModel);
	this._stageLayer.add(this._scroll.getDisplay());

	/*var getLumContrast = function(r,g,b) {
		var rgb = {r:r,g:g,b:b};
		var col = IDB.ColorHelper.getContrastingColor(rgb);
		var lum = IDB.ColorHelper.rgbToLuminance(r,g,b);
		var int = col.r<<16 ^ col.g <<8 ^ col.b;

		console.log(r+' '+g+' '+b+' ==> ,"lum":'+lum+',"x":{"r":'+
			col.r+',"g":'+col.g+',"b":'+col.b+',"css":"'+IDB.rgbToCSS(col)+'","i":'+int+'}');
	};

	getLumContrast(255, 255, 255);
	getLumContrast(0, 0, 0);
	getLumContrast(128, 128, 128);
	getLumContrast(187, 187, 187);
	getLumContrast(255, 0, 0);
	getLumContrast(207, 216, 230);
	getLumContrast(64, 91, 128);
	getLumContrast(221, 0, 0);
	getLumContrast(68, 68, 68);
	getLumContrast(51, 51, 51);
	getLumContrast(153, 153, 153);
	getLumContrast(64, 91, 128);
	getLumContrast(170, 170, 170);
	getLumContrast(102, 102, 102);
	getLumContrast(255, 208, 0);
	getLumContrast(68, 68, 68);
	getLumContrast(244, 244, 244);*/
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportGraph.prototype._getRelevantDatapoints = function() {
	return this._bucketSet.getBucket().getDatapoints();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportGraph.prototype._update = function(width, height) {
	this._scroll.setSize(width, height);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportGraph.prototype.getLegendAxes = function() {
	return this._dataSet.getGraphAxes(true);
};

/*====================================================================================================*/
IDB.ReportScroll = function(graphContext, bucketSet) {
	this._graphContext = graphContext;
	this._bucketSet = bucketSet;

	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScroll.prototype._build = function() {
	this._table = new IDB.ReportTable(this._graphContext, this._bucketSet);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScroll.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScroll.prototype.setOnTableRefresh = function(callback) {
	this._graphContext.addTableRefreshListener(callback, 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScroll.prototype.setOnColumnDrag = function(callback) {
	this._graphContext.addColumnDragListener(callback);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScroll.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScroll.prototype.getTable = function() {
	return this._table;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScroll.prototype.isOutgoingDatapointEvent = function() {
	return this._graphContext.isOutgoingDatapointEvent;
};

/*====================================================================================================*/
IDB.ReportScrollView = function(model) {
	this._model = model;
	model.setOnTableRefresh(IDB.listener(this, '_onTableSizeUpdate'));
	model.setOnColumnDrag(IDB.listener(this, '_onTableSizeUpdate'));
	model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScrollView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	this._table = new IDB.ReportTableView(model.getTable());
	this._hold.add(this._table.getDisplay());

	this._scrollV = new IDB.GraphScroll();
	this._scrollV.onVisualUpdate(IDB.listener(model, 'refreshDisplay'));
	this._scrollV.onScroll(IDB.listener(this, '_onScrollV'));
	this._scrollV.lineScrollSize = 16;
	this._display.add(this._scrollV.getDisplay());
	
	this._scrollH = new IDB.GraphScroll(true);
	this._scrollH.onVisualUpdate(IDB.listener(model, 'refreshDisplay'));
	this._scrollH.onScroll(IDB.listener(this, '_onScrollH'));
	this._scrollH.lineScrollSize = 16;
	this._scrollH.getDisplay().rotation(-90);
	this._display.add(this._scrollH.getDisplay());

	//LATER: KineticJS doesn't support mouse-wheel scrolling
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScrollView.prototype._onTableSizeUpdate = function() {
	this.setSize(this._savedW, this._savedH);
	this._model.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScrollView.prototype._onDatapointsUpdated = function() {
	if ( this._model.isOutgoingDatapointEvent() ) {
		return;
	}

	var pos = this._table.getDatapointHighlightPos();

	if ( pos == null ) {
		return;
	}

	var sv = this._scrollV;

	if ( sv.scrollPos == pos ) {
		return;
	}

	sv.scrollPos = pos;
	sv.refresh();
	this._onScrollV();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScrollView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScrollView.prototype.setSize = function(width, height) {
	this._savedW = width;
	this._savedH = height;

	var table = this._table;
	var sv = this._scrollV;
	var sh = this._scrollH;

	var tableD = table.getDisplay();
	var svD = sv.getDisplay();
	var shD = sh.getDisplay();

	var fullW = table.getScrollWidth();
	var fullH = table.getScrollHeight();
	var scrollThick = 16;
	var tableMaskH = table.getScrollMaskHeight(height);

	if ( fullH > tableMaskH-scrollThick && fullW > width-scrollThick ) {
		svD.visible(true);
		shD.visible(true);
	}
	else {
		svD.visible(fullH > tableMaskH);
		shD.visible(fullW > width);
	}
	
	var maskW = width-(svD.visible() ? scrollThick : 0);
	var maskH = height-(shD.visible() ? scrollThick : 0);
	var tableH = maskH-1;
	
	this._hold.clip({
		x: 0,
		y: 0,
		width: maskW,
		height: maskH
	});
	
	table.setSize(maskW, tableH);
	var usedTableH = table.getUsedHeight(maskH);

	if ( shD.visible() ) {
		shD.y(usedTableH+scrollThick);
		sh.setSize(scrollThick, maskW);
	}
	else {
		tableD.x(Math.floor((maskW-fullW)/2));
	}
	
	if ( svD.visible() ) {
		svD.opacity(1)
			.x(shD.visible() ? maskW : Math.min(tableD.x()+fullW, maskW))
			.y(table.getTableTop());
		sv.setSize(scrollThick, usedTableH-svD.y());
	}
	
	this._positionDraggersByTable(maskH);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScrollView.prototype._positionDraggersByTable = function(height) {
	var table = this._table;
	var sv = this._scrollV;
	var sh = this._scrollH;

	if ( sv.getDisplay().visible() ) {
		var mh = table.getScrollMaskHeight(height);

		sv.pageSize = mh;
		sv.maxScrollPos = table.getScrollHeight()-mh;
		sv.scrollPos = IDB.MathUtil.clamp(table.getScrollPos(), 0, sv.maxScrollPos);
		sv.refresh();
		this._onScrollV();
	}
	else {
		table.setScrollPos(0);
	}
	
	if ( sh.getDisplay().visible() ) {
		var tw = table.getScrollWidth();

		sh.pageSize = this._hold.clip().width;
		sh.maxScrollPos = tw-sh.pageSize;
		sh.scrollPos = -table.getDisplay().x();
		sh.refresh();
		this._onScrollH();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScrollView.prototype._onScrollV = function() {
	this._table.setScrollPos(this._scrollV.scrollPos);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportScrollView.prototype._onScrollH = function() {
	this._table.setHorizScrollPos(this._scrollH.scrollPos);
};

/*====================================================================================================*/
IDB.ReportTable = function(graphContext, bucketSet) {
	this._graphContext = graphContext;
	graphContext.addTableRefreshListener(IDB.listener(this, '_onTableRefresh'), 1);

	this._bucketSet = bucketSet;
	this._bucket = bucketSet.getBucket();

	this._build();
};

IDB.ReportTable.EmptyFooterLabelText = '\u00e5\u00e5\u00e5'; //prints as '���'


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype._build = function() {
	var gc = this._graphContext;
	var bucketSet = this._bucketSet;
	var sett = gc.settings;
	var rd = gc.reportData;
	var axes = rd.getAllAxes();
	var axesLen = axes.length;
	var onSortChange = IDB.listener(this, '_onSortChange');
	var i, axis, dv, cell, colIndex;

    this._hasWrapEnabledHeaderColumn = rd.hasWrapEnabledHeaderColumn();

	this._grid = new IDB.ReportTableGrid(gc, bucketSet);
	this._headerCells = [];

	for ( i = 0 ; i < axesLen ; ++i ) {
		axis = axes[i];
		dv = new IDB.DataValue('STRING', { s: axis.axisName });

		colIndex = rd.getColumnIndexByAxis(i);

		cell = new IDB.ReportTableHeaderCell(gc, colIndex, true, axis, dv, null,
			rd.getAlignment(i), rd.getSwatchShape(i), rd.getSummaryType(i), rd.hasWordWrap(i));
		cell.setSortChangeListener(onSortChange);

		this._headerCells[colIndex] = cell;
	}

	if ( this.showFooter() ) {
		this._footerCells = [];

		var bucket = bucketSet.getBucket();
		var footerLbl = sett.get('TableFooterLabel');
		var aggVal, aggType;

		if ( footerLbl == IDB.ReportTable.EmptyFooterLabelText ) {
			footerLbl = '';
		}

		for ( i = 0 ; i < axesLen ; ++i ) {
			axis = axes[i];
			aggVal = bucket.getAggregateValue(i);
			colIndex = rd.getColumnIndexByAxis(i);
			aggType = rd.getSummaryType(i);

			if ( aggVal != null ) {
				dv = new IDB.DataValue('NUMBER', {
					n: aggVal,
					f: (aggType == IDB.ReportData.SummaryCount ?
						aggVal+'' : sett.formatNumber(aggVal, axis.numFmt))
				});
			}
			else {
				dv = new IDB.DataValue('STRING', { s: (colIndex == 0 ? footerLbl : '') });
			}

			this._footerCells[colIndex] = new IDB.ReportTableHeaderCell(gc, colIndex, false, axis, dv,
				rd.getAxisRangeList(axis), rd.getAlignment(i),
				rd.getSwatchShape(i), aggType);
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype._onSortChange = function(columnIndex) {
	var cells = this._headerCells;
	var bucketSet = this._bucketSet;

	bucketSet.sort(columnIndex, cells[columnIndex].getSortDir());
	
	for ( var i = 0 ; i < cells.length ; ++i ) {
		if ( i == columnIndex ) {
			continue;
		}

		cells[i].setSortDir(0);
	}
	
	this._onTableRefresh();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype._onTableRefresh = function() {
	this._grid.refreshRows();

	if ( this._onRowRefresh ) {
		this._onRowRefresh();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.doExpandAll = function(expand) {
	this._bucketSet.doExpandAll(expand);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.setCellWidth = function(index, width) {
	var rd = this._graphContext.reportData;
	var colIndex = rd.getColumnIndexByDisplayOrder(index);
	rd.setColumnWidth(colIndex, width);
	this._graphContext.notifyColumnDrag();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.onRowRefresh = function(callback) {
	this._onRowRefresh = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getGrid = function() {
	return this._grid;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getHeaderCellCount = function() {
	return this._headerCells.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getHeaderCell = function(index) {
	return this._headerCells[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getFooterCell = function(index) {
	return this._footerCells[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getCellWidth = function(index) {
	var rd = this._graphContext.reportData;
	var colIndex = rd.getColumnIndexByDisplayOrder(index);
	return rd.getColumnWidth(colIndex);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getHeaderLabelOptions = function() {
	return this._graphContext.settings.get('HeaderLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getHeaderXAxisColor = function() {
	return this._graphContext.settings.get('HeaderXAxisColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.showFooter = function() {
	return this._graphContext.settings.getBool('ShowTableFooter');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.showBorder = function() {
	return this._graphContext.settings.getBool('TableBorderShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getBorderColor = function() {
	return this._graphContext.settings.get('TableBorderColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.showControls = function() {
	return this._graphContext.settings.getBool('ShowTableControls');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getConrolsLabelOptions = function() {
	var sett = this._graphContext.settings;
	var opt = sett.get('CellLabelOptions');

	return {
		size: opt.size,
		bold: opt.bold,
		color: sett.get('TableControlsFontColor')
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTable.prototype.getControlsBgColor = function() {
	return this._graphContext.settings.get('TableControlsColor');
};

/*====================================================================================================*/
IDB.ReportTableCell = function(graphContext, bucketCell) {
	this._graphContext = graphContext;
	this._bucketCell = bucketCell;

	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var bucketCell = this._bucketCell;
	var lblOpt = this._graphContext.settings.get('CellLabelOptions');

	var showBox = sett.getBool('CellShowRangeSwatches');
	var showBg = sett.getBool('CellShowRangeColorBg');
	var hasSpecialShape = (bucketCell.getSwatchShape() != 0);
	var rdp = bucketCell.getDatapoint();
	var axis = rdp.getAxis();
	var isNum = (axis && axis.dataTypeIsNumber);

	this._showSwatchBox = (!showBg && (showBox || hasSpecialShape) && isNum);
	this._showSwatchBg = (showBg && isNum && bucketCell.isDataRow());

	this._graphDatapoint = bucketCell.getDatapoint().getGraphDatapoint();

	this._labelOptions = {
		show: lblOpt.show,
		size: lblOpt.size,
		color: bucketCell.getTextColor(),
		bold: lblOpt.bold
	};
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.setOnTableRefresh = function(callback) {
	this._graphContext.addTableRefreshListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.getBucketCell = function() {
	return this._bucketCell;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.showRangeSwatch = function() {
	return this._showSwatchBox;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.showRangeBackground = function() {
	return this._showSwatchBg;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.getRangeColor = function() {
	var bc = this._bucketCell;

	if ( bc.isCount() || bc.isSummation() ) {
		return null;
	}

	return bc.getDatapoint().getRangeColor();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.getAxisColor = function() {
	var axis = this._bucketCell.getDatapoint().getAxis();
	return (axis.axisId == 0 ? this._graphContext.settings.get('HeaderXAxisColor') : axis.axisColor);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.getVisualState = function() {
	var graphDp = this._graphDatapoint;
	return (graphDp ? graphDp.getVisualState() : IDB.VS.NORMAL);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.getLabelOptions = function() {
	return this._labelOptions;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.getBgColor = function() {
	return this._graphContext.settings.get('CellBgColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.showGridHoriz = function() {
	return this._graphContext.settings.getBool('GridHorizShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.showGridVert = function() {
	return this._graphContext.settings.getBool('GridVertShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.getGridColor = function() {
	return this._graphContext.settings.get('GridColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.showLevelShading = function() {
	return this._graphContext.settings.getBool('PivotLevelShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.getLevelShadingColor = function() {
	return this._graphContext.settings.get('PivotLevelShadingColor');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.createExpandClickListener = function() {
	return IDB.leftClickListener(this, '_handleExpandClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, "_handleClick");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.createOverListener = function() {
	return IDB.listener(this, "_handleOver");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype.createOutListener = function() {
	return IDB.listener(this, "_handleOut");
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype._handleExpandClick = function() {
	return this._bucketCell.toggleExpanded();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype._handleClick = function() {
	if ( this._graphDatapoint ) {
		this._graphContext.onDatapointClicked(this._graphDatapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype._handleOver = function() {
	if ( this._graphDatapoint ) {
		this._graphContext.onDatapointOver(this._graphDatapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCell.prototype._handleOut = function() {
	if ( this._graphDatapoint ) {
		this._graphContext.onDatapointOut(this._graphDatapoint);
	}
};

/*====================================================================================================*/
IDB.ReportTableCellView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
	this._buildKinetic();
};

IDB.ReportTableCellView.Gap = 5;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype._buildKinetic = function() {
	var model = this._model;
	var bucketCell = model.getBucketCell();
	var bgColor = model.getBgColor();

	var lblOpt = model.getLabelOptions();
	lblOpt.align = bucketCell.getAlign();

	this._display = new Kinetic.Group();

	this._box = new Kinetic.Rect({
		fill: bgColor.css,
		opacity: (bucketCell.isOddRow() ? 0.5 : 1.0)
	});
	this._display.add(this._box);
	
	////

	if ( model.showLevelShading() && bucketCell.getGroupDepth() < 1 ) {
		this._groupColor = new Kinetic.Rect({
			fill: IDB.toRGBA(model.getLevelShadingColor(), 1-bucketCell.getGroupDepth())
		});
		this._display.add(this._groupColor);
	}

	if ( bucketCell.isPivot() ) {
		this._plusIcon = IDB.ReportTableView.buildPlusIcon(false);
		this._plusIcon.visible(false);
		this._display.add(this._plusIcon);

		this._minusIcon = IDB.ReportTableView.buildPlusIcon(true);
		this._minusIcon.visible(false);
		this._display.add(this._minusIcon);

		this._updateExpanded();

		model.setOnTableRefresh(IDB.listener(this, '_updateExpanded'));
		this._display.on('click', model.createExpandClickListener());
	}
	else {
		this._display.on('click', model.createClickListener());
		this._display.on('mouseover', model.createOverListener());
		this._display.on('mouseout', model.createOutListener());
	}

	this._display._touchHandler = new IDB.TouchHandler(this._display);

	////

	var showSwatchBox = model.showRangeSwatch();
	var showSwatchBg = model.showRangeBackground();

	if ( showSwatchBox || showSwatchBg ) {
		this._swatchSize = Math.round((lblOpt.size+4)*0.65);
		this._swatchInvMult = 1;

		var rangeColor = model.getRangeColor();
		
		if ( rangeColor ) {
			this._swatch = new Kinetic.Line({
				points: this._getSwatchShapePoints(bucketCell.getSwatchShape()),
				closed: true,
				fill: rangeColor.css
			});
		}
		else {
			this._swatch = new Kinetic.Rect({
				width: 100,
				height: 100,
				stroke: bgColor.x.css,
				strokeWidth: 1,
				opacity: 0.25
			});
		}
		
		if ( showSwatchBox ) {
			this._swatch
				.scaleX(this._swatchSize/100)
				.scaleY(this._swatchSize/100*bucketCell.getSwatchShapeDir())
				.rotation(bucketCell.isSwatchShapeZero() ? -90*this._swatchInvMult : 0);
		}

		this._display.add(this._swatch);
	}

	////

	this._shade = new Kinetic.Rect({
		fill: model.getAxisColor().css,
		opacity: 0.25,
		visible: false
	});
	this._display.add(this._shade);

	////

	this._label = new IDB.GraphLabel(lblOpt, bucketCell.getText());
	this._label.wordWrap = bucketCell.hasWordWrap();
	this._display.add(this._label.getDisplay());

	////

	this._borderHold = IDB.ReportTableCellView.buildBorder(
		model.showGridHoriz(), model.showGridVert(), model.getGridColor(), 1000);

	if ( this._borderHold ) {
		this._display.add(this._borderHold);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype._getSwatchShapePoints = function(shape) {
	this._swatchInvMult = (shape == 3 || shape == 5 ? 1 : -1);
	var my = this._swatchInvMult;
	
	if ( shape == 2 || shape == 3 ) {
		return [
			  0,  50*my,
			-50,   0*my,
			-25,   3*my,
			-30, -50*my,
			 30, -50*my,
			 25,   3*my,
			 50,   0*my,
			  0,  50*my
		];
	}
	
	if ( shape == 4 || shape == 5 ) {
		return [
			  0,  50*my,
			-50, -50*my,
			 50, -50*my,
			  0,  50*my
		];
	}

	return [
		-50, -50,
		 50, -50,
		 50,  50,
		-50,  50
	];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.buildBorder = function(showHoriz, showVert, color, size) {
	if ( !showHoriz && !showVert ) {
		return null;
	}

	var config = {
		stroke: color.css,
		strokeWidth: 1,
		strokeScaleEnabled: false
	};

	var hold = new Kinetic.Group();

	var line;
	
	if ( showHoriz ) {
		config.points = [
			0, 0,
			size, 0
		];

		line = new Kinetic.Line(config);
		hold.add(line);

		config.points = [
			size, size,
			0, size
		];

		line = new Kinetic.Line(config);
		hold.add(line);
	}
	
	if ( showVert ) {
		config.points = [
			0, 0,
			0, size
		];

		line = new Kinetic.Line(config);
		hold.add(line);

		config.points = [
			size, size,
			size, 0
		];

		line = new Kinetic.Line(config);
		hold.add(line);
	}

	return hold;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype.getHeight = function() {
	return this._label.height+IDB.ReportTableCellView.Gap*2;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype.setWidth = function(width) {
	if ( width == this._box.width() ) {
		return;
	}

	var lbl = this._label;
	var pad = IDB.ReportTableCellView.Gap;

	this._box.width(width);
	this._shade.width(width);
	lbl.x = pad;

	if ( this._groupColor ) {
		this._groupColor.width(width);
	}

	if ( this._borderHold ) {
		this._borderHold.scaleX(width/1000);
	}
	
	if ( this._plusIcon ) {
		this._plusIcon.x(pad);
		this._minusIcon.x(pad);
		lbl.x += 8+pad;
	}
	
	lbl.width = width-lbl.x-pad;

	if ( !lbl.wordWrap ) {
		lbl.truncate();
	}

	if ( this._swatch ) {
		var model = this._model;
		var swatchSize = this._swatchSize;

		if ( model.showRangeBackground() ) {
			this._swatch
				.x(width/2)
				.scaleX(width/100);
		}
		else {
			this._swatch.x(Math.round(width-pad-swatchSize/2)+0.5);
			lbl.width -= swatchSize+pad;
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype.setHeight = function(height) {
	var pad = IDB.ReportTableCellView.Gap;

	this._label.y = this._getVertAlignY(height, this._label.height, pad)+1;

	this._box.height(height);
	this._shade.height(height);
	
	if ( this._groupColor ) {
		this._groupColor.height(height);
	}
	
	if ( this._plusIcon ) {
		var iconY = this._getVertAlignY(height, 8, pad);
		this._plusIcon.y(iconY);
		this._minusIcon.y(iconY);
	}

	if ( this._swatch ) {
		var model = this._model;
		var swatchSize = this._swatchSize;

		if ( model.showRangeBackground() ) {
			this._swatch
				.y(height/2)
				.scaleY(height/100);
		}
		else {
			var swatchY = this._getVertAlignY(height, swatchSize, pad);
			this._swatch.y(Math.round(swatchY+swatchSize/2)-0.5);
		}
	}

	if ( this._borderHold ) {
		this._borderHold.scaleY(height/1000);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype._getVertAlignY = function(fullH, itemH, pad) {
	var yMult = (this._model.getBucketCell().getVAlign()+1)/2;
	return pad + Math.round((fullH-itemH-pad*2)*yMult);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype._updateExpanded = function() {
	var exp = this._model.getBucketCell().isExpanded();
	this._plusIcon.visible(!exp);
	this._minusIcon.visible(exp);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableCellView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getVisualState();
	this._display.opacity(state == IDB.VS.FADED ? 0.25 : 1.0);
	this._shade.visible(state == IDB.VS.HIGHLIGHTED);
};

/*====================================================================================================*/
IDB.ReportTableGrid = function(graphContext, bucketSet) {
	this._graphContext = graphContext;
	this._bucketSet = bucketSet;

	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGrid.prototype._build = function() {
	this._rowMap = {};
	this.refreshRows();

	var rd = this._graphContext.reportData;
	var colLen = rd.getAllAxes().length;
	
	this._useWrap = false;

	for ( var i = 0 ; i < colLen ; ++i ) {
		if ( rd.hasWordWrap(i) ) {
			this._useWrap = true;
			break;
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGrid.prototype.refreshRows = function() {
	//Update the current list of rows, pulling them from a cached list when they exist. The bucket row
	//list can change due to sorting and pivot expand/collapse events.

	var gc = this._graphContext;
	var rowMap = this._rowMap;
	var bucketRows = this._bucketSet.getBucket().getRows();
	var rows = [];
	var i, bucketRow, rowId, row;
	
	for ( i in bucketRows ) {
		bucketRow = bucketRows[i];
		rowId = bucketRow.bucketRowId;
		row = rowMap[rowId];

		if ( !row ) {
			row = new IDB.ReportTableRow(gc, bucketRow);
			rowMap[rowId] = row;
		}
		
		rows.push(row);
	}

	this._rows = rows;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGrid.prototype.getRowCount = function() {
	return this._rows.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGrid.prototype.getRow = function(index) {
	return this._rows[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGrid.prototype.getColumnCount = function() {
	return this._rows[0].getCellCount();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGrid.prototype.useCellWordWrap = function() {
	return this._useWrap;
};

/*====================================================================================================*/
IDB.ReportTableGridView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();
	this._rowIdMap = {};

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	this.rebuild();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype.rebuild = function() {
	//Build any rows which haven't been created yet, otherwise keep the existing one. Row order can
	//change due to sorting. Rows can become hidden/visible due to pivot selections.

	var model = this._model;
	var rows = this._rows;
	var hold = this._hold;
	var rowCount = model.getRowCount();
	var colCount = model.getColumnCount();
	var idMap = this._rowIdMap;
	var currIdMap = {};
	var i, rowModel, rowId, row;

	for ( i in rows ) {
		rows[i].getDisplay().remove();
	}

	rows = [];

	for ( i = 0 ; i < rowCount ; ++i ) {
		rowModel = model.getRow(i);
		rowId = rowModel.getRowId();
		row = idMap[rowId];

		if ( !row ) {
			row = new IDB.ReportTableRowView(colCount);
			row.setModel(rowModel);
			idMap[rowId] = row;
		}

		row.hide();
		hold.add(row.getDisplay());

		rows[i] = row;
		currIdMap[rowId] = true;
	}

	this._rows = rows;
	this._updateRowMaps();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype._updateRowMaps = function() {
	//A cell's height can change due to column resizing. This changes its row height, which also
	//changes the y-position of all subsequent rows.

	var rows = this._rows;
	var rowCount = rows.length;
	var heightMap = {};
	var posMap = {};
	var py = 0;
	var i, row, rowId, rowH;

	for ( i = 0 ; i < rowCount ; ++i ) {
		row = rows[i];
		row.updateLayout();

		rowId = row.getRowId();
		rowH = row.getHeight();

		heightMap[rowId] = rowH;
		posMap[rowId] = py;
		py += rowH;
	}
	
	this._fullH = py;
	this._rowHeightMap = heightMap;
	this._rowPosMap = posMap;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype._updateRowLayouts = function() {
	var rows = this._rows;
	var rowCount = rows.length;

	for ( var i = 0 ; i < rowCount ; ++i ) {
		rows[i].updateLayout();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype._updateLayout = function() {
	var rows = this._rows;
	var hold = this._hold;
	var rowsLen = rows.length;
	var heightMap = this._rowHeightMap;
	var posMap = this._rowPosMap;
	var topY = -hold.y();
	var botY = topY+this._height;
	var i, row, rowId, rowY;

	for ( i = 0 ; i < rowsLen ; ++i ) {
		row = rows[i];
		rowId = row.getRowId();
		rowY = posMap[rowId];

		if ( rowY >= botY ) {
			row.hide();
			continue;
		}

		if ( rowY+heightMap[rowId] <= topY ) {
			row.hide();
			continue;
		}

		row.getDisplay().y(rowY);
		row.show();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype.getRowFullHeight = function() {
	return this._fullH;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype.getScrollPos = function() {
	return -this._hold.y();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype.getDatapointHighlightPos = function() {
	var rows = this._rows;
	var rowLen = rows.length;
	var posMap = this._rowPosMap;
	var heightMap = this._rowHeightMap;
	var height = this._height;
	var topY = -this._hold.y();
	var botY = topY+height;
	var i, row, rowId, rowY, rowH;

	for ( i = 0 ; i < rowLen ; ++i ) {
		row = rows[i];
		
		if ( !row.hasHighlightedCell() ) {
			continue;
		}
		
		rowId = row.getRowId();
		rowY = posMap[rowId];
		rowH = heightMap[rowId];
		
		if ( rowY < topY ) {
			return rowY;
		}

		if ( rowY+rowH > botY ) {
			return rowY-height+rowH;
		}

		return this.getScrollPos();
	}

	return null;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype.setHeight = function(height, isCellWidthChanged, isWordWrapColumn) {
	this._height = height;

	if ( isCellWidthChanged ) {
		if ( isWordWrapColumn ) {
			this._updateRowMaps(); //because row/cell heights can change
		}
		else {
			this._updateRowLayouts();
		}
	}

	this._updateLayout();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype.setScrollPos = function(pos) {
	this._hold.y(Math.round(-pos));
	this._updateLayout();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableGridView.prototype.updateHorizVis = function(pos, availW) {
	var model = this._model;
	var rows = this._rows;
	var rowLen = rows.length;
	var rowModel = model.getRow(0);
	var colLen = rowModel.getCellCount();
	var colShow = [];
	var px = 0;
	var i, cellW;

	for ( i = 0 ; i < colLen ; ++i ) {
		cellW = rowModel.getCellWidth(i);
		colShow[i] = (pos+px+cellW > 0 && pos+px < availW);
		px += cellW;
	}

	for ( i = 0 ; i < rowLen ; ++i ) {
		rows[i].setCellVis(colShow);
	}
};

/*====================================================================================================*/
IDB.ReportTableHeaderCell = function(graphContext, columnIndex, isHeader, axis, value, rangeList,
                                                align, swatchShape, summaryType, isWrapCol) {
    this._graphContext = graphContext;
    this._columnIndex = columnIndex;
    this._isHeader = isHeader;
    this._axis = axis;
    this._value = value;
    this._rangeList = rangeList;
    this._align = align;
    this._swatchShape = swatchShape;
    this._summaryType = summaryType;
    this._isWrapCol = isWrapCol;
    this._isHeaderWrapCol = false;

    this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype._build = function() {
    var gc = this._graphContext, reportData = gc.reportData;
    var sett = gc.settings;
    var axis = this._axis;
    var st = this._summaryType;

    this._isHeaderWrapCol = this._isHeader && reportData.hasHeaderWordWrap(axis.axisId);

    this._vAlign = this._isHeader ? reportData.getHeaderVerticalAlignment(axis.axisId) : -1;

    this._sortDir = 0;
    this._sortChangeListeners = [];
    this._bgColor = (axis.axisId == 0 ? sett.get('HeaderXAxisColor') : axis.axisColor);
    this._swatchNeeded = !this._isHeader && (!this.showRangeColorBg() && axis.dataTypeIsNumber &&
        (sett.getBool('CellShowRangeSwatches') || this._swatchShape != 0));
    this._swatchColor = null;

    var showColor = (!this._isHeader && this._swatchNeeded && st &&
        st != IDB.ReportData.SummaryCount && st != IDB.ReportData.SummarySum);

    if ( showColor ) {
        var range = IDB.RangeHelper.getRangeForValue(
            this._value, this._rangeList, Number.POSITIVE_INFINITY, true);

        this._swatchColor = (range ? range.color : IDB.rgbx('#000'));
    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.setSortDir = function(sortDir) {
    this._sortDir = sortDir;

    if ( this._sortRedrawListener ) {
        this._sortRedrawListener();
    }
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.onSortClick = function() {
    this.setSortDir(this._sortDir == 0 ? -1 : this._sortDir*-1);

    if ( this._sortChangeListener ) {
        this._sortChangeListener(this._columnIndex);
    }
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.setSortChangeListener = function(callback) {
    this._sortChangeListener = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.setSortRedrawListener = function(callback) {
    this._sortRedrawListener = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.refreshDisplay = function() {
    this._graphContext.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getColumnIndex = function() {
    return this._columnIndex;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.isHeader = function() {
    return this._isHeader;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getAxis = function() {
    return this._axis;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getText = function() {
    return this._value.formattedValue;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getBgColor = function() {
    return this._bgColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getAlign = function() {
    return this._align;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getVAlign = function() {
    return this._vAlign;
};


/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getSwatchShape = function() {
    return this._swatchShape;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.isSwatchNeeded = function() {
    return this._swatchNeeded;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getSwatchColor = function() {
    return this._swatchColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getSortDir = function() {
    return this._sortDir;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.isWordWrapColumn = function() {
    return this._isWrapCol;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.isWordWrapHeader = function() {
    return this._isHeaderWrapCol;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getLabelOptions = function() {
    return this._graphContext.settings.get('HeaderLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.showGridHoriz = function() {
    return this._graphContext.settings.getBool('GridHorizShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.showGridVert = function() {
    return this._graphContext.settings.getBool('GridVertShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getGridColor = function() {
    return this._graphContext.settings.get('GridHeaderColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.getCellLabelSize = function() {
    return this._graphContext.settings.get('CellLabelOptions').size;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.showShading = function() {
    return this._graphContext.settings.getBool('CellHeaderShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCell.prototype.showRangeColorBg = function() {
    return this._graphContext.settings.getBool('CellShowRangeColorBg');
};

/*====================================================================================================*/
IDB.ReportTableHeaderCellView = function(model) {
    this._model = model;
    model.setSortRedrawListener(IDB.listener(this, '_updateSortIcon'));
    this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCellView.prototype._buildKinetic = function() {
    var model = this._model;
    var lblOpt = model.getLabelOptions();
    var bgColor = model.getBgColor();
    var height = this._minHeight = lblOpt.size+8;
    var sortIconH = height/2.5;

    this._explicitHeight = -1;

    this._wordWrap = model.isWordWrapHeader();
    this._vAlign = model.getVAlign();

    this._display = new Kinetic.Group();

    this._bg = new Kinetic.Rect({
        fill: bgColor.css,
        height: height
    });
    this._display.add(this._bg);

    if ( model.isSwatchNeeded() ) {
        var swatchW = Math.round((model.getCellLabelSize()+4)*0.65);
        var swatchH = Math.min(swatchW, height-4);
        var swatchColor = model.getSwatchColor();

        this._swatch = new Kinetic.Rect({
            y: Math.round((height-swatchH)/2)+0.5,
            width: swatchW,
            height: swatchH
        });
        
        if ( swatchColor ) {
            this._swatch.fill(swatchColor.css);
        }
        else {
            this._swatch
                .stroke(bgColor.x.css)
                .strokeWidth(1)
                .opacity(0.25);
        }
        
        this._display.add(this._swatch);
    }

    lblOpt.align = model.getAlign();

    this._label = new IDB.GraphLabel(lblOpt, model.getText());
    this._label.wordWrap = this._wordWrap;
    this._label.y = (height-this._label.textHeight)/2+1;
    this._display.add(this._label.getDisplay());

    if ( model.isHeader() ) {
        this._sortIcon = new Kinetic.Line({
            closed: true,
            fill: lblOpt.color.css,
            y: height/2,
            points: [
                0, -sortIconH*0.6,
                sortIconH*0.5, sortIconH*0.4,
                -sortIconH*0.5, sortIconH*0.4
            ]
        });
        this._display.add(this._sortIcon);
        
        this._display.on("click", IDB.leftClickListener(this, '_onClick'));
        this._touchHandler = new IDB.TouchHandler(this._display);
    }

    if ( model.showShading() ) {
        this._shade = new IDB.GenericShading(IDB.GenericShading.TypeGloss, true);
        this._shade.getDisplay().scaleY(height/IDB.GenericShading.FullDimension);
        this._display.add(this._shade.getDisplay());
    }

    this._borderHold = IDB.ReportTableCellView.buildBorder(
        model.showGridHoriz(), model.showGridVert(), model.getGridColor(), 1000);
        
    if ( this._borderHold ) {
        this._display.add(this._borderHold);
    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCellView.prototype.getDisplay = function() {
    return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCellView.prototype.getHeight = function() {
    return this._bg.height();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCellView.prototype.getMeasuredHeight = function() {
    if(!this._wordWrap) {
        return this._minHeight;
    }
    return Math.max(this._minHeight,  this._label.height + 8);
};

IDB.ReportTableHeaderCellView.prototype.setExplicitHeight = function(h) {
    var me = this, lbl = me._label, bg = me._bg, shade = me._shade,
        shadeScaleY = h/IDB.GenericShading.FullDimension, border =  me._borderHold;
    me._explicitHeight = h;
    bg.height(h);
    if(shade) {
        shade.getDisplay().scaleY(shadeScaleY);
    }
    if(border) {
        border.scaleY(h/1000);
    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCellView.prototype.setWidth = function(width) {
    var pad = IDB.ReportTableCellView.Gap;
    var lbl = this._label, model = this._model;
    var height = this._bg.height();

    this._bg.width(width);
    
    if ( lbl ) {
        lbl.x = pad;
        lbl.width = width-pad*2;
    }

    if ( this._swatch ) {
        var swatchW = this._swatch.width();
        this._swatch.x(Math.round(width-swatchW-pad)-0.5);
        lbl.width = lbl.width-swatchW-pad;
    }

    if ( this._sortIcon ) {
        this._sortIcon.x(width-this._minHeight/2);
    }

    if ( this._shade ) {
        this._shade.getDisplay().scaleX(width/IDB.GenericShading.FullDimension);
    }

    if ( this._borderHold ) {
        this._borderHold
            .scaleX((width)/1000)
            .scaleY((height)/1000);
    }

    var vAlign = this._vAlign;
    if(model.isHeader() && 0 != vAlign && model._graphContext.reportData.hasWrapEnabledHeaderColumn()) {
        
        var pad = 4;//Math.ceil(vLabel.textHeight * 0.2);

        if(vAlign == 1) {
            lbl.y = height - pad - lbl.height;
        }
        else {
            lbl.y = pad;
        }

    }
    else {
        // Summary, and old header behavior.
        lbl.y = (height-lbl.height)/2;
    }


    this._labelFullW = this._label.width;
    this._updateSortIcon();
};



////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCellView.prototype._onClick = function() {
    this._model.onSortClick();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableHeaderCellView.prototype._updateSortIcon = function() {
    if ( !this._sortIcon ) {
        return;
    }

    var model = this._model;
    var dir = model.getSortDir();
    var show = (dir != 0);
    var lw = this._labelFullW-(show ? this._minHeight : 0);

    this._sortIcon
        .visible(show)
        .scaleY(dir)
        .y(this._bg.height()/2);

    if ( this._swatch ) {
        this._swatch.visible(!show);
    }

    this._label.width = Math.max(0, lw);

    if(!this._wordWrap) {
        this._label.truncate();
    }

    model.refreshDisplay();
};

/*====================================================================================================*/
IDB.ReportTableRow = function(graphContext, bucketRow) {
	this._graphContext = graphContext;
	this._bucketRow = bucketRow;

	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRow.prototype._build = function() {
	var gc = this._graphContext;
	var bucketRow = this._bucketRow;
	var bucketRowLen = bucketRow.length;
	var i, cell;

	this._cells = [];
	
	for ( i = 0 ; i < bucketRowLen ; ++i ) {
		cell = null;

		if ( bucketRow[i] ) {
			cell = new IDB.ReportTableCell(gc, bucketRow[i]);
		}

		this._cells.push(cell);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRow.prototype.getRowId = function() {
	return this._bucketRow.bucketRowId;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRow.prototype.getCellCount = function() {
	return this._cells.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRow.prototype.getCell = function(index) {
	return this._cells[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRow.prototype.getCellWidth = function(index) {
	var rd = this._graphContext.reportData;
	var colIndex = rd.getColumnIndexByDisplayOrder(index);
	return rd.getColumnWidth(colIndex);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRow.prototype.hasHighlightedCell = function() {
	var cells = this._cells;
	var cellsLen = cells.length;
	var i, cell;
	
	for ( i = 0 ; i < cellsLen ; ++i ) {
		cell = cells[i];

		if ( cell && cell.getVisualState() == IDB.VS.HIGHLIGHTED ) {
			return true;
		}
	}

	return false;
};

/*====================================================================================================*/
IDB.ReportTableRowView = function(columnCount) {
	this._columnCount = columnCount;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();
	this._show = true;
	this._needsUpdateLayout = false;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.getRowId = function() {
	return this._model.getRowId();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.getHeight = function() {
	return this._rowHeight;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.hasHighlightedCell = function() {
	return this._model.hasHighlightedCell();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.setModel = function(model) {
	this._model = model;

	this._display.destroyChildren();
	this._cells = [];

	var cellCount = this._columnCount;
	var i, cell, cellModel;

	for ( i = 0 ; i < cellCount ; ++i ) {
		cellModel = model.getCell(i);
		
		if ( !cellModel ) {
			continue;
		}

		cell = new IDB.ReportTableCellView(cellModel);
		this._display.add(cell.getDisplay());
		this._cells[i] = cell;
	}

	this.updateLayout();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.show = function() {
	this._show = true;
	this._display.visible(true);

	if ( this._needsUpdateLayout ) {
		this.updateLayout();
		this._needsUpdateLayout = false;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.hide = function() {
	this._show = false;
	this._display.visible(false);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.setCellVis = function(cellVisList) {
	var cells = this._cells;
	var cellsLen = cells.length;
	var i, cell;

	for ( i = 0 ; i < cellsLen ; ++i ) {
		cell = cells[i];

		if ( cell ) {
			cell.getDisplay().visible(cellVisList[i]);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableRowView.prototype.updateLayout = function() {
	if ( !this._show ) {
		this._needsUpdateLayout = true;
		return; //to improve performance, delay the update of currently-hidden rows
	}

	var model = this._model;
	var cells = this._cells;
	var cellsLen = cells.length;
	var px = 0;
	var maxH = 0;
	var i, cell, cellW;

	for ( i = 0 ; i < cellsLen ; ++i ) {
		cellW = model.getCellWidth(i);
		cell = cells[i];

		if ( cell ) {
			cell.setWidth(cellW);
			cell.getDisplay().x(px);
			maxH = Math.max(maxH, cell.getHeight());
		}

		px += cellW;
	}

	if ( maxH == this._rowHeight ) {
		return; //skip height updates
	}
	
	for ( i = 0 ; i < cellsLen ; ++i ) {
		cell = cells[i];
		
		if ( cell ) {
			cell.setHeight(maxH);
		}
	}

	this._rowHeight = maxH;
};
/*global IDB, Kinetic, $ */
// "use strict";
/*====================================================================================================*/
IDB.ReportTableView = function(model) {
	this._model = model;
	model.onRowRefresh(IDB.listener(this, '_onRowRefresh'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._buildKinetic = function() {
	var model = this._model;
	var headerCellCount = model.getHeaderCellCount();
	var me = this;
	var i, cell, edge;

	var onMoveFunc = IDB.listener(this, '_onMouseMove');
	var onUpFunc = me._mouseUpListener = IDB.listener(this, '_onMouseUp');

    me.$window = $(window);

	var getEdgeFunc = function(type, index) {
		return function(event) {
			return me._onHeaderEdgeEvent(event, type, index);
		};
	};

	this._display = new Kinetic.Group();
	this._headerCells = [];
	this._headerEdges = [];
	
	for ( i = 0 ; i < headerCellCount ; ++i ) {
		cell = new IDB.ReportTableHeaderCellView(model.getHeaderCell(i));
		this._display.add(cell.getDisplay());
		this._headerCells[i] = cell;
	}

	this._headerH = cell.getHeight();

	for ( i = 0 ; i < headerCellCount ; ++i ) {
		edge = new Kinetic.Rect({
			width: 6,
			height: this._headerH,
			fill: 'rgba(0, 0, 0, 0.0001)'
		});
		
		edge.on('mouseover', getEdgeFunc('over', i));
		edge.on('mouseout', getEdgeFunc('out', i));
		edge.on('mousedown', getEdgeFunc('down', i));
		edge.on('mousemove', onMoveFunc);
		edge.on('mouseup', onUpFunc);
		edge._touchHandler = new IDB.TouchHandler(edge);

		this._display.add(edge);
		this._headerEdges[i] = edge;
	}

	this._grid = new IDB.ReportTableGridView(model.getGrid());
	this._display.add(this._grid.getDisplay());

	if ( model.showFooter() ) {
		this._footerCells = [];

		for ( i = 0 ; i < headerCellCount ; ++i ) {
			cell = new IDB.ReportTableHeaderCellView(model.getFooterCell(i));
			this._display.add(cell.getDisplay());
			this._footerCells[i] = cell;
		}
	}

	this._grid.getDisplay().moveToBottom();

	if ( model.showBorder() ) {
		this._border = new Kinetic.Rect({
			x: 0.5,
			y: 0.5,
			stroke: model.getBorderColor().css,
			strokeWidth: 1,
			listening: false
		});
		this._display.add(this._border);
	}

	if ( model.showControls() ) {
		this._btnExpAll = this._buildExpandButton(true);
		this._btnExpAll.on('click', IDB.leftClickListener(this, '_onExpandAllClick'));
		this._display.add(this._btnExpAll);

		this._btnColAll = this._buildExpandButton(false);
		this._btnColAll.x(Math.round(this._btnExpAll.btnWidth+5));
		this._btnColAll.on('click', IDB.leftClickListener(this, '_onCollapseAllClick'));
		this._display.add(this._btnColAll);
	}

	this._dragCover = new Kinetic.Rect({
		fill: 'rgba(0, 0, 0, 0.0001)',
		visible: false
	});
	this._dragCover.on('mousemove', onMoveFunc);
	this._dragCover.on('mouseup', onUpFunc);
	this._dragCover._touchHandler = new IDB.TouchHandler(this._dragCover);
	this._display.add(this._dragCover);

	this._dragIcon = this._buildDragIcon();
	this._dragIcon
		.visible(false)
		.listening(false);
	this._display.add(this._dragIcon);

	this._dragIndex = null;

};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._onRowRefresh = function() {
	this._grid.rebuild();
	this._updateLayout(false);
	this._model.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._buildExpandButton = function(expand) {
	var model = this._model;
	var lblOpt = model.getConrolsLabelOptions();
	var langKey = (expand ? "ReportExpandAll" : "ReportCollapseAll");
	var height = lblOpt.size+10;

	var hold = new Kinetic.Group();

	var box = new Kinetic.Rect({
		fill: model.getControlsBgColor().css,
		height: height
	});
	hold.add(box);

	var icon = IDB.ReportTableView.buildPlusIcon(!expand)
		.x(Math.floor((height-8)/2))
		.y(Math.floor((height-8)/2));
	hold.add(icon);

	var lbl = new IDB.GraphLabel(lblOpt, IDB.GraphLang.get(langKey));
	lbl.x = height;
	lbl.y = (height-lbl.textHeight)/2+1;
	hold.add(lbl.getDisplay());

	box.width(lbl.textWidth+lbl.x+6);

	hold.btnWidth = box.width();
	hold.btnHeight = height;
	return hold;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._buildDragIcon = function() {
	var hold = new Kinetic.Group();
	
	var center = new Kinetic.Rect({
		x: -1.25,
		y: -7,
		width: 2.5,
		height: 14,
		fill: '#000'
	});
	hold.add(center);

	var arrow = new Kinetic.Line({
		points: [
			-7, 0,
			-4, -3,
			-4, 3
		],
		closed: true,
		fill: '#000'
	});
	hold.add(arrow);
	
	arrow = new Kinetic.Line({
		points: [
			7, 0,
			4, -3,
			4, 3
		],
		closed: true,
		fill: '#000'
	});
	hold.add(arrow);

	return hold;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.buildPlusIcon = function(isMinus) {
	var hold = new Kinetic.Group();

	var box = new Kinetic.Rect({
		x: 0.5,
		y: 0.5,
		width: 8,
		height: 8,
		fill: '#fff',
		stroke: '#000',
		strokeWidth: 1
	});
	hold.add(box);

	var line = new Kinetic.Line({
		points: [
			2, 4.5,
			7, 4.5
		],
		stroke: '#000',
		strokeWidth: 1
	});
	hold.add(line);

	if ( !isMinus ) {
		line = new Kinetic.Line({
			points: [
				4.5, 2,
				4.5, 7
			],
			stroke: '#000',
			strokeWidth: 1
		});
		hold.add(line);
	}

	return hold;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._onExpandAllClick = function() {
	this._model.doExpandAll(true);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._onCollapseAllClick = function() {
	this._model.doExpandAll(false);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._onHeaderEdgeEvent = function(event, type, index) {
	if ( type == 'down' ) {
		this._dragIndex = index;
		this._dragCover.visible(true);
        this.$window.off("mouseup", this._mouseUpListener);
        this.$window.on("mouseup", this._mouseUpListener);
	}

	this._dragIcon.visible(this._dragIndex !== null || type == 'over');
	this._onMouseMove(event);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._onMouseMove = function(event) {
	event = (event.evt ? event.evt : event); //supports either Kinetic version

	var d = this._display;
	var model = this._model;
	var dragX = event.offsetX-d.x();
	var i = this._dragIndex;

	this._dragIcon
		.x(dragX)
		.y(Math.max(this._headerY+7, Math.min(this._footerBotY - 7,  event.offsetY-d.y())));

	if ( i !== null ) {
		var cellX = this._headerCells[i].getDisplay().x();
		var cellW = Math.max(dragX-cellX, this._headerH);
		model.setCellWidth(i, cellW);
		this._updateLayout(true, model.getHeaderCell(i).isWordWrapColumn());
	}

	model.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._onMouseUp = function() {
    this.$window.off("mouseup", this._mouseUpListener);
	this._dragIndex = null;
	this._dragIcon.visible(false);
	this._dragCover.visible(false);
	this._model.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.getScrollPos = function() {
	return this._grid.getScrollPos();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.getScrollWidth = function() {
	var model = this._model;
	var headerCellCount = this._headerCells.length;
	var px = 0;

	for ( var i = 0 ; i < headerCellCount ; ++i ) {
		px += model.getCellWidth(i);
	}

	return px+1;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.getScrollHeight = function() {
	return this._grid.getRowFullHeight();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.getScrollMaskHeight = function(testH) {
	return testH-this._getNonRowsHeight();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.getUsedHeight = function(maskH) {
	return Math.min(maskH, this._grid.getRowFullHeight()+this._getNonRowsHeight());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.getTableTop = function() {
	return (this._btnExpAll ? this._btnExpAll.btnHeight+5 : 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._getNonRowsHeight = function() {
	return this._headerH*(this._footerCells ? 2 : 1)+this.getTableTop();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.getDatapointHighlightPos = function() {
	return this._grid.getDatapointHighlightPos();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.setSize = function(width, height) {
	this._width = width;
	this._height = height;
	this._updateLayout(false, false);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype._updateLayout = function(isColumnDrag, isWordWrapColumn) {
	var model = this._model;
	var width = this._width;
	var height = this._height;
	var grid = this._grid;
	var headerCells = this._headerCells;
	var headerEdges = this._headerEdges;
	var footerCells = this._footerCells;
	var headerCellCount = headerCells.length;
	var footerCellCount = (footerCells ? footerCells.length : 0);
	var px = 0;
	var py = 0;
	var i, cell, cellW, edge, hasWrapEnabledHeaderColumn = model._hasWrapEnabledHeaderColumn;

	grid.setHeight(height-this._getNonRowsHeight(), isColumnDrag, isWordWrapColumn);
	grid.updateHorizVis(-grid.getDisplay().x(), width);

	if ( this._btnExpAll ) {
		py += Math.round(this._btnExpAll.btnHeight+5);
	}

	var borderY = py;
    var headerHeight = this._headerH;

    this._headerY = py;

	for ( i = 0 ; i < headerCellCount ; ++i ) {
		cellW = model.getCellWidth(i);

		cell = headerCells[i];  // ReportTableHeaderCellView
		cell.getDisplay().x(px).y(py);
		cell.setWidth(cellW);
        if(hasWrapEnabledHeaderColumn) {
            headerHeight = Math.max(headerHeight, cell.getMeasuredHeight());
        }
        
		px += cellW;

		edge = headerEdges[i];
		edge.x(px-3).y(py);
	}

    // If wrapping, do a second pass
	for (i=0; hasWrapEnabledHeaderColumn && i<headerCellCount; ++i) {
        cellW = model.getCellWidth(i);
		cell = headerCells[i];
        cell.setExplicitHeight(headerHeight);
        cell.setWidth(cellW);
        edge = headerEdges[i];
        edge.height(headerHeight);
    }

	var fullW = px+1;
	py += headerHeight;

	grid.getDisplay()
		.y(py)
		.clip({
			x: 0,
			y: 0,
			width: fullW,
			height: height-this._getNonRowsHeight()
		});

	px = 0;
	py += grid.getRowFullHeight();

	var footerBotY = this._footerBotY = Math.min(py+(footerCellCount ? this._headerH : 0), height);

	for ( i = 0 ; i < footerCellCount ; ++i ) {
		cellW = model.getCellWidth(i);

		cell = footerCells[i];
		cell.getDisplay().x(px).y(footerBotY-this._headerH);
		cell.setWidth(cellW);
		px += cellW;
	}

	py += this._headerH;
	this._fullHeight = py;

	if ( this._border ) {
		this._border
			.x(0.5)
			.y(borderY+0.5)
			.width(fullW-1)
			.height(footerBotY-borderY);
	}

	this._dragCover
		.x(-1000)
		.y(-100)
		.width(width+2000)
		.height(height+1000);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.setScrollPos = function(pos) {
	this._grid.setScrollPos(pos);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ReportTableView.prototype.setHorizScrollPos = function(pos) {
	this._display.x(-pos);
	this._grid.updateHorizVis(-pos, this._width);
};
/*global IDB, Kinetic */

