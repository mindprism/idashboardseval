'use strict';
IDB.StoplightBar = function(graphContext, yAxisCol) {
	this._graphContext = graphContext;
	this._yAxisCol = yAxisCol;
	
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var yAxis = dpGrid.getYAxis(this._yAxisCol, true);
	var rlist = dpGrid.getRangeList(this._yAxisCol, true);
	
	this._currValue = null;
	this._reset = false;
	this._datapoint = null;
	this._xRowIndex = 0;
	
	if ( sett.get('StoplightAxisLabelOptions').show ) {
		this._label = new IDB.StoplightBarLabel(sett, yAxis);
	}
	
	this._body = new IDB.StoplightBarBody(sett, yAxis, rlist.length);
	this._range = new IDB.StoplightBarRange(sett, rlist);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.resetBar = function() {
	this._currValue = {
		asNumber: 0,
		asText: ''
	};

	this._reset = true;
	this._datapoint = null;
	this._updateValue();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.setXRowIndex = function(index) {
	var dp = this._graphContext.datapointGrid.getDatapointAt(index, this._yAxisCol, true);
	var dv = dp.yValue;

	this._xRowIndex = index;
	this._datapoint = dp;
	this._reset = dv.isNothing;

	this._currValue = {
		asNumber: (this._reset ? 0 : dv.numberValue),
		asText: (this._reset ? '' : dv.formattedValue)
	};
	
	this._updateValue();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype._updateValue = function() {
	var cv = this._currValue;
	var res = this._reset;

	this._range.setValue(cv, res);
	
	if ( this._label ) {
		this._label.setValue(cv, res);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.getLabel = function() {
	return this._label;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.getBody = function() {
	return this._body;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.getRange = function() {
	return this._range;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.getAxisLabelRelativeWidth = function() {
	return this._graphContext.settings.get('StoplightAxisLabelWidth')/100;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, '_handleClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.createOverListener = function() {
	return IDB.listener(this, '_handleOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype.createOutListener = function() {
	return IDB.listener(this, '_handleOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype._handleClick = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointClicked(this._datapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype._handleOver = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointOver(this._datapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBar.prototype._handleOut = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointOut(this._datapoint);
	}
};

/*====================================================================================================*/
IDB.StoplightBarBody = function(settings, yAxis, numRanges) {
	this._settings = settings;
	this._yAxis = yAxis;
	this._numRanges = numRanges;
};

IDB.StoplightBarBody.StyleFlat = 0;
IDB.StoplightBarBody.StyleChisled = 1;
IDB.StoplightBarBody.StyleClock = 2;
IDB.StoplightBarBody.StyleRound = 3;
IDB.StoplightBarBody.StyleEmbed = 4;
		
IDB.StoplightBarBody.FinishMatte = 0;
IDB.StoplightBarBody.FinishChrome = 1;

IDB.StoplightBarBody.FaceFlat = 0;
IDB.StoplightBarBody.FaceSegment = 1;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getRangeCount = function() {
	return this._numRanges;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getColor = function() {
	var sett = this._settings;

	return (this.getStyle() == IDB.StoplightBarBody.StyleEmbed ? 
		sett.get('BackgroundColor') : sett.get('StoplightBodyColor'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getFaceColor = function() {
	return this._settings.get('StoplightFaceColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getFaceAlpha = function() {
	return this._settings.getAlpha('StoplightFaceTrans');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getFaceStyle = function() {
	return this._settings.get('StoplightFaceStyle');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getPadding = function() {
	return this._settings.get('StoplightBodyPad')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getStyle = function() {
	return this._settings.get('StoplightBodyShade');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getFinish = function() {
	return this._settings.get('StoplightBodyFinish');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBody.prototype.getRelativeLightSize = function() {
	return this._settings.get('StoplightRangeLightSize')/100;
};

/*====================================================================================================*/
IDB.StoplightBarBodyView = function(model) {
	this._model = model;
	this._buildKinetic();
};

//NOTE: this class contains some code copied directly from LinearBarBodyView


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype._buildKinetic = function() {
	var model = this._model;
	var hasShading = (model.getStyle() != IDB.StoplightBarBody.StyleFlat);

	this._display = new Kinetic.Group();

	//LATER: draw "side cover" graphics

	this._bodyRect = new Kinetic.Rect({
		fill: model.getColor().css
	});
	this._display.add(this._bodyRect);
		
	if ( hasShading ) {
		this._bodyShade = new Kinetic.Group();
		this._display.add(this._bodyShade);
	}

	this._face = new Kinetic.Rect({
		fill: IDB.toRGBA(model.getFaceColor(), model.getFaceAlpha())
	});
	this._display.add(this._face);

	if ( model.getFaceStyle() == IDB.StoplightBarBody.FaceSegment ) {
		this._segments = [];

		var segCount = model.getRangeCount()-1;
		var i, segHi, segLo;

		for ( i = 0 ; i < segCount ; ++i ) {
			segHi = new Kinetic.Rect({
				fill: 'rgba(255, 255, 255, 0.25)',
				width: 1.5
			});
			this._display.add(segHi);

			segLo = new Kinetic.Rect({
				fill: 'rgba(0, 0, 0, 0.25)',
				width: 1.5
			});
			this._display.add(segLo);

			this._segments[i] = [segHi, segLo];
		}
	}
	
	//LATER: this.blendMode = BlendMode.LAYER;
	//LATER: this.cacheAsBitmap = true;
};
	

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype.getInnerBounds = function() {
	return this._bounds;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype.setSize = function(width, height) {
	var model = this._model;
	var pad = Math.round(height*model.getPadding()/2);
	var b = {
		x: pad,
		y: pad,
		width: width-pad*2,
		height: height-pad*2
	};

	this._bounds = b;

	this._bodyRect
		.x(0)
		.y(0)
		.width(width)
		.height(height);
	
	if ( this._bodyShade ) {
		var shadeR = {
			x: 0,
			y: 0,
			width: width,
			height: height
		};

		this._drawShading(this._bodyShade, shadeR, b);
	}

	this._face
		.x(b.x)
		.y(b.y)
		.width(b.width)
		.height(b.height);

	this._setSegmentSize();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype._setSegmentSize = function() {
	var segs = this._segments;

	if ( !segs ) {
		return;
	}

	var model = this._model;
	var b = this._bounds;
	var count = segs.length;
	var numRanges = count+1;
	var pad = Math.min(b.width/numRanges, b.height)*(1-model.getRelativeLightSize());
	var lightW = (b.width-pad)/numRanges;
	var i, px;

	for ( i = 0 ; i < count ; ++i ) {
		px = pad/2 + lightW*(i+1) + b.x;

		segs[i][0]
			.x(px-1)
			.y(b.y)
			.height(b.height);
		
		segs[i][1]
			.x(px+0.5)
			.y(b.y)
			.height(b.height);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype._drawShading = function(hold, r, r2) {
	hold.destroyChildren();
	
	var model = this._model;
	var wh = '255, 255, 255';
	var gr = '127, 127, 127';
	var bk = '0, 0, 0';
	var isChrome = (model.getFinish() == IDB.StoplightBarBody.FinishChrome);
	var halfR;

	var stops = [
		0.0, 'rgba('+wh+', 0.4)',
		0.5, 'rgba('+gr+', 0.1)',
		1.0, 'rgba('+bk+', 0.4)'
	];

	switch ( model.getStyle() ) {
		case IDB.StoplightBarBody.StyleChisled:
			halfR = this._getMidRect(r, r2, 0.5);
			this._drawGrad(hold, r, halfR, stops);
			this._drawGrad(hold, halfR, null, stops, true);
			
			if ( isChrome ) {
				this._drawChrome(hold, r, halfR, false, 0.5);
				this._drawChrome(hold, halfR, null, true, 0.5);
			}
			break;
		
		case IDB.StoplightBarBody.StyleClock:
			halfR = this._getMidRect(r, r2, 0.8);
			this._drawGrad(hold, r, halfR, stops);
			this._drawGrad(hold, halfR, null, stops, true);
			
			if ( isChrome ) {
				this._drawChrome(hold, r, halfR, false, 0.5);
				this._drawChrome(hold, halfR, null, true, 0.5);
			}
			break;
		
		case IDB.StoplightBarBody.StyleRound:
			this._drawGrad(hold, r, null, stops);
			
			if ( isChrome ) {
				this._drawChrome(hold, r, null, false, 0.5);
			}
			break;
		
		case IDB.StoplightBarBody.StyleEmbed:
			stops[1] ='rgba('+bk+', 0.3)';
			stops[3] ='rgba('+gr+', 0.15)';
			stops[5] ='rgba('+wh+', 0.3)';

			this._drawGrad(hold, r, null, stops);
			
			if ( isChrome ) {
				this._drawChrome(hold, r, null, false, 0.35);
			}
			break;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype._getMidRect = function(r, r2, mid) {
	return {
		x: r.x+(r2.x-r.x)*mid,
		y: r.y+(r2.y-r.y)*mid,
		width: r.width+(r2.width-r.width)*mid,
		height: r.height+(r2.height-r.height)*mid
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype._getBodyPoints = function(r, r2) {
	var pts = [
		r.x, r.y,
		r.x+r.width, r.y,
		r.x+r.width, r.y+r.height,
		r.x, r.y+r.height
	];
	
	if ( r2 != null ) {
		pts.push(
			r.x, r.y,
			r2.x, r2.y,
			r2.x, r2.y+r2.height,
			r2.x+r2.width, r2.y+r2.height,
			r2.x+r2.width, r2.y,
			r2.x, r2.y
		);
	}

	return pts;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype._drawGrad = function(hold, r, r2, stops, reverse) {
	reverse = IDB.ifUndef(reverse, false);

	var grad = new Kinetic.Line({
		points: this._getBodyPoints(r, r2),
		closed: true,
		fillLinearGradientStartPointY: (reverse ? r.y+r.height : r.y),
		fillLinearGradientEndPointY: (reverse ? r.y : r.y+r.height),
		fillLinearGradientColorStops: stops
	});
	hold.add(grad);
	return grad;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarBodyView.prototype._drawChrome = function(hold, r, r2, reverse, alpha) {
	var stops = [
		0.0, 'rgba(255, 255, 255, 1.0)',
		0.31, 'rgba(0, 0, 0, 0.4)',
		0.63, 'rgba(255, 255, 255, 0.7)',
		0.88, 'rgba(0, 0, 0, 0.5)',
		1.0, 'rgba(255, 255, 255, 0.2)'
	];

	var grad = this._drawGrad(hold, r, r2, stops, reverse);
	grad.opacity(alpha);
};

/*====================================================================================================*/
IDB.StoplightBarLabel = function(settings, yAxis) {
	this._settings = settings;
	this._yAxis = yAxis;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLabel.prototype.setValue = function(value) {
	this._valueText = value.asText;

	if ( this._onValueChange ) {
		this._onValueChange();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLabel.prototype.setOnValueChange = function(callback) {
	this._onValueChange = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLabel.prototype.getAxisName = function() {
	return this._yAxis.axisName;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLabel.prototype.getValueText = function() {
	return this._valueText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLabel.prototype.getAxisLocation = function() {
	return (this._settings.getBool('StoplightAxisLabelPerp') ? 
		IDB.LinearBarLabel.AxisPerp : IDB.LinearBarLabel.AxisPara);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLabel.prototype.getAxisLabelOptions = function() {
	return this._settings.get('StoplightAxisLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLabel.prototype.getValueLabelOptions = function() {
	return this._settings.get('StoplightValueLabelOptions');
};

/*====================================================================================================*/
IDB.StoplightBarLight = function(settings, color) {
	this._settings = settings;
	this._color = color;
};

IDB.StoplightBarLight.ShapeCircle = 0;
IDB.StoplightBarLight.ShapeEllipse = 1;
IDB.StoplightBarLight.ShapeSquare = 2;
IDB.StoplightBarLight.ShapeRectangle = 3;
		
IDB.StoplightBarLight.ShadeFlat = 0;
IDB.StoplightBarLight.ShadeMatte = 1;
IDB.StoplightBarLight.ShadeGloss = 2;
IDB.StoplightBarLight.ShadeChrome = 3;
		
IDB.StoplightBarLight.AnimNone = 0;
IDB.StoplightBarLight.AnimBlink = 1;
IDB.StoplightBarLight.AnimSoftPulse = 2;
IDB.StoplightBarLight.AnimHardPulse = 3;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.turnOn = function(isOn) {
	this._isOn = isOn;

	if ( this._onStateChange ) {
		this._onStateChange();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.setOnStateChange = function(callback) {
	this._onStateChange = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.isOn = function() {
	return this._isOn;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.getColor = function() {
	return this._color;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.isVertMode = function() {
	return this._settings.getBool('StoplightVertMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.getShape = function() {
	return this._settings.get('StoplightRangeLightShape');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.getShading = function() {
	return this._settings.get('StoplightRangeLightShade');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.getRelativeSize = function() {
	return this._settings.get('StoplightRangeLightSize')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.hasCover = function() {
	return this._settings.getBool('StoplightRangeLightCover');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLight.prototype.hasGlow = function() {
	return this._settings.getBool('StoplightRangeLightGlow');
};

/*====================================================================================================*/
IDB.StoplightBarLightView = function(model) {
	this._model = model;
	model.setOnStateChange(IDB.listener(this, '_onStateChange'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLightView.prototype._buildKinetic = function() {
	var model = this._model;
	var shape = model.getShape();
	var shading = model.getShading();
	var size = 1000;

	this._display = new Kinetic.Group();
	this._size = size;
	this._squareAspect = (shape == IDB.StoplightBarLight.ShapeCircle || 
		shape == IDB.StoplightBarLight.ShapeSquare);

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);
	
	if ( shape == IDB.StoplightBarLight.ShapeCircle || 
			shape == IDB.StoplightBarLight.ShapeEllipse ) {
		this._mask = new Kinetic.Ellipse();
		this._round = true;
	}
	else {
		this._mask = new Kinetic.Rect();
		this._round = false;
	}

	this._display.add(this._mask);
	IDB.KineticUtil.setMask(this._hold, this._mask);

	this._bg = new Kinetic.Rect({
		x: -size/2,
		y: -size/2,
		width: size,
		height: size,
		fill: '#000'
	});
	this._hold.add(this._bg);

	this._light = new Kinetic.Rect({
		x: -size/2,
		y: -size/2,
		width: size,
		height: size,
		fill: model.getColor().css
	});
	this._hold.add(this._light);

	if ( shading != IDB.StoplightBarLight.ShadeFlat ) {
		this._shade = new Kinetic.Group();
		this._hold.add(this._shade);
		
		var gs = new IDB.GenericShading(shading, !this._round);
		this._shade.add(gs.getDisplay());

		if ( this._round ) {
			gs.getDisplay()
				.rotation(model.isVertMode() ? 90 : 0);
		}
		else {
			gs.getDisplay()
				.x(-IDB.GenericShading.FullDimension/2)
				.y(-IDB.GenericShading.FullDimension/2);
		}

		this._shadow = (this._round ? new Kinetic.Ellipse() : new Kinetic.Rect());
		this._shadow
			.fill('#0ff')
			.shadowEnabled(true)
			.shadowBlur(8)
			.shadowOpacity(0.4)
			.shadowOffset({ x: 1, y: 6 });
		this._display.add(this._shadow);
	}
	
	//LATER: draw cover
	/*if ( model.hasCover() ) {
		this._cover = new IDB.StoplightBarRangeCoverView(model);
		this.addChild(this._cover);
	}*/
	
	if ( model.hasGlow() ) {
		this._glow = (this._round ? new Kinetic.Ellipse() : new Kinetic.Rect());
		this._glow
			.fill('#000')
			.offsetY(size)
			.shadowEnabled(true)
			.shadowColor(this._light.fill())
			.shadowOpacity(0.75);
		this._display.add(this._glow);
	}

	this._hold.moveToTop();
	this._onStateChange();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLightView.prototype._onStateChange = function() {
	var isOn = this._model.isOn();

	this._light.opacity(isOn ? 1 : 0.15);
	
	if ( this._glow ) {
		this._glow.visible(isOn);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLightView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarLightView.prototype.setSize = function(width, height) {
	var model = this._model;
	var size = this._size;
	var relSize = model.getRelativeSize();
	var cs = 1.1;
	
	if ( this._cover ) {
		width /= cs;
		height /= cs;
	}
	
	var holdSx = width/size;
	var holdSy = height/size;
	var maskW, maskH;

	this._hold
		.scaleX(holdSx)
		.scaleY(holdSy);
	
	if ( this._squareAspect ) {
		maskW = maskH = Math.min(width, height)*relSize;
	}
	else {
		maskW = width*relSize;
		maskH = height*relSize;
	}
	
	var maskX = (this._round ? 0 : -maskW/2);
	var maskY = (this._round ? 0 : -maskH/2);

	this._mask
		.x(maskX)
		.y(maskY)
		.width(maskW)
		.height(maskH);
	
	if ( this._cover ) {
		this._cover.setSize(maskW*cs, maskH*cs);
	}
	
	if ( this._shade ) {
		this._shade
			.scaleX(maskW/holdSx/size)
			.scaleY(maskH/holdSy/size);

		this._shadow
			.x(maskX)
			.y(maskY)
			.width(maskW)
			.height(maskH);
	}
	
	if ( this._glow ) {
		var f = Math.min(holdSx, holdSy)*15;
		var isVert = model.isVertMode();

		this._glow
			.x(maskX)
			.y(maskY)
			.width(maskW)
			.height(maskH)
			.shadowBlur(f*8);

		this._glow.shadowOffset({
			x: (isVert ? size : 0),
			y: (isVert ? 0 : size)
		});
	}
};

/*====================================================================================================*/
IDB.StoplightBarRange = function(settings, rangeList) {
	this._settings = settings;
	this._rangeList = rangeList;
	
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarRange.prototype._build = function() {
	this._lights = [];

	var sett = this._settings;
	var rangeList = this._rangeList;
	var count = rangeList.length;
	var i, light;

	for ( i = 0 ; i < count ; ++i ) {
		light = new IDB.StoplightBarLight(sett, rangeList[i].color);
		this._lights[i] = light;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarRange.prototype.setValue = function(value, reset) {
	var lights = this._lights;
	var rangeList = this._rangeList;
	var count = rangeList.length;
	var num = value.asNumber;
	var i, r0, r1;

	for ( i = 0 ; i < count ; ++i ) {
		r0 = rangeList[i];
		r1 = rangeList[i+1];
		lights[i].turnOn(!reset && num >= r0.val && (!r1 || num < r1.val));
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarRange.prototype.getLightCount = function() {
	return this._lights.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarRange.prototype.getLight = function(index) {
	return this._lights[index];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarRange.prototype.getRelativeSize = function() {
	return this._settings.get('StoplightRangeLightSize')/100;
};

/*====================================================================================================*/
IDB.StoplightBarRangeView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarRangeView.prototype._buildKinetic = function() {
	var model = this._model;
	var count = model.getLightCount();
	var i, light;

	this._display = new Kinetic.Group();
	this._lights = [];

	for ( i = 0 ; i < count ; ++i ) {
		light = new IDB.StoplightBarLightView(model.getLight(i));
		this._display.add(light.getDisplay());
		this._lights[i] = light;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarRangeView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarRangeView.prototype.setSize = function(width, height) {
	var model = this._model;
	var lights = this._lights;
	var count = lights.length;
	var i, light;

	var pad = Math.min(width/count, height)*(1-model.getRelativeSize());
	var lightW = (width-pad)/count;
	var lightY = Math.round(height/2);
	
	for ( i = 0 ; i < count ; ++i ) {
		light = lights[i];
		light.setSize(lightW, height-pad);
		light.getDisplay()
			.x(Math.round(pad/2 + lightW*(i+0.5)))
			.y(lightY);
	}

	this._display
		.clip({
			x: 0,
			y: 0,
			width: width,
			height: height
		});
};

/*====================================================================================================*/
IDB.StoplightBarView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());
	this._display._touchHandler = new IDB.TouchHandler(this._display);

	if ( model.getLabel() ) {
		this._label = new IDB.LinearBarLabelView(model.getLabel()); //reusing Linear code
		this._display.add(this._label.getDisplay());
	}
	
	this._body = new IDB.StoplightBarBodyView(model.getBody());
	this._display.add(this._body.getDisplay());
	
	this._range = new IDB.StoplightBarRangeView(model.getRange());
	this._display.add(this._range.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightBarView.prototype.setSize = function(width, height) {
	var model = this._model;
	var lblW = 0;
	var bod = this._body;
	var bodDisp = bod.getDisplay();

	if ( this._label ) {
		lblW = width*model.getAxisLabelRelativeWidth();
		this._label.setSize(lblW-1, height, height/2);
	}

	bod.setSize(width-lblW, height);
	bodDisp.x(lblW);

	var rect = bod.getInnerBounds();

	this._range.setSize(rect.width, rect.height);
	this._range.getDisplay()
		.x(bodDisp.x()+rect.x)
		.y(rect.y);
};

/*====================================================================================================*/
IDB.StoplightGraph = function(chart, $div, graphProperties, dataSet) {
    var mt = graphProperties.get('DatapointMatchType');
    if(mt !== IDB.MT.NONE) {
        graphProperties.set('DatapointMatchType', IDB.MT.X_VALUE);
    }
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	this._info.calloutStyle = 'rangeColors';
	this._info.legendStyle = IDB.LS.RANGES;
    this._info.xMatchOnly = true;
};

IDB.StoplightGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.StoplightGraph.prototype.constructor = IDB.StoplightGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightGraph.prototype._build = function() {
	var gc = this._graphContext;
	this._updateCounts();

	this._lights = new IDB.StoplightSet(gc);
	this._lightsView = new IDB.StoplightSetView(this._lights);
	this._stageLayer.add(this._lightsView.getDisplay());
	
	this._odoms = new IDB.GraphOdomSet(gc, this._odomStart, this._odomCount);
	this._odomsView = new IDB.GraphOdomSetView(this._odoms);
	this._stageLayer.add(this._odomsView.getDisplay());
	
	var showFunc = IDB.listener(this, '_showRow');
	var persist = gc.settings.getBool('StoplightRowPersist');
	var sr = new IDB.GraphSingleRow(gc, showFunc, persist);

	this._srView = new IDB.GraphSingleRowView(sr);
	this._stageLayer.add(this._srView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightGraph.prototype._updateCounts = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var yCount = dpGrid.getNumYCols(true);
	var maxN = yCount;
	var nMult = 1;
	var legI = 0;
	
	////
	
	var n = sett.get('StoplightBarCount');
	var sn = sett.get('StoplightBarShowCount');

	if ( n == 0 || n > maxN ) {
		n = maxN;
	}

	if ( sn == 0 || sn > n ) {
		sn = n;
	}

	sett.set('StoplightBarCount', n);
	sett.set('StoplightBarShowCount', sn);

	this._odomStart = n*nMult;
	this._odomCount = Math.max(0, yCount-this._odomStart);

	////

	this._legendAxes = [ dpGrid.getXAxis() ];

	for ( var i = legI ; i < this._odomStart ; i += nMult ) {
		this._legendAxes.push(dpGrid.getYAxis(i, true));
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightGraph.prototype._showRow = function(datapointRow) {
	if ( datapointRow == null ) {
		this._lights.resetBars();
		this._odoms.resetOdoms();
		return;
	}

	this._lights.setXRowIndex(datapointRow.rowId);
	this._odoms.setXRowIndex(datapointRow.rowId);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightGraph.prototype._update = function(width, height) {
	this._srView.setSize(width, height);
	var rect = this._srView.getContentRect();
	this._lightsView.setSize(rect.width, rect.height);

	var pad = 5;
	var barH = rect.height;
	var odomW = this._odomsView.getMinWidth(barH);
	var barW = rect.width-(odomW ? odomW+pad : 0);
	
	this._lightsView.setSize(barW, barH);
	this._lightsView.getDisplay().y(rect.y);

	this._odomsView.setSize(odomW, barH);
	this._odomsView.getDisplay()
		.x(rect.width-odomW)
		.y(rect.y)
		.visible(this._odoms.getOdomCount() > 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightGraph.prototype.getLegendAxes = function() {
	return this._legendAxes;
};

/*====================================================================================================*/
IDB.StoplightSet = function(graphContext) {
	this._graphContext = graphContext;

	var sett = graphContext.settings;

	this._currXRow = 0;
	this._reset = true;
	this._pages = new IDB.GraphPages(graphContext, IDB.listener(this, '_buildBar'), 
		sett.get('StoplightBarCount'), sett.get('StoplightBarShowCount'));
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSet.prototype._buildBar = function(index) {
	var gc = this._graphContext;
	var bar = new IDB.StoplightBar(gc, index);
	
	if ( this._reset ) {
		bar.resetBar();
	}
	else {
		bar.setXRowIndex(this._currXRow);
	}

	var i = (this._pages ? this._pages.getCurrentIndex() : 0);
	this._graphContext.onYAxisSelected(i);
	return bar;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSet.prototype.setXRowIndex = function(index) {
	this._currXRow = index;
	this._reset = false;
	
	var pages = this._pages;
	var itemCount = this._pages.getItemCount();
	var i, bar;
	
	for ( i = 0 ; i < itemCount ; ++i ) {
		bar = pages.getItem(i);

		if ( bar ) {
			bar.setXRowIndex(index);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSet.prototype.resetBars = function() {
	this._reset = true;
	
	var pages = this._pages;
	var itemCount = this._pages.getItemCount();
	var i, bar;
	
	for ( i = 0 ; i < itemCount ; ++i ) {
		bar = pages.getItem(i);

		if ( bar ) {
			bar.resetBar();
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSet.prototype.getGraphPages = function() {
	return this._pages;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSet.prototype.isVertMode = function() {
	return this._graphContext.settings.getBool('StoplightVertMode');
};

/*====================================================================================================*/
IDB.StoplightSetView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSetView.prototype._buildKinetic = function() {
	var model = this._model;
	var buildFunc = IDB.listener(this, '_buildBar');
	var layoutFunc = IDB.listener(this, '_updateLayout');
	
	this._display = new Kinetic.Group();

	this._pagesView = new IDB.GraphPagesView(model.getGraphPages());
	this._pagesView.build(buildFunc, layoutFunc);
	this._display.add(this._pagesView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSetView.prototype._buildBar = function(index, barModel) {
	return new IDB.StoplightBarView(barModel);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSetView.prototype._updateLayout = function() {
	var model = this._model;
	var pv = this._pagesView;
	var pages = model.getGraphPages();
	var availW = pv.getAvailableWidth();
	var availH = pv.getAvailableHeight();
	var itemHold = pv.getItemDisplay();
	
	if ( model.isVertMode() ) {
		itemHold.rotation(-90).y(availH);

		var temp = availW;
		availW = availH;
		availH = temp;
	}

	var useW = availW;
	var useH = availH;
	var btnH = pv.getButtonsHeight();
	var currI = pages.getCurrentIndex();
	var count = pages.getShowCount();
	var maxI = currI+count;
			
	if ( model.isVertMode() ) {
		useW -= btnH;
	}
	else {
		useH -= btnH;
	}
			
	var barPad = Math.min(5, useH/(count*10));
	var barH = (useH-(count-1)*barPad)/count;

	if ( barH/useW > 0.35 ) {
		barH = useW*0.35;
	}

	var pushY = Math.max(0, (useH-count*(barH+barPad))/2);
	var i, barView;
	
	for ( i = currI ; i < maxI ; ++i ) {
		barView = pv.getItemView(i);
		barView.setSize(useW, barH);
		barView.getDisplay()
			.x(Math.round(availW-useW))
			.y(Math.round((i-currI)*(barH+barPad)+pushY));
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSetView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.StoplightSetView.prototype.setSize = function(width, height) {
	this._pagesView.setSize(width, height);
};
/*====================================================================================================*/

