'use strict';
IDB.PlacardGraph = function(chart, $div, graphProperties, dataSet) {
    var me = this, labelOptions, padding;
    graphProperties.set('ButtonRowShow', !graphProperties.get('HideButtonRow'));
    graphProperties.set('ButtonRowPos', 0);
    labelOptions = graphProperties.get('GallXLabelOptions');
    labelOptions.align = graphProperties.get('GallXLabelTextAlign');
    labelOptions.show = me._showLabel = graphProperties.get('GallXLabelVisible');
    labelOptions.clickable = true;
    graphProperties.set('XValueLabelOptions', labelOptions);
    graphProperties.set('XLabelShadow', graphProperties.get('GallXLabelShadow'));
    graphProperties.set('XValueLabelPos', (graphProperties.get('GallXLabelStyle') == "below")?0:1);
    graphProperties.set('XLabelFunction', IDB.listener(me, "_labelFunction"));
    me._labelPos = graphProperties.get('GallXLabelStyle');
    me._htmlTemplate = graphProperties.get("PlHtmlTemplate") || "";
    me._delay = graphProperties.get("SSSlideIntervalSeconds")*1000;
    me._slideshowEnabled = graphProperties.get("SSSlideEnabled");
    me._maxWidth = graphProperties.get("SSMaxFrameWidth");
    me._maxHeight = graphProperties.get("SSMaxFrameHeight");
    me._vAlign = graphProperties.get("SSFrameVAlign");
    me._autoPlay = graphProperties.get("SSSlideAutoplay");
    me._frameBorderThickness = graphProperties.get("GallFrameBorderVisible")?graphProperties.get("PlFrameBorderThickness"):0;
    padding = me._padding = {};
    padding.left = graphProperties.get("PlPaddingLeft");
    padding.right = graphProperties.get("PlPaddingRight");
    padding.top = graphProperties.get("PlPaddingTop");
    padding.bottom = graphProperties.get("PlPaddingBottom");

    if(graphProperties.get("PlFrameBorderStyle") != "solid") {
        me._frameBorderThickness = 2; 
    }

    me._drilldownFromFrameClick = graphProperties.get("PlDDFromTextClick");

    me._framePosition = {my:"center", at:"center"};
    switch(me._vAlign) {
        case "middle":
            me._framePosition.my = "center";
            me._framePosition.at = "center";
            break;
        case "top":
            me._framePosition.my = "top";
            me._framePosition.at = "top";
            break;
        case "bottom":
            me._framePosition.my = "bottom";
            me._framePosition.at = "bottom";
    }

    me._currentRow = null;

    IDB.GraphBaseKinetic.call(me, chart, $div, graphProperties, dataSet, null);
};

IDB.PlacardGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.PlacardGraph.prototype.constructor = IDB.PlacardGraph;

//"Placard":{
//    "PlHtmlTemplate":null,
//    "SSMaxFrameWidth":0,
//    "SSMaxFrameHeight":0,
//    "SSFrameVAlign":"middle",
//    "PLPreventScrollbars":false, *
//    "PlSelectable":false, *
//    "PlDDFromTextClick":false, *
//    "PlPaddingLeft":2, *
//    "PlPaddingTop":2, *
//    "PlPaddingBottom":2, *
//    "PlPaddingRight":2, *
//    "PLAdvText":false, *
//    "PLRTLText":false, *
//    "PlFrameBackgroundColor":{"r":255,"g":255,"b":255,"css":"#ffffff","i":16777215,"lum":255.0,"x":{"r":25,"g":25,"b":25,"css":"#191919","i":1644825}}, *
//    "PlFrameTransparency":0, *
//    "GallFrameBorderVisible":false,
//    "PlFrameBorderStyle":"solid", *
//    "PlFrameBorderThickness":1, *
//    "GallFrameBorderColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0,"lum":0.0,"x":{"r":229,"g":229,"b":229,"css":"#e5e5e5","i":15066597}},
//    "SSSlideEnabled":true,
//    "SSSlideStepButtons":true,
//    "SSSlidePlayPause":true,
//    "SSSlideButtonHeight":60,
//    "SSSlideIntervalSeconds":10,
//    "SSSlideAutoplay":false,
//    "StickySelect":true,
//    "AutoscrollSpeed":10,
//    "HideButtonRow":false,
//    "XLabelTemplate":null,
//    "GallXLabelVisible":true,
//    "GallXLabelStyle":"below",
//    "GallXLabelOptions":{"show":false,"size":25,"color":{"r":0,"g":0,"b":0,"css":"#000000","i":0,"lum":0.0,"x":{"r":229,"g":229,"b":229,"css":"#e5e5e5","i":15066597}},"bold":false},
//    "GallXLabelSize":25, 
//    "GallXLabelColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0,"lum":0.0,"x":{"r":229,"g":229,"b":229,"css":"#e5e5e5","i":15066597}},
//    "GallXLabelTextAlign":"center",
//    "GallXLabelShadow":true
//  }

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PlacardGraph.prototype._build = function() {
    var me = this, I = IDB, gc = me._graphContext, gs = gc.settings, showFunc, url, gp = me.gp,
    showStepButtons, showPlayPauseButton, $frameDiv, frameBorderColor, frameBorderStyle, frameTransparency,
    frameBackgroundColor, rgba, padding;

    me._info.xMatchOnly = true;
    me._info.numericYOnly = false;
    me._info.displaysMultipleXValues = false;
    me._info.calloutStyle = 'none';
    me._info.legendStyle = IDB.LS.NONE;

    me._$contentRectDiv = $("<div class=\"idb-placard-rect\">").appendTo(me._$div).css({"position":"absolute","z-index":-100});

    //
    // Configure the frame.
    //
    $frameDiv = me._$frameDiv = $("<div class=\"idb-placard-frame\">").appendTo(me._$div).css({"position":"absolute"});

    if(me._frameBorderThickness > 0) {
        frameBorderStyle = gp("PlFrameBorderStyle");
        frameBorderColor = gp("GallFrameBorderColor");
        $frameDiv.css({"border-style":frameBorderStyle, "border-color":frameBorderColor.css, "border-width":me._frameBorderThickness+"px"});
    }

    frameTransparency = (100-gp("PlFrameTransparency"))/100;
    frameBackgroundColor = gp("PlFrameBackgroundColor"); 
    rgba = IDB.toRGBA(frameBackgroundColor.i, frameTransparency);
    $frameDiv.css({"background-color":rgba});


    if(gp("PLPreventScrollbars")) {
        $frameDiv.css({"overflow-x":"hidden"});
        $frameDiv.css({"overflow-y":"hidden"});
    }
    else {
        //
        // This should be auto, but it is being set to hidden to replicate the Flash behavior.
        // 
        $frameDiv.css({"overflow-x":"hidden"});
        $frameDiv.css({"overflow-y":"auto"});
    }

    padding = me._padding;
    $frameDiv.css({"padding-left":padding.left, "padding-right":padding.right, "padding-top":padding.top, "padding-bottom":padding.bottom}); 

     if(gp("PLAdvText") && gp("PLRTLText")) {
         $frameDiv.css({"direction":"rtl", "unicode-bidi":"bidi-override"});
     }

    me._framePosition.of = me._$contentRectDiv;

    //
    // Configure the slideshow.
    //
    showStepButtons = gp("SSSlideStepButtons");
    showPlayPauseButton = gp("SSSlidePlayPause");
    me._$buttonBar = $("<div>").appendTo(me._$frameDiv).slideshowButtonBar({
                                            "forwardCallback":IDB.listener(me, "_stepForward"),
                                            "backCallback":IDB.listener(me, "_stepBackward"),
                                            "playCallback":IDB.listener(me, "_startSlideshow"),
                                            "pauseCallback":IDB.listener(me, "_stopSlideshow"),
                                            "showStepButtons":showStepButtons,
                                            "showPlayPauseButton":showPlayPauseButton,
                                            "buttonHeight":gp("SSSlideButtonHeight"),
                                            "visible":false});

    me._stickySelect = gp('StickySelect') || (me._slideshowEnabled && (me._autoPlay || showStepButtons || showPlayPauseButton)); 
    showFunc = IDB.listener(me, '_showRow');
    me._sr = new IDB.GraphSingleRow(gc, showFunc, me._stickySelect);

    me._srView = new IDB.GraphSingleRowView(me._sr);
    me._stageLayer.add(me._srView.getDisplay());

    me._$frameDiv.on("mouseover", IDB.listener(me, "_onFrameMouseOver"));
    me._$frameDiv.on("mouseout", IDB.listener(me, "_onFrameMouseOut"));
    me._$frameDiv.on("click", IDB.listener(me, "_onFrameClick"));
};

IDB.PlacardGraph.prototype._stepForward = function() {
    this._srView.stepForward();
};

IDB.PlacardGraph.prototype._stepBackward = function() {
    this._srView.stepBackward();
};

IDB.PlacardGraph.prototype._onTimer = function() {
    var me = this;
    if(me._$buttonBar.slideshowButtonBar("mouseIsOverStepButton")) {
        return;
    }
    else {
        me._stepForward();
        if(me._currentRow) {
            //
            // This should select a matching data row in other single row graph canvases, without
            // any highlight/fade flickering on the other graph types.
            //

            var dp = me._currentRow.datapoints[0];
            me.datapointMouseOver(dp);
            me.datapointMouseOut(dp);
        }
    }
};

IDB.PlacardGraph.prototype._startSlideshow = function() {
    var me = this;
    me._stopSlideshow();
    me._timer = window.setInterval(IDB.listener(me, "_onTimer"), me._delay);
};

IDB.PlacardGraph.prototype._stopSlideshow = function() {
      var me = this;
      if(!IDB.isNil(me._timer)) {
          window.clearInterval(me._timer);
          me._timer = null;
      }
};


IDB.PlacardGraph.prototype._onFrameClick = function(evt) {
    var $target = $(evt.target);
    if($target.hasClass('idb-slideshow-btn')) {
        return;
    }

    if(!this._drilldownFromFrameClick) {
        return;
    }
    else {
        if(!$target.is("a")) {
            evt.preventDefault();
        }
        var dp = this._currentRow.datapoints[0];
        this.datapointClicked(dp);
    }
};

IDB.PlacardGraph.prototype._onFrameMouseOver = function(evt) {
     var me = this, index;
     me._$buttonBar.slideshowButtonBar("option", "visible", me._slideshowEnabled); 
     index = me._sr.getSelectedIndex();
     if(index >= 0) {
         me._sr.onItemMouseOver(index);
     }
     
     me._chart.handCursor(me._drilldownFromFrameClick);
};

IDB.PlacardGraph.prototype._onFrameMouseOut = function(evt) {
     var me = this, index;
     me._$buttonBar.slideshowButtonBar("option", "visible", false); 
     index = me._sr.getSelectedIndex();
     if(index >= 0) {
         me._sr.onItemMouseOut(index);
     }
     else {
         if(!me._stickySelect) {
             if(me._$htmlDiv) {
                 me._$htmlDiv.remove();
                 me._$htmlDiv = null;
             }
         }
     }
};


/*----------------------------------------------------------------------------------------------------*/
IDB.PlacardGraph.prototype._update = function(width, height) {
    var me = this, $htmlDiv = me._$htmlDiv, rect, w, h, padding = me._padding, oh, position, possibleY,
        frameDiv;

    me._srView.setSize(width, height);
    rect = me._srView.getContentRect();
    w = rect.width;
    h = rect.height;
    me._$contentRectDiv.css({"top":rect.y+"px", "left":rect.x+"px", "height":h+"px", "width":w+"px"}); 
    w = Math.min(w, me._maxWidth) || w;
    h = Math.min(h, me._maxHeight) || h;


    w = w - 2*me._frameBorderThickness;
    h = h - 2*me._frameBorderThickness;

    w = w - padding.left - padding.right;
    h = h - padding.top - padding.bottom;

    me._$frameDiv.width(w);
    me._$frameDiv.height(h);
    me._$frameDiv.position(me._framePosition);

    if(me._showLabel) {
        position = me._$frameDiv.position();
        if(me._labelPos === "below" && me._vAlign !== "bottom") {
            oh = me._$frameDiv.outerHeight();
            possibleY = oh + position.top + 5;
            if(possibleY < rect.height) {
                me._srView.setLabelY(possibleY);
            }
        }
        else if(me._labelPos == "above" && me._vAlign != "top") {
            possibleY = position.top - 5;
            if(possibleY > rect.y) {
                me._srView.setLabelY(possibleY);
            }
        }
    }

    frameDiv = me._$frameDiv.get(0);
    me._$buttonBar.slideshowButtonBar("resize", frameDiv.clientWidth, frameDiv.clientHeight);

    if($htmlDiv) {
        $htmlDiv.css({"visibility":"visible"});
    }
};

IDB.PlacardGraph.prototype._showRow = function(datapointRow) {
    var me = this, I = IDB, html, $htmlDiv;
    if(datapointRow === null) {
        if(!me._stickySelect) {
            if(me._$htmlDiv) {
                me._$htmlDiv.remove();
                me._$htmlDiv = null;
            }
        }
        return;
    }
    me._currentRow = datapointRow;
    html = me.getHtml(datapointRow);
    $htmlDiv = $('<div class="idb-placard-html">').html(html);

    //
    // Remove any JavaScript before adding the div to the DOM.
    // 
    IDB.HtmlUtils.removeScript($htmlDiv);

    //
    // If the HTML is 'flashified' HTML it needs to be standardized for browser presentation.
    //
    if(IDB.HtmlUtils.isFlashHtml($htmlDiv)) {
        IDB.HtmlUtils.toStandardHtml($htmlDiv);
    }

    if(me._$htmlDiv) {
        me._$htmlDiv.replaceWith($htmlDiv);
    }
    else {
       $htmlDiv.appendTo(me._$frameDiv);
    }
    me._$htmlDiv = $htmlDiv;

    //
    // Start the slideshow if indicated.
    // 
    if(me._slideshowEnabled && me._autoPlay) {
     me._$buttonBar.slideshowButtonBar("startOrStopSlideshow"); 
        me._autoPlay = false;
    }

};

IDB.PlacardGraph.prototype.getHtml = function(datapointRow) {
   var me = this, I = IDB, config = I.config, gp = me.gp, html = me._htmlTemplate,
       axes = me._dataSet.axisList, axis, firstY, chart = me._chart;

       if(html === "") {
           firstY = null;
           for(var j=1; j<axes.length; j++) {
               axis = axes[j];
               if(!axis.isGraphAxis()) continue;
               if(!firstY) firstY = axis;
               if(axis.dataTypeIsString) {
                   html = "${value:" + axis.axisName + "}";
                   break;
                }
           }
        }
        if(html === "") {
            html = "${value:" + firstY.axisName + "}";
        }
     
        html.replace(/\r\n/g, '\n');

        return chart.expandMacros(html, 'html', datapointRow.datapoints[0]);
};


IDB.PlacardGraph.prototype._labelFunction  = function(dp) {
    var me = this, gp = me.gp, template = gp("XLabelTemplate"), chart = me._chart;

    if(template) {
        return chart.expandMacros(template, null, dp);
    }
    else {
        return dp.xValue.formattedValue;
    }
};

IDB.PlacardGraph.prototype.destroy = function() {
    IDB.GraphBaseKinetic.prototype.destroy.call(this);
    this._stopSlideshow();
};



/*====================================================================================================*/

