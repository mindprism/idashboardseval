'use strict';
IDB.GMDatapointGrid = function(graph) {
    var me = this, I = IDB, LatLon = I.LatLon, dpGrid = graph._graphContext.datapointGrid, ds = graph.getDataSet(),
        rows = dpGrid.getDatapointRows(), axisList = ds._axisList, j=0, jj=0,
        _geoMapConfig = graph._geoMapConfig, axis, nameLC, haveLatLonAxes, latAxisId, lonAxisId, latLonAxisId,  tokens,
        maxLatFound, minLatFound, maxLonFound, minLonFound, haveCenter = false, _geoMapPoints,
        dpr, vals, latV, lonV, lat, lon, latLon, pc, dprProps,
        ids = {
            _color:0,
            _colorIsNumber:false,
            _colorIsString:false,
            _shape:0,
            _size:0,
            _blink:0,
            _blinkFactor:0
        };

    IDB.GMDatapointGrid.initProps(me);
    Object.preventExtensions(me);
    me._graph = graph;
    _geoMapPoints = me._geoMapPoints;

    for(j=1, jj=axisList.length; j<jj; j++) {

        axis = axisList[j];
        nameLC = axis.axisName.toLowerCase();
        if(axis.dataTypeIsString || axis.dataTypeIsNumber) {
            if("_color" === nameLC) {
                ids._color = j;
                ids._colorIsNumber = axis.dataTypeIsNumber;
                ids._colorIsString = axis.dataTypeIsString;
            }
        }
        if(axis.dataTypeIsString) {
            if("_latlon" === nameLC) {
                me._latLonAxisId = latLonAxisId = axis.axisId;
            }
            else if("_shape" === nameLC) {
                ids._shape = j;
            }
        }
        else if(axis.dataTypeIsNumber) {
            if("_lat" === nameLC) {
                me._latAxisId = latAxisId = axis.axisId;
            }
            else if("_lon" === nameLC) {
                me._lonAxisId = lonAxisId = axis.axisId;
            }
            else if(nameLC === "_size") {
                ids._size = j;
            }
            else if(nameLC === "_blink") {
                ids._blink = j;
            }
            else if(nameLC === "_blinkfactor") {
                ids._blinkfactor = j;
            }
        }
    }

    me._haveLatLonAxes = haveLatLonAxes = (me._latLonAxisId > 0) || (me._latAxisId > 0 && me._lonAxisId > 0);


    maxLatFound = me._maxLatFound;
    minLatFound = me._minLatFound;
    maxLonFound = me._maxLonFound;
    minLonFound = me._minLonFound;

    for(j=0, jj=rows.length; j<jj; j++) {

//            var  dpr:GMDatapointRow = GMDatapointRow(datapointRow), vals:Array = dpr.datapoints[0].dataRow,
//                latV:DataValue, lonV:DataValue, lat:Number, lon:Number, latLon:LatLon, pc:GeoMapPointConfig;

        var dpr = rows[j];
        latLon = null, lat = NaN, lon = NaN, point = null;
        vals = dpr.dataRow;
        dprProps = dpr.map;

        if(haveLatLonAxes) {
            if(latLonAxisId > 0) {
                tokens = LatLon.splitGPS(vals[latLonAxisId].stringValue);
                lat = tokens[0];
                lon = tokens[1];
                latLon = new LatLon(lat, lon);
            }
            else {
                lonV = vals[lonAxisId];
                latV = vals[latAxisId];
                lat = latV.isNothing ? NaN : latV.numberValue;
                lon = lonV.isNothing ? NaN : lonV.numberValue;
                latLon = new LatLon(lat, lon);
            }
        }

        if((!latLon || !latLon._isValid) && _geoMapConfig) {
            pc = _geoMapConfig.getPointConfig(vals[0].stringValue);
            if(pc) {
                latLon = pc.latLon();
//                    trace("GMDatapointGrid.dpRowPostCreate(): " + vals[0].stringValue + ", " + pc +  ", " + latLon);
            }
        }

        if(!latLon || !latLon._isValid) {
            continue;
        }

        lat = latLon._lat;
        lon = latLon._lon;

        dprProps.lat = lat;
        dprProps.lon = lon;
        dprProps.latLon = latLon;

        var point = new IDB.GeoMapPoint(graph, ids, dpr);
        point.setDatapointRow(dpr);
        point._latLon = latLon;
//        point.buttonMode = graph.clickable;
        dprProps.mapPoint = point;
        _geoMapPoints.push(point);

        haveCenter = true;

        if(lat > maxLatFound) {
            maxLatFound = lat;
        }
        if(lat < minLatFound) {
            minLatFound = lat;
        }
        if(lon > maxLonFound) {
            maxLonFound = lon;
        }
        if(lon < minLonFound) {
            minLonFound = lon;
        }


    }

    me._haveCenter = haveCenter;
    me._minLatFound = minLatFound;
    me._maxLatFound = maxLatFound;
    me._minLonFound = minLonFound;
    me._maxLonFound = maxLonFound;

};

// ._graphContext (GraphContext)
// ._graphContext.datapointGrid (DatapointGrid)
// ._graphContext.datapointGrid.getDatapointRows() (Array/DatapointRow)
// ._graphContext.settings (GraphSettings)
// .getDataSet() (DataSet)
// .getDataSet().axisList (Array/Axis)

IDB.GMDatapointGrid.prototype.getGeoMapPoints = function() {
    return this._geoMapPoints.concat();
};

IDB.GMDatapointGrid.prototype.getCenter = function() {
    if(!this._haveCenter) return null;
    var me = this, I = IDB, LatLon = I.LatLon, centerLon =  me._minLonFound + (me._maxLonFound - me._minLonFound)/2,
        topYM = LatLon.latToMaps(me._maxLatFound), botYM = LatLon.latToMaps(me._minLatFound),
        centerYM = topYM + (botYM - topYM)/2, centerLat = LatLon.latFromMaps(centerYM);
    return new LatLon(centerLat, centerLon);
};

IDB.GMDatapointGrid.prototype.getTopLeft = function() {
    var me = this, LatLon = IDB.LatLon;
    if(!me._haveCenter) return null;
    return new LatLon(me._maxLatFound, me._minLonFound);
};

IDB.GMDatapointGrid.prototype.getBottomRight = function() {
    var me = this, LatLon = IDB.LatLon;
    if(!me._haveCenter) return null;
    return new LatLon(me._minLatFound, me._maxLonFound);
};


IDB.GMDatapointGrid.initProps = function(newInstance) {
    var LatLon = IDB.LatLon, props = {
      _graph:null,
      _geoMapConfig:null,
      _latLonAxisId:-1,
      _latAxisId:-1,
      _lonAxisId:-1,
      _haveLatLonAxes:false,
      _geoMapPoints:[],
      _minLatFound:LatLon.MAX_LAT + 10,
      _maxLatFound:LatLon.MIN_LAT - 10,
      _minLonFound:LatLon.MAX_LON + 10,
      _maxLonFound:LatLon.MIN_LON - 10,
      _haveCenter:false
    };
    $.extend(newInstance, props);
};

/*global IDB, $ */
IDB.GeoMapConfig = function($xml, configFileUrl, graph) {
    var me = this, I = IDB, GMIC = I.GeoMapImageConfig, j, jj, k, kk, $points = $xml.children("points").children("point"),
        $objectGroups = $xml.children("objects"), $oGroup,
        $items = $xml.children("legend").first().children("items").children("item"),
        valueMap = {}, liConfigs = [], pointConfig, liConfig, hasLegendIcon = false,
        gp = graph.gp, get, num, bool, col, viewer = graph._viewer, $objects, $object, zmin = 0, zmax = 54;

    me._valueMap = valueMap;

    me._geoMapObjects = [];
    me._geoMapImageConfigs = [];
    
    
    for(j=0, jj=$points.length; j<jj; j++) {
        pointConfig = new I.GeoMapPointConfig($($points[j]), valueMap);
    }

    for(j=0, jj=$objectGroups.length; j<jj; j++) {
        $oGroup = $($objectGroups[j]);
        get = function(name, defaultVal) {
            return $oGroup.attr(name) || defaultVal;
        };
        num = function(name, defaultVal) {
            var n = Number(get(name, NaN));
            if(isNaN(n)) {
               return defaultVal;
            }
            return n;
        };
        bool = function(name, defaultVal) {
            return IDB.toBool(get(name), defaultVal);
        };
        col = function(name, defaultVal) {
            return IDB.rgbx(get(name), defaultVal);
        };

        zmin = Math.max(0, num("zmin", 12));
        zmax = Math.min(54, num("zmax", 54));
        var labelDefaults = {};
        var markerDefaults = {};

        labelDefaults.hasLine = bool("label-line",  true);
        labelDefaults.labelShadow = bool("label-shadow",  gp("LabelShadow"));
        labelDefaults.labelSize = num("label-size",  gp("LabelSize"));
        labelDefaults.labelColor = col("label-color",  gp("LabelColor"));
        labelDefaults.labelAlign = get("label-align",  gp("LabelAlign"));
        //labelDefaults.labelRTL = XMLUtils.xmlListToBoolean(objectGroupXML.attribute("label-rtl"),  gp("LabelRightToLeftText"));
        labelDefaults.labelHOffset = num("label-ho",  0);
        labelDefaults.labelVOffset = num("label-vo",  0);

        markerDefaults.shape = get("marker-shape", "circle");
        markerDefaults.size = num("marker-size", 10);
        markerDefaults.shapeColor = col("marker-color", IDB.rgbx(0x000000));

        $objects = $oGroup.children("*");
        for(k=0, kk=$objects.length; k<kk; k++) {
            $object = $($objects[k]);
            var gmo = new IDB.GeoMapObject(viewer, graph);
            gmo.configureFromXml($object, zmin, zmax, labelDefaults, markerDefaults);                      //($xml, $defaultZMin, $defaultZMax, labelDefaults, markerDefaults) {
            if(gmo.isValid()) {
                 me._geoMapObjects.push(gmo);
            }
        }
    }

    $objectGroups = $xml.children("images");

    for(j=0, jj=$objectGroups.length; j<jj; j++) {
        $oGroup = $($objectGroups[j]);
        get = function(name, defaultVal) {
            return $oGroup.attr(name) || defaultVal;
        };
        num = function(name, defaultVal) {
            var n = Number(get(name, NaN));
            if(isNaN(n)) {
               return defaultVal;
            }
            return n;
        };
        bool = function(name, defaultVal) {
            return IDB.toBool(get(name), defaultVal);
        };
        col = function(name, defaultVal) {
            return IDB.rgbx(get(name), defaultVal);
        };

        zmin = Math.max(0, num("zmin", 12));
        zmax = Math.min(54, num("zmax", 54));
        var tile = bool("tile", false);
        var defUseProxy = bool("useProxy", false);
        var defW = num("image-width", NaN);
        var defH = num("image-height", NaN);
        var defHO = num("image-ho", 0);
        var defVO = num("image-vo", 0);
        var defUrl = get("image-url", null);

        $objects = $oGroup.children("image");
        for(k=0, kk=$objects.length; k<kk; k++) {
            $object = $($objects[k]);
            var overrideUrl = null;
            var imageConfig = new GMIC($object, overrideUrl, defUrl, zmin, zmax, defW, defH, defHO, defVO,
                tile, defUseProxy);
            if(imageConfig._isValid) {
                me._geoMapImageConfigs.push(imageConfig);
                imageConfig._graph = graph;
            }
        }
    }


    me.legendItemConfigs = liConfigs;
    for(j=0, jj=$items.length; j<jj; j++) {
        liConfigs.push(liConfig = new I.PlotLIConfig($($items[j])));
        hasLegendIcon = hasLegendIcon || liConfig.hasIcon;
    }

    me.hasLegendIcon = hasLegendIcon;


   Object.preventExtensions(me);
};

IDB.GeoMapConfig.prototype.getPointConfig = function(s) {
    if(!s) {
        return null;
    }
    return this._valueMap[$.trim(s).toLowerCase()];
};
/*global IDB, $ */

IDB.GeoMapGraph = function(chart, $div, graphProperties, dataSet) {
//    graphProperties._settings.UseRangeColors = false;

    IDB.GraphBase.call(this, chart, $div, graphProperties, dataSet, null);
    Object.preventExtensions(this);
    var me = this, info = me._info;
    info.calloutStyle = "none";
    $div.addClass("idb-noselect");
//	this._info.legendStyle = (this._graphContext.settings.getBool('BarFillRangeColors') ?
//		IDB.LS.RANGES : IDB.LS.AXES);

};

IDB.GeoMapGraph.prototype = Object.create(IDB.GraphBase.prototype);
IDB.GeoMapGraph.prototype.constructor = IDB.GeoMapGraph;


////////////////////////////////////////////////////////////////////////////
// static stuff

IDB.GeoMapGraph.OSM_COPYRIGHT_TEXT = "Map Data \u00A9 OpenStreetMap contributors";
IDB.GeoMapGraph.OSM_COPYRIGHT_LINK = "http://www.openstreetmap.org/copyright";

(function(GMG) {

    var shapeAxisNames = {
            _lat:true,
            _lon:true,
            _latlon:true,
            _shape:true,
            _color:true,
            _size:true,
            _blink:true,
            _blinkfactor:true
        };

    GMG.shapeAxisNames = shapeAxisNames;

    /**
     * Returns true if the given axis name matches (case-INsensitive) one of the following:
     * _x, _y, _xpct, _ypct, _lat, _lon, _shape, _color, _size.
     */
    GMG.isShapeAxisName = function(axisName) {
        if(!axisName) return false;
        return !!shapeAxisNames[axisName.toLowerCase()];
    };

})(IDB.GeoMapGraph);


////////////////////////////////////////////////////////////////////////////
// Overrides

IDB.GeoMapGraph.prototype._build = function() {
    var me = this, $div = me._$div, I = IDB, config = I.config, log = I.log, listener = I.listener, gp = me.gp,
        GMG = I.GeoMapGraph, configUrl = me.getConfigUrl(), j, jj,
        dataSet = me._dataSet, axes = dataSet.getGraphAxes(false), firstYAxis = axes[1],
        axis, rangeAxes = [], selectorAxes, s, $viewer, rad = "4px", $zp, btnOut = listener(me, "onBtnMouseOut"),  //btnOver = listener(me, "onBtnMouseOver"),
        tpl = gp("TileUrlTemplate"), useProxy = gp("TileUrlUseProxy"), _viewer, _grid,
        dtut = config.dtut || null, ctype = gp("CopyrightNoticeType"), mtc = config.mtc || null, mtcl = config.mtcl || null, ctext = null, clink = null;

    IDB.GeoMapGraph.initProps(this);  // _build() is called from within the GraphBase constructor.

    if(!tpl) {
        if(dtut) {
            tpl = dtut;
            ctext = mtc;
            clink = mtcl;
        }
        else {
            ctext = GMG.OSM_COPYRIGHT_TEXT;
            clink = GMG.OSM_COPYRIGHT_LINK;
        }
    }
    else {
    
        if(ctype == "default" || ctype == "osm") {
            ctext = GMG.OSM_COPYRIGHT_TEXT;
            clink = GMG.OSM_COPYRIGHT_LINK;
        }
        else if(ctype == "custom") {
            ctext = gp("CopyrightNoticeText");
            clink = gp("CopyrightNoticeLinkUrl");
        }
    }
    ctext = (ctext || "").replace(/\$\{\s*copyright\s*\}/ig, "\u00A9");
    clink = $.trim(clink);



    log("GeoMapGraph._build(): url=" + configUrl);

    me._configUrl = configUrl;
    me._pointLabelShadowColor = null;
    me._rangeLegendAxes = rangeAxes;
    me.rangeAxisIds = [];

    me._maxFineZoom = Math.max(0, Math.min(54,  gp("MaxZoom")));
    me._minFineZoom = Math.max(0, Math.min(me._maxFineZoom,  gp("MinZoom")));
    me._lockZoom = (me._minFineZoom === me._maxFineZoom) || gp("LockZoom");
    me._lockCenter = gp("LockInitialCenter");


    s = $.trim(gp("PointLabelTemplate"));
    me._suppressPointLabels = (s && s.match(/^\$\{\s*blank\s*\}$/i));

    for(j=1, jj=axes.length; j<jj; j++) {
        axis = axes[j];
        if(GMG.isShapeAxisName(axis.axisName)) {
            continue;
        }
        if(axis.dataTypeIsNumber) {
             me._firstNonShapeNumericYGraphAxis = axis;
            if( ! me._firstNonShapeYGraphAxis) {
                me._firstNonShapeYGraphAxis = axis;
            }
            break;
        }
        else {
            if( ! me._firstNonShapeYGraphAxis) {
                me._firstNonShapeYGraphAxis = axis;
            }
        }
    }

    if(me._firstNonShapeNumericYGraphAxis) {
        // A range legend is a possibility, so get the list of axes that will
        // be used in the legend.
        for(j=0, jj=axes.length; j<jj; j++) {
            axis = axes[j];
            if(j===0 || (!GMG.isShapeAxisName(axis.axisName) && axis.dataTypeIsNumber)) {
                rangeAxes.push(axis);
                if(j > 0) {
                    me.rangeAxisIds.push(axis.axisId);
                }
            }
        }
    }

    if(gp("UseRangeColors")) {
        if(me._firstNonShapeNumericYGraphAxis) {
            me._dataYGraphAxis = me._firstNonShapeNumericYGraphAxis;
            me._canUseRangeLegend = true;
            selectorAxes = rangeAxes.concat();
            selectorAxes.shift();
            me._axisSelector = new I.AxisSelector($('<div class="idb-axis-selector">').prependTo($div), me, selectorAxes);
        }
        else if(me._firstNonShapeYGraphAxis) {
            me._dataYGraphAxis = me._firstNonShapeYGraphAxis;
        }
        else {
            me._dataYGraphAxis = firstYAxis;
        }
    }
    else {
        if(me._firstNonShapeYGraphAxis) {
            me._dataYGraphAxis = me._firstNonShapeYGraphAxis;
        }
        else {
            me._dataYGraphAxis = firstYAxis;
        }
    }

    me._dataYGraphAxisId = me._dataYGraphAxis.axisId;

    if(configUrl) {
        me._configPending = true;
         $.ajax({
             url:configUrl,
             dataType:"xml",
             type:"GET",
             success:this.onConfigLoaded,
             context:this,
             error:this.onConfigFail
             });
    }

    me._$viewer = $viewer = $('<div class="idb-geomap-viewer">').prependTo($div);
    me._viewer = _viewer = new I.GeoMapViewer(me, $viewer);
    _grid = _viewer._grid;
    _grid._useProxy = useProxy && config.proxyEnabled;
    _grid.setUrlTemplate(tpl);

    

    me._$zin = $("<a>");
    me._$zout = $("<a>");
    $zp = me._$zpanel = $("<div class=\"idb-zpanel\"><p class=\"idb-zpanel-z\"></p><p class=\"idb-zpanel-c\"></p>");

    if(!me._lockZoom) {

        _viewer._clearanceW += 30;

        $viewer.on("click", listener(me, "_offButtonTouch"));

        me._$zin.idbPosAbs().idbHide().text("+").addClass("idb-zoom-btn").css(
        {"border-top-left-radius":rad, "border-top-right-radius":rad}
        ).appendTo($div).on("click", listener(me, "_onZoomIn")
        ).on("mousewheel", listener(me, "_onBtnMouseWheel")).on("mouseover", listener(me, "onBtnZoomInMouseOver")).on("mouseout", btnOut);
        me._$zout.idbPosAbs().idbHide().text("-").addClass("idb-zoom-btn").css(
        {"border-bottom-left-radius":rad, "border-bottom-right-radius":rad}
        ).appendTo($div).on("click", listener(me, "_onZoomOut")
        ).on("mousewheel", listener(me, "_onBtnMouseWheel")).on("mouseover", listener(me, "onBtnZoomOutMouseOver")).on("mouseout", btnOut);
    
        $zp.idbPosAbs().idbHide().appendTo($div).css({
            width:"110px",
            height:"40px"
        });
    }


    if(ctext) {
        me._$CLabel = $("<a>").appendTo($div).text(ctext).idbPosAbs().css(
        {
            right:"1px",
            "padding":"1px 1px",
            "background-color":"rgba(238, 238, 238, 0.8)",
            "font-size":"10px",
            "color":"#000000",
            "text-decoration":"none",
            "cursor":"default"
        });
        if(clink) {
            if(clink.toLowerCase().indexOf("http://") != 0 && clink.toLowerCase().indexOf("https://") != 0) {
                clink = "http://" + clink;
            }

            me._$CLabel.attr({
                 href:clink,
                 target:"_blank"
            }).css({"cursor":"auto"});
        }
    }


    me._mwts = 0;
    me._mwd = 0;

    if(!me._configPending) {
        me.createPoints();
        setTimeout(IDB.listener(me, "_initLegend"), 0);
    }


};


IDB.GeoMapGraph.prototype.createLegendItems = function() {
    var me = this, arr = [], geoMapConfig = me._geoMapConfig, hasIcon, numCols, liConfigs;
    if(!geoMapConfig) {
        return [];
    }
    hasIcon = geoMapConfig.hasLegendIcon;
    numCols = hasIcon ? 2 : 1;
    liConfigs = geoMapConfig.legendItemConfigs;
    for(var j=0, jj=liConfigs.length; j<jj; j++) {
        arr.push(liConfigs[j].createLI(numCols));
    }

    return arr;
};



////////////////////////////////////////////////////////////////////////////


IDB.GeoMapGraph.prototype._initLegend = function() {
    var me = this, chart = me._chart, info = me._info, I = IDB, LS = I.LS, canUseRangeLegend = me._canUseRangeLegend,
        geoMapConfig = me._geoMapConfig;
    if(geoMapConfig && geoMapConfig.legendItemConfigs.length) {
        if(canUseRangeLegend && "ranges" === me.gp("LegendPriority")) {
            info.legendStyle = LS.RANGES;
        }
        else {
            info.legendStyle = LS.CUSTOM;
        }
        chart.legendStyleChanged(me, info.legendStyle);
    }
    else if(canUseRangeLegend) {
        info.legendStyle = LS.RANGES;
        chart.legendStyleChanged(me, LS.RANGES);
    }
    else {
        IDB.log("_initLegend(): Doing nothing.");
    }
};

IDB.GeoMapGraph.prototype.createPoints = function() {
    var me = this, I = IDB, LatLon = I.LatLon, gp = me.gp, viewer = me._viewer,
        center = LatLon.parseGPS(gp("InitialCenter"));
    me._$zin.idbShow();
    me._$zout.idbShow();
    me._dpm.reset();
    me._dpGrid = new I.GMDatapointGrid(me);
    me._points = me._dpGrid._geoMapPoints;

    viewer.setCenter(center);
    viewer.setFineZoom(me.validZoom(me.gp("InitialZoom")));
    viewer.setDatapointGrid(me._dpGrid);
    viewer.enable();

    me._relevantDatapoints = (me._dpm.datapoints || []).concat();
};


IDB.GeoMapGraph.prototype.validZoom = function(fz) {
    var me = this, min = me._minFineZoom, max = me._maxFineZoom;
    return Math.max(min, Math.min(max, fz));
};

IDB.GeoMapGraph.prototype.getConfigUrl = function() {
   var me = this, I = IDB, config = I.config, gp = me.gp, url = (gp("ConfigXmlUrlTemplate") || ""), urlLC = url.toLowerCase(),
       isServerUrl = (urlLC.indexOf("content:") === 0),
       useProxy = (!isServerUrl && config.proxyEnabled && !!gp("ConfigUseProxy")), chart = me._chart;

   return I.resolveUrl(chart.expandMacros(url, 'url', null), useProxy);
};

IDB.GeoMapGraph.prototype._onZoomIn = function(e) {
    var me = this, viewer = me._viewer, old = viewer.getFineZoom(), newFz = me.validZoom(old+1),
        shiftKey = (e && e.shiftKey), max = me._maxFineZoom;

    e.target.focus();

    if(shiftKey) {
        if((newFz % 3) && (newFz < max)) {
            newFz = Math.min(max, (newFz - (newFz %3) + 3));
        }
    }

    if(old != newFz) {
        viewer.setFineZoom(newFz);
        me.updateZPanel();
    }
};


IDB.GeoMapGraph.prototype._onZoomOut = function(e) {
    var me = this, viewer = me._viewer, old = viewer.getFineZoom(), newFz = me.validZoom(old-1),
        shiftKey = (e && e.shiftKey), min = me._minFineZoom;


    e.target.focus();
    if(shiftKey) {
        if((newFz % 3) && newFz > min) {
            newFz -= (newFz % 3);
            newFz = Math.max(min, newFz);
        }
    }

    if(old != newFz) {
        viewer.setFineZoom(newFz);
        me.updateZPanel();
    }
};

IDB.GeoMapGraph.prototype._onBtnMouseWheel = function(evt) {
    var me = this, I = IDB, viewer = me._viewer, oldFz = viewer.getFineZoom(), ts = evt.timeStamp, et = (ts - me._mwts), deltaY = evt.deltaY,
    dir = (deltaY > 0 ? 1 : -1), dirChange = !me._mwd || (dir != me._mwd),
    newFz = me.validZoom(oldFz + deltaY);

    evt.preventDefault();

    // detect weird behavior when rotating mouswheel rapidly.
    if(et < 100 && dirChange) {
        I.log("RETURNING.............................................: et=" + et + ", dir=" + dir + ", dirChange=" + dirChange);
        return;
    }

    me._mwts = ts;
    me._mwd = dir;

    if(oldFz === newFz) {
        return;
    }

    viewer.setFineZoom(newFz);
    me.updateZPanel();
};



IDB.GeoMapGraph.prototype.onBtnZoomInMouseOver = function() {
    var me = this;
    me._$overBtn = me._$zin;
    me.onBtnMouseOver();
};

IDB.GeoMapGraph.prototype.onBtnZoomOutMouseOver = function() {
    var me = this;
    me._$overBtn = me._$zout;
    me.onBtnMouseOver();
};


IDB.GeoMapGraph.prototype.onBtnMouseOver = function() {
    var me = this, $zp = me._$zpanel, viewer = me._viewer;
//    me._viewer._touchHandler._preventDefaultOn.touchstart = false;
    me.updateZPanel();
    $zp.idbShow();
    viewer.setCrosshairsVisible(true);
};

IDB.GeoMapGraph.prototype.onBtnMouseOut = function() {
    var me = this, $zp = me._$zpanel, viewer = me._viewer;
    $zp.idbHide();
    viewer.setCrosshairsVisible(false);
    me._$overBtn = null;

//    me._viewer._touchHandler._preventDefaultOn.touchstart = true;
};

IDB.GeoMapGraph.prototype.datapointClicked = function(dp) {
    var me = this;
    me._offButtonTouch();
    IDB.GraphBase.prototype.datapointClicked.call(me, dp);
};


IDB.GeoMapGraph.prototype._offButtonTouch = function() {
    var me = this, $overBtn = me._$overBtn;
    if($overBtn) {
        me._viewer._blockTimer();
        $overBtn.trigger("mouseout");
        me._$overBtn = null;
    }
    
};



IDB.GeoMapGraph.prototype.updateZPanel = function() {
    var me = this, fz = me._viewer.getFineZoom(), $zp = me._$zpanel, center = me._viewer._grid._initialCenter,
       z = (fz % 3) ? "" + fz : "[" + fz + "]";
    if(!center) {
        return;
    }
    $(".idb-zpanel-z", $zp).text(z);
    $(".idb-zpanel-c", $zp).text( center.toGPS(4));
};


IDB.GeoMapGraph.prototype.updateZoomButtons = function() {
    var me = this, viewer = me._viewer, fz = viewer.getFineZoom(), $zin = me._$zin, $zout = me._$zout;
    if(fz >= me._maxFineZoom) {
        $zin.addClass("idb-zoom-btn-disabled");
        $zout.removeClass("idb-zoom-btn-disabled");
    }
    else if(fz <= me._minFineZoom) {
        $zout.addClass("idb-zoom-btn-disabled");
        $zin.removeClass("idb-zoom-btn-disabled");
    }
    else {
        $zout.removeClass("idb-zoom-btn-disabled");
        $zin.removeClass("idb-zoom-btn-disabled");
    }
};


IDB.GeoMapGraph.prototype._update = function(w, h) {
    var me = this, I = IDB, log = I.log, viewer = me._viewer, axisSelector = me._axisSelector, $selector, selectorHeight = 0,
        $zp = me._$zpanel;
    log("GeoMapGraph._update(): w=" + w + ", h=" + h);


    if(axisSelector) {
        $selector = axisSelector._$div;
        selectorHeight = axisSelector.measuredHeight();
        $selector.width(w + "px").height(selectorHeight + "px");
        h = h-selectorHeight;

        $selector.css({"top":h+"px"});
        axisSelector.resize();
    }

    viewer.resize(w, h);
    me._$zin.idbXY(w-30, h-65);
    me._$zout.idbXY(w-30, h-45);
    if(me._$CLabel) {
        me._$CLabel.css({
            "bottom":(selectorHeight+1) + "px"
        });
    }

    $zp.idbXY(w-150, h-60);
};

IDB.GeoMapGraph.prototype.axisSelected = function(axis) {
    var me = this, points = me._points, dpm = me._dpm, chart = me._chart;
    dpm.reset();  // remove all datapoints from the DPM
    for(var j=0, jj=points.length; j<jj; j++) {
        points[j].setRangeAxis(axis);  // this call will add relevant datapoints back to DPM.
    }
    chart.yAxisSelected(me, axis);
    me._viewer.invalidateStage();
};


////////////////////////////////////////////////////////////////////////////
// Config file
/* jshint unused:true */
IDB.GeoMapGraph.prototype.onConfigLoaded = function(data, status, jqXHR) {
/* jshint unused:false */

    var me = this, I = IDB, $xml = $(data).children("geoplot"), chart = me._chart, msg;

    if($xml.length === 0) {
        msg = IDB.format("GeoPlot.html5.configFileInvalidRoot", me._configUrl, "geoplot");
        chart.displayUserMessage(me, msg, "error", null);
        return;
    }

    try {
        me._geoMapConfig = new I.GeoMapConfig($xml, me._configUrl, me);
    }
    catch(err) {
        console.log("ERROR BUILDING CONFIG *******************************", err);
        chart.displayUserMessage(me, err.message, "error", null);
        return;
    }

    me._viewer._objectLayer.setGeoMapConfig(me._geoMapConfig);
    me._viewer._imageLayer.setGeoMapConfig(me._geoMapConfig);


    if(me._geoMapConfig.legendItemConfigs.length > 0 || me._canUseRangeLegend) {
        setTimeout(IDB.listener(me, "_initLegend"), 0);
    }

    me._configPending = false;
    me.createPoints();
};


IDB.GeoMapGraph.prototype.onConfigFail = function() {
    IDB.log(arguments, "onConfigFail()");
   var me = this, chart = me._chart,
        msg = IDB.format("GeoPlot.html5.configFileError", me._configUrl);
        chart.displayUserMessage(me, msg, "warning", null);
};

IDB.GeoMapGraph.prototype.destroy = function() {
    var me = this, destroyed = me._destroyed, points = me._points;
    IDB.log(me, "destroy(): destroyed=" + destroyed);
    if(destroyed) {
       return;
    }
    if(points) {
        for(var j=0, jj=points.length; j<jj; j++) {
            points[j].destroy();
        }
        me._points = null;
    }
    if(me._axisSelector) {
       me._axisSelector.destroy();
       me._axisSelector = null;
    }
    IDB.GraphBase.prototype.destroy.call(me);
    me._destroyed = true;
 

};

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GeoMapGraph.prototype.highlightMatchingDatapoints = function(value, matchType, fadeOthers) {
	var points = IDB.GraphBase.prototype.highlightMatchingDatapoints
		.call(this, value, matchType, fadeOthers);

	this._viewer.invalidateStage();
	return points;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GeoMapGraph.prototype.highlightDatapoint = function(dp, matchType, fadeOthers) {
	IDB.GraphBase.prototype.highlightDatapoint.call(this, dp, matchType, fadeOthers);
	this._viewer.invalidateStage();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GeoMapGraph.prototype.normalizeAllDatapoints = function() {
	IDB.GraphBase.prototype.normalizeAllDatapoints.call(this);
	this._viewer.invalidateStage();
};



IDB.GeoMapGraph.initProps = function(newInstance) {
    var props = {
        _configUrl:null,
        _configPending:false,
        _geoMapConfig:null,
        _dpGrid:null,
        _points:null,
        _pointLabelShadowColor:null,
        _destroyed:false,
        _rangeLegendAxes:null,
        _canUseRangeLegend:false,
        _maxFineZoom:54,
        _minFineZoom:0,
        _lockZoom:false,
        _lockCenter:false,
        _suppressPointLabels:false,
        _$viewer:null,
        _viewer:null,
        _$zin:null,
        _$zout:null,
        _mwts:0,
        _mwd:0,
        _$overBtn:null,
        _$CLabel:null
     };
     $.extend(newInstance, props);
};

/*global IDB, $ */

IDB.GeoMapGrid = function(viewer, $div) {
    var me = this, I = IDB;
	IDB.LayoutContainer.call(this);
    $div.idbPosAbs();

    me._viewer = viewer;
    me._$div = $div;
    me._tileCache = [];
    me._displayedTiles = [];
    me._zoom = 17;
    me._tileSize = 322;
    me._gridEnabled = me._layoutEnabled = false;
    me._W = NaN;
    me._H = NaN;
    me._lastW = NaN;
    me._lastH = NaN;
    me._model = null;
    me._modelValid = false;
    me._initialCenter = I.LatLon.ORIGIN;

    me._gridX = 0;
    me._gridY = 0;
    me._gridW = 0;
    me._gridH = 0;

    me._extU = 0;
    me._extD = 0;
    me._extL = 0;
    me._extR = 0;
    me._modelExtensionsPending = false;

    me._normalizedUrlTemplate = null;
    me._urlHosts = null;
    me._useQuadKey = false;
    me._useProxy = false;
    
//    var tile = new I.MapTile(me, 18, 132805, 90180);
//    tile._$element.appendTo($div);
   
   Object.preventExtensions(me);
};

IDB.GeoMapGrid.prototype = Object.create(IDB.LayoutContainer.prototype);
IDB.GeoMapGrid.prototype.constructor = IDB.GeoMapGrid;

IDB.GeoMapGrid.prototype.enable = function() {
    var me = this;
    me._gridEnabled = me._layoutEnabled = true;
    me.invalidateLayout();
};

IDB.GeoMapGrid.prototype.ext = function(dir, pixelsNeeded) {
   var me = this, ts = me._tileSize,
       n = pixelsNeeded <= 0 ? 1 : Math.ceil(pixelsNeeded/ts);
    me["_ext" + dir] = n;
    me._modelExtensionsPending = true;
    me.invalidateLayout();
};

IDB.GeoMapGrid.prototype.setCenter = function(c) {
    var me = this, I = IDB, LatLon = I.LatLon;
    me._initialCenter = c || LatLon.ORIGIN;
    me._modelValid = false;
    me.invalidateLayout();
};

IDB.GeoMapGrid.prototype.setTileSize = function(n) {
    var me = this, ts = me._tileSize;
    me._modelValid = (ts === n) && me._modelValid;
    me._tileSize = n;
    if(!me._modelValid) {
        me.invalidateLayout();
    }
};

IDB.GeoMapGrid.prototype.getTileSize = function() {
    return this._tileSize;
};

IDB.GeoMapGrid.prototype.setZoom = function(n) {
    var me = this, z = me._zoom;
    n = Math.min(18, Math.max(0, n));
    me._modelValid = (z === n) && me._modelValid;
    me._zoom = n;
    if(!me._modelValid) {
        me.invalidateLayout();
    }
};

IDB.GeoMapGrid.prototype.getZoom = function() {
    return this._zoom;
};

IDB.GeoMapGrid.prototype.position = function() {
    return this._$div.position();
};

IDB.GeoMapGrid.prototype.cacheTile = function(tile, zoom, tx, ty) {
    var me = this, _tileCache = me._tileCache, txList = _tileCache[zoom], tyList;
    if(!txList) {
        txList = [];
        _tileCache[zoom] = txList;
    }
    tyList = txList[tx];
    if(!tyList) {
        tyList = [];
        txList[tx] = tyList;
    }
    tyList[ty] = tile;
};

IDB.GeoMapGrid.prototype.getCachedTile = function(zoom, tx, ty) {
    var me = this, _tileCache = me._tileCache, txList = _tileCache[zoom], tyList;
    if(!txList) {
        return null;
    }
    tyList = txList[tx];
    if(!tyList) {
        return null;
    }
    return (tyList[ty] || null);
};

IDB.GeoMapGrid.prototype.setUrlTemplate = function(urlTemplate) {
    var me = this, I = IDB, GMG = I.GeoMapGrid, DUT = GMG.DUT, normalized = $.trim(urlTemplate),
        hosts = null, re = /\[(\w+(\|\w+)*)\]/, reps = re.exec(normalized), lkey = I.config.tsid || null;
    me._urlHosts = me._normalizedUrlTemplate = null;
    if(normalized) {
        if(reps) {
            hosts = reps[1].split("|");
            if(hosts.length === 1) {
                hosts = reps[1].split("");
            }
            me._urlHosts = hosts;
            normalized = normalized.replace(reps[0], "${host}");
        }
        if(lkey) {
            normalized = normalized.replace(/\$\{auth\}/g, lkey);
        }
        normalized = normalized.replace(/\/(zoom)([^A-Za-z0-9])/, "/${z}$2");
        normalized = normalized.replace(/\/(x)([^A-Za-z0-9])/, "/${x}$2");
        normalized = normalized.replace(/\/(y)([^A-Za-z0-9])/, "/${y}$2");
        normalized = normalized.replace(/\$\{slash\}/g, "/");
        normalized = normalized.replace(/\$\{dollar\}/g, "$");
        me._normalizedUrlTemplate = normalized;
    }
    else {
        if(lkey) {
            me._normalizedUrlTemplate = DUT.replace(/\$\{auth\}/g,  lkey);
            me._urlHosts = GMG.DUH.concat();
        }
    }

    me._useQuadKey = me._normalizedUrlTemplate && (me._normalizedUrlTemplate.indexOf("${quadkey}") >= 0);
};

IDB.GeoMapGrid.getQuadKey = function(x,y,z) {
    var j = z, qk = [], d, m;
    for (j=z; j>0; j--) {
        d = '0';
        m = (1 << (j - 1));
        if ((x & m) != 0) {
            d++;
        }
        if ((y & m) != 0) {
            d++;
            d++;
        }
        qk.push(d);
    }
    return qk.join('');
};

IDB.GeoMapGrid.DUT = "http://${host}.tile.idashboards.com/auth/${auth}/tiles/${z}/${x}/${y}.png";

IDB.GeoMapGrid.DUH = ["a", "b", "c"];

IDB.GeoMapGrid.prototype.getUrl = function(zoom, tx, ty) {
    var me = this, I = IDB, GMG = I.GeoMapGrid, DUT = GMG.DUT, DUH = GMG.DUH,
        url = me._normalizedUrlTemplate, hosts = me._urlHosts, numHosts = hosts ? hosts.length : 0,
        proxyUrl = I.config.proxyUrl, useProxy = me._useProxy && proxyUrl, useQK = me._useQuadKey;

    if(!url) {
        return null;
    }

    if(numHosts) {
        url = url.replace("${host}", hosts[tx % numHosts]);
    }

    url = url.replace("${z}", zoom);
    url = url.replace("${x}", tx);
    url = url.replace("${y}", ty);
    if(useQK) {
        url = url.replace("${quadkey}", GMG.getQuadKey(tx, ty, zoom));
    }

    if(useProxy) {
        url = proxyUrl.replace(/\{0\}/, encodeURIComponent(url));
    }
    return url;
};

IDB.GeoMapGrid.prototype.displayTile = function(zoom, tx, ty) {
    var me = this, I = IDB, MapTile = I.MapTile,  $div = me._$div,
        _displayedTiles = me._displayedTiles,
        tile = me.getCachedTile(zoom, tx, ty), newTile = false;

    if(tile && tile._isBroken) {
        IDB.log("Replacing broken tile.");
        tile._$element.remove();
        tile = null;
    }
    if(!tile) {
        tile = new MapTile(me, zoom, tx, ty, me.getUrl(zoom, tx, ty));
        me.cacheTile(tile, zoom, tx, ty);
        newTile = true;
    }

    if(newTile || !$.contains($div[0], tile._$element[0])) {
//        IDB.log(tile._$element[0],  "C: " + $.contains($div[0], tile._$element[0]));
        tile._$element.prependTo($div);
    }

    tile.displayed = true;
    if(_displayedTiles.indexOf(tile) < 0) {
        _displayedTiles.push(tile);
    }
    return tile;
};

IDB.GeoMapGrid.prototype.resetDisplayedFlags = function() {
    var me = this, _displayedTiles = me._displayedTiles;
    for(var j=0, jj=_displayedTiles.length; j<jj; j++) {
        _displayedTiles[j].displayed = false;
    }
};

IDB.GeoMapGrid.prototype.removeUndisplayedTiles = function() {
    
    var me = this, _displayedTiles = me._displayedTiles, tile, arr = [];
    for(var j=0, jj=_displayedTiles.length; j<jj; j++) {
        tile = _displayedTiles[j];
        if(!tile.displayed) {
            tile.detach();
        }
        else {
            arr.push(tile);
        }
    }
    me._displayedTiles = arr;
};

IDB.GeoMapGrid.prototype.move = function(x,y) {
   var me = this, $div = me._$div, px = "px";
   $div.css({left:x+px, top:y+px});
};

IDB.GeoMapGrid.prototype.resize = function(w,h) {
    var me = this, $div = me._$div;

    $div.width(w-2).height(h-2);

    me._W = w;
    me._H = h;
    me.invalidateLayout();
};

IDB.GeoMapGrid.prototype.doLayout = function() {

    //IDB.log("GeoMapGrid.doLayout()");
    
    var me = this, w = me._W, h = me._H, T, model, tilesWide, tilesHigh, gridTX, gridTY, row, col, tileYPixels, tileXPixels, tx, ty, tile;

    T = me._tileSize;

    me.maybeRefreshModel(w, h);

    model = me._model;

    tilesWide = model.tilesWide;
    tilesHigh = model.tilesHigh;

    me._gridX = model.gridX;
    me._gridY = model.gridY;
    me._gridW = tilesWide * T;
    me._gridH = tilesHigh * T;

    gridTX = model.gridTX;
    gridTY = model.gridTY;

    me.resetDisplayedFlags();

    for(row=0; row<tilesHigh; row++) {
        tileYPixels = me._gridY + (row*T);
        ty = gridTY + row;
        for(col = 0; col<tilesWide; col++) {
            tileXPixels = me._gridX + (col*T);
            tx = gridTX + col;
            tile = me.displayTile(me._zoom, tx, ty);
//            tile.debugBorderEnabled = _debugBorderEnabled;
            tile.moveAndSize(tileXPixels, tileYPixels, T);
        }
    }

    me.removeUndisplayedTiles();

    me._model = model;
    me._lastW = w;
    me._lastH = h;

    me.validateLayout();
};

IDB.GeoMapGrid.prototype.maybeRefreshModel = function(w,h) {
    var me = this, I = IDB, lastW = me._lastW, lastH = me._lastH, changed = true, center = me._initialCenter, zoom = me._zoom;
    if(!me._gridEnabled) {
        return false;
    }

    changed = (!me._modelValid || isNaN(lastW) || (lastW != w) || isNaN(lastH) || (lastH != h));

    me._model = !changed ? me._model : new I.MGModel(w, h, center, zoom, me._tileSize);
    me._modelValid = true;

    changed = changed || me.applyExtensions();
    if(changed) {
        me.fireModelChange();
    }

    return changed;
};

IDB.GeoMapGrid.prototype.applyExtensions = function() {

    var me = this, model = me._model, dirs = ["U", "L", "R", "D"], n;
    if(!me._gridEnabled || !model || !me._modelExtensionsPending) {
        return false;
    }
    for(var j=0, jj=dirs.length; j<jj; j++) {
        n = me["_ext" + dirs[j]];
        if(n) {
            model.ext(dirs[j], n);
        }
        me["_ext" + dirs[j]] = 0;
    }
    me._modelExtensionsPending = false;
//    trace("GeoMapGrid..applyExtensions(): Returning true");
    return true;
};

IDB.GeoMapGrid.prototype.fireModelChange = function() {

};
IDB.GeoMapGrid.prototype.getCoordsAt2 = function(xPixels, yPixels) {
    var me = this;
    return me.getCoordsAt(xPixels - me._gridX, yPixels - me._gridY);
};
IDB.GeoMapGrid.prototype.getCoordsAt = function(xPixels, yPixels) {
    var me = this, I = IDB, LatLon = I.LatLon, M = me._model, T = M.tileSize, msP = M.msP,
        xMaps =  (xPixels+(M.gridTX*T))/msP,
        yMaps = (yPixels+(M.gridTY*T))/msP,
        lat = LatLon.latFromMaps(yMaps),
        lon = LatLon.lonFromMaps(xMaps),
        c = new LatLon(lat, lon);
    return c;
};

IDB.GeoMapGrid.prototype.isOnGrid = function(c, loose) {
    var me = this, model = me._model;
    return me._gridEnabled && model && model.isOnGrid(c, !!loose);
};

IDB.GeoMapGrid.prototype.getXYFor = function(c) {
    var me = this, model = me._model, x, y, p;
    if(!me._gridEnabled  || !model) {
        return null;
    }

    x = model.mapX + (c.xM * model.msP);
    y = model.mapY + (c.yM * model.msP);
    p = {x:x, y:y};
    return p;
};



/*global IDB, $ */
IDB.GeoMapImage = function(owner,  url, zmin,  zmax, imageWidth, imageHeight, ho,  vo, tile, latLon) {
    var me = this, css = {}, $img = $("<img class=\"idb-geomap-image\" draggable=\"false\">").idbPosAbs().on( 'dragstart', function() { return false; } );
    IDB.GeoMapImage.initProps(me);
    Object.preventExtensions(me);

    me._parent = owner;
    me._$element = $img;
    me._zmin = zmin;
    me._zmax = zmax;
    me._imageWidth = tile ? 256 : imageWidth;
    me._imageHeight = tile ? 256 : imageHeight;
    me._ho = ho;
    me._vo = vo;
    me._tile = tile;
    me._latLon = latLon;

    if(!isNaN(me._imageWidth)) {
       css.width = me._imageWidth + "px";
    }
    if(!isNaN(me._imageHeight)) {
       css.height = me._imageHeight + "px";
    }
    $img.css(css).attr("src", url);

};

Object.defineProperties(IDB.GeoMapImage.prototype, {
    displayed:{
        get:function() {
            return (this._displayed);
        },
        set:function(b) {
            var me = this;
            b = !!b;
            me._displayed = b;
        }
    }
});

IDB.GeoMapImage.prototype.move = function(x,y) {
   var me = this, ho = me._ho, vo = me._vo, $img = me._$element;
   $img.idbXY(x+ho,y+vo);
};

IDB.GeoMapImage.prototype.detach = function() {
    var me = this, $img = me._$element;
    $img.detach();
};

IDB.GeoMapImage.prototype.showAtZoom = function(fineZoom) {
    var me = this, zmin = me._zmin, zmax = me._zmax;
    return (fineZoom >= zmin && fineZoom <= zmax);
};

IDB.GeoMapImage.prototype.setTileScale = function(tileScale) {
    var me = this, $img = me._$element, isTile = me._tile, newSize = (tileScale * 256) + "px";
    if(!isTile) {
        return;
    }
    me._tileScale = tileScale;
    $img.css( {
        width:newSize,
        height:newSize
    });
};



IDB.GeoMapImage.initProps = function(newInstance) {
    var props = {
        _parent:null,
        _$element:null,
        _url:null,
        _zmin:0,
        _zmax:54,
        _imageWidth:NaN,
        _imageHeight:NaN,
        _ho:0,
        _vo:0,
        _tile:false,
        _tileScale:1.0,
        _latLon:null,
        _displayed:false
     };
     $.extend(newInstance, props);
};

/*global IDB, $ */
IDB.GeoMapImageConfig = function($xml, overrideUrl, defUrl, defZMin, defZMax, defW, defH, defHO, defVO,
            defTile, defUseProxy) {
    var me = this, I = IDB, MLC = I.MapLocationConfig, GMIC = I.GeoMapImageConfig;
    MLC.call(me);
    GMIC.initProps(me);
    Object.preventExtensions(me);
    me.configure($xml, defZMin, defZMax);

    me._url = overrideUrl || $xml.idbFirstChildText("url") || defUrl;
    me._tile = IDB.toBool($xml.idbFirstChildText("tile"), defTile);
    if(me._tile) {
        me._imageWidth = me._imageHeight = 256;
    }
    else {
        me._imageWidth = $xml.children("width").idbTextToNumber(defW);
        me._imageHeight = $xml.children("height").idbTextToNumber(defH);
        me._ho = $xml.children("ho").idbTextToNumber(defHO);
        me._vo = $xml.children("vo").idbTextToNumber(defVO);
    }

    me._useProxy = $xml.children("useProxy").idbTextToBool(defUseProxy);
    me._isValid = me._url && me._isValid;
};

IDB.GeoMapImageConfig.prototype = Object.create(IDB.MapLocationConfig.prototype);
IDB.GeoMapImageConfig.prototype.constructor = IDB.GeoMapPoint;

IDB.GeoMapImageConfig.prototype.getGeoMapImage = function(owner) {
    var me = this, I = IDB, config = I.config, proxyEnabled = config.proxyEnabled, gmi = me._geoMapImage,
    urlLC = (me._url || "").toLowerCase(), isServerUrl = (urlLC.indexOf("content:") === 0),
    useProxy = (!isServerUrl && proxyEnabled && me._useProxy),
    graph = me._graph, chart = graph._chart,
    url = I.resolveUrl(chart.expandMacros(me._url, 'url', null), useProxy);

    if(!me._isValid) {
        return null;
    }
    if(gmi) {
        return gmi;
    }

    me._geoMapImage = gmi = new I.GeoMapImage(owner,  url, me._zmin,  me._zmax, me._imageWidth, me._imageHeight, me._ho,  me._vo, me._tile, me._latLon);
    return gmi;
};

IDB.GeoMapImageConfig.initProps = function(newInstance) {
    var props = {
        _url:null,
        _imageWidth:NaN,
        _imageHeight:NaN,
        _ho:0,
        _vo:0,
        _tile:false,
        _useProxy:false,
        _graph:null,
        _geoMapImage:null
     };
     $.extend(newInstance, props);
};
/*global IDB, $ */
IDB.GeoMapImageLayer = function($div, viewer, grid, graph) {
    var me = this, I = IDB;
    I.LayoutContainer.call(me);
    I.GeoMapImageLayer.initProps(me);
    Object.preventExtensions(me);
    me._$div = $div;
    me._viewer = viewer;
    me._grid = grid;
    me._graph = graph;
};
IDB.GeoMapImageLayer.prototype = Object.create(IDB.LayoutContainer.prototype);
IDB.GeoMapImageLayer.prototype.constructor = IDB.GeoMapImageLayer;

IDB.GeoMapImageLayer.prototype.setGeoMapConfig = function(config) {
   var me = this, objects = config ? (config._geoMapImageConfigs || null) : null,
   displayed = [];
   me._geoMapImageConfigs = objects;
   if(!objects) {
       return;
   }
   for(var j=0, jj=objects.length; j<jj; j++) {
       displayed.push(objects[j].getGeoMapImage(me));
       //objects[j].addToLayer(layer);
   }
   me._geoMapImages = displayed;
   me.invalidateLayout();
};


IDB.GeoMapImageLayer.prototype.move = function(x, y) {
   var me = this, $div = me._$div, px = "px", css= {left:x+px, top:y+px};
   $div.css(css);
};

IDB.GeoMapImageLayer.prototype.resize = function(w, h) {
    var me = this, $div = me._$div;

    $div.width(w-2).height(h-2);

    me._W = w;
    me._H = h;
    me.invalidateLayout();
};

IDB.GeoMapImageLayer.prototype.doLayout = function() {

    var me = this, grid = me._grid, $div = me._$div, p,
        objects = me._geoMapImages || [], o, fineZoom = me._viewer.getFineZoom(),
        tileScale = grid._tileSize / 256;

    for(var j=0, jj=objects.length; j<jj; j++) {
        o = objects[j];
        var vis = o.showAtZoom(fineZoom) && grid.isOnGrid(o._latLon, o._tile);
        if(!vis) {
           o.detach();
           continue;
        }

        if(!$.contains($div[0], o._$element[0])) {
    //        IDB.log(tile._$element[0],  "C: " + $.contains($div[0], tile._$element[0]));
            o._$element.prependTo($div);
        }

        if(o._tile) {
            o.setTileScale(tileScale);
        }
        p = grid.getXYFor(o._latLon);
        o.move(p.x,  p.y);
    }

    me.validateLayout();
};


IDB.GeoMapImageLayer.initProps = function(newInstance) {
    var props = {
        _$div:null,
        _viewer:null,
        _grid:null,
        _graph:null,
        _W:NaN,
        _H:NaN,
        _geoMapImageConfigs:null,
        _geoMapImages:null
     };
     $.extend(newInstance, props);
};
/*global IDB, $ */
IDB.GeoMapObject = function(stageMgr, graph) {
    var me = this;

    IDB.MapObject.call(me, stageMgr);
    IDB.GeoMapObject.initProps(me);
    Object.preventExtensions(me);
    me._graph = graph;
    me.setVisible(false);
    me._hasShape = false;
};
IDB.GeoMapObject.prototype = Object.create(IDB.MapObject.prototype);
IDB.GeoMapObject.prototype.constructor = IDB.GeoMapObject;

IDB.GeoMapObject.prototype.showAtZoom = function(fineZoom){
    var me = this, zmin = me._zmin, zmax = me._zmax;
    return (fineZoom >= zmin && fineZoom <= zmax);
};

IDB.GeoMapObject.prototype.isValid = function() {
    var me = this, latLon = me._latLon;
    return latLon && latLon._isValid;
};

IDB.GeoMapObject.prototype.configureFromXml = function($xml, $defaultZMin, $defaultZMax, labelDefaults, markerDefaults) {
    var me = this, $top = null, $marker = null, $label = null, localName = $xml.prop("tagName");

    if(localName === "object") {
        $top = $xml;
        $marker = $xml.idbFirstChild("marker");
        if($marker.length === 0) {
            $marker = null;
        }
        $label = $xml.idbFirstChild("label");
        if($label.length === 0) {
            $label = null;
        }
    }
    else if(localName == "marker") {
        $top = $marker = $xml;
    }
    else if(localName == "label") {
        $top = $label = $xml;
    }
    else {
        IDB.log($xml, "GeoMapObject.configureFromXml(): Invalid XML: ");
    }

    me._hasShape = $marker !== null;
    if(null === $top) {
        return;
    }

    me.configureTop($top, $defaultZMin, $defaultZMax);

    if($marker) {
        me.configureMarker($marker, markerDefaults);
    }
    if($label) {
        me.configureLabel($label, labelDefaults);
    }
};


IDB.GeoMapObject.prototype.configureTop = function($xml, $defaultZMin, $defaultZMax) {
    var me = this, get = function(name) {return $xml.idbFirstChildText(name) || null;},
        num = function(name, defaultVal) {if(defaultVal === undefined) {defaultVal = NaN;}
           return $xml.children(name).idbTextToNumber(defaultVal);},
        gps = get("latlon"),
        lon = num("lon", NaN),
        lat = num("lat", NaN), LatLon = IDB.LatLon, s;
    if(gps) {
        me._latLon = LatLon.parseGPS(gps);
    }
    else {
        me._latLon = new LatLon(lat, lon);
    }
    me._zmin = Math.max(0, $xml.idbAttrToNumber("zmin", $defaultZMin));
    me._zmax = Math.max(me._zmin, Math.min(54, $xml.idbAttrToNumber("zmax", $defaultZMax)));
};

IDB.GeoMapObject.prototype.configureLabel = function($xml, def) {
//    if(null === xml) {
//        this.hasLabel = false;
//        return;
//    }

    var me = this,
        get = function(name) {return $xml.idbFirstChildText(name) || null;},
        num = function(name, defaultVal) {if(defaultVal === undefined) {defaultVal = NaN;}
           return $xml.children(name).idbTextToNumber(defaultVal);},
        text = ($.trim(get("text")) || $.trim(get("value"))), s;

    if(!text) {
       return;
    }

    me._hasLine = IDB.toBool(get("line"), def.hasLine);
    me._labelShadow = IDB.toBool(get("shadow"), def.labelShadow);

    me._labelSize = num("size", def.labelSize);
    s = get("color");
    me._labelColor = s ? IDB.rgbx(s) : def.labelColor;
    me._labelAlign = get("align") || def.labelAlign;
//    this.labelRTL = XMLUtils.xmlListToBoolean(xml.rtl, def.labelRTL);
    me._labelText = text;
    me._labelHOffset = num("ho", def.labelHOffset);
    me._labelVOffset = num("vo", def.labelVOffset);
};


IDB.GeoMapObject.prototype.configureMarker = function($xml, def) {
    var me = this, s;
    if(null === $xml) {
        me._hasShape = false;
        return;
    }
    me._hasShape = true;
    me._setShapeFromString($xml.idbFirstChildText("shape") || def.shape);
    me._size = $xml.children("size").idbTextToNumber(def.size);
    s = $xml.idbFirstChildText("color");
    me._shapeColor = s ? IDB.rgbx(s) : def.shapeColor;
};




IDB.GeoMapObject.initProps = function(newInstance) {
    var props = {
        _graph:null,
        _latLon:null,
        _zmin:12,
        _zmax:54
    };
    $.extend(newInstance, props);
};

/*global IDB, Kinetic */
IDB.GeoMapObjectLayer = function(layer, viewer, grid, graph) {
    var me = this, crosshairs, line, hq;
    IDB.LayoutContainer.call(me);
    me._W = me._H = 0;
    me._layer = layer;
    me._grid = grid;
    me._graph = graph;
    me._viewer = viewer;
    me._geoMapObjects = null;

    me._crosshairs = crosshairs = new Kinetic.Group();

    layer.add(crosshairs);

    line = new Kinetic.Line({
        x:0, y:0, strokeWidth:1, fillEnabled:false, stroke:'red', points:[-0.5, -16, -0.5, 16]
    });

    crosshairs.add(line);

    line = new Kinetic.Line({
        x:0, y:0, strokeWidth:1, fillEnabled:false, stroke:'red', points:[-16, -0.5, 16, -0.5]
    });

    crosshairs.add(line);

   me._hq = hq = new IDB.GeoMapObject(viewer, graph);
   $.extend(hq, {
       _shape:"star",
       _size:20,
       _shapeColor:IDB.rgbx(0xFF0000),
       _labelText:"iDashboards\nWorld HeadQuarters",
       _labelHOffset:-20,
       _labelVOffset:-55,
       _labelVisible:true,
       _hasLine:true,
       _blinkEnabled:false
       });
//   hq.addToLayer(layer);
//   hq._group.on("mouseover", function() {
//       hq._blinkPaused = true;
//       hq.setLabelVisible(true);
//   } );
//
//   hq._group.on("mouseout", function() {
//       hq._blinkPaused = false;
//       hq.setLabelVisible(false);
//   } );

   Object.preventExtensions(me);

};
IDB.GeoMapObjectLayer.prototype = Object.create(IDB.LayoutContainer.prototype);
IDB.GeoMapObjectLayer.prototype.constructor = IDB.GeoMapObjectLayer;


IDB.GeoMapObjectLayer.prototype.setGeoMapConfig = function(config) {
   var me = this, objects = config ? (config._geoMapObjects || null) : null, layer = me._layer;
   me._geoMapObjects = objects;
   if(!objects) {
       return;
   }
   for(var j=0, jj=objects.length; j<jj; j++) {
       objects[j].addToLayer(layer);
   }
   me.invalidateLayout();
};

IDB.GeoMapObjectLayer.prototype.setCrosshairsVisible = function(b) {
    var me = this, ch = me._crosshairs, v = me._viewer;
    ch.visible(!!b);
    v.invalidateStage();
};

IDB.GeoMapObjectLayer.prototype.resize = function(w,h) {
    var me = this;
    me._W = w;
    me._H = h;
    me.doLayout();
};

IDB.GeoMapObjectLayer.prototype.doLayout = function() {
    var me = this, w = me._W, h = me._H, grid = me._grid,
    objects = me._geoMapObjects || [], o, fineZoom = me._viewer.getFineZoom(),
    c = IDB.LatLon.HQ, p = grid.getXYFor(c) || {x:0, y:0}, ch = me._crosshairs, hq = me._hq,
    onGrid = grid.isOnGrid(c);
//    IDB.log(p, "GeoMapObjectLayer.doLayout() onGrid=" + onGrid);

    ch.x(w/2);
    ch.y(h/2);
    hq.setVisible(onGrid);
    if(onGrid) {
        hq.move(p.x,  p.y);
    }

    for(var j=0, jj=objects.length; j<jj; j++) {
        o = objects[j];
        var vis = o.showAtZoom(fineZoom) && grid.isOnGrid(o._latLon);
        o.setVisible(vis);
        if(vis) {
            p = grid.getXYFor(o._latLon);
            o.move(p.x,  p.y);
        }
    }



    me.validateLayout();
    me._viewer.invalidateStage();
};


IDB.GeoMapObjectLayer.prototype.move = function(x,y) {


    var me = this, layer = me._layer;
    layer.x(100 + x);
    layer.y(100 + y);
    me.invalidateLayout();
};
/*global IDB, $ */
IDB.GeoMapPoint = function(graph, shapeAxisIds, dpr) {
    var me = this, I = IDB, gp = graph.gp, listener = I.listener;

    IDB.MapObject.call(me, graph._viewer);
    IDB.GeoMapPoint.initProps(me);
    Object.preventExtensions(me);
    me._graph = graph;
    me._shapeAxisIds = shapeAxisIds;
    me._haloHighlight = gp("HaloHighlight");
    me._hasLabel = !graph._suppressPointLabels;
    me._pointLabelsAlwaysVisible = me._hasLabel && gp("PointLabelAlwaysVisible");
    me._labelVisible = me._hasLabel && me._pointLabelsAlwaysVisible;
    me._labelVOffset = gp("PointLabelVerticalOffset");
    me._labelHOffset = gp("PointLabelHorizontalOffset");
    me._pointLabelTemplate = $.trim(gp("PointLabelTemplate"));

    me._labelColor = gp("PointLabelColor");
    me._labelSize = gp("PointLabelSize");
    me._labelShadow = gp("PointLabelShadow");
    me._labelAlign = gp("PointLabelAlign");


    me._group.on("mouseover", listener(me, "_handleOver"));
    me._group.on("mouseout", listener(me, "_handleOut"));
    me._group.on("click", IDB.leftClickListener(me, "_handleClick"));
    me._touchHandler = new I.TouchHandler(me._group);
//    me.setDatapointRow(dpr);
};
IDB.GeoMapPoint.prototype = Object.create(IDB.MapObject.prototype);
IDB.GeoMapPoint.prototype.constructor = IDB.GeoMapPoint;

IDB.GeoMapPoint.prototype.isValid = function() {
    var me = this, latLon = me._latLon;
    return latLon && latLon._isValid;
};

IDB.GeoMapPoint.prototype.setDatapointRow = function(dpr) {
    var me = this, I = IDB, graph = me._graph, gp = graph.gp,
        $baseColor = gp("PointColor"), _viewableYAxes = {}, _rangeDatapoints = [],
        usingRangeColors, dataValues = dpr.dataRow, ids = me._shapeAxisIds, dv, s, id, val,
        dp, datapoints = dpr.datapoints, j, jj, $blinkEnabled = false, $blinkInterval = 1000, $blinkFactor = 1.0;

    me._datapointRow = dpr;

    me._setShapeFromString(gp("PointShape"));
    me._size = Math.max(Math.min(100, gp("PointSize")), 5);
    
    me._shapeColor = $baseColor;
    me._viewableYAxes = _viewableYAxes;
    me._rangeDatapoints = _rangeDatapoints;

    for(j=0, jj=datapoints.length; j<jj; j++) {
        dp = datapoints[j];
        _rangeDatapoints[dp.yAxis.axisId] = dp;
    }

    // This is the primary datapoint, and also the currently selected datapoint.

    me._currDp = dp = _rangeDatapoints[graph._dataYGraphAxisId];

    me._usingRangeColors = usingRangeColors = (gp("UseRangeColors") && dp.yAxis.dataTypeIsNumber);

    if(usingRangeColors) {
        $baseColor = dp.rangeColor;
    }

    id = ids._color;
    if(!usingRangeColors && ids._color) {
        dv = dataValues[id];
        if(!dv.isNothing) {
            if(ids._colorIsString) {
                s = dv.stringValue;
                s = s.replace(/^#/, "0x");
                $baseColor = I.rgbx(s, I.MapObject.BLACK);
            }
            else if(ids._colorIsNumber) {
                val = dv.numberValue;
                $baseColor = I.rgbx(Math.max(0, Math.min(0xFFFFFF, val)));
            }
        }
    }

    id = ids._shape;
    if(id) {
        dv = dataValues[id];
        if(!dv.isNothing) {
             s = dv.stringValue.toLowerCase();
             me._setShapeFromString(s);
        }
    }

    id = ids._size;
    if(id) {
        dv = dataValues[id];
        if(!dv.isNothing) {
            val = dv.numberValue;
            if(val !== 0) {
                me._size = Math.max(Math.min(100, val), 5);
            }
        }
    }

    id = ids._blink;
    if(id) {
        dv = dataValues[id];
        if(!dv.isNothing) {
            val = dv.numberValue;
            if(val > 0) {
                $blinkEnabled = true;
                $blinkInterval = Math.max(100, val);
            }
        }
    }

    id = ids._blinkFactor;
    if(id) {
        dv = dataValues[id];
        if(!dv.isNothing) {
            $blinkFactor = dv.numberValue;
        }
    }

    me._shapeColor = $baseColor;

    if($blinkEnabled) {
        me._blinkEnabled = true;
        me._blinkInterval = $blinkInterval;
        me._blinkFactor = $blinkFactor;
    }

    if(me._hasLabel) {
        me._labelText = me.getLabelText(dp);
    }

//    initLabelText();

};


IDB.GeoMapPoint.prototype._build = function() {
    var me = this, useRangeColors = me._usingRangeColors,
        stateListener = IDB.listener(me, "_onVisualState"),
        datapoint = me._currDp, rangeDatapoints = me._rangeDatapoints, graph = me._graph,
        rangeAxisIds = graph.rangeAxisIds;

    IDB.MapObject.prototype._build.call(this);

    graph._dpm.addDatapoint(datapoint);
    datapoint.onVisualState = stateListener;
    if(useRangeColors && datapoint.yAxis.dataTypeIsNumber) {
        for(var j=0, jj=rangeAxisIds.length; j<jj; j++) {
            rangeDatapoints[rangeAxisIds[j]].onVisualState = stateListener;
        }
    }
    else {
        datapoint.onVisualState = stateListener;
    }
};

IDB.GeoMapPoint.prototype.setRangeAxis = function(axis) {
    var me = this, axisId = axis.axisId, dp = me._rangeDatapoints[axisId], graph = me._graph;
    me._currDp = dp;
    graph._dpm.addDatapoint(dp);
    me._kineticShape.fill(dp.rangeColor.css);
    if(me._labelTemplate) {
        me._label.text(me.getLabelText(dp));
    }
};


IDB.GeoMapPoint.prototype.getLabelText = function(dp) {
    var me = this, graph = me._graph, s = dp.map.pointLabel, lt = me._pointLabelTemplate, temp, labelTemplate = me._labelTemplate;
    if(s) {
//        IDB.log("Cached: " + s);
        return s;
    }
    if(labelTemplate) {
//       IDB.log("Template: " + labelTemplate);
       s = me.expandSelectedAxisMacros(labelTemplate, dp);
       dp.map.pointLabel = s;
       return s;
    }
    if(lt) {
        lt = lt.replace(/\$\{\s*br\s*\}/ig, "\n");
        temp = graph._chart.expandMacros(lt, null, dp);
        s = me.expandSelectedAxisMacros(temp, dp);
        if(temp !== s) {
            me._labelTemplate = temp;
        }
//        IDB.log("Expanded: " + s);
    }
    else {
        s = dp.xValue.fv;
//        IDB.log("Using X Value: " + s);
    }
    dp.map.pointLabel = s;
    return s;
};

IDB.GeoMapPoint.prototype.expandSelectedAxisMacros = function(template, dp) {
    var s,
        yValue = dp.yValue,
        yAxis = dp.yAxis;
    if(!template) return template;
    s = template.replace(/\$\{\s*selected\s*:\s*axis\s*\}/ig, yAxis.axisName);
    s = s.replace(/\$\{\s*selected\s*:\s*value\s*\}/ig, yValue.fv);
    return s;
};

IDB.GeoMapPoint.prototype.maybeShowLabel = function() {
    var me = this, label = me._label, line = me._line;
    if(!me._labelText || me._labelVisible || !label) {
        return;
    }
    label.visible(true);
    if(line) {
        line.visible(true);
    }
    me._graph._viewer.invalidateStage();

};

IDB.GeoMapPoint.prototype.maybeHideLabel = function() {
    var me = this, label = me._label, line = me._line;
    if(!me._labelText || me._labelVisible || !label) {
        return;
    }
    label.visible(false);
    if(line) {
        line.visible(false);
    }

    me._graph._viewer.invalidateStage();
};

////////////////////////////////////////////////////////////////////////////
// Event handlers
////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
/* jshint unused:true */
IDB.GeoMapPoint.prototype._handleClick = function(evt) {
/* jshint unused:false */
    this._graph._graphContext.onDatapointClicked(this._currDp);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GeoMapPoint.prototype._handleOver = function() {
    this._graph._graphContext.onDatapointOver(this._currDp);
    this._group.moveToTop();
    this.maybeShowLabel();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GeoMapPoint.prototype._handleOut = function() {
    this._graph._graphContext.onDatapointOut(this._currDp);
    this.maybeHideLabel();
};


IDB.GeoMapPoint.initProps = function(newInstance) {
    var props = {
        _graph:null,
        _latLon:null,
        _shapeAxisIds:null,
        _datapointRow:null,
        _hasLabel:true,
        _pointLabelsAlwaysVisible:false,
        _pointLabelTemplate:null,
        _labelTemplate:null,
        _touchHandler:null,
        _currDp:null,
        _viewableYAxes:null,
        _rangeDatapoints:null,
        _usingRangeColors:false
    };
    $.extend(newInstance, props);
};


/*global IDB, $ */
IDB.GeoMapPointConfig = function($xml, valueMap) {
    var me = this, $values, I = IDB, LatLon = I.LatLon,
       get = function(name) {
           return $xml.idbFirstChildText(name);
       },
       num = function(name, defaultVal) {
           if(defaultVal === undefined) {
               defaultVal = NaN;
           }
           return $xml.children(name).idbTextToNumber(defaultVal);
       },
       latlon = get("latlon"), lat = num("lat"), lon = num("lon"), tokens,
       s, pol;
    $values = $xml.children("value");
    for(var j=0, jj=$values.length; j<jj; j++) {
        s = $.trim($($values[j]).text());
        if(!s) continue;
        valueMap[s.toLowerCase()] = me;
    }

    if(latlon) {
        tokens = LatLon.splitGPS(latlon);
        me._lat = tokens[0];
        me._lon = tokens[1];
    }
    else {
        me._lat = lat;
        me._lon = lon;
    }

    me._latLon = new LatLon(me._lat, me._lon);


   Object.preventExtensions(me);
};

IDB.GeoMapPointConfig.prototype.lat = function() {
    return this._lat;
};

IDB.GeoMapPointConfig.prototype.lon = function() {
    return this._lon;
};

IDB.GeoMapPointConfig.prototype.latLon = function() {
    return this._latLon;
};



/*global IDB */
IDB.GeoMapPointLayer = function(layer, viewer, grid, graph) {
    var me = this;
    IDB.LayoutContainer.call(me);
    me._W = me._H = 0;
    me._layer = layer;
    me._grid = grid;
    me._graph = graph;
    me._viewer = viewer;
    me._datapointGrid = null;
    me._displayedPoints = [];

    me._dot = me._dotloc = me._p1 = me._p2 = me._loc1 = me._loc2 = null;

    me._dot = new Kinetic.Circle({
        radius:20,
        fill:"blue",
        x:50, y:50
    });

    me._p1 = new Kinetic.Circle({
        radius:20,
        fill:"red",
        x:50, y:50
    });


    me._p2 = new Kinetic.Circle({
        radius:20,
        fill:"green",
        x:50, y:50
    });


//    layer.add(me._dot);
//    layer.add(me._p1);
//    layer.add(me._p2);
//
//    me._dotloc = new IDB.LatLon(42.59682879609097, IDB.LatLon.HQ._lon);
//    me._loc1 = IDB.LatLon.ORIGIN;
//    me._loc2 = IDB.LatLon.ORIGIN;


   Object.preventExtensions(me);

};
IDB.GeoMapPointLayer.prototype = Object.create(IDB.LayoutContainer.prototype);
IDB.GeoMapPointLayer.prototype.constructor = IDB.GeoMapPointLayer;

IDB.GeoMapPointLayer.prototype.setDotLoc = function(p1, p2, c) {
    this._dotloc = c;
    this._loc1 = p1 || this._loc1;
    this._loc2 = p2 || this._loc2;
    this.doLayout();
//    this.invalidateStage();
};

IDB.GeoMapPointLayer.prototype.setDatapointGrid = function(dpGrid) {
    var me = this, layer = me._layer, points = dpGrid._geoMapPoints;
    me._datapointGrid = dpGrid;
    me._displayedPoints = points.concat();
    for(var j=0, jj=points.length; j<jj; j++) {
        points[j].addToLayer(layer);
    }

    me.invalidateLayout();


};

IDB.GeoMapPointLayer.prototype.resize = function(w,h) {
    var me = this;
    me._W = w;
    me._H = h;
    me.doLayout();
};

IDB.GeoMapPointLayer.prototype.doLayout = function() {
    var me = this, w = me._W, h = me._H, grid = me._grid,
        points = me._displayedPoints || [], point, vis,
        c, p, onGrid;
//        c = me._dotloc, p = grid.getXYFor(c) || {x:0, y:0}, dot = me._dot, onGrid = grid.isOnGrid(c), p1 = me._p1, p2 = me._p2, loc1 = me._loc1, loc2 = me._loc2;


//    IDB.log(c, "onGrid=" + onGrid);
//    dot.visible(true);
//    if(onGrid) {
//        dot.x(p.x);
//        dot.y(p.y);
//    }
//
//    p = grid.getXYFor(loc1);
//    if(p) {
//       p1.x(p.x);
//       p1.y(p.y);
//    }
//
//    p = grid.getXYFor(loc2);
//    if(p) {
//       p2.x(p.x);
//       p2.y(p.y);
//    }


    for(var j=0, jj=points.length; j<jj; j++) {
        point = points[j];
        c = point._latLon;
        vis = grid.isOnGrid(c);
        point.setVisible(vis);
        if(vis) {
            p = grid.getXYFor(c);
            point.move(p.x,  p.y);
        }

        var v = point.getVisible();
        var b = point._kineticShape.visible();
    }

    me.validateLayout();
    me._viewer.invalidateStage();
};


IDB.GeoMapPointLayer.prototype.move = function(x,y) {
    var me = this, layer = me._layer;
    layer.x(100 + x);
    layer.y(100 + y);
    me.invalidateLayout();
};

/*global IDB, Kinetic, $ */

IDB.GeoMapViewer = function(graph, $div) {
    var me = this, I = IDB, $grid, grid, $sdiv, layer, crosshairs, line, $imageLayer;

	IDB.LayoutContainer.call(this);

    me._isEnabled = me._layoutEnabled = false;
    me._redrawFunc = null;
    me._redrawTimeoutID = 0;
    me._initialCenter = null;
    me._graph = graph;
    me._$div = $div;
    me._isDragging = false;
    me._lastW = me._lastH = 0;
    me._mwts = 0;
    me._mwd = 0;
    me._dragTimer = null;
    me._isDragging = false;
    me._dragLastX = me._dragLastY = 0;
    me._dragStartTime = 0;
    me._dragTimerID = 0;
    me._datapointGrid = null;
    me._autoView = me._autoCenterNeeded = graph.gp("AutoView");
    me._firstUpdate = true;
    me._phantomUpdatesPending = false;
    me._clearanceH = me._clearanceW = 6;
    me._touchHandler = new I.DOMTouchHandler($div[0]);

    me._pinchStart = me._lastPinch = null;
    me._contentScale = 1.0;

    $div.on("touchstart", I.listener(me, "_onPinchStart"));
    me._callPinchMove = I.listener(me, "_onPinchMove");
    me._callPinchEnd = I.listener(me, "_onPinchEnd");
    
    $div.idbPosAbs().css({"border":"1px solid #000000", "background-color":"#CCCCFF", "overflow":"hidden"});
    if(!graph._lockZoom) {
        $div.on("mousewheel", I.listener(me, "_onMouseWheel"));
    }

    if(!graph._lockCenter) {
        $div.on("mousedown", I.listener(me, "_onMapMouseDown"));
    }

    me._$grid = $grid = $('<div class="idb-geomap-grid">').appendTo(me._$div);
    me._grid = grid = new I.GeoMapGrid(me, $grid);

    me._$imageLayer = $imageLayer = $("<div class=\"idb-geomap-imagelayer\">").idbPosAbs().appendTo(me._$div);
    me._imageLayer = new I.GeoMapImageLayer($imageLayer, me, grid, graph);

    me._callStartDragging = I.listener(me, "_startDragging");
    me._callDraggingAborted = I.listener(me, "_draggingAborted");
    me._callDoDrag = I.listener(me, "_doDrag");
    me._callStopDragging = I.listener(me, "_stopDragging");

    me._$sdiv = $sdiv = $('<div class="idb-geomap-stage">').idbPosAbs().appendTo(me._$div);
	me._stage = new Kinetic.Stage({
		container: $sdiv[0],
		width: $div.width()+200,
		height: $div.height()+200,
		draggable: false,
		x: 0,
		y: 0,
		offset: [0,0]
	});

	me._stage.visible(false); //becomes visible upon first "invalidateStage()"

	me._stageLayer = new Kinetic.Layer({x:100, y:100});
	me._stage.add(me._stageLayer);

	layer = new Kinetic.Layer({x:100, y:100});
	me._stage.add(layer);
    me._objectLayer = new I.GeoMapObjectLayer(layer, me, grid, graph);

	layer = new Kinetic.Layer({x:100, y:100});
	me._stage.add(layer);
    me._pointLayer = new I.GeoMapPointLayer(layer, me, grid, graph);

	me._topLayer = new Kinetic.Layer({x:100, y:100});
	me._stage.add(me._topLayer);

    me._crosshairs = crosshairs = new Kinetic.Group();

    me._topLayer.add(crosshairs);

    line = new Kinetic.Line({
        x:0, y:0, strokeWidth:1, fillEnabled:false, stroke:'black', points:[-0.5, -16, -0.5, 16]
    });

    crosshairs.add(line);

    line = new Kinetic.Line({
        x:0, y:0, strokeWidth:1, fillEnabled:false, stroke:'black', points:[-16, -0.5, 16, -0.5]
    });

    crosshairs.add(line);

};

IDB.GeoMapViewer.prototype = Object.create(IDB.LayoutContainer.prototype);
IDB.GeoMapViewer.prototype.constructor = IDB.GeoMapViewer;

IDB.GeoMapViewer.prototype.setDatapointGrid = function(dpGrid) {
    var me = this, I = IDB, LatLon = I.LatLon, pointLayer = me._pointLayer, graph = me._graph, gp = graph.gp, autoView = me._autoView,
       gps = gp("InitialCenter") || "0,0", fz = gp("InitialZoom"),
       center = dpGrid.getCenter();
    me._datapointGrid = dpGrid;

    if(center && autoView) {
        me._autoCenterNeeded = true;
    }
    else {
        center = LatLon.parseGPS(gps);
        if(!center._isValid) {
            center = new LatLon(0,0);
        }
        me.setCenter(center);
        me.setFineZoom(fz);
        me._graph.updateZoomButtons();
    }

    pointLayer.setDatapointGrid(dpGrid);
    me.invalidateLayout();
};


IDB.GeoMapViewer.prototype.doAutoCenter = function(w, h) {
    var me = this, I = IDB, LatLon = I.LatLon, origin = LatLon.ORIGIN, C = I.GeoMapViewer,
       dpGrid = me._datapointGrid, center = dpGrid.getCenter(), grid = me._grid,  graph = me._graph,
       tl = dpGrid.getTopLeft() || origin, br = dpGrid.getBottomRight() || origin,
       widthNeededM = br.xM - tl.xM, heightNeededM = br.yM - tl.yM,
       widthShown = false, heightShown = false, fz = 54, z, rem, ts, ms, msP, widthNeededP, heightNeededP,
       max = graph._maxFineZoom, min = graph._minFineZoom, started = false;
    
    //                trace("GeoMapViewer.updateDisplayList(): center=" + center + ", widthNeededM=" + widthNeededM + ", heightNeededM=" + heightNeededM);

//    w *= 0.8;
//    h *= 0.8;

    if(!center) { // no valid data rows.
        IDB.log("No center, returning.");
        return;
    }

//    IDB.log("max=" + max + ", min=" + min);
    
    for(fz=max; (!started || fz>=min); fz--) {
    
        started = true;

        var floor = fz/3;
        z = Math.floor(floor);
        rem = fz % 3;
        z = rem > 1 ? z+1 : z;// z; // (rem > 2) ? z+1 : z;
        ts = rem ? (rem > 1 ? C.T_SMALLER : C.T_BIGGER) : C.T_NORMAL;
    
        ms = LatLon.mapSize(z);
        msP = ms * ts;
    
        widthNeededP = widthNeededM * msP;
        heightNeededP = heightNeededM * msP;
    
        // are we wide enough?
        widthShown = widthShown ||  (widthNeededP < w);
    
        // are we high enough?
        heightShown = heightShown || (heightNeededP < h);
    
    //                    trace("GeoMapViewer.updateDisplayList(): w=" + w + ", h=" + h + ", widthNeededP=" + widthNeededP + ", heightNeededP=" + heightNeededP);
    //                    trace("GeoMapViewer.updateDisplayList(): z=" + z + ", ts=" + ts + ", widthShown=" + widthShown + ", heightShown=" + heightShown + ", center=" + center);
    
        if(widthShown && heightShown) {
            break;
        }
    }

    me.setCenter(center);
    grid.setZoom(z);
    grid.setTileSize(ts);
    me._objectLayer.invalidateLayout();
    me._imageLayer.invalidateLayout();
    me._pointLayer.invalidateLayout();

    me._graph.updateZoomButtons();
};

IDB.GeoMapViewer.prototype.enable = function() {
   var me = this;
   me._isEnabled = me._layoutEnabled = true;
   me._grid.enable();
};

IDB.GeoMapViewer.prototype.setCenter = function(latlon) {
    var me = this, grid = me._grid;
    grid.setCenter(latlon);
};

IDB.GeoMapViewer.T_NORMAL = 256;
IDB.GeoMapViewer.T_BIGGER = 322;
IDB.GeoMapViewer.T_SMALLER = 204;

IDB.GeoMapViewer.prototype.getFineZoom = function() {
    var me = this, I = IDB, C = I.GeoMapViewer, grid = me._grid, zoom = grid._zoom, tileSize = grid._tileSize,
        coarseZoom = zoom * 3;
    if(tileSize === C.T_BIGGER) {
        return coarseZoom + 1;
    }
    if(tileSize === C.T_SMALLER) {
        return coarseZoom - 1;
    }
    return coarseZoom;
};


IDB.GeoMapViewer.MSPFZ = (function() {
    var I = IDB, C = I.GeoMapViewer, T_NORMAL = C.T_NORMAL, T_BIGGER = C.T_BIGGER, T_SMALLER = C.T_SMALLER,
        z, zoom, rem, ms, msP, tileSize, j, arr = [];

    for(j=0; j<=54; j++) {
        z = Math.floor(j/3);
        rem = j % 3;
        zoom = rem > 1 ? z+1 : z;
        tileSize = rem ? (rem > 1 ? T_SMALLER : T_BIGGER) : T_NORMAL;
        ms = Math.pow(2, zoom);
        msP = ms * tileSize;
        arr.push(msP);
    }
    return arr;
})();

IDB.GeoMapViewer.getFineZoomForScale = function(oldFz, scale) {
    var I = IDB, GMV = I.GeoMapViewer, MSPFZ = GMV.MSPFZ, oldMsP = MSPFZ[oldFz], newMsP = oldMsP * scale,
        below = 0, above = 0, belowMsP, aboveMsP, j, cur, newFz;

    for(j=0; j<=54; j++) {
        above = j;
        cur = MSPFZ[j];
        if(newMsP > cur) {
            below = j;
        }
        else {
            break;
        }
    }

    belowMsP = MSPFZ[below];
    aboveMsP = MSPFZ[above];

    if(aboveMsP <= newMsP) {
        newFz = above;
    }
    else if(belowMsP >= newMsP) {
        newFz = below;
    }
    else if((aboveMsP - newMsP) <= (newMsP - belowMsP)) {
        newFz = above;
    }
    else {
        newFz = below;
    }

    return newFz;
};

IDB.GeoMapViewer.prototype.setFineZoom = function($fineZoom) {
    var me = this, I = IDB, C = I.GeoMapViewer, old = me.getFineZoom(), grid = me._grid,
        fz = Math.max(0, Math.min(54, $fineZoom)),
        z = Math.floor(fz / 3), rem = fz % 3,
        zoom = rem > 1 ? z+1 : z,
        tileSize = rem ? (rem > 1 ? C.T_SMALLER : C.T_BIGGER) : C.T_NORMAL;
    if(fz === old) {
        I.log("Fine zoom - no change: " + fz);
        return;
    }
    grid.setTileSize(tileSize);
    grid.setZoom(zoom);
    me._objectLayer.invalidateLayout();
    me._imageLayer.invalidateLayout();
    me._pointLayer.invalidateLayout();
    me._graph.updateZoomButtons();
};

IDB.GeoMapViewer.prototype.resize = function(w,h) {
    var me = this;
    me._lastW = w;
    me._lastH = h;
    me.invalidateLayout();
};

IDB.GeoMapViewer.prototype.doLayout = function() {
    var me = this, w = me._lastW, h = me._lastH, $div = me._$div, $sdiv = me._$sdiv, grid = me._grid, stage = me._stage, ch = me._crosshairs,
        oLayer = me._objectLayer, iLayer = me._imageLayer, pLayer = me._pointLayer, clearanceW = me._clearanceW, clearanceH = me._clearanceH, autoView = me._autoView;

    if(!w || !h) {
        return;
    }

    if(me._phantomUpdatePending) {
        if(autoView) {
            me._autoCenterNeeded = true;
        }
    }
    else if(me._firstUpdate) {
        me._firstUpdate = false;
        if(autoView) {
            me._phantomUpdatePending = true;
            setTimeout(function() {
                me._phantomUpdatePending = false;
            }, 500);
        }
    }

    
    $div.width(w-2).height(h-2);
    grid.move(0,0);
    grid.resize(w, h);
    me._$grid.css({"transform":"none", "top":"0px", "left":"0px"});
    $sdiv.css({"transform":"none", "top":"-100px", "left":"-100px", "width":(w+200) + "px", "height":(h+200) + "px"});


    if(me._autoCenterNeeded) {
        me.doAutoCenter(w-(2*clearanceW),h-(2*clearanceH));
        me._autoCenterNeeded = false;
    }


    grid.doLayout();

    iLayer.move(0,0);
    iLayer.resize(w,h);
    iLayer.doLayout();

	stage.width(w+200);
	stage.height(h+200);
	stage.position({ x:0, y:0});

    oLayer.move(0,0);
    oLayer.resize(w,h);
    oLayer.doLayout();

    pLayer.move(0,0);
    pLayer.resize(w,h);
    pLayer.doLayout();

    ch.x(w/2);
    ch.y(h/2);

    me.validateLayout();
    me.invalidateStage();
};

IDB.GeoMapViewer.prototype._onMouseWheel = function(evt) {
    var me = this, I = IDB, LatLon = I.LatLon, oldFz = me.getFineZoom(), ts = evt.timeStamp, et = (ts - me._mwts), deltaY = evt.deltaY,
    dir = (deltaY > 0 ? 1 : -1), dirChange = !me._mwd || (dir != me._mwd),
    newFz = me._graph.validZoom(oldFz + deltaY), offsetX = evt.offsetX, offsetY = evt.offsetY, lockCenter = me._graph._lockCenter,
        grid = me._grid, gridX = grid._gridX, gridY = grid._gridY, lastW = me._lastW, lastH = me._lastH, newZoom, newTileSize,
        latLon, centerX, centerY, xOffset, yOffset, msP, xM, yM, newCenter;

    if(!me._isEnabled) {
        return;
    }
    evt.preventDefault();

    // detect weird behavior when rotating mouswheel rapidly.
    if(et < 100 && dirChange) {
        return;
    }

    me._mwts = ts;
    me._mwd = dir;

    if((oldFz === newFz) || me._isDragging) {
        return;
    }
    
    me.setFineZoom(newFz);
    newZoom = grid.getZoom();
    newTileSize = grid.getTileSize();

    if(!lockCenter) {
    
        // Get the latLon of the point where the mouse was in the viewer's local coordinate system.
        latLon = grid.getCoordsAt(offsetX - gridX,  offsetY - gridY);
    
        if(latLon._isValid && !evt.shiftKey) {
    
            centerX = lastW / 2;
            centerY = lastH / 2;
    
            // Where was the mouse relative to the center?
            xOffset = offsetX - centerX;
            yOffset = offsetY - centerY;
    
            // Get the overall map size in pixels for the new fineZoom.
            msP = LatLon.mapSize(newZoom) * newTileSize;
    
            // What will the map units be for the center at the new fineZoom?
            xM = latLon.xM  - (xOffset / msP);
            yM = latLon.yM - (yOffset / msP);
    
            newCenter = new LatLon(LatLon.latFromMaps(yM), LatLon.lonFromMaps(xM));
            grid.setCenter(newCenter);
        }
    }

    // update everything immediately.
    grid.doLayout();
    me._objectLayer.doLayout();
    me._imageLayer.doLayout();
    me._pointLayer.doLayout();
    me.redrawStage();
};

/////////////////////////////////////////////////////////////
// Dragging

IDB.GeoMapViewer.prototype._onMapMouseDown = function(e) {
    var me = this, lockCenter = me._graph._lockCenter;
    if(!me._isEnabled || lockCenter) {
        return;
    }
    me._dragLastX = e.pageX;
    me._dragLastY = e.pageY;
    me._dragStartTime = e.timeStamp;
    me._maybeStartDragging();
};

IDB.GeoMapViewer.prototype._maybeStartDragging = function() {
    var me = this, $div = me._$div;

    me._startTimer();
    // Don't start actual dragging until they move the mouse, so we
    // can handle simple clicks separately.
    $div.on("mousemove", me._callStartDragging);
    $div.on("mouseup", me._callDraggingAborted);
};

IDB.GeoMapViewer.prototype._startTimer = function() {
    var me = this;
    me._stopTimer(); // don't allow multiple callbacks to be queued at once.
    me._dragTimerID = setTimeout(function timerHandler() {
        me._showDragStuff(true);
    }, 500);

};

IDB.GeoMapViewer.prototype._stopTimer = function() {
    var me = this, timerId = me._dragTimerID;
    if(timerId) {
        clearTimeout(timerId);
    }
    me._dragTimerID = 0;
};


IDB.GeoMapViewer.prototype._blockTimer = function() {
    setTimeout(IDB.listener(this, "_stopTimer"), 100);
};

IDB.GeoMapViewer.prototype._showDragStuff = function(b) {
    var me = this, $div = me._$div, curs = (b ? "all-scroll" : "inherit");
    me._stopTimer();
    $div.css({"cursor":curs});
    me._objectLayer.setCrosshairsVisible(b);
    me._crosshairs.visible(b);
};


IDB.GeoMapViewer.prototype.setCrosshairsVisible = function(b) {
    var me = this, ch = me._crosshairs, dragging = me._isDragging;
    if(dragging) {
        return;
    }
    ch.visible(b);
    me.invalidateStage();
};

IDB.GeoMapViewer.prototype._startDragging = function(e) {
    var me = this, $div = me._$div, $win = $(window), elapsed = e.timeStamp - me._dragStartTime;

    // Chrome dispatches a spurious mousemove event immediately after the mousedown.
    // This will ignore it.
    if(elapsed < 15) {
        return;
    }

    $div.off("mousemove", me._callStartDragging);
    $div.off("mouseup", me._callDraggingAborted);
    me._isDragging = true;
    me._showDragStuff(true);
    me._doDrag(e);
    $win.on("mousemove", me._callDoDrag);
    $win.on("mouseup", me._callStopDragging);
};

IDB.GeoMapViewer.prototype._draggingAborted = function(evt) {
    var me = this, $div = me._$div;
    $div.off("mousemove", me._callStartDragging);
    $div.off("mouseup", me._callDraggingAborted);

    me._showDragStuff(false);
    me._isDragging = false;
};

IDB.GeoMapViewer.prototype._stopDragging = function(_) {
    var me = this, $win = $(window), lastW = me._lastW, lastH = me._lastH, grid = me._grid,
       gridPos = grid.position(), gridPosX = gridPos.left, gridPosY = gridPos.top, centerX, centerY, coord;
    me._isDragging = false;
    me._showDragStuff(false);

    $win.off("mousemove", me._callDoDrag);
    $win.off("mouseup", me._callStopDragging);

    // find the centerpoint
    centerX = lastW / 2;
    centerY = lastH / 2;
    coord = grid.getCoordsAt(centerX - gridPosX - grid._gridX, centerY - gridPosY - grid._gridY);
    grid.setCenter( coord );
    me.invalidateLayout();
};

IDB.GeoMapViewer.prototype._doDrag = function(evt) {
    var me = this, _dragLastX = me._dragLastX, _dragLastY = me._dragLastY,
        pageX = evt.pageX, pageY = evt.pageY,
        xDelta = pageX - _dragLastX, yDelta = pageY - _dragLastY,
        _grid = me._grid,
        gridPos = _grid.position(),
        vX = gridPos.left, vNewX = vX + xDelta,
        vY = gridPos.top, vNewY = vY + yDelta, needed = 0,
        gridX = _grid._gridX, gridY = _grid._gridY,
        lastW = me._lastW, lastH = me._lastH, oLayer = me._objectLayer, iLayer = me._imageLayer, pLayer = me._pointLayer;

    // vX and vY are relative to 0,0.
    // Do we have the tiles we need to the left?
    needed = vX + gridX;  // pixels needed.
    if(needed > -15) {
        _grid.ext("L", needed);
    }
    // Do we have the tiles we need at the top?
    needed = vY + gridY;
    if(needed > -15) {
        _grid.ext("U", needed);
    }

    // right?
    needed = lastW - (vX + gridX + _grid._gridW);
    if(needed > -15) {
        _grid.ext("R", needed);
    }

    // down?
    needed = lastH - (vY + gridY + _grid._gridH);
    if(needed > -15) {
        _grid.ext("D", needed);
    }

    _grid.move(vNewX, vNewY);
    pLayer.move(vNewX, vNewY);
    pLayer.doLayout();
    oLayer.move(vNewX, vNewY);
    oLayer.doLayout();
    iLayer.move(vNewX, vNewY);
    iLayer.doLayout();
    me.redrawStage(); // must be immediate for smooth scrolling along with the map.
    me._dragLastX = pageX;
    me._dragLastY = pageY;
};

////////////////////////////////////////////////////////////////////////////
// Pinch/Zoom

IDB.GeoMapViewer.prototype._onPinchStart = function(evt) {
    var me = this, e = evt.originalEvent, touches = e.touches, grid = me._grid, $div = me._$div, mp, center, p1, p2;

    if(touches.length < 2) {
        return;
    }

//    IDB.log(evt, "\n\n\n\n\n\n\nPINCH START: " + me.segLength(touches));

    mp = me.midpoint(touches);

    center = mp;
    me._pinchStart = {
        segLength:me.segLength(touches),
        midpoint:center,
        latLon:grid.getCoordsAt2(center.x,  center.y)
    };

    p1 = me.toLocal(touches[0]);
    p2 = me.toLocal(touches[1]);

    me._pointLayer.setDotLoc(grid.getCoordsAt2(p1.x, p1.y),
                             grid.getCoordsAt2(p2.x, p2.y), me._pinchStart.latLon);

    me._lastPinch = touches;
    
    $div.off("touchmove", me._callPinchMove);
    $div.off("touchend", me._callPinchEnd);
    $div.on("touchmove", me._callPinchMove);
    $div.on("touchend", me._callPinchEnd);

    evt.preventDefault();
};

IDB.GeoMapViewer.prototype.toLocal = function(touch) {
    var me = this, $div = me._$div;
    return $div.idbGlobalToLocal(touch.clientX, touch.clientY);
};

IDB.GeoMapViewer.prototype.segLength = function(touches) {
   var t0 = touches[0], t1 = touches[1], x0=t0.screenX, y0 = t0.screenY, x1 = t1.screenX, y1 = t1.screenY,
      rise = Math.abs(y1 - y0), run = Math.abs(x1 - x0);

   return Math.sqrt((rise*rise) + (run*run));
};

IDB.GeoMapViewer.prototype.midpoint = function(touches) {
   var t0 = touches[0], t1 = touches[1], x0=t0.clientX, y0 = t0.clientY, x1 = t1.clientX, y1 = t1.clientY;
   return this._$div.idbGlobalToLocal((x0+x1)/2, (y0+y1)/2);
};



IDB.GeoMapViewer.prototype._onPinchMove = function(evt) {

    var me = this, e = evt.originalEvent, touches = e.touches, ps = me._pinchStart, center = ps.midpoint, startLength = ps.segLength;
    if(touches.length <= 1) {
        return;
    }
    
    var now = me.segLength(touches),
        ratio = now/startLength,
        CX = (me._lastW/2), CY = (me._lastH/2),
        scale = "scale(" + ratio + ", " + ratio + ")",
        oX = center.x - CX,
        oY = center.y - CY,
        oXs = ratio * oX,
        oYs = ratio * oY,
        SX = center.x - oXs - CX,
        SY = center.y - oYs - CY;


    me._$grid.css({"transform":scale, left:SX + "px", top:SY + "px"});
    me._$sdiv.css({"transform":scale, left:(SX-100) + "px", top:(SY-100) + "px"});
    me._contentScale = ratio;

   evt.preventDefault();
};

IDB.GeoMapViewer.prototype._onPinchEnd = function(evt) {
    var me = this, I = IDB, LatLon = I.LatLon, C = I.GeoMapViewer, $div = me._$div, grid = me._grid, graph = me._graph,
        scale = me._contentScale,
        oldFz = me.getFineZoom(),
        newFz = C.getFineZoomForScale(oldFz, scale),
        lastW = me._lastW, lastH = me._lastH,
        centerX = lastW/2,
        centerY = lastH/2,
        ps = me._pinchStart,
        latLon = ps.latLon,
        mp = ps.midpoint,
        offsetX = mp.x, offsetY = mp.y,
        xOffset = offsetX - centerX,
        yOffset = offsetY - centerY,
        newZoom, newTileSize, msP, xM, yM, newCenter;


//    IDB.log(touches, "PINCH END: " + scale + ", xOffset = " + xOffset + ", yOffset=" + yOffset);

    $div.off("touchmove", me._callPinchMove);
    $div.off("touchend", me._callPinchEnd);

    me._contentScale = 1.0;
    me._pinchStart = null;
    me._lastPinch = null;

    me.invalidateLayout();

    newFz = graph.validZoom(newFz);

    me.setFineZoom(newFz);
    newZoom = grid.getZoom();
    newTileSize = grid.getTileSize();


    if(latLon._isValid) {

        // Get the overall map size in pixels for the new fineZoom.
        msP = LatLon.mapSize(newZoom) * newTileSize;

        // What will the map units be for the center at the new fineZoom?
        xM = latLon.xM  - (xOffset / msP);
        yM = latLon.yM - (yOffset / msP);

        newCenter = new LatLon(LatLon.latFromMaps(yM), LatLon.lonFromMaps(xM));
        if(newCenter._isValid) {
            grid.setCenter(newCenter);
        }
     }
    evt.preventDefault();
};




////////////////////////////////////////////////////////////////////////////
// Stage validation

IDB.GeoMapViewer.prototype.invalidateStage = function() {

    var me = this;

	if ( me._redrawFunc ) {
		return;
	}

	me._redrawFunc = function() {
		//console.log('GeoMapViewer._redrawFunc: ');
        me._redrawTimeoutID = 0;
		//var t0 = window.performance.now();
        me.redrawStage();
		//var t1 = window.performance.now();
		//me._redrawLabel.html('draw: '+(t1-t0)+'ms'); //TEMP
		//console.log('draw: '+(t1-t0)+'ms');
	};

	//console.log('GraphBaseKinetic.invalidateStage: set');
	me._redrawTimeoutID = setTimeout(me._redrawFunc, 0);
};

IDB.GeoMapViewer.prototype.redrawStage = function() {
    var me = this, stage = me._stage, timeoutID = me._redrawTimeoutID;
    stage.visible(true);
    stage.draw();
    if(timeoutID) {
        clearTimeout(timeoutID);
    }
    me._redrawTimeoutID = 0;
    me._redrawFunc = null;
};







/*global IDB, $ */
IDB.LatLon = function($lat, $lon) {
    var me = this, I = IDB, F = I.LatLon, log = I.log,
       lat = me._lat = F.normalizeLat($lat),
       lon = me._lon = F.normalizeLon($lon),
       isValid = me._isValid = (lat === $lat && lon === $lon);

    me._xM = F.lonToMaps(lon);
    me._yM = F.latToMaps(lat);

    me._inits = [];
    me._xTList = [];
    me._yTList = [];
    me._tXList = [];
    me._tYList = [];
    me._pXList = [];



    if(!isValid) {
       log("LatLon.LatLon(): WARNING: Invalid params: $lat=" + $lat + ", $lon=" + $lon);
    }


   Object.preventExtensions(me);
};

IDB.LatLon.prototype.init = function(z) {
    var me = this, I = IDB, F = I.LatLon, ms, yT, xT;
    z = F.normalizeZoom(z);
    if(me._inits[z]) {
        return z;
    }
    me._inits[z] = true;
    ms = F._mapSizes[z];
    yT = me._yM * ms;
    xT = me._xM * ms;
    me._yTList[z] = yT;
    me._xTList[z] = xT;
    me._tYList[z] = Math.floor(yT);
    me._tXList[z] = Math.floor(xT);
    return z;
};


/**
 * The X index of the map tile that contains this point, at the given
 * major zoom level. Equal to Math.floor(this.xT(z)).
 */
IDB.LatLon.prototype.tx = function(z) {
    return this._tXList[this.init(z)];
};

/**
 * The Y index of the map tile that contains this point, at the given
 * major zoom level. Equal to Math.floor(this.yT(z)).
 */
IDB.LatLon.prototype.ty = function(z) {
    return this._tYList[this.init(z)];
};

/**
* The x coordinate of this point on the map, expressed as a number of tiles. This
* will be a floating point number between 0 and (Map Size in Tiles). It may have
* a fractional part.
*/
IDB.LatLon.prototype.xT = function(z) {
    return this._xTList[this.init(z)];
};


/**
 * The y coordinate of this point on the map, expressed as a number of tiles. This
 * will be a floating point number between 0 and (Map Size in Tiles). It may have
 * a fractional part.
 */
IDB.LatLon.prototype.yT = function(z) {
    return this._yTList[this.init(z)];
};

/**
 * The x coordinate of this point on the map, expressed in pixels, for the given
 * major zoom level (z) and tile size (T). Equal to this.xT(z) * T.
 */
IDB.LatLon.prototype.xP = function(z, T) {
    return this.xT(z) * T;
};

/**
 * The y coordinate of this point on the map, expressed in pixels, for the given
 * major zoom level (z) and tile size (T). Equal to this.yT(z) * T.
 */
IDB.LatLon.prototype.yP = function(z, T) {
    return this.yT(z) * T;
};

Object.defineProperties(IDB.LatLon.prototype, {
    xM:{
        get:function() {
            return (this._xM);
        }
    },
    yM:{
        get:function() {
            return (this._yM);
        }
    }
});

IDB.LatLon.prototype.toGPS = function(decimalPlaces) {
    var me = this, _lat = me._lat, _lon = me._lon;
    decimalPlaces = IDB.ifUndef(decimalPlaces, -1);
    if(decimalPlaces < 0) {
        return _lat + "," + _lon;
    }
    return _lat.toFixed(decimalPlaces) + ", " + _lon.toFixed(decimalPlaces);
};

IDB.LatLon.prototype.toString = function() {
    var me = this;
    return "LatLon[lat=" + me._lat + ", lon=" + me._lon + ", xM=" + me._xM + ", yM=" + me.yM + ", _isValid=" + me._isValid + "]";
};


IDB.LatLon.MAX_LAT = 85.0511287798066;
IDB.LatLon.MIN_LAT = -85.0511287798066;
IDB.LatLon.MAX_LON = 180;
IDB.LatLon.MIN_LON = -180;
IDB.LatLon.MIN_ZOOM = 0;
IDB.LatLon.MAX_ZOOM = 18;

IDB.LatLon._mapSizes =  (function() {
    var arr = [], n=1, j=0;
    for(j=0; j<20; j++) {
        n = Math.pow(2,j);
        arr.push(n);
    }
    return arr;

})();

IDB.LatLon.mapSize = function(z) {
    return IDB.LatLon._mapSizes[z];
};

IDB.LatLon.normalizeLat = function(lat) {
    var I = IDB, F = I.LatLon, log = I.log;
    if(isNaN(lat) || lat > F.MAX_LAT) {
        log("LatLon.normalizeLat(): Invalid lat: " + lat);
        return F.MAX_LAT;
    }
    else if(lat < F.MIN_LAT) {
        log("LatLon.normalizeLat(): Invalid lat: " + lat);
        return F.MIN_LAT;
    }
    return lat;
};

IDB.LatLon.normalizeLon = function(lon) {
    var I = IDB, F = I.LatLon, log = I.log;
    if(isNaN(lon) || lon > F.MAX_LON) {
        log("LatLon.normalizeLon(): Invalid lon: " + lon);
        return F.MAX_LON;
    }
    else if(lon < F.MIN_LON) {
        log("LatLon.normalizeLon(): Invalid lon: " + lon);
        return F.MIN_LON;
    }
    return lon;
};

IDB.LatLon.normalizeZoom = function(z) {
    var I = IDB, F = IDB.LatLon, log = I.log;
    if(isNaN(z) || z < F.MIN_ZOOM) {
        log("LatLon.normalizeZoom(): ERROR: Invalid zoom: " + z);
        return F.MIN_ZOOM;
    }
    else if(z > F.MAX_ZOOM) {
        log("LatLon.normalizeZoom(): ERROR: Invalid zoom: " + z);
        return F.MAX_ZOOM;
    }
    if(Math.floor(z) != z) {
        log("LatLon.normalizeZoom(): ERROR: Invalid zoom: " + z);
        return Math.floor(z);
    }
    return z;
};


/**
 * The given latitude is normalized so MIN_LAT <= lat <= MAX_LAT, and then converted to a y coordinate
 * expressed as the fraction of the whole map's height, from the top edge of the map. This value
 * is the same regardless of the zoom level or tile size.
 */
IDB.LatLon.latToMaps = function(lat) {
    var I = IDB, F = I.LatLon, sinLat = Math.sin(F.normalizeLat(lat)*Math.PI/180);
    return Math.max(0, Math.min(1.0, (0.5 - Math.log((1+sinLat)/(1-sinLat)) / (4*Math.PI))));
};

/**
 * The given longitude is normalized so -180 <= lat <= 180, and then converted to a x coordinate
 * expressed as the fraction of the whole map's height, from the top edge of the map. This value
 * is the same regardless of the zoom level or tile size.
 */
IDB.LatLon.lonToMaps = function(lon) {
    var I=IDB, F=I.LatLon;
    return((F.normalizeLon(lon)+180)/360);
};


IDB.LatLon.latFromMaps = function(maps) {
    var n = Math.PI - (2 * Math.PI * maps);
    return (180/Math.PI*Math.atan(0.5*(Math.exp(n)-Math.exp(-n))));
};

IDB.LatLon.lonFromMaps = function(maps) {
    return ((maps*360) - 180);
};

IDB.LatLon.strToNum = function(s) {
    s = s || "";
    if(s.length === 0) {
        return NaN;
    }
    return Number(s);
};

IDB.LatLon.splitGPS = function(gps) {
    var I = IDB, F = I.LatLon, strToNum = F.strToNum, tokens;
    gps = $.trim(gps);
    if(!gps) {
        return [NaN, NaN];
    }
    tokens = gps.split(/\s*,\s*/);
    if(tokens.length != 2) {
        I.log(tokens,  "INVALID GPS: " + gps);
        return [NaN, NaN];
    }
    
    return [strToNum(tokens[0]), strToNum(tokens[1])];
};

IDB.LatLon.parseGPS = function(gps) {
    var tokens, I = IDB, F = I.LatLon, splitGPS = F.splitGPS;
    if(!gps) {
        return new F(0,0);
    }
    tokens = splitGPS(gps);
    return new F(tokens[0], tokens[1]);
};







// End of class definition
IDB.LatLon.HQ = new IDB.LatLon(42.596931045719444,-83.1670524040222);
IDB.LatLon.ORIGIN = new IDB.LatLon(0,0);

//IDB.log(IDB.LatLon.HQ,  "HQ--------------" + IDB.LatLon.HQ.xP(10, 256));
//var L = IDB.LatLon.parseGPS("42.596931045719444,-83.1670524040222");
//IDB.log(L, "COPY");


/*global IDB */
IDB.MGModel = function(vW, vH, center, $zoom, $tileSize) {

    var me = this, I = IDB, LatLon = I.LatLon, ms = LatLon.mapSize($zoom),
        hVW = vW / 2, hVH = vH / 2,
        mapOffsetX = hVW - center.xP($zoom, $tileSize),
        mapOffsetY = hVH - center.yP($zoom, $tileSize),
        firstTileTX = mapOffsetX < 0 ? (Math.floor(Math.abs(mapOffsetX/$tileSize))) : 0,
        firstTileTY = mapOffsetY < 0 ? (Math.floor(Math.abs(mapOffsetY/$tileSize))) : 0,
        firstTileX = mapOffsetX + (firstTileTX*$tileSize),
        firstTileY = mapOffsetY + (firstTileTY*$tileSize),
        tilesWide = Math.min(ms-firstTileTX, Math.ceil((vW - firstTileX)/$tileSize)),
        tilesHigh = Math.min(ms-firstTileTY, Math.ceil((vH-firstTileY)/$tileSize));


//    IDB.log("MGModel.MGModel():  center=" + center + ", $zoom=" + $zoom + ", $tileSize=" + $tileSize + ", mapOffsetX=" + mapOffsetX + ", mapOffsetY=" + mapOffsetY +
//        ", firstTileTX=" + firstTileTX +  ", firstTileTY=" + firstTileTY + ", firstTileX=" + firstTileX + ", firstTileY=" + firstTileY + ", tilesWide=" + tilesWide +
//        ", tilesHigh=" + tilesHigh + ", ms=" + ms);

    me._ms = ms;
    me._msP = ms * $tileSize;
    me._zoom = $zoom;
    me._tileSize = $tileSize;
    me._mapX = mapOffsetX;
    me._mapY = mapOffsetY;
    me._gridX = firstTileX;
    me._gridY = firstTileY;
    me._gridTX = firstTileTX;
    me._gridTY = firstTileTY;
    me._gridWT = tilesWide;
    me._gridHT = tilesHigh;

    me._gridTopM = Math.max(0, (firstTileTY / ms));
    me._gridBottomM = Math.min(1.0, ((firstTileTY + tilesHigh)/ms));

    me._gridLeftM = Math.max(0, (firstTileTX/ms));
    me._gridRightM = Math.min(1.0, ((firstTileTX + tilesWide)/ms));

   Object.preventExtensions(me);
};


/**
 * Extends the grid in a given direction ('U', 'D', 'L', or 'R') by n strips. A strip is a row of tiles if the direction
 * is 'U' or 'D', and a column of tiles if the direction is 'L' or 'R'.
 */
IDB.MGModel.prototype.ext = function(dir, n) {

    var me = this, T = me._tileSize, ms = me._ms, added = 0;

    if(dir === 'U') {
        added = Math.min(n, me._gridTY);
        me._gridTY -= added;
        me._gridY -= (added * T);
        me._gridHT += added;
        me._gridTopM = Math.max(0, me._gridTY/ms);
        return added;
    }
    else if(dir === 'D') {
        added = Math.min(n, ms - (me._gridTY + me._gridHT));
        me._gridHT += added;
        me._gridBottomM = Math.min(1.0, (me._gridTY + me._gridHT)/ms);
        return added;
    }
    if(dir === 'L') {
        added = Math.min(n, me._gridTX);
        me._gridTX -= added;
        me._gridX -= (added * T);
        me._gridWT += added;
        me._gridLeftM = Math.max(0, me._gridTX/ms);
        return added;
    }
    else if(dir === 'R') {
        added = Math.min(n, ms - (me._gridTX + me._gridWT));
        me._gridWT += added;
        me._gridRightM = Math.min(1.0, (me._gridTX + me._gridWT)/ms);
        return added;
    }
    IDB.log("MGModel.ext(): ERROR: Invalid dir parameter: " + dir);
    return 0;
};

IDB.MGModel.prototype.isOnGrid = function(c, loose) {
    if(!c) return false;
   
    var me = this, xM = c.xM, yM = c.yM,
        onGrid = (xM >= me._gridLeftM && xM <= me._gridRightM &&
        yM >= me._gridTopM && yM <= me._gridBottomM);
//  trace("MGModel.isOnGrid(): xM=" + xM + ", yM=" + yM + ", me._gridLeftM=" + me._gridLeftM + ", me._gridRightM=" + me._gridRightM);

    if(loose && !onGrid) {
        if(yM > me._gridBottomM || xM > me._gridRightM) {
            return false;
        }

        var missLeft = (me._gridLeftM - xM) * me._msP, missTop = (me._gridTopM - yM) * me._msP;

        if(missLeft <= 1 && missTop <= 1) {
            onGrid = true;
            IDB.log("\n====================================\nmissLeft=" + missLeft + ", missTop=" + missTop);
        }
    }
    return onGrid;
};

Object.defineProperties(IDB.MGModel.prototype, {
    gridTX:{
        get:function() {
            return (this._gridTX);
        }
    },
    gridTY:{
        get:function() {
            return (this._gridTY);
        }
    },
    gridWT:{
        get:function() {
            return (this._gridWT);
        }
    },
    gridHT:{
        get:function() {
            return (this._gridHT);
        }
    },
    gridX:{
        get:function() {
            return (this._gridX);
        }
    },
    gridY:{
        get:function() {
            return (this._gridY);
        }
    },
    gridTopM:{
        get:function() {
            return (this._gridTopM);
        }
    },
    gridBottomM:{
        get:function() {
            return (this._gridBottomM);
        }
    },
    gridLeftM:{
        get:function() {
            return (this._gridLeftM);
        }
    },
    gridRightM:{
        get:function() {
            return (this._gridRightM);
        }
    },
    tileSize:{
        get:function() {
            return (this._tileSize);
        }
    },
    mapX:{
        get:function() {
            return (this._mapX);
        }
    },
    mapY:{
        get:function() {
            return (this._mapY);
        }
    },
    msP:{
        get:function() {
            return (this._msP);
        }
    },
    tilesWide:{
        get:function() {
            return (this._gridWT);
        }
    },
    tilesHigh:{
        get:function() {
            return (this._gridHT);
        }
    },
    zoom:{
        get:function() {
            return (this._zoom);
        }
    }
});

/*global IDB, $ */
IDB.MapTile = function(grid, z, tx, ty, tileUrl) {
    var me = this, url = me._url = tileUrl,
        $img = $("<img class=\"idb-maptile\" draggable=\"false\">").idbPosAbs().on( 'dragstart', function() { return false; } ).one("error", IDB.listener(me, "_onError"));

    me._grid = grid;
    me._$element = $img;
    me._tx = tx;
    me._ty = ty;
    me._displayed = false;
    me._isBroken = false;
    $img.attr("src", url);
   Object.preventExtensions(me);
};

Object.defineProperties(IDB.MapTile.prototype, {
    displayed:{
        get:function() {
            return (this._displayed);
        },
        set:function(b) {
            var me = this;
            b = !!b;
            me._displayed = b;
        }
    }
});

IDB.MapTile.prototype.moveAndSize = function(x,y, T) {
   var me = this, $img = me._$element;
   $img.idbXYWH({x:x,y:y,w:T,h:T});
};

IDB.MapTile.prototype.detach = function() {
    var me = this, $img = me._$element;
    $img.detach();
};

IDB.MapTile.prototype._onError = function(evt) {
    this._isBroken = true;
};


/*global IDB, Kinetic */

