'use strict';
IDB.ErrorGraph = function(chart, $div, dataSet, errMsg, textRgb) {
    var me = this, info, I = IDB, LS = I.LS;
	me._$div = $div;
	me._chart = chart;
    me._dataSet = dataSet;
    me._errMsg = errMsg;
	info = me._info = new IDB.GraphInfo();
    info.calloutStyle = "none";
    info.legendStyle = LS.NONE;

	me._build((errMsg || ""), (textRgb || IDB.rgb(0)));

	me.resize();
};


IDB.ErrorGraph.prototype._build = function(errMsg, textRgb) {
    this._$errDiv = $("<div>").css({"color":textRgb.css, "overflow":"hidden", "height":"100%", "width":"100%"}).appendTo(this._$div);
    this._$textSpan = $("<span>").css({"word-wrap":"break-word"}).appendTo(this._$errDiv).html(errMsg.replace(/\n/g, "<br/>"));
};



////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.getDataSet = function() {
    return this._dataSet;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.getInfo = function() {
    return this._info;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.resize = function() {
   this._$textSpan.position({my:"center", at:"center", of:this._$errDiv}); 
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.destroy = function() {
    IDB.log("ErrorGraph.destroy()", "------------------------------------------------------");
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.highlightMatchingDatapoints = function(value, matchType, fadeOthers) {
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.highlightDatapoint = function(dp, matchType, fadeOthers) {

};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.normalizeAllDatapoints = function() {
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.width = function() {
    return this._$div.width();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.height = function() {
    return this._$div.height();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.datapointHighlighted = function(dp, previousState, matchType) {
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.datapointFaded = function(dp, previousState, matchType) {
};
	
/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.datapointUnhighlighted = function(dp) {
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.datapointUnfaded = function(dp) {
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.datapointUpdatesCompleted = function() {
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.getAbsMinW = function() {
    return 20;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ErrorGraph.prototype.getAbsMinH = function() {
    return 20;
};

/*global IDB, $, Kinetic */


