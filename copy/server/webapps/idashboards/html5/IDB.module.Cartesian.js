'use strict';
IDB.CartesianGrid = function(graphContext) {
	var sett = graphContext.settings;
	var is3D = (sett.get('GraphTypeCode').indexOf('3d') > 0);
	sett.set('Is3dGraph', is3D);

	this._axes = new IDB.CartesianGridAxes(graphContext);
	
	//LATER: this._drag = new IDB.CartesianDrag(...)
};

IDB.CartesianGrid.ShowNoYAxis = -1;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGrid.prototype.getAxes = function() {
	return this._axes;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGrid.prototype.getStartIndex = function() {
	return 0; //LATER: (this._drag ? this._drage.getStartIndex() : 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGrid.prototype.setStartIndex = function(index) {
	this._axes.setStartIndex(index);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGrid.prototype.positionDatapointMatches = function(matches, matchType) {
	//LATER: CartesianGrid.positionDatapointMatches() (needed for window-dragging)
};

/*====================================================================================================*/
IDB.CartesianGridAxes = function(graphContext) {
	this._graphContext = graphContext;
	this._graphContext.addDatapointsUpdatedListener(IDB.listener(this, '_onDatapointsUpdated'));

	var dpGrid = graphContext.datapointGrid;
	var sett = graphContext.settings;
	var is3D = sett.getBool('Is3dGraph');
	
	this._numXRows = dpGrid.getNumXRows();
	this._numYAxes = dpGrid.getNumYCols(true);
	
	this._onActiveYAxisChange = null;
	this._xStartIndex = (is3D ? 0 : sett.get('HiddenStartXIndex'));
	this._currYAxisId = 0;
	this._defaultRangeList = dpGrid.getXAxis().getRangeList();

	this._yAxisLabelText = 
		(!is3D && this.getYAxisLabelOptions().show ? sett.get('YAxisLabelText') : null);
	this._xAxisLabelText =
		(!is3D && this.getXAxisLabelOptions().show ? dpGrid.getXAxis().axisName : null);

	this._yGridAxisMap = {};
	this._xGridAxis = this._buildXAxis();
	this._yGridAxes = this._buildYAxes();
};

IDB.CartesianGridAxes.MixedStyle = 1;
IDB.CartesianGridAxes.CombinedStackedStyle = 2;
IDB.CartesianGridAxes.CombinedStyle = 3;
IDB.CartesianGridAxes.StackedStyle = 4;
IDB.CartesianGridAxes.RatioStyle = 5;
IDB.CartesianGridAxes.StandardStyle = 6;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._buildXAxis = function() {
	if ( !this._graphContext.settings.get('XLabelOptions').show ) {
		return null;
	}

	return new IDB.CartesianGridAxisX(this._graphContext, this.getNumRowsDisplayed());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._buildYAxes = function() {
	var sett = this._graphContext.settings;

	if ( this._numYAxes <= 1 ) {
		sett.set('GridMixedYRanges', false);
	}

	if ( sett.getBool('GridMixedYRanges') ) {
		return this._buildYAxesMixed();
	}

	if ( sett.contains('CombinedShareGrid') ) {
		if ( this._useStackedStyle() ) {
			return this._buildYAxesCombinedStacked();
		}

		if ( !sett.getBool('CombinedShareGrid') ) {
			return this._buildYAxesCombined();
		}

		return this._buildYAxesStandard();
	}
	
	if ( this._useStackedStyle() ) {
		return this._buildYAxesStacked();
	}
	
	if ( this._useRatioStyle() ) {
		return this._buildYAxesRatio();
	}

	return this._buildYAxesStandard();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._buildYAxesMixed = function() {
	this._gridStyle = IDB.CartesianGridAxes.MixedStyle;
	this._currYAxisId = IDB.CartesianGrid.ShowNoYAxis;

	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var map = this._yGridAxisMap;
	var numY = this._numYAxes;
	var list = [];
	var yAxis, yGridAxis, stats;

	for ( var i = 0 ; i < numY ; ++i ) {
		yAxis = dpGrid.getYAxis(i, true);
		stats = dpGrid.getStatSet(yAxis.axisId);

		yGridAxis = new IDB.CartesianGridAxisY(gc, this._gridStyle);
		yGridAxis.setAxis(yAxis);
		yGridAxis.addStats(yAxis.axisId, stats.min, stats.max);
		yGridAxis.setActive(false);
		yGridAxis.build();

		list[i] = yGridAxis;
		map[yAxis.axisId] = i;
	}

	return list;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._buildYAxesCombinedStacked = function() {
	this._gridStyle = IDB.CartesianGridAxes.CombinedStackedStyle;
	
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var map = this._yGridAxisMap;
	var numX = this._numXRows;
	var numY = this._numYAxes;
	var shareGrid = gc.settings.getBool('CombinedShareGrid');
	var numLineAxes = gc.settings.get('CombinedNumLineAxes');
	var lineStartJ = numY-numLineAxes;
	var i ,j, sum, dp, yAxis, stats;

	var yGridAxis = new IDB.CartesianGridAxisY(gc, this._gridStyle);

	for ( i = 0 ; i < numX ; ++i ) {
		sum = 0;
						
		for ( j = 0 ; j < lineStartJ ; ++j ) {
			dp = dpGrid.getDatapointAt(i, j, true);
			sum += dp.yValue.numberValue;
			yGridAxis.addStats(dp.yAxis.axisId, sum, sum, false); //captures min/max values
		}
	}

	for ( j = 0 ; j < numY ; ++j ) {
		yAxis = dpGrid.getYAxis(j, true);
		map[yAxis.axisId] = 0;

		if ( j == 0 ) {
			this._currYAxisId = yAxis.axisId;
		}

		if ( j >= lineStartJ ) {
			stats = dpGrid.getStatSet(yAxis.axisId);
			yGridAxis.addStats(yAxis.axisId, stats.min, stats.max, !shareGrid);
		}
	}
					
	yGridAxis.build();
	return [yGridAxis];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._buildYAxesCombined = function() {
	this._gridStyle = IDB.CartesianGridAxes.CombinedStyle;

	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var map = this._yGridAxisMap;
	var numY = this._numYAxes;
	var numLineAxes = gc.settings.get('CombinedNumLineAxes');
	var lineStartI = numY-numLineAxes;
	var i, yAxis, stats;

	var yGridAxis = new IDB.CartesianGridAxisY(gc, this._gridStyle);

	for ( i = 0 ; i < numY ; ++i ) {
		yAxis = dpGrid.getYAxis(i, true);
		stats = dpGrid.getStatSet(yAxis.axisId);
		yGridAxis.addStats(yAxis.axisId, stats.min, stats.max, (i >= lineStartI));
		map[yAxis.axisId] = 0;
		
		if ( i == 0 ) {
			this._currYAxisId = yAxis.axisId;
		}
	}
	
	yGridAxis.build();
	return [yGridAxis];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._buildYAxesStacked = function() {
	this._gridStyle = IDB.CartesianGridAxes.StackedStyle;

	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var map = this._yGridAxisMap;
	var numX = this._numXRows;
	var numY = this._numYAxes;
	var i, j, sum, dp, yAxis;

	var yGridAxis = new IDB.CartesianGridAxisY(gc, this._gridStyle);

	for ( i = 0 ; i < numX ; ++i ) {
		sum = 0;
						
		for ( j = 0 ; j < numY ; ++j ) {
			dp = dpGrid.getDatapointAt(i, j, true);
			sum += dp.yValue.numberValue;
			yGridAxis.addStats(dp.yAxis.axisId, sum, sum); //captures min/max values
		}
	}

	for ( j = 0 ; j < numY ; ++j ) {
		yAxis = dpGrid.getYAxis(j, true);
		map[yAxis.axisId] = 0;

		if ( j == 0 ) {
			this._currYAxisId = yAxis.axisId;
		}
	}
					
	yGridAxis.build();
	return [yGridAxis];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._buildYAxesRatio = function() {
	this._gridStyle = IDB.CartesianGridAxes.RatioStyle;

	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var map = this._yGridAxisMap;
	var numX = this._numXRows;
	var numY = this._numYAxes;
	var min = Number.MAX_VALUE;
	var max = Number.MIN_VALUE;
	var val, yAxis, i ,j;

	var yGridAxis = new IDB.CartesianGridAxisY(gc, this._gridStyle);

	for ( i = 0 ; i < numX ; ++i ) {
		for ( j = 0 ; j < numY ; ++j ) {
			val = dpGrid.getDatapointAt(i, j, true).yValue.numberValue;
			if ( val > 0 ) { max = 100; }
			if ( val < 0 ) { min = -100; }
		}
	}

	for ( j = 0 ; j < numY ; ++j ) {
		yAxis = dpGrid.getYAxis(j, true);
		yGridAxis.addStats(yAxis.axisId, min, max);
		map[yAxis.axisId] = 0;
		
		if ( j == 0 ) {
			this._currYAxisId = yAxis.axisId;
		}
	}
					
	yGridAxis.build();
	return [yGridAxis];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._buildYAxesStandard = function() {
	this._gridStyle = IDB.CartesianGridAxes.StandardStyle;

	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var map = this._yGridAxisMap;
	var numY = this._numYAxes;
	var yAxis, stats, i;

	var yGridAxis = new IDB.CartesianGridAxisY(gc, this._gridStyle);
	
	for ( i = 0 ; i < numY ; ++i ) {
		yAxis = dpGrid.getYAxis(i, true);
		stats = dpGrid.getStatSet(yAxis.axisId);
		yGridAxis.addStats(yAxis.axisId, stats.min, stats.max);
		map[yAxis.axisId] = 0;
		
		if ( i == 0 ) {
			this._currYAxisId = yAxis.axisId;
		}
	}

	yGridAxis.build();
	return [yGridAxis];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._useStackedStyle = function() {
	var sett = this._graphContext.settings;
	return (sett.getOptionalBool('BarStackedMode') || sett.getOptionalBool('AreaStackedMode'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._useRatioStyle = function() {
	var sett = this._graphContext.settings;
	return (sett.getOptionalBool('BarRatioMode') || sett.getOptionalBool('AreaRatioMode'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._onDatapointsUpdated = function() {
	var gc = this._graphContext;
	var sett = gc.settings;

	// This only applies when the graph uses "Mixed-Y" mode and "Y_AXIS_NAME" matches

	if ( !sett.getBool('GridMixedYRanges') ) {
		return;
	}

	if ( sett.get('DatapointMatchType') != IDB.MT.Y_AXIS_NAME ) {
		this._setCurrYAxisId(IDB.CartesianGrid.ShowNoYAxis);
		return;
	}

	// Determine which y-axis is highlighted, then make it active
	
	var dpGrid = gc.datapointGrid;
	var len = dpGrid.getNumXRows();
	var i, j, row, dp;

	for ( i = 0 ; i < len ; ++i ) {
		row = dpGrid.getDatapointRowAt(i, true);
		
		for ( j in row ) {
			dp = row[j];

			if ( dp.getVisualState() != IDB.VS.HIGHLIGHTED ) {
				continue;
			}

			this._setCurrYAxisId(dp.yAxis.axisId);
			return;
		}
	}

	// No datapoints are highlighted

	this._setCurrYAxisId(IDB.CartesianGrid.ShowNoYAxis);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getXGridAxis = function() {
	return this._xGridAxis;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getYGridAxisCount = function() {
	return this._yGridAxes.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getYGridAxis = function(index) {
	return this._yGridAxes[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getYGridAxisById = function(axisId) {
	return this._yGridAxes[this._yGridAxisMap[axisId]];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getXAxisLabelText = function() {
	return this._xAxisLabelText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getYAxisLabelText = function() {
	return this._yAxisLabelText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getNumXRows = function() {
	return this._numXRows;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getStartXIndex = function() {
	return this._xStartIndex;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getCurrYAxisIndex = function() {
	if ( this._currYAxisId == IDB.CartesianGrid.ShowNoYAxis ) {
		return IDB.CartesianGrid.ShowNoYAxis;
	};

	return this._yGridAxisMap[this._currYAxisId];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getXAxisLabelOptions = function() {
	return this._graphContext.settings.get('XAxisLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getYAxisLabelOptions = function() {
	return this._graphContext.settings.get('YAxisLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getHiddenAllowedXSpace = function() {
	return this._graphContext.settings.get('HiddenAllowedXSpace');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getGridColor = function() {
	return this._graphContext.settings.get('GridColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.showXTicks = function() {
	return this._graphContext.settings.getBool('GridTickXShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.isHorizMode = function() {
	return this._graphContext.settings.getBool('HorizMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.isYReversed = function() {
	return this._graphContext.settings.getOptionalBool('ReverseY');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getNumRowsDisplayed = function() {
	return this._numXRows;
		//LATER: Math.min(this._numXRows, this._graphContext.settings.get('MaxRowsDisplayed'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getRangeBgAlpha = function() {
	return (this._graphContext.settings.getBool('GridShowRangeColors') && !this._useRatioStyle() ?
		this._graphContext.settings.getAlpha('GridRangeColorTransparency') : 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype._setCurrYAxisId = function(axisId) {
	if ( !this._graphContext.settings.getBool('GridMixedYRanges') ) {
		return;
	}

	if ( this._currYAxisId != IDB.CartesianGrid.ShowNoYAxis ) {
		this.getYGridAxisById(this._currYAxisId).setActive(false);
	}
	
	if ( this._currYAxisId == axisId ) {
		return;
	}

	this._currYAxisId = axisId;
	
	if ( this._currYAxisId != IDB.CartesianGrid.ShowNoYAxis ) {
		this.getYGridAxisById(this._currYAxisId).setActive(true);
	}

	if ( this._onActiveYAxisChange ) {
		this._onActiveYAxisChange();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.setOnActiveYAxisChange = function(callback) {
	this._onActiveYAxisChange = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.setStartIndex = function(index) {
	this._xStartIndex = index;

	if ( this._xGridAxis ) {
		this._xGridAxis.setStartIndex(index);
	}

	//LATER: CartesianGridAxes.onSetStartIndex()
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
// For 3D Mode
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.isStackedOrRatioMode = function() {
	return (this._useStackedStyle() || this._useRatioStyle());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getNumYNumericCols = function() {
	return this._numYAxes;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getCameraHorizRotation = function() {
	return this._graphContext.settings.get('CameraHorizRotation');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getCameraVertRotation = function() {
	return this._graphContext.settings.get('CameraVertRotation');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxes.prototype.getCameraZoom = function() {
	return -(this._graphContext.settings.get('CameraZoom')/100)+1;
};

/*====================================================================================================*/
IDB.CartesianGridAxesView = function(model, limitHold) {
	this._model = model;
	this._model.setOnActiveYAxisChange(IDB.listener(this, "_onActiveYAxisChange"));

	this._limitHold = limitHold;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype._buildKinetic = function() {
	var model = this._model;
	var xTitleText = model.getXAxisLabelText();
	var yTitleText = model.getYAxisLabelText();
	var xGridAxis = model.getXGridAxis();
	var yGridAxisCount = model.getYGridAxisCount();
	var yAxisMaxLblW = 0;
	var yAxisView, i;

	this._display = new Kinetic.Group();
	this._yAxes = [];
	this._xTitleHeight = 0;
	this._yTitleHeight = 0;
	this._yLabelLeftPush = 0;
	this._yLabelRightPush = 0;

	if ( model.getRangeBgAlpha() > 0 ) {
		this._ranges = new Kinetic.Group();
		this._display.add(this._ranges);
	}

	this._lines = new Kinetic.Group();
	this._display.add(this._lines);

	if ( xTitleText ) {
		this._xTitle = new IDB.GraphLabel(model.getXAxisLabelOptions(), xTitleText);
		this._xTitle.getDisplay().rotation(model.isHorizMode() ? 180 : 0);
		this._display.add(this._xTitle.getDisplay());
		
		this._xTitleHeight = this._xTitle.height;
	}

	if ( yTitleText ) {
		this._yTitle = new IDB.GraphLabel(model.getYAxisLabelOptions(), yTitleText);
		this._yTitle.getDisplay().rotation(-90);
		this._display.add(this._yTitle.getDisplay());

		this._yTitleHeight = this._yTitle.height;
	}

	if ( xGridAxis ) {
		this._xAxis = new IDB.CartesianGridAxisXView(xGridAxis);
		this._display.add(this._xAxis.getDisplay());
	}

	for ( i = 0 ; i < yGridAxisCount ; ++i ) {
		yAxisView = new IDB.CartesianGridAxisYView(model.getYGridAxis(i), this._limitHold);
		this._display.add(yAxisView.getDisplay());
		this._yAxes[i] = yAxisView;

		yAxisMaxLblW = Math.max(yAxisMaxLblW, yAxisView.getLabelWidth());
	}

	for ( i = 0 ; i < yGridAxisCount ; ++i ) {
		yAxisView = this._yAxes[i];
		yAxisView.setLabelWidth(yAxisMaxLblW);
		this._yLabelLeftPush = Math.max(this._yLabelLeftPush, yAxisView.getLeftPush());
		this._yLabelRightPush = Math.max(this._yLabelRightPush, yAxisView.getRightPush());
	}
	
	this._yLabelLeftPush += this._yTitleHeight;

	//LATER: XDragger functionality
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype.getPlotPosX = function() {
	return this._lines.x();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype.getPlotPosY = function() {
	return this._lines.y();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype.getPlotWidth = function() {
	return this._plotW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype.getPlotHeight = function() {
	return this._plotH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype.setSize = function(width, height) {
	this._plotW = this._calcPlotWidth(width);
	this._plotH = this._calcPlotHeight(this._plotW, height);

	var model = this._model;
	var plotW = this._plotW;
	var plotH = this._plotH;

	this._lines.x(this._yLabelLeftPush);

	if ( this._ranges ) {
		this._ranges.x(this._lines.x());
	}
	
	if ( this._xAxis ) {
		this._xAxis.setSize(plotW, height-plotH-this._xTitleHeight);
	}

	this._redrawLines();

	if ( this._limitHold ) {
		this._limitHold.x(this._yTitleHeight);
	}

	for ( var i in this._yAxes ) {
		var yAxis = this._yAxes[i];
		yAxis.getDisplay().x(this._yTitleHeight);
		yAxis.setSize(width-this._yTitleHeight, plotH);
	}

	this._redrawRanges();
			
	if ( this._xTitle ) {
		this._xTitle.x = this._lines.x()+(model.isHorizMode() ? plotW : 0);
		this._xTitle.y = height-(model.isHorizMode() ? 0 : this._xTitleHeight);
		this._xTitle.width = plotW;
	}
	
	if ( this._yTitle ) {
		this._yTitle.y = plotH;
		this._yTitle.width = plotH;
	}
			
	if ( this._xAxis ) {
		var xAxisDisp = this._xAxis.getDisplay();
		xAxisDisp.x(this._lines.x());
		xAxisDisp.y(plotH);
	}

	//LATER: XDragger
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype._calcPlotWidth = function(width) {
	var plotW = width-this._yLabelLeftPush-this._yLabelRightPush;
	return (plotW > 10 ? plotW : 10);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype._calcPlotHeight = function(plotW, height) {
	var plotH = height-this._xTitleHeight;
			
	if ( this._xAxis ) {
		plotH -= this._xAxis.getMaxHeight(plotW, height*this._model.getHiddenAllowedXSpace());
	}
	
	return (plotH > 0 ? plotH : 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype._redrawLines = function() {
	var model = this._model;
	var lines = this._lines;
	var plotW = this._plotW;
	var plotH = this._plotH;
	var numXRows = model.getNumXRows();
	var numRowsDisp = model.getNumRowsDisplayed();
	var startXIndex = model.getStartXIndex();
	var xPos = plotW/numRowsDisp;
	var maxI = startXIndex+numRowsDisp;
	var px, i, line;

	var lineConfig = {
		stroke: model.getGridColor().css,
		strokeWidth: 1
	};

	lines.destroyChildren();

	//Draw top and bottom lines...
	
	lineConfig.points = [0, 0, plotW, 0];
	line = new Kinetic.Line(lineConfig);
	lines.add(line);
	
	lineConfig.points = [0, plotH, plotW, plotH];
	line = new Kinetic.Line(lineConfig);
	lines.add(line);

	//Draw bottom tick marks...

	var revY = model.isYReversed();
	var tickY0 = (revY ?  2 : plotH-2);
	var tickY1 = (revY ? -3 : plotH+3);

	for ( i = startXIndex ; i < maxI ; ++i ) {
		px = xPos*(i-startXIndex+0.5);

		lineConfig.points = [px, tickY0, px, tickY1];
		line = new Kinetic.Line(lineConfig);
		lines.add(line);
		
		if ( model.showXTicks() ) {
			lineConfig.points = [px, 0, px, plotH];
			line = new Kinetic.Line(lineConfig);
			lines.add(line);
		}
	}

	//Draw left line...
			
	if ( startXIndex == 0 ) { //solid
		lineConfig.points = [0, 0, 0, plotH];
		line = new Kinetic.Line(lineConfig);
		lines.add(line);
	}
	else { //dashed //TODO: use Kinetic dashed line
		for ( i = 0 ; i < plotH ; i += 4 ) {
			lineConfig.points = [0, i, 0, Math.min(plotH, i+2)];
			line = new Kinetic.Line(lineConfig);
			lines.add(line);
		}
	}

	//Draw right side...
			
	if ( maxI >= numXRows ) { //solid
		lineConfig.points = [plotW, 0, plotW, plotH];
		line = new Kinetic.Line(lineConfig);
		lines.add(line);
	}
	else { //dashed
		for ( i = 0 ; i < plotH ; i += 4 ) {
			lineConfig.points = [plotW, i, plotW, Math.min(plotH, i+2)];
			line = new Kinetic.Line(lineConfig);
			lines.add(line);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype._redrawRanges = function() {
	var ranges = this._ranges;
	var model = this._model;

	if ( !ranges ) {
		return;
	}

	ranges.destroyChildren();

	var currYIndex = model.getCurrYAxisIndex();

	if ( currYIndex == IDB.CartesianGrid.ShowNoYAxis ) {
		return;
	}

	var plotW = this._plotW;
	var plotH = this._plotH;
	var rangeList = model.getYGridAxis(currYIndex).getRangeList();

	if ( rangeList == null ) {
		return;
	}

	var alpha = model.getRangeBgAlpha();
	var i, r, box;

	for ( i in rangeList ) {
		r = rangeList[i];

		box = new Kinetic.Rect({
			x: 0,
			y: (1-r.hi)*plotH,
			width: plotW,
			height: (r.hi-r.lo)*plotH,
			fill: IDB.toRGBA(r.color, alpha)
		});
		ranges.add(box);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxesView.prototype._onActiveYAxisChange = function() {
	var views = this._yAxes;
	var i;

	for ( i in views ) {
		views[i].updateVisible();
	}

	this._redrawRanges();
};

/*====================================================================================================*/
IDB.CartesianGridAxisX = function(graphContext, numRowsDisplayed) {
	this._graphContext = graphContext;
	this._datapointGrid = graphContext.datapointGrid;
	this._numRowsDisplayed = numRowsDisplayed;

	var len = this._datapointGrid.getNumXRows();
	var dp; //IDB.Datapoint
	var sett = graphContext.settings;
	var is3D = sett.getBool('Is3dGraph');
	
	this._graphContext.addDatapointsUpdatedListener(IDB.listener(this, '_onDatapointsUpdated'));
	this._onVisualStateChange = null;
	this._startIndex = 0; //LATER: this._settings.HiddenStartXIndex;
	this._numYAxes = this._datapointGrid.getNumYCols(true);
			
	var rot = (is3D ? 0 : sett.get('XLabelRotation'));

	if ( !is3D && this.isHorizMode() ) {
		rot -= 90;

		if ( rot < -180 ) {
			rot += 360;
		}
	}

	this._labelRotation = rot;
	this._labelBackwards = ((rot > 0 && rot <= 90) || (rot > -180 && rot < -90));
	this._labelTexts = [];

	for ( var i = 0 ; i < len ; ++i ) {
		dp = this._datapointGrid.getDatapointAt(i, 0, true);
		var fv = dp.xValue.getFV(true);

		this._labelTexts[i] = (fv && fv.length ? fv : ' ');
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.setStartIndex = function(startIndex) {
	this._startIndex = startIndex;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.setOnVisualStateChange = function(callback) {
	this._onVisualStateChange = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype._onDatapointsUpdated = function() {
	if ( !this._onVisualStateChange ) {
		return;
	}

	var dpGrid = this._datapointGrid;
	var len = dpGrid.getNumXRows();
	var i;

	for ( i = 0 ; i < len ; ++i ) {
		this._onVisualStateChange(i, this._getRowState(i));
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype._getRowState = function(index) {
	var dpGrid = this._datapointGrid;
	var row = dpGrid.getDatapointRowAt(index, true);
	var rowState = IDB.VS.FADED;
	var i, vs;

	for ( i in row ) {
		vs = row[i].getVisualState();

		if ( vs == IDB.VS.HIGHLIGHTED ) {
			return vs;
		}

		if ( vs != IDB.VS.FADED ) {
			rowState = IDB.VS.NORMAL;
		}
	}

	return rowState;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.getLabelRotation = function() {
	return this._labelRotation;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.isLabelRotatedBackwards = function() {
	return this._labelBackwards;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.getLabelTexts = function() {
	return this._labelTexts;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.getStartXIndex = function() {
	return this._startIndex;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.getNumRowsDisplayed = function() {
	return this._numRowsDisplayed;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.getXLabelOptions = function() {
	return this._graphContext.settings.get('XLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.centerXLabels = function() {
	return this._graphContext.settings.getOptionalBool('CenterXLabel');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisX.prototype.isHorizMode = function() {
	return this._graphContext.settings.getBool('HorizMode');
};

/*====================================================================================================*/
IDB.CartesianGridAxisXView = function(model) {
	this._model = model;
	this._model.setOnVisualStateChange(IDB.listener(this, "_onVisualStateChange"));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._buildKinetic = function() {
	var model = this._model;
	var labelTexts = model.getLabelTexts();
	var labelRotation = this._model.getLabelRotation();
	var i, lbl, lblHold;

	this._display = new Kinetic.Group();

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	this._labels = [];
	this._labelHolds = [];
	this._labelRightmostPoints = [];
	this._maxW = 0;

	for ( i in labelTexts ) {
		lbl = new IDB.GraphLabel(model.getXLabelOptions(), labelTexts[i]);
		lbl.y = -lbl.height/2;

		this._labels[i] = lbl;
		this._maxW = Math.max(this._maxW, lbl.width);
	}

	this._useMaxW = this._maxW;

	for ( i in this._labels ) {
		this._updateLabelAt(i);

		lblHold = new Kinetic.Group();
		lblHold.rotation(labelRotation);
		lblHold.add(this._labels[i].getDisplay());
		this._hold.add(lblHold);

		this._labelHolds[i] = lblHold;
		this._updateLabelHoldAt(i);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype.getMaxHeight = function(width, giveMaxHeight) {
	var lbl = this._labels[0];
	
	if ( !lbl ) {
		return 0;
	}

	var testW = this._getAngularLabelWidth(width, giveMaxHeight);
	var lblW = lbl.width;

	this._useMaxW = Math.min(this._maxW, testW);
	lbl.resetText();
	lbl.width = this._useMaxW;
	this._updateLabelAt(0);
	this._updateLabelHoldAt(0);

	var max = this._getLabelHoldHeightAt(0)+5;
			
	lbl.width = lblW;
	this._updateLabelAt(0);
	this._updateLabelHoldAt(0);

	return max;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._getAngularLabelWidth = function(width, height) {
	var degrees = this._model.getLabelRotation();

	if ( (degrees % 180) == 0 ) {
		return width;
	}

	var cos = IDB.MathCache.cos(IDB.MathUtil.toRadians(90+degrees));
	return Math.abs(height/cos);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._getLabelHoldHeightAt = function(index) {
	var lblHold = this._labelHolds[index];
	return IDB.MathUtil.calculateRotatedBounds(
		lblHold.fill.width(), lblHold.fill.height(), lblHold.rotation()).height;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype.setSize = function(width, height, force) {
	if ( !force && width == this._lastWidth && height == this._lastHeight ) {
		return;
	}

	this._lastWidth = width;
	this._lastHeight = height;

	var model = this._model;
	var startXIndex = model.getStartXIndex();
	var numRowsDisp = model.getNumRowsDisplayed();
	var xPos = width/numRowsDisp;
	var useMaxW = this._useMaxW;
	var centerLbls = model.centerXLabels();
	var rot = model.getLabelRotation();
	var lblBounds = null;
	var lbl = null;
	var i, lblHold;

	for ( i in this._labels ) {
		lbl = this._labels[i];
		lblHold = this._labelHolds[i];

		if ( i < startXIndex || i-startXIndex >= numRowsDisp ) {
			lbl.setOffscreen(true);
			lblHold.visible(false);
			continue;
		}

		lbl.resetText();
		lbl.width = (centerLbls ? useMaxW : Math.min(useMaxW, lbl.textWidth+5));
		lbl.setOffscreen(false);
		lbl.setOverlap(false); //reset prior to overlap tests
		this._updateLabelAt(i);
		this._updateLabelHoldAt(i);

		lblBounds = IDB.MathUtil.calculateRotatedBounds(
			lblHold.fill.width(), lblHold.fill.height(), lblHold.rotation());

		lblHold.x(xPos*(i-startXIndex+0.5));
		lblHold.y(height-this._getLabelHoldHeightAt(i)/2);
		lblHold.visible(true);
	}

	if ( rot % 90 ) { //if not 0, 90, 180, -90, or -180
		var dir = (rot > 0 ? 1 : -1)*(Math.abs(rot) > 90 ? -1 : 1);
		this._hold.x(lblBounds.width/2*dir - lbl.height/2*dir);
	}

	this._hideOverlaps();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._hideOverlaps = function() {
	var model = this._model;
	var rot = model.getLabelRotation();

	if ( rot%90 == 0 ) {
		this._hideOverlapsStraight(rot);
	}
	else {
		this._hideOverlapsAngled(rot);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._hideOverlapsStraight = function(degrees) {
	var holds = this._labelHolds;
	var labels = this._labels;
	var flat = (degrees%180 == 0);
	var lastI = null;
	var currLbl, currL, lastR, overlap, i;

	for ( i in labels ) {
		currLbl = labels[i];
		overlap = false;

		if ( currLbl.getOffscreen() ) {
			continue;
		}

		if ( lastI != null ) {
			currL = holds[i].x()-(flat ? currLbl.textWidth : currLbl.textHeight)/2;
			lastR = holds[lastI].x()+(flat ? labels[lastI].textWidth : labels[lastI].textHeight)/2;
			overlap = (currL < lastR);
		}

		currLbl.setOverlap(overlap);

		if ( !overlap ) {
			lastI = i;
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._hideOverlapsAngled = function(degrees) {
	var holds = this._labelHolds;

	if ( holds.length == 0 ) {
		return;
	}

	var labels = this._labels;
	var rad = IDB.MathUtil.toRadians(Math.abs(degrees));
	var sin = IDB.MathCache.sin(rad);
	var cos = IDB.MathCache.cos(rad);
	var halfH = labels[0].textHeight*0.45; //less than half
	var lastLbl = null;
	var currLbl, overlap, holdX, rotX, rotY, lastRotX, lastRotY, currNw, lastSe, i;

	/*if ( this._debug ) {
		this._debug.destroy();
	}

	this._debug = new Kinetic.Group();
	this._hold.add(this._debug);*/

	for ( i in labels ) {
		currLbl = labels[i];
		overlap = false;

		if ( currLbl.getOffscreen() ) {
			continue;
		}

		holdX = holds[i].x();
		rotX = holdX*cos;
		rotY = holdX*sin;

		/*var dot = new Kinetic.Circle({x:holdX, y:holds[i].y(), radius: 3, fill:'red'});
		this._debug.add(dot);

		dot = new Kinetic.Circle({x:rotX, y:rotY, radius: 3, fill:'blue'});
		this._debug.add(dot);

		dot = new Kinetic.Line({
			closed: true,
			points: [
				[rotX, rotY-halfH],
				[rotX, rotY+halfH],
				[rotX-currLbl.textWidth, rotY+halfH],
				[rotX-currLbl.textWidth, rotY-halfH],
			],
			fill: 'rgba(0, 0, 0, 0.2)'
		});
		this._debug.add(dot);*/

		if ( lastLbl != null ) {
			currNw = { x: rotX-currLbl.textWidth, y: rotY-halfH };
			lastSe = { x: lastRotX, y: lastRotY+halfH };
			overlap = (currNw.y < lastSe.y && currNw.x < lastSe.x);

			/*dot = new Kinetic.Circle({x:currNw.x, y:currNw.y, radius: 2, fill:'orange'});
			this._debug.add(dot);
			
			dot = new Kinetic.Circle({x:lastSe.x, y:lastSe.y, radius: 2, fill:'magenta'});
			this._debug.add(dot);*/
		}

		currLbl.setOverlap(overlap);

		if ( !overlap ) {
			lastLbl = currLbl;
			lastRotX = rotX;
			lastRotY = rotY;
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._updateLabelAt = function(index) {
	var lbl = this._labels[index];
	lbl.truncate();

	var lblX = -lbl.width/2;
	var rot = this._model.getLabelRotation();
	var pos = ((rot % 180) ? (rot > 0 ? -1 : 1) : 0);

	if ( pos ) {
		lblX += (this._useMaxW-lbl.width)/2*pos;
	}

	lbl.x = lblX;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._updateLabelHoldAt = function(index) {
	var lblHold = this._labelHolds[index];
	var lbl = this._labels[index];
	var useMaxW = this._useMaxW;
	var lblH = lbl.height;

	this._labelRightmostPoints[index] = this._getRightmostPoint(lbl);

	if ( lblHold.fill ) {
		lblHold.fill.destroy();
	}

	lblHold.fill = new Kinetic.Rect({
		x: -useMaxW/2,
		y: -lblH/2,
		width: useMaxW,
		height: lblH,
		fill: 'rgba(0, 0, 0, 0)'
	});
	lblHold.add(lblHold.fill);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._getRightmostPoint = function(label) {
	var model = this._model;
	var rot = model.getLabelRotation();
	var flipV = (rot > 0 ? -1 : 1);
	var flipH = (Math.abs(rot) <= 90 ? 1 : -1);
			
	if ( model.isLabelRotatedBackwards() ) {
		flipH *= -1;
		flipV *= -1;
	}
	
	return {
		x: label.x+(flipH > 0 ? label.width-2 : 2),
		y: label.y+(flipV > 0 ? label.height-2 : 2)
	};
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisXView.prototype._onVisualStateChange = function(index, visualState) {
	var lbl = this._labels[index];

	switch ( visualState ) {
		case IDB.VS.HIGHLIGHTED:
			lbl.setHighlight(true);
			this._labelHolds[index].moveToTop();
			break;
			
		case IDB.VS.NORMAL:
		case IDB.VS.FADED:
			lbl.setHighlight(false);
			break;
	}
};

/*====================================================================================================*/
IDB.CartesianGridAxisY = function(graphContext, axisStyle) {
	this._graphContext = graphContext;
	this._axisStyle = axisStyle;
	this._axis = null;
	this._active = true;
	this._ranges = [];

	var ticks = graphContext.settings.get('GridTickYMajorCount');

	this._statsCount = [0, 0];
	this._min = [Number.MAX_VALUE, Number.MAX_VALUE];
	this._max = [-Number.MAX_VALUE, -Number.MAX_VALUE];
	this._axisStatsType = {};
	this._numDivs = [ticks, ticks];
	this._bounds = [null, null];
	this._zeroBoundaryIndex = [-1, -1];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.setAxis = function(axis) {
	this._axis = axis;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.addStats = function(axisId, min, max, isSecondary) {
	var i = (IDB.ifUndef(isSecondary, false) ? 1 : 0);
	this._axisStatsType[axisId] = i;
	this._statsCount[i]++;

	if ( min < this._min[i] ) {
		this._min[i] = min;
	}
	
	if ( max > this._max[i] ) {
		this._max[i] = max;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.build = function() {
	this._finishStats(0);
	this._finishStats(1);
	this._buildRangeList();
	this._leftLabels = null;
	this._rightLabels = null;
	this._rightLblOpts = this._buildRightLabelOptions();

	if ( this.getLeftLabelOptions().show ) {
		this._leftLabels = (this.isMetricsMode() ? 
			this._buildMetricsLabels() : this._buildDivisionLabels(0));
	}

	if ( this._rightLblOpts && this._rightLblOpts.show ) {
		this._rightLabels = this._buildRightLabels();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype._finishStats = function(index) {
	this._ensureStatsIncludeOrigin(index);
	
	if ( this._statsCount[index] == 0 ) {
		return;
	}

	var numDivs = this._numDivs[index];

	if ( !numDivs ) {
		return;
	}

	var sett = this._graphContext.settings;
	var min = this._min[index];
	var max = this._max[index];
	var is3D = sett.getBool('Is3dGraph');

	if ( !is3D && index == 0 && sett.getBool('GridZoomForce') && !sett.getBool('GridMixedYRanges') ) {
		min = sett.get('GridZoomMin');
		max = Math.max(min, sett.get('GridZoomMax'));
	}
	
	if ( min == max ) {
		if ( min > 0 ) {
			min = 0;
		}
		else if ( max < 0 ) {
			max = 0;
		}
		else {
			max = 10;
		}
	}

	var dmObj = IDB.Calculator.getDivisionMetrics(min, max, numDivs);
	var dm = dmObj.largeDivisions;
	var bounds = dm.boundaries;
	var boundsLen = bounds.length;
	var i;

	this._min[index] = dm.getMinBoundary();
	this._max[index] = dm.getMaxBoundary();
	this._bounds[index] = bounds;
	this._numDivs[index] = boundsLen;

	if ( sett.getOptionalBool('ReverseY') ) {
		bounds.reverse();
	}
	
	for ( i in bounds ) {
		if ( bounds[i] == 0 ) {
			this._zeroBoundaryIndex[index] = i;
			break;
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype._buildDivisionLabels = function(index) {
	var values = [];
	var bounds = this._bounds[index];
	var sett = this._graphContext.settings;
	var isRatio = (this._axisStyle == IDB.CartesianGridAxes.RatioStyle);
	var numFmt = (this._axis ? this._axis.numFmt : null);
	var val, i;

	for ( i in bounds ) {
		val = bounds[i];

		if ( isRatio ) {
			values[i] = val+'%';
		}
		else {
			values[i] = sett.formatNumber(val, numFmt);
		}
	}

	return values;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype._buildMetricsLabels = function() {
	var values = [];
	var dpGrid = this._graphContext.datapointGrid;
	var sett = this._graphContext.settings;
	var targ = sett.getBool('MetricsTargetMode');
	var fullLen = dpGrid.getNumYCols(true);
	var targLen = fullLen;
	var inc = 1;
	var i, lblI;

	if ( targ ) {
		fullLen = Math.floor(fullLen/2)*2;
		targLen = fullLen/2;
		inc = 2;
	}

	for ( i = 0 ; i < fullLen ; i += inc ) {
		lblI = (targ ? Math.floor(i/inc) : i);
		lblI = (this.isHorizMode() ? (targ ? targLen : fullLen)-lblI-1 : lblI);

		values[lblI] = dpGrid.getYAxis(fullLen-i-1,true).axisName+
			(targ ? ' / '+dpGrid.getYAxis(fullLen-i-2,true).axisName : '');
	}

	return values;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype._buildRightLabelOptions = function() {
	var sett = this._graphContext.settings;
	var leftLblOpt = this.getLeftLabelOptions();
	var rightLblOpt = {
		show: leftLblOpt.show,
		size: leftLblOpt.size,
		color: leftLblOpt.color,
		bold: leftLblOpt.bold,
		graph: null
	};

	if ( sett.contains('CombinedShareGrid') ) { //Combined graph
		rightLblOpt.show = (leftLblOpt.show && !sett.getBool('CombinedMultiMode'));
		rightLblOpt.color = sett.get('CombinedRightYLabelColor');
		rightLblOpt.graph = 'Combined';
		return rightLblOpt;
	}

	if ( sett.contains('ParetoBreakPercent') ) { //Pareto graph
		rightLblOpt.color = sett.get('LineColor');
		rightLblOpt.graph = 'Pareto';
		return rightLblOpt;
	}

	return null;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype._buildRightLabels = function() {
	var values = [];

	switch ( this._rightLblOpts.graph ) {
		case 'Combined':
			values = this._buildDivisionLabels(1);
			break;

		case 'Pareto':
			for ( var i = 0 ; i <= 5 ; ++i ) {
				values[i] = Math.round(i*20)+'%';
			}
			break;
	}

	return values;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype._ensureStatsIncludeOrigin = function(index) {
	var sett = this._graphContext.settings;

	if ( !sett.getBool('Is3dGraph') && sett.getBool('GridZoomDynamic') ) {
		if ( this._min[index] == this._max[index] ) {
			this._max[index] += 0.001;
		}

		return;
	}

	if ( this._min[index] > 0 ) {
		this._min[index] = 0;
	}
	
	if ( this._max[index] < 0 ) {
		this._max[index] = 0;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype._buildRangeList = function() {
	var defaultList = this._graphContext.datapointGrid.getXAxis().getRangeList();
	var rl = (this._axis ? this._axis.getRangeList() : null);
	
	if ( !rl || !rl.length ) {
		rl = defaultList;
	}

	var len = rl.length;
	var stats = this._getStats(0);
	var valRange = stats.max-stats.min;
	var isRev = this._graphContext.settings.getOptionalBool('ReverseY');
	var i, r1, r2, p1, p2;
	
	for ( i = 0 ; i < len ; ++i ) {
		r1 = rl[i];
		r2 = (i == rl.length-1 ? null : rl[i+1]);
		
		p1 = (r1.val-stats.min)/valRange;
		p2 = (r2 ? (r2.val-stats.min)/valRange : 1);
		
		this._ranges.push({
			color: r1.color,
			lo: IDB.MathUtil.clamp((isRev ? 1-p1 : p1), 0, 1),
			hi: IDB.MathUtil.clamp((isRev ? 1-p2 : p2), 0, 1)
		});
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getActive = function() {
	return this._active;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getDivsionCount = function() {
	return this._numDivs[0];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getLeftLabelValues = function() {
	return this._leftLabels;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getRightLabelValues = function() {
	return this._rightLabels;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getZeroBoundaryIndex = function() {
	return this._zeroBoundaryIndex[0];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getStatsByAxis = function(axisId) {
	return this._getStats(this._axisStatsType[axisId]);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype._getStats = function(index) {
	return {
		min: this._min[index],
		max: this._max[index]
	};
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getLeftLabelOptions = function() {
	return this._graphContext.settings.get('YLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.isHorizMode = function() {
	return this._graphContext.settings.getBool('HorizMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.isHorizLabel = function() {
	return (this.isHorizMode() && this._graphContext.settings.getBool('YLabelParallel'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.isMetricsMode = function() {
	return this._graphContext.settings.contains('MetricsShape');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.showTicks = function() {
	return this._graphContext.settings.getBool('GridTickYShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getMinorTickCount = function() {
	return this._graphContext.settings.get('GridTickYMinorCount');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getTickFrontShow = function() { //for 3D graphs
	return this._graphContext.settings.getBool('GridTickFrontShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.showGridBars = function() {
	return this._graphContext.settings.getBool('GridBars');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getGridColor = function() {
	return this._graphContext.settings.get('GridColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.isReversed = function() {
	return this._graphContext.settings.getOptionalBool('ReverseY');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getRightLabelOptions = function() {
	return this._rightLblOpts;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getLimitObject = function(isUpper) {
	var sett = this._graphContext.settings;
	var min = this._min[0];
	var max = this._max[0];
	var type = (isUpper ? 'Upper' : 'Lower');
	var is3D = sett.getBool('Is3dGraph');
	var val = sett.get(type+'TickValue');
	var col = sett.get(type+'TickColor');
	var text = (is3D ? null : sett.get(type+'TickLabel'));
	var leftLblOpt = this.getLeftLabelOptions();
	var numFmt = (this._axis ? this._axis.numFmt : null);
	var horizMode = (is3D ? false : this.isHorizMode());
	var yLblParallel = (is3D ? false : sett.getBool('YLabelParallel'));

	return {
		show: (sett.getBool(type+'TickShow') && val >= min && val <= max),
		color: col,
		showText: (text != null && text.length > 0),
		text: text,
		labelOptions: {
			show: sett.get('YLabelOptions').show,
			size: leftLblOpt.size,
			bold: leftLblOpt.bold,
			color: col
		},
		value: val,
		valueStr: sett.formatNumber(val, numFmt),
		position: 1-(val-min)/(max-min),
		horizMode: horizMode,
		yLabelParallel: yLblParallel,
		horizLabel: (horizMode && yLblParallel)
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getRangeList = function() {
	return this._ranges;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.getRefreshDisplayFunc = function() {
	return this._graphContext.refreshDisplay;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisY.prototype.setActive = function(active) {
	this._active = active;
};

/*====================================================================================================*/
IDB.CartesianGridAxisYLimitView = function(model, maxW, refreshDisplayFunc) {
	this._model = model;
	this._maxW = maxW;
	this._refreshDisplayFunc = refreshDisplayFunc;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYLimitView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	this._line = new Kinetic.Line({
		stroke: model.color.css,
		strokeWidth: 1,
		dash: [4, 3]

	});
	this._display.add(this._line);

	////

	if ( model.labelOptions.show ) {
		this._labelHold = new Kinetic.Group();
		this._display.add(this._labelHold);

		model.labelOptions.align = (model.horizMode ? 
			(model.yLabelParallel ? 'center' : 'left') : 'right');

		this._label = new IDB.GraphLabel(model.labelOptions, model.valueStr);
		this._label.rotateFromCenter(model.horizLabel ? -90 : 0);
		this._labelHold.add(this._label.getDisplay());
	
		if ( !model.horizLabel ) {
			this._label.width = Math.max(this._maxW, this._label.width);
		}
	}

	////

	if ( !model.showText ) {
		return;
	}

	this._icon = new Kinetic.Circle({
		radius: 6,
		fill: IDB.toRGBA(model.color, 0.2),
		stroke: model.color.css,
		strokeWidth: 1
	});
	this._icon.on('mouseover tap', IDB.listener(this, '_onMouseOver'));
	this._icon.on('mouseout', IDB.listener(this, '_onMouseOut'));
	this._display.add(this._icon);

	model.labelOptions.align = 'center';

	this._iconText = new IDB.GraphLabel(model.labelOptions, model.text);
	this._iconText.width += 6;
	this._iconText.setHighlight(true);
	this._iconText.visible = false;
	this._iconText.getDisplay().listening(false);
	this._display.add(this._iconText.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYLimitView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYLimitView.prototype.getViewHeight = function() {
	return 12;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYLimitView.prototype.setWidthAndPush = function(width, push) {
	var model = this._model;
	var lineW = width;
	var lblX = (model.horizMode ? width : 0);
	var iconW;
	
	if ( this._labelHold ) {
		if ( model.horizLabel ) {
			lblX += this._label.height;
		}
		else {
			lblX += (this._maxW-this._label.textWidth/2+4)*(model.horizMode ? 1 : -1);
		}

		this._labelHold.x(lblX);
		this._labelHold.y(push);
	}

	if ( this._icon ) {
		iconW = this._icon.getWidth();
		lineW -= iconW;
		this._icon.x(width-iconW/2);
		this._icon.y(push);
		
		this._iconText.x = width-this._iconText.width;
		this._iconText.y = push-this._iconText.height/2;
	}

	this._line.points([
		0, 0,
		lineW, 0
	]);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYLimitView.prototype._onMouseOver = function() {
	this._iconText.visible = true;
	this._refreshDisplayFunc();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYLimitView.prototype._onMouseOut = function() {
	this._iconText.visible = false;
	this._refreshDisplayFunc();
};

/*====================================================================================================*/
IDB.CartesianGridAxisYView = function(model, limitHold) {
	this._model = model;
	this._limitHold = limitHold;
	
	this._leftPush = 0;
	this._rightPush = 0;
	this._pad = 4;
	this._maxW = 0;

	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();
	this._labels = this._buildLeftLabels();
	this._rightLabels = this._buildRightLabels();
	this._lines = [];
	this._upperLimit = this._buildLimit(true);
	this._lowerLimit = this._buildLimit(false);
	this.updateVisible();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype._buildLeftLabels = function() {
	var model = this._model;

	if ( !model.getLeftLabelValues() ) {
		return null;
	}

	var labels = [];
	var lblOpt = model.getLeftLabelOptions();
	var values = model.getLeftLabelValues();
	var horizLabel = model.isHorizLabel();
	var lbl, i;

	lblOpt.align = (horizLabel ? 'center' : 'right');
	
	for ( i in values ) {
		lbl = new IDB.GraphLabel(lblOpt, values[i]);
		this._display.add(lbl.getDisplay());

		labels.push(lbl);
		this._maxW = Math.max(this._maxW, lbl.width);
	}

	return labels;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype._buildRightLabels = function() {
	var model = this._model;

	if ( !model.getRightLabelValues() ) {
		return null;
	}

	var labels = [];
	var lblOpt = model.getRightLabelOptions();
	var values = model.getRightLabelValues();
	var i, lbl;

	lblOpt.align = 'left';

	for ( i in values ) {
		lbl = new IDB.GraphLabel(lblOpt, values[i]);
		this._display.add(lbl.getDisplay());

		labels.push(lbl);
		this._rightPush = Math.max(this._rightPush, lbl.width);
	}

	for ( i in labels ) {
		labels[i].width = this._rightPush;
	}

	return labels;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype._buildLimit = function(isUpper) {
	var model = this._model;
	var obj = model.getLimitObject(isUpper);

	if ( !obj.show ) {
		return null;
	}

	var limit = new IDB.CartesianGridAxisYLimitView(obj, this._maxW, model.getRefreshDisplayFunc());
	this._limitHold.add(limit.getDisplay());
	return limit;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype.getLabelWidth = function() {
	return this._maxW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype.getLeftPush = function() {
	return (this._leftPush ? this._leftPush+this._pad : 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype.getRightPush = function() {
	return (this._rightPush ? this._rightPush+this._pad : 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype.setLabelWidth = function(width) {
	if ( !this._labels ) {
		return;
	}

	var labels = this._labels;
	var model = this._model;
	var horizLabel = model.isHorizLabel();
	var lbl = null;
	var i;

	for ( i in labels ) {
		lbl = labels[i];
		lbl.width = width;
		
		if ( horizLabel ) {
			lbl.rotateFromCenter(-90);
		}
	}

	this._maxW = width;
	this._leftPush = (horizLabel && lbl ? lbl.height+5 : this._maxW);
	
	if ( model.isHorizMode() && (this._upperLimit || this._lowerLimit) ) {
		this._rightPush = this._leftPush;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype.setSize = function(width, height) {
	var model = this._model;

	if ( model.showTicks() ) {
		this._setSizeForTicks(width, height);
	}

	if ( this._labels ) {
		this._setSizeForLabels(width, height);
	}

	if ( this._rightLabels ) {
		this._setSizeForRightLabels(width, height);
	}

	if ( this._upperLimit ) {
		this._setSizeForLimit(width, height, this._upperLimit, true);
	}

	if ( this._lowerLimit ) {
		this._setSizeForLimit(width, height, this._lowerLimit, false);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype._setSizeForTicks = function(width, height) {
	for ( i in this._lines ) {
		this._lines[i].destroy();
	}

	this._lines = [];

	var model = this._model;
	var numY = model.getDivsionCount();
	var dist = height/(numY-1);
	var mc = model.getMinorTickCount();
	var mcDist = dist/(mc+1);
	var rawGridCol = model.getGridColor();
	var barCol = IDB.toRGBA(rawGridCol, 0.5);
	var gridCol = rawGridCol.css;
	var mcCol = IDB.toRGBA(rawGridCol, 0.3);
	var zeroIndex = model.getZeroBoundaryIndex();
	var leftPush = this.getLeftPush();
	var rightPush = this.getRightPush();
	var useW = width-leftPush-rightPush;
	var py, line, mcY, i, j;

	for ( i = 0 ; i < numY ; ++i ) {
		py = dist*i;

		if ( model.showGridBars() ) {
			if ( (i%2) == 0 && i != numY-1 ) {
				line = new Kinetic.Rect({
					x: leftPush,
					y: py,
					width: useW,
					height: dist,
					fill: barCol
				});

				this._display.add(line);
				this._lines.push(line);
			}

			continue;
		}

		line = new Kinetic.Line({
			points: [leftPush, py, width-rightPush, py],
			stroke: gridCol,
			strokeWidth: (numY-i-1 == zeroIndex ? 2 : 1)
		});

		this._display.add(line);
		this._lines.push(line);

		if ( !i || !mc ) {
			continue;
		}

		for ( j = 1; j <= mc ; ++j ) {
			mcY = py-mcDist*j;

			line = new Kinetic.Line({
				points: [leftPush, mcY, width-rightPush, mcY],
				stroke: mcCol
			});

			this._display.add(line);
			this._lines.push(line);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype._setSizeForLabels = function(width, height) {
	var model = this._model;
	var labels = this._labels;
	var horizLabel = model.isHorizLabel();
	var numY = labels.length;
	var dist = height/(numY-1);
	var hideY = Number.POSITIVE_INFINITY;
	var lbl, lblH, py, i;

	for ( i = 0 ; i < numY ; ++i ) {
		lbl = labels[i];
		lblH = lbl.calculateBounds().height;

		if ( model.isMetricsMode() ) {
			py = height/numY*(numY-i-0.5);
		}
		else {
			py = height-dist*i;

			if ( i == 0 ) {
				py = height-lblH*0.333;
			}
			else if ( i == numY-1 ) {
				py = lblH/2;
			}
		}

		lbl.x = (horizLabel ? lbl.height : 0);
		lbl.y = py-(horizLabel ? 0 : lblH/2);

		if ( py+(horizLabel ? lblH/2 : 0) > hideY ) {
			lbl.visible = false;
		}
		else {
			lbl.visible = true;
			hideY = py-(horizLabel ? lblH/2+4 : lblH);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype._setSizeForRightLabels = function(width, height) {
	var labels = this._rightLabels;
	var numLabels = labels.length;
	var rightPush = this.getRightPush();
	var i, lbl;

	for ( i in labels) {
		lbl = labels[i];
		lbl.x = width-rightPush+5;
		lbl.y = height*(numLabels-i-1)/(numLabels-1);

		if ( i == 0 ) {
			lbl.y -= lbl.height*0.75;
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype._setSizeForLimit = function(width, height, limitView, isUpper) {
	var model = this._model;
	var limitObject = model.getLimitObject(isUpper);
	var horizMode = model.isHorizMode();
	var display = limitView.getDisplay();
	var push = limitView.getViewHeight()*0.3;
	var px = this.getLeftPush();
	var py = limitObject.position*height;

	display.x(px);
	display.y((model.isReversed() ? height-py : py));

	limitView.setWidthAndPush(width-px-this.getRightPush(),
		(py < push ? push : (py > height-push ? -push : 0)));

	if ( this._labels == null || horizMode ) {
		return;
	}

	var labels = this._labels;
	var lbl, lblH, i;

	for ( i in labels ) {
		lbl = labels[i];
		lblH = lbl.calculateBounds().height;

		if ( Math.abs((lbl.y+lblH/2)-display.y()) <= lblH*0.8 ) {
			lbl.visible = false;
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridAxisYView.prototype.updateVisible = function() {
	var vis = this._model.getActive();
	this._display.visible(vis);

	if ( this._upperLimit ) {
		this._upperLimit.getDisplay().visible(vis);
	}

	if ( this._lowerLimit ) {
		this._lowerLimit.getDisplay().visible(vis);
	}

	return vis;
};

/*====================================================================================================*/
IDB.CartesianGridView = function(model, limitHold) {
	this._model = model;
	this._limitHold = limitHold;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();

	this._axes = new IDB.CartesianGridAxesView(this._model.getAxes(), this._limitHold);
	//LATER: axes update event (related to dragger)
	this._display.add(this._axes.getDisplay());

	//LATER: this._drag
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridView.prototype.getPlotPosX = function() {
	return this._axes.getPlotPosX();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridView.prototype.getPlotPosY = function() {
	return this._axes.getPlotPosY();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridView.prototype.getPlotWidth = function() {
	return this._axes.getPlotWidth();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridView.prototype.getPlotHeight = function() {
	return this._axes.getPlotHeight();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CartesianGridView.prototype.setSize = function(width, height) {
	var axesH = height; //LATER: subtract 20 if this._drag is visible
	this._axes.setSize(width, axesH);
	//LATER: this._drag.setSize()
};

/*====================================================================================================*/

