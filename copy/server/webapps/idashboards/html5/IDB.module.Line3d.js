'use strict';
IDB.Line3dAxisView = function(draw3d, model) {
	this._draw3d = draw3d;
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dAxisView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();
	this._segments = [];

	var draw3d = this._draw3d;
	var model = this._model;
	var segCount = model.getSegmentCount();
	var i, seg;

	for ( i = 0 ; i < segCount ; ++i ) {
		seg = new IDB.Line3dSegmentView(draw3d, model.getSegment(i), segCount);
		this._display.add(seg.getDisplay());
		this._segments[i] = seg;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dAxisView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dAxisView.prototype.setMaxDimensions = function(maxDims) {
	var model = this._model;
	var segments = this._segments;
	var axisIndex = model.getYAxisIndex();
	var maxDimY = maxDims.y;

	for ( var i in segments ) {
		segments[i].setPositionAndSize(i, axisIndex, maxDimY);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dAxisView.prototype.render = function() {
	var segments = this._segments;

	for ( var i in segments ) {
		segments[i].render();
	}
};

/*====================================================================================================*/
IDB.Line3dGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	this._info.legendStyle = IDB.LS.AXES;
};

IDB.Line3dGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.Line3dGraph.prototype.constructor = IDB.Line3dGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dGraph.prototype._build = function() {
	var gc = this._graphContext;
	var grid = new IDB.CartesianGrid(gc);
	var model = new IDB.LinePlot(gc, grid.getAxes());

	this._gridView = new IDB.Cartesian3dGridView(grid);
	this._stageLayer.add(this._gridView.getDisplay());

	var plotHold = this._gridView.getPlotDisplay();
	var draw3d = this._gridView.getDraw3d();

	this._plotView = new IDB.Line3dPlotView(draw3d, model);
	plotHold.add(this._plotView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dGraph.prototype._update = function(width, height) {
	var gv = this._gridView;
	var pv = this._plotView;

	gv.updateScreen(width, height);
	gv.updateCamera();
	gv.render();

	pv.setMaxDimensions(gv.getMaxDimensions());
	pv.render();
};

/*====================================================================================================*/
IDB.Line3dPlotView = function(draw3d, model) {
	this._draw3d = draw3d;
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dPlotView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();
	this._axes = [];

	var draw3d = this._draw3d;
	var plotModel = this._model;
	var axisCount = plotModel.getAxisCount();
	var i, axis;

	for ( i = axisCount-1 ; i >= 0 ; --i ) {
		axis = new IDB.Line3dAxisView(draw3d, plotModel.getAxis(i));
		this._display.add(axis.getDisplay());
		this._axes[i] = axis;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dPlotView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dPlotView.prototype.setMaxDimensions = function(maxDims) {
	var axes = this._axes;

	for ( var i in axes ) {
		axes[i].setMaxDimensions(maxDims);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dPlotView.prototype.render = function() {
	var axes = this._axes;

	for ( var i in axes ) {
		axes[i].render();
	}
};

/*====================================================================================================*/
IDB.Line3dSegmentView = function(draw3d, model, segmentCount) {
	this._draw3d = draw3d;
	this._model = model;
	this._segRelWidth = 1/segmentCount;
	model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dSegmentView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	if ( !model.getActive() ) {
		return;
	}
	
	var disp = this._display;
	disp.on("click", model.createClickListener());
	disp.on("mouseover", model.createOverListener());
	disp.on("mouseout", model.createOutListener());
	disp._touchHandler = new IDB.TouchHandler(disp);

	var relPts = model.getRelativePoints();
	var lblOpt = model.getLabelOptions();

	var lineConfig = {
		closed: true,
		lineJoin: 'round',
		lineCap: 'butt'
	};

	lineConfig.fill = this._getAngledFaceFill(relPts[0], relPts[1]);
	this._fillA = new Kinetic.Line(lineConfig);

	lineConfig.fill = this._getAngledFaceFill(relPts[1], relPts[2]);
	this._fillB = new Kinetic.Line(lineConfig);

	lineConfig.closed = false;
	lineConfig.fill = null;
	lineConfig.stroke = model.getLineColor().css;
	lineConfig.strokeWidth = 1;

	this._lineA = new Kinetic.Line(lineConfig);
	this._lineB = new Kinetic.Line(lineConfig);

	disp.add(this._lineA);
	disp.add(this._fillA);
	disp.add(this._lineB);
	disp.add(this._fillB);

	if ( lblOpt.show ) {
		this._labelHold = new Kinetic.Group();
		disp.add(this._labelHold);

		this._label = new IDB.GraphLabel(lblOpt, model.getLabelText());
		this._label.rotateFromCenter(0);
		this._labelHold.add(this._label.getDisplay());
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dSegmentView.prototype._getAngledFaceFill = function(a, b) {
	var model = this._model;
	var ci = model.getDatapoint().axisColor.i;
	var shade = IDB.Draw3D.getAngledFaceShade(a, b, this._segRelWidth, false);
	return IDB.toRGBA(IDB.Draw3D.shadeColor(ci, shade), model.getFillAlpha());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dSegmentView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dSegmentView.prototype.setLabelRef = function(labelRef) {
	this._labelRef = labelRef;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dSegmentView.prototype.setPositionAndSize = function(px, pz, maxY) {
	this._px = Number(px)+0.5;
	this._pz = Number(pz)+0.5;
	this._maxY = maxY;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dSegmentView.prototype.render = function() {
	var model = this._model;
	
	if ( !model.getActive() ) {
		return;
	}
	
	var d3 = this._draw3d;
	var px = this._px;
	var pz = this._pz;
	var maxY = this._maxY;
	var lblHold = this._labelHold;
	var relPts = model.getRelativePoints();
	var frontPts = [];
	var backPts = [];
	var thick = 1;
	var lblVec = null;
	var i, rp, v;

	for ( i in relPts ) {
		rp = relPts[i];

		if ( !rp ) {
			continue;
		}

		v = new IDB.Vec3(px+rp.x, (1-rp.y)*maxY, pz-thick/2);
		frontPts[i] = d3.project(v);
		
		v.z += thick;
		backPts[i] = d3.project(v);

		if ( i == 1 ) {
			v.z -= thick/2;
			lblVec = v;
		}
	}

	var leftPts = [frontPts[0], frontPts[1], backPts[1], backPts[0]];
	var rightPts = [frontPts[2], frontPts[1], backPts[1], backPts[2]];

	IDB.Draw3D.setFacePoints(this._fillA, leftPts);
	IDB.Draw3D.setFacePoints(this._fillB, rightPts);

	IDB.Draw3D.setFacePoints(this._lineA, leftPts);
	IDB.Draw3D.setFacePoints(this._lineB, rightPts);

	if ( lblHold ) {
		var lblH = this._label.height;
		var r = model.getCameraHorizRotation();
		var isNeg = (model.getDatapoint().yValue.numberValue < 0);
		var push = (isNeg ? -1 : 1)*(lblH/2);
		
		if ( r > 45 ) {
			var lblPt = d3.project(lblVec);
			lblHold.x(lblPt.x);
			lblHold.y(lblPt.y+push);
		}
		else {
			lblHold.x(frontPts[1].x);
			lblHold.y(frontPts[1].y+push);
		}

		//Handle "limit" scenarios, where the label for a positive value extends below the grid base,
		//or the label for a negative value extends above the grid top.

		rp = relPts[1];
		v = new IDB.Vec3(px+rp.x, (isNeg ? maxY : 0), pz-thick/2);

		var limitPt = d3.project(v);
		var halfH = lblH/2;
		
		if ( !isNeg && lblHold.y()+halfH > limitPt.y ) {
			lblHold.y(limitPt.y-halfH);
		}
		else if ( isNeg && lblHold.y()-halfH < limitPt.y ) {
			lblHold.y(limitPt.y+halfH);
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Line3dSegmentView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getDatapoint().getVisualState();
	var isFade = (state == IDB.VS.FADED);
	var alpha = (isFade ? 0.15 : 1.0);
	var strokeW = (state == IDB.VS.HIGHLIGHTED ? 2 : 1);
	
	this._fillA.opacity(alpha);
	this._fillB.opacity(alpha);

	this._lineA
		.opacity(alpha)
		.strokeWidth(strokeW);

	this._lineB
		.opacity(alpha)
		.strokeWidth(strokeW);
	
	if  ( this._labelHold ) {
		this._labelHold.visible(!isFade);
	}
};

/*====================================================================================================*/

