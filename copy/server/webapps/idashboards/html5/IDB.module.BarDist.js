// Z_DOT file::IDB.module.BarDist.js z.99211177005683301.2019.04.09.21.25.11.299|file
/* global IDB, Kinetic, jQueryElement, Point, Unknown */
/**
 * Kinetic BarDist module.
 * @module BarDist
 * @see IDB.BarDistDatapointGrid
 * @see IDB.BarDistGraph
 */
'use strict';
// Z_DOT ctor BarDistDatapointGrid(datapointGrid, forceYCol):BarDistDatapointGrid  ::IDB.module.BarDist.js  z.25877777005683301.2019.04.09.21.36.17.852|
  /**
   * BarDistDatapointGrid constructor
   * @class BarDistDatapointGrid
   * @constructor
   * @memberof IDB
   *
   * @param {Object} datapointGrid
   * @param {Boolean} forceYCol
   *
   * @see z.25877777005683301
   * @see module:BarDist
   */
  IDB.BarDistDatapointGrid = function(datapointGrid, forceYCol) {
    this._dpGrid = datapointGrid;
    this._forceYCol = forceYCol;
    }; //-IDB.BarDistDatapointGrid
  // Z_DOT ppt getNumXRows():Number                           ::IDB.module.BarDist.js  z.59370877005683301.2019.04.09.21.36.47.395|+@.
    /**
     * Num X Rows
     * @memberof IDB.BarDistDatapointGrid
     * @method
     * @return {Number}
     *
     * @see z.59370877005683301
     */
    IDB.BarDistDatapointGrid.prototype.getNumXRows = function() {
      return this._dpGrid.getNumXRows();
      }; //-IDB.BarDistDatapointGrid.prototype.getNumXRows
  // Z_DOT ppt getNumYCols(numOnly):Number                    ::IDB.module.BarDist.js  z.31269877005683301.2019.04.09.21.38.16.213|+@.
    // noinspection JSUnusedLocalSymbols
    /**
     * Num Y Cols
     * @memberof IDB.BarDistDatapointGrid
     * @method
     *
     * @param {Boolean} numOnly
     * @return {Number} 1
     *
     * @see z.31269877005683301
     * @todo fix param or deprecate
     *
     */
    IDB.BarDistDatapointGrid.prototype.getNumYCols = function(numOnly) {
      return 1;
      }; //-IDB.BarDistDatapointGrid.prototype.getNumYCols
  // Z_DOT ppt getXAxis():Object                              ::IDB.module.BarDist.js  z.59312187005683301.2019.04.09.21.42.01.395|+@.
    /**
     * X Axis
     * @memberof IDB.BarDistDatapointGrid
     * @method
     *
     * @return {Object}
     *
     * @see z.59312187005683301
     */
    IDB.BarDistDatapointGrid.prototype.getXAxis = function() {
      return this._dpGrid.getXAxis();
      }; //-IDB.BarDistDatapointGrid.prototype.getXAxis
  // Z_DOT ppt getYAxis(yCol, numOnly):Object                 ::IDB.module.BarDist.js  z.82657187005683301.2019.04.09.21.42.55.628|+@.
    // noinspection JSUnusedLocalSymbols
    /**
     * Y Axis
     * @memberof IDB.BarDistDatapointGrid
     * @method
     *
     * @param {Number} yCol
     * @param {Boolean} numOnly
     *
     * @return {Object}
     *
     * @see z.82657187005683301
     * @todo Fix params, illogic
     *
     */
    IDB.BarDistDatapointGrid.prototype.getYAxis = function(yCol, numOnly) {
      return this._dpGrid.getYAxis(this._forceYCol, true);
      }; //-IDB.BarDistDatapointGrid.prototype.getYAxis
  // Z_DOT fn getDatapointAt(xRow, yCol, numOnly):Point       ::IDB.module.BarDist.js  z.84722287005683301.2019.04.09.21.43.42.748|+?
    // noinspection JSUnusedLocalSymbols
    /**
     * Datapoint At
     * @memberof IDB.BarDistDatapointGrid
     * @method
     *
     *
     * @param {Number} xRow
     * @param {Number} yCol
     * @param {Boolean} numOnly
     *
     * @return {Point}
     *
     * @see z.84722287005683301
     *
     */
    IDB.BarDistDatapointGrid.prototype.getDatapointAt = function(xRow, yCol, numOnly) {
      return this._dpGrid.getDatapointAt(xRow, this._forceYCol, true);
      }; //-IDB.BarDistDatapointGrid.prototype.getDatapointAt
  // Z_DOT fn getDatapointRowAt(xRow, numOnly):[Point,Point]  ::IDB.module.BarDist.js  z.69100387005683301.2019.04.09.21.45.00.196|+?
    /**
     * Datapoint Row At
     * @memberof IDB.BarDistDatapointGrid
     * @method
     *
     *
     * @param {Number} xRow
     * @param {Boolean} numOnly
     *
     * @return {Array}
     *
     * @see z.69100387005683301
     */
    IDB.BarDistDatapointGrid.prototype.getDatapointRowAt = function(xRow, numOnly) {
      return [
        this._dpGrid.getDatapointAt(xRow, 0, numOnly),
        this._dpGrid.getDatapointAt(xRow, 1, numOnly)
      ];
      }; //-IDB.BarDistDatapointGrid.prototype.getDatapointRowAt
  // Z_DOT ppt getStatSet(axisId):Object                      ::IDB.module.BarDist.js  z.51784387005683301.2019.04.09.21.45.48.715|+@.
    /**
     * Stat Set
     * @memberof IDB.BarDistDatapointGrid
     * @method
     *
     * @param {Unknown} axisId
     *
     * @return {Object}
     *
     * @see z.51784387005683301
     */
    IDB.BarDistDatapointGrid.prototype.getStatSet = function(axisId) {
      return this._dpGrid.getStatSet(axisId);
      }; //-IDB.BarDistDatapointGrid.prototype.getStatSet
// Z_DOT ctor BarDistGraph(chart, $div, graphProperties, dataSet):BarDistGraph     ::IDB.module.BarDist.js  z.35094487005683301.2019.04.09.21.47.29.053|class
  /**
   * BarDistGraph constructor
   * @class BarDistGraph
   * @constructor
   * @memberof IDB
   * @augments IDB.GraphBaseKinetic
   *
   * @param {Object} chart
   * @param {jQueryElement} $div
   * @param {Object} graphProperties
   * @param {Object} dataSet
   *
   * @see z.35094487005683301
   * @see module:BarDist
   */
  IDB.BarDistGraph = function(chart, $div, graphProperties, dataSet) {
    IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
    this._info.legendStyle = (this._graphContext.settings.getBool('BarFillRangeColors') ?
      IDB.LS.RANGES : IDB.LS.AXES);
    }; //-IDB.BarDistGraph
  IDB.BarDistGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
  IDB.BarDistGraph.prototype.constructor = IDB.BarDistGraph;
  // Z_DOT cmd _build():void                                    ::IDB.module.BarDist.js  z.23373587005683301.2019.04.09.21.48.57.332|-!
    /**
     * Build
     * @memberof IDB.BarDistGraph
     * @method
     *
     *
     * @see z.23373587005683301
     */
    IDB.BarDistGraph.prototype._build = function() {
      this._hold = new Kinetic.Group(undefined);
      this._stageLayer.add(this._hold);
      //
      var gc = this._graphContext;
      //
      ////
      //
      // noinspection JSCheckFunctionSignatures
      var gcGridL = this._cloneContext();
      gcGridL.settings.set('ReverseY', true);
      gcGridL.settings.get('XLabelOptions').show = false;
      this._gridL = new IDB.CartesianGrid(gcGridL);
      //
      // noinspection JSCheckFunctionSignatures
      var dpGridL = new IDB.BarDistDatapointGrid(gc.datapointGrid, 0);
      var gcBarL = this._cloneContext(dpGridL, gcGridL.settings);
      // noinspection JSCheckFunctionSignatures
      var modelL = new IDB.BarPlot(gcBarL, this._gridL.getAxes(), undefined, undefined, true);
      //
      this._limitL = new Kinetic.Group(undefined);
      //
      this._gridViewL = new IDB.CartesianGridView(this._gridL, this._limitL);
      this._hold.add(this._gridViewL.getDisplay());
      //
      this._plotViewL = new IDB.BarPlotView(modelL);
      this._hold.add(this._plotViewL.getDisplay());
      //
      this._hold.add(this._limitL);
      //
      ////
      //
      // noinspection JSCheckFunctionSignatures
      var gcGridR = this._cloneContext();
      gcGridR.settings.set('CenterXLabel', true);
      gcGridR.settings.get('XAxisLabelOptions').show = false;
      this._gridR = new IDB.CartesianGrid(gcGridR);
      //
      // noinspection JSCheckFunctionSignatures
      var dpGridR = new IDB.BarDistDatapointGrid(gc.datapointGrid, 1);
      var gcBarR = this._cloneContext(dpGridR, gcGridR.settings);
      // noinspection JSCheckFunctionSignatures
      var modelR = new IDB.BarPlot(gcBarR, this._gridR.getAxes());
      //
      this._limitR = new Kinetic.Group(undefined);
      //
      this._gridViewR = new IDB.CartesianGridView(this._gridR, this._limitR);
      this._hold.add(this._gridViewR.getDisplay());
      //
      this._plotViewR = new IDB.BarPlotView(modelR);
      this._hold.add(this._plotViewR.getDisplay());
      //
      this._hold.add(this._limitR);
      }; //-IDB.BarDistGraph.prototype._build
  // Z_DOT fn _cloneContext(dpGrid, settings):IDB.GraphContext  ::IDB.module.BarDist.js  z.69942687005683301.2019.04.09.21.50.24.996|-?
    /**
     * Clone Context
     * @memberof IDB.BarDistGraph
     * @method
     *
     *
     * @param {Object} dpGrid
     * @param {Object} settings
     *
     * @return {IDB.GraphContext}
     *
     * @see z.69942687005683301
     */
    IDB.BarDistGraph.prototype._cloneContext = function(dpGrid, settings) {
      var gc = this._graphContext;
      var clone = new IDB.GraphContext((settings ? settings : gc.settings.clone()),
        (dpGrid ? dpGrid : gc.datapointGrid), gc.refreshDisplay,
        gc.onDatapointClicked, gc.onDatapointOver, gc.onDatapointOut, gc.onYAxisSelected);
      //
      var getUpdateFunc = function(clonedContext) {
        return function() {
          clonedContext.notifyDatapointsUpdated();
        };
      };
      //
      gc.addDatapointsUpdatedListener(getUpdateFunc(clone));
      return clone;
      }; //-IDB.BarDistGraph.prototype._cloneContext
  // Z_DOT cmd _update(width, height):void                      ::IDB.module.BarDist.js  z.00709687005683301.2019.04.09.21.51.30.700|-!
    /**
     * Update
     * @memberof IDB.BarDistGraph
     * @method
     *
     *
     * @param {Number} width
     * @param {Number} height
     *
     * @see z.00709687005683301
     */
    IDB.BarDistGraph.prototype._update = function(width, height) {
      var gvL = this._gridViewL;
      var gvR = this._gridViewR;
      var pvL = this._plotViewL;
      var pvR = this._plotViewR;
      //
      // noinspection MagicNumberJS
      this._hold
        .rotation(90)
        .x(width);
      //
      gvL.setSize(height, width/2);
      gvR.setSize(height, width/2);
      //
      var wDiff = (gvL.getPlotHeight()-gvR.getPlotHeight())/2;
      //
      ////
      //
      this._limitL.y(width/2+wDiff+3);
      gvL.getDisplay().y(width/2+wDiff+3);
      gvL.setSize(height, width/2-wDiff-3);
      //
      pvL.setSize(gvL.getPlotWidth(), gvL.getPlotHeight());
      pvL.getDisplay()
        .x(gvL.getDisplay().x()+gvL.getPlotPosX())
        .y(gvL.getDisplay().y()+gvL.getPlotPosY())
        .clip({
          x: 0,
          y: -3,
          width: gvL.getPlotWidth(),
          height: gvL.getPlotHeight()+6
        });
      //
      ////
      //
      gvR.setSize(height, width/2+wDiff-3);
      //
      pvR.setSize(gvR.getPlotWidth(), gvR.getPlotHeight());
      pvR.getDisplay()
        .x(gvR.getDisplay().x()+gvR.getPlotPosX())
        .y(gvR.getDisplay().y()+gvR.getPlotPosY())
        .clip({
          x: 0,
          y: -3,
          width: gvR.getPlotWidth(),
          height: gvR.getPlotHeight()+6
        });
      }; //-IDB.BarDistGraph.prototype._update
//EOF
