// Z_DOT file::IDB.module.Bar.js z.03684204005683301.2019.04.09.11.10.48.630|file
/* global IDB, $, Kinetic, jQueryElement, Point */
/**
 * Kinetic Bar module.
 * @module Bar
 * @see IDB.BarBox
 * @see IDB.BarBoxView
 * @see IDB.BarGraph
 * @see IDB.BarPlot
 * @see IDB.BarPlotView
 */
'use strict';
// Z_DOT ctor BarBox(graphContext, datapoint, stats, relativeY, relativeHeight, labelText):BarBox                    ::IDB.module.Bar.js  z.93270154005683301.2019.04.09.12.31.47.239|class
  /**
   * BarBox constructor
   * @class BarBox
   * @constructor
   * @memberof IDB
   *
   * @param {Object} graphContext
   * @param {Point} datapoint
   * @param {Object} stats
   * @param {Number} relativeY
   * @param {Number} relativeHeight
   * @param {String} labelText
   *
   * @see z.93270154005683301
   */
  IDB.BarBox = function(graphContext, datapoint, stats, relativeY, relativeHeight, labelText) {
    this._graphContext = graphContext;
    this._datapoint = datapoint;
    this._relativeY = relativeY;
    this._relativeHeight = relativeHeight;
    this._labelText = labelText;
    //
    var s = graphContext.settings;
    //
    if ( s.getBool('Is3dGraph') ) {
      return;
    }
    //
    this._gradientData = {
      show: s.get('BarGradient'),
      effect: s.get('BarGradientEffect'),
      value: this._datapoint.yValue.numberValue,
      valueMin: stats.min,
      valueMax: stats.max,
      elevation: s.get('BarGradientElevation'),
      fade: s.get('BarGradientFade'),
      strength: s.get('BarGradientStrength')
    };
    }; //-IDB.BarBox
  // Z_DOT cmd setOnDatapointsUpdated(callback):void ::IDB.module.Bar.js  z.19754254005683301.2019.04.09.12.34.05.791|+!
    /**
     * On Datapoints Updated
     * @memberof IDB.BarBox
     * @method
     *
     * @param {Function} callback
     *
     * @see z.19754254005683301
     */
    IDB.BarBox.prototype.setOnDatapointsUpdated = function(callback) {
      this._graphContext.addDatapointsUpdatedListener(callback);
      }; //-IDB.BarBox.prototype.setOnDatapointsUpdated
  // Z_DOT ppt getDatapoint():Point                  ::IDB.module.Bar.js  z.95758354005683301.2019.04.09.12.36.25.759|+@.
    /**
     * Datapoint
     * @memberof IDB.BarBox
     * @method
     * @return {Point}
     *
     * @see z.
     */
    IDB.BarBox.prototype.getDatapoint = function() {
      return this._datapoint;
      }; //-IDB.BarBox.prototype.getDatapoint
  // Z_DOT ppt getRelativeY():Number                 ::IDB.module.Bar.js  z.54143454005683301.2019.04.09.12.37.14.145|+@.
    /**
     * Relative Y
     * @memberof IDB.BarBox
     * @method
     * @return {Number}
     *
     * @see z.54143454005683301
     */
    IDB.BarBox.prototype.getRelativeY = function() {
      return this._relativeY;
      }; //-IDB.BarBox.prototype.getRelativeY
  // Z_DOT ppt getRelativeHeight():Number            ::IDB.module.Bar.js  z.57513554005683301.2019.04.09.12.38.51.575|+@.
    /**
     * Relative Height
     * @memberof IDB.BarBox
     * @method
     * @return {Number}
     *
     * @see z.57513554005683301
     */
    IDB.BarBox.prototype.getRelativeHeight = function() {
      return this._relativeHeight;
      }; //-IDB.BarBox.prototype.getRelativeHeight
  // Z_DOT ppt getLabelText():String                 ::IDB.module.Bar.js  z.53987654005683301.2019.04.09.12.41.18.935|+@.
    /**
     * Label Text
     * @memberof IDB.BarBox
     * @method
     * @return {String}
     *
     * @see z.53987654005683301
     */
    IDB.BarBox.prototype.getLabelText = function() {
      return this._labelText;
      }; //-IDB.BarBox.prototype.getLabelText
  // Z_DOT ppt isHorizMode():Boolean                 ::IDB.module.Bar.js  z.70495064005683301.2019.04.09.12.47.39.407|+@.
    /**
     * Is Horiz Mode
     * @memberof IDB.BarBox
     * @method
     * @return {Boolean}
     *
     * @see z.70495064005683301
     */
    IDB.BarBox.prototype.isHorizMode = function() {
      return this._graphContext.settings.getBool('HorizMode');
      }; //-IDB.BarBox.prototype.isHorizMode
  // Z_DOT ppt getGradientData():Object              ::IDB.module.Bar.js  z.78040164005683301.2019.04.09.12.48.24.087|+@.
    /**
     * Gradient Data
     * @memberof IDB.BarBox
     * @method
     * @return {Object}
     *
     * @see z.78040164005683301
     */
    IDB.BarBox.prototype.getGradientData = function() {
      return this._gradientData;
      }; //-IDB.BarBox.prototype.getGradientData
  // Z_DOT ppt showShadow():Boolean                  ::IDB.module.Bar.js  z.93055164005683301.2019.04.09.12.49.15.039|+@.
    /**
     * Show Shadow
     * @memberof IDB.BarBox
     * @method
     * @return {Boolean}
     *
     * @see z.93055164005683301
     */
    IDB.BarBox.prototype.showShadow = function() {
      return this._graphContext.settings.getBool('BarShadow');
      }; //-IDB.BarBox.prototype.showShadow
  // Z_DOT ppt showShading():Boolean                 ::IDB.module.Bar.js  z.19332264005683301.2019.04.09.12.50.23.391|+@.
    /**
     * Show Shading
     * @memberof IDB.BarBox
     * @method
     * @return {Boolean}
     *
     * @see z.19332264005683301
     */
    IDB.BarBox.prototype.showShading = function() {
      return this._graphContext.settings.getBool('BarShading');
      }; //-IDB.BarBox.prototype.showShading
  // Z_DOT ppt fillWithRangeColor():Boolean          ::IDB.module.Bar.js  z.46874264005683301.2019.04.09.12.50.47.864|+@.
    /**
     * Fill With Range Color
     * @memberof IDB.BarBox
     * @method
     * @return {Boolean}
     *
     * @see z.46874264005683301
     */
    IDB.BarBox.prototype.fillWithRangeColor = function() {
      return this._graphContext.settings.getBool('BarFillRangeColors');
      }; //-IDB.BarBox.prototype.fillWithRangeColor
  // Z_DOT ppt getFillAlpha():                       ::IDB.module.Bar.js  z.49286264005683301.2019.04.09.12.51.08.294|+@.
    /**
     * Fill Alpha
     * @memberof IDB.BarBox
     * @method
     * @return {Number}
     *
     * @see z.49286264005683301
     */
    IDB.BarBox.prototype.getFillAlpha = function() {
      return this._graphContext.settings.getAlpha('BarFillTransparency');
      }; //-IDB.BarBox.prototype.getFillAlpha
  // Z_DOT ppt getBarLabelOptions():Object           ::IDB.module.Bar.js  z.86582364005683301.2019.04.09.12.52.08.568|+@.
    /**
     * Bar Label Options
     * @memberof IDB.BarBox
     * @method
     * @return {Object}
     *
     * @see z.86582364005683301
     */
    IDB.BarBox.prototype.getBarLabelOptions = function() {
      return this._graphContext.settings.get('BarLabelOptions');
      }; //-IDB.BarBox.prototype.getBarLabelOptions
  // Z_DOT ppt getThicknessRatio():Number            ::IDB.module.Bar.js  z.74674364005683301.2019.04.09.12.52.27.647|+@.
    /**
     * Thickness Ratio
     * for 3D mode
     * @memberof IDB.BarBox
     * @method
     * @return {Number}
     *
     * @see z.74674364005683301
     */
    IDB.BarBox.prototype.getThicknessRatio = function() {
      return this._graphContext.settings.get('BarThicknessRatio');
      }; //-IDB.BarBox.prototype.getThicknessRatio
  // Z_DOT ppt getCameraHorizRotation():Number       ::IDB.module.Bar.js  z.65676364005683301.2019.04.09.12.52.47.656|+@.
    /**
     * Camera Horiz Rotation
     * for 3D mode
     * @memberof IDB.BarBox
     * @method
     * @return {Number}
     *
     * @see z.65676364005683301
     */
    IDB.BarBox.prototype.getCameraHorizRotation = function() {
      return this._graphContext.settings.get('CameraHorizRotation');
      }; //-IDB.BarBox.prototype.getCameraHorizRotation
  // Z_DOT ppt isCylinderMode():Boolean              ::IDB.module.Bar.js  z.78080464005683301.2019.04.09.12.53.28.087|+@.
    /**
     * Is Cylinder Mode
     * for 3D mode
     * @memberof IDB.BarBox
     * @method
     * @return {Boolean}
     *
     * @see z.78080464005683301
     */
    IDB.BarBox.prototype.isCylinderMode = function() {
      return this._graphContext.settings.getBool('BarCylinderMode');
      }; //-IDB.BarBox.prototype.isCylinderMode
  // Z_DOT cmd createClickListener():Object          ::IDB.module.Bar.js  z.60252464005683301.2019.04.09.12.53.45.206|+!
    /**
     * Create Click Listener
     * @memberof IDB.BarBox
     * @method
     * @return {Object}
     *
     * @see z.60252464005683301
     */
    IDB.BarBox.prototype.createClickListener = function() {
      return IDB.leftClickListener(this, "_handleClick");
      }; //-IDB.BarBox.prototype.createClickListener
  // Z_DOT cmd createOverListener():Object           ::IDB.module.Bar.js  z.38177464005683301.2019.04.09.12.54.37.183|+!
    /**
     * Create Over Listener
     * @memberof IDB.BarBox
     * @method
     * @return {Object}
     *
     * @see z.38177464005683301
     */
    IDB.BarBox.prototype.createOverListener = function() {
      return IDB.listener(this, "_handleOver");
      }; //-IDB.BarBox.prototype.createOverListener
  // Z_DOT cmd createOutListener():Object            ::IDB.module.Bar.js  z.88059464005683301.2019.04.09.12.54.55.088|+!
    /**
     * Create Out Listener
     * @memberof IDB.BarBox
     * @method
     * @return {Object}
     *
     * @see z.88059464005683301
     */
    IDB.BarBox.prototype.createOutListener = function() {
      return IDB.listener(this, "_handleOut");
      }; //-IDB.BarBox.prototype.createOutListener
  // Z_DOT evt _handleClick():void                   ::IDB.module.Bar.js  z.37336036005683301.2019.04.09.17.31.03.373|-\
    /**
     * Handle Click
     * @memberof IDB.BarBox
     * @method
     *
     * @see z.37336036005683301
     */
    IDB.BarBox.prototype._handleClick = function() {
      this._graphContext.onDatapointClicked(this._datapoint);
      }; //-IDB.BarBox.prototype._handleClick
  // Z_DOT evt _handleOver():void                    ::IDB.module.Bar.js  z.49698036005683301.2019.04.09.17.31.29.694|-\
    /**
     * Handle Over
     * @memberof IDB.BarBox
     * @method
     *
     * @see z.49698036005683301
     */
    IDB.BarBox.prototype._handleOver = function() {
      this._graphContext.onDatapointOver(this._datapoint);
      }; //-IDB.BarBox.prototype._handleOver
  // Z_DOT evt _handleOut():void                     ::IDB.module.Bar.js  z.10731136005683301.2019.04.09.17.31.53.701|-\
    /**
     * Handle Out
     * @memberof IDB.BarBox
     * @method
     *
     * @see z.10731136005683301
     */
    IDB.BarBox.prototype._handleOut = function() {
      this._graphContext.onDatapointOut(this._datapoint);
      }; //-IDB.BarBox.prototype._handleOut
// Z_DOT ctor BarBoxView(model):BarBoxView                                                                           ::IDB.module.Bar.js  z.10993136005683301.2019.04.09.17.32.19.901|class
 /**
  * BarBoxView constructor
  * @class BarBoxView
  * @constructor
  * @memberof IDB
  *
  * @param {Object} model
  *
  * @see z.10993136005683301
  */
 IDB.BarBoxView = function(model) {
   this._model = model;
   model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
   this._buildKinetic();
   }; //-IDB.BarBoxView
// noinspection JSValidateTypes
Object.defineProperties(IDB.BarBoxView.prototype, {
  // Z_DOT ppt isAnimationEnabled:Boolean                                                     ::IDB.module.Bar.js  z.68625336005683301.2019.04.09.17.35.52.686|+.
    /**
     * Is Animation Enabled
     * @memberof IDB.BarBoxView.prototype
     * @type {Boolean}
     *
     *
     * @see z.68625336005683301
     */
    isAnimationEnabled:{
      enumerable: true,
      configurable: false,
      get: function() {
        var me = this;
        if (me._animationsEnabled === undefined){
          var model = me._model, ctx = model && model._graphContext ? model._graphContext : null,
            settings = ctx && ctx.settings ? ctx.settings : null;
          // noinspection AssignmentResultUsedJS
          return (me._animationsEnabled = settings && settings.animationEnabled ? settings.animationEnabled() : false);
        }
        return me._animationsEnabled;
      },
      set: function(val) {
        this._animationsEnabled = !!val;
      }
    },
  // Z_DOT ppt isAnimating:Boolean                                                            ::IDB.module.Bar.js  z.18347336005683301.2019.04.09.17.36.14.381|+.
    /**
     * Is Animating
     * @memberof IDB.BarBoxView.prototype
     * @type {Boolean}
     *
     *
     *
     * @see z.
     */
    isAnimating:{
      enumerable: true,
      configurable: false,
      get: function() {
        var me = this, animation = me._animation;
        return animation !== null && animation.isRunning();
      },
      set: function(val) {
        var me = this, animation = me._animation;
        if (val === false)
        {
          if (animation !== null)
          {
            if (animation.isRunning && animation.isRunning())
            {
              animation.stop();
            }
            else
            {
              me.doneAnimating = true;
            }
          }
          me._animation = null;
        }
      }
    },
  // Z_DOT ppt willAnimate:Boolean                                                            ::IDB.module.Bar.js  z.99358336005683301.2019.04.09.17.36.25.399|+@.
    /**
     * Will Animate
     * @memberof IDB.BarBoxView.prototype
     * @type {Boolean}
     *
     * @see z.99358336005683301
     */
    willAnimate: {
      enumerable: true,
      configurable: false,
      get: function() {
        return this._animationTimeoutOrPromise !== null;
      }
    },
  // Z_DOT ppt animationParametersChanged:Array                                               ::IDB.module.Bar.js  z.54619336005683301.2019.04.09.17.36.31.645|+@.
    /**
     * Animation Parameters Changed
     * @memberof IDB.BarBoxView.prototype
     * @type {Array}
     *
     * @see z.54619336005683301
     */
    animationParametersChanged: {
      enumerable: true,
      configurable: false,
      get: function() {
        var me = this, retArr = me._paramsChanged.slice();
        me._paramsChanged.length = 0;
        return retArr;
      }
    },
  // Z_DOT ppt didAnimationParametersChange:Boolean                                           ::IDB.module.Bar.js  z.35899336005683301.2019.04.09.17.36.39.853|+@.
    /**
     * Did Animation Parameters Change
     * @memberof IDB.BarBoxView.prototype
     * @type {Boolean}
     *
     * @see z.35899336005683301
     */
    didAnimationParametersChange: {
      enumerable: true,
      configurable:false,
      get: function() {
        return this._paramsChanged.length>0;
      }
    },
  // Z_DOT ppt doneAnimating:Boolean                                                          ::IDB.module.Bar.js  z.71180436005683301.2019.04.09.17.36.48.117|+.
    /**
     * Done Animating
     * @memberof IDB.BarBoxView.prototype
     * @type {Boolean}
     *
     * @see z.71180436005683301
     */
    doneAnimating: {
      value: false,
      enumerable: true,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _animation:Object                                                              ::IDB.module.Bar.js  z.75931436005683301.2019.04.09.17.36.53.957|+.
    /**
     * Animation
     * @memberof IDB.BarBoxView.prototype
     * @type {(null|Object)}
     *
     * @see z.75931436005683301
     */
    _animation: {
      enumerable: false,
      value: null,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _animationTimeoutOrPromise:Object                                              ::IDB.module.Bar.js  z.83802436005683301.2019.04.09.17.37.00.838|+.
    /**
     * Animation Timeout or Promise
     * @memberof IDB.BarBoxView.prototype
     * @type {(null|Object)}
     *
     * @see z.83802436005683301
     */
    _animationTimeoutOrPromise: {
      enumerable: false,
      value: null,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _animationsEnabled:Boolean                                                     ::IDB.module.Bar.js  z.14782436005683301.2019.04.09.17.37.08.741|+.
    /**
     * Animations Enabled
     * @memberof IDB.BarBoxView.prototype
     * @type {Boolean}
     *
     * @see z.14782436005683301
     */
    _animationsEnabled: {
      enumerable: false,
      value: undefined,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _paramsChanged:Array                                                           ::IDB.module.Bar.js  z.71933436005683301.2019.04.09.17.37.13.917|+.
    /**
     * Params Changed
     * @memberof IDB.BarBoxView.prototype
     * @type {Array}
     *
     * @see z.71933436005683301
     */
    _paramsChanged: {
      value: [],
      enumerable: false,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _animationParameters:Object                                                    ::IDB.module.Bar.js  z.96264436005683301.2019.04.09.17.37.26.269|+.
    /**
     * Animation Parameters
     * @memberof IDB.BarBoxView.prototype
     * @type {{groupX:Number, width:Number, opac:Number, barHighVis:Boolean, lblVis:Boolean, plotHeight:Number}}
     *
     * @see z.96264436005683301
     */
    _animationParameters: {
      value: {groupX:0.0, width:0.0, opac:1.0, barHighVis:false, lblVis:false, plotHeight:0.0},
      enumerable: false,
      configurable: false,
      writable: true
    }
  }); //-Object.defineProperties(IDB.BarBoxView.prototype
  // Z_DOT cmd _buildKinetic():void                                                           ::IDB.module.Bar.js  z.48488456005683301.2019.04.09.18.11.28.484|-!
    /**
     * Build Kinetic
     * @memberof IDB.BarBoxView
     *
     * @see z.48488456005683301
     */
    IDB.BarBoxView.prototype._buildKinetic = function() {
      var model = this._model;
      //
      var dp = model.getDatapoint();
      var fillColor = (model.fillWithRangeColor() ? dp.rangeColor : dp.axisColor);
      var fillAlpha = model.getFillAlpha();
      //
      if ( model.showShadow() ) {
        // noinspection MagicNumberJS
        this._shadow = new Kinetic.Rect({fill: IDB.toRGBA(0x0, 0.2*fillAlpha)});
      }
      //
      if ( model.getLabelText() ) {
        var lblOpt = model.getBarLabelOptions();
        this._label = new IDB.GraphLabel(lblOpt, model.getLabelText());
        // noinspection MagicNumberJS
        this._label.rotateFromCenter(lblOpt.rotation-(model.isHorizMode() ? 90 : 0));
        this._label.getDisplay().listening(false);
      }
      //
      this._bar = new Kinetic.Group(undefined);
      this._bar.on("click", model.createClickListener());
      this._bar.on("mouseover", model.createOverListener());
      this._bar.on("mouseout", model.createOutListener());
      this._touchHandler = new IDB.TouchHandler(this._bar);
      //
      this._barBox = new Kinetic.Rect({
        fill: IDB.toRGBA(fillColor, fillAlpha)
      });
      this._bar.add(this._barBox);
      //
      this._barCover = new Kinetic.Rect({
        fill: 'rgba(0, 0, 0, 0.001)'
      });
      this._bar.add(this._barCover);
      //
      if ( model.getGradientData().show ) {
        this._barGrad = new Kinetic.Rect(undefined);
        this._bar.add(this._barGrad);
      }
      //
      if ( model.showShading() ) {
        this._barShading = new Kinetic.Group(undefined);
        this._bar.add(this._barShading);
        //
        var gs = new IDB.GenericShading(IDB.GenericShading.TypeGloss, true);
        var gsDisp = gs.getDisplay();
        // noinspection MagicNumberJS
        gsDisp.rotation(-90);
        gsDisp.y(gsDisp.getHeight());
        this._barShading.add(gsDisp);
      }
      //
      // noinspection JSCheckFunctionSignatures
      this._barHigh = new Kinetic.Line({ closed: true });
      this._barHigh.points([{ x: 0, y: 0 }]);
      // noinspection MagicNumberJS
      this._barHigh.stroke(IDB.toRGBA(fillColor.x, 0.35));
      this._barHigh.strokeWidth(2);
      this._barHigh.visible(false);
      this._bar.add(this._barHigh);
      }; //-IDB.BarBoxView.prototype._buildKinetic
  // Z_DOT cmd runInitialAnimation(groupX, width, plotHeight, numBoxes, promiseOrDelay):void  ::IDB.module.Bar.js  z.76805366005683301.2019.04.09.18.25.50.867|+!
    /**
     * Run Initial Animation
     * @memberof IDB.BarBoxView
     *
     * @param {Object} groupX
     * @param {Number} width
     * @param {Number} plotHeight
     * @param {Number} numBoxes
     * @param {(Promise|Number)} promiseOrDelay
     *
     * @see z.76805366005683301
     */
    IDB.BarBoxView.prototype.runInitialAnimation = function(groupX, width, plotHeight, numBoxes, promiseOrDelay) {
      // noinspection OverlyComplexBooleanExpressionJS
      var me = this, isPromise = promiseOrDelay !== undefined && promiseOrDelay !== 0 && typeof promiseOrDelay === 'object' && promiseOrDelay.hasOwnProperty('then');
      if (me.isAnimating || me.willAnimate){
        // noinspection JSCheckFunctionSignatures
        me._updateAnimationParameters({groupX:groupX, width:width, plotHeight:plotHeight});
        return (isPromise && !!me._animation && me._animation.hasOwnProperty('promise')) ? me._animation.promise : undefined;
      }
      // noinspection JSCheckFunctionSignatures,NegatedConditionalExpressionJS
      var anim = me._makeBarAnimation(groupX, width, plotHeight, numBoxes!==0 ? numBoxes : 1);
      me._setAnimation(anim);
      if (isPromise){
        // noinspection AssignmentResultUsedJS,JSObjectNullOrUndefined
        return (me._animationTimeoutOrPromise = promiseOrDelay.then(function runChainedAnimation() {
          me._runAnimation();
          return anim.promise;
        }));
      }
      else { // noinspection OverlyComplexBooleanExpressionJS,JSObjectNullOrUndefined
        if (promiseOrDelay===0  ||
                (promiseOrDelay!==undefined && promiseOrDelay !== null && typeof promiseOrDelay === 'number' ||
                  (!!promiseOrDelay.constructr && promiseOrDelay.constructor===Number))) {
                me._animationTimeoutOrPromise = setTimeout(function runDelayedAnimation() {
                  me._runAnimation();
                }, +promiseOrDelay);
        }
      }
      }; //-IDB.BarBoxView.prototype.runInitialAnimation
  // Z_DOT cmd _runAnimation():void                                                           ::IDB.module.Bar.js  z.11004466005683301.2019.04.09.18.27.20.011|-!
    /**
     * Run Animation
     * @memberof IDB.BarBoxView
     *
     * @see z.11004466005683301
     */
    IDB.BarBoxView.prototype._runAnimation = function() {
      var me = this;
      me._bar.setVisible(true);
      me._animation.start();
      me._animationTimeoutOrPromise = null;
      }; //-IDB.BarBoxView.prototype._runAnimation
  // Z_DOT ppt _setAnimation(animationObj):void                                               ::IDB.module.Bar.js  z.70563666005683301.2019.04.09.18.30.36.507|-.
    /**
     * Set Animation
     * @memberof IDB.BarBoxView
     *
     * @param {Object} animationObj
     *
     * @see z.70563666005683301
     */
    IDB.BarBoxView.prototype._setAnimation = function(animationObj) {
      var me = this, anim = me._animation;
      // noinspection OverlyComplexBooleanExpressionJS,JSUnresolvedFunction
      if (me.isAnimating && anim.hasOwnProperty('id') && animationObj.hasOwnProprty('id') && anim.id !== animationObj.id) {
        if (anim.isRunning()) {anim.stop();}
      }
      me._animation = animationObj;
      }; //-IDB.BarBoxView.prototype._setAnimation
  // Z_DOT cmd _makeBarAnimation(gx, w, ph, num):Kinetic.Animation                            ::IDB.module.Bar.js  z.30026666005683301.2019.04.09.18.31.02.003|-!
    /**
     * Make Bar Animation
     * @memberof IDB.BarBoxView
     *
     * @param {Number} gx
     * @param {Number} w
     * @param {Number} ph
     * @param {Number} num
     * @return {Kinetic.Animation}
     *
     * @see z.30026666005683301
     */
    IDB.BarBoxView.prototype._makeBarAnimation = function(gx, w, ph, num) {
      // noinspection AssignmentToFunctionParameterJS
      num = num || 1;
      // noinspection MagicNumberJS,NestedConditionalExpressionJS,NegatedConditionalExpressionJS
      var me = this,
        model = me._model,
        disp = me._bar,
        bh = me._barHigh,
        g = me._barGrad,
        gVis = !!g,
        gStops = gVis ? me._calcGradientStops() :[],
        gEnd = {x:0, y:0},
        sh = me._shadow,
        lbl = me._label,
        lblPos = lbl ? model.getBarLabelOptions().position : 0,
        rh = model.getRelativeHeight(),
        ry = model.getRelativeY(),
        h = rh * ph,
        initY = ph * (1-ry),
        lblY = initY - h + lblPos * (h<=0 ? 1 : -1),
        lblDisp = !!lbl ? lbl.getDisplay() : {setOpacity: function(){}},
        fd = IDB.GenericShading.FullDimension,
        shadScaleX = w/fd,
        shYOffset = model.isHorizMode() ? -2 : 2, // Compute once
        box = me._barBox,
        factorh = 0.001 * h, // Initial factor, it'll be invisible, anyway
        x = gx-w/2,
        y = initY - factorh,
        cover = me._barCover,
        shad = me._barShading,
        showCover = (factorh>0?factorh:-factorh) < 6,
        dfd = num > 1 ? $.Deferred() : null,
        spd = num > 1 ? Math.max(1, num - (Math.hasOwnProperty('trunc') ? Math.trunc(num/2) : Math.floor(num/2))) : 1,
        dur = 400/spd,
        lblVis = model.getDatapoint().getVisualState() !== IDB.VS.FADED,
        opac = lblVis ? 1.0 : 0.2,
        bhVis = model.getDatapoint().getVisualState() === IDB.VS.HIGHLIGHTED,
        bShadow = !!sh,
        bShading = !!shad,
        highY = factorh <= 0 ? -1 : 1,
        retAnim = new Kinetic.Animation(function(frame) {
          if (me.didAnimationParametersChange){
            var updatedParams = me.animationParametersChanged, params = me._animationParameters,
              xUpdated = false, wUpdated = false, phChanged = false;
            for(var i = 0, numChanged = updatedParams.length; i < numChanged; ++i){
              var val = params[updatedParams[i]];
              switch(updatedParams[i]){
                case 'groupX':
                  // noinspection AssignmentToFunctionParameterJS
                  gx = val;
                  xUpdated = true;
                  break;
                case 'width':
                  // noinspection AssignmentToFunctionParameterJS
                  w = val;
                  shadScaleX = w/fd;
                  if (bShading) {shad.setScaleX(shadScaleX);}
                  if (bShadow) {sh.setWidth(w);}
                  // noinspection NestedAssignmentJS,AssignmentResultUsedJS
                  xUpdated = wUpdated = true;
                  break;
                case 'opac':
                  opac = val;
                  break;
                case 'barHighVis':
                  // noinspection AssignmentResultUsedJS
                  bh.setVisible(bhVis = val);
                  break;
                case 'lblVis':
                  if (!!lbl) {
                    // noinspection NestedAssignmentJS,AssignmentResultUsedJS
                    lbl.visible = lblVis = val;
                  }
                  break;
                case 'plotHeight':
                  // noinspection AssignmentToFunctionParameterJS
                  ph = val;
                  h = rh * ph;
                  initY = ph * (1-ry);
                  phChanged = true;
                  break;
                default:
              }
            }
            if (xUpdated){
              // noinspection AssignmentResultUsedJS
              disp.setX(x = gx-w/2);
              if (bShadow) {sh.setX(x + 2);}
              if (!!lbl) {lbl.x = x;}
            }
            if (wUpdated){
              box.setWidth(w);
              shadScaleX = w/fd;
            }
            if (phChanged && !!lbl){
              lblY = initY - h + lblPos * (h<=0 ? 1 : -1);
              lbl.y = IDB.GraphLabel.clampYPosition(lbl, lblY, ph);
            }
          }
          if (frame.time<dur){
            var factor = frame.time / dur;
            //
            factorh = h * factor;
            y = initY - factorh;
            highY = factorh <=0 ? -1 : 1;
            //
            if (bShadow){
              sh.setY(y+shYOffset).setHeight(factorh);
            }
            //
            if (lblVis && lblDisp) {lblDisp.setOpacity(factor);}
            //
            disp.setY(y).setOpacity(factor*opac);
            box.setHeight(factorh);
            //
            if (showCover && (factorh>0?factorh:-factorh)>=6) {
              // noinspection AssignmentResultUsedJS
              cover.setVisible(showCover = false);
            }
            else {
              if (!showCover && (factorh > 0 ? factorh : -factorh) >= 6) {
                // noinspection AssignmentResultUsedJS
                cover.setVisible(showCover = true).setWidth(w);
              }
            }
            //
            if(bShading) {shad.setScaleY(-factorh / fd);}
            //
            if (gVis){
              g.fillLinearGradientStartPoint({x:0, y:factorh});
              g.fillLinearGradientEndPoint(gEnd);
              g.fillLinearGradientColorStops(gStops);
              g.width(w);
              g.height(factorh);
            }
            //
            if (bhVis){
              bh.setPoints([
                1, highY,
                1, factorh-highY,
                w-1, factorh - highY,
                w-1, highY
              ]);
            }
          }
          else{
            this.stop();
            me.isAnimating = false;
            if (lblVis && lblDisp) {lblDisp.setOpacity(1.0);}
            disp.setOpacity(opac);
            me.setPositionAndSize(gx, w, ph);
            if (!!dfd) {dfd.resolve();}
          }
        }, disp.getLayer() || null);
      //
      // Create parameters unique to this box view instance
      var assignObj = function(o1, o2) { // Polyfill
        if (Object.hasOwnProperty('assign')) {return Object.assign(o1, o2);}
        for (var param in o2){
          if (o2.hasOwnProperty(param)) {o1[param] = o2[param];}
        }
        return o1;
      };
      me._animationParameters = assignObj(Object.create(null), {groupX:gx, width:w, opac:opac, barHighVis:bhVis, lblVis:lblVis, plotHeight:ph});
      //
      // Pre-animation setup
      // noinspection MagicNumberJS
      disp.setOpacity(0.001);
      disp.setVisible(false);
      if(bShadow){
        sh.setX(x+2).setY(y+shYOffset).setWidth(w).setHeight(factorh);
      }
      if (lbl){
        lbl.x = gx;
        lbl.y = IDB.GraphLabel.clampYPosition(lbl, lblY, ph);
        lblDisp.setOpacity(0.0);
      }
      disp.setX(x).setY(y);
      box.setWidth(w).setHeight(factorh);
      //
      cover.setVisible(showCover);
      if (showCover) {cover.setWidth(w).setHeight(6).setY(-3);}
      //
      if (bShading){
        shad.setScaleX(shadScaleX).setScaleY(-factorh/fd);
      }
      //
      if (gVis){
        g.fillLinearGradientStartPoint({x:0, y:factorh});
        g.fillLinearGradientEndPoint(gEnd);
        g.fillLinearGradientColorStops(gStops);
        g.setWidth(w).setHeight(factorh);
      }
      //
      if (!!dfd) {retAnim.promise = dfd.promise();}
      return retAnim;
      }; //-IDB.BarBoxView.prototype._makeBarAnimation
  // Z_DOT ppt setPositionAndSize(groupX, width, plotHeight):void                             ::IDB.module.Bar.js  z.63498766005683301.2019.04.09.18.33.09.436|+@.
    /**
     * Position And Size
     * @memberof IDB.BarBoxView
     *
     *
     *
     * @param {Number} groupX
     * @param {Number} width
     * @param {Number} plotHeight
     *
     * @see z.63498766005683301
     */
    IDB.BarBoxView.prototype.setPositionAndSize = function(groupX, width, plotHeight) {
      var model = this._model;
      var w = width;
      var h = model.getRelativeHeight()*plotHeight;
      var x = groupX-w/2;
      var y = (plotHeight-model.getRelativeY()*plotHeight)-h;
      var highY = (h <= 0 ? -1 : 1);
      var showCover = (Math.abs(h) < 6);
      //
      if ( this._shadow ) {
        this._shadow.x(x+2);
        this._shadow.y(y+2*(model.isHorizMode() ? -1 : 1));
        this._shadow.width(w);
        this._shadow.height(h);
      }
      //
      if ( this._label ) {
        var lblY = y + model.getBarLabelOptions().position*(h <= 0 ? 1 : -1);
        //
        this._label.x = groupX;
        this._label.y = IDB.GraphLabel.clampYPosition(this._label, lblY, plotHeight);
      }
      //
      this._bar.x(x);
      this._bar.y(y);
      //
      this._barBox.width(w);
      this._barBox.height(h);
      //
      this._barCover
        .visible(showCover)
        .width(width)
        .height(showCover ? 6 : 0)
        .y(showCover ? -3 : 0);
      //
      if ( this._barShading ) {
        var shadSize = IDB.GenericShading.FullDimension;
        this._barShading.scaleX(w/shadSize);
        this._barShading.scaleY(-h/shadSize);
      }
      //
      this._resizeGradient(w, h);
      //
      this._barHigh.points([
        1, highY,
        1, h-highY,
        w-1, h-highY,
        w-1, highY
      ]);
      }; //-IDB.BarBoxView.prototype.setPositionAndSize
  // Z_DOT cmd _resizeGradient(w, h):void                                                     ::IDB.module.Bar.js  z.32702866005683301.2019.04.09.18.33.40.723|-!
    /**
     * Resize Gradient
     * @memberof IDB.BarBoxView
     *
     * @param {Number} w
     * @param {Number} h
     *
     * @see z.32702866005683301
     */
    IDB.BarBoxView.prototype._resizeGradient = function(w, h) {
      var g = this._barGrad;
      //
      if ( !g ) {
        return;
      }
      //
      var model = this._model;
      var data = model.getGradientData();
      var val = data.value;
      // noinspection MagicNumberJS,EqualityComparisonWithCoercionJS
      var color = (data.effect == 0 ? 0xffffff : 0x0);
      var a0, a1;
      //
      // noinspection EqualityComparisonWithCoercionJS
      if ( 0 == data.elevation ) {
        a0 = (val >= 0 ? 0: val/data.valueMin);
        a1 = (val >= 0 ? val/data.valueMax : 0);
      }
      else {
        a0 = (val >= 0 ? 0 : 1);
        a1 = (val >= 0 ? 1 : 0);
      }
      //
      // noinspection EqualityComparisonWithCoercionJS
      if ( 1 == data.fade ) {
        a0 = 1-a0;
        a1 = 1-a1;
      }
      //
      a0 *= data.strength;
      a1 *= data.strength;
      //
      g.fillLinearGradientStartPoint({ x: 0, y: h });
      g.fillLinearGradientEndPoint({ x: 0, y: 0 });
      g.fillLinearGradientColorStops([0, IDB.toRGBA(color, a0), 1, IDB.toRGBA(color, a1)]);
      g.width(w);
      g.height(h);
      }; //-IDB.BarBoxView.prototype._resizeGradient
  // Z_DOT fn _calcGradientStops():Array                                                      ::IDB.module.Bar.js  z.99004866005683301.2019.04.09.18.34.00.099|-?
    /**
     * Calc Gradient Stops
     * @memberof IDB.BarBoxView
     * @return {Array}
     * @see z.99004866005683301
     */
    IDB.BarBoxView.prototype._calcGradientStops = function() {
      // noinspection MagicNumberJS
      var me = this, model = me._model, data = model.getGradientData(), val = data.value, str = data.strength,
        color = data.effect === 0 ? 0xffffff : 0, elevation = data.elevation, noElevation = elevation===0,
        maxVal = noElevation ? val/data.valueMin : 1, minVal = noElevation ? val/data.valueMax : 0,
        a0 = (val >= 0 ? 0 : maxVal) * str, a1 = (val>=0 ? minVal : 0) * str;
      //
      return [0, IDB.toRGBA(color, a0), 1, IDB.toRGBA(color, a1)];
      }; //-IDB.BarBoxView.prototype._calcGradientStops
  // Z_DOT ppt getBarDisplay():Kinetic.Group                                                  ::IDB.module.Bar.js  z.95247866005683301.2019.04.09.18.34.34.259|+@.
    /**
     * Bar Display
     * @memberof IDB.BarBoxView
     *
     * @see z.95247866005683301
     */
    IDB.BarBoxView.prototype.getBarDisplay = function() {
      return this._bar;
      }; //-IDB.BarBoxView.prototype.getBarDisplay
  // Z_DOT ppt getShadowDisplay():Boolean                                                     ::IDB.module.Bar.js  z.13739866005683301.2019.04.09.18.34.53.731|+@.
    /**
     * Shadow Display
     * @memberof IDB.BarBoxView
     * @return {Boolean}
     *
     * @see z.13739866005683301
     */
    IDB.BarBoxView.prototype.getShadowDisplay = function() {
      return this._shadow;
      }; //-IDB.BarBoxView.prototype.getShadowDisplay
  // Z_DOT ppt getLabelDisplay():Kinetic.Group                                                ::IDB.module.Bar.js  z.78741966005683301.2019.04.09.18.35.14.787|+@.
    /**
     * Label Display
     * @memberof IDB.BarBoxView
     * @return {Kinetic.Group.Group}
     *
     * @see z.78741966005683301
     */
    IDB.BarBoxView.prototype.getLabelDisplay = function() {
      return (this._label ? this._label.getDisplay() : null);
      }; //-IDB.BarBoxView.prototype.getLabelDisplay
  // Z_DOT cmd _updateAnimationParameters(params, quiet):void                                 ::IDB.module.Bar.js  z.59156966005683301.2019.04.09.18.36.05.195|-!
    /**
     * Update Animation Parameters
     * @memberof IDB.BarBoxView
     *
     * @param {Object} params
     * @param {Boolean} quiet
     *
     * @see z.59156966005683301
     */
    IDB.BarBoxView.prototype._updateAnimationParameters = function(params, quiet) {
      var me = this, animParams = me._animationParameters, validParams = Object.keys(animParams),
        changedParams = validParams.reduce(function validateParms(o, key) {
            var animParam = animParams[key];
            // noinspection OverlyComplexBooleanExpressionJS
          if (params.hasOwnProperty(key) && params[key]!==animParams[key] && !(Array.isArray(params[key] &&
              Array.isArray(animParam) && params[key].length === animParam.length &&
              params[key].every(function compareArrs(e, i) { return e === animParam[i]; })))){
            // noinspection AssignmentResultUsedJS,CommaExpressionJS
            return (o[key] = params[key]), o; // Wouldn't it be nice to just be able to use ES6? Object.assign(o, {[key]: params[key]});
            }
            return o;
          }, Object.create(null)),
        changedKeys = Object.keys(changedParams),
        alreadyChangedMap = me._paramsChanged.reduce(function alreadyChangedKeys(o, key) {
          // noinspection AssignmentResultUsedJS,CommaExpressionJS
          return (o[key] = true), o;
          }, Object.create(null));
      if (changedKeys.length > 0){
        for (var param in changedParams){
          // noinspection JSUnfilteredForInLoop
          if (!!param && typeof param === 'string' && animParams[param]!==undefined){
            // noinspection JSUnfilteredForInLoop
            me._animationParameters[param] = changedParams[param];
            // noinspection JSUnfilteredForInLoop
            alreadyChangedMap[param] = true;
          }
        }
        me._paramsChanged = Object.keys(alreadyChangedMap);
      }
      if (!!quiet){
        me._paramsChanged.length = 0;
      }
      }; //-IDB.BarBoxView.prototype._updateAnimationParameters
  // Z_DOT evt _onDatapointsUpdated():void                                                    ::IDB.module.Bar.js  z.30889966005683301.2019.04.09.18.36.38.803|-\
    /**
     * On Datapoints Updated
     * @memberof IDB.BarBoxView
     *
     * @see z.30889966005683301
     */
    IDB.BarBoxView.prototype._onDatapointsUpdated = function() {
      var state = this._model.getDatapoint().getVisualState();
      // noinspection EqualityComparisonWithCoercionJS
      var isFade = (state == IDB.VS.FADED);
      // noinspection MagicNumberJS
      var barAlpha = (isFade ? 0.2 : 1.0);
      var me = this, changeAnimation = me.isAnimationEnabled && !me.doneAnimating;
      //
      if (changeAnimation)
      {
        // noinspection JSCheckFunctionSignatures
        me._updateAnimationParameters({opac: barAlpha, lblVis: !isFade, barHighVis: state === IDB.VS.HIGHLIGHTED});
      }
      else
      {
        this._bar.setOpacity(1.0);
        this._barBox.opacity(barAlpha);
        // noinspection EqualityComparisonWithCoercionJS
        this._barHigh.visible(state == IDB.VS.HIGHLIGHTED);
        //
        if ( this._barShading ) {
          this._barShading.opacity(barAlpha);
        }
        //
        if ( this._barGrad ) {
          this._barGrad.opacity(barAlpha);
        }
        //
        if ( this._shadow ) {
          this._shadow.opacity(barAlpha);
        }
        //
        if ( this._label ) {
          this._label.visible = !isFade;
        }
      }
      }; //-IDB.BarBoxView.prototype._onDatapointsUpdated
// Z_DOT ctor BarGraph(chart, $div, graphProperties, dataSet):BarGraph                                               ::IDB.module.Bar.js  z.86032076005683301.2019.04.09.18.37.03.068|class
  /**
   * BarGraph constructor
   * @class BarGraph
   * @constructor
   * @memberof IDB
   *
   * @param {Object} chart
   * @param {jQueryElement} $div
   * @param {Object} graphProperties
   * @param {Object} dataSet
   *
   * @see z.86032076005683301
   */
  IDB.BarGraph = function(chart, $div, graphProperties, dataSet) {
    IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
    this._info.legendStyle = (this._graphContext.settings.getBool('BarFillRangeColors') ?
      IDB.LS.RANGES : IDB.LS.AXES);
    }; //-IDB.BarGraph
  IDB.BarGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
  IDB.BarGraph.prototype.constructor = IDB.BarGraph;
  // Z_DOT cmd _build():void                                                                                         ::IDB.module.Bar.js  z.65914076005683301.2019.04.09.18.37.21.956|-!
    /**
     * Build
     * @memberof IDB.BarGraph
     *
     * @see z.65914076005683301
     */
    IDB.BarGraph.prototype._build = function() {
      this._hold = new Kinetic.Group(undefined);
      this._stageLayer.add(this._hold);
      //
      this._grid = new IDB.CartesianGrid(this._graphContext);
      // noinspection JSCheckFunctionSignatures
      var model = new IDB.BarPlot(this._graphContext, this._grid.getAxes());
      //
      this._limitHold = new Kinetic.Group(undefined);
      //
      this._gridView = new IDB.CartesianGridView(this._grid, this._limitHold);
      this._hold.add(this._gridView.getDisplay());
      //
      this._plotView = new IDB.BarPlotView(model);
      this._hold.add(this._plotView.getDisplay());
      //
      this._hold.add(this._limitHold);
      }; //-IDB.BarGraph.prototype._build
  // Z_DOT cmd _update(width, height):void                                                                           ::IDB.module.Bar.js  z.39966076005683301.2019.04.09.18.37.46.993|-!
    /**
     * Update
     * @memberof IDB.BarGraph
     *
     * @param {Number} width
     * @param {Number} height
     *
     * @see z.39966076005683301
     */
    IDB.BarGraph.prototype._update = function(width, height) {
      var hold = this._hold;
      var gv = this._gridView;
      var pv = this._plotView;
      var pvd = pv.getDisplay();
      //
      if ( this._graphContext.settings.getBool('HorizMode') ) {
        // noinspection MagicNumberJS
        hold.rotation(90);
        hold.x(width);
        gv.setSize(height, width);
      }
      else {
        hold.rotation(0);
        hold.x(0);
        gv.setSize(width, height);
      }
      //
      pvd.x(gv.getPlotPosX());
      pvd.y(gv.getPlotPosY());
      pv.setSize(gv.getPlotWidth(), gv.getPlotHeight());
      //
      pvd.clip({
        x: 0,
        y: -3,
        width: gv.getPlotWidth(),
        height: gv.getPlotHeight()+6
      });
      }; //-IDB.BarGraph.prototype._update
// Z_DOT ctor BarGroup(graphContext, cartesianGridAxes, xIndex, displayXIndex, startYIndex, displayYCount):BarGroup  ::IDB.module.Bar.js  z.42098076005683301.2019.04.09.18.38.09.024|class
  /**
   * BarGroup constructor
   * @class BarGroup
   * @constructor
   * @memberof IDB
   *
   * @param {Object} graphContext
   * @param {Object} cartesianGridAxes
   * @param {Number} xIndex
   * @param {Number} displayXIndex
   * @param {Number} startYIndex
   * @param {Number} displayYCount
   *
   * @see z.42098076005683301
   */
  IDB.BarGroup = function(graphContext, cartesianGridAxes, xIndex, displayXIndex, startYIndex, displayYCount) {
    this._graphContext = graphContext;
    this._cartesianGridAxes = cartesianGridAxes;
    this._xIndex = xIndex;
    this._displayXIndex = displayXIndex;
    this._startYIndex = startYIndex;
    this._displayYCount = displayYCount;
    //
    this._active = false;
    this._sum = this._calcSum();
    this._boxes = this._buildBoxes();
    }; //-IDB.BarGroup
  // Z_DOT fn _calcSum():Number                   ::IDB.module.Bar.js  z.50360176005683301.2019.04.09.18.38.26.305|-?
    /**
     * Calc Sum
     * @memberof IDB.BarGroup
     * @return {Number}
     *
     * @see z.50360176005683301
     */
    IDB.BarGroup.prototype._calcSum = function() {
      var sum = 0;
      var dg = this._graphContext.datapointGrid;
      var yCount = dg.getNumYCols(true);
      var dp, i;
      //
      for ( i = 0 ; i < yCount ; ++i ) {
        dp = dg.getDatapointAt(this._xIndex, i, true);
        sum += Math.abs(dp.yValue.numberValue);
      }
      //
      return sum;
      }; //-IDB.BarGroup.prototype._calcSum
  // Z_DOT cmd _buildBoxes():void                 ::IDB.module.Bar.js  z.35122176005683301.2019.04.09.18.38.42.153|-!
    /**
     * Build Boxes
     * @memberof IDB.BarGroup
     *
     * @see z.35122176005683301
     */
    IDB.BarGroup.prototype._buildBoxes = function() {
      var boxes = [];
      var gc = this._graphContext;
      var dg = gc.datapointGrid;
      var sett = gc.settings;
      var stackStyle = sett.getBool('BarStackedMode');
      var ratioStyle = sett.getBool('BarRatioMode');
      var clusterStyle = (!stackStyle && !ratioStyle);
      var showLabel = sett.get('BarLabelOptions').show;
      var labelStackedSum = sett.getBool('BarLabelStackedSum');
      var cartGridAxes = this._cartesianGridAxes;
      var yCount = this._displayYCount;
      var revMode = sett.getOptionalBool('ReverseY');
      var numVal = 0;
      var nextRelY = 0;
      var xi = this._xIndex;
      var stats, diff, relY, box, boxY, boxH, dp, yAxisId, i, lblText;
      //
      for ( i = this._startYIndex ; i < yCount ; ++i ) {
        dp = dg.getDatapointAt(xi, i, true);
        yAxisId = dp.yAxis.axisId;
        stats = cartGridAxes.getYGridAxisById(yAxisId).getStatsByAxis(yAxisId);
        diff = stats.max-stats.min;
        relY = nextRelY;
        //
        if ( stackStyle ) {
          numVal += dp.yValue.numberValue;
        }
        else if ( ratioStyle ) {
          // noinspection MagicNumberJS,NegatedConditionalExpressionJS,EqualityComparisonWithCoercionJS
          numVal += (this._sum != 0 ? dp.yValue.numberValue/this._sum*100 : 0);
        }
        else {
          numVal = dp.yValue.numberValue;
        }
        //
        // noinspection EqualityComparisonWithCoercionJS
        if ( clusterStyle || 0 == i ) {
          relY = -stats.min/diff; //start at "zero" position
        }
        //
        nextRelY = (numVal-stats.min)/diff;
        lblText = null;
        //
        if ( showLabel ) {
          if ( labelStackedSum ) {
            // noinspection EqualityComparisonWithCoercionJS
            lblText = (i == yCount-1 ? sett.formatNumber(numVal) : null);
          }
          else {
            lblText = dp.yValue.getFV(true);
          }
        }
        //
        boxY = (revMode ? 1-relY : relY);
        boxH = (nextRelY-relY)*(revMode ? -1 : 1);
        //
        box = new IDB.BarBox(gc, dp, stats, boxY, boxH, lblText);
        boxes.push(box);
      }
      //
      return boxes;
      }; //-IDB.BarGroup.prototype._buildBoxes
  // Z_DOT ppt setActive(active):                 ::IDB.module.Bar.js  z.50364176005683301.2019.04.09.18.39.06.305|+.
    /**
     * Active
     * @memberof IDB.BarGroup
     *
     * @param {Boolean} active
     *
     * @see z.50364176005683301
     */
    IDB.BarGroup.prototype.setActive = function(active) {
      this._active = active;
      }; //-IDB.BarGroup.prototype.setActive
  // Z_DOT ppt isActive():Boolean                 ::IDB.module.Bar.js  z.39527176005683301.2019.04.09.18.39.32.593|+.
    /**
     * Is Active
     * @memberof IDB.BarGroup
     * @return {Boolean}
     *
     * @see z.39527176005683301
     */
    IDB.BarGroup.prototype.isActive = function() {
      return this._active;
      }; //-IDB.BarGroup.prototype.isActive
  // Z_DOT ppt getDisplayXIndex():Number          ::IDB.module.Bar.js  z.63309176005683301.2019.04.09.18.39.50.336|+@.
    /**
     * Display X Index
     * @memberof IDB.BarGroup
     * @return {Number}
     *
     * @see z.63309176005683301
     */
    IDB.BarGroup.prototype.getDisplayXIndex = function() {
      return this._displayXIndex;
      }; //-IDB.BarGroup.prototype.getDisplayXIndex
  // Z_DOT ppt getBoxCount():Number               ::IDB.module.Bar.js  z.94441276005683301.2019.04.09.18.40.14.449|+@.
    /**
     * Box Count
     * @memberof IDB.BarGroup
     * @return {Number}
     *
     * @see z.94441276005683301
     */
    IDB.BarGroup.prototype.getBoxCount = function() {
      return this._boxes.length;
      }; //-IDB.BarGroup.prototype.getBoxCount
  // Z_DOT ppt getBox(i):Object                   ::IDB.module.Bar.js  z.88292276005683301.2019.04.09.18.40.29.288|+@.
    /**
     * Box
     * @memberof IDB.BarGroup
     * @return {Object}
     *
     * @param {Number} i
     *
     * @see z.88292276005683301
     */
    IDB.BarGroup.prototype.getBox = function(i) {
      return this._boxes[i];
      }; //-IDB.BarGroup.prototype.getBox
  // Z_DOT ppt getClusterThicknessRatio():Number  ::IDB.module.Bar.js  z.80056276005683301.2019.04.09.18.41.05.008|+@.
    /**
     * Cluster Thickness Ratio
     * @memberof IDB.BarGroup
     * @return {Number}
     *
     * @see z.80056276005683301
     */
    IDB.BarGroup.prototype.getClusterThicknessRatio = function() {
      return this._graphContext.settings.get('BarThicknessRatioInCluster');
      }; //-IDB.BarGroup.prototype.getClusterThicknessRatio
// Z_DOT ctor BarPlot(graphContext, cartesianGridAxes, startYIndex, yAxisDisplayCount):BarPlot                       ::IDB.module.Bar.js  z.21597276005683301.2019.04.09.18.41.19.512|class
  /**
   * BarPlot constructor
   * @class BarPlot
   * @constructor
   * @memberof IDB
   *
   * @param {Object} graphContext
   * @param {Object} cartesianGridAxes
   * @param {Number} startYIndex
   * @param {Number} yAxisDisplayCount
   *
   * @see z.21597276005683301
   */
  IDB.BarPlot = function(graphContext, cartesianGridAxes, startYIndex, yAxisDisplayCount) {
    this._graphContext = graphContext;
    this._cartesianGridAxes = cartesianGridAxes;
    this._startYIndex = IDB.ifUndef(startYIndex, 0);
    //
    var yAxisCount = graphContext.datapointGrid.getNumYCols(true);
    this._yYAxisDisplayCount = (yAxisDisplayCount > 0 ? yAxisDisplayCount : yAxisCount);
    //
    var sett = graphContext.settings;
    //
    if ( !sett.getBool('Is3dGraph') ) {
      var lo = sett.get('BarLabelOptions');
      lo.position = sett.get('BarLabelPos');
      lo.rotation = sett.get('BarLabelRotation');
    }
    //
    this._groups = this._buildGroups();
    }; //-IDB.BarPlot
  // Z_DOT cmd _buildGroups():[IDB.BarGroup]    ::IDB.module.Bar.js  z.69089276005683301.2019.04.09.18.41.38.096|-!
    /**
     * Build Groups
     * @memberof IDB.BarPlot
     * @return {Array<IDB.BarGroup>}
     *
     *
     * @see z.69089276005683301
     */
    IDB.BarPlot.prototype._buildGroups = function() {
      var groups = [];
      var gc = this._graphContext;
      var cartGridAxes = this._cartesianGridAxes;
      var xCount = gc.datapointGrid.getNumXRows();
      //
      var startX = 0; //LATER: gc.settings.get('HiddenStartXIndex');
      var startY = this._startYIndex;
      var displayXCount = xCount; //LATER: gc.settings.get('numRowsDisplayed');
      var displayYCount = this._yYAxisDisplayCount;
      var activeCount = 0;
      var i, g, displayI, isActive;
      //
      for ( i = 0 ; i < xCount ; ++i ) {
        displayI = i-startX;
        isActive = (i >= startX && displayI < displayXCount);
        activeCount += (isActive ? 1 : 0);
        //
        g = new IDB.BarGroup(gc, cartGridAxes, i, displayI, startY, displayYCount);
        g.setActive(isActive);
        groups.push(g);
      }
      //
      this._activeGroupCount = activeCount;
      return groups;
      }; //-IDB.BarPlot.prototype._buildGroups
  // Z_DOT ppt getGroupCount():Number           ::IDB.module.Bar.js  z.48351376005683301.2019.04.09.18.41.55.384|+@.
    /**
     * Group Count
     * @memberof IDB.BarPlot
     * @return {Number}
     *
     * @see z.48351376005683301
     */
    IDB.BarPlot.prototype.getGroupCount = function() {
      return this._groups.length;
      }; //-IDB.BarPlot.prototype.getGroupCount
  // Z_DOT ppt getActiveGroupCount():Number     ::IDB.module.Bar.js  z.21913376005683301.2019.04.09.18.42.11.912|+@.
    /**
     * Active Group Count
     * @memberof IDB.BarPlot
     * @return {Number}
     *
     * @see z.21913376005683301
     */
    IDB.BarPlot.prototype.getActiveGroupCount = function() {
      return this._groups.length;
      }; //-IDB.BarPlot.prototype.getActiveGroupCount
  // Z_DOT ppt getGroup(i):IDB.BarGroup         ::IDB.module.Bar.js  z.82186376005683301.2019.04.09.18.42.48.128|+@.
    /**
     * Group
     * @memberof IDB.BarPlot
     * @return IDB.BarGroup
     *
     * @param {Number} i
     *
     * @see z.82186376005683301
     */
    IDB.BarPlot.prototype.getGroup = function(i) {
      return this._groups[i];
      }; //-IDB.BarPlot.prototype.getGroup
  // Z_DOT ppt isClusterMode():Boolean          ::IDB.module.Bar.js  z.44968376005683301.2019.04.09.18.43.06.944|+@.
    /**
     * Is Cluster Mode
     * @memberof IDB.BarPlot
     * @return {Boolean}
     *
     *
     * @see z.44968376005683301
     */
    IDB.BarPlot.prototype.isClusterMode = function() {
      var sett = this._graphContext.settings;
      return (!sett.getBool('BarStackedMode') && !sett.getBool('BarRatioMode'));
      }; //-IDB.BarPlot.prototype.isClusterMode
  // Z_DOT ppt getGroupThicknessRatio():Number  ::IDB.module.Bar.js  z.80650476005683301.2019.04.09.18.43.25.608|+@.
    /**
     * Group Thickness Ratio
     * @memberof IDB.BarPlot
     * @return {Number}
     *
     * @see z.80650476005683301
     */
    IDB.BarPlot.prototype.getGroupThicknessRatio = function() {
      return this._graphContext.settings.get('BarThicknessRatio');
      }; //-IDB.BarPlot.prototype.getGroupThicknessRatio
// Z_DOT ctor BarPlotView(model):BarPlotView                                                                         ::IDB.module.Bar.js  z.05032476005683301.2019.04.09.18.43.43.050|class
  /**
   * BarPlotView constructor
   * @class BarPlotView
   * @constructor
   * @memberof IDB
   *
   * @param {Object} model
   *
   * @see z.05032476005683301
   */
  IDB.BarPlotView = function(model) {
    this._model = model;
    this._buildKinetic();
    this._boxGrid = this._buildBoxGrid();
    }; //-IDB.BarPlotView
  // Z_DOT cmd _buildKinetic():void         ::IDB.module.Bar.js  z.96154476005683301.2019.04.09.18.44.05.169|-!
    /**
     * Build Kinetic
     * @memberof IDB.BarPlotView
     *
     * @see z.96154476005683301
     */
    IDB.BarPlotView.prototype._buildKinetic = function() {
      this._display = new Kinetic.Group(undefined);
      //
      this._shadowHold = new Kinetic.Group(undefined);
      this._barHold = new Kinetic.Group(undefined);
      this._labelHold = new Kinetic.Group(undefined);
      //
      this._display.add(this._shadowHold);
      this._display.add(this._barHold);
      this._display.add(this._labelHold);
      }; //-IDB.BarPlotView.prototype._buildKinetic
  // Z_DOT fn _buildBoxGrid():Array         ::IDB.module.Bar.js  z.52636476005683301.2019.04.09.18.44.23.625|-?
    /**
     * Build Box Grid
     * @memberof IDB.BarPlotView
     *
     * @see z.52636476005683301
     */
    IDB.BarPlotView.prototype._buildBoxGrid = function() {
      var boxGrid = [];
      var plotModel = this._model;
      var shadHold = this._shadowHold;
      var barHold = this._barHold;
      var lblHold = this._labelHold;
      var groupCount = plotModel.getGroupCount();
      var i, j, boxGridRow, groupModel, boxCount, box;
      //
      for ( i = 0 ; i < groupCount ; ++i ) {
        boxGridRow = [];
        boxGrid[i] = boxGridRow;
        groupModel = plotModel.getGroup(i);
        boxCount = groupModel.getBoxCount();
        //
        for ( j = 0 ; j < boxCount ; ++j ) {
          box = new IDB.BarBoxView(groupModel.getBox(j));
          barHold.add(box.getBarDisplay());
          //
          if ( box.getShadowDisplay() ) {
            shadHold.add(box.getShadowDisplay());
          }
          //
          if ( box.getLabelDisplay() ) {
            lblHold.add(box.getLabelDisplay());
          }
          //
          boxGridRow.push(box);
        }
      }
      //
      return boxGrid;
      }; //-IDB.BarPlotView.prototype._buildBoxGrid
  // Z_DOT ppt getDisplay():Kinetic.Group   ::IDB.module.Bar.js  z.54378476005683301.2019.04.09.18.44.47.345|+@.
    /**
     * Display
     * @memberof IDB.BarPlotView
     * @return {Kinetic.Group.Group}
     *
     * @see z.54378476005683301
     */
    IDB.BarPlotView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.BarPlotView.prototype.getDisplay
  // Z_DOT ppt setSize(width, height):void  ::IDB.module.Bar.js  z.16130576005683301.2019.04.09.18.45.03.161|+@.
    /**
     * Size
     * @memberof IDB.BarPlotView
     *
     * @param {Number} width
     * @param {Number} height
     *
     * @see z.16130576005683301
     */
    IDB.BarPlotView.prototype.setSize = function(width, height) {
      var plotModel = this._model;
      var boxGrid = this._boxGrid;
      var groupFullW = width/plotModel.getActiveGroupCount();
      var groupW = groupFullW*plotModel.getGroupThicknessRatio();
      var clusterMode = plotModel.isClusterMode();
      var i, j, boxGridRow, groupModel, box, groupX, barFullW, barW;
      // noinspection MagicNumberJS
      var animDelay = 35;
      //
      // Note for clarity: Thomas Cavalli did NOT write the following for loops, but notices their utility in traversing an arrays...
      //
      for ( i in boxGrid ) {
        // noinspection JSUnfilteredForInLoop
        boxGridRow = boxGrid[i];
        // noinspection JSUnfilteredForInLoop
        groupModel = plotModel.getGroup(i);
        //
        // noinspection MagicNumberJS
        groupX = (groupModel.getDisplayXIndex()+0.5)*groupFullW;
        barFullW = groupW/groupModel.getBoxCount();
        barW = barFullW*groupModel.getClusterThicknessRatio();
        //
        if ( clusterMode ) {
          groupX -= (groupW-barFullW)/2; //shift so the group (not its first bar) is centered
        }
        //
        // If we're animatng, things get tricky right about now:
        // * Clustered graphs animate all boxes in boxGridRow simultaneously, and delayed depending on their position
        //   in the plot (left-most animations begin first). From left to right, animations are delayed, but they DON'T
        //   necessarily wait for the cluster to the left's animation to complete (don't believe me? get a screen video capture
        //   utility, record the animation, and then watch the result in slow motion).
        //
        // * Non-clustered graphs are only stacked or ratio graphs (see the implementation of IDB.BarPlot.prototype.isClusterMode).
        //   In these cases, not only are each grouping animated from left-to-right in the same delayed fashion as clustered graphs,
        //   but each box in the group must wait for the first box to animate to completion before subsequent box animations within
        //   the group can begin animation. We'll have to chain these animations.
        var curChainPromise = $.Deferred().resolve();
        //
        for ( j in boxGridRow ) {
          // noinspection JSUnfilteredForInLoop
          box = boxGridRow[j];
          //
          if (box.isAnimationEnabled && !box.doneAnimating)
          {
            if (clusterMode) {
              box.runInitialAnimation(groupX + barFullW * j, barW, height, 1, animDelay * i);
            } else {
              curChainPromise = box.runInitialAnimation(groupX, groupW, height, boxGridRow.length, curChainPromise);
            }
          }
          else
          {
            if ( clusterMode ) {
              box.setPositionAndSize(groupX+barFullW*j, barW, height);
            }
            else {
              box.setPositionAndSize(groupX, groupW, height);
            }
          }
        }
      }
      }; //-IDB.BarPlotView.prototype.setSize
//EOF

