// Z_DOT file::IDB.module.Area3d.js z.94030603005683301.2019.04.09.08.30.03.049|file
/* global IDB, Kinetic, Kinetic.Group, jQueryElement, Point*/
/**
 * Kinetic Area3d module.
 * @module Area3d
 * @see IDB.Area3dAxisView
 * @see IDB.Area3dGraph
 * @see IDB.Area3dPlotView
 * @see IDB.Area3dSegmentView
 */
'use strict';
// Z_DOT ctor Area3dAxisView(draw3d, model, isClusterMode):Area3dAxisView              ::IDB.module.Area3d.js  z.33113053005683301.2019.04.09.09.43.51.133|class
  /**
   * Area3dAxisView constructor
   * @class Area3dAxisView
   * @constructor
   * @memberof IDB
   *
   * @param {Object} draw3d
   * @param {Object} model
   * @param {Boolean} isClusterMode
   *
   * @see z.33113053005683301
   * @see module:Area3d
   */
  IDB.Area3dAxisView = function(draw3d, model, isClusterMode) {
    this._draw3d = draw3d;
    this._model = model;
    this._isClusterMode = isClusterMode;
    this._buildKinetic();
    }; //-IDB.Area3dAxisView
  // Z_DOT cmd _buildKinetic():void                  ::IDB.module.Area3d.js  z.92646053005683301.2019.04.09.09.44.24.629|-!
    /**
     * Build Kinetic
     * @memberof IDB.Area3dAxisView
     * @private
     * @method
     * @see z.92646053005683301
     */
    IDB.Area3dAxisView.prototype._buildKinetic = function() {
      var draw3d = this._draw3d;
      var model = this._model;
      var segCount = model.getSegmentCount();
      var lblOpt = model.getLabelOptions();
      var i, segModel, seg, lbl, lblText;
      //
      this._segments = [];
      this._labels = [];
      //
      for ( i = 0 ; i < segCount ; ++i ) {
        segModel = model.getSegment(i);
        seg = new IDB.Area3dSegmentView(draw3d, segModel, i, segCount);
        this._segments[i] = seg;
      }
      //
      if ( !lblOpt.show ) {
        return;
      }
      //
      for ( i = 0 ; i < segCount ; ++i ) {
        segModel = model.getSegment(i);
        lblText = segModel.getLabelText();
        //
        if ( !segModel.getActive() || !lblText ) {
          continue;
        }
        //
        lbl = new IDB.GraphLabel(lblOpt, lblText);
        lbl.rotateFromCenter(lblOpt.rotation);
        lbl.getDisplay().listening(false);
        //
        this._segments[i].setLabelRef(lbl);
        this._labels[i] = lbl;
      }
      }; //-IDB.Area3dAxisView.prototype._buildKinetic
  // Z_DOT ppt getSegments():[IDB.Area3dSegmentView]  ::IDB.module.Area3d.js  z.39010153005683301.2019.04.09.09.45.01.093|+.
    /**
     * Segments
     * @memberof IDB.Area3dAxisView
     * @method
     * @return {Array<IDB.Area3dSegmentView>} this._segments
     * @see z.39010153005683301
     * @see IDB.Area3dSegmentView
     */
    IDB.Area3dAxisView.prototype.getSegments = function() {
      return this._segments;
      }; //-IDB.Area3dAxisView.prototype.getSegments
  // Z_DOT ppt getLabels():[IDB.GraphLabel]             ::IDB.module.Area3d.js  z.10580153005683301.2019.04.09.09.45.08.501|+.
    /**
     * Labels
     * @memberof IDB.Area3dAxisView
     * @method
     * @return {Array<IDB.GraphLabel>} this._labels
     * @see z.10580153005683301
     * @see IDB.GraphLabel
     */
    IDB.Area3dAxisView.prototype.getLabels = function() {
      return this._labels;
      }; //-IDB.Area3dAxisView.prototype.getLabels
  // Z_DOT cmd setMaxDimensions(maxDims):void         ::IDB.module.Area3d.js  z.18951153005683301.2019.04.09.09.45.15.981|+!
    /**
     * Max Dimensions
     * @memberof IDB.Area3dAxisView
     * @method
     * @param {Point} maxDims
     * @see z.18951153005683301
     */
    IDB.Area3dAxisView.prototype.setMaxDimensions = function(maxDims) {
      var model = this._model;
      var isClusterMode = this._isClusterMode;
      var segments = this._segments;
      var axisIndex = model.getYAxisIndex();
      var maxDimY = maxDims.y;
      //
      for ( var i in segments ) {
        segments[i].setPositionAndSize(i, (isClusterMode ? axisIndex : 0), maxDimY);
      }
      }; //-IDB.Area3dAxisView.prototype.setMaxDimensions
  // Z_DOT cmd render():void                          ::IDB.module.Area3d.js  z.12602153005683301.2019.04.09.09.45.20.621|+!
    /**
     * Render
     * @memberof IDB.Area3dAxisView
     * @method
     * @see z.12602153005683301
     */
    IDB.Area3dAxisView.prototype.render = function() {
      var segments = this._segments;
      //
      for ( var i in segments ) {
        segments[i].render();
      }
      }; //-IDB.Area3dAxisView.prototype.render
// Z_DOT ctor Area3dGraph(chart, $div, graphProperties, dataSet):Area3dGraph           ::IDB.module.Area3d.js  z.61362153005683301.2019.04.09.09.45.26.316|class
  /**
   * Area3dGraph constructor
   * @constructor
   * @memberof IDB
   * @augments IDB.GraphBaseKinetic
   *
   * @param {Object} chart
   * @param {jQueryElement} $div
   * @param {Object} graphProperties
   * @param {Object} dataSet
   *
   * @see z.61362153005683301
   * @see module:Area3d
   */
  IDB.Area3dGraph = function(chart, $div, graphProperties, dataSet) {
    IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
    this._info.legendStyle = IDB.LS.AXES;
    }; //-IDB.Area3dGraph
    IDB.Area3dGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
    IDB.Area3dGraph.prototype.constructor = IDB.Area3dGraph;
  // Z_DOT cmd _build():void                ::IDB.module.Area3d.js  z.58223153005683301.2019.04.09.09.45.32.285|-!
    /**
     * Build
     * @memberof IDB.Area3dGraph
     * @method
     * @private
     * @see z.58223153005683301
     * @see IDB.CartesianGrid
     * @see IDB.AreaPlot
     * @see IDB.Cartesian3dGridView
     * @see IDB.Area3dPlotView
     */
    IDB.Area3dGraph.prototype._build = function() {
      var gc = this._graphContext;
      var grid = new IDB.CartesianGrid(gc);
      var model = new IDB.AreaPlot(gc, grid.getAxes());
      //
      this._gridView = new IDB.Cartesian3dGridView(grid);
      this._stageLayer.add(this._gridView.getDisplay());
      //
      var plotHold = this._gridView.getPlotDisplay();
      var draw3d = this._gridView.getDraw3d();
      //
      this._plotView = new IDB.Area3dPlotView(draw3d, model);
      plotHold.add(this._plotView.getDisplay());
      }; //-IDB.Area3dGraph.prototype._build
  // Z_DOT cmd _update(width, height):void  ::IDB.module.Area3d.js  z.98994153005683301.2019.04.09.09.45.49.989|-!
    /**
     * Update
     * @memberof IDB.Area3dGraph
     * @method
     * @private
     * @param {Number} width
     * @param {Number} height
     * @see z.98994153005683301
     */
    IDB.Area3dGraph.prototype._update = function(width, height) {
      var gv = this._gridView;
      var pv = this._plotView;
      //
      gv.updateScreen(width, height);
      gv.updateCamera();
      gv.render();
      //
      pv.setMaxDimensions(gv.getMaxDimensions());
      pv.render();
      }; //-IDB.Area3dGraph.prototype._update
// Z_DOT ctor Area3dPlotView(draw3d, model):Area3dPlotView                             ::IDB.module.Area3d.js  z.50895153005683301.2019.04.09.09.45.59.805|class
  /**
   * Area3dPlotView constructor
   * @constructor
   * @memberof IDB
   *
   * @param {Object} draw3d
   * @param {Object} model
   *
   * @see z.50895153005683301
   * @see module:Area3d
   */
  IDB.Area3dPlotView = function(draw3d, model) {
    this._draw3d = draw3d;
    this._model = model;
    this._buildKinetic();
    }; //-IDB.Area3dPlotView
  // Z_DOT cmd _buildKinetic():void            ::IDB.module.Area3d.js  z.96646153005683301.2019.04.09.09.46.04.669|-!
    /**
     * Build Kinetic
     * @memberof IDB.Area3dPlotView
     * @method
     * @private
     * @see z.96646153005683301
     */
    IDB.Area3dPlotView.prototype._buildKinetic = function() {
      var draw3d = this._draw3d;
      var model = this._model;
      var axisCount = model.getAxisCount();
      var isClusterMode = model.isClusterMode();
      var zIndexes = [];
      var rightHold = null;
      var frontHold = null;
      var i, j, axis, segments, labels, segCount, seg, rightFaces, frontFaces, zInd;
      //
      this._display = new Kinetic.Group(undefined);
      this._axes = [];
      //
      var segHold = new Kinetic.Group(undefined);
      this._display.add(segHold);
      //
      if ( !isClusterMode ) {
        rightHold = new Kinetic.Group(undefined);
        this._display.add(rightHold);
        //
        frontHold = new Kinetic.Group(undefined);
        this._display.add(frontHold);
      }
      //
      var labelHold = new Kinetic.Group(undefined);
      this._display.add(labelHold);
      //
      for ( i = 0 ; i < axisCount ; ++i ) {
        axis = new IDB.Area3dAxisView(draw3d, model.getAxis(i), isClusterMode);
        segments = axis.getSegments();
        labels = axis.getLabels();
        segCount = segments.length;
        //
        this._axes[i] = axis;
        //
        for ( j = 0 ; j < segCount ; ++j ) {
          seg = segments[j];
          //
          zIndexes.push({
            z: (isClusterMode ? (axisCount-i-1)*segCount+j : i*segCount+j),
            seg: seg,
            segDisp: seg.getDisplay()
          });
        }
        //
        for ( j in labels ) {
          // noinspection JSUnfilteredForInLoop
          labelHold.add(labels[j].getDisplay());
        }
      }
      //
      zIndexes.sort(function(a, b) {
        return a.z-b.z;
      });
      //
      for ( i in zIndexes ) {
        zInd = zIndexes[i];
        segHold.add(zInd.segDisp);
        //
        if ( isClusterMode ) {
          continue;
        }
        //
        zInd.seg.prepareForFaceSeparation();
        rightFaces = zInd.seg.getRightFaces();
        frontFaces = zInd.seg.getFrontFaces();
        //
        for ( j in rightFaces ) {
          // noinspection JSObjectNullOrUndefined,JSUnfilteredForInLoop
          rightHold.add(rightFaces[j]);
        }
        //
        for ( j in frontFaces ) {
          // noinspection JSObjectNullOrUndefined,JSUnfilteredForInLoop
          frontHold.add(frontFaces[j]);
        }
      }
      }; //-IDB.Area3dPlotView.prototype._buildKinetic
  // Z_DOT ppt getDisplay():Kinetic.Group      ::IDB.module.Area3d.js  z.75907153005683301.2019.04.09.09.46.10.957|+@.
    /**
     * Display
     * @memberof IDB.Area3dPlotView
     * @method
     * @return {Kinetic.Group.Group} this._display
     * @see z.75907153005683301
     */
    IDB.Area3dPlotView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.Area3dPlotView.prototype.getDisplay
  // Z_DOT ppt setMaxDimensions(maxDims):void  ::IDB.module.Area3d.js  z.82418153005683301.2019.04.09.09.46.21.428|+@.
    /**
     * Max Dimensions
     * @memberof IDB.Area3dPlotView
     * @method
     * @param {Object} maxDims
     * @see z.82418153005683301
     */
    IDB.Area3dPlotView.prototype.setMaxDimensions = function(maxDims) {
      var axes = this._axes;
      //
      for ( var i in axes ) {
        axes[i].setMaxDimensions(maxDims);
      }
      }; //-IDB.Area3dPlotView.prototype.setMaxDimensions
  // Z_DOT cmd render():void                   ::IDB.module.Area3d.js  z.35688153005683301.2019.04.09.09.46.28.653|+!
    /**
     * Render
     * @memberof IDB.Area3dPlotView
     * @method
     * @see z.35688153005683301
     */
    IDB.Area3dPlotView.prototype.render = function() {
      var axes = this._axes;
      //
      for ( var i in axes ) {
        axes[i].render();
      }
      }; //-IDB.Area3dPlotView.prototype.render
// Z_DOT ctor Area3dSegmentView(draw3d, model, index, segmentCount):Area3dSegmentView  ::IDB.module.Area3d.js  z.90949153005683301.2019.04.09.09.46.34.909|class
  /**
   * Area3dSegmentView constructor
   * @constructor
   * @memberof IDB
   *
   * @param {Object} draw3d
   * @param {Object} model
   * @param {Number} index
   * @param {Number} segmentCount
   *
   * @see z.90949153005683301
   * @see module:Area3d
   */
  IDB.Area3dSegmentView = function(draw3d, model, index, segmentCount) {
    this._draw3d = draw3d;
    this._model = model;
    this._segRelWidth = 1/segmentCount;
    // noinspection EqualityComparisonWithCoercionJS
    this._isFirst = (0 == index);
    // noinspection EqualityComparisonWithCoercionJS
    this._isLast = (index == segmentCount-1);
    this._buildKinetic();
    }; //-IDB.Area3dSegmentView
  // Z_DOT cmd _buildKinetic():void                                ::IDB.module.Area3d.js  z.58899153005683301.2019.04.09.09.46.39.885|-!
    /**
     * Build Kinetic
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     * @see z.58899153005683301
     */
    IDB.Area3dSegmentView.prototype._buildKinetic = function() {
      var model = this._model;
      //
      this._display = new Kinetic.Group(undefined);
      this._addHoverListeners(this._display);
      //
      this._labelRef = null;
      this._facesSeparated = false;
      //
      if ( !model.getActive() ) {
        return;
      }
      //
      model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
      //
      var dp = model.getDatapoint();
      var fillRel = model.getRelativeFillPoints();
      var a = fillRel[0];
      var b = fillRel[1];
      var c = fillRel[2];
      var baseA = fillRel[5];
      var baseB = fillRel[4];
      var baseC = fillRel[3];
      var abInter = null;
      var bcInter = null;
      var col = dp.axisColor;
      var alpha = model.getFillAlpha();
      var t;
      //
      // noinspection MagicNumberJS
      this._fillDarkest = IDB.toRGBA(IDB.Draw3D.shadeColor(col.i, -0.6), alpha);
      // noinspection MagicNumberJS
      this._fillDark = IDB.toRGBA(IDB.Draw3D.shadeColor(col.i, -0.25), alpha);
      this._fillNormal = IDB.toRGBA(col, alpha);
      // noinspection MagicNumberJS
      this._fillLight = IDB.toRGBA(IDB.Draw3D.shadeColor(col.i, 0.25), alpha);
      //
      ////
      //
      var getIntersectPerc = function(ay, by, baseAy, baseBy) {
        //ay+slope*t = baseAy+baseSlope*t
        //(slope-baseSlope)*t = baseAy-ay
        //t = (baseAy-ay) / (slope-baseSlope)
        return (baseAy-ay)/(by-ay-baseBy+baseAy);
      };
      //
      var getIntersectPoint = function(a, b, t) {
        // noinspection MagicNumberJS
        return {
          x: a.x+t*0.5,
          y: a.y+(b.y-a.y)*t
        };
      };
      //
      // noinspection EqualityComparisonWithCoercionJS
      if ( (a.y > baseA.y) != (b.y > baseB.y) ) { //if AB intersects BaseAB
        t = getIntersectPerc(a.y, b.y, baseA.y, baseB.y);
        abInter = getIntersectPoint(a, b, t);
      }
      //
      // noinspection EqualityComparisonWithCoercionJS
      if ( (b.y > baseB.y) != (c.y > baseC.y) ) { //if BC intersects BaseBC
        t = getIntersectPerc(b.y, c.y, baseB.y, baseC.y);
        bcInter = getIntersectPoint(b, c, t);
      }
      //
      this._faceGroups = [];
      //
      // noinspection IfStatementWithTooManyBranchesJS
      if ( abInter && bcInter ) {
        this._faceGroups = [
          this._buildTriFaces(baseA, a, abInter, -1),
          this._buildPolyFaces(abInter, b, bcInter, null, baseB, null),
          this._buildTriFaces(bcInter, c, baseC, 1)
        ];
      }
      else if ( abInter ) {
        this._faceGroups = [
          this._buildTriFaces(baseA, a, abInter, -1),
          this._buildPolyFaces(abInter, b, c, baseC, baseB, null)
        ];
      }
      else if ( bcInter ) {
        this._faceGroups = [
          this._buildPolyFaces(a, b, bcInter, null, baseB, baseA),
          this._buildTriFaces(bcInter, c, baseC, 1)
        ];
      }
      else {
        this._faceGroups = [
          this._buildPolyFaces(a, b, c, baseC, baseB, baseA)
        ];
      }
      }; //-IDB.Area3dSegmentView.prototype._buildKinetic
  // Z_DOT cmd _buildPolyFaces(a, b, c, baseC, baseB, baseA):void  ::IDB.module.Area3d.js  z.52580253005683301.2019.04.09.09.46.48.525|-!
    /**
     * Build Poly Faces
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Point} a
     * @param {Point} b
     * @param {Point} c
     * @param {Point} baseC
     * @param {Point} baseB
     * @param {Point} baseA
     *
     * @see z.52580253005683301
     */
    IDB.Area3dSegmentView.prototype._buildPolyFaces = function(a, b, c, baseC, baseB, baseA) {
      var model = this._model;
      var disp = this._display;
      var fillNormal = this._fillNormal;
      var fillDark = this._fillDark;
      var fillDarkest = this._fillDarkest;
      var dp = model.getDatapoint();
      var upsideDown = (b.y > baseB.y);
      var left = null;
      var right = null;
      var back, baseAb, baseBc, ab, bc, front;
      var lineBaseMid, lineBaseBack, lineBack, lineBaseFront, lineFront, lineMid;
      //
      var lineConfig = {
        closed: true
      };
      //
      var fillAb = this._getAngledFaceFill(a, b, upsideDown);
      var fillBc = this._getAngledFaceFill(b, c, upsideDown);
      //
      ////
      //
      lineConfig.fill = fillDark;
      // noinspection JSCheckFunctionSignatures
      back = new Kinetic.Line(lineConfig);
      //
      if ( baseC ) {
        lineConfig.fill = fillNormal;
        lineConfig.visible = this._isFirst;
        //
        // noinspection JSCheckFunctionSignatures
        left = new Kinetic.Line(lineConfig);
        left.defaultVisible = lineConfig.visible;
        //
        lineConfig.visible = true;
      }
      //
      lineConfig.fill = (upsideDown ? fillDarkest : fillNormal);
      // noinspection JSCheckFunctionSignatures
      baseAb = new Kinetic.Line(lineConfig);
      // noinspection JSCheckFunctionSignatures
      baseBc = new Kinetic.Line(lineConfig);
      //
      lineConfig.fill = fillAb;
      // noinspection JSCheckFunctionSignatures
      ab = new Kinetic.Line(lineConfig);
      //
      lineConfig.fill = fillBc;
      // noinspection JSCheckFunctionSignatures
      bc = new Kinetic.Line(lineConfig);
      //
      if ( baseA ) {
        lineConfig.fill = fillDark;
        lineConfig.visible = this._isLast;
        //
        // noinspection JSCheckFunctionSignatures
        right = new Kinetic.Line(lineConfig);
        right.defaultVisible = lineConfig.visible;
        //
        lineConfig.visible = true;
      }
      //
      lineConfig.fill = fillNormal;
      // noinspection JSCheckFunctionSignatures
      front = new Kinetic.Line(lineConfig);
      //
      ////
      //
      lineConfig.closed = false;
      lineConfig.stroke = dp.axisColor.css;
      lineConfig.strokeWidth = 1;
      lineConfig.lineJoin = 'round';
      lineConfig.lineCap = 'butt';
      //
      // noinspection JSCheckFunctionSignatures
      lineBaseBack = new Kinetic.Line(lineConfig);
      // noinspection JSCheckFunctionSignatures
      lineBaseMid = new Kinetic.Line(lineConfig);
      // noinspection JSCheckFunctionSignatures
      lineBaseFront = new Kinetic.Line(lineConfig);
      // noinspection JSCheckFunctionSignatures
      lineBack = new Kinetic.Line(lineConfig);
      // noinspection JSCheckFunctionSignatures
      lineMid = new Kinetic.Line(lineConfig);
      // noinspection JSCheckFunctionSignatures
      lineFront = new Kinetic.Line(lineConfig);
      //
      ////
      //
      disp.add(upsideDown ? lineMid : lineBaseMid);
      disp.add(lineBaseBack);
      disp.add(lineBack);
      disp.add(back);
      //
      if ( left ) {
        disp.add(left);
      }
      //
      if ( upsideDown ) {
        disp.add(ab);
        disp.add(bc);
        disp.add(baseAb);
        disp.add(baseBc);
      }
      else {
        disp.add(baseAb);
        disp.add(baseBc);
        disp.add(ab);
        disp.add(bc);
      }
      //
      if ( right ) {
        disp.add(right);
      }
      //
      disp.add(front);
      disp.add(lineBaseFront);
      disp.add(lineFront);
      disp.add(upsideDown ? lineBaseMid : lineMid);
      //
      ////
      //
      return {
        isPoly: true,
        points: [a, b, c, baseC, baseB, baseA],
        faces: [back, left, baseAb, baseBc, ab, bc, right, front],
        lines: [lineBaseBack, lineBaseMid, lineBaseFront, lineBack, lineMid, lineFront]
      };
      }; //-IDB.Area3dSegmentView.prototype._buildPolyFaces
  // Z_DOT cmd _buildTriFaces(a, b, c, side):void                  ::IDB.module.Area3d.js  z.94122253005683301.2019.04.09.09.47.02.149|-!
    /**
     * Build Tri Faces
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Point} a
     * @param {Point} b
     * @param {Point} c
     * @param {Number} side
     *
     * @see z.94122253005683301
     */
    IDB.Area3dSegmentView.prototype._buildTriFaces = function(a, b, c, side) {
      var model = this._model;
      var disp = this._display;
      var fillNormal = this._fillNormal;
      var fillDark = this._fillDark;
      var fillDarkest = this._fillDarkest;
      var upsideDown = (a.y < b.y);
      var dp = model.getDatapoint();
      var back, ab, bc, ca, front, lineBack, lineFront, lineMid;
      //
      var lineConfig = {
        closed: true
      };
      //
      var fillAb = this._getAngledFaceFill(a, b, upsideDown);
      var fillBc = this._getAngledFaceFill(b, c, upsideDown);
      //
      ////
      //
      lineConfig.fill = fillDark;
      // noinspection JSCheckFunctionSignatures
      back = new Kinetic.Line(lineConfig);
      //
      lineConfig.fill = fillAb;
      lineConfig.visible = (side !== -1 || this._isFirst);
      // noinspection JSCheckFunctionSignatures
      ab = new Kinetic.Line(lineConfig);
      ab.defaultVisible = lineConfig.visible;
      //
      lineConfig.fill = fillBc;
      lineConfig.visible = (side !== 1 || this._isLast);
      // noinspection JSCheckFunctionSignatures
      bc = new Kinetic.Line(lineConfig);
      bc.defaultVisible = lineConfig.visible;
      bc.isRightSide = (side === 1);
      //
      lineConfig.fill = (upsideDown ? fillDarkest : fillNormal);
      lineConfig.visible = true;
      // noinspection JSCheckFunctionSignatures
      ca = new Kinetic.Line(lineConfig);
      //
      lineConfig.fill = fillNormal;
      // noinspection JSCheckFunctionSignatures
      front = new Kinetic.Line(lineConfig);
      //
      ////
      //
      lineConfig.closed = false;
      lineConfig.stroke = dp.axisColor.css;
      lineConfig.strokeWidth = 1;
      lineConfig.lineJoin = 'round';
      lineConfig.lineCap = 'butt';
      //
      // noinspection JSCheckFunctionSignatures
      lineBack = new Kinetic.Line(lineConfig);
      // noinspection JSCheckFunctionSignatures
      lineFront = new Kinetic.Line(lineConfig);
      //
      lineConfig.dash = [4, 3];
      // noinspection JSCheckFunctionSignatures
      lineMid = new Kinetic.Line(lineConfig);
      //
      ////
      //
      disp.add(lineBack);
      disp.add(back);
      //
      if ( upsideDown ) {
        disp.add(ab);
        disp.add(bc);
        disp.add(ca);
      }
      else {
        disp.add(ca);
        disp.add(ab);
        disp.add(bc);
      }
      //
      disp.add(front);
      disp.add(lineFront);
      disp.add(lineMid);
      //
      return {
        isPoly: false,
        points: [a, b, c],
        faces: [back, ab, bc, ca, front],
        lines: [lineBack, lineFront, lineMid]
      };
      }; //-IDB.Area3dSegmentView.prototype._buildTriFaces
  // Z_DOT ppt _getAngledFaceFill(a, b, upsideDown):Object         ::IDB.module.Area3d.js  z.02203253005683301.2019.04.09.09.47.10.220|-.
    /**
     * Angled Face Fill
     *
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Point} a
     * @param {Point} b
     * @param {Boolean} upsideDown
     * @return {Object} IDB.toRGBA
     * @see z.02203253005683301
     * @see IDB.toRGBA
     */
    IDB.Area3dSegmentView.prototype._getAngledFaceFill = function(a, b, upsideDown) {
      var model = this._model;
      var ci = model.getDatapoint().axisColor.i;
      var shade = IDB.Draw3D.getAngledFaceShade(a, b, this._segRelWidth, upsideDown);
      return IDB.toRGBA(IDB.Draw3D.shadeColor(ci, shade), model.getFillAlpha());
      }; //-IDB.Area3dSegmentView.prototype._getAngledFaceFill
  // Z_DOT cmd _addHoverListeners(disp):void                       ::IDB.module.Area3d.js  z.21263253005683301.2019.04.09.09.47.16.212|-!
    /**
     * Add Hover Listeners
     *
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Kinetic.Group.Group} disp
     *
     * @see z.21263253005683301
     */
    IDB.Area3dSegmentView.prototype._addHoverListeners = function(disp) {
      var model = this._model;
      //
      // noinspection JSUnresolvedFunction
      disp.on("click", model.createClickListener());
      // noinspection JSUnresolvedFunction
      disp.on("mouseover", model.createOverListener());
      // noinspection JSUnresolvedFunction
      disp.on("mouseout", model.createOutListener());
      // noinspection JSUndefinedPropertyAssignment
      disp._touchHandler = new IDB.TouchHandler(disp);
      }; //-IDB.Area3dSegmentView.prototype._addHoverListeners
  // Z_DOT ppt getDisplay():Kinetic.Group                          ::IDB.module.Area3d.js  z.98514253005683301.2019.04.09.09.47.21.589|+.
    /**
     * Display
     * @memberof IDB.Area3dSegmentView
     * @method
     * @return {Kinetic.Group.Group} this._display
     * @see z.98514253005683301
     */
    IDB.Area3dSegmentView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.Area3dSegmentView.prototype.getDisplay
  // Z_DOT ppt getLabelRef():Object                                ::IDB.module.Area3d.js  z.21274253005683301.2019.04.09.09.47.27.212|+.
    /**
     * Label Ref
     * @memberof IDB.Area3dSegmentView
     * @method
     * @return {Object} this._labelRef
     * @see z.21274253005683301
     */
    IDB.Area3dSegmentView.prototype.getLabelRef = function() {
      return this._labelRef;
      }; //-IDB.Area3dSegmentView.prototype.getLabelRef
  // Z_DOT cmd prepareForFaceSeparation():void                     ::IDB.module.Area3d.js  z.67625253005683301.2019.04.09.09.47.32.676|+!
    /**
     * Prepare For Face Separation
     * @memberof IDB.Area3dSegmentView
     * @method
     *
     * @see z.67625253005683301
     */
    IDB.Area3dSegmentView.prototype.prepareForFaceSeparation = function() {
      this._facesSeparated = true;
      //
      var faceGroups = this._faceGroups;
      var i, grp, right, front;
      //
      for ( i in faceGroups ) {
        // noinspection JSUnfilteredForInLoop
        grp = faceGroups[i];
        right = grp.faces[grp.isPoly ? 6 : 2];
        front = grp.faces[grp.isPoly ? 7 : 3];
        //
        this._addHoverListeners(front);
        //
        if ( right && (grp.isPoly || right.isRightSide) ) {
          right.listening(false);
        }
      }
      }; //-IDB.Area3dSegmentView.prototype.prepareForFaceSeparation
  // Z_DOT ppt getFrontFaces():[Object]                            ::IDB.module.Area3d.js  z.31085253005683301.2019.04.09.09.47.38.013|+.
    /**
     * Front Faces
     * @memberof IDB.Area3dSegmentView
     * @method
     * @return {Array<Object>} this._getFaces(7, 4, null)
     * @see z.31085253005683301
     */
    IDB.Area3dSegmentView.prototype.getFrontFaces = function() {
      return this._getFaces(7, 4, null);
      }; //-IDB.Area3dSegmentView.prototype.getFrontFaces
  // Z_DOT ppt getRightFaces():[Object]                            ::IDB.module.Area3d.js  z.80126253005683301.2019.04.09.09.47.42.108|+.
    /**
     * Right Faces
     * @memberof IDB.Area3dSegmentView
     * @method
     * @return {Array<Object>}
     * @see z.80126253005683301
     */
    IDB.Area3dSegmentView.prototype.getRightFaces = function() {
      return this._getFaces(6, 2, function(grp, face) {
        return (grp.isPoly || face.isRightSide);
      });
      }; //-IDB.Area3dSegmentView.prototype.getRightFaces
  // Z_DOT ppt _getFaces(polyI, triI, filter):[Object]             ::IDB.module.Area3d.js  z.06276253005683301.2019.04.09.09.47.47.260|+.
    /**
     * Faces
     *
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Object} polyI
     * @param {Object} triI
     * @param {Function} filter
     * @return {Array<Object>}
     * @see z.06276253005683301
     */
    IDB.Area3dSegmentView.prototype._getFaces = function(polyI, triI, filter) {
      var faces = [];
      var faceGroups = this._faceGroups;
      var i, grp, face;
      //
      for ( i in faceGroups ) {
        // noinspection JSUnfilteredForInLoop
        grp = faceGroups[i];
        face = grp.faces[grp.isPoly ? polyI : triI];
        //
        if ( !face ) {
          continue;
        }
        //
        if ( !filter || filter(grp, face) ) {
          faces.push(face);
        }
      }
      //
      return faces;
      }; //-IDB.Area3dSegmentView.prototype._getFaces
  // Z_DOT ppt setLabelRef(labelRef):void                          ::IDB.module.Area3d.js  z.77627253005683301.2019.04.09.09.47.52.677|+.
    /**
     * Label Ref
     *
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Object} labelRef
     *
     * @see z.77627253005683301
     */
    IDB.Area3dSegmentView.prototype.setLabelRef = function(labelRef) {
      this._labelRef = labelRef;
      }; //-IDB.Area3dSegmentView.prototype.setLabelRef
  // Z_DOT ppt setPositionAndSize(px, pz, maxY):void               ::IDB.module.Area3d.js  z.27777253005683301.2019.04.09.09.47.57.772|+.
    /**
     * Position And Size
     *
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Number} px
     * @param {Number} pz
     * @param {Number} maxY
     *
     * @see z.27777253005683301
     */
    IDB.Area3dSegmentView.prototype.setPositionAndSize = function(px, pz, maxY) {
      // noinspection MagicNumberJS
      this._px = Number(px)+0.5;
      // noinspection MagicNumberJS
      this._pz = Number(pz)+0.5;
      this._maxY = maxY;
      }; //-IDB.Area3dSegmentView.prototype.setPositionAndSize
  // Z_DOT cmd render():void                                       ::IDB.module.Area3d.js  z.08128253005683301.2019.04.09.09.48.02.180|+!
    /**
     * Render
     * @memberof IDB.Area3dSegmentView
     * @method
     *
     * @see z.08128253005683301
     */
    IDB.Area3dSegmentView.prototype.render = function() {
      var model = this._model;
      //
      if ( !model.getActive() ) {
        return;
      }
      //
      var d3 = this._draw3d;
      var faceGroups = this._faceGroups;
      var px = this._px;
      var pz = this._pz;
      var maxY = this._maxY;
      var thick = model.getAxisThickness();
      var i, grp, frontPts, backPts, pi, rp, v;
      //
      for ( i in faceGroups ) {
        // noinspection JSUnfilteredForInLoop
        grp = faceGroups[i];
        frontPts = [];
        backPts = [];
        //
        for ( pi in grp.points ) {
          // noinspection JSUnfilteredForInLoop
          rp = grp.points[pi];
          //
          if ( !rp ) {
            continue;
          }
          //
          v = new IDB.Vec3(px+rp.x, (1-rp.y)*maxY, pz-thick/2);
          // noinspection JSUnfilteredForInLoop
          frontPts[pi] = d3.project(v);
          //
          v.z += thick;
          // noinspection JSUnfilteredForInLoop
          backPts[pi] = d3.project(v);
        }
        //
        if ( grp.isPoly ) {
          this._renderPolyFaces(grp, frontPts, backPts);
        }
        else {
          this._renderTriFaces(grp, frontPts, backPts);
        }
      }
      }; //-IDB.Area3dSegmentView.prototype.render
  // Z_DOT cmd _renderPolyFaces(grp, frontPts, backPts):void       ::IDB.module.Area3d.js  z.40878253005683301.2019.04.09.09.48.07.804|-!
    /**
     * Render Poly Faces
     *
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Object} grp
     * @param {Array<Point>} frontPts
     * @param {Array<Point>} backPts
     *
     * @see z.40878253005683301
     */
    IDB.Area3dSegmentView.prototype._renderPolyFaces = function(grp, frontPts, backPts) {
      var pa = 0;
      var pb = 1;
      var pc = 2;
      var pbc = 3; //baseC
      var pbb = 4; //baseB
      var pba = 5; //baseA
      //
      var getPoints = function(i, j) {
        return [
          frontPts[i],
          frontPts[j],
          backPts[j],
          backPts[i]
        ];
      };
      //
      var hasBaseA = (frontPts[pba] != null);
      var hasBaseC = (frontPts[pbc] != null);
      var faceLeft = grp.faces[1];
      var faceRight = grp.faces[6];
      //
      var leftPts  = (hasBaseA ? getPoints(pa, pba) : []);
      var rightPts = (hasBaseC ? getPoints(pc, pbc) : []);
      //
      var baseAbPts = getPoints((hasBaseA ? pba : pa), pbb);
      var baseBcPts = getPoints(pbb, (hasBaseC ? pbc : pc));
      //
      var abPts = getPoints(pa, pb);
      var bcPts = getPoints(pb, pc);
      //
      var lineBaseBack = [backPts[hasBaseA ? pba : pa], backPts[pbb], backPts[hasBaseC ? pbc : pc]];
      var lineBaseFront = [frontPts[hasBaseA ? pba : pa], frontPts[pbb], frontPts[hasBaseC ? pbc : pc]];
      var lineBaseMid = [frontPts[pbb],  backPts[pbb]];
      //
      var lineBack = [backPts[pa], backPts[pb], backPts[pc]];
      var lineFront = [frontPts[pa], frontPts[pb], frontPts[pc]];
      var lineMid = [frontPts[pb],  backPts[pb]];
      //
      if ( faceLeft && faceLeft.defaultVisible ) {
        lineBaseBack.unshift(backPts[pa]);
        lineBaseFront.unshift(frontPts[pa]);
      }
      //
      if ( faceRight && faceRight.defaultVisible ) {
        lineBack.push(backPts[pbc]);
        lineFront.push(frontPts[pbc]);
      }
      //
      IDB.Draw3D.setFacePoints(grp.faces[0], backPts);
      IDB.Draw3D.setFacePoints(faceLeft,     leftPts);
      IDB.Draw3D.setFacePoints(grp.faces[2], baseAbPts);
      IDB.Draw3D.setFacePoints(grp.faces[3], baseBcPts);
      IDB.Draw3D.setFacePoints(grp.faces[4], abPts);
      IDB.Draw3D.setFacePoints(grp.faces[5], bcPts);
      IDB.Draw3D.setFacePoints(faceRight,    rightPts);
      IDB.Draw3D.setFacePoints(grp.faces[7], frontPts);
      //
      IDB.Draw3D.setFacePoints(grp.lines[0], lineBaseBack);
      IDB.Draw3D.setFacePoints(grp.lines[1], lineBaseMid);
      IDB.Draw3D.setFacePoints(grp.lines[2], lineBaseFront);
      IDB.Draw3D.setFacePoints(grp.lines[3], lineBack);
      IDB.Draw3D.setFacePoints(grp.lines[4], lineMid);
      IDB.Draw3D.setFacePoints(grp.lines[5], lineFront);
      }; //-IDB.Area3dSegmentView.prototype._renderPolyFaces
  // Z_DOT cmd _renderTriFaces(grp, frontPts, backPts):void        ::IDB.module.Area3d.js  z.04379253005683301.2019.04.09.09.48.17.340|-!
    /**
     * Render Tri Faces
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @param {Object} grp
     * @param {Array<Point>} frontPts
     * @param {Array<Point>} backPts
     *
     * @see z.04379253005683301
     */
    IDB.Area3dSegmentView.prototype._renderTriFaces = function(grp, frontPts, backPts) {
      var faceAb = grp.faces[1];
      var faceBc = grp.faces[2];
      var faceCa = grp.faces[3];
      var lineBackPts, lineFrontPts, lineMidPts;
      //
      if ( faceAb.defaultVisible ) {
        lineBackPts = [backPts[2], backPts[0], backPts[1]];
        lineFrontPts = [frontPts[2], frontPts[0], frontPts[1]];
        lineMidPts = [frontPts[0], backPts[0]];
      }
      else {
        lineBackPts = [backPts[1], backPts[2], backPts[0]];
        lineFrontPts = [frontPts[1], frontPts[2], frontPts[0]];
        lineMidPts = [frontPts[2], backPts[2]];
      }
      //
      IDB.Draw3D.setFacePoints(grp.faces[0], backPts);
      IDB.Draw3D.setFacePoints(faceAb, [frontPts[0], frontPts[1], backPts[1], backPts[0]]);
      IDB.Draw3D.setFacePoints(faceBc, [frontPts[1], frontPts[2], backPts[2], backPts[1]]);
      IDB.Draw3D.setFacePoints(faceCa, [frontPts[2], frontPts[0], backPts[0], backPts[2]]);
      IDB.Draw3D.setFacePoints(grp.faces[4], frontPts);
      //
      IDB.Draw3D.setFacePoints(grp.lines[0], lineBackPts);
      IDB.Draw3D.setFacePoints(grp.lines[1], lineFrontPts);
      IDB.Draw3D.setFacePoints(grp.lines[2], lineMidPts);
      }; //-IDB.Area3dSegmentView.prototype._renderTriFaces
  // Z_DOT cmd _onDatapointsUpdated():void                         ::IDB.module.Area3d.js  z.84730353005683301.2019.04.09.09.48.23.748|-\
    /**
     * On Datapoints Updated
     * @memberof IDB.Area3dSegmentView
     * @method
     * @private
     *
     * @see z.84730353005683301
     */
    IDB.Area3dSegmentView.prototype._onDatapointsUpdated = function() {
      var state = this._model.getDatapoint().getVisualState();
      var isHigh = (state === IDB.VS.HIGHLIGHTED);
      var isFade = (state === IDB.VS.FADED);
      // noinspection MagicNumberJS
      var alpha = (isFade ? 0.2 : 1.0);
      var faceSep = this._facesSeparated;
      //
      if ( !faceSep ) {
        this._display.opacity(alpha);
      }
      //
      if  ( this._labelRef ) {
        this._labelRef.visible = !isFade;
      }
      //
      var faceGroups = this._faceGroups;
      var i, grp, pi, face;
      //
      for ( i in faceGroups ) {
        // noinspection JSUnfilteredForInLoop,JSUnfilteredForInLoop
        grp = faceGroups[i];
        //
        for ( pi in grp.faces ) {
          face = grp.faces[pi];
          //
          if ( !face ) {
            continue;
          }
          //
          if ( faceSep ) {
            face.opacity(alpha);
          }
          //
          face.visible(isHigh || face.defaultVisible);
        }
      }
      }; //-IDB.Area3dSegmentView.prototype._onDatapointsUpdated
//EOF

