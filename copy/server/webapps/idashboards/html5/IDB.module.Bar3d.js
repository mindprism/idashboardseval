// Z_DOT file::IDB.module.Bar3d.js z.91135337005683301.2019.04.09.20.22.33.119|file
/* global IDB, Kinetic, jQueryElement */
/**
 * Kinetic Bar3d module.
 * @module Bar3d
 * @see IDB.Bar3dBoxView
 * @see IDB.Bar3dGraph
 * @see IDB.Bar3dPlotView
 */
'use strict';
// Z_DOT ctor Bar3dBoxView(draw3d, model):Bar3dBoxView                      ::IDB.module.Bar3d.js  z.42562947005683301.2019.04.09.20.48.46.524|class
  /**
   * Bar3dBoxView constructor
   * @class Bar3dBoxView
   * @constructor
   * @memberof IDB
   *
   * @param {Object} draw3d
   * @param {Object} model
   *
   * @see z.42562947005683301
   * @see module:Bar3d
   */
  IDB.Bar3dBoxView = function(draw3d, model) {
    this._draw3d = draw3d;
    this._model = model;
    model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
    this._buildKinetic();
    }; //-IDB.Bar3dBoxView
  // Z_DOT cmd _buildKinetic():void                   ::IDB.module.Bar3d.js  z.15607947005683301.2019.04.09.20.49.30.651|-!
    /**
     * Build Kinetic
     * @memberof IDB.Bar3dBoxView
     * @method
     *
     * @see z.15607947005683301
     */
    IDB.Bar3dBoxView.prototype._buildKinetic = function() {
      var model = this._model;
      var dp = model.getDatapoint();
      //
      this._display = new Kinetic.Group(undefined);
      //
      this._gteZero = (dp.yValue.numberValue >= 0);
      this._color = (model.fillWithRangeColor() ? dp.rangeColor : dp.axisColor);
      this._alpha = model.getFillAlpha();
      this._faces = {};
      this._highs = {};
      //
      this._facesHold = new Kinetic.Group(undefined);
      this._facesHold.on('click', model.createClickListener());
      this._facesHold.on('mouseover', model.createOverListener());
      this._facesHold.on('mouseout', model.createOutListener());
      this._touchHandler = new IDB.TouchHandler(this._facesHold);
      this._display.add(this._facesHold);
      //
      this._highsHold = new Kinetic.Group(undefined);
      this._display.add(this._highsHold);
      //
      this._buildFaces();
      //
      if ( model.getLabelText() ) {
        var lblOpt = model.getBarLabelOptions();
        lblOpt.align = 'center';
        //
        this._label = new IDB.GraphLabel(lblOpt, model.getLabelText());
        this._label.rotateFromCenter(0);
        this._label.getDisplay().listening(false);
      }
      }; //-IDB.Bar3dBoxView.prototype._buildKinetic
  // Z_DOT cmd _buildFaces():void                     ::IDB.module.Bar3d.js  z.68411057005683301.2019.04.09.20.50.11.486|-!
    /**
     * Build Faces
     * @memberof IDB.Bar3dBoxView
     * @method
     *
     * @see z.68411057005683301
     */
    IDB.Bar3dBoxView.prototype._buildFaces = function() {
      var faces = this._faces;
      var highs = this._highs;
      var facesHold = this._facesHold;
      var highsHold = this._highsHold;
      var col = this._color;
      var colorUint = col.i;
      var alpha = this._alpha;
      var isCylinder = this._model.isCylinderMode();
      // noinspection MagicNumberJS
      var stroke = IDB.toRGBA(col.x.css, 0.12);
      // noinspection MagicNumberJS
      var highStroke = IDB.toRGBA(col.x.css, 0.3);
      // noinspection MagicNumberJS
      var fillDarkest = IDB.toRGBA(IDB.Draw3D.shadeColor(colorUint, -0.6), alpha);
      // noinspection MagicNumberJS
      var fillDark = IDB.toRGBA(IDB.Draw3D.shadeColor(colorUint, -0.25), alpha);
      var fillNormal = IDB.toRGBA(col, alpha);
      // noinspection MagicNumberJS
      var fillLight = IDB.toRGBA(IDB.Draw3D.shadeColor(colorUint, 0.25), alpha);
      var face;
      //
      if ( alpha < 1 ) {
        if ( isCylinder ) {
          // noinspection JSCheckFunctionSignatures,MagicNumberJS
          face = new Kinetic.Line({
            closed: true,
            stroke: IDB.toRGBA(col.x.css, 0.2),
            strokeWidth: 1
          });
          facesHold.add(face);
          faces['D'] = face;
        }
        else {
          // noinspection JSCheckFunctionSignatures
          face = new Kinetic.Line({
            closed: true,
            fill: fillDark,
            stroke: stroke,
            strokeWidth: 1
          });
          facesHold.add(face);
          faces['B'] = face; //back
          //
          // noinspection JSCheckFunctionSignatures
          face = new Kinetic.Line({
            closed: true,
            fill: fillNormal,
            stroke: stroke,
            strokeWidth: 1
          });
          facesHold.add(face);
          faces['L'] = face; //left
        }
      }
      //
      // noinspection JSCheckFunctionSignatures
      face = new Kinetic.Line({
        closed: true,
        fill: fillNormal,
        stroke: stroke,
        strokeWidth: 1
      });
      facesHold.add(face);
      faces['F'] = face; //front
      //
      if ( isCylinder ) {
        // noinspection MagicNumberJS
        face
          .fill(null)
          .fillLinearGradientColorStops([
            0.0, fillNormal,
            1.0, IDB.toRGBA(IDB.Draw3D.shadeColor(colorUint, -0.4), alpha)
          ]);
      }
      //
      if ( !isCylinder ) {
        // noinspection JSCheckFunctionSignatures
        face = new Kinetic.Line({
          closed: true,
          fill: fillDark,
          stroke: stroke,
          strokeWidth: 1
        });
        facesHold.add(face);
        faces['R'] = face; //right
      }
      //
      // noinspection JSCheckFunctionSignatures
      face = new Kinetic.Line({
        closed: true,
        fill: (this._gteZero ? fillLight : fillDarkest),
        stroke: stroke,
        strokeWidth: 1
      });
      facesHold.add(face);
      faces['C'] = face; //cap
      //
      ////
      //
      // noinspection JSCheckFunctionSignatures
      face = new Kinetic.Line({
        stroke: highStroke,
        strokeWidth: 1
      });
      highsHold.add(face);
      highs['F'] = face; //front
      //
      if ( !isCylinder ) {
        // noinspection JSCheckFunctionSignatures
        face = new Kinetic.Line({
          stroke: highStroke,
          strokeWidth: 1
        });
        highsHold.add(face);
        highs['R'] = face; //right
      }
      //
      // noinspection JSCheckFunctionSignatures
      face = new Kinetic.Line({
        stroke: highStroke,
        strokeWidth: 1
      });
      highsHold.add(face);
      highs['C'] = face; //cap
      }; //-IDB.Bar3dBoxView.prototype._buildFaces
  // Z_DOT ppt getDisplay():Kinetic.Group             ::IDB.module.Bar3d.js  z.23723057005683301.2019.04.09.20.50.32.732|+@.
    /**
     * Display
     * @memberof IDB.Bar3dBoxView
     * @method
     * @return {Kinetic.Group.Group}
     *
     * @see z.23723057005683301
     */
    IDB.Bar3dBoxView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.Bar3dBoxView.prototype.getDisplay
  // Z_DOT ppt getLabelDisplay():Object               ::IDB.module.Bar3d.js  z.88145057005683301.2019.04.09.20.50.54.188|+@.
    /**
     * Label Display
     * @memberof IDB.Bar3dBoxView
     * @method
     * @return {Object}
     *
     * @see z.88145057005683301
     */
    IDB.Bar3dBoxView.prototype.getLabelDisplay = function() {
      return (this._label ? this._label.getDisplay() : null);
      }; //-IDB.Bar3dBoxView.prototype.getLabelDisplay
  // Z_DOT ppt setPositionAndSize(px, pz, maxY):void  ::IDB.module.Bar3d.js  z.46327057005683301.2019.04.09.20.51.12.364|+@.
    /**
     * Position and Size
     * @memberof IDB.Bar3dBoxView
     * @method
     *
     *
     * @param {Number} px
     * @param {Number} pz
     * @param {Number} maxY
     *
     * @see z.46327057005683301
     */
    IDB.Bar3dBoxView.prototype.setPositionAndSize = function(px, pz, maxY) {
      this._px = Number(px);
      this._pz = Number(pz);
      this._maxY = maxY;
      }; //-IDB.Bar3dBoxView.prototype.setPositionAndSize
  // Z_DOT cmd render():void                          ::IDB.module.Bar3d.js  z.88500157005683301.2019.04.09.20.51.40.588|+@.
    /**
     * Render
     * @memberof IDB.Bar3dBoxView
     * @method
     *
     *
     * @see z.88500157005683301
     */
    IDB.Bar3dBoxView.prototype.render = function() {
      var model = this._model;
      //
      if ( model.isCylinderMode() ) {
        this._renderCylinder();
      }
      else {
        this._renderBox();
      }
      }; //-IDB.Bar3dBoxView.prototype.render
  // Z_DOT cmd _renderBox():void                      ::IDB.module.Bar3d.js  z.15641157005683301.2019.04.09.20.51.54.651|-!
    /**
     * Render Box
     * @memberof IDB.Bar3dBoxView
     * @method
     *
     *
     * @see z.15641157005683301
     */
    IDB.Bar3dBoxView.prototype._renderBox = function() {
      var d3 = this._draw3d;
      var model = this._model;
      var maxY = this._maxY;
      var faces = this._faces;
      var highs = this._highs;
      var gteZero = this._gteZero;
      var lbl = this._label;
      var relY = model.getRelativeY();
      var relH = model.getRelativeHeight();
      var thick = model.getThicknessRatio();
      var half = thick/2;
      //
      // noinspection MagicNumberJS
      var px = this._px+0.5;
      var py = relY*maxY;
      // noinspection MagicNumberJS
      var pz = this._pz+0.5;
      var h = relH*maxY;
      //
      var v0 = new IDB.Vec3(px-half, py, pz-half);
      var v1 = new IDB.Vec3(px+half, py, pz-half);
      var v2 = new IDB.Vec3(px+half, py, pz+half);
      var v3 = new IDB.Vec3(px-half, py, pz+half);
      //
      var b0 = d3.project(v0);
      var b1 = d3.project(v1);
      var b2 = d3.project(v2);
      var b3 = d3.project(v3);
      //
      var t0 = d3.project(new IDB.Vec3(v0.x, v0.y+h, v0.z));
      var t1 = d3.project(new IDB.Vec3(v1.x, v1.y+h, v1.z));
      var t2 = d3.project(new IDB.Vec3(v2.x, v2.y+h, v2.z));
      var t3 = d3.project(new IDB.Vec3(v3.x, v3.y+h, v3.z));
      //
      IDB.Draw3D.setFacePoints(faces['B'], [t2, t3, b3, b2]);
      IDB.Draw3D.setFacePoints(faces['L'], [t3, t0, b0, b3]);
      IDB.Draw3D.setFacePoints(faces['F'], [t0, t1, b1, b0]);
      IDB.Draw3D.setFacePoints(faces['R'], [t1, t2, b2, b1]);
      IDB.Draw3D.setFacePoints(faces['C'], (gteZero ? [t0, t1, t2, t3] : [b0, b1, b2, b3]));
      //
      IDB.Draw3D.setFacePoints(highs['F'], [t0, t1, b1, b0, t0]);
      IDB.Draw3D.setFacePoints(highs['R'], [t1, t2, b2, b1]);
      IDB.Draw3D.setFacePoints(highs['C'], (gteZero ? [t2, t3, t0] : [b2, b3, b0]));
      //
      if ( lbl ) {
        var r = model.getCameraHorizRotation();
        var push = (gteZero ? 1 : -1)*lbl.height;
        var halfH = lbl.height/2;

        // noinspection MagicNumberJS
        if ( r < 35 ) {
          lbl.x = t0.x+(t1.x-t0.x)/2;
          lbl.y = t0.y+(t1.y-t0.y)/2+push;
        }
        else {
          // noinspection MagicNumberJS
          if ( r > 55 ) {
            lbl.x = t1.x+(t2.x-t1.x)/2;
            lbl.y = t1.y+(t2.y-t1.y)/2+push;
          }
          else {
            lbl.x = t1.x;
            lbl.y = t1.y+push;
          }
        }
        //
        if ( gteZero && lbl.y+halfH > b1.y ) {
          lbl.y = b1.y-halfH;
        }
        else if ( !gteZero && lbl.y-halfH < b1.y ) {
          lbl.y = b1.y+halfH;
        }
      }
      }; //-IDB.Bar3dBoxView.prototype._renderBox
  // Z_DOT cmd _renderCylinder():void                 ::IDB.module.Bar3d.js  z.53413157005683301.2019.04.09.20.52.11.435|-!
    /**
     * Render Cylinder
     * @memberof IDB.Bar3dBoxView
     * @method
     *
     *
     * @see z.53413157005683301
     */
    IDB.Bar3dBoxView.prototype._renderCylinder = function() {
      var d3 = this._draw3d;
      var model = this._model;
      var maxY = this._maxY;
      var faces = this._faces;
      var highs = this._highs;
      var gteZero = this._gteZero;
      var lbl = this._label;
      var relY = model.getRelativeY();
      var relH = model.getRelativeHeight();
      var thick = model.getThicknessRatio();
      var half = thick/2;
      //
      // noinspection MagicNumberJS
      var px = this._px+0.5;
      var py = relY*maxY;
      // noinspection MagicNumberJS
      var pz = this._pz+0.5;
      var h = relH*maxY;
      //
      var topPts = [];
      var botPts = [];
      var frontPts = [];
      var highPts = [];
      // noinspection MagicNumberJS
      var steps = 24; //AS3 uses variable number of steps; 24 is sufficient for most cases
      var inc = (Math.PI*2)/steps;
      var botL = { x:  99999 };
      // noinspection MagicNumberJS
      var botR = { x: -99999 };
      var topL = null;
      var topR = null;
      var i, a, v, bp, tp, p;
      //
      for ( i = 0 ; i <= steps ; ++i ) {
        a = i*inc;
        //
        v = new IDB.Vec3(
          px+Math.sin(a)*half,
          py,
          pz+Math.cos(a)*half
        );
        //
        bp = d3.project(v);
        botPts.push(bp);
        //
        v.y += h;
        tp = d3.project(v);
        topPts.push(tp);

        if ( bp.x < botL.x ) {
          botL = bp;
          topL = tp;
        }
        else if ( bp.x > botR.x ) {
          botR = bp;
          topR = tp;
        }
      }
      //
      ////
      //
      frontPts.push(topR);
      //
      for ( i = 0 ; i < topPts.length ; ++i ) {
        p = topPts[i];
        //
        if ( p.y > topL.y || p.y > topR.y ) {
          frontPts.push(p);
        }
      }
      //
      frontPts.push(topL);
      highPts.push(topL);
      //
      for ( i = botPts.length-1 ; i >= 0 ; --i ) {
        p = botPts[i];
        //
        if ( p.y > botL.y || p.y > botR.y ) {
          frontPts.push(p);
          highPts.push(p);
        }
      }
      //
      frontPts.push(botR, topR);
      highPts.push(botR, topR);
      //
      ////
      //
      faces['F']
        .fillLinearGradientStartPointX(botL.x)
        .fillLinearGradientEndPointX(botR.x);
      //
      IDB.Draw3D.setFacePoints(faces['D'], (gteZero ? botPts : topPts));
      IDB.Draw3D.setFacePoints(faces['F'], frontPts);
      IDB.Draw3D.setFacePoints(faces['C'], (gteZero ? topPts : botPts));
      //
      IDB.Draw3D.setFacePoints(highs['F'], highPts);
      IDB.Draw3D.setFacePoints(highs['C'], (gteZero ? topPts : botPts));
      //
      if ( lbl ) {
        // noinspection MagicNumberJS
        a = model.getCameraHorizRotation()/180*Math.PI;
        //
        v = new IDB.Vec3(
          px+Math.sin(a)*half,
          py+h,
          pz-Math.cos(a)*half
        );
        //
        tp = d3.project(v);
        //
        lbl.x = tp.x;
        lbl.y = tp.y + lbl.height*(gteZero ? 1 : -1);
      }
      }; //-IDB.Bar3dBoxView.prototype._renderCylinder
  // Z_DOT evt _onDatapointsUpdated():void            ::IDB.module.Bar3d.js  z.74184157005683301.2019.04.09.20.52.28.147|-\
    /**
     * On Datapoints Updated
     * @memberof IDB.Bar3dBoxView
     * @method
     *
     *
     * @see z.74184157005683301
     */
    IDB.Bar3dBoxView.prototype._onDatapointsUpdated = function() {
      var state = this._model.getDatapoint().getVisualState();
      // noinspection EqualityComparisonWithCoercionJS
      var isFade = (state == IDB.VS.FADED);
      //
      // noinspection MagicNumberJS
      this._facesHold.opacity(isFade ? 0.15 : 1.0);
      // noinspection EqualityComparisonWithCoercionJS
      this._highsHold.visible(state == IDB.VS.HIGHLIGHTED);
      //
      if ( this._label ) {
        this._label.visible = !isFade;
      }
      }; //-IDB.Bar3dBoxView.prototype._onDatapointsUpdated
// Z_DOT ctor Bar3dGraph(chart, $div, graphProperties, dataSet):Bar3dGraph  ::IDB.module.Bar3d.js  z.13736157005683301.2019.04.09.20.52.43.731|class
  /**
   * Bar3dGraph constructor
   * @class Bar3dGraph
   * @constructor
   * @memberof IDB
   * @augments IDB.GraphBaseKinetic
   *
   * @param {Object} chart
   * @param {jQueryElement} $div
   * @param {Object} graphProperties
   * @param {Object} dataSet
   *
   * @see z.13736157005683301
   * @see module:Bar3d
   */
  IDB.Bar3dGraph = function(chart, $div, graphProperties, dataSet) {
    IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
    this._info.legendStyle = (this._graphContext.settings.getBool('BarFillRangeColors') ?
      IDB.LS.RANGES : IDB.LS.AXES);
    }; //-IDB.Bar3dGraph
  IDB.Bar3dGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
  IDB.Bar3dGraph.prototype.constructor = IDB.Bar3dGraph;
  // Z_DOT cmd _build():void                ::IDB.module.Bar3d.js  z.46505257005683301.2019.04.09.20.54.10.564|-!
    /**
     * Build
     * @memberof IDB.Bar3dGraph
     * @method
     *
     *
     * @see z.46505257005683301
     */
    IDB.Bar3dGraph.prototype._build = function() {
      var gc = this._graphContext;
      var grid = new IDB.CartesianGrid(gc);
      // noinspection JSCheckFunctionSignatures
      var model = new IDB.BarPlot(gc, grid.getAxes());
      //
      this._gridView = new IDB.Cartesian3dGridView(grid);
      this._stageLayer.add(this._gridView.getDisplay());
      //
      var plotHold = this._gridView.getPlotDisplay();
      var draw3d = this._gridView.getDraw3d();
      //
      this._plotView = new IDB.Bar3dPlotView(draw3d, model);
      plotHold.add(this._plotView.getDisplay());
      }; //-IDB.Bar3dGraph.prototype._build
  // Z_DOT cmd _update(width, height):void  ::IDB.module.Bar3d.js  z.38607257005683301.2019.04.09.20.54.30.683|-!
    /**
     * Update
     * @memberof IDB.Bar3dGraph
     * @method
     *
     * @param width
     * @param height
     *
     * @see z.38607257005683301
     */
    IDB.Bar3dGraph.prototype._update = function(width, height) {
      var gv = this._gridView;
      var pv = this._plotView;
      //
      gv.updateScreen(width, height);
      gv.updateCamera();
      gv.render();
      //
      pv.setMaxDimensions(gv.getMaxDimensions());
      pv.render();
      }; //-IDB.Bar3dGraph.prototype._update
// Z_DOT ctor Bar3dPlotView(draw3d, model):Bar3dPlotView                    ::IDB.module.Bar3d.js  z.98119257005683301.2019.04.09.20.54.51.189|class
  /**
   * Bar3dPlotView constructor
   * @class Bar3dPlotView
   * @constructor
   * @memberof IDB
   *
   * @param draw3d
   * @param model
   *
   * @see z.98119257005683301
   * @see module:Bar3d
   */
  IDB.Bar3dPlotView = function(draw3d, model) {
    this._draw3d = draw3d;
    this._model = model;
    this._buildKinetic();
    this._boxGrid = this._buildBoxGrid();
    }; //-IDB.Bar3dPlotView
  // Z_DOT cmd _buildKinetic():void                 ::IDB.module.Bar3d.js  z.00531357005683301.2019.04.09.20.55.13.500|-!
    /**
     * Build Kinetic
     * @memberof IDB.Bar3dPlotView
     * @method
     *
     *
     * @see z.00531357005683301
     */
    IDB.Bar3dPlotView.prototype._buildKinetic = function() {
      this._display = new Kinetic.Group(undefined);
      //
      this._barHold = new Kinetic.Group(undefined);
      this._labelHold = new Kinetic.Group(undefined);
      //
      this._display.add(this._barHold);
      this._display.add(this._labelHold);
      }; //-IDB.Bar3dPlotView.prototype._buildKinetic
  // Z_DOT fn _buildBoxGrid():[[IDB.Bar3dBoxView]]  ::IDB.module.Bar3d.js  z.23992357005683301.2019.04.09.20.55.29.932|-?
    /**
     * Build Box Grid
     * @memberof IDB.Bar3dPlotView
     * @method
     * @return {Array<Array<IDB.Bar3dBoxView>>}
     *
     * @see z.23992357005683301
     */
    IDB.Bar3dPlotView.prototype._buildBoxGrid = function() {
      var boxGrid = [];
      var plotModel = this._model;
      var draw3d = this._draw3d;
      var barHold = this._barHold;
      var lblHold = this._labelHold;
      var groupCount = plotModel.getGroupCount();
      var isClusterMode = plotModel.isClusterMode();
      var zIndexes = [];
      var i, j, boxGridRow, groupModel, boxCount, boxModel, box, boxLbl;
      //
      for ( i = 0 ; i < groupCount ; ++i ) {
        boxGridRow = [];
        boxGrid[i] = boxGridRow;
        groupModel = plotModel.getGroup(i);
        boxCount = groupModel.getBoxCount();
        //
        for ( j = 0 ; j < boxCount ; ++j ) {
          boxModel = groupModel.getBox(j);
          box = new IDB.Bar3dBoxView(draw3d, boxModel);
          boxGridRow.push(box);
          //
          zIndexes.push({
            z: (isClusterMode ? (boxCount-j-1)*groupCount+i : i*boxCount+boxModel.getRelativeY()),
            boxDisp: box.getDisplay()
          });
          //
          boxLbl = box.getLabelDisplay();
          //
          if ( boxLbl ) {
            (isClusterMode ? box.getDisplay() : lblHold).add(boxLbl);
          }
        }
      }
      //
      zIndexes.sort(function(a, b) {
        return a.z-b.z;
      });
      //
      for ( i in zIndexes ) {
        barHold.add(zIndexes[i].boxDisp);
      }
      //
      return boxGrid;
      }; //-IDB.Bar3dPlotView.prototype._buildBoxGrid
  // Z_DOT ppt getDisplay():Kinetic.Group           ::IDB.module.Bar3d.js  z.00774357005683301.2019.04.09.20.55.47.700|+@.
    /**
     * Display
     * @memberof IDB.Bar3dPlotView
     * @method
     * @return {Kinetic.Group.Group}
     *
     * @see z.00774357005683301
     */
    IDB.Bar3dPlotView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.Bar3dPlotView.prototype.getDisplay
  // Z_DOT ppt setMaxDimensions(maxDims):void       ::IDB.module.Bar3d.js  z.25636357005683301.2019.04.09.20.56.03.652|+@.
    /**
     * Max Dimensions
     * @memberof IDB.Bar3dPlotView
     * @method
     *
     *
     * @param maxDims
     *
     * @see z.25636357005683301
     */
    IDB.Bar3dPlotView.prototype.setMaxDimensions = function(maxDims) {
      var plotModel = this._model;
      var boxGrid = this._boxGrid;
      var maxDimY = maxDims.y;
      var clusterMode = plotModel.isClusterMode();
      var i, j, boxGridRow, box;
      //
      for ( i in boxGrid ) {
        // noinspection JSUnfilteredForInLoop
        boxGridRow = boxGrid[i];
        //
        for ( j in boxGridRow ) {
          // noinspection JSUnfilteredForInLoop
          box = boxGridRow[j];
          // noinspection JSUnfilteredForInLoop
          box.setPositionAndSize(i, (clusterMode ? j : 0), maxDimY);
        }
      }
      }; //-IDB.Bar3dPlotView.prototype.setMaxDimensions
  // Z_DOT cmd render():void                        ::IDB.module.Bar3d.js  z.52587357005683301.2019.04.09.20.56.18.525|+!
    /**
     * Render
     * @memberof IDB.Bar3dPlotView
     * @method
     *
     *
     * @see z.52587357005683301
     */
    IDB.Bar3dPlotView.prototype.render = function() {
      var boxGrid = this._boxGrid;
      var i, j, boxGridRow, box;
      //
      for ( i in boxGrid ) {
        // noinspection JSUnfilteredForInLoop
        boxGridRow = boxGrid[i];
        //
        for ( j in boxGridRow ) {
          // noinspection JSUnfilteredForInLoop
          box = boxGridRow[j];
          box.render();
        }
      }
      }; //-IDB.Bar3dPlotView.prototype.render
//EOF
