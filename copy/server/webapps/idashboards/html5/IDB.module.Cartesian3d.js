'use strict';
IDB.Cartesian3dGridAxesView = function(model) {
	this._model = model;
	this._model.setOnActiveYAxisChange(IDB.listener(this, "_onActiveYAxisChange"));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype._buildKinetic = function() {
	var model = this._model;
	var yGridAxisCount = model.getYGridAxisCount();
	var xGridAxis = model.getXGridAxis();
	var hr = model.getCameraHorizRotation();
	var vr = model.getCameraVertRotation();
	var gridCol = model.getGridColor();
	var i, yAxisView;

	this._display = new Kinetic.Group();
	this._draw3d = new IDB.Draw3D();
	
	this._yAxes = [];
	this._maxDims = new IDB.Vec3(0, 0, 0);
	this._center = new IDB.Vec3(0, 0, 0);
	this._currYAxisId = IDB.CartesianGrid.ShowNoYAxis;
	this._startXIndex = 0;
	this._xLabelMaxW = 0;
	this._xLabelNumHigh = 0;
	this._rotateXLabels = (hr >= 70 || (hr >= 40 && vr >= 50));

	////
	
	this._gridBack = new Kinetic.Group();
	this._display.add(this._gridBack);
	
	this._gridBackFace = new Kinetic.Line({
		closed: true,
		fill: IDB.toRGBA(gridCol, 0.1),
		stroke: IDB.toRGBA(gridCol, 0.6),
		strokeWidth: 1,
		points: []
	});
	this._gridBack.add(this._gridBackFace);

	this._gridLeftFace = new Kinetic.Line({
		closed: true,
		fill: IDB.toRGBA(gridCol, 0.1),
		stroke: IDB.toRGBA(gridCol, 0.6),
		strokeWidth: 1,
		points: []
	});
	this._gridBack.add(this._gridLeftFace);
	
	this._gridBottomFace = new Kinetic.Line({
		closed: true,
		fill: IDB.toRGBA(gridCol, 0.333),
		stroke: IDB.toRGBA(gridCol, 0.6),
		strokeWidth: 1,
		points: []
	});
	this._gridBack.add(this._gridBottomFace);

	////

	this._plotHold = new Kinetic.Group();
	this._display.add(this._plotHold);
	
	this._gridFore = new Kinetic.Group();
	this._display.add(this._gridFore);

	////

	for ( i = 0 ; i < yGridAxisCount ; ++i ) {
		yAxisView = new IDB.Cartesian3dGridAxisYView(model.getYGridAxis(i), this._draw3d);
		this._gridBack.add(yAxisView.getDisplayBack());
		this._gridFore.add(yAxisView.getDisplayFore());
		this._yAxes[i] = yAxisView;
	}
	
	if ( xGridAxis ) {
		this._xGridView = new IDB.Cartesian3dGridAxisXView(
			xGridAxis, this._draw3d, this._rotateXLabels, gridCol);
		this._gridBack.add(this._xGridView.getDisplay());
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype.getPlotDisplay = function() {
	return this._plotHold;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype.getDraw3d = function() {
	return this._draw3d;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype.getMaxDimensions = function() {
	return this._maxDims;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype.updateScreen = function(width, height) {
	this._width = width;
	this._height = height;

	var model = this._model;
	var draw = this._draw3d;
	var numRowsDisp = model.getNumRowsDisplayed();
	var zMax = (model.isStackedOrRatioMode() ? 1 : model.getNumYNumericCols());

	draw.setScreen(this._width, this._height, 0.1, 1000, 0.001);

	this._maxDims = new IDB.Vec3(numRowsDisp, numRowsDisp/draw.getWidthRatio(), zMax);
	this._center = IDB.Vec3.vecMult(this._maxDims, 0.5);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype.updateCamera = function() {
	var model = this._model;
	var height = this._height;
	var maxDims = this._maxDims;
	var mag = 90000;
	var lenInit = Math.max(maxDims.x, maxDims.y, maxDims.z)*mag;
	var d3 = this._draw3d;
	
	var hrot = model.getCameraHorizRotation()/180*Math.PI;
	var vrot = model.getCameraVertRotation()/180*Math.PI;
	var hrotSin = IDB.MathCache.sin(hrot);
	var hrotCos = IDB.MathCache.cos(hrot);
	var vrotSin = IDB.MathCache.sin(vrot);
	var vrotCos = IDB.MathCache.cos(vrot);
	
	var tv0 = new IDB.Vec3(0, maxDims.y, 0);
	var tv1 = new IDB.Vec3(maxDims.x, maxDims.y, 0);
	var tv2 = new IDB.Vec3(maxDims.x, maxDims.y, maxDims.z);
	var tv3 = new IDB.Vec3(0, maxDims.y, maxDims.z);
	var bv1 = new IDB.Vec3(maxDims.x, 0, 0);
	var bv2 = new IDB.Vec3(maxDims.x, 0, maxDims.z);
	var t0, t1, t2, t3, b1, b2; //projected tops/bots
	
	var len = 0;
	var count = 0;
	var z = 0;
	var zMax = (lenInit*1000)/mag;
	var zMin = -zMax*10;
	var min = 0;
	var maxW = this._width;
	var maxH = (this._rotateXLabels ? height : height-Math.min(height/5, this._xLabelMaxW));
	var r, h;

	//Search for correct initial zoom level, uses a pseudo-binary search implementation

	while ( count++ < 50 ) { //Fail-safe: should never reach 50 (probably not even 25)
		len = lenInit - mag/1000*z;

		if ( zMax-zMin < 2 ) {
			break;
		}
		
		r = vrotCos*len;
		h = vrotSin*len;

		this.updateCameraLook(r*hrotSin, h, -r*hrotCos);
		
		t0 = d3.project(tv0);
		t1 = d3.project(tv1);
		t2 = d3.project(tv2);
		t3 = d3.project(tv3);
		b1 = d3.project(bv1);
		b2 = d3.project(bv2);
		
		if ( t0.y < min || t1.y > maxH || t2.y > maxH || t3.y < min ||
				b1.y > maxH || b2.y > maxH || t0.x < min || t1.x > maxW ||
				t2.x > maxW || t3.x < min || b1.x > maxW || b2.x > maxW ) {
			zMax = z; //Graph currently zoomed too large
		}
		else {
			zMin = z; //Graph currently zoomed too small
		}
		
		z = (zMax-zMin)/2 + zMin;
	}
	
	len *= (model.getCameraZoom()*0.5)+0.5;
	r = vrotCos*len;
	h = vrotSin*len;

	this.updateCameraLook(r*hrotSin, h, -r*hrotCos);
	return (len/1000000);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype.updateCameraLook = function(x, y, z) {
	this._draw3d.lookAt(
		new IDB.Vec3(this._center.x+x, this._center.y+y, this._center.z+z),
		new IDB.Vec3(this._center.x, this._center.y, this._center.z),
		new IDB.Vec3(0, 1, 0)
	);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype.render = function() {
	var maxDims = this._maxDims;
	var yAxes = this._yAxes;
	var yAxesLen = yAxes.length;
	var d3 = this._draw3d;
	
	var b0 = d3.project(new IDB.Vec3(0, 0, 0));
	var b1 = d3.project(new IDB.Vec3(0, 0, maxDims.z));
	var b2 = d3.project(new IDB.Vec3(maxDims.x, 0, maxDims.z));
	var b3 = d3.project(new IDB.Vec3(maxDims.x, 0, 0));

	var t0 = d3.project(new IDB.Vec3(0, maxDims.y, 0));
	var t1 = d3.project(new IDB.Vec3(0, maxDims.y, maxDims.z));
	var t2 = d3.project(new IDB.Vec3(maxDims.x, maxDims.y, maxDims.z));
	
	this._gridBackFace.points([
		b1.x, b1.y,
		t1.x, t1.y,
		t2.x, t2.y,
		b2.x, b2.y
	]);

	this._gridLeftFace.points([
		b1.x, b1.y,
		t1.x, t1.y,
		t0.x, t0.y,
		b0.x, b0.y
	]);

	this._gridBottomFace.points([
		b0.x, b0.y,
		b1.x, b1.y,
		b2.x, b2.y,
		b3.x, b3.y
	]);
	
	var i, yAxis;
	
	for ( i = 0 ; i < yAxesLen ; ++i ) {
		yAxis = yAxes[i];
		yAxis.setMaxDims(maxDims);
		yAxis.render();
	}

	if ( this._xGridView ) {
		this._xGridView.render(this._height);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxesView.prototype._onActiveYAxisChange = function() {
	var views = this._yAxes;
	var i;

	for ( i in views ) {
		views[i].updateVisible();
	}
};

/*====================================================================================================*/
IDB.Cartesian3dGridAxisXView = function(model, draw3d, rotateXLabels, gridColor) {
	this._model = model;
	this._model.setOnVisualStateChange(IDB.listener(this, "_onVisualStateChange"));
	this._draw3d = draw3d;
	this._rotateXLabels = rotateXLabels;
	this._gridColor = gridColor;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisXView.prototype._buildKinetic = function() {
	var model = this._model;
	var labelTexts = model.getLabelTexts();
	var rot = this._rotateXLabels;
	var gridCol = this._gridColor;
	var i, lbl, lblHold, line;

	this._display = new Kinetic.Group();

	this._labels = [];
	this._labelHolds = [];
	this._lines = [];

	for ( i in labelTexts ) {
		lblHold = new Kinetic.Group();
		this._display.add(lblHold);
		this._labelHolds[i] = lblHold;

		lbl = new IDB.GraphLabel(model.getXLabelOptions(), labelTexts[i]);
		lbl.rotateFromCenter(rot ? 0 : -90);
		lblHold.add(lbl.getDisplay());
		this._labels[i] = lbl;

		line = new Kinetic.Line({
			stroke: IDB.toRGBA(gridCol, 0.6),
			strokeWidth: 1,
			points: []
		});
		this._display.add(line);
		this._lines[i] = line;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisXView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisXView.prototype.render = function(height) {
	var d3 = this._draw3d;
	var labels = this._labels;
	var labelHolds = this._labelHolds;
	var lines = this._lines;
	var rot = this._rotateXLabels;
	var labelsLen = labels.length;
	var lastPos = -99999;
	var i, lbl, lblHold, line, p, p2, lblTw, lblPush, currPos;
	
	for ( i = 0 ; i < labelsLen ; ++i ) {
		lbl = labels[i];
		lblHold = labelHolds[i];
		line = lines[i];
		
		p = d3.project(new IDB.Vec3(i+0.5, 0, 0));
		p2 = d3.project(new IDB.Vec3(i+0.5, 0, -0.25));
		
		line.points([
			p.x, p.y,
			p2.x, p2.y
		]);
		
		lbl.resetText();
		lblTw = lbl.textWidth;
		lbl.width = Math.min(lbl.textWidth, height/5);
		lbl.truncate();
		lblPush = lbl.width-lblTw/2+3;

		if ( rot ) {
			lblHold.x(p2.x-lblPush);
			lblHold.y(p2.y);
			currPos = lblHold.y();
		}
		else {
			lblHold.x(p2.x);
			lblHold.y(p2.y+lblPush);
			currPos = lblHold.x();
		}
		
		if ( currPos < lastPos ) {
			lbl.setOverlap(true);
		}
		else {
			lbl.setOverlap(false);
			lastPos = currPos+lbl.height*0.85;
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisXView.prototype._onVisualStateChange = function(index, visualState) {
	var lbl = this._labels[index];

	switch ( visualState ) {
		case IDB.VS.HIGHLIGHTED:
			lbl.setHighlight(true);
			this._labelHolds[index].moveToTop();
			break;
			
		case IDB.VS.NORMAL:
		case IDB.VS.FADED:
			lbl.setHighlight(false);
			break;
	}
};

/*====================================================================================================*/
IDB.Cartesian3dGridAxisYLimitView = function(model, showFrontGrid) {
	this._model = model;
	this._frontGrid = showFrontGrid;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYLimitView.prototype._buildKinetic = function() {
	var model = this._model;
	var lineConfig = {
		stroke: model.color.css,
		strokeWidth: 1,
		dash: [4, 3]
	};
	
	this._lineBack = new Kinetic.Line(lineConfig);

	if ( this._frontGrid ) {
		this._lineFore = new Kinetic.Line(lineConfig);
	}
	
	if ( model.labelOptions.show ) {
		model.labelOptions.align = 'right';
		this._label = new IDB.GraphLabel(model.labelOptions, model.valueStr);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYLimitView.prototype.getModelPosition = function() {
	return this._model.position;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYLimitView.prototype.getLineBack = function() {
	return this._lineBack;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYLimitView.prototype.getLineFore = function() {
	return this._lineFore;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYLimitView.prototype.getLabel = function() {
	return this._label;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYLimitView.prototype.updateLines = function(p0, p1, p2, p3) {
	var lineFore = this._lineFore;
	var lbl = this._label;

	this._lineBack.points([
		p0.x, p0.y,
		p3.x, p3.y,
		p2.x, p2.y
	]);

	if ( lineFore ) {
		lineFore.points([
			p0.x, p0.y,
			p1.x, p1.y,
			p2.x, p2.y
		]);
	}

	if ( lbl ) {
		lbl.x = p0.x-lbl.width-4;
		lbl.y = p0.y-lbl.height/2;
	}
};

/*====================================================================================================*/
IDB.Cartesian3dGridAxisYView = function(model, draw3d) {
	this._model = model;
	this._draw3d = draw3d;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYView.prototype._buildKinetic = function() {
	var model = this._model;
	var gridCol = model.getGridColor();
	var numDivs = model.getDivsionCount();
	var lblOpt = model.getLeftLabelOptions();
	var labels = model.getLeftLabelValues();
	var showFrontGrid = model.getTickFrontShow();
	var lbl = null;
	var i, lineAlpha, backLine, foreLine;
	
	this._displayBack = new Kinetic.Group();
	this._displayFore = new Kinetic.Group();
	this._levels = [];
	this._limits = [];

	lblOpt.align = 'right';

	for ( i = 0 ; i < numDivs ; ++i ) {
		lineAlpha = (i == 0 ? 1 : 0.25);

		backLine = new Kinetic.Line({
			stroke: IDB.toRGBA(gridCol, lineAlpha),
			strokeWidth: 1
		});
		this._displayBack.add(backLine);
		
		if ( lblOpt.show ) {
			lbl = new IDB.GraphLabel(lblOpt, labels[i]);
			this._displayBack.add(lbl.getDisplay());
		}

		if ( showFrontGrid || !i ) {
			foreLine = new Kinetic.Line({
				stroke: IDB.toRGBA(gridCol, lineAlpha),
				strokeWidth: 1
			});
			this._displayFore.add(foreLine);
		}
		else {
			foreLine = null;
		}
		
		this._levels[i] = {
			backLine: backLine,
			label: lbl,
			foreLine: foreLine
		};
	}
	
	if ( showFrontGrid ) {
		this._frontCorner = new Kinetic.Line({
			stroke: IDB.toRGBA(gridCol, 0.2),
			strokeWidth: 1
		});
		this._displayFore.add(this._frontCorner);
	}

	this._buildLimit(true);
	this._buildLimit(false);
	this.updateVisible();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYView.prototype._buildLimit = function(isUpper) {
	var model = this._model;
	var obj = model.getLimitObject(isUpper);

	if ( !obj.show || obj.position < 0 || obj.position > 1 ) {
		return null;
	}

	var showFrontGrid = (this._frontCorner != null);
	var limit = new IDB.Cartesian3dGridAxisYLimitView(obj, showFrontGrid);
	var lineBack = limit.getLineBack();
	var lineFore = limit.getLineFore();
	var label = limit.getLabel();

	this._displayBack.add(lineBack);
	
	if ( label ) {
		this._displayBack.add(label.getDisplay());
	}

	if ( lineFore ) {
		this._displayFore.add(lineFore);
	}

	this._limits.push(limit);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYView.prototype.getDisplayBack = function() {
	return this._displayBack;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYView.prototype.getDisplayFore = function() {
	return this._displayFore;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYView.prototype.updateVisible = function() {
	var vis = this._model.getActive();
	this._displayBack.visible(vis);
	this._displayFore.visible(vis);
	return vis;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYView.prototype.setMaxDims = function(maxDims) {
	this._maxDims = maxDims;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridAxisYView.prototype.render = function() {
	var d3 = this._draw3d;
	var levels = this._levels;
	var maxDims = this._maxDims;
	var limits = this._limits;
	var levelsLen = levels.length;
	var yDist = maxDims.y/(levelsLen-1);
	var nextY = 99999;

	var v0 = new IDB.Vec3(0, 0, 0);
	var v1 = new IDB.Vec3(maxDims.x, 0, 0);
	var v2 = new IDB.Vec3(maxDims.x, 0, maxDims.z);
	var v3 = new IDB.Vec3(0, 0, maxDims.z);
	var i, level, py, p0, p1, p2, p3, lbl, limI, limit, limitLbl;

	for ( i = 0 ; i < levelsLen ; ++i ) {
		level = levels[i];
		py = i*yDist;

		p0 = d3.project(new IDB.Vec3(v0.x, py, v0.z));
		p1 = d3.project(new IDB.Vec3(v1.x, py, v1.z));
		p2 = d3.project(new IDB.Vec3(v2.x, py, v2.z));
		p3 = d3.project(new IDB.Vec3(v3.x, py, v3.z));
		
		level.backLine.points([
			p2.x, p2.y,
			p3.x, p3.y,
			p0.x, p0.y
		]);
		
		if ( level.foreLine ) {
			level.foreLine.points([
				p0.x, p0.y,
				p1.x, p1.y,
				p2.x, p2.y
			]);
		}
		
		if ( level.label ) {
			lbl = level.label;
			lbl.resetText();
			lbl.width = Math.min(lbl.textWidth, p0.x);
			lbl.truncate();
			lbl.x = p0.x-lbl.width-4;
			lbl.y = p0.y-(i == 0 ? lbl.height : (i == levelsLen-1 ? 0 : lbl.height/2));

			if ( lbl.y+lbl.height*0.85 > nextY ) {
				lbl.setOverlap(true);
			}
			else {
				lbl.setOverlap(false);
				nextY = lbl.y;
			}
		}
	}
	
	if ( this._frontCorner ) {
		p1 = d3.project(new IDB.Vec3(v1.x, 0, v1.z));
		p2 = d3.project(new IDB.Vec3(v1.x, maxDims.y, v1.z));

		this._frontCorner.points([
			p1.x, p1.y,
			p2.x, p2.y
		]);
	}

	for ( limI in limits ) {
		limit = limits[limI];
		limitLbl = limit.getLabel();
		py = (1-limit.getModelPosition())*maxDims.y;

		p0 = d3.project(new IDB.Vec3(v0.x, py, v0.z));
		p1 = d3.project(new IDB.Vec3(v1.x, py, v1.z));
		p2 = d3.project(new IDB.Vec3(v2.x, py, v2.z));
		p3 = d3.project(new IDB.Vec3(v3.x, py, v3.z));
		
		limit.updateLines(p0, p1, p2, p3);

		for ( i = 0 ; limitLbl && i < levelsLen ; ++i ) {
			level = levels[i];

			if ( !level.label ) {
				break;
			}

			if ( level.label.getOverlap() ) {
				continue;
			}

			level.label.setOverlap(
				Math.abs(limitLbl.y-level.label.y) < limitLbl.height*0.8
			);
		}
	}
};

/*====================================================================================================*/
IDB.Cartesian3dGridView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();

	this._axes = new IDB.Cartesian3dGridAxesView(this._model.getAxes());
	this._display.add(this._axes.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridView.prototype.getPlotDisplay = function() {
	return this._axes.getPlotDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridView.prototype.getDraw3d = function() {
	return this._axes.getDraw3d();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridView.prototype.getMaxDimensions = function() {
	return this._axes.getMaxDimensions();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridView.prototype.updateScreen = function(width, height) {
	this._axes.updateScreen(width, height);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridView.prototype.updateCamera = function() {
	this._axes.updateCamera();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Cartesian3dGridView.prototype.render = function() {
	this._axes.render();
};

/*====================================================================================================*/
IDB.Draw3D = function() {
	this._cam = new IDB.Mat4x4();
	this._proj = new IDB.Mat4x4();
	this._persAndCam = new IDB.Mat4x4();
	this._halfW = 1;
	this._halfH = 1;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Draw3D.prototype.getWidthRatio = function() {
	return this._halfW/this._halfH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Draw3D.prototype.setScreen = function(w, h, n, f, fov) {
	var angle = fov/360*Math.PI;
	var proj = this._proj;
	
	proj.loadIdentity();
	proj.a1 = 1/Math.tan(angle);
	proj.b2 = 1/Math.tan(angle*(h/w));
	proj.c3 = (f+n)/(f-n);
	proj.c4 = (-2*f*n)/(f-n);
	proj.d3 = 1;
	
	this._halfW = w/2;
	this._halfH = h/2;
	
	this._persAndCam = IDB.Mat4x4.mult(proj, this._cam);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Draw3D.prototype.lookAt = function(eye, targ, up) {
	up.normalize();

	var f = IDB.Vec3.vecSub(targ, eye).normalized();
	var s = f.crossedWith(up).normalized();
	var u = s.crossedWith(f);
	
	var mat = new IDB.Mat4x4();
	mat.translate(-eye.x, -eye.y, -eye.z);
	
	var cam = this._cam;
	cam.a1 = s.x;
	cam.a2 = s.y;
	cam.a3 = s.z;
	cam.b1 = u.x;
	cam.b2 = u.y;
	cam.b3 = u.z;
	cam.c1 = -f.x;
	cam.c2 = -f.y;
	cam.c3 = -f.z;
	
	this._persAndCam = IDB.Mat4x4.mult(
		this._proj,
		IDB.Mat4x4.mult(cam, mat)
	);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Draw3D.prototype.project = function(point) {
	var sv = IDB.Mat4x4.mult4x4WithVec3(this._persAndCam, point);
	var invW = 1/sv.w;

	return {
		x: sv.x*invW*this._halfW + this._halfW,
		y: sv.y*invW*this._halfH + this._halfH
	};
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Draw3D.shadeColor = function(color, shade) {
	if ( !shade ) {
		return color;
	}
	
	var r = (color >> 16);
	var g = (color >> 8) - (r << 8);
	var b = color - (r << 16) - (g << 8);
	var n = Math.abs(shade);
	var mix = (shade > 0 ? 255 : 0);
	
	r = r*(1-n) + mix*n;
	g = g*(1-n) + mix*n;
	b = b*(1-n) + mix*n;
	return (r << 16) ^ (g << 8) ^ b;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Draw3D.getAngledFaceShade = function(a, b, segmentRelWidth, upsideDown) {
	if ( !IDB.Draw3D._LightVec ) {
		IDB.Draw3D._LightVec = new IDB.Vec3(-1, 1, 0).normalized();
	}

	var lightVec = IDB.Draw3D._LightVec;
	var normal = new IDB.Vec3((b.x-a.x)*2*segmentRelWidth, b.y-a.y, 0).normalized();

	var swap = normal.x;
	normal.x = normal.y;
	normal.y = swap;

	return IDB.Vec3.vecDot(normal, lightVec)*0.5*(upsideDown ? -1 : 1);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Draw3D.setFacePoints = function(face, points) {
	if ( !face ) {
		return;
	}

	var coords = [];

	for ( var i in points ) {
		coords.push(points[i].x, points[i].y);
	}

	face.points(coords);
};

/*====================================================================================================*/
IDB.Mat4x4 = function() {
	this.loadIdentity();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Mat4x4.prototype.loadIdentity = function() {
	this.a1 = 1;
	this.a2 = 0;
	this.a3 = 0;
	this.a4 = 0;
	this.b1 = 0;
	this.b2 = 1;
	this.b3 = 0;
	this.b4 = 0;
	this.c1 = 0;
	this.c2 = 0;
	this.c3 = 1;
	this.c4 = 0;
	this.d1 = 0;
	this.d2 = 0;
	this.d3 = 0;
	this.d4 = 1;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Mat4x4.prototype.translate = function(px, py, pz) {
	var m = new IDB.Mat4x4();
	m.a4 = px;
	m.b4 = py;
	m.c4 = pz;
	this.mult(m);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Mat4x4.prototype.scale = function(px, py, pz) {
	var m = new IDB.Mat4x4();
	m.a1 *= px;
	m.b2 *= py;
	m.c3 *= pz;
	this.mult(m);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Mat4x4.prototype.rotate = function(degrees, px, py, pz) {
	var invLen = 1/Math.sqrt(px*px + py*py + pz*pz);
	px *= invLen;
	py *= invLen;
	pz *= invLen;
	
	var a = degrees/180*Math.PI;
	var c = Math.cos(a);
	var s = Math.sin(a);
	var t = 1-c;
	var m = new IDB.Mat4x4();
	
	//From: www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/
	
	m.a1 = t*px*px + c;
	m.a2 = t*px*py - pz*s;
	m.a3 = t*pz*pz + py*s;
	
	m.b1 = t*px+py + pz*s;
	m.b2 = t*py*py + c;
	m.b3 = t*py*pz - px*s;
	
	m.c1 = t*px*pz - py*s;
	m.c2 = t*py*pz + px*s;
	m.c3 = t*pz*pz + c;
	
	this.mult(m);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Mat4x4.prototype.mult = function(m) {
	this.a1 = this.a1*m.a1 + this.a2*m.b1 + this.a3*m.c1 + this.a4*m.d1;
	this.a2 = this.a1*m.a2 + this.a2*m.b2 + this.a3*m.c2 + this.a4*m.d2;
	this.a3 = this.a1*m.a3 + this.a2*m.b3 + this.a3*m.c3 + this.a4*m.d3;
	this.a4 = this.a1*m.a4 + this.a2*m.b4 + this.a3*m.c4 + this.a4*m.d4;
	
	this.b1 = this.b1*m.a1 + this.b2*m.b1 + this.b3*m.c1 + this.b4*m.d1;
	this.b2 = this.b1*m.a2 + this.b2*m.b2 + this.b3*m.c2 + this.b4*m.d2;
	this.b3 = this.b1*m.a3 + this.b2*m.b3 + this.b3*m.c3 + this.b4*m.d3;
	this.b4 = this.b1*m.a4 + this.b2*m.b4 + this.b3*m.c4 + this.b4*m.d4;
	
	this.c1 = this.c1*m.a1 + this.c2*m.b1 + this.c3*m.c1 + this.c4*m.d1;
	this.c2 = this.c1*m.a2 + this.c2*m.b2 + this.c3*m.c2 + this.c4*m.d2;
	this.c3 = this.c1*m.a3 + this.c2*m.b3 + this.c3*m.c3 + this.c4*m.d3;
	this.c4 = this.c1*m.a4 + this.c2*m.b4 + this.c3*m.c4 + this.c4*m.d4;
	
	this.d1 = this.d1*m.a1 + this.d2*m.b1 + this.d3*m.c1 + this.d4*m.d1;
	this.d2 = this.d1*m.a2 + this.d2*m.b2 + this.d3*m.c2 + this.d4*m.d2;
	this.d3 = this.d1*m.a3 + this.d2*m.b3 + this.d3*m.c3 + this.d4*m.d3;
	this.d4 = this.d1*m.a4 + this.d2*m.b4 + this.d3*m.c4 + this.d4*m.d4;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Mat4x4.mult4x4WithVec3 = function(mat, vec) {
	var v = new IDB.Vec3(0, 0, 0);
	v.x = mat.a1*vec.x + mat.a2*vec.y + mat.a3*vec.z + mat.a4;
	v.y = mat.b1*vec.x + mat.b2*vec.y + mat.b3*vec.z + mat.b4;
	v.z = mat.c1*vec.x + mat.c2*vec.y + mat.c3*vec.z + mat.c4;
	v.w = mat.d1*vec.x + mat.d2*vec.y + mat.d3*vec.z + mat.d4;
	return v;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Mat4x4.mult = function(m, n) {
	var mat = new IDB.Mat4x4();
	
	mat.a1 = m.a1*n.a1 + m.a2*n.b1 + m.a3*n.c1 + m.a4*n.d1;
	mat.a2 = m.a1*n.a2 + m.a2*n.b2 + m.a3*n.c2 + m.a4*n.d2;
	mat.a3 = m.a1*n.a3 + m.a2*n.b3 + m.a3*n.c3 + m.a4*n.d3;
	mat.a4 = m.a1*n.a4 + m.a2*n.b4 + m.a3*n.c4 + m.a4*n.d4;
	
	mat.b1 = m.b1*n.a1 + m.b2*n.b1 + m.b3*n.c1 + m.b4*n.d1;
	mat.b2 = m.b1*n.a2 + m.b2*n.b2 + m.b3*n.c2 + m.b4*n.d2;
	mat.b3 = m.b1*n.a3 + m.b2*n.b3 + m.b3*n.c3 + m.b4*n.d3;
	mat.b4 = m.b1*n.a4 + m.b2*n.b4 + m.b3*n.c4 + m.b4*n.d4;
	
	mat.c1 = m.c1*n.a1 + m.c2*n.b1 + m.c3*n.c1 + m.c4*n.d1;
	mat.c2 = m.c1*n.a2 + m.c2*n.b2 + m.c3*n.c2 + m.c4*n.d2;
	mat.c3 = m.c1*n.a3 + m.c2*n.b3 + m.c3*n.c3 + m.c4*n.d3;
	mat.c4 = m.c1*n.a4 + m.c2*n.b4 + m.c3*n.c4 + m.c4*n.d4;
	
	mat.d1 = m.d1*n.a1 + m.d2*n.b1 + m.d3*n.c1 + m.d4*n.d1;
	mat.d2 = m.d1*n.a2 + m.d2*n.b2 + m.d3*n.c2 + m.d4*n.d2;
	mat.d3 = m.d1*n.a3 + m.d2*n.b3 + m.d3*n.c3 + m.d4*n.d3;
	mat.d4 = m.d1*n.a4 + m.d2*n.b4 + m.d3*n.c4 + m.d4*n.d4;
	
	return mat;
};

/*====================================================================================================*/
IDB.Vec3 = function(px, py, pz) {
	this.x = px;
	this.y = py;
	this.z = pz;
	this.w = 0;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------* /
IDB.Vec3.prototype.getMat4x1 = function() {
	var m = new IDB.Mat(4,1);
	m.setValues([x,y,z,1]);
	return m;
};*/


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.addV = function(vec) {
	this.x += vec.x;
	this.y += vec.y;
	this.z += vec.z;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.subV = function(vec) {
	this.x -= vec.x;
	this.y -= vec.y;
	this.z -= vec.z;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.multV = function(vec) {
	this.x *= vec.x;
	this.y *= vec.y;
	this.z *= vec.z;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.divV = function(vec) {
	this.x /= vec.x;
	this.y /= vec.y;
	this.z /= vec.z;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.multS = function(s) {
	this.x *= s;
	this.y *= s;
	this.z *= s;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.divS = function(s) {
	var invS = 1/s;
	this.x *= invS;
	this.y *= invS;
	this.z *= invS;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.setV = function(vec) {
	this.x = vec.x;
	this.y = vec.y;
	this.z = vec.z;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.equalsV = function(vec) {
	return (
		this.x == vec.x &&
		this.y == vec.y &&
		this.z == vec.z
	);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.vecAdd = function(a, b) {
	return new IDB.Vec3(a.x+b.x, a.y+b.y, a.z+b.z);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.vecSub = function(a, b) {
	return new IDB.Vec3(a.x-b.x, a.y-b.y, a.z-b.z);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.vecMult = function(a, s) {
	return new IDB.Vec3(a.x*s, a.y*s, a.z*s);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.vecMultV = function(a, b) {
	return new IDB.Vec3(a.x*b.x, a.y*b.y, a.z*b.z);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.vecDiv = function(a, s) {
	var invS = 1/s;
	return new IDB.Vec3(a.x*invS, a.y*invS, a.z*invS);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.vecDot = function(a, b) {
	return a.x*b.x + a.y*b.y + a.z*b.z;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.mag = function() {
	return this.x*this.x + this.y*this.y + this.z*this.z;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.len = function() {
	return Math.sqrt(this.x*this.x + this.y*this.y + this.z*this.z);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.normalized = function() {
	var len = this.len();

	if ( !len ) {
		return new IDB.Vec3(0, 0, 0);
	}

	var v = new IDB.Vec3(this.x, this.y, this.z);
	v.divS(len);
	return v;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.crossedWith = function(vec) {
	return new IDB.Vec3(
		(this.y*vec.z)-(this.z*vec.y),
		(this.z*vec.x)-(this.x*vec.z),
		(this.x*vec.y)-(this.y*vec.x)
	);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.normalize = function() {
	var len = this.len();

	if ( !len ) {
		return;
	}

	this.divS(len);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.normalizeAtLength = function(currLen) {
	if ( !currLen ) {
		return;
	}

	this.divS(currLen);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.normalizeToLength = function(newLen) {
	var len = this.len();
	
	if ( !len ) {
		return;
	}

	this.multS(newLen/len);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Vec3.prototype.toString = function() {
	return "Vec3[ "+
		Math.floor(this.x*1000)/1000+", "+
		Math.floor(this.y*1000)/1000+", "+
		Math.floor(this.z*1000)/1000+" ]";
};
/*====================================================================================================*/

