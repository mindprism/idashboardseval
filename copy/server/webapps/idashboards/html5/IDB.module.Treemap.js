'use strict';
IDB.TreemapAlgorithm = {}; //static class


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapAlgorithm.sliceAndDice = function(nodes, rect) {
	var nodesLen = nodes.length;
	var splitRatio = 0.5;
	var totalSum = 0;
	var splitSum = 0;
	var useRatio = 0;
	var i, node, sortedNodes, nodesA, nodesB, useA, splitRectA, splitRectB;

	if ( !nodesLen ) {
		return;
	}

	if ( nodesLen == 1 ) {
		node = nodes[0];
		node.rect = rect;
		//console.log(' * END: '+node.name+' = '+node.valueSum+' / %o', node.rect);
		return;
	}

	// Calculate sum and sort nodes
	
	sortedNodes = [];

	for ( i = 0 ; i < nodesLen ; ++i ) {
		node = nodes[i];
		totalSum += node.valueSum;
		sortedNodes.push(node);
	}

	sortedNodes.sort(IDB.TreemapAlgorithm._compareTreemapNodes);

	// Obtain node lists before and after the split
	
	nodesA = [];
	nodesB = [];

	for ( i = 0 ; i < nodesLen ; ++i ) {
		node = sortedNodes[i];
		splitSum += node.valueSum;
				
		useA = (i == 0 || splitSum <= totalSum*splitRatio);
		(useA ? nodesA : nodesB).push(node);
				
		if ( useA ) {
			useRatio = splitSum/totalSum;
		}
	}

	// Calculate the rectangle split

	splitRectA = IDB.Util.copyProps(rect, {});

	if ( rect.width > rect.height ) {
		splitRectA.width *= useRatio;
		splitRectB = IDB.Util.copyProps(splitRectA, {});
		splitRectB.x += splitRectA.width;
		splitRectB.width = rect.width-splitRectA.width;
	}
	else {
		splitRectA.height *= useRatio;
		splitRectB = IDB.Util.copyProps(splitRectA, {});
		splitRectB.y += splitRectA.height;
		splitRectB.height = rect.height-splitRectA.height;
	}

	// Recurse into the split lists

	IDB.TreemapAlgorithm.sliceAndDice(nodesA, splitRectA);
	IDB.TreemapAlgorithm.sliceAndDice(nodesB, splitRectB);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapAlgorithm.squarify = function(nodes, rect) {
	var nodesLen = nodes.length;
	var i, node, sortedNodes;

	if ( !nodesLen ) {
		return;
	}
	
	if ( nodesLen == 1 ) {
		node = nodes[0];
		node.rect = rect;
		//console.log(' * END: '+node.name+' = '+node.valueSum+' / %o', node.rect);
		return;
	}

	// Calculate sum and sort nodes

	sortedNodes = [];

	for ( i = 0 ; i < nodesLen ; ++i ) {
		sortedNodes.push(nodes[i]);
	}

	sortedNodes.sort(IDB.TreemapAlgorithm._compareTreemapNodes);
	IDB.TreemapAlgorithm.squarifyInner(sortedNodes, rect);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapAlgorithm.squarifyInner = function(nodes, rect) {
	var nodesLen = nodes.length;
	var horizSide = rect.width/rect.height;
	var vertSide = 1/horizSide;
	var shortestSide = Math.min(horizSide, vertSide);
	var isHoriz = (rect.width > rect.height);
	var shortestSideSqr = shortestSide*shortestSide;
	//console.log('--- '+pRect.width+' / '+pRect.height+' / '+shortestSide);

	var row = [];
	var rowMaxArea = 0;
	var rowMinArea = 1;
	var rowSumArea = 0;
	var rowRatio = Number.POSITIVE_INFINITY;

	var nodeTotalSum = 0;
	var remainNodes = [];
	var i, node, nodeArea, rowMaxAreaWithNode, rowMinAreaWithNode, rowSumAreaWithNode, 
		rowRatioWithNode, rowSumAreaWithNodeSqr, rowRect, remainRect, rowNodePos

	for ( i = 0 ; i < nodesLen ; ++i ) {
		node = nodes[i];
		nodeTotalSum += node.valueSum;
		remainNodes.push(node);
	}

	while ( remainNodes.length > 0 ) {
		node = remainNodes[0];
		nodeArea = node.valueSum/nodeTotalSum;

		rowMaxAreaWithNode = Math.max(rowMaxArea, nodeArea);
		rowMinAreaWithNode = Math.min(rowMinArea, nodeArea);
		rowSumAreaWithNode = rowSumArea+nodeArea;
		rowSumAreaWithNodeSqr = Math.pow(rowSumAreaWithNode, 2);

		rowRatioWithNode = Math.max(
			shortestSideSqr*rowMaxAreaWithNode/(rowSumAreaWithNodeSqr),
			rowSumAreaWithNodeSqr/(shortestSideSqr*rowMinAreaWithNode)
		);

		//console.log(remainNodes.length+': '+nodeArea+' ... '+rowRatio+' / '+rowRatioWithNode);

		if ( rowRatio <= rowRatioWithNode ) {
			break;
		}

		row.push(node);
		remainNodes.shift();

		rowMaxArea = rowMaxAreaWithNode;
		rowMinArea = rowMinAreaWithNode;
		rowSumArea = rowSumAreaWithNode;
		rowRatio = rowRatioWithNode;
	}

	////
	
	rowRect = IDB.Util.copyProps(rect, {});
	remainRect = IDB.Util.copyProps(rect, {});

	if ( isHoriz ) {
		rowRect.width *= rowSumArea;
		rowNodePos = rowRect.y;
		remainRect.x += rowRect.width;
		remainRect.width -= rowRect.width;
	}
	else {
		rowRect.height *= rowSumArea;
		rowNodePos = rowRect.x;
		remainRect.y += rowRect.height;
		remainRect.height -= rowRect.height;
	}

	//console.log('# '+pr(pRect)+' // '+pr(rowRect)+' // '+pr(remainRect)+' ... '+rowSumArea);

	for ( i = 0 ; i < row.length ; ++i ) {
		node = row[i];
		node.rect = IDB.Util.copyProps(rowRect, {});

		nodeArea = node.valueSum/nodeTotalSum/rowSumArea;

		if ( !isHoriz ) {
			node.rect.x = rowNodePos;
			node.rect.width *= nodeArea;
			rowNodePos += node.rect.width;
		}
		else {
			node.rect.y = rowNodePos;
			node.rect.height *= nodeArea;
			rowNodePos += node.rect.height;
		}

		//console.log(' * END: '+node.name+' = '+node.valueSum+' / %o', node.rect);
		//console.log('   - '+pr(node.rect)+' ... '+node.valueSum/nodeTotalSum+' => '+nodeArea);
	}

	////

	if ( remainNodes.length ) {
		IDB.TreemapAlgorithm.squarifyInner(remainNodes, remainRect);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------* /
IDB.TreemapAlgorithm.pr = function(rect) {
	return 'R('+Math.round(rect.x)+', '+Math.round(rect.y)+', '+
		Math.round(rect.width)+', '+Math.round(rect.height)+')';
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapAlgorithm._compareTreemapNodes = function(nodeA, nodeB) {
	var a = nodeA.valueSum;
	var b = nodeB.valueSum;
	return (a == b ? 0 : (a > b ? -1 : 1));
};

/*====================================================================================================*/
IDB.TreemapBox = function(graphContext) {
	this._graphContext = graphContext;
};

IDB.TreemapBox.ShadingNone = 0;
IDB.TreemapBox.ShadingCushion = 1;
IDB.TreemapBox.ShadingMatte = 2;
IDB.TreemapBox.ShadingGloss = 3;
IDB.TreemapBox.ShadingChrome = 4;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.init = function(node, parentNodes) {
	var isNodeChanged = (node != this._node);
	var dp = node.datapoint;
	var groupText = '';
	var parentNode, percDec;

	this._node = node;

	if ( this.getNameLabelOptions().show && isNodeChanged ) {
		this._nameText = this._getNameLabel(dp.datapointRow);
	}

	if ( this.getValueLabelOptions().show && isNodeChanged ) {
		this._valueText = dp.yValue.formattedValue;
	}

	if ( this.getGroupLabelOptions().show && isNodeChanged ) {
		for ( var i = 1 ; i < parentNodes.length ; ++i ) {
			parentNode = parentNodes[i];
			groupText += (i == 1 ? '' : ', ') + parentNode.datapoint.yValue.formattedValue;
		}

		this._groupText = groupText;
	}

	if ( this.getPercentLabelOptions().show && isNodeChanged ) {
		percDec = this._graphContext.settings.get('TreemapPercentLabelDecimals');
		this._percentText = (node.valuePercent*100).toFixed(percDec)+'%';
	}

	if ( this._onNodeChanged ) {
		this._onNodeChanged();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype._getNameLabel = function(datapointRow) {
	if ( !datapointRow ) {
		return '';
	}

	var dp = datapointRow.datapoints[0];
	var sett = this._graphContext.settings;
	var template = sett.get('TreemapNameLabelTemplate');

	if ( !template ) {
		return dp.xValue.formattedValue;
	}

	return IDB.ChartUtils.expandMacros(template, 'html', null, null, sett.get('chart'), dp);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.setOnNodeChanged = function(callback) {
	this._onNodeChanged = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getNode = function() {
	return this._node;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getNameLabelText = function() {
	return this._nameText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getValueLabelText = function() {
	return this._valueText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getGroupLabelText = function() {
	return this._groupText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getPercentLabelText = function() {
	return this._percentText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getXColor = function() {
	return this._graphContext.datapointGrid.getColorForXValue(this._node.datapoint.xValue.stringValue);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getNameLabelOptions = function() {
	return this._graphContext.settings.get('TreemapNameLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getValueLabelOptions = function() {
	return this._graphContext.settings.get('TreemapValueLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getGroupLabelOptions = function() {
	return this._graphContext.settings.get('TreemapGroupLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getPercentLabelOptions = function() {
	return this._graphContext.settings.get('TreemapPercentLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.hasGroupAxis = function() {
	return IDB.TreemapGraph.hasGroupAxis(this._graphContext.settings);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.hasShading = function() {
	return (this.getShading() != IDB.TreemapBox.ShadingNone);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getShading = function() {
	return this._graphContext.settings.get('TreemapBoxShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getPadding = function() {
	return this._graphContext.settings.get('TreemapBoxPadding');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getCornerRadius = function() {
	return this._graphContext.settings.get('TreemapBoxCornerRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.useXColor = function() {
	return this._graphContext.settings.getBool('TreemapDefaultBoxUseXColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.getDefaultColor = function() {
	return this._graphContext.settings.get('TreemapDefaultBoxColor');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, '_handleClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.createOverListener = function() {
	return IDB.listener(this, '_handleOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype.createOutListener = function() {
	return IDB.listener(this, '_handleOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype._handleClick = function() {
	this._graphContext.onDatapointClicked(this._node.datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype._handleOver = function() {
	this._graphContext.onDatapointOver(this._node.datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBox.prototype._handleOut = function() {
	this._graphContext.onDatapointOut(this._node.datapoint);
};

/*====================================================================================================*/
IDB.TreemapBoxView = function(initialModel) {
	this._model = initialModel; //used to obtain settings values
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBoxView.prototype._buildKinetic = function() {
	var model = this._model;
	var labels = [];
	var nameLO = model.getNameLabelOptions();
	var valueLO = model.getValueLabelOptions();
	var groupLO = model.getGroupLabelOptions();
	var percentLO = model.getPercentLabelOptions();

	this._display = new Kinetic.Group();
	this._touchHandler = new IDB.TouchHandler(this._display);
	
	this._bg = new Kinetic.Rect({
		fill: 'rgba(0, 0, 0, 0)'
	});
	this._display.add(this._bg);

	this._hold = new Kinetic.Group();
	this._hold.listening(false);
	this._display.add(this._hold);
	
	this._mask = new Kinetic.Rect();
	IDB.KineticUtil.setMask(this._hold, this._mask);
	this._hold.add(this._mask);

	this._box = new Kinetic.Rect();
	this._hold.add(this._box);
	
	if ( model.hasShading() ) {
		var isSquare = true;
		var shadeType;

		this._shadingScale = 1;

		switch ( model.getShading() ) {
			case IDB.TreemapBox.ShadingCushion:
				shadeType = IDB.GenericShading.TypeMatte;
				isSquare = false;
				this._shadingScale = 2;
				break;

			case IDB.TreemapBox.ShadingMatte:
				shadeType = IDB.GenericShading.TypeMatte;
				break;

			case IDB.TreemapBox.ShadingGloss:
				shadeType = IDB.GenericShading.TypeGloss;
				break;

			case IDB.TreemapBox.ShadingChrome:
				shadeType = IDB.GenericShading.TypeChrome;
				break;
		}

		this._shading = new Kinetic.Group();
		this._hold.add(this._shading);
		
		var gs = new IDB.GenericShading(shadeType, isSquare);
		var gsDisp = gs.getDisplay();
		this._shading.add(gsDisp);

		if ( isSquare ) {
			gsDisp.rotation(-90);
		}
		else {
			gsDisp.x(600);
			gsDisp.y(-500);
		}
	}
	
	this._labelHold = new Kinetic.Group();
	this._labelHold.listening(false);
	this._display.add(this._labelHold);

	if ( nameLO.show ) {
		nameLO.align = 'center';
		this._nameLabel = new IDB.GraphLabel(nameLO);
		this._labelHold.add(this._nameLabel.getDisplay());
		labels.push(this._nameLabel);
	
	}
	if ( valueLO.show ) {
		valueLO.align = 'center';
		this._valueLabel = new IDB.GraphLabel(valueLO);
		this._labelHold.add(this._valueLabel.getDisplay());
		labels.push(this._valueLabel);
	}

	if ( groupLO.show && model.hasGroupAxis() ) {
		groupLO.align = 'center';
		this._groupLabel = new IDB.GraphLabel(groupLO);
		this._labelHold.add(this._groupLabel.getDisplay());
		labels.push(this._groupLabel);
	}

	if ( percentLO.show ) {
		percentLO.align = 'center';
		this._percentLabel = new IDB.GraphLabel(percentLO);
		this._labelHold.add(this._percentLabel.getDisplay());
		labels.push(this._percentLabel);
	}

	this._labels = labels;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBoxView.prototype.init = function(model) {
	var nameLabel = this._nameLabel;
	var valueLabel = this._valueLabel;
	var groupLabel = this._groupLabel;
	var percentLabel = this._percentLabel;

	this._model = model;

	model.setOnNodeChanged(IDB.listener(this, '_onNodeChanged'));
	model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
	
	this._display.off('click');
	this._display.off('mouseover');
	this._display.off('mouseout');

	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());

	if ( nameLabel ) {
		nameLabel.text = model.getNameLabelText();
	}

	if ( valueLabel ) {
		valueLabel.text = model.getValueLabelText();
	}

	if ( groupLabel ) {
		groupLabel.text = model.getGroupLabelText();
	}

	if ( percentLabel ) {
		percentLabel.text = model.getPercentLabelText();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBoxView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBoxView.prototype.updateAfterResize = function() {
	var model = this._model;
	var shading = this._shading;
	var shadingScale = this._shadingScale;
	var labels = this._labels;
	var labelHold = this._labelHold;
	var node = model.getNode();
	var r = node.rect;
	var minSide = Math.min(r.width, r.height);
	var pad = Math.min(model.getPadding()/2, minSide*0.25);
	var cornerRad = Math.min(model.getCornerRadius()*2, minSide*0.5);
	var valueDp = node.datapoint;
	var rangeDp = node.rangeDatapoint;
	var alpha = 1;
	var lblSumH = 0;
	var i, color, nextLblSumH, lbl, lblEnd;
	
	var fillRect = {
		x: -r.width/2+pad,
		y: -r.height/2+pad,
		width: r.width-pad*2,
		height: r.height-pad*2
	};

	if ( valueDp.map.groupColor != null ) {
		color = valueDp.map.groupColor;

		if ( valueDp.map.rangeAlpha != null ) {
			alpha = valueDp.map.rangeAlpha*0.9 + 0.1; //ensure at least 10% alpha
		}
	}
	else if ( rangeDp ) {
		color = rangeDp.rangeColor;
	}
	else if ( model.useXColor() ) {
		color = model.getXColor();
	}
	else {
		color = model.getDefaultColor();
	}

	this._bg
		.x(-r.width/2)
		.y(-r.height/2)
		.width(r.width)
		.height(r.height);

	this._box
		.fill(IDB.toRGBA(color, alpha))
		.x(-r.width/2)
		.y(-r.height/2)
		.width(r.width)
		.height(r.height);

	this._mask
		.x(-fillRect.width/2)
		.y(-fillRect.height/2)
		.width(fillRect.width)
		.height(fillRect.height)
		.cornerRadius(cornerRad/2);

	if ( shading ) {
		shading
			.x(fillRect.x*shadingScale)
			.y(-fillRect.y*shadingScale)
			.scaleX(fillRect.width/IDB.GenericShading.FullDimension*shadingScale)
			.scaleY(fillRect.height/IDB.GenericShading.FullDimension*shadingScale);
	}

	for ( i = 0 ; i < labels.length ; ++i ) {
		lbl = labels[i];
		lbl.width = fillRect.width;
		lbl.truncate();
		lbl.y = lblSumH;

		nextLblSumH = lblSumH+lbl.height;
		lbl.visible = (!lblEnd && nextLblSumH < fillRect.height && lbl.text != '...');

		if ( lbl.visible ) {
			lblSumH = nextLblSumH;
		}
		else {
			lblEnd = true;
		}
	}

	labelHold.x(-fillRect.width/2);
	labelHold.y(-lblSumH/2);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBoxView.prototype._onNodeChanged = function() {
	this._onDatapointsUpdated();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapBoxView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getNode().datapoint.getVisualState();
	var isFaded = (state == IDB.VS.FADED);

	this._box.opacity(isFaded ? 0.2 : 1.0);
	this._labelHold.visible(!isFaded);

	if ( this._shading ) {
		this._shading.opacity(this._box.opacity());
	}
};

/*====================================================================================================*/
IDB.TreemapGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);

	var sett = this._graphContext.settings;
	var legend = IDB.LS.NONE;

	if ( IDB.TreemapGraph.hasGroupAxis(sett) ) {
		legend = IDB.LS.NONE;
	}
	else if ( IDB.TreemapGraph.hasRangeAxis(sett) ) {
		legend = IDB.LS.RANGES;
	}
	else if ( sett.getBool('TreemapDefaultBoxUseXColor') ) {
		legend = IDB.LS.X_VALUES;
	}

	if ( legend == IDB.LS.RANGES ) {
		var axes = this._dataSet.getGraphAxes(true);
		var rangeAxisName = sett.get('TreemapRangeAxis');

		for ( var i = 0 ; i < axes.length ; i++ ) {
			if ( rangeAxisName == axes[i].axisName ) {
				this._legendAxes = [axes[0], axes[i]];
				break;
			}
		}
	}
	
	this._info.legendStyle = legend;
	this._info.calloutStyle = 'rangeColors';

	this._graphContext.settings.set('chart', chart);
};

IDB.TreemapGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.TreemapGraph.prototype.constructor = IDB.TreemapGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapGraph.prototype._build = function() {
	var gc = this._graphContext;

	this._scroll = new IDB.TreemapScroll(gc);
	this._scrollView = new IDB.TreemapScrollView(this._scroll);
	this._stageLayer.add(this._scrollView.getDisplay());

	var datapoints = this._scroll.getPlot().getDatapoints();

	this._widthMult = Math.max(1, datapoints.length/gc.settings.get('TreemapMaxRowsDisplayed'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapGraph.prototype._update = function(width, height) {
	var sv = this._scrollView;
	sv.setFullSize(width*this._widthMult, height);
	sv.setViewWindow(width);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapGraph.prototype.getLegendAxes = function() {
	if ( this._legendAxes ) {
		return this._legendAxes;
	}
	
	return IDB.GraphBase.prototype.getLegendAxes.call(this);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapGraph.NO_AXIS = '#$NoAxis$#';

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapGraph.hasGroupAxis = function(settings) {
	var name = settings.get('TreemapGroupAxis');
	return (name && name != IDB.TreemapGraph.NO_AXIS);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapGraph.hasRangeAxis = function(settings) {
	var name = settings.get('TreemapRangeAxis');
	return (name && name != IDB.TreemapGraph.NO_AXIS);
};

/*====================================================================================================*/
IDB.TreemapNode = function(name, isLeaf, datapoint, rangeDatapoint) {
	this._name = name;
	this._isLeaf = isLeaf;
	this._datapoint = datapoint;
	this._rangeDatapoint = rangeDatapoint;

	this._children = (this._isLeaf ? null : []);
	this._childMap = {};
	this._rect = { x: 0, y: 0, width: 0, height: 0 };
	this._groupingAxisIndexes = [];
	this._dataRowIndexes = [];
	this._level = 0;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapNode.prototype.buildChildren = function(dataGrid, defaultValueAxisI, rangeAxisI) {
	if ( this._isLeaf ) {
		return;
	}

	var groupingAxisIndexes = this._groupingAxisIndexes;
	var dataRowIndexes = this._dataRowIndexes;
	var level = this._level;
	var children = this._children;
	var isGrouping = (groupingAxisIndexes.length > 0);
	var valueAxisI = (isGrouping ? groupingAxisIndexes[0] : defaultValueAxisI);
	var childGroups = (isGrouping ? groupingAxisIndexes.slice(1) : []);
	var node, i, rowI, valueDp, rangeDp, name;

	for ( i = 0 ; i < dataRowIndexes.length ; ++i ) {
		rowI = dataRowIndexes[i];
		valueDp = dataGrid.getDatapointAt(rowI, valueAxisI, false);

		if ( !isGrouping ) {
			if ( valueDp.yValue.isNothing ) {
				continue;
			}

			if ( valueDp.yValue.numberValue <= 0 ) {
				this._containedNegativeValue = true;
				continue;
			}

			if ( valueDp.yValue.numberValue == 0 ) {
				continue;
			}

			rangeDp = (rangeAxisI == -1 ? null : 
				dataGrid.getDatapointAt(rowI, rangeAxisI, false));

			node = new IDB.TreemapNode(valueDp.xValue.stringValue, true, valueDp, rangeDp);
			node.level = level+1;
			this.addChild(node);
			continue;
		}

		name = valueDp.yValue.stringValue;
		node = this.getChildWithName(name);

		if ( !node ) {
			node = new IDB.TreemapNode(name, false, valueDp);
			node.groupingAxisIndexes = childGroups;
			node.level = level+1;
			this.addChild(node);
		}

		node.dataRowIndexes.push(rowI);
	}

	for ( i = 0 ; i < children.length ; ++i ) {
		children[i].buildChildren(dataGrid, defaultValueAxisI, rangeAxisI);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
Object.defineProperties(IDB.TreemapNode.prototype, {
	name: {
		get: function() {
			return this._name;
		}
	},

	isLeaf: {
		get: function() {
			return this._isLeaf;
		}
	},

	datapoint: {
		get: function() {
			return this._datapoint;
		}
	},

	rangeDatapoint: {
		get: function() {
			return this._rangeDatapoint;
		}
	},

	value: {
		get: function() {
			return this._datapoint.yValue.numberValue;
		}
	},

	children: {
		get: function() {
			return this._children;
		}
	},

	rect: {
		get: function() {
			return this._rect;
		},
		set: function(rect) {
			this._rect = rect;
		}
	},

	groupingAxisIndexes: {
		get: function() {
			return this._groupingAxisIndexes;
		},
		set: function(indexes) {
			this._groupingAxisIndexes = indexes;
		}
	},
	
	dataRowIndexes: {
		get: function() {
			return this._dataRowIndexes;
		}
	},

	level: {
		get: function() {
			return this._level;
		},
		set: function(level) {
			this._level = level;
		}
	},
	
	containedNegativeValue: {
		get: function() {
			return this._containedNegativeValue;
		}
	},

	valueSum: {
		get: function() {
			if ( !this._isFinal ) {
				throw 'Not finalized yet.';
			}

			return this._valueSum;
		}
	},

	valuePercent: {
		get: function() {
			if ( !this._isFinal ) {
				throw 'Not finalized yet.';
			}

			return this._valuePercent;
		}
	}
});


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapNode.prototype.addChild = function(node) {
	if ( this._isLeaf ) {
		throw 'Cannot add a child to a leaf node.';
	}

	if ( this._isFinal ) {
		throw 'Cannot add a child to a finalized node.';
	}

	this._children.push(node);
	this._childMap[node.name] = node;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapNode.prototype.getChildWithName = function(name) {
	return this._childMap[name];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapNode.prototype.finalizeAllChildren = function() {
	if ( this._isFinal ) {
		throw 'Already finalized.';
	}
	
	this._isFinal = true;
	
	if ( this._isLeaf ) {
		this._valueSum = this.value;
		//console.log(this+" / %o", this);
		return;
	}

	var children = this._children;
	var sum = 0;

	for ( var i = 0 ; i < children.length ; ++i ) {
		var child = children[i];
		child.finalizeAllChildren();

		sum += (child.isLeaf ? child.value : child.valueSum);

		if ( child.containedNegativeValue ) {
			this._containedNegativeValue = true;
		}
	}

	this._valueSum = sum;
	//console.log(this+" / %o", this);
	this.setPercentForAllChildren(sum);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapNode.prototype.setPercentForAllChildren = function(totalValueSum) {
	if ( !this._isFinal ) {
		throw 'Not finalized yet.';
	}

	if ( this._isLeaf ) {
		this._valuePercent = this.value/totalValueSum;
		return;
	}

	var children = this._children;

	for ( var i = 0 ; i < children.length ; ++i ) {
		children[i].setPercentForAllChildren(totalValueSum);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapNode.prototype.getChildrenAtLevel = function(level) {
	var children = this._children;
	var result = [];
	var childChildren;

	if ( !level ) {
		return children;
	}

	for ( var i = 0 ; i < children.length ; ++i ) {
		childChildren = children[i].getChildrenAtLevel(level-1);

		for ( var j = 0 ; j < childChildren.length ; ++j ) {
			result.push(childChildren[j]);
		}
	}

	return result;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapNode.prototype.toString = function() {
	var level = this._level;
	var tabs = '';
			
	for ( var lev = 0 ; lev < level ; ++lev ) {
		tabs += '    ';
	}
			
	return tabs+this._name+': '+this._valueSum+(this._isLeaf ? ' (leaf)' : '');
};

/*====================================================================================================*/
IDB.TreemapPlot = function(graphContext) {
	this._graphContext = graphContext;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlot.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var yAxesCount = dpGrid.getNumYCols(false);
	var xAxesCount = dpGrid.getNumXRows();
	var valueAxisIndex = 0;
	var rangeAxisIndex = -1;
	var rootNode = new IDB.TreemapNode('Root');
	var datapoints = [];
	var basicColorLen = IDB.ColorSet.BASIC_COLORS.length;
	var i, j, rowI, dp, rangeDp, rangeStats, rangeDist, childNode, groupColor;

	for ( i = 0 ; i < yAxesCount ; ++i ) {
		var yAxis = dpGrid.getYAxis(i, false);

		if ( yAxis.axisName == sett.get('TreemapValueAxis') ) {
			valueAxisIndex = i;
		}

		if ( yAxis.axisName == sett.get('TreemapRangeAxis') ) {
			rangeAxisIndex = i;
			rangeStats = dpGrid.getStatSet(yAxis.axisId);
			rangeDist = (rangeStats ? rangeStats.max-rangeStats.min : 0);
		}

		if ( yAxis.axisName == sett.get('TreemapGroupAxis') ) {
			//The "groupingAxisIndexes" array allows for multiple, nested groupings.
			//But, this currently just uses one axis, defined by the "groupAxisName".
			rootNode.groupingAxisIndexes.push(i);
		}
	}

	for ( i = 0 ; i < xAxesCount ; ++i ) {
		rootNode.dataRowIndexes.push(i);
	}

	rootNode.buildChildren(dpGrid, valueAxisIndex, rangeAxisIndex);
	rootNode.finalizeAllChildren();

	if ( rootNode.containedNegativeValue ) {
		gc.showWarning(IDB.GraphLang.get('TreemapWarningTitle'),
			IDB.GraphLang.get('TreemapNegativeWarning'));
	}

	// Set the group colors

	for ( i = 0 ; i < rootNode.children.length ; ++i ) {
		childNode = rootNode.children[i];
		groupColor = IDB.ColorSet.BASIC_COLORS[i%basicColorLen];

		for ( j = 0 ; j < childNode.dataRowIndexes.length ; ++j ) {
			rowI = childNode.dataRowIndexes[j];
			dp = dpGrid.getDatapointAt(rowI, valueAxisIndex, false);
			dp.map.groupColor = groupColor;

			if ( rangeStats ) {
				rangeDp = dpGrid.getDatapointAt(rowI, rangeAxisIndex, false);
				dp.map.rangeAlpha = (rangeDist == 0 ? 1 : 
					(rangeDp.yValue.numberValue-rangeStats.min)/rangeDist);
			}
		} 
	}

	// Capture all of the datapoints

	for ( i = 0 ; i < rootNode.dataRowIndexes.length ; ++i ) {
		rowI = rootNode.dataRowIndexes[i];
		dp = dpGrid.getDatapointAt(rowI, valueAxisIndex, false);
		datapoints.push(dp);
	}

	this._rootNode = rootNode;
	this._datapoints = datapoints;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlot.prototype.getRootNode = function() {
	return this._rootNode;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlot.prototype.getDatapoints = function() {
	return this._datapoints;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlot.prototype.getNewBox = function() {
	return new IDB.TreemapBox(this._graphContext);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlot.prototype.updateNodeSizes = function(fullW, fullH) {
	var sett = this._graphContext.settings;
	var rootNode = this._rootNode;
	var rootChildren = rootNode.children;

	rootNode.rect = {
		x: 0,
		y: 0,
		width: fullW,
		height: fullH
	};

	if ( sett.getBool('TreemapSquarifyFirst') ) {
		IDB.TreemapAlgorithm.squarify(rootChildren, rootNode.rect);
	}
	else {
		IDB.TreemapAlgorithm.sliceAndDice(rootChildren, rootNode.rect);
	}

	this._performNestedAlgorithm(rootChildren, sett.getBool('TreemapSquarifySecond'));
}

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlot.prototype._performNestedAlgorithm = function(nodes, useSquarify) {
	var nodesLen = nodes.length;
	var i, node;
	
	for ( i = 0 ; i < nodesLen ; ++i ) {
		node = nodes[i];

		if ( node.isLeaf ) {
			continue;
		}

		if ( useSquarify ) {
			IDB.TreemapAlgorithm.squarify(node.children, node.rect);
		}
		else {
			IDB.TreemapAlgorithm.sliceAndDice(node.children, node.rect);
		}

		this._performNestedAlgorithm(node.children, useSquarify);
	}
};

/*====================================================================================================*/
IDB.TreemapPlotView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlotView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();

	this._boxHold = new Kinetic.Group();
	this._display.add(this._boxHold);
			
	this._boxCache = [];
	this._boxMap = {};
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlotView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlotView.prototype.getFirstHighlightedDatapointForScrolling = function() {
	var leafNodes = this._leafNodes;
	var viewX = this._viewX;
	var viewW = this._viewW;
	var datapoints = this._model.getDatapoints();
	var highlights = [];
	var firstNode = null;
	var i, j, dp, node, nodeIsMatch, r;

	for ( i = 0 ; i < datapoints.length ; ++i ) {
		dp = datapoints[i];

		if ( dp.getVisualState() == IDB.VS.HIGHLIGHTED ) {
			highlights.push(dp);
		}
	}

	if ( highlights.length == 0 ) {
		return null;
	}

	for ( i = 0 ; i < leafNodes.length ; ++i ) {
		node = leafNodes[i];
		nodeIsMatch = false;

		for ( j = 0 ; j < highlights.length ; ++j ) {
			if ( highlights[j] == node.datapoint ) {
				nodeIsMatch = true;
				break;
			}
		}

		if ( !nodeIsMatch ) {
			continue; //node is not a match; ignore it
		}

		r = node.rect;

		if ( r.x >= viewX && r.x+r.width <= viewX+viewW ) {
			return null; //node is completely within bounds; no scrolling required
		}
		else if ( !firstNode ) {
			firstNode = node; //store first match in case no nodes are within bounds
		}
	}

	return firstNode;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlotView.prototype.setFullSize = function(fullW, fullH) {
	this._model.updateNodeSizes(fullW, fullH);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlotView.prototype.setViewWindow = function(viewX, viewW) {
	this._viewX = viewX;
	this._viewW = viewW;

	this._leafNodes = [];
	this._visibleNodes = [];
	this._visibleNodeParents = [];

	this._findVisibleNodes(this._model.getRootNode(), []);
	this._updateBoxes();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlotView.prototype._findVisibleNodes = function(node, parentNodes) {
	var viewX = this._viewX;
	var viewW = this._viewW;
	var i, r;

	if ( !node.isLeaf ) {
		parentNodes = parentNodes.concat(node);

		for ( i = 0 ; i < node.children.length ; ++i ) {
			this._findVisibleNodes(node.children[i], parentNodes);
		}

		return;
	}

	this._leafNodes.push(node);
	r = node.rect;

	if ( r.x >= viewX+viewW || r.x+r.width <= viewX ) {
		this._boxMap[node.datapoint.datapointRow.rowId] = null;
		return;
	}

	this._visibleNodes.push(node);
	this._visibleNodeParents.push(parentNodes);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlotView.prototype._updateBoxes = function() {
	var model = this._model;
	var boxCache = this._boxCache;
	var visibleNodes = this._visibleNodes;
	var boxMap = this._boxMap;
	var boxHold = this._boxHold;
	var missedIndexes = [];
	var lastCacheI = 0;
	var i, boxPair, boxModel, boxView, node, missI, cacheI, useCachedBox;

	// Hide all cached boxes

	for ( i = 0 ; i < boxCache.length ; ++i ) {
		boxPair = boxCache[i];
		boxPair.view.getDisplay().visible(false);
	}

	// Update any boxes that are already mapped to visible nodes
	// Record which nodes did not have a match in the map 

	for ( i = 0 ; i < visibleNodes.length ; ++i ) {
		node = visibleNodes[i];
		boxPair = boxMap[node.datapoint.datapointRow.rowId];

		if ( !boxPair ) {
			missedIndexes.push(i);
			continue;
		}
		
		//console.log(" * mapped: "+node.name+" / "+i+" / "+node.datapoint.datapointRow.rowId);
		this._updateBox(boxPair, i);
	}

	//console.log("TreemapPlot.updateBoxes(): viz="+visibleNodes.length+" / miss="+
	//	missedIndexes.length+" / total="+boxCache.length);

	// Use any remaining (hidden) boxes in the cache
	// Create new boxes if the cache is not large enough

	for ( missI = 0 ; missI < missedIndexes.length ; ++missI ) {
		i = missedIndexes[missI];
		boxPair = null;
		useCachedBox = false;

		for ( cacheI = lastCacheI+1 ; cacheI < boxCache.length ; ++cacheI ) {
			boxPair = boxCache[cacheI];

			if ( !boxPair.view.getDisplay().visible() ) {
				useCachedBox = true;
				break;
			}
		}

		lastCacheI = cacheI;

		if ( !useCachedBox ) {
			//console.log(" * new: "+node.name+" / "+i);
			boxModel = model.getNewBox();

			boxView = new IDB.TreemapBoxView(boxModel);
			boxHold.add(boxView.getDisplay());
			
			boxPair = {
				model: boxModel,
				view: boxView
			};

			boxCache.push(boxPair);
		}
		/*else {
			console.log(" * from cache: "+node.name+" / "+i);
		}*/

		this._updateBox(boxPair, i);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapPlotView.prototype._updateBox = function(boxPair, index) {
	var boxM = boxPair.model;
	var boxV = boxPair.view;
	var boxVDisp = boxV.getDisplay();
	var node = this._visibleNodes[index];
	var r = node.rect;
	
	boxVDisp.visible(true);
	boxVDisp.x(r.x+r.width/2);
	boxVDisp.y(r.y+r.height/2);

	boxM.init(node, this._visibleNodeParents[index]);
	boxV.init(boxM);
	boxV.updateAfterResize();

	this._boxMap[node.datapoint.datapointRow.rowId] = boxPair;
};

/*====================================================================================================*/
IDB.TreemapScroll = function(graphContext) {
	this._graphContext = graphContext;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScroll.prototype._build = function() {
	this._plot = new IDB.TreemapPlot(this._graphContext);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScroll.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScroll.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScroll.prototype.getPlot = function() {
	return this._plot;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScroll.prototype.isOutgoingDatapointEvent = function() {
	return this._graphContext.isOutgoingDatapointEvent;
};

/*====================================================================================================*/
IDB.TreemapScrollView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScrollView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	this._plot = new IDB.TreemapPlotView(model.getPlot());
	this._hold.add(this._plot.getDisplay());

	this._horiz = new IDB.GraphScroll(true);
	this._horiz.onVisualUpdate(IDB.listener(model, 'refreshDisplay'));
	this._horiz.onScroll(IDB.listener(this, '_onScroll'));
	this._horiz.lineScrollSize = 16;
	this._horiz.getDisplay().rotation(-90);
	this._display.add(this._horiz.getDisplay());

	//LATER: KineticJS doesn't support mouse-wheel scrolling
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScrollView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScrollView.prototype.setFullSize = function(width, height) {
	this._fullW = width;
	this._fullH = height;
}

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScrollView.prototype.setViewWindow = function(width) {
	var plot = this._plot;
	var horiz = this._horiz;
	var horizD = horiz.getDisplay();
	var fullW = this._fullW;
	var fullH = this._fullH;
	var scrollbarH = 16;
	var viewW = width;

	this._viewW = viewW;
			
	horiz.setSize(scrollbarH, viewW);
	horizD.y(fullH);
	horizD.visible(fullW > viewW);
			
	if ( horizD.visible() ) {
		horiz.pageSize = viewW;
		horiz.maxScrollPos = fullW-viewW;
		horiz.scrollPos = -plot.getDisplay().x();
		horiz.refresh();
				
		plot.setFullSize(fullW, fullH-scrollbarH-2);
		this._onScroll();
	}
	else {
		plot.setFullSize(fullW, fullH);
		plot.setViewWindow(0, viewW);
	}
			
	this._hold.clip({
		x: 0,
		y: 0,
		width: viewW,
		height: fullH
	});
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScrollView.prototype._onScroll = function() {
	var plot = this._plot;
	var horiz = this._horiz;
	var viewW = this._viewW;

	plot.getDisplay().x(Math.round(-horiz.scrollPos));
	plot.setViewWindow(-plot.getDisplay().x(), viewW);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TreemapScrollView.prototype._onDatapointsUpdated = function() {
	if ( this._model.isOutgoingDatapointEvent() ) {
		return;
	}
	
	var horiz = this._horiz;
	var node = this._plot.getFirstHighlightedDatapointForScrolling();
	var rect, minPos, maxPos, pos;

	if ( !node ) {
		return;
	}

	rect = node.rect;
	minPos = rect.x-this._viewW+rect.width;
	maxPos = rect.x;
	pos = (horiz.scrollPos < minPos ? minPos : maxPos);

	horiz.scrollPos = Math.max(0, Math.min(horiz.maxScrollPos, pos));
	horiz.refresh();

	this._onScroll();
};

