function loadDashframeFromConfig(config, dashboardContext) {
    if(!config) {
       return;
    }

    var frameId = config.frameId;
    var jDashframe = $("[data-frame-id='" + frameId + "']");
    jDashframe.data("dashframeConfig", config);

    var dashframeType = config.dashframeType;

    switch(dashframeType) {
       case 'chart':
             loadChart(jDashframe, dashboardContext);
             break;
       case 'image': 
            loadImage(jDashframe);
            break;
       case 'dashPanel':
            loadDashPanel(jDashframe, dashboardContext);
           break; 
    }
}


function loadDashframes(dashframeConfigs, dashboardContext) {
    for(var i=0; i < dashframeConfigs.length; i++) {
        loadDashframeFromConfig(dashframeConfigs[i], dashboardContext);
    }
}

function sizeAndPositionImage(jImg) {
    var jDashframe = jImg.parent();
    var dashframeConfig = jDashframe.data('dashframeConfig');

    //
    // Size the image.
    //
    if(dashframeConfig.imageSizing == "center") {
        jImg.css("max-width", jDashframe.width());
        jImg.css("max-height", jDashframe.height());
    }
    else {
        var dimensions = getImageSize(jImg.height(), jImg.width(), jDashframe.height(), jDashframe.width(), dashframeConfig.imageAspect);
 
        jImg.width(dimensions.width);   
        jImg.height(dimensions.height);   
     }

     //
     // Position the image.
     //
     var position = getImagePosition(jImg.height(), jImg.width(), jDashframe.height(), jDashframe.width());

     jImg.css({
         'position':'relative',
         'left':(position.left)+'px',
         'top':(position.top)+'px'
     });
}

function loadImage(jDashframe) {
    var dashframeConfig = jDashframe.data('dashframeConfig');

    var url = dashframeConfig.imageUrl;
    jDashframe.append("<img src='" + url + "' class='frame-content'>"); 
    var jImg = $('img',jDashframe);
    jImg.data('resizeCallback', sizeAndPositionImage); 

    var jLayoutParent = jDashframe.parent();
    jLayoutParent.css("overflow","hidden");

    jImg.on('load', function() {
                       sizeAndPositionImage($(this));
                    });

     //
     // Set the background color.
     //
     var backgroundColor = dashframeConfig.rgbaBgColor;
     if(!backgroundColor) {
         backgroundColor = "#FFFFFF";
     }

     jDashframe.css({
         'background-color': backgroundColor
     });

     //
     // Set the tooltip.
     //
     var tooltip = $.trim(dashframeConfig.imageToolTip);
     if(tooltip) {
         jImg.attr("title", tooltip);
     }

     //
     // Set the link.
     //
     var linkUrl = dashframeConfig.linkUrl;
     if(linkUrl) {
         var linkTarget = $.trim(dashframeConfig.linkTarget);
         if(!linkTarget) {
             linkTarget = "_blank";
         }
         jImg.click(function() {
             window.open(linkUrl, linkTarget, dashframeConfig.linkUrlSettings); 
         });

         jImg.hover(function() {$(this).css('cursor','pointer');},
                  function() {$(this).css('cursor','auto');});
     }
}

function getImageSize(imgHeight, imgWidth, containerHeight, containerWidth, preserveAspectRatio) {
    var newWidth = containerWidth;
    var newHeight = containerHeight;

    if(preserveAspectRatio) { 
        var wScale = containerWidth/imgWidth;
        var hScale = containerHeight/imgHeight;

        var scale = (wScale > hScale)?hScale:wScale;
        
        newWidth = scale*imgWidth;
        newHeight = scale*imgHeight;
    }

    return {width:newWidth, height:newHeight};
}

function getImagePosition(imgHeight, imgWidth, containerHeight, containerWidth) {

    var left = -(imgWidth - containerWidth)/2;
    var top = -(imgHeight - containerHeight)/2;

    return {top:top, left:left};
}

function toRGBA(color, alpha) {
    var rgb = getRGB(color);
    return "rgba(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + "," + alpha + ")";
}
    
function toRGB(color) {
    var rgb = getRGB(color);
    return "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";       
}   
    
function getRGB(color) {
    var rgb = [];
    rgb[0] = (color & 0xFF0000) >> 16;
    rgb[1] = (color & 0x00FF00) >> 8;
    rgb[2] = color & 0x0000FF;
    return rgb;
}

function resizeContent(paneName, pane) {
    $('.frame-content', pane).each(function() {
        var resizeCallback = $(this).data('resizeCallback');
        if(resizeCallback) {
            resizeCallback($(this));
        }
    });
}

function loadChart(jDashframe, dashboardContext) {
    var dashframeConfig = jDashframe.data('dashframeConfig');

    var chartId = dashframeConfig.chartId;
    jDashframe.append("<div data-chart-id='" + chartId + "' class='frame-content'/>"); 
    var url = dashboardContext.serverCmdUrl + "loadChart&chartID=" + chartId;
     $.ajax({
        url:url,
        dataType:"xml",
        type:"GET",
        context:jDashframe,
        complete:onCompleteLoadChart
     });
}

  function onSuccessLoadChart(jDashframe, responseText) {
       var jqXMLDoc = $.parseXML(responseText);
       var json = $.parseJSON($('json', jqXMLDoc).text()).datapacket;
       var chartId = json.chart.chartId;
       var chartDiv = $("[data-chart-id='" + chartId + "']", jDashframe);
       chartDiv.html(json.expandedChartTitle);
       chartDiv.css({"text-align":"center", "font-size":"20px", "font-weight":"bold", "padding-top":"10px"});
  }

  function onErrorLoadChart(jqXHR, textStatus, errorThrown) {
      alert("ERROR:" + textStatus);
  }


  function onCompleteLoadChart(jqXHR, textStatus) {
      switch(textStatus) {

      case "success" :
          var data = jqXHR.responseText;
          onSuccessLoadChart(this, data);
          break;
      
      case "error":
         // call onError
         break;
      }
 
  }


  function loadDashPanel(jDashframe, dashboardContext) {
    var dashframeConfig = jDashframe.data('dashframeConfig');
    jDashframe.append("<div class='frame-content'/>"); 
    var panelCode = dashframeConfig.panelCode;

    switch(panelCode) {
        case "TEXT":
             createTextPanel(jDashframe);
             break;
        default :
             break;
      }
  }

  function applyBaseDashPanelSettings(jDashframe) {
    var dashframeConfig = jDashframe.data('dashframeConfig');
    var jPanel = $('.frame-content', jDashframe);
    var styles = {"background-color":dashframeConfig.backgroundColor,
                  "color":dashframeConfig.textColor,
                  "font-size":dashframeConfig.textSize,
                  "height":"100%",
                  "width":"100%"};
    jPanel.css(styles);

  }

  function createTextPanel(jDashframe) {
    applyBaseDashPanelSettings(jDashframe);
    var dashframeConfig = jDashframe.data('dashframeConfig');
  }

