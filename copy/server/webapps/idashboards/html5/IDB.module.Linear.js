'use strict';
IDB.LinearBar = function(graphContext, yAxisCol, yTargetCol) {
	this._graphContext = graphContext;
	this._yAxisCol = yAxisCol;
	this._yTargetCol = yTargetCol;
	
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var yAxis = dpGrid.getYAxis(this._yAxisCol, true);
	var rlist = dpGrid.getRangeList(this._yAxisCol, true);
	
	this._targetMode = sett.getBool('LinearTargetMode');
	this._targetFixed = sett.getBool('LinearTargetFixed');

	this._currValue = null;
	this._targetValue = null;
	this._reset = false;
	this._datapoint = null;
	this._xRowIndex = 0;
	
	this._calcMinMaxVals();
	
	var lblLoc = sett.get('LinearAxisLabelLocation');

	if ( sett.get('LinearAxisLabelOptions').show &&
			(lblLoc == IDB.LinearBarLabel.AxisPara || lblLoc == IDB.LinearBarLabel.AxisPerp) ) {
		this._labels = new IDB.LinearBarLabel(sett, yAxis);
	}
	
	if ( sett.getBool('LinearIndiDotShow') ) {
		this._dot = new IDB.LinearBarDot(sett, rlist);
	}
	
	if ( sett.getBool('LinearRangeShow') ) {
		this._range = new IDB.LinearBarRange(sett, this._minVal, this._maxVal, rlist);
	}

	if ( sett.getBool('LinearTickShow') ) {
		this._ticks = new IDB.LinearBarTicks(sett, this._minVal, this._maxVal,
			this._divMetrics, yAxis.numFmt);
	}
	
	this._indi = new IDB.LinearBarIndi(sett, this._minVal, this._maxVal, yAxis, rlist);
	this._body = new IDB.LinearBarBody(sett, yAxis, this._indi.getBulbColor());
	
	if ( sett.getBool('LinearTargetMode') ) {
		var targetAxis = dpGrid.getYAxis(this._yTargetCol, true);
		this._target = new IDB.LinearBarTarget(sett, this._minVal, this._maxVal, targetAxis);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype._calcMinMaxVals = function() {
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var rangeList = dpGrid.getRangeList(this._yAxisCol, true);
	
	var min = rangeList[0].val;
	var max = rangeList[rangeList.length-1].val;
	max += (max-min)/10000; //ensure that the final range color is shown

	if ( this._targetMode && !this._targetFixed ) {
		min = Math.min(min, 99.9);
		max = Math.max(max, 100.1);
		
		var rowCount = dpGrid.getNumXRows();
		var i, dp, tdp, val;
		
		for ( i = 0 ; i < rowCount ; ++i ) {
			dp  = dpGrid.getDatapointAt(i, this._yAxisCol, true);
			tdp = dpGrid.getDatapointAt(i, this._yTargetCol, true);
			
			if ( tdp.yValue.numberValue ) {
				val = dp.yValue.numberValue/tdp.yValue.numberValue*100;
				min = Math.min(min, val);
				max = Math.max(max, val);
			}
		}
	}
	else {
		var ax = dpGrid.getYAxis(this._yAxisCol, true);
		var ss = dpGrid.getStatSet(ax.axisId);
		min = Math.min(min, ss.min);
		max = Math.max(max, ss.max);

		if ( this._targetFixed ) {
			var targAxis = dpGrid.getYAxis(this._yTargetCol, true);
			var targStat = dpGrid.getStatSet(targAxis.axisId);
			
			if ( targStat.min < min ) {
				min = targStat.min-0.01;
			}

			if ( targStat.max > max ) {
				max = targStat.max+0.01;
			}
		}
	}
	
	if ( min == max ) {
		if ( min > 0 ) {
			min = 0;
		}
		else if ( max < 0 ) {
			max = 0;
		}
		else {
			max = 10;
		}
	}
	
	var numDivs = gc.settings.get('LinearTickNumMajors');
	this._divMetrics = IDB.Calculator.getDivisionMetrics(min, max, numDivs);
	var dm = this._divMetrics.largeDivisions;
	this._minVal = dm.getMinBoundary();
	this._maxVal = dm.getMaxBoundary();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.resetBar = function() {
	this._currValue = {
		asNumber: this._minVal,
		asText: ''
	};

	this._targetValue = {
		asNumber: 0,
		asText: ''
	};

	this._reset = true;
	this._datapoint = null;
	this._updateValue();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.setXRowIndex = function(index) {
	this._xRowIndex = index;
	this._reset = false;
	
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	
	this._datapoint = dpGrid.getDatapointAt(index, this._yAxisCol, true);

	var dv = this._datapoint.yValue;

	this._currValue = {
		asNumber: dv.numberValue,
		asText: dv.getFV(true)
	};

	if ( this._targetMode ) {
		var targDv = dpGrid.getDatapointAt(index, this._yTargetCol, true).yValue;

		this._targetValue = {
			asNumber: targDv.numberValue,
			asText: targDv.getFV(true)
		};
	}
	
	this._updateValue();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype._updateValue = function() {
	var cv = this._currValue;
	var tv = this._targetValue;
	
	this._body.setValue(cv);
	this._indi.setValue(cv, tv);
	
	if ( this._labels ) {
		this._labels.setValue(cv);
	}
	
	if ( this._dot ) {
		this._dot.setValue(cv, tv, this._reset, (this._datapoint ? this._datapoint.rangeColor : 0x0));
	}
	
	if ( this._target ) {
		this._target.setValue(tv);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getLabels = function() {
	return this._labels;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getDot = function() {
	return this._dot;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getBody = function() {
	return this._body;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getRange = function() {
	return this._range;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getTicks = function() {
	return this._ticks;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getIndi = function() {
	return this._indi;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getTarget = function() {
	return this._target;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getAxisLabelRelativeWidth = function() {
	return this._graphContext.settings.get('LinearAxisLabelWidth')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getIndiDotRelativeWidth = function() {
	return this._graphContext.settings.get('LinearIndiDotWidth')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.getIndiRelativePos = function() {
	var sett = this._graphContext.settings;

	return {
		top: sett.get('LinearIndiPosA')/100,
		bot: sett.get('LinearIndiPosB')/100
	};
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, '_handleClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.createOverListener = function() {
	return IDB.listener(this, '_handleOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype.createOutListener = function() {
	return IDB.listener(this, '_handleOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype._handleClick = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointClicked(this._datapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype._handleOver = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointOver(this._datapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBar.prototype._handleOut = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointOut(this._datapoint);
	}
};

/*====================================================================================================*/
IDB.LinearBarBody = function(settings, yAxis, bulbColor) {
	this._settings = settings;
	this._yAxis = yAxis;
	this._bulbColor = bulbColor;
	
	this._build();
};

IDB.LinearBarBody.StyleFlat = 0;
IDB.LinearBarBody.StyleChisled = 1;
IDB.LinearBarBody.StyleClock = 2;
IDB.LinearBarBody.StyleRound = 3;
IDB.LinearBarBody.StyleEmbed = 4;

IDB.LinearBarBody.FinishMatte = 0;
IDB.LinearBarBody.FinishChrome = 1;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype._build = function() {
	var sett = this._settings;
	var lblLoc = sett.get('LinearAxisLabelLocation');

	if ( sett.get('LinearAxisLabelOptions').show &&
			(lblLoc == IDB.LinearBarLabel.AxisTop || lblLoc == IDB.LinearBarLabel.AxisBot) ) {
		this._flag = new IDB.LinearBarBodyFlag(sett, this._yAxis.axisName);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.setValue = function(value) {
	if ( this._flag ) {
		this._flag.setValueText(value.asText);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getFlag = function() {
	return this._flag;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getBulbColor = function() {
	return this._bulbColor;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.isThermometerMode = function() {
	return this._settings.getBool('LinearThermometerMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getColor = function() {
	var sett = this._settings;

	return (this.getStyle() == IDB.LinearBarBody.StyleEmbed ?
		sett.get('BackgroundColor') : sett.get('LinearBodyColor'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getAlpha = function() {
	return this._settings.getAlpha('LinearBodyTrans');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getPadding = function() {
	return this._settings.get('LinearBodyPad')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getStyle = function() {
	return this._settings.get('LinearBodyStyle');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getFinish = function() {
	return this._settings.get('LinearBodyFinish');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getIndiBgColor = function() {
	return this._settings.get('LinearIndiBgColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getIndiBgAlpha = function() {
	return this._settings.getAlpha('LinearIndiBgTrans');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getIndiAlpha = function() {
	return this._settings.getAlpha('LinearIndiTrans');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getIndiShading = function() {
	return this._settings.get('LinearIndiShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBody.prototype.getAxisLabelLocation = function() {
	return this._settings.get('LinearAxisLabelLocation');
};

/*====================================================================================================*/
IDB.LinearBarBodyFlag = function(settings, axisName) {
	this._settings = settings;
	this._axisName = axisName;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlag.prototype.setValueText = function(valueText) {
	this._valueText = valueText;

	if ( this._onValueChange ) {
		this._onValueChange();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlag.prototype.setOnValueChange = function(callback) {
	this._onValueChange = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlag.prototype.getAxisName = function() {
	return this._axisName;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlag.prototype.getValueText = function() {
	return this._valueText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlag.prototype.getBgColor = function() {
	return this._settings.get('LinearAxisLabelFlagColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlag.prototype.getAxisLabelOptions = function() {
	return this._settings.get('LinearAxisLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlag.prototype.getValueLabelOptions = function() {
	return this._settings.get('LinearValueLabelOptions');
};

/*====================================================================================================*/
IDB.LinearBarBodyFlagView = function(model) {
	this._model = model;
	model.setOnValueChange(IDB.listener(this, '_onValueChange'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlagView.prototype._buildKinetic = function() {
	var model = this._model;
	var axisOpt = model.getAxisLabelOptions();
	var valOpt = model.getValueLabelOptions();
	var bgColor = model.getBgColor();

	this._display = new Kinetic.Group();
	this._lblPad = 5;

	axisOpt.align = valOpt.align = 'left';

	this._bgName = new Kinetic.Rect({
		fillLinearGradientStartPointX: 0,
		fillLinearGradientColorStops: [
			0.0, IDB.toRGBA(bgColor, 0.2),
			1.0, IDB.toRGBA(bgColor, 1.0)
		]
	});
	this._display.add(this._bgName);

	this._axisLabel = new IDB.GraphLabel(axisOpt, model.getAxisName());
	this._axisLabel.x = this._lblPad;
	this._display.add(this._axisLabel.getDisplay());

	if ( valOpt.show ) {
		this._bgVal = new Kinetic.Rect({
			fillLinearGradientStartPointX: 0,
			fillLinearGradientColorStops: [
				0.0, IDB.toRGBA(bgColor, 0.0),
				1.0, IDB.toRGBA(bgColor, 0.3)
			]
		});
		this._display.add(this._bgVal);

		this._valLabel = new IDB.GraphLabel(valOpt, ' ');
		this._display.add(this._valLabel.getDisplay());
	}

	this._onValueChange();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlagView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlagView.prototype.getRequiredHeight = function() {
	return this._requiredH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlagView.prototype.setSize = function(width, height) {
	var al = this._axisLabel;
	var vl = this._valLabel;
	
	this._display.clip({
		x: 0,
		y: 0,
		width: width,
		height: height
	});
	
	var lblH = Math.max(al.textHeight, (vl ? vl.textHeight : 0))+this._lblPad*2;
	lblH = Math.min(lblH, height/2);
	
	this._bgName.height(lblH);
	
	al.y = Math.max(0, (lblH-al.textHeight)/2)+1;
	
	if ( vl ) {
		vl.y = Math.max(0, (lblH-vl.textHeight)/2)+1;
		this._bgVal.height(lblH);
	}

	this._requiredH = Math.floor(lblH);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyFlagView.prototype._onValueChange = function() {
	var model = this._model;
	var al = this._axisLabel;
	var vl = this._valLabel;
	var val = model.getValueText();
	var pad = this._lblPad;
	var x1 = al.width+pad*2;

	this._bgName
		.width(x1)
		.fillLinearGradientEndPointX(x1);
	
	if ( vl && val && val.length > 0 ) {
		vl.visible = true;
		vl.x = x1+pad;
		vl.text = val;
		vl.width = vl.textWidth;

		var x2 = vl.x+vl.width+pad;
		
		this._bgVal
			.x(x1)
			.width(x2-x1)
			.fillLinearGradientEndPointX(x2-x1)
			.visible(true);
	}
	else if ( vl ) {
		vl.visible = false;
		vl.text = ' ';

		this._bgVal.visible(false);
	}
};

/*====================================================================================================*/
IDB.LinearBarBodyView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	if ( model.getFlag() ) {
		this._flag = new IDB.LinearBarBodyFlagView(model.getFlag());
		this._display.add(this._flag.getDisplay());
	}

	if ( model.isThermometerMode() ) {
		this._buildThermom();
	}
	else {
		this._buildBar();
	}
	
	//LATER: this.blendMode = BlendMode.LAYER;
	//LATER: this.cacheAsBitmap = true;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._buildThermom = function() {
	var model = this._model;
	var hasShading = (model.getStyle() != IDB.LinearBarBody.StyleFlat);

	////

	this._bodyBulb = new Kinetic.Circle({
		opacity: model.getAlpha(),
		fill: model.getColor().css
	});
	this._display.add(this._bodyBulb);

	if ( hasShading ) {
		this._bulbShade = new Kinetic.Group();
		this._display.add(this._bulbShade);
	}
	
	////

	this._bodyRect = new Kinetic.Line({
		closed: true,
		opacity: this._bodyBulb.opacity(),
		fill: this._bodyBulb.fill()
	});
	this._display.add(this._bodyRect);
	
	if ( hasShading ) {
		this._bodyShade = new Kinetic.Group();
		this._display.add(this._bodyShade);
	}

	////

	this._thermIndiBulb = new Kinetic.Group();
	this._display.add(this._thermIndiBulb);
	
	var indiBulbBg = new Kinetic.Circle({
		fill: model.getIndiBgColor().css,
		opacity: model.getIndiBgAlpha(),
		radius: IDB.GenericShading.FullDimension/2
	});
	this._thermIndiBulb.add(indiBulbBg);

	var indiBulb = new Kinetic.Circle({
		fill: model.getBulbColor().css,
		opacity: model.getIndiAlpha(),
		radius: IDB.GenericShading.FullDimension/2
	});
	this._thermIndiBulb.add(indiBulb);
	
	if ( model.getIndiShading() != IDB.LinearBarIndi.ShadingNone ) {
		var gs = new IDB.GenericShading(model.getIndiShading(), false);
		this._thermIndiBulb.add(gs.getDisplay());
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._buildBar = function() {
	var model = this._model;
	var hasShading = (model.getStyle() != IDB.LinearBarBody.StyleFlat);

	this._bodyRect = new Kinetic.Rect({
		opacity: model.getAlpha(),
		fill: model.getColor().css
	});
	this._display.add(this._bodyRect);
	
	if ( hasShading ) {
		this._bodyShade = new Kinetic.Group();
		this._display.add(this._bodyShade);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype.getInnerBounds = function() {
	return this._bounds;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype.setSize = function(width, height) {
	var model = this._model;
	var isTherm = model.isThermometerMode();
	var py = 0;

	this._bounds = {
		x: 0,
		y: 0,
		width: 100,
		height: 100
	};
	
	if ( this._flag ) {
		var top = (model.getAxisLabelLocation() == IDB.LinearBarLabel.AxisTop);
		var lblX = (isTherm ? height*0.8 : 0);
		this._flag.setSize(width-lblX, height);
		var lblH = this._flag.getRequiredHeight();

		if ( isTherm ) {
			var push = Math.max(0, lblH-height*0.1);
			height -= push;
			py = (top ? push : 0);

			this._flag.getDisplay()
				.x(lblX)
				.y(top ? Math.max(0, height*0.1-lblH) : height*0.9);
		}
		else {
			height -= lblH;
			py = (top ? lblH : 0);

			this._flag.getDisplay()
				.x(lblX)
				.y(top ? 0 : height);
		}
	}

	if ( isTherm ) {
		this._setSizeTherm(width, height, py);
	}
	else {
		this._setSizeBar(width, height, py);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._setSizeTherm = function(width, height, py) {
	var model = this._model;
	var r = this._bounds;
	var th = height*0.8;
	var pad = th*model.getPadding()/2;
	
	this._bodyBulb
		.x(height/2)
		.y(height/2+py)
		.radius(height/2);
		
	var bulbX = height/2;
	var bulbWidth = height-th*model.getPadding();
	var bulbScale = bulbWidth/IDB.GenericShading.FullDimension;

	this._thermIndiBulb
		.x(bulbX)
		.y(bulbX+py)
		.scaleX(bulbScale)
		.scaleY(bulbScale);
		
	r.x = height/2+bulbWidth*0.325;
	r.y = (height-th)/2+pad+py;
	r.width = width-r.x-pad;
	r.height = th-pad*2;
	
	var tbr = {
		x: r.x+0.5,
		y: (height-th)/2+py,
		width: width-r.x-0.5,
		height: th
	};

	var mx = th*0.03;
	var rx0 = tbr.x+mx;

	this._bodyRect.points([
		rx0, tbr.y,
		tbr.x+tbr.width, tbr.y,
		tbr.x+tbr.width, tbr.y+tbr.height,
		rx0, tbr.y+tbr.height,
		tbr.x-th*0.12, tbr.y+tbr.height/2
	]);
	
	if ( this._bodyShade ) {
		this._drawShading(this._bodyShade, tbr, r);
		
		var bulbC = bulbX-bulbWidth/2;

		var bsr = {
			x: 0,
			y: py,
			width: height,
			height: height
		};
		
		var bsr2 = {
			x: bulbC,
			y: bulbC+py,
			width: bulbWidth,
			height: bulbWidth
		};

		this._drawShading(this._bulbShade, bsr, bsr2, true);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._setSizeBar = function(width, height, py) {
	var model = this._model;
	var pad = Math.round(height*model.getPadding()/2);
	var r = this._bounds;

	this._bodyRect
		.x(0)
		.y(py)
		.width(width)
		.height(height);
	
	r.x = pad;
	r.y = py+pad;
	r.width = width-pad*2;
	r.height = height-pad*2;
	
	if ( this._bodyShade ) {
		var shadeR = {
			x: 0,
			y: py,
			width: width,
			height: height
		};

		this._drawShading(this._bodyShade, shadeR, r);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._drawShading = function(hold, r, r2, isCircle) {
	isCircle = IDB.ifUndef(isCircle, false);
	hold.destroyChildren();
	
	var model = this._model;
	var wh = '255, 255, 255';
	var gr = '127, 127, 127';
	var bk = '0, 0, 0';
	var isChrome = (model.getFinish() == IDB.LinearBarBody.FinishChrome);
	var halfR;

	var stops = [
		0.0, 'rgba('+wh+', 0.4)',
		0.5, 'rgba('+gr+', 0.1)',
		1.0, 'rgba('+bk+', 0.4)'
	];

	switch ( model.getStyle() ) {
		case IDB.LinearBarBody.StyleChisled:
			halfR = this._getMidRect(r, r2, 0.5);
			this._drawGrad(hold, r, halfR, stops, isCircle);
			this._drawGrad(hold, halfR, null, stops, isCircle, true);
			
			if ( isChrome ) {
				this._drawChrome(hold, r, halfR, false, 0.5, isCircle);
				this._drawChrome(hold, halfR, null, true, 0.5, isCircle);
			}
			break;
		
		case IDB.LinearBarBody.StyleClock:
			halfR = this._getMidRect(r, r2, 0.8);
			this._drawGrad(hold, r, halfR, stops, isCircle);
			this._drawGrad(hold, halfR, null, stops, isCircle, true);
			
			if ( isChrome ) {
				this._drawChrome(hold, r, halfR, false, 0.5, isCircle);
				this._drawChrome(hold, halfR, null, true, 0.5, isCircle);
			}
			break;
		
		case IDB.LinearBarBody.StyleRound:
			this._drawGrad(hold, r, null, stops, isCircle);
			
			if ( isChrome ) {
				this._drawChrome(hold, r, null, false, 0.5, isCircle);
			}
			break;
		
		case IDB.LinearBarBody.StyleEmbed:
			stops[1] ='rgba('+bk+', 0.3)';
			stops[3] ='rgba('+gr+', 0.15)';
			stops[5] ='rgba('+wh+', 0.3)';

			this._drawGrad(hold, r, null, stops, isCircle);
			
			if ( isChrome ) {
				this._drawChrome(hold, r, null, false, 0.35, isCircle);
			}
			break;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._getMidRect = function(r, r2, mid) {
	return {
		x: r.x+(r2.x-r.x)*mid,
		y: r.y+(r2.y-r.y)*mid,
		width: r.width+(r2.width-r.width)*mid,
		height: r.height+(r2.height-r.height)*mid
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._getBodyPoints = function(r, r2) {
	if ( this._model.isThermometerMode() ) {
		var bodyPts = this._bodyRect.points();
		var x0 = bodyPts[8]; //extended point
		var y0 = bodyPts[9];
		var x1 = bodyPts[0]; //top left corner
		var y1 = bodyPts[1];

		var m = (y1-y0)/(x1-x0);
		var b = -x0*m+y0;
		var leftX, leftX2;

		if ( r2 == null ) {
			leftX = (r.y-b)/m;

			return [
				leftX, r.y,
				r.x+r.width, r.y,
				r.x+r.width, r.y+r.height,
				leftX, r.y+r.height,
				x0, y0
			];
		}

		leftX = (r.y-b)/m;
		leftX2 = (r2.y-b)/m;

		return [
			leftX, r.y,
			r.x+r.width, r.y,
			r.x+r.width, r.y+r.height,
			leftX, r.y+r.height,

			leftX2, r2.y+r2.height,
			r2.x+r2.width, r2.y+r2.height,
			r2.x+r2.width, r2.y,
			leftX2, r2.y
		];
	}

	////
	
	var pts = [
		r.x, r.y,
		r.x+r.width, r.y,
		r.x+r.width, r.y+r.height,
		r.x, r.y+r.height
	];
	
	if ( r2 != null ) {
		pts.push(
			r.x, r.y,
			r2.x, r2.y,
			r2.x, r2.y+r2.height,
			r2.x+r2.width, r2.y+r2.height,
			r2.x+r2.width, r2.y,
			r2.x, r2.y
		);
	}

	return pts;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._drawGrad = function(hold, r, r2, stops, isCircle, reverse) {
	reverse = IDB.ifUndef(reverse, false);

	var config = {
		fillLinearGradientStartPointY: (reverse ? r.y+r.height : r.y),
		fillLinearGradientEndPointY: (reverse ? r.y : r.y+r.height),
		fillLinearGradientColorStops: stops
	};
	
	var grad;
	
	if ( isCircle ) {
		config.outerRadius = r.width/2;
		config.innerRadius = (r2 ? r2.width/2 : 0);
		config.angle = 360;
		config.x = r.x+config.outerRadius;
		config.y = r.y+config.outerRadius;
		config.fillLinearGradientStartPointY -= r.height/2;
		config.fillLinearGradientEndPointY -= r.height/2;

		grad = new Kinetic.Arc(config);
	}
	else {
		config.points = this._getBodyPoints(r, r2);
		config.closed = true;

		grad = new Kinetic.Line(config);
	}

	hold.add(grad);
	return grad;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarBodyView.prototype._drawChrome = function(hold, r, r2, reverse, alpha, isCircle) {
	var stops = [
		0.0, 'rgba(255, 255, 255, 1.0)',
		0.31, 'rgba(0, 0, 0, 0.4)',
		0.63, 'rgba(255, 255, 255, 0.7)',
		0.88, 'rgba(0, 0, 0, 0.5)',
		1.0, 'rgba(255, 255, 255, 0.2)'
	];

	var grad = this._drawGrad(hold, r, r2, stops, isCircle, reverse);
	grad.opacity(alpha);
};

/*====================================================================================================*/
IDB.LinearBarDot = function(settings, rangeList) {
	this._settings = settings;
	this._rangeList = rangeList;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDot.prototype.setValue = function(value, targetValue, reset, color) {
	var sett = this._settings;

	this._reset = reset;
	
	if ( sett.getBool('LinearTargetMode') && !sett.getBool('LinearTargetFixed') ) {
		var targNum = targetValue.asNumber;
		var perc = (targNum ? value.asNumber/targNum*100 : 0);
		var range = IDB.RangeHelper.getRangeForValue(
			perc, this._rangeList, Number.POSITIVE_INFINITY, true);

		this._text = (reset ? '' : Math.round(perc*10)/10+"%");
		this._color = (range ? range.color : IDB.getRGB(0x0));
	}
	else {
		this._text = value.asText;
		this._color = color;
	}
	
	if ( this._onValueChange ) {
		this._onValueChange();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDot.prototype.setOnValueChange = function(callback) {
	this._onValueChange = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDot.prototype.getText = function() {
	return this._text;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDot.prototype.getColor = function() {
	return this._color;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDot.prototype.isReset = function() {
	return this._reset;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDot.prototype.getShading = function() {
	return this._settings.get('LinearIndiShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDot.prototype.getLabelOptions = function() {
	return this._settings.get('LinearIndiDotLabelOptions');
};

/*====================================================================================================*/
IDB.LinearBarDotView = function(model) {
	this._model = model;
	model.setOnValueChange(IDB.listener(this, '_onValueChange'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDotView.prototype._buildKinetic = function() {
	var model = this._model;
	var shadingMode = model.getShading();
	var lblOpt = model.getLabelOptions();

	this._display = new Kinetic.Group();

	this._dot = new Kinetic.Circle();
	this._display.add(this._dot);

	if ( shadingMode != IDB.LinearBarIndi.ShadingNone ) {
		this._dot
			.shadowEnabled(true)
			.shadowOpacity(0.3)
			.shadowBlur(4)
			.shadowOffset({ x: 0.5, y: 3 });

		this._shading = new IDB.GenericShading(shadingMode, false);
		this._display.add(this._shading.getDisplay());
	}

	lblOpt.align = 'left';

	this._label = new IDB.GraphLabel(lblOpt, ' ');
	this._display.add(this._label.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDotView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDotView.prototype.getDotHeight = function() {
	return this._dot.radius()*2;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDotView.prototype.setSize = function(width, height) {
	var dia = Math.min(width/2, height);
	var rad = dia/2;
	var lbl = this._label;

	this._dot
		.x(rad)
		.y(rad)
		.radius(rad);

	if ( this._shading ) {
		var scale = dia/IDB.GenericShading.FullDimension;

		this._shading.getDisplay()
			.x(rad)
			.y(rad)
			.scaleX(scale)
			.scaleY(scale);
	}

	lbl.x = dia+4;
	lbl.y = (dia-lbl.textHeight)/2+2;
	lbl.width = Math.max(0, width-lbl.x);

	this._onValueChange();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarDotView.prototype._onValueChange = function() {
	var model = this._model;

	this._dot
		.fill(model.getColor().css)
		.visible(!model.isReset());

	this._label.text = model.getText();
	this._label.truncate();

	if ( this._shading ) {
		this._shading.getDisplay().opacity(model.isReset() ? 0.5 : 1);
	}
};
/* global IDB */
/*====================================================================================================*/
IDB.LinearBarIndi = function(settings, minVal, maxVal, yAxis, rangeList) {
	this._settings = settings;
	this._minVal = minVal;
	this._maxVal = maxVal;
	this._yAxis = yAxis;
	this._rangeList = rangeList;
	
	this._build();
};

IDB.LinearBarIndi.StyleCircle = 0;
IDB.LinearBarIndi.StyleTriDn = 1;
IDB.LinearBarIndi.StyleTriUp = 2;
IDB.LinearBarIndi.StyleArrowDn = 3;
IDB.LinearBarIndi.StyleArrowUp = 4;
IDB.LinearBarIndi.StyleBar = 5;
IDB.LinearBarIndi.StyleSegBox = 6;
IDB.LinearBarIndi.StyleSegRBox = 7;
IDB.LinearBarIndi.StyleSegEll = 8;

IDB.LinearBarIndi.ColorAxis = 0;
IDB.LinearBarIndi.ColorRange = 1;
IDB.LinearBarIndi.ColorCustom = 2;

IDB.LinearBarIndi.ShadingNone = 0;
IDB.LinearBarIndi.ShadingMatte = 1;
IDB.LinearBarIndi.ShadingGloss = 2;
IDB.LinearBarIndi.ShadingChrome = 3;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype._build = function() {
	var sett = this._settings;

	this._currPos = 0;

	switch ( sett.get('LinearIndiColor') ) {
		case IDB.LinearBarIndi.ColorRange:
			this._range = new IDB.LinearBarRange(sett, this._minVal, this._maxVal, this._rangeList);
			this._bulbColor = this._rangeList[0].color;
			break;
			
		case IDB.LinearBarIndi.ColorAxis:
			this._bulbColor = this._yAxis.axisColor;
			break;
			
		case IDB.LinearBarIndi.ColorCustom:
			this._bulbColor = sett.get('LinearIndiColorCustom');
			break;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.setValue = function(value, targetValue) {
	var sett = this._settings;
	var num = value.asNumber;
	
	if ( sett.getBool('LinearTargetMode') && !sett.getBool('LinearTargetFixed') ) {
		var targNum = targetValue.asNumber;
		num = (targNum ? num/targNum*100 : this._minVal);
	}
	
	var oldPos = this._currPos;
	this._currPos = (num-this._minVal)/(this._maxVal-this._minVal);
	
	if ( this._onPositionChange ) {
		this._onPositionChange(oldPos, this._currPos);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.setOnPositionChange = function(callback) {
	this._onPositionChange = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getCurrentPosition = function() {
	return this._currPos;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getRange = function() {
	return this._range;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getBulbColor = function() {
	return this._bulbColor;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getShading = function() {
	return this._settings.get('LinearIndiShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.useFullLengthShading = function() {
	return this._settings.getBool('LinearIndiShadingFull');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getShape = function() {
	return this._settings.get('LinearIndiShape');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getShapeSegmentCount = function() {
	return this._settings.get('LinearIndiShapeSegments');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getAlpha = function() {
	return this._settings.getAlpha('LinearIndiTrans');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getBgColor = function() {
	return this._settings.get('LinearIndiBgColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getBgAlpha = function() {
	return this._settings.getAlpha('LinearIndiBgTrans');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getRelativePos = function() {
	return {
		top: this._settings.get('LinearIndiPosA')/100,
		bot: this._settings.get('LinearIndiPosB')/100
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.getRelativeBgPos = function() {
	return {
		top: this._settings.get('LinearIndiBgPosA')/100,
		bot: this._settings.get('LinearIndiBgPosB')/100
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndi.prototype.isVertMode = function() {
	return this._settings.getBool('LinearVertMode');
};
/* global IDB, Kinetic, $ */
/*====================================================================================================*/
IDB.LinearBarIndiView = function(model, tickLayer) {
	this._model = model;
	this._tickLayer = tickLayer;
	model.setOnPositionChange(IDB.listener(this, '_onPositionChange'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
Object.defineProperties(IDB.LinearBarIndiView.prototype,
{
	isAnimationEnabled:
	{
		enumerable: true,
		configurable: false,
		get: function()
		{
			return this._animationsEnabled;
		},
		set: function(val)
		{
			this._animationsEnabled = !!val;
		}
	},
	isAnimating:
	{
		enumerable: true,
		configurable: false,
		get: function()
		{
			var me = this, animation = me._animation;
			return animation !== null && animation.isRunning();
		},
		set: function(val)
		{
			var me = this, animation = me._animation;
			if (val === false)
			{
				if (animation !== null)
				{
					if (animation.isRunning && animation.isRunning())
					{
						animation.stop();
					}
				}
				me._animation = null;
			}
		}
	},
	animationParametersChanged: {
		enumerable: true,
		configurable: false,
		get: function()
		{
			var me = this, changed = me._paramsChangedFlag;
			me._paramsChangedFlag = false;
			return !!changed;
		}
	},
	_animation: {
		enumerable: false,
		value: null,
		configurable: false,
		writable: true
	},
	_animationsEnabled: {
		enumerable: false,
		value: false,
		configurable: false,
		writable: true
	},
	_paramsChangedFlag: {
		value: false,
		enumerable: false,
		configurable: false,
		writable: true
	},
	_animationParameters: {
		value: null,
		enumerable: false,
		configurable: false,
		writable: true
	}
});


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._buildKinetic = function() {
	var me = this, model = me._model, context = model ? model._graphContext : null,
		settings = context ? context.settings : model && model._settings ? model._settings : null;
	var shapeMode = model.getShape();
	var shadingMode = model.getShading();
	var hasShading = (shadingMode != IDB.LinearBarIndi.ShadingNone);
	var size = 1000;

	me.isAnimationEnabled = settings && settings.animationEnabled ? settings.animationEnabled() : false;
	this._display = new Kinetic.Group();
	this._bgSize = size;
	this._indiSize = size;

	////
	
	this._bgHold = new Kinetic.Group();
	this._display.add(this._bgHold);

	var bg = new Kinetic.Rect({
		width: size,
		height: size,
		fill: model.getBgColor().css,
		opacity: model.getBgAlpha()
	});
	this._bgHold.add(bg);

	if ( hasShading ) {
		var bgShade = new Kinetic.Rect({
			width: size,
			height: size,
			fillLinearGradientStartPointY: 0,
			fillLinearGradientEndPointY: size,
			fillLinearGradientColorStops: [
				0.0, 'rgba(0, 0, 0, 0.2)',
				0.5, 'rgba(0, 0, 0, 0.0)',
				1.0, 'rgba(0, 0, 0, 0.1)'
			]
		});
		this._bgHold.add(bgShade);
	}

	if ( this._tickLayer ) {
		this._display.add(this._tickLayer);
	}

	////

	this._groupHold = new Kinetic.Group();
	this._display.add(this._groupHold);
	
	this._indiHold = new Kinetic.Group();
	this._groupHold.add(this._indiHold);

	this._indi = new Kinetic.Line({
		closed: true,
		points: this._buildIndiPoints(shapeMode)
	});
	this._indiHold.add(this._indi);
	
	this._indiFillHold = new Kinetic.Group();
	IDB.KineticUtil.setMask(this._indiFillHold, this._indi);
	this._groupHold.add(this._indiFillHold);

	if ( model.getRange() ) {
		var range = new IDB.LinearBarRangeView(model.getRange());
		range.setRawSize(size);
		this._indiFill = range.getDisplay();
	}
	else {
		this._indiFill = new Kinetic.Rect({
			width: size,
			height: size,
			fill: model.getBulbColor().css
		});
	}

	this._indiFill.opacity(model.getAlpha());
	this._indiFillHold.add(this._indiFill);

	if ( this._isFillMode && shapeMode != IDB.LinearBarIndi.StyleBar ) {
		this._segments = new Kinetic.Line({
			closed: true
		});
		this._display.add(this._segments);

		IDB.KineticUtil.setMask(this._bgHold, this._segments);
		IDB.KineticUtil.setMask(this._groupHold, this._segments);
	}
	
	if ( hasShading ) {
		if ( shapeMode == IDB.LinearBarIndi.StyleCircle ) {
			this._shading = new IDB.GenericShading(shadingMode, false);
			this._groupHold.add(this._shading.getDisplay());
		}
		else {
			this._shading = new IDB.GenericShading(shadingMode, true);
			this._shading.getDisplay()
				.width(size)
				.height(size);
				
			if ( model.useFullLengthShading() && shadingMode == IDB.LinearBarIndi.ShadingGloss ) {
				this._indiCover = new Kinetic.Group();
				this._display.add(this._indiCover);

				this._indiCover.add(this._shading.getDisplay());
			}
			else {
				this._indiFillHold.add(this._shading.getDisplay());
			}
		}

		if ( !this._segments ) { //the "segment" mask would obscure this shadow
			this._indiShadow = new Kinetic.Line({
				closed: true,
				points: this._indi.points(),
				scaleX: 0.999,
				offsetY: 2000,
				fill: bg.fill(),
				shadowEnabled: true,
				shadowOpacity: 0.3
			});
			this._indiHold.add(this._indiShadow);
		}

		//LATER: create shadows for segmented indicator that do not obsure the tick layer. Kinetic
		//doesn't provide a good solution for this. Ideally, the segmented indicator (which is a box
		//masked with a complex shape) could have its visible portions cast a shadow (like Flash).
		//Without this, the solution would involve drawing an entirely separte shape (without masking)
		//which would duplicate the visible shape of the masked/segmented indicator.
	}
	
	//LATER: this.blendMode = BlendMode.LAYER;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._buildIndiPoints = function(shapeMode) {
	var size = this._indiSize;
	var half = size/2;
	
	switch ( shapeMode ) {
		case IDB.LinearBarIndi.StyleCircle:
			var pts = [];
			var steps = 36;
			var inc = Math.PI*2/steps;
			var ccos = IDB.MathCache.cos;
			var csin = IDB.MathCache.sin;
			var i, a;
	
			for ( i = 0 ; i <= steps ; ++i ) {
				a = i*inc;
				pts.push(ccos(a)*half, csin(a)*half);
			}

			return pts;
			
		case IDB.LinearBarIndi.StyleTriDn:
			return [
				0, half,
				-half, -half,
				half, -half
			];
			
		case IDB.LinearBarIndi.StyleTriUp:
			return [
				0, -half,
				-half, half,
				half, half
			];
			
		case IDB.LinearBarIndi.StyleArrowDn:
			return [
				0, half,
				-size*0.15, size*0.35,
				-size*0.15, -half,
				size*0.15, -half,
				size*0.15, size*0.35
			];
			
		case IDB.LinearBarIndi.StyleArrowUp:
			return [
				0, -half,
				-size*0.15, -size*0.35,
				-size*0.15, half,
				size*0.15, half,
				size*0.15, -size*0.35
			];
		
		case IDB.LinearBarIndi.StyleBar:
		case IDB.LinearBarIndi.StyleSegBox:
		case IDB.LinearBarIndi.StyleSegRBox:
		case IDB.LinearBarIndi.StyleSegEll:
			this._isFillMode = true;

			return [
				0, -half,
				size, -half,
				size, half,
				0, half
			];
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype.setSize = function(width, height) {
	var model = this._model;
	var pos = model.getRelativePos();
	var bgPos = model.getRelativeBgPos();
	var useH = height*(pos.bot-pos.top);
	var indiScaleX = width/this._indiSize;
	var indiScaleY = useH/this._indiSize;
	
	this._bgHold
		.y(height*(this._segments ? pos.top : bgPos.top))
		.scaleX(width/this._bgSize)
		.scaleY((this._segments ? useH : height*(bgPos.bot-bgPos.top))/this._bgSize);

	this._indiHold
		.y(height*(pos.top+pos.bot)/2)
		.scaleX(indiScaleY) //uses "Y" scale to maintain the "shaped" indicator aspect ratios
		.scaleY(indiScaleY);

	this._indiFillHold
		.y(height*pos.top)
		.scaleX(indiScaleX)
		.scaleY(indiScaleY);
	
	if ( this._indiCover ) {
		this._indiCover
			.y(height*pos.top)
			.scaleX(indiScaleX)
			.scaleY(indiScaleY);
	}
	
	if ( this._segments ) {
		this._redrawSegments(width, useH);
	}
	
	if ( this._indiShadow ) {
		var s = 2;
		var off = {
			x: 0.5*s+indiScaleY,
			y: 3*s
		};

		if ( model.isVertMode() ) {
			off.x += this._indiShadow.offsetY()*this._indiHold.scaleY();
		}
		else {
			off.y += this._indiShadow.offsetY()*this._indiHold.scaleY();
		}

		this._indiShadow
			.shadowOffset(off)
			.shadowBlur(4*s);
	}

	this._onPositionChange();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._redrawSegments = function(width, useH) {
	var model = this._model;
	var n = model.getShapeSegmentCount();
	var pad = Math.min(2, width/n/2);
	var sw = (width-pad*(n-1))/n;
	var sw2 = sw+pad;
	var r = Math.min(sw, useH)/3;
	var sy = this._bgHold.y();
	var pts = [];
	var segPts = [];
	var i, j;
	
	switch ( model.getShape() ) {
		case IDB.LinearBarIndi.StyleSegBox:
			segPts = [
				0, 0,
				sw, 0,
				sw, useH,
				0, useH,
				0, 0
			];
			break;
			
		case IDB.LinearBarIndi.StyleSegRBox:
			segPts = this._getRoundRectPoints(sw, useH, r);
			break;
			
		case IDB.LinearBarIndi.StyleSegEll:
			segPts = this._getEllipsePoints(sw, useH);
			break;
	}

	var segPtsLen = segPts.length;

	for ( i = 0 ; i < n ; ++i ) {
		for ( j = 0 ; j < segPtsLen ; j += 2 ) {
			pts.push(segPts[j]+i*sw2, segPts[j+1]+sy);
		}
	}

	pts.push(segPts[0], segPts[1]+sy);
	this._segments.points(pts);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._getRoundRectPoints = function(width, height, radius) {
	var pts = [];
	var steps = IDB.MathUtil.clamp(Math.floor(radius*radius/25), 3, 9);
	var inc = Math.PI/2/steps;
	var ccos = IDB.MathCache.cos;
	var csin = IDB.MathCache.sin;
	var i, j, a, pushX, pushY, pushA;
	
	for ( i = 0 ; i < 4 ; ++i ) {
		pushX = (i == 0 || i == 3 ? radius : width-radius);
		pushY = (i == 0 || i == 1 ? radius : height-radius);
		pushA = i*Math.PI/2;

		for ( j = 0 ; j <= steps ; ++j ) {
			a = j*inc+pushA;
			pts.push(-ccos(a)*radius+pushX, -csin(a)*radius+pushY);
		}
	}

	pts.push(pts[0], pts[1]);
	return pts;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._getEllipsePoints = function(width, height) {
	var pts = [];
	var xRad = width/2;
	var yRad = height/2;
	var avgRad = (xRad+yRad)/2;
	var steps = IDB.MathUtil.clamp(Math.floor(avgRad*avgRad/20), 12, 36);
	var inc = Math.PI*2/steps;
	var ccos = IDB.MathCache.cos;
	var csin = IDB.MathCache.sin;
	var i, a;
	
	for ( i = 0 ; i <= steps ; ++i ) {
		a = i*inc;
		pts.push(ccos(a)*xRad+xRad, csin(a)*yRad+yRad);
	}

	pts.push(pts[0], pts[1]);
	return pts;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._onPositionChange = function(oldPos, newPos) {
	var me = this, model = me._model;
	var pos = model.getCurrentPosition();
	var w = this._bgSize*this._bgHold.scaleX();

	if ( this._isFillMode ) {
		if (oldPos !== undefined && newPos !== undefined && me.isAnimationEnabled)
		{
			if (me.isAnimating)
			{
				me.isAnimating = false;
			}
			me._animateFill(oldPos, newPos);
		}
		else if (me.isAnimationEnabled && me.isAnimating)
		{
			var ap = me._animationParameters;
			me._updateAnimationParameters({from: ap.from, to: ap.to, width: w});
		}
		else
		{
			var viz = (pos != 0); //Fix for #221; Chrome doesn't handle scaleX=0 correctly

			this._indiHold
				.visible(viz)
				.scaleX(w*pos/this._indiSize);

			this._indiFillHold.visible(viz);
		}
		return;
	}

	if (oldPos !== undefined && newPos !== undefined && me.isAnimationEnabled)
	{
		if (me.isAnimating)
		{
			me.isAnimating = false;
		}
		me._animateIndicator(oldPos, newPos);
	}
	else if (me.isAnimationEnabled && me.isAnimating)
	{
		var animParams = me._animationParameters;
		me._updateAnimationParameters({from: animParams.from, to: animParams.to, width: w});
	}
	else
	{
		var ih = this._indiHold;
		ih.x(w*pos);
		
		if ( model.getShape() != IDB.LinearBarIndi.StyleCircle ) {
			return;
		}

	    if(this._shading) {
	    	this._shading.getDisplay()
	    		.x(ih.x())
	    		.y(ih.y())
	    		.scaleX(ih.scaleX())
	    		.scaleY(ih.scaleY());
	    }
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._animateIndicator = function(from, to)
{
	var me = this, model = me._model, shapeIsStyleCircle = model ? model.getShape() == IDB.LinearBarIndi.StyleCircle : false,
		w = me._bgSize*me._bgHold.scaleX(),
		nop = function(){},
		shadingDisp = me._shading ? me._shading.getDisplay() : {setX:nop, setY:nop, setScaleX:nop, setScaleY:nop},
		disp = me._display,
		ih = me._indiHold,
		range = to - from,
		dur = 1000;

	me._animationParameters = {from: from, to: to, width: w};
	me._paramsChangedFlag = false;

	me._animation = new Kinetic.Animation(function(frame)
	{
		if (me.animationParametersChanged)
		{
			from = me._animationParameters.from;
			to = me._animationParameters.to;
			w = me._animationParameters.width;
			range = to - from;
		}
		if (frame.time <= dur)
		{
			var curPos = from + $.easing.easeOutBack(frame.time / dur) * range;
			ih.setX(w*curPos);
			if (shapeIsStyleCircle)
			{
				shadingDisp.setX(w*curPos);
				shadingDisp.setY(ih.getY());
				shadingDisp.setScaleX(ih.getScaleX());
				shadingDisp.setScaleY(ih.getScaleY());
			}
		}
		else
		{
			me._animation.stop();
			ih.setX(w*to);
			if (shapeIsStyleCircle)
			{
				shadingDisp.setX(w*to);
				shadingDisp.setY(ih.getY());
				shadingDisp.setScaleX(ih.getScaleX());
				shadingDisp.setScaleY(ih.getScaleY());
			}
		}
	}, disp.getLayer() || null);
	me._animation.start();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._animateFill = function(from, to)
{
	var me = this,
		w = me._bgSize*me._bgHold.scaleX(),
		range = to - from,
		disp = me._display,
		ih = me._indiHold,
		ifh = me._indiFillHold,
		is = me._indiSize,
		dur = 1000;

	me._animationParameters = {from: from, to: to, width: w};
	me._paramsChangedFlag = false;

	me._animation = new Kinetic.Animation(function(frame)
	{
		if (me.animationParametersChanged)
		{
			from = me._animationParameters.from;
			to = me._animationParameters.to;
			w = me._animationParameters.width;
			range = to - from;
		}
		if (frame.time <= dur)
		{
			var curPos = from + $.easing.easeOutBack(frame.time / dur) * range;
			if (curPos<=0)
			{
				ih.setVisible(false);
				ifh.setVisible(false);
			}
			else
			{
				ih.setVisible(true);
				ih.setScaleX(w*curPos/is);
				ifh.setVisible(true);
			}
		}
		else
		{
			me._animation.stop();
			if (to!==0)
			{
				ih.setVisible(true);
				ih.setScaleX(w*to/is);
				ifh.setVisible(true);
			}
			else
			{
				ih.setVisible(false);
				ifh.setVisible(false);
			}
		}
	}, disp.getLayer() || null);
	me._animation.start();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarIndiView.prototype._updateAnimationParameters = function(animationParameters)
{
	var me = this;
	if (me.isAnimationEnabled)
	{
		var params = me._animationParameters;
		me._animationParameters = animationParameters;
		if (params !== null && ((Object.hasOwnProperty('is') && !Object.is(animationParameters, params)) || (!Object.hasOwnProperty('is') && animationParameters !== params)))
			me._paramsChangedFlag = true;
	}
};

/*====================================================================================================*/
IDB.LinearBarLabel = function(settings, yAxis) {
	this._settings = settings;
	this._yAxis = yAxis;
};

IDB.LinearBarLabel.AxisPara = 0;
IDB.LinearBarLabel.AxisPerp = 1;
IDB.LinearBarLabel.AxisTop = 2;
IDB.LinearBarLabel.AxisBot = 3;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabel.prototype.setValue = function(value) {
	this._valueText = value.asText;

	if ( this._onValueChange ) {
		this._onValueChange();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabel.prototype.setOnValueChange = function(callback) {
	this._onValueChange = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabel.prototype.getAxisName = function() {
	return this._yAxis.axisName;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabel.prototype.getValueText = function() {
	return this._valueText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabel.prototype.getAxisLocation = function() {
	return this._settings.get('LinearAxisLabelLocation');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabel.prototype.getAxisLabelOptions = function() {
	return this._settings.get('LinearAxisLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabel.prototype.getValueLabelOptions = function() {
	return this._settings.get('LinearValueLabelOptions');
};

/*====================================================================================================*/
IDB.LinearBarLabelView = function(model) {
	this._model = model;
	model.setOnValueChange(IDB.listener(this, '_onValueChange'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabelView.prototype._buildKinetic = function() {
	var model = this._model;
	var axisOpt = model.getAxisLabelOptions();
	var valOpt = model.getValueLabelOptions();

	this._display = new Kinetic.Group();
	this._rotate = (model.getAxisLocation() == IDB.LinearBarLabel.AxisPerp);

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	axisOpt.align = valOpt.align = (this._rotate ? 'center' : 'right');

	this._axisLabel = new IDB.GraphLabel(axisOpt, model.getAxisName());
	this._hold.add(this._axisLabel.getDisplay());

	if ( valOpt.show ) {
		this._valLabel = new IDB.GraphLabel(valOpt, ' ');
		this._valLabel.y = this._axisLabel.textHeight;
		this._hold.add(this._valLabel.getDisplay());
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabelView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabelView.prototype.setSize = function(width, height, centerY) {
	var rot = this._rotate;
	var al = this._axisLabel;
	var vl = this._valLabel;

	this._onValueChange();

	al.resetText();
	al.width = (rot ? height : width);
	al.truncate();

	if ( vl ) {
		vl.resetText();
		vl.width = al.width;
		vl.truncate();
	}

	var rotHoldW = al.textHeight+(vl ? vl.textHeight : 0);
	var holdH = (rot ? al.width : rotHoldW);
	
	this._hold
		.x(rot ? width/2 + rotHoldW/2 : 0)
		.y((rot ? height/2 : centerY)-holdH/2)
		.clip({
			x: 0,
			y: 0,
			width: (rot ? height : width),
			height: (rot ? width : height)
		});
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarLabelView.prototype._onValueChange = function() {
	if ( this._valLabel ) {
		this._valLabel.text = this._model.getValueText();
	}
};

/*====================================================================================================*/
IDB.LinearBarRange = function(settings, minVal, maxVal, ranges) {
	this._settings = settings;
	this._segments = [];
	
	var valueDiff = maxVal-minVal;
	var len = ranges.length;
	var i, lo, hi, range, nextRange;
	
	for ( i = 0 ; i < len ; ++i ) {
		range = ranges[i];
		nextRange = ranges[i+1];
		lo = (i == 0 ? minVal : range.val);
		hi = (i == len-1 ? maxVal : nextRange.val);

		this._segments.push({
			start: (lo-minVal)/valueDiff,
			end: (hi-minVal)/valueDiff,
			color: range.color
		});
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarRange.prototype.getSegmentCount = function() {
	return this._segments.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarRange.prototype.getSegment = function(index) {
	return this._segments[index];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarRange.prototype.getRelativePos = function() {
	return {
		top: this._settings.get('LinearRangePosA')/100,
		bot: this._settings.get('LinearRangePosB')/100
	};
};

/*====================================================================================================*/
IDB.LinearBarRangeView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarRangeView.prototype._buildKinetic = function() {
	var model = this._model;
	var numSegs = model.getSegmentCount();
	var size = 1000;
	var i, seg, shape;

	this._display = new Kinetic.Group();
	this._size = size;

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	for ( i = 0 ; i < numSegs ; ++i ) {
		seg = model.getSegment(i);

		shape = new Kinetic.Rect({
			x: seg.start*size,
			width: (seg.end-seg.start)*size,
			height: size,
			fill: seg.color.css
		});
		this._hold.add(shape);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarRangeView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarRangeView.prototype.setRawSize = function(size) {
	this._hold
		.scaleX(size/this._size)
		.scaleY(size/this._size);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarRangeView.prototype.setSize = function(width, height) {
	var pos = this._model.getRelativePos();
	var y = height*pos.top;
	var h = height*pos.bot-y;

	this._hold
		.y(y)
		.scaleX(width/this._size)
		.scaleY(h/this._size);
};

/*====================================================================================================*/
IDB.LinearBarTarget = function(settings, minValue, maxValue, targetAxis) {
	this._settings = settings;
	this._minValue = minValue;
	this._maxValue = maxValue;
	this._targetAxis = targetAxis;
	this._valuePos = (100-minValue)/(maxValue-minValue);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.setValue = function(targetValue) {
	var sett = this._settings;
	
	this._valueText = targetValue.asText;

	if ( this._onValueChange ) {
		this._onValueChange();
	}

	if ( sett.getBool('LinearTargetFixed') ) {
		this._valuePos = (targetValue.asNumber-this._minValue)/(this._maxValue-this._minValue);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.setOnValueChange = function(callback) {
	this._onValueChange = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.getAxisName = function() {
	return this._targetAxis.axisName;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.getValueText = function() {
	return this._valueText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.getValuePosition = function() {
	return this._valuePos;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.getLabelOptions = function() {
	return this._settings.get('LinearTargetLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.isLabelRotated = function() {
	return this._settings.getBool('LinearTargetLabelRotate');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.isLabelOutlined = function() {
	return this._settings.getBool('LinearTargetLabelOutline');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.getLabelOutlineColor = function() {
	return this.getLabelOptions().color.x;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.getLineColor = function() {
	return this._settings.get('LinearTargetLineColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.getLabelPosition = function() {
	return this._settings.get('LinearTargetLabelPos')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTarget.prototype.getLineRelativePos = function() {
	return {
		top: this._settings.get('LinearTargetLinePosA')/100,
		bot: this._settings.get('LinearTargetLinePosB')/100
	};
};
/*====================================================================================================*/
IDB.LinearBarTargetView = function(model) {
	this._model = model;
	model.setOnValueChange(IDB.listener(this, '_onValueChange'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTargetView.prototype._buildKinetic = function() {
	var model = this._model;
	var lblOpt = model.getLabelOptions();

	this._display = new Kinetic.Group();

	this._target = new Kinetic.Line({
		points: [
			0, 0,
			0, 1000
		],
		stroke: model.getLineColor().css,
		strokeWidth: 3,
		strokeScaleEnabled: false
	});
	this._display.add(this._target);

	if ( lblOpt.show ) {
		lblOpt.align = 'left';

		this._label = new IDB.GraphLabel(lblOpt, ' ');
		this._display.add(this._label.getDisplay());

		if ( model.isLabelOutlined() ) {
			this._label.getDisplayLabel()
				.shadowEnabled(true)
				.shadowOffset({ x: 2, y: 2 })
				.shadowColor(model.getLabelOutlineColor().css)
				.shadowOpacity(1.0)
				.shadowBlur(3);
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTargetView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTargetView.prototype.setSize = function(width, height) {
	this._width = width;
	this._height = height;

	var model = this._model;
	var pos = model.getLineRelativePos();

	this._target.y(height*pos.top);
	this._target.points()[3] = height*(pos.bot-pos.top);
	
	this._onValueChange();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTargetView.prototype._onValueChange = function() {
	var model = this._model;
	var lbl = this._label;
	var width = this._width;
	var height = this._height;
	var lineX = width*model.getValuePosition();

	this._target.x(lineX);

	if ( !lbl ) {
		return;
	}

	var rot = model.isLabelRotated();
	var axisName = model.getAxisName()+' ';
	var valText = model.getValueText();
	var useW = (rot ? height : width);
	
	valText = (valText.length ? '= '+valText : '');

	lbl.text = axisName+valText;
	useW = Math.min(lbl.textWidth, useW);
	lbl.width = useW;

	if ( lbl.truncate() ) {
		lbl.text = valText;
		lbl.width = useW;
		
		if ( lbl.truncate() ) { //try truncating value only
			lbl.width = lbl.textWidth;
		}
		else { //the value alone is not truncated, so add some label
			lbl.width = useW-lbl.textWidth;
			lbl.text = axisName;
			lbl.truncate();

			lbl.text = (lbl.text == '...' ? '' : lbl.text+' ')+valText;
			lbl.width = Math.max(useW, lbl.textWidth);
		}
	}
	
	var twHalf = lbl.textWidth/2;
	var th = lbl.textHeight;
	lbl.rotateFromCenter(rot ? 90 : 0);
	lbl.x = lineX;
	lbl.y = th/2 + (height-th)*model.getLabelPosition();

	if ( rot ) {
		lbl.y = IDB.MathUtil.clamp(lbl.y, twHalf, height-twHalf);
	}
	else {
		lbl.x = IDB.MathUtil.clamp(lbl.x, twHalf, width-twHalf);
	}
};

/*====================================================================================================*/
IDB.LinearBarTicks = function(settings, minVal, maxVal, divMetrics, numFmt) {
	this._settings = settings;
	this._minVal = minVal;
	this._maxVal = maxVal;
	this._numFmt = numFmt;
	
	var gt = new IDB.GraphTicks(divMetrics,
		settings.get('LinearTickNumMinors'), settings.get('LinearTickNumThirds'));
	
	this._data = {};
	this._data[IDB.LinearBarTicks.Major] = this._buildTicks(gt.getMajorValues(), true);
	this._data[IDB.LinearBarTicks.Minor] = this._buildTicks(gt.getMinorValues());
	this._data[IDB.LinearBarTicks.Third] = this._buildTicks(gt.getThirdValues());
};

IDB.LinearBarTicks.StyleLineThin = 0;
IDB.LinearBarTicks.StyleLine = 1;
IDB.LinearBarTicks.StyleTriangle = 2;
IDB.LinearBarTicks.StyleSquare = 3;
IDB.LinearBarTicks.StyleCircle = 4;

IDB.LinearBarTicks.GuideNone = 0;
IDB.LinearBarTicks.GuideCenter = 1;
IDB.LinearBarTicks.GuideSideA = 2;
IDB.LinearBarTicks.GuideSideB = 3;
IDB.LinearBarTicks.GuideBoth = 4;

IDB.LinearBarTicks.Major = 0;
IDB.LinearBarTicks.Minor = 1;
IDB.LinearBarTicks.Third = 2;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype._buildTicks = function(values, showLabel) {
	var ticks = [];
	var len = values.length;
	var sett = this._settings;
	var targMode = (sett.getBool('LinearTargetMode') && !sett.getBool('LinearTargetFixed'));
	var v0 = this._minVal;
	var v1 = this._maxVal;
	var vDiff = v1-v0;
	var i, val, tick;

	showLabel = (showLabel && sett.get('LinearTickLabelOptions').show);

	for ( i = 0 ; i < len ; ++i ) {
		val = values[i];

		tick = {
			pos: (val-v0)/vDiff,
			label: null
		};

		if ( showLabel ) {
			tick.label = (targMode ? val+'%' : sett.formatNumber(val, this._numFmt));
		}

		ticks.push(tick);
	}

	return ticks;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getDataCount = function(type) {
	return this._data[type].length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getData = function(type, index) {
	return this._data[type][index];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getColor = function() {
	return this._settings.get('LinearTickColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getStyle = function() {
	return this._settings.get('LinearTickShape');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getRelativePos = function() {
	return {
		top: this._settings.get('LinearTickPosA')/100,
		bot: this._settings.get('LinearTickPosB')/100
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getLabelOptions = function() {
	var lo = this._settings.get('LinearTickLabelOptions');
	lo.position = this._settings.get('LinearTickLabelPos')/100;
	lo.rotate = this._settings.getBool('LinearTickLabelRotate');
	return lo;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getMinorRatio = function() {
	return this._settings.get('LinearTickMinorRatio')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getMinorRelativePosition = function() {
	return this._settings.get('LinearTickMinorPosition')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getMinorColor = function() {
	return this._settings.get('LinearTickMinorColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getGuideStyle = function() {
	return this._settings.get('LinearTickGuideStyle');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicks.prototype.getGuideColor = function() {
	return this._settings.get('LinearTickGuideColor');
};

/*====================================================================================================*/
IDB.LinearBarTicksView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicksView.prototype._buildKinetic = function() {
	var model = this._model;
	var size = 1000;
	var minorA = size*(1-model.getMinorRatio())*model.getMinorRelativePosition();
	var minorB = minorA+size*model.getMinorRatio();

	var majorShape = this._getMajorShape(size);
	
	var minorShape = new Kinetic.Line({
		points: [
			0, minorA,
			0, minorB
		],
		stroke: IDB.toRGBA(model.getMinorColor(), 0.85),
		strokeWidth: 1,
		strokeScaleEnabled: false
	});
	
	var thirdShape = new Kinetic.Line({
		points: minorShape.points(),
		stroke: IDB.toRGBA(model.getMinorColor(), 0.22),
		strokeWidth: 1,
		strokeScaleEnabled: false
	});

	this._ticks = [];
	this._labels = [];
	this._size = size;
	this._labelH = 0;
	
	this._display = new Kinetic.Group();

	this._tickHold = new Kinetic.Group();
	this._display.add(this._tickHold);

	this._labelHold = new Kinetic.Group();

	this._drawGuides();
	this._drawTicks(IDB.LinearBarTicks.Major, majorShape);
	this._drawTicks(IDB.LinearBarTicks.Minor, minorShape);
	this._drawTicks(IDB.LinearBarTicks.Third, thirdShape);

	this._display.add(this._labelHold);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicksView.prototype._getMajorShape = function(size) {
	var model = this._model;
	var style = model.getStyle();
	var col = model.getColor().css;
	var s;

	if ( style == IDB.LinearBarTicks.StyleCircle ) {
		return new Kinetic.Circle({
			offsetY: -size/2,
			radius: size/2,
			fill: col
		});
	}

	var config = {
		closed: true,
		fill: col,
		strokeWidth: 0,
		strokeScaleEnabled: false
	};
	
	switch ( style ) {
		case IDB.LinearBarTicks.StyleLineThin:
		case IDB.LinearBarTicks.StyleLine:
			config.closed = false;
			config.stroke = col;
			config.strokeWidth = (style == IDB.LinearBarTicks.StyleLineThin ? 2 : 3);

			config.points = [
				0, 0,
				0, size
			];

			break;
			
		case IDB.LinearBarTicks.StyleTriangle:
			s = size/3;

			config.points = [
				0, size,
				s, 0,
				-s, 0
			];

			break;
			
		case IDB.LinearBarTicks.StyleSquare:
			s = size/2;

			config.points = [
				-s, 0,
				s, 0,
				s, size,
				-s, size
			];

			break;
	}

	return new Kinetic.Line(config);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicksView.prototype._drawGuides = function() {
	var model = this._model;
	var style = model.getGuideStyle();
	var size = this._size;
	var half = size/2;
	
	if ( style == IDB.LinearBarTicks.GuideNone ) {
		return;
	}
	
	var line = new Kinetic.Line({
		stroke: model.getGuideColor().css,
		strokeWidth: 1,
		strokeScaleEnabled: false
	});
	
	switch ( style ) {
		case IDB.LinearBarTicks.GuideCenter:
			line.points([
				0, half,
				size, half
			]);
			this._tickHold.add(line);
			return;

		case IDB.LinearBarTicks.GuideSideA:
			line.points([
				0, 0,
				size, 0
			]);
			this._tickHold.add(line);
			return;

		case IDB.LinearBarTicks.GuideSideB:
			line.points([
				0, size,
				size, size
			]);
			this._tickHold.add(line);
			return;

		case IDB.LinearBarTicks.GuideBoth:
			line.points([
				0, 0,
				size, 0
			]);
			this._tickHold.add(line);

			line = line.clone();
			line.points([
				0, size,
				size, size
			]);
			this._tickHold.add(line);
			return;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicksView.prototype._drawTicks = function(type, shape) {
	var model = this._model;
	var size = this._size;
	var count = model.getDataCount(type);
	var lblOpt = model.getLabelOptions();
	var i, data, tick, lbl;
	
	lblOpt.align = 'center';

	for ( i = 0 ; i < count ; ++i ) {
		data = model.getData(type, i);

		tick = shape.clone();
		tick.x(data.pos*size+1);
		this._tickHold.add(tick);
		this._ticks.push(tick);

		if ( !data.label ) {
			continue;
		}

		lbl = new IDB.GraphLabel(lblOpt, data.label);
		lbl.x = data.pos*size;
		lbl.rotateFromCenter(lblOpt.rotate ? 90 : 0);
		this._labelHold.add(lbl.getDisplay());
		this._labels.push(lbl);

		if ( i == 0 ) {
			this._labelH = lbl.textHeight*(lblOpt.rotate ? 0.8 : 0.9);
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicksView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarTicksView.prototype.setSize = function(width, height) {
	var model = this._model;
	var pos = model.getRelativePos();
	var lblOpt = model.getLabelOptions();
	var lblRot = lblOpt.rotate;
	var ticks = this._ticks;
	var labels = this._labels;
	var size = this._size;
	var tickY = height*pos.top;
	var tickH = height*pos.bot-tickY+0.5;
	var scaleX = (width-1)/size;
	var lblHoldMinY = this._labelH/2;
	var lblHoldMaxY = height-this._labelH/2;
	var lblHoldY = (lblHoldMaxY-lblHoldMinY)*lblOpt.position + lblHoldMinY;
	var i, lbl, halfTextW, halfTextH;

	this._tickHold
		.y(tickY)
		.scaleX(scaleX)
		.scaleY(tickH/size);
		
	this._labelHold
		.y(lblHoldY)
		.scaleX(scaleX);

	var undoScaleX = this._tickHold.scaleY()/scaleX;
	var prevX = -9999;

	for ( i in ticks ) {
		ticks[i].scaleX(undoScaleX);
	}

	undoScaleX = 1/scaleX;

	for ( i in labels ) {
		lbl = labels[i];
		lbl.scaleX = undoScaleX;
		lbl.x = ticks[i].x()-1;

		if ( lblRot ) {
			halfTextW = lbl.textHeight*undoScaleX/2;
			halfTextH = lbl.textWidth*0.25;
			lbl.x += halfTextW;
			lbl.y = 0;

			if ( lblHoldY+halfTextH > lblHoldMaxY ) {
				lbl.y = (lblHoldMaxY-lblHoldY)-halfTextH;
			}

			if ( lblHoldY-halfTextH < lblHoldMinY ) {
				lbl.y = halfTextH-(lblHoldY-lblHoldMinY);
			}
		}
		else {
			halfTextW = lbl.textWidth*undoScaleX/2;
			lbl.y = 1;
		}

		if ( lbl.x+halfTextW > size ) {
			lbl.x = size-halfTextW;
		}
		else if ( lbl.x-halfTextW < 1 ) {
			lbl.x = halfTextW+1;
		}
		
		if ( lbl.x-halfTextW < prevX ) {
			lbl.visible = false;
		}
		else {
			lbl.visible = true;
			prevX = lbl.x+halfTextW;
		}
	}
};

/*====================================================================================================*/
IDB.LinearBarView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());
	this._display._touchHandler = new IDB.TouchHandler(this._display);

	if ( model.getLabels() ) {
		this._labels = new IDB.LinearBarLabelView(model.getLabels());
		this._display.add(this._labels.getDisplay());
	}
	
	if ( model.getDot() ) {
		this._dot = new IDB.LinearBarDotView(model.getDot());
		this._display.add(this._dot.getDisplay());
	}
	
	this._body = new IDB.LinearBarBodyView(model.getBody());
	this._display.add(this._body.getDisplay());
	
	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	if ( model.getRange() ) {
		this._range = new IDB.LinearBarRangeView(model.getRange());
		this._hold.add(this._range.getDisplay());
	}
	
	if ( model.getTicks() ) {
		this._ticks = new IDB.LinearBarTicksView(model.getTicks());
	}
	
	this._indi = new IDB.LinearBarIndiView(model.getIndi(),
		(this._ticks ? this._ticks.getDisplay() : null));
	this._hold.add(this._indi.getDisplay());
	
	if ( model.getTarget() ) {
		this._target = new IDB.LinearBarTargetView(model.getTarget());
		this._hold.add(this._target.getDisplay());
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearBarView.prototype.setSize = function(width, height) {
	var model = this._model;

	var lblW = (this._labels ? width*model.getAxisLabelRelativeWidth() : 0);
	var dotW = (this._dot ? width*model.getIndiDotRelativeWidth() : 0);
	var bodyX = Math.round(lblW);

	this._body.getDisplay().x(bodyX);
	this._body.setSize(width-bodyX-dotW, height);
	
	var rect = this._body.getInnerBounds();
	rect.x = Math.round(rect.x);
	rect.y = Math.round(rect.y);

	var indiPos = model.getIndiRelativePos();
	var pa = indiPos.top;
	var indiCenter = rect.y+(pa+(indiPos.bot-pa)/2)*rect.height;
	
	this._hold
		.x(Math.round(bodyX+rect.x))
		.y(Math.round(rect.y))
		.clip({
			x: 0,
			y: 0,
			width: rect.width,
			height: rect.height
		});

	this._indi.setSize(rect.width, rect.height);
	
	if ( this._range ) {
		this._range.setSize(rect.width, rect.height);
	}

	if ( this._ticks ) {
		this._ticks.setSize(rect.width, rect.height);
	}

	if ( this._target ) {
		this._target.setSize(rect.width, rect.height);
	}
	
	if ( this._labels ) {
		this._labels.setSize(lblW-3, height, indiCenter);
	}
	
	if ( this._dot ) {
		this._dot.setSize(dotW-4, height/2.5);
		var dotH = this._dot.getDotHeight();

		this._dot.getDisplay()
			.x(width-dotW+4)
			.y(indiCenter-dotH/2);
	}
};

/*====================================================================================================*/
IDB.LinearGraph = function(chart, $div, graphProperties, dataSet) {
    var mt = graphProperties.get('DatapointMatchType');
    if(mt !== IDB.MT.NONE) {
        graphProperties.set('DatapointMatchType', IDB.MT.X_VALUE);
    }
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	
	var sett = this._graphContext.settings;
	var isColorAxis = (sett.get('LinearIndiColor') == IDB.LinearBarIndi.ColorAxis);

	this._info.calloutStyle = (sett.getBool('LinearTargetMode') ? 'none' :
		(isColorAxis ? 'axisColors' : 'rangeColors'));
	this._info.legendStyle = (isColorAxis ? IDB.LS.AXES : IDB.LS.RANGES);
    this._info.xMatchOnly = true;
};

IDB.LinearGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.LinearGraph.prototype.constructor = IDB.LinearGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearGraph.prototype._build = function() {
	var gc = this._graphContext;
	this._updateCounts();
	
	if ( this._graphContext.settings.getBool('LinearTargetMode') ) {
		gc.datapointGrid.setDefaultRangeList(IDB.GraphInfo.TargetRangeList);
	}

	this._bars = new IDB.LinearSet(gc);
	this._barsView = new IDB.LinearSetView(this._bars);
	this._stageLayer.add(this._barsView.getDisplay());
	
	this._odoms = new IDB.GraphOdomSet(gc, this._odomStart, this._odomCount);
	this._odomsView = new IDB.GraphOdomSetView(this._odoms);
	this._stageLayer.add(this._odomsView.getDisplay());
	
	var showFunc = IDB.listener(this, '_showRow');
	var persist = gc.settings.getBool('LinearRowPersist');
	var sr = new IDB.GraphSingleRow(gc, showFunc, persist);

	this._srView = new IDB.GraphSingleRowView(sr);
	this._stageLayer.add(this._srView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearGraph.prototype._updateCounts = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var yCount = dpGrid.getNumYCols(true);
	var maxN = yCount;
	var nMult = 1;
	var legI = 0;
	
	if ( sett.getBool('LinearTargetMode') ) {
		nMult = 2;
		maxN = Math.floor(yCount/nMult);
		legI = 1;
	}

	////
	
	var n = sett.get('LinearBarCount');
	var sn = sett.get('LinearBarShowCount');

	if ( n == 0 || n > maxN ) {
		n = maxN;
	}

	if ( sn == 0 || sn > n ) {
		sn = n;
	}

	sett.set('LinearBarCount', n);
	sett.set('LinearBarShowCount', sn);

	this._odomStart = n*nMult;
	this._odomCount = Math.max(0, yCount-this._odomStart);

	////

	this._legendAxes = [ dpGrid.getXAxis() ];

	for ( var i = legI ; i < this._odomStart ; i += nMult ) {
		this._legendAxes.push(dpGrid.getYAxis(i, true));
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearGraph.prototype._showRow = function(datapointRow) {
	if ( datapointRow == null ) {
		this._bars.resetBars();
		this._odoms.resetOdoms();
		return;
	}

	this._bars.setXRowIndex(datapointRow.rowId);
	this._odoms.setXRowIndex(datapointRow.rowId);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearGraph.prototype._update = function(width, height) {
	this._srView.setSize(width, height);
	var rect = this._srView.getContentRect();
	this._barsView.setSize(rect.width, rect.height);

	var pad = 5;
	var barH = rect.height;
	var odomW = this._odomsView.getMinWidth(barH);
	var barW = Math.max(0, rect.width-(odomW ? odomW+pad : 0));
	
	this._barsView.setSize(barW, barH);
	this._barsView.getDisplay().y(rect.y);

	this._odomsView.setSize(odomW, barH);
	this._odomsView.getDisplay()
		.x(rect.width-odomW)
		.y(rect.y)
		.visible(this._odoms.getOdomCount() > 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearGraph.prototype.getLegendAxes = function() {
	return this._legendAxes;
};

/*====================================================================================================*/
IDB.LinearSet = function(graphContext) {
	this._graphContext = graphContext;

	var sett = graphContext.settings;
	var itemCount = sett.get('LinearBarCount');

	this._currXRow = 0;
	this._reset = true;
	this._columns = this._buildColumns(itemCount);
	this._pages = new IDB.GraphPages(graphContext, IDB.listener(this, '_buildBar'),
		itemCount, sett.get('LinearBarShowCount'));
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSet.prototype._buildColumns = function(itemCount) {
	var cols = [];
	var sett = this._graphContext.settings;
	var yCol = 0;
	var i;

	if ( sett.getBool('LinearTargetMode') ) {
		for ( i = 0 ; i < itemCount ; ++i ) {
			cols[i] = [yCol+1, yCol];
			yCol += 2;
		}

		return cols;
	}

	for ( i = 0 ; i < itemCount ; ++i ) {
		cols[i] = [yCol++, -1];
	}

	return cols;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSet.prototype._buildBar = function(index) {
	var cols = this._columns[index];
	var gc = this._graphContext;
	var sett = gc.settings;
	var bar = new IDB.LinearBar(gc, cols[0], cols[1]);
	
	if ( this._reset ) {
		bar.resetBar();
	}
	else {
		bar.setXRowIndex(this._currXRow);
	}

	var i = (this._pages ? this._pages.getCurrentIndex() : 0);

	if ( sett.getBool('LinearTargetMode') ) {
		i = i*2+1;
	}

	this._graphContext.onYAxisSelected(i);
	return bar;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSet.prototype.setXRowIndex = function(index) {
	this._currXRow = index;
	this._reset = false;
	
	var pages = this._pages;
	var itemCount = this._pages.getItemCount();
	var i, bar;
	
	for ( i = 0 ; i < itemCount ; ++i ) {
		bar = pages.getItem(i);

		if ( bar ) {
			bar.setXRowIndex(index);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSet.prototype.resetBars = function() {
	this._reset = true;
	
	var pages = this._pages;
	var itemCount = this._pages.getItemCount();
	var i, bar;
	
	for ( i = 0 ; i < itemCount ; ++i ) {
		bar = pages.getItem(i);

		if ( bar ) {
			bar.resetBar();
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSet.prototype.getGraphPages = function() {
	return this._pages;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSet.prototype.isVertMode = function() {
	return this._graphContext.settings.getBool('LinearVertMode');
};

/*====================================================================================================*/
IDB.LinearSetView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSetView.prototype._buildKinetic = function() {
	var model = this._model;
	var buildFunc = IDB.listener(this, '_buildBar');
	var layoutFunc = IDB.listener(this, '_updateLayout');
	
	this._display = new Kinetic.Group();

	this._pagesView = new IDB.GraphPagesView(model.getGraphPages());
	this._pagesView.build(buildFunc, layoutFunc);
	this._display.add(this._pagesView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSetView.prototype._buildBar = function(index, barModel) {
	return new IDB.LinearBarView(barModel);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSetView.prototype._updateLayout = function() {
	var model = this._model;
	var pv = this._pagesView;
	var pages = model.getGraphPages();
	var availW = pv.getAvailableWidth();
	var availH = pv.getAvailableHeight();
	var itemHold = pv.getItemDisplay();
	
	if ( model.isVertMode() ) {
		itemHold.rotation(-90).y(availH);

		var temp = availW;
		availW = availH;
		availH = temp;
	}

	var useW = availW;
	var useH = availH;
	var btnH = pv.getButtonsHeight();
	var currI = pages.getCurrentIndex();
	var count = pages.getShowCount();
	var maxI = currI+count;
	
	if ( model.isVertMode() ) {
		useW -= btnH;
	}
	else {
		useH -= btnH;
	}
	
	var barPad = Math.min(5, useH/(count*10));
	var barH = (useH-(count-1)*barPad)/count;

	if ( barH/useW > 0.35 ) {
		barH = useW*0.35;
	}

	var pushY = Math.max(0, (useH-count*(barH+barPad))/2);
	var i, barView;
	
	for ( i = currI ; i < maxI ; ++i ) {
		barView = pv.getItemView(i);
		barView.setSize(useW, barH);
		barView.getDisplay()
			.x(Math.round(availW-useW))
			.y(Math.round((i-currI)*(barH+barPad)+pushY));
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSetView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinearSetView.prototype.setSize = function(width, height) {
	this._pagesView.setSize(width, height);
};

/*====================================================================================================*/

