'use strict';
IDB.GaugeGraph = function(chart, $div, graphProperties, dataSet) {
    var mt = graphProperties.get('DatapointMatchType');
    if(mt !== IDB.MT.NONE) {
        graphProperties.set('DatapointMatchType', IDB.MT.X_VALUE);
    }
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	this._info.calloutStyle = 'rangeColors';
	this._info.legendStyle = IDB.LS.RANGES;
    this._info.xMatchOnly = true;
};

IDB.GaugeGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.GaugeGraph.prototype.constructor = IDB.GaugeGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeGraph.prototype._build = function() {
	var gc = this._graphContext;
	
	if ( this._graphContext.settings.getBool('GaugeTargetMode') ) {
		gc.datapointGrid.setDefaultRangeList(IDB.GraphInfo.TargetRangeList);
	}

	this._updateCounts();
	this._updateAngles();
	this._updateLabels();
	this._calcValues();

	this._gauges = new IDB.GaugeSet(gc);
	this._gaugesView = new IDB.GaugeSetView(this._gauges);
	this._stageLayer.add(this._gaugesView.getDisplay());
	
	this._odoms = new IDB.GraphOdomSet(gc, this._odomStart, this._odomCount);
	this._odomsView = new IDB.GraphOdomSetView(this._odoms);
	this._stageLayer.add(this._odomsView.getDisplay());
	
	var showFunc = IDB.listener(this, '_showRow');
	var persist = gc.settings.getBool('GaugeRowPersist');
	var sr = new IDB.GraphSingleRow(gc, showFunc, persist);

	this._srView = new IDB.GraphSingleRowView(sr);
	this._stageLayer.add(this._srView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeGraph.prototype._updateCounts = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var yCount = dpGrid.getNumYCols(true);
	var maxN = yCount;
	var nMult = 1;
	var legI = 0;
	var legMult = 1;
			
	if ( sett.getBool('GaugeTargetMode') ) {
		nMult = 2;
		maxN = Math.floor(yCount/nMult);
		legI = 1;
		legMult = nMult;
	}
	else if ( sett.getBool('GaugeClusterMode') ) {
		nMult = sett.get('GaugeClusterCount');
		maxN = Math.max(1, Math.floor(yCount/nMult));
		legMult = 1;
	}
	else if ( sett.getBool('GaugeMultiNeedleMode') ) {
		nMult = Math.min(sett.get('GaugeNumNeedles'), yCount);
		maxN = Math.floor(yCount/nMult);
		sett.set('GaugeNumNeedles', nMult);
		legMult = nMult;
	}

	////
	
	var n = sett.get('GaugeNumSpeedoms');
	var sn = sett.get('GaugeNumShownSpeedoms');

	if ( n == 0 || n > maxN ) {
		n = maxN;
	}

	if ( sn == 0 || sn > n ) {
		sn = n;
	}

	sett.set('GaugeNumSpeedoms', n);
	sett.set('GaugeNumShownSpeedoms', sn);

	this._odomStart = n*nMult;
	this._odomCount = Math.max(0, yCount-this._odomStart);

	////

	this._legendAxes = [ dpGrid.getXAxis() ];

	for ( var i = legI ; i < this._odomStart ; i += legMult ) {
		this._legendAxes.push(dpGrid.getYAxis(i, true));
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeGraph.prototype._updateAngles = function() {
	var sett = this._graphContext.settings;
	var start = sett.get('GaugeAngleStart');
	var end = sett.get('GaugeAngleEnd');
	var range = end-start;

	if ( range < 60 ) {
		var center = start+range/2;
		sett.set('GaugeAngleStart', center-30);
		sett.set('GaugeAngleEnd', center+30);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeGraph.prototype._updateLabels = function() {
	var sett = this._graphContext.settings;
	
	var lo = sett.get('GaugeTickLabelOptions');
	lo.radius = (sett.getBool('GaugeClusterMode') ? 0.79 : sett.get('GaugeTickLabelRadius')/100);
	lo.rotate = sett.getBool('GaugeTickLabelRotate');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeGraph.prototype._calcValues = function() {
	var sett = this._graphContext.settings;
	var rad = 200;

	sett.set('ValueRadius', rad);

	var bordEdge = rad*sett.get('GaugeBorderThick')/100;
	var faceRad = rad-bordEdge;
	var bordInner = faceRad+(rad-faceRad)*sett.get('GaugeBorderMattingThick')/100;

	sett.set('ValueBorderEdge', bordEdge);
	sett.set('ValueFaceRadius', faceRad);
	sett.set('ValueBorderInner', bordInner);

	var needLen = faceRad*sett.get('GaugeNeedleLength')/100;
	var needLenCut = Math.min(0.03, 0.666/sett.get('GaugeNumNeedles'));
	var needThick = faceRad*sett.get('GaugeNeedleThickness')/100*0.2;
	var needExtLen = needLen*0.25;
	var needPinRad = needExtLen*sett.get('GaugeNeedleBaseSize')/100;

	sett.set('ValueNeedleLen', needLen);
	sett.set('ValueNeedleLenCut', needLenCut);
	sett.set('ValueNeedleThick', needThick);
	sett.set('ValueNeedleExtLen', needExtLen);
	sett.set('ValueNeedlePinRad', needPinRad);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeGraph.prototype._showRow = function(datapointRow) {
	if ( datapointRow == null ) {
		this._gauges.resetSpeedoms();
		this._odoms.resetOdoms();
		return;
	}

	this._gauges.setXRowIndex(datapointRow.rowId);
	this._odoms.setXRowIndex(datapointRow.rowId);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeGraph.prototype._update = function(width, height) {
	this._srView.setSize(width, height);
	var rect = this._srView.getContentRect();
	this._gaugesView.setSize(rect.width, rect.height);

	var pad = 5;
	var speedH = rect.height;
	var odomW = this._odomsView.getMinWidth(speedH);
	var speedAllowW = rect.width-(odomW ? odomW+pad : 0);
	var speedW = this._gaugesView.getRequiredWidth(speedAllowW, speedH);
	var gvDisp = this._gaugesView.getDisplay();
	var odDisp = this._odomsView.getDisplay();

	this._gaugesView.setSize(speedW, speedH);
	gvDisp
		.x((rect.width-(speedW+(odomW ? odomW+pad : 0)))/2)
		.y(rect.y);

	this._odomsView.setSize(odomW, speedH);
	odDisp
		.x(gvDisp.x()+speedW+pad)
		.y(rect.y)
		.visible(this._odoms.getOdomCount() > 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeGraph.prototype.getLegendAxes = function() {
	return this._legendAxes;
};

/*====================================================================================================*/
IDB.GaugeSet = function(graphContext) {
	this._graphContext = graphContext;

	var sett = graphContext.settings;
	var itemCount = sett.get('GaugeNumSpeedoms');

	this._currXRow = 0;
	this._reset = true;
	this._labelOnFace = this._hasLabelOnFace();
	this._columns = this._buildColumns(itemCount);
	this._pages = new IDB.GraphPages(graphContext, IDB.listener(this, '_buildGauge'), 
		itemCount, sett.get('GaugeNumShownSpeedoms'));
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSet.prototype._hasLabelOnFace = function() {
	var sett = this._graphContext.settings;
	var onFace = false;

	if ( sett.get('GaugeAxisLabelOptions').show ) {
		var gtc = sett.get('GraphTypeCode');

		onFace = (gtc.indexOf('Full') != -1 || gtc.indexOf('Square') != -1 || 
			gtc.indexOf('Cluster') != -1);

		if ( !onFace && gtc.indexOf('Custom') != -1 ) {
			var diff = sett.get('GaugeAngleEnd')-sett.get('GaugeAngleStart');
			onFace = (diff > 180 && diff <= 270);
		}
	}

	return onFace;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSet.prototype._buildColumns = function(itemCount) {
	var cols = [];
	var sett = this._graphContext.settings;
	var yCol = 0;
	var i;

	if ( sett.getBool('GaugeTargetMode') ) {
		for ( i = 0 ; i < itemCount ; ++i ) {
			cols[i] = [yCol+1, yCol];
			yCol += 2;
		}

		return cols;
	}

	if ( sett.getBool('GaugeClusterMode') ) {
		var clusterCount = Math.max(sett.get('GaugeClusterCount'), 1);
		var j, indexes;

		for ( i = 0 ; i < itemCount ; ++i ) {
			indexes = [];

			for ( j = 1 ; j < clusterCount ; ++j ) {
				indexes.push(yCol+j);
			}

			cols[i] = [yCol, indexes, -1];
			yCol += clusterCount;
		}

		return cols;
	}

	if ( sett.getBool('GaugeMultiNeedleMode') ) {
		var numNeedles = sett.get('GaugeNumNeedles');

		for ( i = 0 ; i < itemCount ; ++i ) {
			cols[i] = [yCol, -1];
			yCol += numNeedles;
		}

		return cols;
	}

	for ( i = 0 ; i < itemCount ; ++i ) {
		cols[i] = [yCol++, -1];
	}

	return cols;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSet.prototype._buildGauge = function(index) {
	var cols = this._columns[index];
	var gc = this._graphContext;
	var sett = gc.settings;
	var gauge;
			
	if ( sett.getBool('GaugeClusterMode') ) {
		gauge = new IDB.GaugeCluster(gc, cols[0], cols[1]);
	}
	else {
		gauge = new IDB.GaugeSpd(gc, cols[0], cols[1], this._labelOnFace);
	}
	
	if ( this._reset ) {
		gauge.resetNeedle();
	}
	else {
		gauge.setXRowIndex(this._currXRow);
	}

	var i = (this._pages ? this._pages.getCurrentIndex() : 0);

	if ( sett.getBool('GaugeTargetMode') ) {
		i = i*2+1;
	}
	else if ( sett.getBool('GaugeMultiNeedleMode') ) {
		i = i*sett.get('GaugeNumNeedles');
	}

	this._graphContext.onYAxisSelected(i);
	return gauge;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSet.prototype.setXRowIndex = function(index) {
	this._currXRow = index;
	this._reset = false;
	
	var pages = this._pages;
	var itemCount = this._pages.getItemCount();
	var i, gauge;
			
	for ( i = 0 ; i < itemCount ; ++i ) {
		gauge = pages.getItem(i);

		if ( gauge ) {
			gauge.setXRowIndex(index);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSet.prototype.resetSpeedoms = function() {
	this._reset = true;
	
	var pages = this._pages;
	var itemCount = this._pages.getItemCount();
	var i, gauge;
			
	for ( i = 0 ; i < itemCount ; ++i ) {
		gauge = pages.getItem(i);

		if ( gauge ) {
			gauge.resetNeedle();
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSet.prototype.getGraphPages = function() {
	return this._pages;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSet.prototype.isClusterMode = function() {
	return this._graphContext.settings.getBool('GaugeClusterMode');
};

/*====================================================================================================*/
IDB.GaugeSetView = function(model) {
	this._model = model;
	this._sizeRatio = null;
	this._extraH = 0;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSetView.prototype._buildKinetic = function() {
	var model = this._model;
	var buildFunc = IDB.listener(this, '_buildGauge');
	var layoutFunc = IDB.listener(this, '_updateGaugeLayout');
	
	this._display = new Kinetic.Group();

	this._pagesView = new IDB.GraphPagesView(model.getGraphPages());
	this._pagesView.build(buildFunc, layoutFunc);
	this._display.add(this._pagesView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSetView.prototype._buildGauge = function(index, itemModel) {
	var item;

	if ( this._model.isClusterMode() ) {
		item = new IDB.GaugeClusterView(itemModel);
	}
	else {
		item = new IDB.GaugeSpdView(itemModel);
	}

	if ( !this._sizeRatio ) {
		this._sizeRatio = item.getSizeRatio();
		this._extraH = item.getFixedExtraHeight();
	}

	return item;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSetView.prototype._updateGaugeLayout = function() {
	var pv = this._pagesView;
	var availW = pv.getAvailableWidth();
	var availH = pv.getAvailableHeight();
	var useW = availW;
	var useH = availH;
	var btnsW = pv.getButtonsWidth();
	var btnW = 0;
			
	if ( btnsW ) {
		var sp1 = this._getSpacing(availW, availH);
				
		if ( sp1.getTotalHeight() < availH-25 ) {
			useH -= 25;
		}
		else {
			var sp2 = this._getSpacing(availW-btnsW, availH);
			var sp3 = this._getSpacing(availW, availH-25);
					
			if ( sp2.getItemWidth() > sp3.getItemWidth() ) {
				btnW = btnsW/2;
				useW -= btnsW;
			}
			else {
				useH -= 25;
			}
		}
	}
	
	var pages = this._model.getGraphPages();
	var currI = pages.getCurrentIndex();
	var maxI = currI+pages.getShowCount();
	var spacing = this._getSpacing(useW, useH);
	var itemW = spacing.getItemWidth();
	var itemH = spacing.getItemHeight();
	var i, p, itemView;
	var maxY = 0;

	for ( i = currI ; i < maxI ; ++i ) {
		p = spacing.getPos(i-currI);

		itemView = pv.getItemView(i);
		itemView.setSize(itemW, itemH);
		itemView.getDisplay().x(p.x+btnW).y(p.y);

		maxY = Math.max(p.y, maxY);
	}
			
	pv.setButtonY(maxY+itemH+5);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSetView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSetView.prototype.getRequiredWidth = function(width, height) {
	var sp1 = this._getSpacing(width, height);
	var btnW = this._pagesView.getButtonsWidth();

	if ( !btnW || sp1.getTotalHeight() < height-25 ) {
		return sp1.getTotalWidth();
	}
	
	var sp2 = this._getSpacing(width-btnW, height);
	var sp3 = this._getSpacing(width, height-25);

	if ( sp2.getItemWidth() > sp3.getItemWidth() ) {
		return sp2.getTotalWidth()+btnW;
	}

	return sp3.getTotalWidth();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSetView.prototype._getSpacing = function(width, height) {
	var showCount = this._model.getGraphPages().getShowCount();
	return new IDB.GraphSpacing(showCount, width, height, this._sizeRatio, this._extraH);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSetView.prototype.setSize = function(width, height) {
	this._pagesView.setSize(width, height);
};

/*====================================================================================================*/
IDB.GaugeSpd = function(graphContext, yAxisCol, yTargetCol, labelOnFace) {
	this._graphContext = graphContext;
	this._yAxisCol = yAxisCol;
	this._yTargetCol = yTargetCol;
	this._labelOnFace = labelOnFace;

	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var yAxis = dpGrid.getYAxis(this._yAxisCol, true);
	
	this._targetMode = sett.getBool('GaugeTargetMode');
	this._numNeedles = sett.get('GaugeNumNeedles');
	this._angleStart = sett.get('GaugeAngleStart');
	this._angleEnd = sett.get('GaugeAngleEnd');
	this._needles = [];
	this._currValues = [];

	this._targetValue = null;
	this._reset = false;
	this._datapoint = null;
	this._xRowIndex = 0;
	
	this._calcMinMaxVals();

	this._mask = new IDB.GaugeSpdMask(gc);
	this._face = new IDB.GaugeSpdFace(gc, yAxis.axisColor);
	
	if ( sett.getBool('GaugeRangeShow') ) {
		this._range = new IDB.GaugeSpdRange(gc, this._minVal, this._maxVal, 
			dpGrid.getRangeList(this._yAxisCol, true));
	}
	
	if ( sett.getBool('GaugeTickShow') ) {
		this._ticks = new IDB.GaugeSpdTicks(gc, this._minVal, this._maxVal, 
			this._divMetrics, yAxis.numFmt);
	}
	
	this._border = new IDB.GaugeSpdBorder(gc);
			
	if ( this._targetMode ) {
		var targAxis = dpGrid.getYAxis(this._yTargetCol, true);

		this._target = new IDB.GaugeSpdTarget(gc, this._minVal, this._maxVal, targAxis.axisName);
	}
	
	var i, needle, needleYAxis;
	
	for ( i = 0 ; i < this._numNeedles ; ++i ) {
		needleYAxis = dpGrid.getYAxis(this._yAxisCol+i, true);
		
		needle = new IDB.GaugeSpdNeedle(gc, this._minVal, this._maxVal, i, needleYAxis.axisColor);
		this._needles[i] = needle;
	}
	
	this._cover = new IDB.GaugeSpdCover(gc);
	
	if ( sett.get('GaugeAxisLabelOptions').show ) {
		this._label = new IDB.GaugeSpdLabel(gc, this._labelOnFace, yAxis.axisName);
	}
	
	if ( this._targetMode && sett.getBool('GaugeTargetOdomShow') ) {
		var nr = dpGrid.getNumXRows();
		var max = 100; //at least 3 digits (for 100%)
		var val, targVal;
		
		for ( i = 0 ; i < nr ; ++i ) {
			val = dpGrid.getDatapointAt(i, this._yAxisCol, true).yValue.numberValue;
			targVal = dpGrid.getDatapointAt(i, this._yTargetCol, true).yValue.numberValue;

			if ( !targVal ) {
				continue; //don't divide by zero
			}

			max = Math.max(max, Math.abs(val/targVal*100));
		}
		
		var ip = IDB.GraphOdom.getIntegerPlacesByValue(max);
		this._odom = new IDB.GraphOdom(sett, ip, null, true);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype._calcMinMaxVals = function() {
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var rangeList = dpGrid.getRangeList(this._yAxisCol, true);
	var i;
	
	var min = rangeList[0].val;
	var max = rangeList[rangeList.length-1].val;
	max += (max-min)/10000; //ensure that the final range color is shown
	
	if ( this._targetMode ) {
		min = Math.min(min, 99.9);
		max = Math.max(max, 100.1);
				
		var rowCount = dpGrid.getNumXRows();
		var dp, tdp, val;
		
		for ( i = 0 ; i < rowCount ; ++i ) {
			dp  = dpGrid.getDatapointAt(i, this._yAxisCol, true);
			tdp = dpGrid.getDatapointAt(i, this._yTargetCol, true);
					
			if ( tdp.yValue.numberValue ) {
				val = dp.yValue.numberValue/tdp.yValue.numberValue*100;
				min = Math.min(min, val);
				max = Math.max(max, val);
			}
		}
	}
	else {
		var ax, ss;

		for ( i = 0 ; i < this._numNeedles ; ++i ) {
			ax = dpGrid.getYAxis(this._yAxisCol+i, true);
			ss = dpGrid.getStatSet(ax.axisId);
			min = Math.min(min, ss.min);
			max = Math.max(max, ss.max);
		}
	}
			
	if ( min == max ) {
		if ( min > 0 ) {
			min = 0;
		}
		else if ( max < 0 ) {
			max = 0;
		}
		else {
			max = 10;
		}
	}
	
	var numDivs = gc.settings.get('GaugeTickNumMajors')+1;
	this._divMetrics = IDB.Calculator.getDivisionMetrics(min, max, numDivs);
	var dm = this._divMetrics.largeDivisions;
	this._minVal = dm.getMinBoundary();
	this._maxVal = dm.getMaxBoundary();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.resetNeedle = function() {
	var count = this._numNeedles;
	var i;

	for ( i = 0 ; i < count ; ++i ) {
		this._currValues[i] = {
			asNumber: this._minVal,
			asText: ''
		};
	}

	this._targetValue = {
		asNumber: 0,
		asText: ''
	};

	this._reset = true;
	this._datapoint = null;
	this._updateValue();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.setXRowIndex = function(index) {
	this._xRowIndex = index;
	this._reset = false;
	
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var count = this._numNeedles;
	var i, dv;
	
	this._datapoint = dpGrid.getDatapointAt(index, this._yAxisCol, true);
	
	for ( i = 0 ; i < count ; ++i ) {
		dv = dpGrid.getDatapointAt(index, this._yAxisCol+i, true).yValue;

		this._currValues[i] = {
			asNumber: dv.numberValue,
			asText: dv.getFV(true)
		};
	}
	
	if ( this._targetMode ) {
		var targDv = dpGrid.getDatapointAt(index, this._yTargetCol, true).yValue;

		this._targetValue = {
			asNumber: targDv.numberValue,
			asText: targDv.getFV(true)
		};
	}
	
	this._updateValue();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype._updateValue = function() {
	var count = this._numNeedles;
	var i;

	for ( i = 0 ; i < count ; ++i ) {
		this._needles[i].setValue(this._currValues[i], this._targetValue);
	}
			
	if ( this._targetMode ) {
		this._target.setValue(this._targetValue);
		
		if ( this._odom ) {
			this._odom.setCurrValue(this._reset ? 0 : 
				(this._currValues[0].asNumber/this._targetValue.asNumber)*100);
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getMask = function() {
	return this._mask;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getFace = function() {
	return this._face;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getRange = function() {
	return this._range;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getTicks = function() {
	return this._ticks;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getBorder = function() {
	return this._border;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getTarget = function() {
	return this._target;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getNeedleCount = function() {
	return this._needles.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getNeedle = function(index) {
	return this._needles[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getCover = function() {
	return this._cover;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getLabel = function() {
	return this._label;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getOdom = function() {
	return this._odom;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getAngle = function(percent) {
	return percent*(this._angleEnd-this._angleStart) + this._angleStart;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.getValueDiameter = function() {
	return this._graphContext.settings.get('ValueRadius')*2;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, '_handleClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.createOverListener = function() {
	return IDB.listener(this, '_handleOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype.createOutListener = function() {
	return IDB.listener(this, '_handleOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype._handleClick = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointClicked(this._datapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype._handleOver = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointOver(this._datapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpd.prototype._handleOut = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointOut(this._datapoint);
	}
};

/*====================================================================================================*/
IDB.GaugeSpdBorder = function(graphContext) {
	this._graphContext = graphContext;
};

IDB.GaugeSpdBorder.StyleFlat = 0;
IDB.GaugeSpdBorder.StyleChisled = 1;
IDB.GaugeSpdBorder.StyleTubular = 2;
IDB.GaugeSpdBorder.StyleClock = 3;
IDB.GaugeSpdBorder.StyleBand = 4;
IDB.GaugeSpdBorder.StyleEmbed = 5;
		
IDB.GaugeSpdBorder.FinishMatte = 0;
IDB.GaugeSpdBorder.FinishChrome = 1;
IDB.GaugeSpdBorder.FinishGloss = 2;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.isSquare = function() {
	return this._graphContext.settings.getBool('GaugeSquareMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getFullRadius = function() {
	return this._graphContext.settings.get('ValueRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getFaceRadius = function() {
	return this._graphContext.settings.get('ValueFaceRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getInnerRadius = function() {
	return this._graphContext.settings.get('ValueBorderInner');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getStyle = function() {
	return this._graphContext.settings.get('GaugeBorderStyle');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getFinish = function() {
	return this._graphContext.settings.get('GaugeBorderFinish');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getBackgroundColor = function() {
	return this._graphContext.settings.get('BackgroundColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getColor = function() {
	return this._graphContext.settings.get('GaugeBorderColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getThickness = function() {
	return this._graphContext.settings.get('GaugeBorderThick')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getMattingThickness = function() {
	return this._graphContext.settings.get('GaugeBorderMattingThick')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorder.prototype.getMattingColor = function() {
	return this._graphContext.settings.get('GaugeBorderMattingColor');
};

/*====================================================================================================*/
IDB.GaugeSpdBorderView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	if ( model.isSquare() ) {
		this._drawSquare();
	}
	else {
		this._drawRound();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawRound = function() {
	var model = this._model;
	var fullRad = model.getFullRadius();
	var innerRad = model.getInnerRadius();
	var faceRad = model.getFaceRadius();
	var style = model.getStyle();
	var wh = '255, 255, 255';
	var gr = '127, 127, 127';
	var bk = '0, 0, 0';
	var midRad, stops;
	
	if ( model.getMattingThickness() ) {
		var matting = new Kinetic.Ring({
			outerRadius: innerRad+(fullRad-innerRad)/2, //avoid seam between border and matting
			innerRadius: faceRad,
			fill: model.getMattingColor().css
		});
		this._display.add(matting);
	}

	var config = {
		outerRadius: fullRad,
		innerRadius: innerRad,
		fill: (style == IDB.GaugeSpdBorder.StyleEmbed ? 
			model.getBackgroundColor().css : model.getColor().css)
	};

	this._base = new Kinetic.Ring(config);
	this._display.add(this._base);

	switch ( style ) {
		case IDB.GaugeSpdBorder.StyleFlat:
			break;
			
		case IDB.GaugeSpdBorder.StyleChisled:
			midRad = innerRad+(fullRad-innerRad)/2;
			stops = [
				0.0, 'rgba('+wh+', 0.5)',
				0.5, 'rgba('+gr+', 0.2)',
				1.0, 'rgba('+bk+', 0.5)'
			];

			this._drawRoundGrad(fullRad, midRad, true, stops);
			this._drawRoundFinish(fullRad, midRad, 0, 0.5);

			this._drawRoundGrad(midRad, innerRad, true, stops, -1);
			this._drawRoundFinish(midRad, innerRad, 180, 0.5);
			break;
			
		case IDB.GaugeSpdBorder.StyleTubular:
			var rat = (1-innerRad/fullRad);
			var base = (1-rat);

			stops = [
				base, 'rgba('+bk+', 0.4)',
				base+0.21*rat, 'rgba('+wh+', 0.0)',
				base+0.5*rat, 'rgba('+wh+', 0.5)',
				base+0.79*rat, 'rgba('+wh+', 0.0)',
				base+rat, 'rgba('+bk+', 0.4)'
			];

			this._drawRoundGrad(fullRad, innerRad, false, stops);
			
			midRad = innerRad+(fullRad-innerRad)/2;
			this._drawRoundFinish(fullRad, midRad, 0, 0.35);
			this._drawRoundFinish(midRad, innerRad, 180, 0.35);
			break;
			
		case IDB.GaugeSpdBorder.StyleClock:
			midRad = innerRad+(fullRad-innerRad)*0.15;
			stops = [
				0.0, 'rgba('+wh+', 0.5)',
				0.5, 'rgba('+gr+', 0.25)',
				1.0, 'rgba('+bk+', 0.5)'
			];

			this._drawRoundGrad(fullRad, midRad, true, stops);
			this._drawRoundFinish(fullRad, midRad, 0, 0.5);
			
			stops = [
				0.0, 'rgba('+bk+', 0.7)',
				0.5, 'rgba('+gr+', 0.35)',
				1.0, 'rgba('+wh+', 0.7)'
			];

			this._drawRoundGrad(midRad, innerRad, true, stops);
			this._drawRoundFinish(midRad, innerRad, 180, 0.5);
			break;
			
		case IDB.GaugeSpdBorder.StyleBand:
			midRad = innerRad+(fullRad-innerRad)*0.8;
			var midRad2 = innerRad+(fullRad-innerRad)*0.2;

			stops = [
				0.0, 'rgba('+wh+', 0.5)',
				0.5, 'rgba('+gr+', 0.25)',
				1.0, 'rgba('+bk+', 0.5)'
			];

			this._drawRoundGrad(fullRad, midRad, true, stops);
			this._drawRoundFinish(fullRad, midRad, 0, 0.5);
			this._drawRoundGrad(midRad2, innerRad, true, stops, -1);
			
			stops = [
				0.0, 'rgba('+wh+', 0.1)',
				0.5, 'rgba('+gr+', 0.05)',
				1.0, 'rgba('+bk+', 0.1)'
			];

			this._drawRoundGrad(midRad, midRad2, true, stops);
			this._drawRoundFinish(midRad2, innerRad, 180, 0.5);
			break;
			
		case IDB.GaugeSpdBorder.StyleEmbed:
			stops = [
				0.0, 'rgba('+bk+', 0.3)',
				0.5, 'rgba('+gr+', 0.15)',
				1.0, 'rgba('+wh+', 0.3)'
			];

			this._drawRoundGrad(fullRad, innerRad, true, stops);
			this._drawRoundFinish(fullRad, innerRad, 180, 0.35);
			break;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawRoundGrad = function(outer, inner, isLinear, stops, startMult) {
	startMult = IDB.ifUndef(startMult, 1);

	var config = {
		outerRadius: outer,
		innerRadius: inner
	};

	if ( isLinear ) {
		config.fillLinearGradientStartPointY = -outer*startMult;
		config.fillLinearGradientEndPointY = outer*startMult;
		config.fillLinearGradientColorStops = stops;
	}
	else {
		config.fillRadialGradientEndRadius = outer;
		config.fillRadialGradientColorStops = stops;
	}
	
	var grad = new Kinetic.Ring(config);
	this._display.add(grad);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawRoundFinish = function(outer, inner, rot, opac) {
	var fin = this._model.getFinish();

	if ( fin == IDB.GaugeSpdBorder.FinishMatte ) {
		return;
	}

	if ( fin == IDB.GaugeSpdBorder.FinishChrome ) {
		this._drawRoundFinishChrome(outer, inner, rot, opac);
	}
	else {
		this._drawRoundFinishShine(outer, inner, rot, opac);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawRoundFinishChrome = function(outer, inner, rot, opac) {
	var config = {
		outerRadius: outer,
		innerRadius: inner,
		rotation: rot,
		opacity: opac
	};

	config.fillLinearGradientStartPointY = -outer;
	config.fillLinearGradientEndPointY = outer;
	config.fillLinearGradientColorStops = [
		0.0, 'rgba(255, 255, 255, 1.0)',
		0.31, 'rgba(0, 0, 0, 0.4)',
		0.63, 'rgba(255, 255, 255, 0.7)',
		0.88, 'rgba(0, 0, 0, 0.5)',
		1.0, 'rgba(255, 255, 255, 0.2)'
	];
	
	var chrome = new Kinetic.Ring(config);
	this._display.add(chrome);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawRoundFinishShine = function(outer, inner, rot, opac) {
	rot -= 90;
	
	var config = {
		outerRadius: outer,
		innerRadius: inner,
		angle: 84,
		rotation: rot-44,
		fill: 'rgba(255, 255, 255, 0.65)',
		opacity: opac
	};

	var shine = new Kinetic.Arc(config);
	this._display.add(shine);
	
	////

	config.angle = 132;
	config.rotation = rot+121;
	config.fill = 'rgba(0, 0, 0, 0.2)';

	shine = new Kinetic.Arc(config);
	this._display.add(shine);
	
	////

	config.angle = 63;
	config.rotation = rot+253;
	config.fill = 'rgba(255, 255, 255, 0.15)';

	shine = new Kinetic.Arc(config);
	this._display.add(shine);
	
	////

	config.angle = 81;
	config.rotation = rot+40;
	config.fill = 'rgba(0, 0, 0, 0.1)';

	shine = new Kinetic.Arc(config);
	this._display.add(shine);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawSquare = function() {
	var model = this._model;
	var fullRad = model.getFullRadius();
	var innerRad = model.getInnerRadius();
	var faceRad = model.getFaceRadius();
	var style = model.getStyle();
	var wh = '255, 255, 255';
	var gr = '127, 127, 127';
	var bk = '0, 0, 0';
	var midRad, stops;

	var config = {
		points: this._getSquarePoints(fullRad, innerRad),
		closed: true,
		fill: (style == IDB.GaugeSpdBorder.StyleEmbed ? 
			model.getBackgroundColor().css : model.getColor().css)
	};

	this._base = new Kinetic.Line(config);
	this._display.add(this._base);

	if ( model.getMattingThickness() ) {
		config.points = this._getSquarePoints(innerRad, faceRad),
		config.fill = model.getMattingColor().css;

		var matting = new Kinetic.Line(config);
		this._display.add(matting);
		matting.moveToBottom();
	}

	switch ( style ) {
		case IDB.GaugeSpdBorder.StyleFlat:
			break;
			
		case IDB.GaugeSpdBorder.StyleChisled:
			midRad = innerRad+(fullRad-innerRad)/2;
			stops = [
				0.0, 'rgba('+wh+', 0.5)',
				0.5, 'rgba('+gr+', 0.2)',
				1.0, 'rgba('+bk+', 0.5)'
			];

			this._drawSquareGrad(fullRad, midRad, stops);
			this._drawSquareFinish(fullRad, midRad, 0, 0.5);

			this._drawSquareGrad(midRad, innerRad, stops, -1);
			this._drawSquareFinish(midRad, innerRad, 180, 0.5);
			break;
			
		case IDB.GaugeSpdBorder.StyleTubular:
			stops = [
				0.0, 'rgba('+bk+', 0.4)',
				0.25, 'rgba('+wh+', 0.0)',
				0.5, 'rgba('+wh+', 0.5)',
				0.75, 'rgba('+wh+', 0.0)',
				1.0, 'rgba('+bk+', 0.4)'
			];

			this._drawSquareGradTubular(fullRad, innerRad, stops, 0);
			this._drawSquareGradTubular(fullRad, innerRad, stops, 90);
			this._drawSquareGradTubular(fullRad, innerRad, stops, 180);
			this._drawSquareGradTubular(fullRad, innerRad, stops, 270);
			
			midRad = innerRad+(fullRad-innerRad)/2;
			this._drawSquareFinish(fullRad, midRad, 0, 0.4);
			this._drawSquareFinish(midRad, innerRad, 180, 0.4);
			break;
			
		case IDB.GaugeSpdBorder.StyleClock:
			midRad = innerRad+(fullRad-innerRad)*0.15;
			stops = [
				0.0, 'rgba('+wh+', 0.5)',
				0.5, 'rgba('+gr+', 0.25)',
				1.0, 'rgba('+bk+', 0.5)'
			];

			this._drawSquareGrad(fullRad, midRad, stops);
			this._drawSquareFinish(fullRad, midRad, 0, 0.5);
			
			stops = [
				0.0, 'rgba('+bk+', 0.7)',
				0.5, 'rgba('+gr+', 0.35)',
				1.0, 'rgba('+wh+', 0.7)'
			];

			this._drawSquareGrad(midRad, innerRad, stops);
			this._drawSquareFinish(midRad, innerRad, 180, 0.5);
			break;
			
		case IDB.GaugeSpdBorder.StyleBand:
			midRad = innerRad+(fullRad-innerRad)*0.8;
			var midRad2 = innerRad+(fullRad-innerRad)*0.2;

			stops = [
				0.0, 'rgba('+wh+', 0.5)',
				0.5, 'rgba('+gr+', 0.25)',
				1.0, 'rgba('+bk+', 0.5)'
			];

			this._drawSquareGrad(fullRad, midRad, stops);
			this._drawSquareFinish(fullRad, midRad, 0, 0.5);
			this._drawSquareGrad(midRad2, innerRad, stops, -1);
			
			stops = [
				0.0, 'rgba('+wh+', 0.1)',
				0.5, 'rgba('+gr+', 0.05)',
				1.0, 'rgba('+bk+', 0.1)'
			];

			this._drawSquareGrad(midRad, midRad2, stops);
			this._drawSquareFinish(midRad2, innerRad, 180, 0.5);
			break;
			
		case IDB.GaugeSpdBorder.StyleEmbed:
			stops = [
				0.0, 'rgba('+bk+', 0.3)',
				0.5, 'rgba('+gr+', 0.15)',
				1.0, 'rgba('+wh+', 0.3)'
			];

			this._drawSquareGrad(fullRad, innerRad, stops);
			this._drawSquareFinish(fullRad, innerRad, 180, 0.35);
			break;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._getSquarePoints = function(outer, inner) {
	return [
		-outer, -outer,
		 outer, -outer,
		 outer,  outer,
		-outer,  outer,
		-outer, -outer,
		-inner, -inner,
		-inner,  inner,
		 inner,  inner,
		 inner, -inner,
		-inner, -inner
	];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawSquareGrad = function(outer, inner, stops, startMult) {
	startMult = IDB.ifUndef(startMult, 1);

	var config = {
		points: this._getSquarePoints(outer, inner),
		closed: true,
		fillLinearGradientStartPointY: -outer*startMult,
		fillLinearGradientEndPointY: outer*startMult,
		fillLinearGradientColorStops: stops
	};
	
	var grad = new Kinetic.Line(config);
	this._display.add(grad);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawSquareGradTubular = function(outer, inner, stops, rotation) {
	var config = {
		points: [
			-outer, -outer,
			 outer, -outer,
			 inner, -inner,
			-inner, -inner
		],
		closed: true,
		rotation: rotation
	};

	config.fillLinearGradientStartPointY = -outer;
	config.fillLinearGradientEndPointY = -inner;
	config.fillLinearGradientColorStops = stops;
	
	var grad = new Kinetic.Line(config);
	this._display.add(grad);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawSquareFinish = function(outer, inner, rot, opac) {
	var fin = this._model.getFinish();

	if ( fin == IDB.GaugeSpdBorder.FinishMatte ) {
		return;
	}

	if ( fin == IDB.GaugeSpdBorder.FinishChrome ) {
		this._drawSquareFinishChrome(outer, inner, rot, opac);
	}
	else {
		this._drawSquareFinishShine(outer, inner, rot, opac);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawSquareFinishChrome = function(outer, inner, rot, opac) {
	var config = {
		points: this._getSquarePoints(outer, inner),
		closed: true,
		rotation: rot,
		opacity: opac
	};

	config.fillLinearGradientStartPointY = -outer;
	config.fillLinearGradientEndPointY = outer;
	config.fillLinearGradientColorStops = [
		0.0, 'rgba(255, 255, 255, 1.0)',
		0.31, 'rgba(0, 0, 0, 0.4)',
		0.63, 'rgba(255, 255, 255, 0.7)',
		0.88, 'rgba(0, 0, 0, 0.5)',
		1.0, 'rgba(255, 255, 255, 0.2)'
	];
	
	var chrome = new Kinetic.Line(config);
	this._display.add(chrome);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype._drawSquareFinishShine = function(outer, inner, rot, opac) {
	var config = {
		points: [
			-outer, -outer,
			 outer, -outer,
			 inner, -inner,
			-inner, -inner
		],
		closed: true,
		fill: 'rgba(255, 255, 255, 0.75)',
		rotation: rot,
		opacity: opac
	};

	var shine = new Kinetic.Line(config);
	this._display.add(shine);

	////

	config.fill = 'rgba(0, 0, 0, 0.3)';
	config.rotation += 180;

	shine = new Kinetic.Line(config);
	this._display.add(shine);
	
	////

	config.points = [
		-outer, -outer,
		-inner, -inner,
		-inner, inner/3,
		-outer, outer/3
	];
	config.fill = 'rgba(255, 255, 255, 0.3)';
	config.rotation = rot;
	
	////

	shine = new Kinetic.Line(config);
	this._display.add(shine);
	
	config.points = [
		 outer, -outer,
		 inner, -inner,
		 inner, inner/2,
		 outer, outer/2
	];
	config.fill = 'rgba(0, 0, 0, 0.2)';

	shine = new Kinetic.Line(config);
	this._display.add(shine);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdBorderView.prototype.setScale = function(scale) {
	var model = this._model;

	if ( model.getStyle() == IDB.GaugeSpdBorder.StyleFlat ) {
		return;
	}

	var st = scale*(model.getThickness()+0.1);

	this._base
		.shadowEnabled(true)
		.shadowOffset({ x: 2*st, y: 23*st })
		.shadowBlur(24*st)
		.shadowOpacity(0.3);
};

/*====================================================================================================*/
IDB.GaugeSpdCover = function(graphContext) {
	this._graphContext = graphContext;
};

IDB.GaugeSpdCover.StyleNone = 0;
IDB.GaugeSpdCover.StyleFlat = 1;
IDB.GaugeSpdCover.StyleBubble = 2;
IDB.GaugeSpdCover.StyleBubbleGloss = 3;
IDB.GaugeSpdCover.StyleGlobeGloss = 4;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCover.prototype.isSquare = function() {
	return this._graphContext.settings.getBool('GaugeSquareMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCover.prototype.getStyle = function() {
	return this._graphContext.settings.get('GaugeCoverStyle');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCover.prototype.getRadius = function() {
	return this._graphContext.settings.get('ValueBorderInner');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCover.prototype.getOpacity = function() {
	return this._graphContext.settings.get('GaugeCoverIntensity')/100;
};

/*====================================================================================================*/
IDB.GaugeSpdCoverView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCoverView.prototype._buildKinetic = function() {
	var model = this._model;
	var isSquare = model.isSquare();

	this._display = new Kinetic.Group();

	this._drawMask();

	this._cover = new Kinetic.Group();
	this._cover.opacity(model.getOpacity());
	//LATER: use something like Flash's "BlendMode.LAYER" once KineticJS supports it
	this._display.add(this._cover);

	if ( isSquare ) {
		this._drawSquare();
	}
	else {
		this._drawRound();
	}

	//LATER: cacheAsBitmap option in KineticJS?
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCoverView.prototype._drawMask = function() {
	var model = this._model;
	var rad = model.getRadius();
	var isSquare = model.isSquare();
	var mask;

	switch ( model.getStyle() ) {
		case IDB.GaugeSpdCover.StyleNone:
		case IDB.GaugeSpdCover.StyleFlat:
		case IDB.GaugeSpdCover.StyleBubble:
			return;
	}

	if ( isSquare ) {
		mask = new Kinetic.Rect({
			x: -rad,
			y: -rad,
			width: rad*2,
			height: rad*2
		});
		this._display.add(mask);
	}
	else {
		mask = new Kinetic.Circle({
			radius: rad
		});
		this._display.add(mask);
	}

	IDB.KineticUtil.setMask(this._display, mask);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCoverView.prototype._drawRound = function() {
	var model = this._model;
	var rad = model.getRadius();
	var wh = '255, 255, 255';
	var gr = '127, 127, 127';
	var gr2 = '153, 153, 153';
	var bk = '0, 0, 0';
	var config, shape;
	
	var bubbleConfig = {
		radius: rad,
		fillRadialGradientStartPointY: -rad*0.433,
		fillRadialGradientEndPointY: -rad*0.433,
		fillRadialGradientEndRadius: rad*1.5,
		fillRadialGradientColorStops: [
			0.0, 'rgba('+wh+', 1.0)',
			0.5, 'rgba('+gr+', 0.5)',
			1.00, 'rgba('+bk+', 0.75)'
		]
	};

	switch ( model.getStyle() ) {
		case IDB.GaugeSpdCover.StyleNone:
			break;
					
		case IDB.GaugeSpdCover.StyleFlat:
			config = {
				radius: rad,
				fillLinearGradientStartPointY: -rad,
				fillLinearGradientEndPointY: rad,
				fillLinearGradientColorStops: [
					0.0, 'rgba('+wh+', 1.0)',
					0.5, 'rgba('+gr2+', 0.5)',
					1.0, 'rgba('+bk+', 0.75)'
				]
			};

			shape = new Kinetic.Circle(config);
			this._cover.add(shape);
			break;
					
		case IDB.GaugeSpdCover.StyleBubble:
			bubbleConfig.fillRadialGradientColorStops[2] = 0.64;
			bubbleConfig.fillRadialGradientColorStops[3] = 'rgba('+gr2+', 0.5)';

			shape = new Kinetic.Circle(bubbleConfig);
			this._cover.add(shape);
			break;
				
		case IDB.GaugeSpdCover.StyleBubbleGloss:
			shape = new Kinetic.Circle(bubbleConfig);
			this._cover.add(shape);

			config = {
				x: -rad*0.1,
				y: -rad*0.65,
				radius: {
					x: rad,
					y: rad*0.5
				},
				fillLinearGradientStartPointY: -rad*0.5,
				fillLinearGradientEndPointY: rad*0.5,
				fillLinearGradientColorStops: [
					0.0, 'rgba('+wh+', 0.25)',
					1.0, 'rgba('+wh+', 0.8)'
				]
			};

			shape = new Kinetic.Ellipse(config);
			this._cover.add(shape);
			
			config = {
				x: -rad*0.02,
				y: rad*0.95,
				radius: {
					x: rad*1.25,
					y: rad*0.5
				},
				fillLinearGradientStartPointY: -rad*0.5,
				fillLinearGradientEndPointY: rad*0.5,
				fillLinearGradientColorStops: [
					0.0, 'rgba('+wh+', 0.3)',
					1.0, 'rgba('+wh+', 0.05)'
				]
			};

			shape = new Kinetic.Ellipse(config);
			this._cover.add(shape);
			break;
					
		case IDB.GaugeSpdCover.StyleGlobeGloss:
			shape = new Kinetic.Circle(bubbleConfig);
			this._cover.add(shape);

			config = {
				x: -rad*0.096,
				y: -rad*0.385,
				innerRadius: rad*0.65,
				scaleX: 2,
				fillLinearGradientColorStops: [
					0.0, 'rgba('+bk+', 0.7)',
					1.0, 'rgba(170, 170, 170, 0.1)'
				]
			};
			
			config.outerRadius = rad-config.y;
			config.fillLinearGradientStartPointY = -config.y;
			config.fillLinearGradientEndPointY = config.outerRadius;

			shape = new Kinetic.Ring(config);
			this._cover.add(shape);
			
			config.radius = config.innerRadius;
			config.fillLinearGradientStartPointY = -config.radius;
			config.fillLinearGradientEndPointY = config.radius;
			config.fillLinearGradientColorStops = [
				0.0, 'rgba('+gr+', 0.3)',
				1.0, 'rgba('+wh+', 0.65)'
			];

			shape = new Kinetic.Circle(config);
			this._cover.add(shape);
			break;
	}
};


/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCoverView.prototype._drawSquare = function() {
	var model = this._model;
	var rad = model.getRadius();
	var wh = '255, 255, 255';
	var gr = '127, 127, 127';
	var gr2 = '153, 153, 153';
	var bk = '0, 0, 0';
	var config, shape;
	
	var bubbleConfig = {
		x: -rad,
		y: -rad,
		width: rad*2,
		height: rad*2,
		fillRadialGradientStartPointX: rad,
		fillRadialGradientStartPointY: rad*0.433,
		fillRadialGradientEndPointX: rad,
		fillRadialGradientEndPointY: rad*0.433,
		fillRadialGradientEndRadius: rad*2,
		fillRadialGradientColorStops: [
			0.0, 'rgba('+wh+', 1.0)',
			0.5, 'rgba('+gr+', 0.5)',
			1.00, 'rgba('+bk+', 0.75)'
		]
	};

	switch ( model.getStyle() ) {
		case IDB.GaugeSpdCover.StyleNone:
			break;
					
		case IDB.GaugeSpdCover.StyleFlat:
			config = {
				x: -rad,
				y: -rad,
				width: rad*2,
				height: rad*2,
				fillLinearGradientStartPointY: 0,
				fillLinearGradientEndPointY: rad*2,
				fillLinearGradientColorStops: [
					0.0, 'rgba('+wh+', 1.0)',
					0.5, 'rgba('+gr2+', 0.5)',
					1.0, 'rgba('+bk+', 0.75)'
				]
			};

			shape = new Kinetic.Rect(config);
			this._cover.add(shape);
			break;
					
		case IDB.GaugeSpdCover.StyleBubble:
			bubbleConfig.fillRadialGradientColorStops[2] = 0.64;
			bubbleConfig.fillRadialGradientColorStops[3] = 'rgba('+gr2+', 0.5)';

			shape = new Kinetic.Rect(bubbleConfig);
			this._cover.add(shape);
			break;
				
		case IDB.GaugeSpdCover.StyleBubbleGloss:
			shape = new Kinetic.Rect(bubbleConfig);
			this._cover.add(shape);

			config = {
				x: -rad*0.2,
				y: -rad*0.8,
				radius: {
					x: rad*1.5,
					y: rad*0.5
				},
				fillLinearGradientStartPointY: -rad*0.5,
				fillLinearGradientEndPointY: rad*0.5,
				fillLinearGradientColorStops: [
					0.0, 'rgba('+wh+', 0.25)',
					1.0, 'rgba('+wh+', 0.8)'
				]
			};

			shape = new Kinetic.Ellipse(config);
			this._cover.add(shape);
			
			config = {
				x: rad*0.05,
				y: rad*1.05,
				radius: {
					x: rad*1.75,
					y: rad*0.5
				},
				fillLinearGradientStartPointY: -rad*0.5,
				fillLinearGradientEndPointY: rad*0.5,
				fillLinearGradientColorStops: [
					0.0, 'rgba('+wh+', 0.3)',
					1.0, 'rgba('+wh+', 0.05)'
				]
			};

			shape = new Kinetic.Ellipse(config);
			this._cover.add(shape);
			break;
					
		case IDB.GaugeSpdCover.StyleGlobeGloss:
			shape = new Kinetic.Rect(bubbleConfig);
			this._cover.add(shape);

			config = {
				x: -rad*0.16,
				y: -rad*0.64,
				innerRadius: rad*0.9,
				scaleX: 2,
				fillLinearGradientColorStops: [
					0.0, 'rgba('+bk+', 0.7)',
					1.0, 'rgba(170, 170, 170, 0.1)'
				]
			};
			
			config.outerRadius = rad-config.y;
			config.fillLinearGradientStartPointY = -config.y;
			config.fillLinearGradientEndPointY = config.outerRadius;

			shape = new Kinetic.Ring(config);
			this._cover.add(shape);
			
			config.radius = config.innerRadius;
			config.fillLinearGradientStartPointY = -config.radius;
			config.fillLinearGradientEndPointY = config.radius;
			config.fillLinearGradientColorStops = [
				0.0, 'rgba('+gr+', 0.3)',
				1.0, 'rgba('+wh+', 0.65)'
			];

			shape = new Kinetic.Circle(config);
			this._cover.add(shape);
			break;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdCoverView.prototype.getDisplay = function() {
	return this._display;
};

/*====================================================================================================*/
IDB.GaugeSpdFace = function(graphContext, axisColor) {
	this._graphContext = graphContext;
	this._axisColor = axisColor;
};

IDB.GaugeSpdFace.FaceStyleSolid = 0;
IDB.GaugeSpdFace.FaceStyleFade = 1;
IDB.GaugeSpdFace.FaceStyleRadial = 2;
IDB.GaugeSpdFace.FaceStyleCircle = 3;
IDB.GaugeSpdFace.FaceStyleChrome = 4;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFace.prototype.isSquare = function() {
	return this._graphContext.settings.getBool('GaugeSquareMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFace.prototype.getStyle = function() {
	return this._graphContext.settings.get('GaugeFaceStyle');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFace.prototype.getFullRadius = function() {
	return this._graphContext.settings.get('ValueRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFace.prototype.getFaceRadius = function() {
	return this._graphContext.settings.get('ValueFaceRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFace.prototype.getBackgroundColor = function() {
	var sett = this._graphContext.settings;
	return (sett.getBool('GaugeFaceColorUseAxis') ? this._axisColor : sett.get('GaugeFaceColor'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFace.prototype.getEffectColor = function() {
	return this._graphContext.settings.get('GaugeFaceEffectColor');
};

/*====================================================================================================*/
IDB.GaugeSpdFaceView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFaceView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	var fullRad = model.getFullRadius();
	var faceRad = model.getFaceRadius();
	var gradRad = faceRad*(model.isSquare() ? 1.415 : 1.01);
	var col = model.getBackgroundColor().css;
	var effCol = model.getEffectColor().css;
	var shape;

	if ( model.isSquare() ) {
		var rectR = fullRad*0.99;

		shape = new Kinetic.Line({
			points: [
				-rectR, -rectR,
				 rectR, -rectR,
				 rectR,  rectR,
				-rectR,  rectR
			],
			closed: true
		});
	}
	else {
		shape = new Kinetic.Circle({
			radius: gradRad
		});
	}
	
	this._display.add(shape);

	switch ( model.getStyle() ) {
		case IDB.GaugeSpdFace.FaceStyleSolid:
			shape.fill(col);
			break;
			
		case IDB.GaugeSpdFace.FaceStyleFade:
			shape.fillLinearGradientStartPointY(-gradRad);
			shape.fillLinearGradientEndPointY(gradRad);
			shape.fillLinearGradientColorStops([0, col, 1, effCol]);
			break;
			
		case IDB.GaugeSpdFace.FaceStyleRadial:
			shape.fillRadialGradientEndRadius(gradRad);
			shape.fillRadialGradientColorStops([0, effCol, 1, col]);
			break;
			
		case IDB.GaugeSpdFace.FaceStyleCircle:
			shape.fill(col);

			var inner = new Kinetic.Circle({
				radius: faceRad/2,
				fill: effCol
			});
			this._display.add(inner);
			break;
			
		case IDB.GaugeSpdFace.FaceStyleChrome:
			shape.fill(col);

			var r2 = gradRad+2;
			var step = Math.PI*2/180;
			var fadeColor = IDB.toRGBA(model.getBackgroundColor(), 0);
			var ccos = IDB.MathCache.cos;
			var csin = IDB.MathCache.sin;
			var i, a, slice;
					
			for ( i = 0 ; i < 180 ; ++i ) {
				a = i*step;
				
				slice = new Kinetic.Line({
					closed: true,
					fill: this._getChromeColor(a),
					points: [
						0, 0,
						csin(a)*r2, -ccos(a)*r2,
						csin(a+step)*r2, -ccos(a+step)*r2
					]
				});
				this._display.add(slice);
			}

			var cover = new Kinetic.Circle({
				radius: r2,
				fillRadialGradientEndRadius: r2,
				fillRadialGradientColorStops: [0, col, 1, fadeColor]
			});
			this._display.add(cover);
			break;
	}

	//LATER: cacheAsBitmap option in KineticJS?
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFaceView.prototype._getChromeColor = function(angle) {
	var c = Math.floor(128+Math.sin(angle*4)*127);
	return 'rgba('+c+','+c+','+c+', 0.333)';
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdFaceView.prototype.getDisplay = function() {
	return this._display;
};

/*====================================================================================================*/
IDB.GaugeSpdLabel = function(graphContext, onFace, text) {
	this._graphContext = graphContext;
	this._onFace = onFace;
	this._text = text;

	var sett = graphContext.settings;
	var gtc = sett.get('GraphTypeCode');

	this._showBg = (sett.getBool('GaugeAxisLabelBgShow') && 
		(gtc.indexOf('Half') > 0 || gtc.indexOf('Target') > 0));
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabel.prototype.showOnFace = function() {
	return this._onFace;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabel.prototype.getText = function() {
	return this._text;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabel.prototype.showBackground = function() {
	return this._showBg;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabel.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('GaugeAxisLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabel.prototype.getBackgroundColor = function() {
	return this._graphContext.settings.get('GaugeAxisLabelBgColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabel.prototype.getBackgroundBorderColor = function() {
	return this._graphContext.settings.get('GaugeAxisLabelBgBordColor');
};

/*====================================================================================================*/
IDB.GaugeSpdLabelView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabelView.prototype._buildKinetic = function() {
	var model = this._model;
	var lblOpt = model.getLabelOptions();

	this._display = new Kinetic.Group();

	if ( model.showBackground() ) {
		this._bg = new Kinetic.Rect({
			fill: model.getBackgroundColor().css,
			stroke: model.getBackgroundBorderColor().css,
			strokeWidth: 3
		});
		this._display.add(this._bg);
	}

	lblOpt.align = 'center';

	this._label = new IDB.GraphLabel(lblOpt, model.getText());
	this._display.add(this._label.getDisplay());
	this._padLabelH = this._label.height+8;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabelView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabelView.prototype.getLabelH = function() {
	return this._padLabelH+(this._bg ? 6 : 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdLabelView.prototype.setWidth = function(width) {
	var model = this._model;
	var lbl = this._label;
	var bg = this._bg;
	var lw = width*(bg ? 0.75 : 1);
	var lh = this._label.height;

	lbl.resetText();
	
	if ( model.showOnFace() ) {
		lbl.width = Math.min(lbl.textWidth, width/3);
	}
	else {
		lbl.width = Math.min(lbl.textWidth, lw);
	}
	
	lbl.truncate();
	lbl.x = (width-lbl.width)/2;
	
	if ( !bg ) {
		lbl.y = -lh/2 + (this._padLabelH-lh)/2;
		return;
	}

	lbl.y = -lh/2+1;
	lh = this._padLabelH;

	var bgW = Math.max(lbl.width, width*0.75);
	var bgH = lh+6;
	var diffH = bgH-lh;
	
	lbl.y -= diffH/2;
	
	bg.x((width-bgW)/2);
	bg.y(-lh/2-diffH);
	bg.width(bgW);
	bg.height(bgH);
	bg.cornerRadius(bgH/2);
};

/*====================================================================================================*/
IDB.GaugeSpdMask = function(graphContext) {
	this._graphContext = graphContext;

	var sett = graphContext.settings;
	this._angleStart = sett.get('GaugeAngleStart');
	this._angleEnd = sett.get('GaugeAngleEnd');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.useFullMask = function() {
	return (this._angleEnd-this._angleStart > 180);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.getAngleStart = function() {
	return this._angleStart;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.getAngleEnd = function() {
	return this._angleEnd;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.getRadius = function() {
	return this._graphContext.settings.get('ValueRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.getPinRadius = function() {
	return this._graphContext.settings.get('ValueNeedlePinRad');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.getBaseThickness = function() {
	return this._graphContext.settings.get('GaugeBaseThick')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.getNeedleThickness = function() {
	return this._graphContext.settings.get('GaugeNeedleThickness')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.getNeedleLength = function() {
	return this._graphContext.settings.get('GaugeNeedleLength')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.hasNeedleExt = function() {
	var s = this._graphContext.settings.get('GaugeNeedleExtShape');
	return (s != IDB.GaugeSpdNeedle.ExtStyleNone && s != IDB.GaugeSpdNeedle.ExtStyleRound);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMask.prototype.isSquare = function() {
	return this._graphContext.settings.getBool('GaugeSquareMode');
};

/*====================================================================================================*/
IDB.GaugeSpdMaskView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype._buildKinetic = function() {
	this._minX = 0;
	this._minY = 0;
	this._maxX = 0;
	this._maxY = 0;
	this._points = [];

	var model = this._model;

	if ( model.useFullMask() ) {
		this._buildFullMask();
	}
	else {
		this._buildPartialMask();
	}
};
	
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype._buildFullMask = function() {
	var model = this._model;
	var rad = model.getRadius();

	this._addPoint(-rad, -rad);
	this._addPoint( rad, -rad);
	this._addPoint( rad,  rad);
	this._addPoint(-rad,  rad);

	if ( model.isSquare() ) {
		this._display = new Kinetic.Rect({
			x: -rad,
			y: -rad,
			width: rad*2,
			height: rad*2
		});
	}
	else {
		this._display = new Kinetic.Circle({
			radius: rad
		});
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype._buildPartialMask = function() {
	var model = this._model;
	var ccos = IDB.MathCache.cos;
	var csin = IDB.MathCache.sin;

	var rad = model.getRadius();
	var baseThick = rad*model.getBaseThickness();
	var thickH = rad*0.1*model.getNeedleThickness();
	var pivLen = Math.max(baseThick, thickH*1.75);
	var basePercOfCircum = baseThick/(Math.PI*rad);

	//LATER: the angles along the base of this mask don't always match 100% with Flash

	var a0 = IDB.MathUtil.toRadians(model.getAngleStart());
	var a1 = IDB.MathUtil.toRadians(model.getAngleEnd());
	var aRange = a1-a0;
	var aPiv = a0+aRange/2+Math.PI;
	var pivot = { x: csin(aPiv)*pivLen, y: -ccos(aPiv)*pivLen };
	var extend = basePercOfCircum*Math.PI*(aRange/Math.PI*0.45+0.55);

	a0 -= extend;
	a1 += extend;

	this._addPoint(pivot.x, pivot.y);
	this._addPoint(csin(a0)*rad, -ccos(a0)*rad);
	
	for ( var a = a0 ; a < a1 ; a += 0.05 ) {
		this._addPoint(csin(a)*rad, -ccos(a)*rad);
	}

	this._addPoint(csin(a1)*rad, -ccos(a1)*rad);
	this._addPoint(pivot.x, pivot.y);

	////

	var rad2 = Math.max(model.getPinRadius(), thickH*2);

	if ( model.hasNeedleExt() ) {
		rad2 = Math.max(rad2, rad*0.28*model.getNeedleLength());
	}

	if ( rad2 > pivot.y ) {
		for ( a = 0 ; a < Math.PI*2 ; a += 0.1 ) {
			this._addPoint(csin(a)*rad2, -ccos(a)*rad2);
		}

		this._addPoint(0, -rad2);
	}

	////
	
	this._display = new Kinetic.Line({
		points: this._points,
		closed: true
	});
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype._addPoint = function(x, y) {
	this._minX = Math.min(this._minX, x);
	this._minY = Math.min(this._minY, y);
	this._maxX = Math.max(this._maxX, x);
	this._maxY = Math.max(this._maxY, y);
	this._points.push(x, y);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype.getW = function() {
	return this._maxX-this._minX;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype.getH = function() {
	return this._maxY-this._minY;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype.getRelativeX = function() {
	return -this._minX/this.getW();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdMaskView.prototype.getRelativeY = function() {
	return -this._minY/this.getH();
};

/*====================================================================================================*/
IDB.GaugeSpdNeedle = function(graphContext, minVal, maxVal, needleIndex, axisColor) {
	this._graphContext = graphContext;
	this._minVal = minVal;
	this._maxVal = maxVal;
	this._needleIndex = needleIndex;
	this._axisColor = axisColor;

	var sett = graphContext.settings;

	this._angleStart = sett.get('GaugeAngleStart');
	this._angleEnd = sett.get('GaugeAngleEnd');
	this._angle = this._angleStart;
	this._text = ' ';
};

IDB.GaugeSpdNeedle.StyleTriangle = 0;
IDB.GaugeSpdNeedle.StyleFlatTip = 1;
IDB.GaugeSpdNeedle.StyleFlatTipTaper = 2;
IDB.GaugeSpdNeedle.StyleRoundTip = 3;
IDB.GaugeSpdNeedle.StyleRoundTipTaper = 4;
IDB.GaugeSpdNeedle.StyleTriTip = 5;
IDB.GaugeSpdNeedle.StyleTriTipTaper = 6;
IDB.GaugeSpdNeedle.StyleArrow = 7;
IDB.GaugeSpdNeedle.StyleOpenTip = 8;

IDB.GaugeSpdNeedle.ExtStyleNone = 0;
IDB.GaugeSpdNeedle.ExtStyleRound = 1;
IDB.GaugeSpdNeedle.ExtStyleExtendFlat = 2;
IDB.GaugeSpdNeedle.ExtStyleExtendRound = 3;
IDB.GaugeSpdNeedle.ExtStyleTriangle = 4;
IDB.GaugeSpdNeedle.ExtStyleCircle = 5;
		
IDB.GaugeSpdNeedle.PinStyleNone = 0;
IDB.GaugeSpdNeedle.PinStylePin = 1;
IDB.GaugeSpdNeedle.PinStyleLayer = 2;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.setOnValueUpdate = function(callback) {
	this._onValueUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.setValue = function(value, targetValue) {
	var sett = this._graphContext.settings;
	var num = value.asNumber;

	if ( sett.getBool('GaugeTargetMode') ) {
		var targNum = targetValue.asNumber;
		num = (targNum ? num/targNum*100 : this._minVal);
	}

	var valPos = (num-this._minVal)/(this._maxVal-this._minVal);

	this._angle = valPos*(this._angleEnd-this._angleStart)+this._angleStart;
	this._text = value.asText;
	
	if ( this._onValueUpdate ) {
		this._onValueUpdate();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getAngle = function() {
	return this._angle;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getText = function() {
	return this._text;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getColor = function() {
	return (this.isMultiMode() ? this._axisColor : this._graphContext.settings.get('GaugeNeedleColor'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.isFirstNeedle = function() {
	return (this._needleIndex == 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.isLastNeedle = function() {
	return (this._needleIndex == this._graphContext.settings.get('GaugeNumNeedles')-1);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('GaugeNeedleLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getLength = function() {
	var sett = this._graphContext.settings;
	var len = sett.get('ValueNeedleLen');
	var cut = sett.get('ValueNeedleLenCut');
	return len*(1-this._needleIndex*cut);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getThickness = function() {
	return this._graphContext.settings.get('ValueNeedleThick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getShape = function() {
	return this._graphContext.settings.get('GaugeNeedleShape');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getExtensionLength = function() {
	return this._graphContext.settings.get('ValueNeedleExtLen');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getExtensionShape = function() {
	return this._graphContext.settings.get('GaugeNeedleExtShape');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getFaceRadius = function() {
	return this._graphContext.settings.get('ValueFaceRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.useShading = function() {
	return this._graphContext.settings.getBool('GaugeNeedleShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.isMultiMode = function() {
	return this._graphContext.settings.getBool('GaugeMultiNeedleMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getPinColor = function() {
	return this._graphContext.settings.get('GaugeNeedleBaseColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getPinStyle = function() {
	return this._graphContext.settings.get('GaugeNeedleBaseType');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedle.prototype.getPinRadius = function() {
	return this._graphContext.settings.get('ValueNeedlePinRad');
};
/* global IDB, $, Kinetic */
/*====================================================================================================*/
IDB.GaugeSpdNeedleView = function(model) {
	this._model = model;
	model.setOnValueUpdate(IDB.listener(this, '_onValueUpdate'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._buildKinetic = function() {
    var me = this, model = me._model, context = model ? model._graphContext : null,
        settings = context ? context.settings : null, animate = settings ? settings.animationEnabled() : false;

    me._animationEnabled = animate;

	this._display = new Kinetic.Group();

	this._spin = new Kinetic.Group();
	this._display.add(this._spin);

	this._buildShape();
	this._buildPin();
	
	var lblOpt = this._model.getLabelOptions();

	if ( lblOpt.show ) {
		lblOpt.align = 'center';

		this._label = new IDB.GraphLabel(lblOpt, ' ');
		this._spin.add(this._label.getDisplay());
	}

	this._onValueUpdate();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._buildShape = function() {
	var model = this._model;
	var shapeData = this._getShapeData();
	var extData = this._getExtensionData(shapeData);
	var drawData = this._mergeShapeData(shapeData, extData);

	var config = {
		points: drawData.points,
		closed: true,
		fill: model.getColor().css,
		strokeWidth: 0
	};

	this._shape = new Kinetic.Line(config);
	this._spin.add(this._shape);
	
	if ( model.useShading() ) {
		config.points = drawData.half;
		config.fill = '#000';
		config.opacity = 0;

		this._shade1 = new Kinetic.Line(config);
		this._spin.add(this._shade1);

		config.points = drawData.half2;

		this._shade2 = new Kinetic.Line(config);
		this._spin.add(this._shade2);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._buildPin = function() {
	var model = this._model;
	var style = model.getPinStyle();

	if ( style == IDB.GaugeSpdNeedle.PinStyleNone ) {
		return;
	}

	var isLayer = (style == IDB.GaugeSpdNeedle.PinStyleLayer);
	var size = model.getPinRadius();
	var pinTopSize = size*(isLayer ? 0.75 : 1.0);
	var showBot = (isLayer && model.isFirstNeedle());
	var showTop = model.isLastNeedle();

	if ( showBot ) {
		this._pinBot = new Kinetic.Circle({
			radius: size,
			fill: IDB.toRGBA(model.getPinColor(), 0.4)
		});
		this._display.add(this._pinBot);
		this._pinBot.moveToBottom(); //underneath the needle layer
	}

	if ( !showTop ) {
		return;
	}

	this._pinTop = new Kinetic.Circle({
		radius: pinTopSize,
		fill: model.getPinColor().css
	});
	this._display.add(this._pinTop);

	if ( model.useShading() ) {
		var innerR = pinTopSize*0.75;

		var outer = new Kinetic.Ring({
			outerRadius: pinTopSize,
			innerRadius: innerR,
			fillLinearGradientStartPointY: -pinTopSize,
			fillLinearGradientEndPointY: pinTopSize,
			fillLinearGradientColorStops: [
				0.0, 'rgba(255, 255, 255, 0.5)',
				0.5, 'rgba(127, 127, 127, 0.0)',
				1.0, 'rgba(0, 0, 0, 0.5)'
			]
		});
		this._display.add(outer);

		var inner = new Kinetic.Circle({
			radius: innerR,
			fillLinearGradientStartPointY: -innerR,
			fillLinearGradientEndPointY: innerR,
			fillLinearGradientColorStops: [
				0.0, 'rgba(255, 255, 255, 0.1)',
				0.5, 'rgba(127, 127, 127, 0.0)',
				1.0, 'rgba(0, 0, 0, 0.2)'
			]
		});
		this._display.add(inner);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._getShapeData = function() {
	var model = this._model;
	var tipLen = -model.getLength();
	var thickH = model.getThickness()/2;
	var thickQ = thickH/2;
	var style = model.getShape();
	var ccos = IDB.MathCache.cos;
	var csin = IDB.MathCache.sin;
	var a, tw, tip2;
	
	var data = {};
	data.half = [];
	data.taper = [0, -1];

	switch ( style ) {
		case IDB.GaugeSpdNeedle.StyleTriangle:
			data.half = [
				0, tipLen,
				thickH, 0,
				0, 0
			];

			data.taper = [ thickH, tipLen ];
			break;
			
		case IDB.GaugeSpdNeedle.StyleFlatTip:
		case IDB.GaugeSpdNeedle.StyleFlatTipTaper:
			tw = (style == IDB.GaugeSpdNeedle.StyleFlatTip ? thickH : thickQ);

			data.half = [
				0, tipLen,
				tw, tipLen,
				thickH, 0,
				0, 0
			];

			data.taper = [ thickH-tw, tipLen ];
			break;

		case IDB.GaugeSpdNeedle.StyleRoundTip:
		case IDB.GaugeSpdNeedle.StyleRoundTipTaper:
			tw = (style == IDB.GaugeSpdNeedle.StyleRoundTip ? thickH : thickQ);
			tip2 = tipLen+tw;
			
			for ( a = 0 ; a < Math.PI/2 ; a += 0.25 ) {
				data.half.push(csin(a)*tw, tipLen-(ccos(a)-1)*tw);
			}

			data.half.push(
				tw, tip2,
				thickH, 0,
				0, 0
			);
			
			data.taper = [ thickH-tw, tip2 ];
			break;
			
		case IDB.GaugeSpdNeedle.StyleTriTip:
		case IDB.GaugeSpdNeedle.StyleTriTipTaper:
			tw = (style == IDB.GaugeSpdNeedle.StyleTriTip ? thickH : thickQ);
			tip2 = tipLen+tw;

			data.half = [
				0, tipLen,
				tw, tip2,
				thickH, 0,
				0, 0
			];

			data.taper = [ thickH-tw, tip2 ];
			break;
			
		case IDB.GaugeSpdNeedle.StyleArrow:
			tip2 = tipLen+thickH*2;

			data.half = [
				0, tipLen,
				thickH, tip2,
				thickQ, tip2-thickQ/2,
				thickH, 0,
				0, 0
			];

			data.taper = [ thickH-thickQ, tip2-thickQ/2 ];
			break;

		case IDB.GaugeSpdNeedle.StyleOpenTip:
			tw = thickH*0.8;

			for ( a = Math.PI ; a >= 0 ; a -= 0.25 ) {
				data.half.push(csin(a)*tw, tipLen-(ccos(a)-1.25)*tw);
			}

			data.half.push(0, tipLen+thickH*0.2);

			for ( a = 0 ; a < Math.PI*0.99 ; a += 0.25 ) {
				data.half.push(csin(a)*thickH, tipLen-(ccos(a)-1)*thickH);
			}

			var lastI = data.half.length-2;
			data.taper = [ thickH-data.half[lastI], data.half[lastI+1] ];

			data.half.push(
				thickH, 0,
				0, 0
			);
			
			break;
	}

	return data;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._getExtensionData = function(needleData) {
	var model = this._model;
	var style = model.getExtensionShape();
	var extLen = model.getExtensionLength();
	var thickH = model.getThickness()/2;
	var ccos = IDB.MathCache.cos;
	var csin = IDB.MathCache.sin;
	var tp = needleData.taper;
	var tpLen = Math.sqrt(tp[0]*tp[0] + tp[1]*tp[1]);
	var a;

	tp = { x: -tp[0]/tpLen, y: -tp[1]/tpLen };

	var data = {};
	data.half = [];

	switch ( style ) {
		case IDB.GaugeSpdNeedle.ExtStyleNone:
			break;
			
		case IDB.GaugeSpdNeedle.ExtStyleRound:
			data.half.push(0, 0);

			for ( a = Math.PI/2 ; a >= 0  ; a -= 0.25 ) {
				data.half.push(csin(a)*thickH, ccos(a)*thickH);
			}

			data.half.push(0, thickH);
			break;
			
		case IDB.GaugeSpdNeedle.ExtStyleExtendFlat:
			data.half = [
				0, 0,
				thickH, 0,
				thickH-extLen*tp.x, extLen*tp.y,
				0, extLen*tp.y
			];
			break;

		case IDB.GaugeSpdNeedle.ExtStyleExtendRound:
			var r, py;
					
			if ( tp.x ) {
				var theta = -Math.asin(tp.x);
				var z = thickH/Math.tan(theta);
				var total = z+extLen;
				var solve = total/(Math.tan(theta)+1);
				r = total-solve;
				py = solve-z;
			}
			else {
				py = extLen-thickH;
				r = thickH;
			}

			data.half.push(0, 0);
			data.half.push(thickH, 0);

			for ( a = Math.PI/2 ; a >= 0  ; a -= 0.25 ) {
				data.half.push(csin(a)*r, py+ccos(a)*r);
			}

			data.half.push(0, py+r);
			break;
			
		case IDB.GaugeSpdNeedle.ExtStyleTriangle:
			data.half = [
				0, 0,
				thickH, 0,
				0, extLen
			];
			break;
			
		case IDB.GaugeSpdNeedle.ExtStyleCircle:
			r = thickH/2;

			data.half = [
				0, 0,
				thickH, 0
			];
			
			for ( a = Math.PI/6 ; a <= Math.PI  ; a += 0.25 ) {
				data.half.push(csin(a)*r, extLen-ccos(a)*r-r);
			}

			data.half.push(0, extLen);
			break;
	}

	return data;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._mergeShapeData = function(shapeData, extData) {
	var data = {};

	if ( extData.half.length > 0 ) {
		var n = shapeData.half.length;
		var shapeHalf = shapeData.half.slice(0, n-2);
		var extHalf = extData.half.slice(2, extData.half.length);
		data.half = shapeHalf.concat(extHalf);
	}
	else {
		data.half = shapeData.half;
	}
	
	data.half2 = this._getReversedPoints(data.half);
	data.points = data.half.concat(data.half2);
	return data;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._getReversedPoints = function(points) {
	var len = points.length;
	var rev = [];

	for ( var i = len-2 ; i >= 0 ; i -= 2 ) {
		rev.push(-points[i], points[i+1]);
	}

	return rev;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._onValueUpdate = function() {
	var me = this, model = me._model, getErrorStack = function getErrStack()
	{
		var stack = new Error().stack;
		if (stack===undefined)
		{
			try
			{
				throw new Error();
			}
			catch(e)
			{
				stack = e.stack;
			}
		}
		return stack;
	};

	if ( this._label ) {
		this._label.text = model.getText();
		this._updateLabelPosition();
    }

    if (!me._animationEnabled || (/GraphPages.*onPageUpdate/g.test(getErrorStack()) && this._spin.getRotation()===0))
    {
    	me._spin.setRotation(model.getAngle());
    	me._completeValueUpdate();
    	return;
    }
	/*Thomas wrote the next line*/
	/*for (var parentLayer = this._spin; 'nodeType' in parentLayer && parentLayer.nodeType!="Layer"; parentLayer = parentLayer.getParent());*/
	/*Kirsten rewrote the next line*/
	for (var parentLayer = this._spin; parentLayer && 'nodeType' in parentLayer && parentLayer.nodeType!="Layer"; parentLayer = parentLayer.getParent());
    var initialAngle = this._spin.rotation(),
        inst = this,
        angleDiff = model.getAngle() - initialAngle,
        animSpeed = 1000.00,
        needleAnim = new Kinetic.Animation(function(frame) {
            if (frame.time <= animSpeed) {
                var curAngle = initialAngle + $.easing.easeOutBack(frame.time / animSpeed) * angleDiff;
                inst._spin.rotation(curAngle);
                inst._completeValueUpdate(curAngle); // Update shadow during animation
            } else {
                inst._spin.rotation(model.getAngle());
                needleAnim.stop();

                inst._completeValueUpdate();
            }
        }, parentLayer);
    needleAnim.start();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._completeValueUpdate = function(overRideAngle)
{
	var me = this, model = me._model || {getAngle: function(){return 0;}};
	if (me._shade1) {
		var a = IDB.MathUtil.toRadians(overRideAngle !== undefined ? overRideAngle : model.getAngle()),
		    sina = Math.sin(a),
		    opac = Math.pow(Math.abs(sina), 0.6) * 0.2,
		    isSinaNegative = sina < 0,
		    isSinaPostivie = sina > 0,
		    shade1Fill = (isSinaNegative && '#fff') || '#000',
		    shade2Fill = (isSinaPostivie && '#fff') || '#000';

        
		me._shade1.opacity(opac).fill(shade1Fill);
		me._shade2.opacity(opac).fill(shade2Fill);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype._updateLabelPosition = function() {
	this._label.width = this._label.textWidth;
	this._label.x = -this._label.width/2;
	this._label.y = -this._model.getLength()-this._label.height;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdNeedleView.prototype.setScale = function(scale) {
	if ( this._shade1 ) {
		var off = { x: 0.5*scale, y: 3*scale };
		var b = 4*scale;
		var o = 0.3;

		this._shape.shadowEnabled(true).shadowOffset(off).shadowBlur(b).shadowOpacity(o);

		if ( this._label ) {
			var lblDisp = this._label.getDisplayLabel();
			lblDisp.shadowEnabled(true).shadowOffset(off).shadowBlur(b).shadowOpacity(o);
		}

		if ( this._pinTop ) {
			this._pinTop.shadowEnabled(true).shadowOffset(off).shadowBlur(b).shadowOpacity(o);
		}
	}

	if ( this._label ) {
		this._label.scaleX = this._label.scaleY = 1/scale;
		this._updateLabelPosition();
	}
};

/*====================================================================================================*/
IDB.GaugeSpdRange = function(graphContext, minVal, maxVal, ranges) {
	this._graphContext = graphContext;
	this._segments = [];
	
	var angleStart = this._getAngleStart();
	var angleEnd = this._getAngleEnd();
	var valueDiff = maxVal-minVal;
	var angleDiff = angleEnd-angleStart;
	var len = ranges.length;
	var i, lo, hi, range, nextRange;
			
	for ( i = 0 ; i < len ; ++i ) {
		range = ranges[i];
		nextRange = ranges[i+1];
		lo = (i == 0 ? minVal : range.val);
		hi = (i == len-1 ? maxVal : nextRange.val);

		this._segments.push({
			angle0: ((lo-minVal)/valueDiff)*angleDiff+angleStart,
			angle1: ((hi-minVal)/valueDiff)*angleDiff+angleStart,
			color: range.color
		});
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRange.prototype._getAngleStart = function() {
	return this._graphContext.settings.get('GaugeAngleStart');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRange.prototype._getAngleEnd = function() {
	return this._graphContext.settings.get('GaugeAngleEnd');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRange.prototype.getSegmentCount = function() {
	return this._segments.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRange.prototype.getSegment = function(index) {
	return this._segments[index];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRange.prototype.getFaceRadius = function() {
	return this._graphContext.settings.get('ValueFaceRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRange.prototype.getOuterRelativeRadius = function() {
	return this._graphContext.settings.get('GaugeRangeRadiusOuter')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRange.prototype.getInnerRelativeRadius = function() {
	return this._graphContext.settings.get('GaugeRangeRadiusInner')/100;
};

/*====================================================================================================*/
IDB.GaugeSpdRangeView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRangeView.prototype._buildKinetic = function() {
	var model = this._model;
	var numSegs = model.getSegmentCount();
	var rad = model.getFaceRadius()*1.005;
	var i, seg, shape;

	var config = {
		outerRadius: rad*model.getOuterRelativeRadius(),
		innerRadius: rad*model.getInnerRelativeRadius(),
		closed: true
	};

	this._display = new Kinetic.Group();

	for ( i = 0 ; i < numSegs ; ++i ) {
		seg = model.getSegment(i);

		config.fill = seg.color.css;
		config.angle = seg.angle1-seg.angle0;
		config.rotation = seg.angle0-90;

		shape = new Kinetic.Arc(config);
		this._display.add(shape);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdRangeView.prototype.getDisplay = function() {
	return this._display;
};

/*====================================================================================================*/
IDB.GaugeSpdTarget = function(graphContext, minVal, maxVal, axisName) {
	this._graphContext = graphContext;
	this._axisName = axisName;
	this._valueStr = null;

	var sett = graphContext.settings;
	var valueRange = maxVal-minVal;
	var angleRange = sett.get('GaugeAngleEnd')-sett.get('GaugeAngleStart');
	var pos = (100-minVal)/valueRange;

	this._angle = pos*angleRange + sett.get('GaugeAngleStart');
	this._outerRadius = sett.get('ValueFaceRadius')+1;
	this._innerRadius = this._outerRadius*sett.get('GaugeTargetLineRadius')/100;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.getAxisName = function() {
	return this._axisName;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.getValueString = function() {
	return this._valueStr;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.getAngle = function() {
	return this._angle;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.getOuterRadius = function() {
	return this._outerRadius;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.getInnerRadius = function() {
	return this._innerRadius;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.setOnValueUpdate = function(callback) {
	this._onValueUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.setValue = function(value) {
	this._valueStr = value.asText;
	
	if ( this._onValueUpdate ) {
		this._onValueUpdate();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.getLineColor = function() {
	return this._graphContext.settings.get('GaugeTargetLineColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('GaugeTargetLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTarget.prototype.getFullRadius = function() {
	return this._graphContext.settings.get('ValueRadius');
};

/*====================================================================================================*/
IDB.GaugeSpdTargetView = function(model, labelLayer) {
	this._model = model;
	this._labelLayer = labelLayer;
	model.setOnValueUpdate(IDB.listener(this, '_onValueUpdate'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTargetView.prototype._buildKinetic = function() {
	var model = this._model;
	var lblOpt = model.getLabelOptions();
	var ri = model.getInnerRadius();
	var ro = model.getOuterRadius();
	var a = IDB.MathUtil.toRadians(model.getAngle());
	var ax = Math.sin(a);
	var ay = -Math.cos(a);
	var lineX = ax*ro;

	lblOpt.align = 'center';

	this._display = new Kinetic.Group();

	var line = new Kinetic.Line({
		points: [
			ax*ri, ay*ri,
			lineX, ay*ro,
			lineX, -1000 //the line is masked along with the rest of the gauge
		],
		stroke: model.getLineColor().css,
		strokeWidth: 3
	});
	this._display.add(line);

	////
	
	this._lineExt = new Kinetic.Line({
		points: [ 
			lineX, ay*ro,
			lineX, -model.getFullRadius()
		],
		stroke: model.getLineColor().css,
		strokeWidth: 3
	});
	this._labelLayer.add(this._lineExt);

	this._label = new IDB.GraphLabel(lblOpt, '');
	this._labelLayer.add(this._label.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTargetView.prototype._onValueUpdate = function() {
	this._updateLabel();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTargetView.prototype._updateLabel = function() {
	var model = this._model;
	var lbl = this._label;
	var axisName = model.getAxisName()+' ';
	var valStr = model.getValueString();
	var fullRad = model.getFullRadius();

	valStr = (valStr.length ? '= '+valStr : '');

	lbl.text = axisName+valStr;
	var lw = Math.min(lbl.textWidth+5, fullRad*2/lbl.scaleX);
	lbl.width = lw;
	
	if ( lbl.truncate() ) {
		lbl.text = valStr;
		lbl.width = lw;
		
		if ( lbl.truncate() ) { //try truncating value only
			lbl.width = lbl.textWidth+5;
		}
		else { //the value alone is not truncated, so add some label
			lbl.width = lw-lbl.textWidth;
			lbl.text = axisName;
			lbl.truncate();

			lbl.text = (lbl.text == '...' ? '' : lbl.text+' ')+valStr;
			lbl.width = Math.max(lw, lbl.textWidth);
		}
	}
	
	var a = IDB.MathUtil.toRadians(model.getAngle());
	var px = Math.sin(a)*model.getOuterRadius();
	var edge = fullRad-lbl.width/2;
	
	lbl.x = IDB.MathUtil.clamp(px, -edge, edge)-lbl.width/2;
	lbl.y = -fullRad-lbl.height;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTargetView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTargetView.prototype.getLabelH = function() {
	return this._label.height;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTargetView.prototype.setScale = function(scale) {
	this._label.scaleX = this._label.scaleY = 1/scale;
	this._updateLabel();
};

/*====================================================================================================*/
IDB.GaugeSpdTicks = function(graphContext, minVal, maxVal, divMetrics, numFmt) {
	this._graphContext = graphContext;
	this._minVal = minVal;
	this._maxVal = maxVal;
	this._numFmt = numFmt;

	var sett = graphContext.settings;
	var gt = new IDB.GraphTicks(divMetrics, 
		sett.get('GaugeTickNumMinors'), sett.get('GaugeTickNumThirds'));
	
	this._data = {};
	this._data[IDB.GaugeSpdTicks.Major] = this._buildTicks(gt.getMajorValues(), true);
	this._data[IDB.GaugeSpdTicks.Minor] = this._buildTicks(gt.getMinorValues());
	this._data[IDB.GaugeSpdTicks.Third] = this._buildTicks(gt.getThirdValues());
};

IDB.GaugeSpdTicks.StyleLine = 0;
IDB.GaugeSpdTicks.StyleTriangle = 1;
IDB.GaugeSpdTicks.StyleSquare = 2;
IDB.GaugeSpdTicks.StyleCircle = 3;
		
IDB.GaugeSpdTicks.RingStyleNone = 0;
IDB.GaugeSpdTicks.RingStyleCenter = 1;
IDB.GaugeSpdTicks.RingStyleInner = 2;
IDB.GaugeSpdTicks.RingStyleOuter = 3;
IDB.GaugeSpdTicks.RingStyleBoth = 4;

IDB.GaugeSpdTicks.Major = 0;
IDB.GaugeSpdTicks.Minor = 1;
IDB.GaugeSpdTicks.Third = 2;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype._buildTicks = function(values, showLabel) {
	var ticks = [];
	var len = values.length;
	var sett = this._graphContext.settings;
	var targMode = sett.getBool('GaugeTargetMode');
	var angleStart = this.getStartAngle();
	var angleEnd = this.getEndAngle();
	var aDiff = angleEnd-angleStart;
	var v0 = this._minVal;
	var v1 = this._maxVal;
	var vDiff = v1-v0;
	var i, val, pos, tick;

	showLabel = (showLabel && sett.get('GaugeTickLabelOptions').show);

	for ( i = 0 ; i < len ; ++i ) {
		val = values[i];
		pos = (values[i]-v0)/vDiff;

		tick = {
			angle: pos*aDiff+angleStart
		};

		if ( showLabel ) {
			tick.label = (targMode ? val+'%' : sett.formatNumber(val, this._numFmt));
		}

		ticks.push(tick);
	}

	return ticks;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getDataCount = function(type) {
	return this._data[type].length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getData = function(type, index) {
	return this._data[type][index];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getStartAngle = function() {
	return this._graphContext.settings.get('GaugeAngleStart');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getEndAngle = function() {
	return this._graphContext.settings.get('GaugeAngleEnd');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getFaceRadius = function() {
	return this._graphContext.settings.get('ValueFaceRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getOuterRelativeRadius = function() {
	return this._graphContext.settings.get('GaugeTickRadiusOuter')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getInnerRelativeRadius = function() {
	return this._graphContext.settings.get('GaugeTickRadiusInner')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getColor = function() {
	return this._graphContext.settings.get('GaugeTickColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getMajorStyle = function() {
	return this._graphContext.settings.get('GaugeTickShape');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getMinorColor = function() {
	return this._graphContext.settings.get('GaugeTickColorMinor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getMinorRatio = function() {
	return this._graphContext.settings.get('GaugeTickMinorRatio')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getMinorPosition = function() {
	return this._graphContext.settings.get('GaugeTickMinorPosition')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getRingColor = function() {
	return this._graphContext.settings.get('GaugeTickRingColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getRingStyle = function() {
	return this._graphContext.settings.get('GaugeTickRingType');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicks.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('GaugeTickLabelOptions');
};


/*====================================================================================================*/
IDB.GaugeSpdTicksView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._buildKinetic = function() {
	var model = this._model;
	var faceRad = model.getFaceRadius()*1.005;
	var minorRat = 1-model.getMinorRatio();
	var minorPos = model.getMinorPosition();
	var r1 = faceRad*model.getOuterRelativeRadius();
	var r2 = faceRad*model.getInnerRelativeRadius();
	var r3 = r1-(r1-r2)*minorRat*(1-minorPos);
	var r4 = r2+(r1-r2)*minorRat*minorPos;

	var majorShape = this._getMajorShape(r1, r2);
	
	var minorShape = new Kinetic.Line({
		points: [0, -r3, 0, -r4],
		stroke: IDB.toRGBA(model.getMinorColor(), 0.85),
		strokeWidth: 1
	});
	
	var thirdShape = new Kinetic.Line({
		points: [0, -r3, 0, -r4],
		stroke: IDB.toRGBA(model.getMinorColor(), 0.22),
		strokeWidth: 1
	});

	this._labels = [];
	
	this._display = new Kinetic.Group();
	this._labelHold = new Kinetic.Group();

	this._drawRing(r1, r2);
	this._drawTicks(IDB.GaugeSpdTicks.Major, majorShape);
	this._drawTicks(IDB.GaugeSpdTicks.Minor, minorShape);
	this._drawTicks(IDB.GaugeSpdTicks.Third, thirdShape);

	this._display.add(this._labelHold);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._getMajorShape = function(r1, r2) {
	var model = this._model;
	var style = model.getMajorStyle();
	var col = model.getColor().css;
	var rr;

	if ( style == IDB.GaugeSpdTicks.StyleCircle ) {
		rr = (r1-r2)/2;

		return new Kinetic.Circle({
			offsetY: r1-rr,
			radius: rr,
			fill: col
		});
	}

	var config = {
		closed: true,
		fill: col,
		strokeWidth: 0
	};
	
	switch ( style ) {
		case IDB.GaugeSpdTicks.StyleLine:
			config.closed = false;
			config.stroke = col;
			config.strokeWidth = 3;

			config.points = [
				0, -r1,
				0, -r2
			];

			break;
			
		case IDB.GaugeSpdTicks.StyleTriangle:
			rr = (r2-r1)/3;

			config.points = [
				0, -r2,
				rr, -r1,
				-rr, -r1
			];

			break;
			
		case IDB.GaugeSpdTicks.StyleSquare:
			rr = (r2-r1)/2;

			config.points = [
				-rr, -r2,
				rr, -r2,
				rr, -r1,
				-rr, -r1
			];

			break;
	}

	return new Kinetic.Line(config);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._drawTicks = function(type, shape) {
	var model = this._model;
	var count = model.getDataCount(type);
	var lblOpt = model.getLabelOptions();
	var i, data, tick, lbl;
	
	lblOpt.align = 'center';

	for ( i = 0 ; i < count ; ++i ) {
		data = model.getData(type, i);

		tick = shape.clone();
		tick.rotation(data.angle);
		this._display.add(tick);

		if ( !data.label ) {
			continue;
		}

		lbl = new IDB.GraphLabel(lblOpt, data.label);
		this._labelHold.add(lbl.getDisplay());
		this._labels.push(lbl);

		if ( lblOpt.rotate ) {
			lbl.rotateFromCenter(Math.abs(data.angle) > 90 ? 180 : 0);
		}
		
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._drawRing = function(r1, r2) {
	switch ( this._model.getRingStyle() ) {
		case IDB.GaugeSpdTicks.RingStyleNone:
			break;

		case IDB.GaugeSpdTicks.RingStyleCenter:
			this._drawRingArc(r1+(r2-r1)/2);
			break;

		case IDB.GaugeSpdTicks.RingStyleInner:
			this._drawRingArc(r2);
			break;

		case IDB.GaugeSpdTicks.RingStyleOuter:
			this._drawRingArc(r1);
			break;

		case IDB.GaugeSpdTicks.RingStyleBoth:
			this._drawRingArc(r1);
			this._drawRingArc(r2);
			break;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._drawRingArc = function(radius) {
	var model = this._model;
	var ccos = IDB.MathCache.cos;
	var csin = IDB.MathCache.sin;
	var a0 = IDB.MathUtil.toRadians(model.getStartAngle());
	var a1 = IDB.MathUtil.toRadians(model.getEndAngle());
	var steps = Math.ceil((a1-a0)*20);
	var inc = (a1-a0)/steps;
	var i, a;

	var config = {
		points: [],
		stroke: model.getRingColor().css,
		strokeWidth: 1
	};
			
	for ( i = 0 ; i <= steps ; ++i ) {
		a = a0+inc*i;
		config.points.push(csin(a)*radius, -ccos(a)*radius);
	}

	var line = new Kinetic.Line(config);
	this._display.add(line);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype.setScale = function(scale) {
	this._updateLabelScale(scale);
	this._updateLabelOverlaps();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._updateLabelScale = function(scale) {
	this._labelPoints = [];

	var model = this._model;
	var labels = this._labels;
	var numLbls = labels.length;
	var invS = 1/scale;
	var lblOpt = model.getLabelOptions();
	var lblRad = model.getFaceRadius()*lblOpt.radius*scale;
	var toRads = IDB.MathUtil.toRadians;
	var ccos = IDB.MathCache.cos;
	var csin = IDB.MathCache.sin;
	var i, data, lbl, lblH, lblW, a;
	
	lblOpt.align = 'center';

	for ( i = 0 ; i < numLbls ; ++i ) {
		data = model.getData(IDB.GaugeSpdTicks.Major, i);
		
		lbl = this._labels[i];
		lbl.scaleX = lbl.scaleY = invS;

		lblW = lbl.textWidth;
		lblH = lbl.textHeight;

		if ( lblOpt.rotate ) {
			lbl.getDisplay()
				.offsetY(lblRad)
				.rotation(data.angle);
		}
		else {
			a = toRads(data.angle);

			lbl.getDisplay()
				.offsetX(-csin(a)*lblRad + lblW/2)
				.offsetY( ccos(a)*lblRad + lblH/2);
		}

		////

		var ld = lbl.getDisplay();
		var x0 = (ld.x()-ld.offsetX())*invS;
		var y0 = (ld.y()-ld.offsetY())*invS;

		var lblPt = {};
		this._labelPoints.push(lblPt);

		var shifts = [
			"nw", 0, 0,
			"ne", lblW*invS, 0,
			"se", lblW*invS, lblH*invS*0.8,
			"sw", 0, lblH*invS*0.8
		];

		var j, name, rotX, sx, sy;

		for ( j = 0 ; j < 4 ; ++j ) {
			name = shifts[j*3];
			sx = x0 + shifts[j*3+1];
			sy = y0 + shifts[j*3+2];

			if ( lblOpt.rotate ) {
				a = toRads(data.angle);

				rotX = sx*ccos(a) - sy*csin(a);
				sy = sy*ccos(a) + sx*csin(a);
				sx = rotX;
			}

			lblPt[name] = { x: sx, y: sy };
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._updateLabelOverlaps = function() {
	var model = this._model;
	var labels = this._labels;
	var numLbls = labels.length;
	var lblOpt = model.getLabelOptions();
	var prevPts = this._labelPoints[0];
	var i, data, lbl, pts, overlap;
	
	for ( i = 1 ; i < numLbls ; ++i ) {
		data = model.getData(IDB.GaugeSpdTicks.Major, i);
		lbl = this._labels[i];
		pts = this._labelPoints[i];

		if ( lblOpt.rotate ) {
			overlap = this._isOverlapAngled(-data.angle, pts, prevPts);
		}
		else {
			overlap = this._isOverlapFlat(pts, prevPts);
		}

		lbl.visible = !overlap;
		prevPts = (overlap ? prevPts : pts);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._isOverlapAngled = function(angle, pts, prevPts) {
	//rotate such that "pts" is flat, and "prevPts" is angled to its left (CCW)
	pts = this._rotatePoints(angle, pts, ['nw', 'sw']);
	prevPts = this._rotatePoints(angle, prevPts, ['ne', 'se']);

	var names = ['ne', 'se'];
	var x0 = pts.nw.x;
	var y1 = pts.sw.y;
	var p;

	for ( var i in names ) {
		p = prevPts[names[i]];

		if ( (p.x < x0 || p.y > y1) ) {
			continue; //"p" is to the left or below the "pts" rectangle bounds
		}

		return true;
	}

	return false;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._isOverlapFlat = function(pts, prevPts) {
	var x0 = pts.nw.x;
	var x1 = pts.se.x;
	var y0 = pts.nw.y;
	var y1 = pts.se.y;
	var names = ['nw', 'ne', 'se', 'sw'];
	var p;

	for ( var i in names ) {
		p = prevPts[names[i]];

		if ( p.x < x0 || p.x > x1 || p.y < y0 || p.y > y1 ) {
			continue; //"p" is not within the "pts" rectangle
		}

		return true;
	}

	return false;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdTicksView.prototype._rotatePoints = function(angle, pts, names) {
	if ( angle == 0 ) {
		return pts;
	}

	names = IDB.ifUndef(names, ['nw', 'ne', 'se', 'sw']);

	var newPts = {};
	var ccos = IDB.MathCache.cos;
	var csin = IDB.MathCache.sin;
	var a = IDB.MathUtil.toRadians(angle);
	var key, p;

	for ( var i in names ) {
		key = names[i];
		p = pts[key];

		newPts[key] = {
			x: p.x*ccos(a) - p.y*csin(a),
			y: p.y*ccos(a) + p.x*csin(a)
		};
	}

	return newPts;
};

/*====================================================================================================*/
IDB.GaugeSpdView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdView.prototype._buildKinetic = function() {
	var model = this._model;
	
	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());

    this._display._touchHandler = new IDB.TouchHandler(this._display);

	this._topH = 0;
	this._botH = 0;
	
	this._spdHold = new Kinetic.Group();
	this._display.add(this._spdHold);

	this._mask = new IDB.GaugeSpdMaskView(model.getMask());
	this._spdHold.add(this._mask.getDisplay());
	
	this._maskedHold = new Kinetic.Group();
	this._spdHold.add(this._maskedHold);

	IDB.KineticUtil.setMask(this._maskedHold, this._mask.getDisplay());
	
	if ( model.getFace() ) {
		this._face = new IDB.GaugeSpdFaceView(model.getFace());
		this._maskedHold.add(this._face.getDisplay());
	}
	
	if ( model.getRange() ) {
		this._range = new IDB.GaugeSpdRangeView(model.getRange());
		this._maskedHold.add(this._range.getDisplay());
	}
	
	if ( model.getTicks() ) {
		this._ticks = new IDB.GaugeSpdTicksView(model.getTicks());
		this._maskedHold.add(this._ticks.getDisplay());
	}
	
	if ( model.getBorder() ) {
		this._border = new IDB.GaugeSpdBorderView(model.getBorder());
		this._maskedHold.add(this._border.getDisplay());
	}
	
	if ( model.getTarget() ) {
		var lblLayer = new Kinetic.Group();
		this._spdHold.add(lblLayer);
		
		this._target = new IDB.GaugeSpdTargetView(model.getTarget(), lblLayer);
		this._maskedHold.add(this._target.getDisplay());
		
		this._topH += this._target.getLabelH();
	}
			
	this._needles = [];
	var needleCount = model.getNeedleCount();
	var i, needle;
	
	for ( i = 0 ; i < needleCount ; ++i ) {
		needle = new IDB.GaugeSpdNeedleView(model.getNeedle(i));
		this._maskedHold.add(needle.getDisplay());
		this._needles[i] = needle;
	}
	
	if ( model.getCover() ) {
		this._cover = new IDB.GaugeSpdCoverView(model.getCover());
		this._maskedHold.add(this._cover.getDisplay());
	}

	if ( model.getLabel() ) {
		this._label = new IDB.GaugeSpdLabelView(model.getLabel());
		this._display.add(this._label.getDisplay());

		this._botH += (model.getLabel().showOnFace() ? 0 : this._label.getLabelH());
	}
	
	if ( model.getOdom() ) {
		this._odom = new IDB.GraphOdomView(model.getOdom());
		this._odom.setSize();
		this._display.add(this._odom.getDisplay());

		this._botH += this._odom.getMinHeight();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdView.prototype.getSizeRatio = function() {
	return this._mask.getW()/this._mask.getH();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdView.prototype.getFixedExtraHeight = function() {
	return this._topH+this._botH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeSpdView.prototype.setSize = function(width, height) {
	var model = this._model;
	var useH = Math.max(0, height-this._topH-this._botH);
	var s = Math.min(width/this._mask.getW(), useH/this._mask.getH());
	var numNeedles = this._needles.length;

	this._spdHold.x(width*this._mask.getRelativeX());
	this._spdHold.y(useH*this._mask.getRelativeY()+this._topH);
	this._spdHold.scaleX(s).scaleY(s);
	
	for ( var i = 0 ; i < numNeedles ; ++i ) {
		this._needles[i].setScale(s);
	}
	
	if ( this._ticks ) {
		this._ticks.setScale(s);
	}

	if ( this._target ) {
		this._target.setScale(s);
	}

	if ( this._border ) {
		this._border.setScale(s);
	}
	
	if ( this._label ) {
		var lw = (this._target ? s*model.getValueDiameter() : width);
		var lblD = this._label.getDisplay();

		this._label.setWidth(lw);
		
		if ( model.getLabel().showOnFace() ) {
			var a = IDB.MathUtil.toRadians(model.getAngle(0.5)-180);

			lblD.x(Math.sin(a)*width/3.5);
			lblD.y(height/2-Math.cos(a)*width/3.5);
		}
		else {
			lblD.x((width-lw)/2);
			lblD.y(height-this._label.getLabelH()/2-
				(this._odom ? this._odom.getMinHeight() : 0));
		}
	}

	if ( this._odom ) {
		this._odom.getDisplay()
			.x((width-this._odom.getMinWidth())/2)
			.y(height-this._odom.getMinHeight()-2);
	}
};

/*====================================================================================================*/
IDB.GaugeCluster = function(graphContext, yAxisCol, yClusterCols) {
	this._graphContext = graphContext;
	this._yAxisCol = yAxisCol;
	this._yClusterCols = yClusterCols;

	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var yAxis = dpGrid.getYAxis(this._yAxisCol, true);
	var yClustCols = this._yClusterCols;
	var yClustCount = yClustCols.length;
	var i, spd;
	
	this._clusters = [];
	this._currValue = null;

	this._reset = false;
	this._datapoint = null;
	this._xRowIndex = 0;
	
	this._calcMinMaxVals();

	this._face = new IDB.GaugeSpdFace(gc, yAxis.axisColor);
	
	for ( i = 0 ; i < yClustCount ; ++i ) {
		spd = new IDB.GaugeClusterSpd(gc, yClustCols[i]);
		this._clusters[i] = spd;
	}
	
	this._valueLblOpt = sett.get('GaugeAxisLabelOptions');
	this._nameText = yAxis.axisName;
	this._valueText = '';
	
	this._nameLblOpt = {
		show: this._valueLblOpt.show,
		size: this._valueLblOpt.size,
		color: this._valueLblOpt.color,
		bold: true
	};

	this._range = new IDB.GaugeClusterRange(gc, this._minVal, this._maxVal, 
		dpGrid.getRangeList(this._yAxisCol, true));

	if ( sett.getBool('GaugeTickShow') ) {
		this._ticks = new IDB.GaugeClusterTicks(gc, this._minVal, this._maxVal, 
			this._divMetrics, yAxis.numFmt);
	}

	this._meter = new IDB.GaugeClusterMeter(gc, this._minVal, this._maxVal);
	this._border = new IDB.GaugeSpdBorder(gc);
	this._cover = new IDB.GaugeSpdCover(gc);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype._calcMinMaxVals = function() {
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var axis = dpGrid.getYAxis(this._yAxisCol, true);
	var rangeList = dpGrid.getRangeList(this._yAxisCol, true);
	var ss = dpGrid.getStatSet(axis.axisId);
	
	var min = rangeList[0].val;
	var max = rangeList[rangeList.length-1].val;
	max += (max-min)/10000; //ensure that the final range color is shown
	
	min = Math.min(min, ss.min);
	max = Math.max(max, ss.max);
			
	if ( min == max ) {
		if ( min > 0 ) {
			min = 0;
		}
		else if ( max < 0 ) {
			max = 0;
		}
		else {
			max = 10;
		}
	}
	
	var numDivs = gc.settings.get('GaugeTickNumMajors')+1;
	this._divMetrics = IDB.Calculator.getDivisionMetrics(min, max, numDivs);
	var dm = this._divMetrics.largeDivisions;
	this._minVal = dm.getMinBoundary();
	this._maxVal = dm.getMaxBoundary();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.setOnValueUpdate = function(callback) {
	this._onValueUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.resetNeedle = function() {
	this._currValue = {
		asNumber: this._minVal,
		asText: ''
	};

	this._reset = true;
	this._datapoint = null;
	this._updateValue();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.setXRowIndex = function(index) {
	this._xRowIndex = index;
	this._reset = false;
	
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	
	this._datapoint = dpGrid.getDatapointAt(index, this._yAxisCol, true);
	var dv = this._datapoint.yValue;

	this._currValue = {
		asNumber: dv.numberValue,
		asText: dv.formattedValue
	};

	this._updateValue();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype._updateValue = function() {
	var count = this._clusters.length;
	var i, spd;

	for ( i = 0 ; i < count ; ++i ) {
		spd = this._clusters[i];

		if ( this._reset ) {
			spd.resetNeedle();
		}
		else {
			spd.setXRowIndex(this._xRowIndex);
		}
	}

	this._valueText = this._currValue.asText;

	if ( this._meter ) {
		this._meter.setValue(this._currValue);
	}
	
	if ( this._onValueUpdate ) {
		this._onValueUpdate();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getFace = function() {
	return this._face;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getRange = function() {
	return this._range;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getTicks = function() {
	return this._ticks;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getBorder = function() {
	return this._border;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getClusterCount = function() {
	return this._clusters.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getCluster = function(index) {
	return this._clusters[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getCover = function() {
	return this._cover;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getMeter = function() {
	return this._meter;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getNameLabelOptions = function() {
	return this._nameLblOpt;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getValueLabelOptions = function() {
	return this._valueLblOpt;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getNameText = function() {
	return this._nameText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getValueText = function() {
	return this._valueText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getValueRadius = function() {
	return this._graphContext.settings.get('ValueRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getValueFaceRadius = function() {
	return this._graphContext.settings.get('ValueFaceRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.getBorderThickness = function() {
	return this._graphContext.settings.get('GaugeBorderThick')/100;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, '_handleClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.createOverListener = function() {
	return IDB.listener(this, '_handleOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype.createOutListener = function() {
	return IDB.listener(this, '_handleOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype._handleClick = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointClicked(this._datapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype._handleOver = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointOver(this._datapoint);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeCluster.prototype._handleOut = function() {
	if ( !this._reset && this._datapoint ) {
		this._graphContext.onDatapointOut(this._datapoint);
	}
};

/*====================================================================================================*/
IDB.GaugeClusterMeter = function(graphContext, minVal, maxVal) {
	this._graphContext = graphContext;
	this._minVal = minVal;
	this._maxVal = maxVal;
};

IDB.GaugeClusterMeter.StyleBar = 0;
IDB.GaugeClusterMeter.StyleCircle = 1;
IDB.GaugeClusterMeter.StyleTri = 2;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.setOnValueUpdate = function(callback) {
	this._onValueUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.setValue = function(value) {
	var valPos = (value.asNumber-this._minVal)/(this._maxVal-this._minVal);
	var a0 = this.getAngleStart();

	this._angle = valPos*(this.getAngleEnd()-a0)+a0;
	
	if ( this._onValueUpdate ) {
		this._onValueUpdate();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getAngle = function() {
	return this._angle;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getOuterRelativeRadius = function() {
	return 0.95;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getMiddleRelativeRadius = function() {
	return 0.9;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getInnerRelativeRadius = function() {
	return 0.85;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getAngleStart = function() {
	return -155;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getAngleEnd = function() {
	return 155;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getFaceRadius = function() {
	return this._graphContext.settings.get('ValueFaceRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getStyle = function() {
	return this._graphContext.settings.get('GaugeClustMeterStyle');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getSize = function() {
	return this._graphContext.settings.get('GaugeClustMeterSize')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.useShading = function() {
	return this._graphContext.settings.getBool('GaugeClustMeterShade');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getColor = function() {
	return this._graphContext.settings.get('GaugeClustMeterColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeter.prototype.getAlpha = function() {
	return this._graphContext.settings.getAlpha('GaugeClustMeterTrans');
};
/* global IDB, $, Kinetic */
/*====================================================================================================*/
IDB.GaugeClusterMeterView = function(model) {
	this._model = model;
	model.setOnValueUpdate(IDB.listener(this, '_onValueUpdate'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeterView.prototype._buildKinetic = function() {
	var me = this, model = me._model ? me._model : null, context = model && model._graphContext ? model._graphContext : null,
		settings = context && context.settings ? context.settings : null, animate = settings && (settings.animationEnabled || false) && typeof settings.animationEnabled==='function' ? settings.animationEnabled() : false;

	me._animationsEnabled = animate;
	this._faceRad = model.getFaceRadius()*1.005;

	var s = model.getSize();
	var outer = model.getOuterRelativeRadius()*this._faceRad;
	var inner = model.getInnerRelativeRadius()*this._faceRad;
	
	this._barOuter = outer-(outer-inner)*(0.5-s/2);
	this._barInner = inner+(outer-inner)*(0.5-s/2);

	this._display = new Kinetic.Group();

	switch ( model.getStyle() ) {
		case IDB.GaugeClusterMeter.StyleBar:
			break;

		case IDB.GaugeClusterMeter.StyleCircle:
			this._drawCircle();
			break;
			
		case IDB.GaugeClusterMeter.StyleTri:
			this._drawTri();
			break;
	}

	this._onValueUpdate();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeterView.prototype._drawCircle = function() {
	var model = this._model;
	var outer = model.getOuterRelativeRadius();
	var inner = model.getInnerRelativeRadius();
	var rad = (outer-inner)*this._faceRad*model.getSize()/2;
	var shadeScale = rad/IDB.GenericShading.FullDimension*2;

	this._markerHold = new Kinetic.Group();
	this._display.add(this._markerHold);

	this._marker = new Kinetic.Circle({
		radius: rad,
		fill: IDB.toRGBA(model.getColor(), model.getAlpha()),
		stroke: 'rgba(0, 0, 0, 0.5)',
		strokeWidth: 0.5
	});
	this._markerHold.add(this._marker);
	
	if ( model.useShading() ) {
		var shade = new IDB.GenericShading(IDB.GenericShading.TypeGloss, false);
		shade.getDisplay().scaleX(shadeScale).scaleY(shadeScale);
		this._markerHold.add(shade.getDisplay());
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeterView.prototype._drawTri = function() {
	var model = this._model;
	var outer = model.getOuterRelativeRadius();
	var inner = model.getInnerRelativeRadius();
	var half = (outer-inner)*this._faceRad*model.getSize()/2;

	var config = {
		points: [
			-half, -half,
			half, -half,
			0, half
		],
		closed: true,
		fill: IDB.toRGBA(model.getColor(), model.getAlpha()),
		stroke: 'rgba(0, 0, 0, 0.5)',
		strokeWidth: 0.5
	};

	this._markerHold = new Kinetic.Group();
	this._display.add(this._markerHold);

	this._marker = new Kinetic.Line(config);
	this._markerHold.add(this._marker);
	
	if ( model.useShading() ) {
		config.fill = config.stroke = null;

		this._markerMask = new Kinetic.Line(config);
		this._markerHold.add(this._markerMask);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeterView.prototype._onValueUpdate = function() {
	var me = this, model = me._model, animations = me._animationsEnabled;


	if (animations && !!me._scale)
	{
		var parentLayer = me._display && me._display.getLayer && typeof me._display.getLayer==='function' ? me._display.getLayer() : null,
		    animDur = 1000.00, // Should be a more global setting?
		    finalAngle = model.getAngle(),
		    initialAngle = me._lastAngle ? me._lastAngle : (me._lastAngle = finalAngle, model.getAngleStart()),
		    angleDiff = finalAngle - initialAngle,
			anim = new Kinetic.Animation(function animateMeter(frame)
			{
				if (frame.time <= animDur)
				{
					var curAngle = initialAngle + $.easing.easeOutBack(frame.time / animDur) * angleDiff;
					me._setUpdatedAngle(curAngle);
					me._lastAngle = curAngle;
				}
				else
				{
					anim.stop();
					me._setUpdatedAngle(finalAngle);
					me._lastAngle = finalAngle;
				}
			}, parentLayer);
		anim.start();
	}
	else
	{
		me._setUpdatedAngle();
	}
}

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeterView.prototype._setUpdatedAngle = function(newAngle) {
	var model = this._model;
	var r2 = model.getMiddleRelativeRadius()*this._faceRad;
	var angle = arguments.length>0 ? newAngle : model.getAngle();

	if ( this._marker ) {
		var a = IDB.MathUtil.toRadians(angle);
		this._markerHold.x(Math.sin(a)*r2).y(-Math.cos(a)*r2);

		if ( model.getStyle() == IDB.GaugeClusterMeter.StyleTri ) {
			this._markerHold.rotation(angle);
			this._redrawBar(angle-4, angle+4);
		}
	}
	else {
		this._redrawBar(model.getAngleStart(), angle);
		this.setScale(this._scale);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeterView.prototype._redrawBar = function(angleStart, angleEnd) {
	var model = this._model;
	var angle = angleEnd-angleStart;
	var outer = this._barOuter;
	var inner = this._barInner;

	var config = {
		angle: angle,
		outerRadius: outer,
		innerRadius: inner,
		rotation: angleStart-90,
		fill: IDB.toRGBA(model.getColor(), model.getAlpha()),
		stroke: 'rgba(0, 0, 0, 0.333)',
		strokeWidth: 0.5
	};

	if ( this._barHold ) {
		this._barHold.destroy();
	}

	this._barHold = new Kinetic.Group();
	this._display.add(this._barHold);

	this._bar = new Kinetic.Arc(config);
	this._barHold.add(this._bar);

	if ( !model.useShading() ) {
		return;
	}

	config.fill = config.stroke = null;
	config.fillRadialGradientStartRadius = inner,
	config.fillRadialGradientEndRadius = outer,
	config.fillRadialGradientColorStops = [
		0, IDB.toRGBA(0x0, 0.4),
		0.65, IDB.toRGBA(0x0, 0),
		1, IDB.toRGBA(0x0, 0.4)
	];
	
	var shade = new Kinetic.Arc(config);
	this._barHold.add(shade);
	
	var steps = Math.ceil(angle/10);
	var inc = angle/steps;
	var ccos = IDB.MathCache.cos;
	var i, a, gloss;

	config.angle = inc;
	config.fill = '#fff';
	config.outerRadius = outer-(outer-inner)*0.1;
	config.innerRadius = outer-(outer-inner)*0.35;

	for ( i = 0 ; i < steps ; ++i ) {
		a = i*inc+angleStart;
		config.rotation = a-90;

		a = IDB.MathUtil.toRadians(a);
		config.opacity = Math.pow(ccos(a), 2)*0.45+0.25;
		
		gloss = new Kinetic.Arc(config);
		this._barHold.add(gloss);
	}
	
	config.outerRadius = outer-(outer-inner)*0.7;
	config.innerRadius = outer-(outer-inner)*0.85;

	for ( i = 0 ; i < steps ; ++i ) {
		a = i*inc+angleStart;
		config.rotation = a-90;

		a = IDB.MathUtil.toRadians(a);
		config.opacity = Math.pow(ccos(a), 2)*0.2+0.1;
		
		gloss = new Kinetic.Arc(config);
		this._barHold.add(gloss);
	}

	if ( this._markerMask ) {
		IDB.KineticUtil.setMask(this._barHold, this._markerMask);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeterView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterMeterView.prototype.setScale = function(scale) {
	this._scale = scale;

	(this._marker || this._bar)
		.shadowEnabled(this._model.useShading())
		.shadowOffsetX(0.25*scale)
		.shadowOffsetY(1.5*scale)
		.shadowBlur(3*scale)
		.shadowOpacity(0.3);
};

/*====================================================================================================*/
IDB.GaugeClusterRange = function(graphContext, minVal, maxVal, ranges) {
	IDB.GaugeSpdRange.call(this, graphContext, minVal, maxVal, ranges);
};

IDB.GaugeClusterRange.prototype = Object.create(IDB.GaugeSpdRange.prototype);
IDB.GaugeClusterRange.prototype.constructor = IDB.GaugeClusterRange;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterRange.prototype._getAngleStart = function() {
	return -155;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterRange.prototype._getAngleEnd = function() {
	return 155;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterRange.prototype.getOuterRelativeRadius = function() {
	return 0.95;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterRange.prototype.getInnerRelativeRadius = function() {
	return 0.85;
};

/*====================================================================================================*/
IDB.GaugeClusterSpd = function(graphContext, yAxisCol) {
	IDB.GaugeSpd.call(this, graphContext, yAxisCol, -1, false);
};

IDB.GaugeClusterSpd.prototype = Object.create(IDB.GaugeSpd.prototype);
IDB.GaugeClusterSpd.prototype.constructor = IDB.GaugeClusterSpd;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpd.prototype._build = function() {
	IDB.GaugeSpd.prototype._build.call(this);
	
	this._face = null;
	this._cover = null;
	this._border = null;
	this._ticks = null;
	this._label = null;

	var gc = this._graphContext;
	var sett = gc.settings;

	this._valueLblOpt = sett.get('GaugeAxisLabelOptions');
	this._nameText = gc.datapointGrid.getYAxis(this._yAxisCol, true).axisName;
	this._valueText = '';
	
	this._nameLblOpt = {
		show: this._valueLblOpt.show,
		size: this._valueLblOpt.size,
		color: this._valueLblOpt.color,
		bold: true
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpd.prototype._updateValue = function() {
	IDB.GaugeSpd.prototype._updateValue.call(this);
	
	this._valueText = this._currValues[0].asText;

	if ( this._onValueUpdate ) {
		this._onValueUpdate();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpd.prototype.setOnValueUpdate = function(callback) {
	this._onValueUpdate = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpd.prototype.getNameLabelOptions = function() {
	return this._nameLblOpt;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpd.prototype.getValueLabelOptions = function() {
	return this._valueLblOpt;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpd.prototype.getNameText = function() {
	return this._nameText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpd.prototype.getValueText = function() {
	return this._valueText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpd.prototype.getBorderThickness = function() {
	return this._graphContext.settings.get('GaugeBorderThick')/100;
};

/*====================================================================================================*/
IDB.GaugeClusterSpdView = function(model) {
	IDB.GaugeSpdView.call(this, model);
	model.setOnValueUpdate(IDB.listener(this, '_onValueUpdate'));
};

IDB.GaugeClusterSpdView.prototype = Object.create(IDB.GaugeSpdView.prototype);
IDB.GaugeClusterSpdView.prototype.constructor = IDB.GaugeClusterSpdView;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpdView.prototype._buildKinetic = function() {
	IDB.GaugeSpdView.prototype._buildKinetic.call(this);
	
	var model = this._model;
	var nameLblOpt = model.getNameLabelOptions();
	var valLblOpt = model.getValueLabelOptions();

	if ( nameLblOpt.show ) {
		nameLblOpt.align = valLblOpt.align = 'center';

		this._nameLabel = new IDB.GraphLabel(nameLblOpt, model.getNameText());
		this._display.add(this._nameLabel.getDisplay());
		
		this._valLabel = new IDB.GraphLabel(valLblOpt, model.getValueText());
		this._display.add(this._valLabel.getDisplay());
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpdView.prototype._onValueUpdate = function() {
	this._valLabel.text = this._model.getValueText();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterSpdView.prototype.setSizeAndScale = function(size, scale) {
	IDB.GaugeSpdView.prototype.setSize.call(this, size, size);

	var model = this._model;
	var holdScale = this._spdHold.scaleX()/(1-model.getBorderThickness());

	this._spdHold.x(0).y(0).scaleX(holdScale).scaleY(holdScale);
	
	if ( this._nameLabel ) {
		var nl = this._nameLabel;
		var vl = this._valLabel;

		nl.scaleX = nl.scaleY = vl.scaleX = vl.scaleY = 1;
		nl.width = vl.width = size*0.8*scale;
		nl.truncate();
		vl.truncate();
		nl.scaleX = nl.scaleY = vl.scaleX = vl.scaleY = 1/scale;

		nl.x = vl.x = -nl.width/2;
		nl.y = 35*holdScale;
		vl.y = nl.y+nl.height;
	}
};

/*====================================================================================================*/
IDB.GaugeClusterTicks = function(graphContext, minVal, maxVal, divMetrics, numFmt) {
	IDB.GaugeSpdTicks.call(this, graphContext, minVal, maxVal, divMetrics, numFmt);
};

IDB.GaugeClusterTicks.prototype = Object.create(IDB.GaugeSpdTicks.prototype);
IDB.GaugeClusterTicks.prototype.constructor = IDB.GaugeClusterTicks;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterTicks.prototype.getStartAngle = function() {
	return -155;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterTicks.prototype.getEndAngle = function() {
	return 155;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterTicks.prototype.getOuterRelativeRadius = function() {
	return 0.95;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterTicks.prototype.getInnerRelativeRadius = function() {
	return 0.85;
};

/*====================================================================================================*/
IDB.GaugeClusterView = function(model) {
	this._model = model;
	model.setOnValueUpdate(IDB.listener(this, '_onValueUpdate'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterView.prototype._buildKinetic = function() {
	var model = this._model;
	var nameLblOpt = model.getNameLabelOptions();
	var valLblOpt = model.getValueLabelOptions();
	
	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());

    this._display._touchHandler = new IDB.TouchHandler(this._display);

	this._spdHold = new Kinetic.Group();
	this._display.add(this._spdHold);

	this._mask = new Kinetic.Circle({ radius: model.getValueRadius() });
	this._spdHold.add(this._mask);
	
	this._maskedHold = new Kinetic.Group();
	this._spdHold.add(this._maskedHold);

	IDB.KineticUtil.setMask(this._maskedHold, this._mask);
	
	this._face = new IDB.GaugeSpdFaceView(model.getFace());
	this._maskedHold.add(this._face.getDisplay());

	if ( nameLblOpt.show ) {
		nameLblOpt.align = valLblOpt.align = 'center';

		this._nameLabel = new IDB.GraphLabel(nameLblOpt, model.getNameText());
		this._maskedHold.add(this._nameLabel.getDisplay());
		
		this._valLabel = new IDB.GraphLabel(valLblOpt, model.getValueText());
		this._maskedHold.add(this._valLabel.getDisplay());
	}

	if ( model.getRange() ) {
		this._range = new IDB.GaugeSpdRangeView(model.getRange());
		this._maskedHold.add(this._range.getDisplay());
	}
	
	if ( model.getTicks() ) {
		this._ticks = new IDB.GaugeSpdTicksView(model.getTicks());
		this._maskedHold.add(this._ticks.getDisplay());
	}

	if ( model.getMeter() ) {
		this._meter = new IDB.GaugeClusterMeterView(model.getMeter());
		this._maskedHold.add(this._meter.getDisplay());
	}
	
	this._border = new IDB.GaugeSpdBorderView(model.getBorder());
	this._maskedHold.add(this._border.getDisplay());
	
	this._clusters = [];
	var clusterCount = model.getClusterCount();
	var i, spd;
	
	for ( i = 0 ; i < clusterCount ; ++i ) {
		spd = new IDB.GaugeClusterSpdView(model.getCluster(i));
		this._maskedHold.add(spd.getDisplay());
		this._clusters[i] = spd;
	}
			
	this._cover = new IDB.GaugeSpdCoverView(model.getCover());
	this._maskedHold.add(this._cover.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterView.prototype._onValueUpdate = function() {
	this._valLabel.text = this._model.getValueText();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterView.prototype.getSizeRatio = function() {
	return 1;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterView.prototype.getFixedExtraHeight = function() {
	return 0;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GaugeClusterView.prototype.setSize = function(width, height) {
	var model = this._model;
	var dia = model.getValueRadius()*2;
	var s = Math.min(width/dia, height/dia);

	this._spdHold.x(width/2);
	this._spdHold.y(height/2);
	this._spdHold.scaleX(s).scaleY(s);

	if ( this._border ) {
		this._border.setScale(s);
	}

	if ( this._ticks ) {
		this._ticks.setScale(s);
	}

	if ( this._meter ) {
		this._meter.setScale(s);
	}
	
	if ( this._nameLabel ) {
		var nl = this._nameLabel;
		var vl = this._valLabel;

		nl.scaleX = nl.scaleY = vl.scaleX = vl.scaleY = 1;
		nl.width = vl.width = width/3;
		nl.truncate();
		vl.truncate();
		nl.scaleX = nl.scaleY = vl.scaleX = vl.scaleY = 1/s;

		nl.x = vl.x = -nl.width/2;
		vl.y = model.getValueFaceRadius()*0.82;
		nl.y = vl.y-nl.height;
	}

	var fullSize = width*(1-model.getBorderThickness());
	var spd, size;
			
	switch ( this._clusters.length ) {
		case 3:
			size = fullSize*0.4/s;

			spd = this._clusters[0];
			spd.setSizeAndScale(size,  s);
			spd.getDisplay().x(0).y(-size*0.4);
			
			spd = this._clusters[1];
			spd.setSizeAndScale(size, s);
			spd.getDisplay().x(-size*0.45).y(size*0.35);
			
			spd = this._clusters[2];
			spd.setSizeAndScale(size,  s);
			spd.getDisplay().x(size*0.45).y(size*0.35);
			break;
			
		case 2:
			size = fullSize*0.4/s;

			spd = this._clusters[0];
			spd.setSizeAndScale(size,  s);
			spd.getDisplay().x(-size*0.45).y(size*0.15);
			
			spd = this._clusters[1];
			spd.setSizeAndScale(size, s);
			spd.getDisplay().x(size*0.45).y(size*0.15);
			break;
			
		case 1:
			size = fullSize*0.75/s;

			spd = this._clusters[0];
			spd.setSizeAndScale(size, s);
			spd.getDisplay().x(0).y(size*0.1);
			break;
	}
};
/*global IDB, $ */

