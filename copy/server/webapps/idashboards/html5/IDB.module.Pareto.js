'use strict';
IDB.ParetoBarPlot = function(graphContext, stats) {
	this._graphContext = graphContext;
	this._stats = stats;

	var lo = graphContext.settings.get('BarLabelOptions');
	lo.position = graphContext.settings.get('BarLabelPos');
	lo.rotation = graphContext.settings.get('BarLabelRotation');

	this._boxes = this._buildBoxes();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoBarPlot.prototype._buildBoxes = function() {
	var boxes = [];
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var rowCount = dpGrid.getNumXRows();
	var stats = this._stats;
	var range = stats.max-stats.min;
	var zero = (0-stats.min)/range;
	var showLabels = sett.get('BarLabelOptions').show;
	var i, dp, yVal, relY, lblText, box;

	for ( i = 0 ; i < rowCount ; ++i ) {
		dp = dpGrid.getDatapointAt(i, 0, true);
		yVal = dp.yValue.numberValue;
		relY = (yVal-stats.min)/range;
		lblText = (showLabels ? dp.yValue.formattedValue : null);
		
		box = new IDB.BarBox(gc, dp, stats, zero, relY-zero, lblText);
		boxes.push(box);
	}

	return boxes;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoBarPlot.prototype.getBoxCount = function() {
	return this._boxes.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoBarPlot.prototype.getBox = function(i) {
	return this._boxes[i];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoBarPlot.prototype.getThicknessRatio = function() {
	return this._graphContext.settings.get('BarThicknessRatio');
};
/* global IDB, Kinetic */
/*====================================================================================================*/
IDB.ParetoBarPlotView = function(model) {
	this._model = model;
	this._buildKinetic();
	this._boxes = this._buildBoxes();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoBarPlotView.prototype._buildKinetic = function() {
    var me = this, model = me._model, context = model ? model._graphContext : null,
        settings = context ? context.settings : null, animate = settings ? settings.animationEnabled() : false;

    me._animationEnabled = animate;

	this._display = new Kinetic.Group();

	this._shadowHold = new Kinetic.Group();
	this._barHold = new Kinetic.Group();
	this._labelHold = new Kinetic.Group();

	this._display.add(this._shadowHold);
	this._display.add(this._barHold);
	this._display.add(this._labelHold);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoBarPlotView.prototype._buildBoxes = function() {
	var boxes = [];
	var model = this._model;
	var shadHold = this._shadowHold;
	var barHold = this._barHold;
	var lblHold = this._labelHold;
	var boxCount = model.getBoxCount();
	var i, box;

	for ( i = 0 ; i < boxCount ; ++i ) {
		box = new IDB.BarBoxView(model.getBox(i));
		barHold.add(box.getBarDisplay());

		if ( box.getShadowDisplay() ) {
			shadHold.add(box.getShadowDisplay());
		}

		if ( box.getLabelDisplay() ) {
			lblHold.add(box.getLabelDisplay());
		}

		boxes[i] = box;
	}

	return boxes;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoBarPlotView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoBarPlotView.prototype.setSize = function(width, height) {
	var model = this._model;
	var boxes = this._boxes;
	var boxCount = model.getBoxCount();
	var barFullW = width/boxCount;
	var barW = barFullW*model.getThicknessRatio();
	var i, px, box, me = this, animate = me._animationEnabled, animDelay = 200, animateLayer = null;
	if (animate)
	{
	    animateLayer = me._display && me._display.getLayer && typeof me._display.getLayer==='function' ? me._display.getLayer() : null;
	}

	for ( i = 0 ; i < boxCount ; ++i ) {
		px = (i+0.5)*barFullW;

		box = boxes[i];
		if (box.isAnimationEnabled && !box.doneAnimating)
		{
			box.runInitialAnimation(px, barW, height, 1, animDelay*i);
		}
		else
		{
			box.setPositionAndSize(px, barW, height);
		}
	}
};
/*====================================================================================================*/
IDB.ParetoGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet);
	this._info.legendStyle = IDB.LS.AXES;
};

IDB.ParetoGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.ParetoGraph.prototype.constructor = IDB.ParetoGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoGraph.prototype._getRelevantDatapoints = function() {
	var dpGrid = this._graphContext.datapointGrid;
	var rowCount = dpGrid.getNumXRows();
	var points = [];
	var i, dp;

	for ( i = 0 ; i < rowCount ; ++i ) {
		dp = dpGrid.getDatapointAt(i, 0, true);
		points.push(dp);
	}

	return points;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoGraph.prototype._build = function() {
	this._graphContext.datapointGrid.sortRowsByYValue(0, true);
	this._grid = new IDB.CartesianGrid(this._graphContext);

	var yAxisId = this._graphContext.datapointGrid.getYAxis(0, true).axisId;
	var stats = this._grid.getAxes().getYGridAxis(0).getStatsByAxis(yAxisId);

	this._bar = new IDB.ParetoBarPlot(this._graphContext, stats);
	this._line = new IDB.ParetoLinePlot(this._graphContext, stats);
	
	this._limitHold = new Kinetic.Group();

	this._gridView = new IDB.CartesianGridView(this._grid, this._limitHold);
	this._stageLayer.add(this._gridView.getDisplay());
	
	this._barView = new IDB.ParetoBarPlotView(this._bar);
	this._stageLayer.add(this._barView.getDisplay());

	this._lineView = new IDB.ParetoLinePlotView(this._line);
	this._stageLayer.add(this._lineView.getDisplay());
	
	this._stageLayer.add(this._limitHold);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoGraph.prototype._update = function(width, height) {
	var gv = this._gridView;
	var bv = this._barView;
	var bvd = bv.getDisplay();
	var lv = this._lineView;
	var lvd = lv.getDisplay();

	gv.setSize(width, height);

	var clip = {
		x: 0,
		y: -3,
		width: gv.getPlotWidth(),
		height: gv.getPlotHeight()+6
	};
	
	bvd.x(gv.getPlotPosX());
	bvd.y(gv.getPlotPosY());
	bv.setSize(gv.getPlotWidth(), gv.getPlotHeight());
	bvd.clip(clip);
	
	lvd.x(gv.getPlotPosX());
	lvd.y(gv.getPlotPosY());
	lv.setSize(gv.getPlotWidth(), gv.getPlotHeight());
	lvd.clip(clip);
};

/*====================================================================================================*/
IDB.ParetoLinePlot = function(graphContext, stats) {
	this._graphContext = graphContext;
	this._stats = stats;

	var lo = graphContext.settings.get('LineLabelOptions');
	lo.position = graphContext.settings.get('LineLabelPos');
	lo.rotation = graphContext.settings.get('LineLabelRotation');

	this._positions = [];
	this._percents = [];
	this._segments = this._buildSegments();
	this._buildPoints();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlot.prototype._buildSegments = function() {
	var segments = [];
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var rowCount = dpGrid.getNumXRows();
	var showLabels = sett.get('LineLabelOptions').show;
	var lineColor = sett.get('LineColor');
	var valueSum = 0;
	var valueClimb = 0;
	var i, dp, seg, pos, perc;

	for ( i = 0 ; i < rowCount ; ++i ) {
		dp = dpGrid.getDatapointAt(i, 0, true);
		valueSum += Math.max(0, dp.yValue.numberValue);

		seg = new IDB.LineSegment(gc, dp);
		segments.push(seg);
	}
	
	for ( i = 0 ; i < rowCount ; ++i ) {
		seg = segments[i];
		valueClimb += seg.getDatapoint().yValue.numberValue;
		pos = valueClimb/valueSum;
		perc = Math.round(pos*1000)/10;

		seg.setLineColor(lineColor);
		seg.setLabelText(showLabels ? perc+'%' : null);

		this._positions[i] = pos;
		this._percents[i] = perc;
	}

	return segments;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlot.prototype._buildPoints = function() {
	var segments = this._segments;
	var positions = this._positions;
	var rowCount = segments.length;
	var breakVal = this._graphContext.settings.get('ParetoBreakPercent')/100;
	var breakIndex = -1;
	var i, y0, y1, y2, p0, p1, p2;

	for ( i = 0 ; i < rowCount ; ++i ) {
		y0 = positions[Math.max(0, i-1)];
		y1 = positions[i];
		y2 = positions[Math.min(i+1, rowCount-1)];

		y0 = (y0-y1)/2+y1;
		y2 = (y2-y1)/2+y1;

		p0 = { x: Math.max(0, i-0.5), y: 1-y0 };
		p1 = { x: i, y: 1-y1 };
		p2 = { x: Math.min(i+0.5, rowCount-1), y: 1-y2 };

		segments[i].setPoints(p0, p1, p2, i);

		if ( breakIndex == -1 && y1 >= breakVal && p0.x >= 0 ) {
			breakIndex = i;
		}
	}

	this._breakIndex = breakIndex;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlot.prototype.getSegmentCount = function() {
	return this._segments.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlot.prototype.getSegment = function(i) {
	return this._segments[i];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlot.prototype.getBreakIndex = function() {
	return this._breakIndex;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlot.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('LineLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlot.prototype.getBreakColor = function() {
	return this._graphContext.settings.get('ParetoBreakColor');
};

/*====================================================================================================*/
IDB.ParetoLinePlotView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlotView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();
	this._segments = [];

	var model = this._model;
	var segCount = model.getSegmentCount();
	var lblOpt = model.getLabelOptions();
	var i, segModel, seg, lbl;

	for ( i = 0 ; i < segCount ; ++i ) {
		segModel = model.getSegment(i);

		seg = new IDB.LineSegmentView(segModel);
		this._display.add(seg.getDisplay());

		this._segments[i] = seg;
	}

	if ( lblOpt.show ) {
		this._labelHold = new Kinetic.Group();
		this._display.add(this._labelHold);

		for ( i = 0 ; i < segCount ; ++i ) {
			segModel = model.getSegment(i);

			if ( !segModel.getActive() ) {
				continue;
			}

			lbl = new IDB.GraphLabel(lblOpt, segModel.getLabelText());
			lbl.rotateFromCenter(lblOpt.rotation);
			lbl.getDisplay().listening(false);
			this._labelHold.add(lbl.getDisplay());

			this._segments[i].setLabelRef(lbl);
		}
	}

	this._breakLines = new Kinetic.Group();
	this._display.add(this._breakLines);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlotView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ParetoLinePlotView.prototype.setSize = function(width, height) {
	var model = this._model;
	var segments = this._segments;
	var segCount = segments.length;
	var segW = width/segCount;
	var lblPos = model.getLabelOptions().position;
	var breakLines = this._breakLines;
	var breakIndex = model.getBreakIndex();
	var breakConfig = {
		points: [],
		stroke: model.getBreakColor().css,
		strokeWidth: 1
	};
	var i, j, seg, segModel, segDisp, segX, relDot, lbl, line;

	breakLines.destroyChildren();

	for ( i = 0 ; i < segCount ; ++i ) {
		seg = segments[i];
		segModel = model.getSegment(i);
		segDisp = seg.getDisplay();
		segX = segW*(i+0.5);

		segDisp.x(segX);
		seg.setSize(segW, height);

		lbl = seg.getLabelRef();

		if ( lbl ) {
			relDot = segModel.getRelativeDotPoint();

			lbl.x = segX+relDot.x*segW;
			lbl.y = IDB.GraphLabel.clampYPosition(lbl, relDot.y*height-lblPos, height);
		}

		if ( i == breakIndex ) {
			segX = segW*i;
			
			for ( j = 0 ; j < height ; j += 7 ) {
				breakConfig.points = [segX, j, segX, j+4];
				line = new Kinetic.Line(breakConfig);
				breakLines.add(line);
			}
		}
	}
};

/*====================================================================================================*/

