'use strict';
IDB.AxisSelector = function($div, $graph, axisList) {
    var me = this;
    me._$div = $div;
    me._graph = $graph;
    me._axisList = axisList;
    me._build();
};

IDB.AxisSelector.BH = 25;
IDB.AxisSelector.BW = 25;

IDB.AxisSelector.prototype._build = function() {
    var me = this, I = IDB, IAS = I.AxisSelector,
        BW = IAS.BW, BH = IAS.BH,
        graph = me._graph, gp = graph.gp,
        axisList = me._axisList, 
        display = new Kinetic.Group(),
        label = null, 
        prevBtn = null,
        nextBtn = null,
        btnsH = 0, lblH = 0,
        $div = me._$div,
        onVisualUpdate = IDB.listener(me, "onVisualUpdate"),
        stageLayer = new Kinetic.Layer(), 
        stage = new Kinetic.Stage({
            container: $div[0],
            width: 200,
            height: 50,
            draggable: false,
            visible:false, //becomes visible upon first "refreshDisplay()"
            x: 0,
            y: 0,
            offset: [0, 0]
            });

    me._destroyed = false;
    me._index = 0;
    me._stage = stage;
    me._stageLayer = stageLayer;
    me._redrawFunc = null;
    me._display = display;


    stage.add(stageLayer);
    stageLayer.add(display);

    if(gp("YLabelVisible")) {
        label = new Kinetic.Text({
            fill:gp("YLabelColor").css,
            text:axisList[0].axisName,
            fontSize:gp("YLabelSize"),
            fontStyle:"bold"
        });
        display.add(label);
        lblH = label.height() + 10;
    }

    me._label = label;

    if(axisList.length > 1) {
        prevBtn = new IDB.GraphButtonArrow('', IDB.GraphButtonArrow.LEFT_DIR, '');
        prevBtn.setSize(BW, BH);
        prevBtn.getDisplay().visible(false);
        prevBtn.setEmphasis(true);
        prevBtn.onVisualUpdate(onVisualUpdate);
        prevBtn.onClick(IDB.listener(me, '_onPrevClick'));
        display.add(prevBtn.getDisplay());
    
        nextBtn = new IDB.GraphButtonArrow('', IDB.GraphButtonArrow.RIGHT_DIR, '');
        nextBtn.setSize(BW, BH);
        nextBtn.setEmphasis(true);
        nextBtn.onVisualUpdate(onVisualUpdate);
        nextBtn.onClick(IDB.listener(me, '_onNextClick'));
        display.add(nextBtn.getDisplay());

        btnsH = BH + 10;
    }

    me._prevBtn =  prevBtn;
    me._nextBtn =  nextBtn;
    me._measuredHeight = Math.max(btnsH, lblH);
};


IDB.AxisSelector.prototype._onPrevClick = function() {
    this._setIndex(this._index-1, true);
};

IDB.AxisSelector.prototype._onNextClick = function() {
    this._setIndex(this._index+1, true);
};

IDB.AxisSelector.prototype._setIndex = function(index, notifyGraph) {
    var me = this, graph = me._graph, label = me._label, axisList = me._axisList, 
        numAxes = axisList.length, axis, prevBtn = me._prevBtn, nextBtn = me._nextBtn;
    if(index > -1 && index < numAxes) {
        me._index = index;
        axis = axisList[index];
        if(label) {
            label.text(axis.axisName);
        }
        if(prevBtn) {
            prevBtn.getDisplay().visible(index > 0);
            nextBtn.getDisplay().visible(index < numAxes - 1);
        }
        me.refreshDisplay();
        if(notifyGraph) {
            graph.axisSelected(axis);
        }
    }
};


IDB.AxisSelector.prototype.resize = function() {
    var me = this, I = IDB, IAS = I.AxisSelector,
        BW = IAS.BW, BH = IAS.BH,
        stage = me._stage, 
        prevBtn = me._prevBtn, nextBtn = me._nextBtn, label = me._label, 
        $div = me._$div, w = $div.width(), h = $div.height(), h_ = h/2,
        BY = h_-BH/2;

    stage.width(w).height(h);

    if(prevBtn) {
        prevBtn.getDisplay().x(5).y(BY);
        nextBtn.getDisplay().x(w - 5 - BW).y(BY);
    }

    if(label) {
       label.x(w/2 - label.width()/2).y(h_ - label.height()/2);
    }

    me.refreshDisplay();

};

IDB.AxisSelector.prototype.measuredHeight = function() {
     return this._measuredHeight;
};

IDB.AxisSelector.prototype.onVisualUpdate = function() {
    this.refreshDisplay();
};

IDB.AxisSelector.prototype.refreshDisplay = function() {
   var me = this;
    if ( me._redrawFunc ) {return;}
    this._redrawFunc = function() {
        me._stage.visible(true);
        me._stage.draw();
        me._redrawFunc = null;
    };
    setTimeout(me._redrawFunc, 0);
};


IDB.AxisSelector.prototype.destroy = function() {
    var me = this, stage = me._stage;
    if(me._destroyed) {
        return;
    }
    stage.destroy();
    me._stage = null;
};

/*====================================================================================================*/
// .getDatapointRows() (Array)
IDB.DatapointGrid = function(graph, dataSet, highRangeDisp) {
	highRangeDisp = IDB.ifUndef(highRangeDisp, true);

	this._dataSet = dataSet;
	this._grid = [];
	this._allIndexes = dataSet.getGraphDataColumnIndices(false);
	this._numIndexes = dataSet.getGraphDataColumnIndices(true);
	this._statSets = dataSet.getStatSets();
	this._datapointRows = [];

	var dataRows = dataSet.getDataRows().slice();

	if ( !dataRows || !dataRows.length ) {
		return;
	}

	var axes = this._dataSet.getGraphAxes(false);
	var rowCount = dataRows.length;
	var colCount = dataRows[0].length;
	var yAxesCount = axes.length-1;
	var defRangeList = axes[0].getRangeList();
	var dp;
	var dpRow;
	var row;
	var range;
	var rl;
	var k;
	
	this._rangeLists = new Array(colCount);
			
	for ( var j = 1 ; j <= yAxesCount ; ++j ) {
		rl = axes[j].getRangeList();
		this._rangeLists[this._allIndexes[j]] = (rl.length ? rl : defRangeList);
	}
			
	for ( var i = 0 ; i < rowCount ; ++i ) {
		this._grid.push([]);
		row = dataRows[i];
		dpRow = new IDB.DatapointRow(i, dataSet.axisList, row);
		
		for ( j = 1 ; j <= yAxesCount ; ++j ) {
			k = this._allIndexes[j];
			rl = this._rangeLists[k];

			dp = dpRow.datapoints[k-1];
			dp.axisColor = dp.yAxis.axisColor;
			
			if ( dp.yAxis.dataTypeIsNumber ) {
				range = IDB.RangeHelper.findRangeForValue(
					dp.yValue.numberValue, rl, highRangeDisp);
							
				if ( range ) {
					dp.rangeColor = range.color;
				}
				else if ( rl.length ) {
					var index = 0;
							
					if ( dp.yValue.numberValue < rl[0].val ) {
						dp.valueOutOfRange = -1;
					}
					else {
						dp.valueOutOfRange = 1;
						index = Math.max(0, rl.length-(highRangeDisp ? 1 : 2));
					}
							
					dp.rangeColor = rl[index].color;
				}
			}
					
			this._grid[i][k] = dp;
		}

        this._datapointRows.push(dpRow);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getAllDatapoints = function(numOnly) {
	var grid = this._grid;
	var colIndexes = (numOnly ? this._numIndexes : this._allIndexes);
	var colCount = colIndexes.length;
	var list = [];
	var row, j, k;

	for ( var i in grid ) {
		row = grid[i];

		for ( j = 1 ; j < colCount ; ++j ) {
			k = colIndexes[j];
			list.push(row[k]);
		}
	}

	return list;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getDatapointRows = function() {
	return this._datapointRows;
};
		
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getNumXRows = function() {
	return this._grid.length;
};
		
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getNumYCols = function(numOnly) {
	return (numOnly ? this._numIndexes.length : this._allIndexes.length)-1;
};
		
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getXAxis = function() {
	return this._dataSet.getGraphAxes(false)[0];
};
		
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getYAxis = function(yCol, numOnly) {
	var a = this._dataSet.getGraphAxes(numOnly);
	return (yCol+1 < a.length ? a[yCol+1] : null);
};
		
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getDatapointAt = function(xRow, yCol, numOnly) {
	var row = this._grid[xRow];

	if ( !row ) {
		return null;
	}

	var c = this._getColumnIndex(yCol, numOnly);
	return (c ? row[c] : null);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getDatapointRowAt = function(xRow, numOnly) {
	var n = this.getNumYCols(numOnly);
	var row = [];
	var c, dp;

	for ( c = 0 ; c < n ; ++c ) {
		dp = this.getDatapointAt(xRow, c, numOnly);
		row.push(dp);
	}

	return row;
};
		
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getStatSet = function(axisId) {
	return this._statSets.nullsAreZeroStatSets[axisId];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getStatSetNN = function(axisId) {
	return this._statSets.nonNullValuesStatSets[axisId];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getRangeList = function(yCol, numOnly) {
	var c = this._getColumnIndex(yCol, numOnly);
	return (c ? this._rangeLists[c] : null);
};
		
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.getColorForXValue = function(value) {
	return this._dataSet.getColorForXValue(value);
};
		
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype._getColumnIndex = function(yCol, numOnly) {
	return (numOnly ? this._numIndexes[yCol+1] : this._allIndexes[yCol+1]);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.setDefaultRangeList = function(rangeList) {
	var lists = this._rangeLists;
	var axes = this._dataSet.getGraphAxes(false);
	var xList = axes[0].getRangeList();

	// Stop if the exisitng default (x-axis) range list is provided by the server. This server-provided
	// default takes priority over the incoming one (which is suggested by the graph).

	if ( xList && xList.length && xList[0].rangeId >= 0 ) {
		return;
	}

	// The existing default range list was generated by the client code, not provided by the server.
	// It is safe to replace all instances of this defaut range list. Typically, the incoming list
	// is an alternate range list for "target mode" graphs.

	for ( var i in lists ) {
		if ( lists[i] == xList ) {
			lists[i] = rangeList;
                        axes[i].setDefaultRangeList(rangeList);
		}
	}
        axes[0].setDefaultRangeList(rangeList);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.DatapointGrid.prototype.sortRowsByYValue = function(yCol, numOnly, desc) {
	desc = IDB.ifUndef(desc, true);

	var colIndex = this._getColumnIndex(yCol, numOnly);
	var aVal, bVal;

	this._grid.sort(function(a, b) {
		aVal = a[colIndex].yValue.value;
		bVal = b[colIndex].yValue.value;

		if ( aVal == bVal ) {
			return 0;
		}

		return (aVal < bVal ? 1 : -1)*(desc ? 1 : -1);
	});
};


/*====================================================================================================*/
IDB.GenericShading = function(type, square) {
	this._display = new Kinetic.Group();

	if ( square ) {
		this._buildKineticBox(type);
	}
	else {
		this._buildKineticCircle(type);
	}
};

IDB.GenericShading.FullDimension = 1000;
IDB.GenericShading.TypeMatte = 1;
IDB.GenericShading.TypeGloss = 2;
IDB.GenericShading.TypeChrome = 3;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GenericShading.prototype._buildKineticBox = function(type) {
	var size = IDB.GenericShading.FullDimension;
	var start = { x: 0, y: 0 };
	var end = { x: 0, y: size };

	var r = new Kinetic.Rect({
		width: size,
		height: size,
		fillLinearGradientStartPoint: start,
		fillLinearGradientEndPoint: end,
		fillLinearGradientColorStops: [
			0, IDB.toRGBA(0x0, 0),
			1, IDB.toRGBA(0x0, 0.4)
		]
	});
	this._display.add(r);

	////

	switch ( type ) {
		case IDB.GenericShading.TypeMatte:
			r = new Kinetic.Rect({
				width: size,
				height: size,
				fillLinearGradientStartPoint: start,
				fillLinearGradientEndPoint: end,
				fillLinearGradientColorStops: [
					0, IDB.toRGBA(0xffffff, 0.4),
					160/255, IDB.toRGBA(0xffffff, 0)
				]
			});
			this._display.add(r);
			break;

		case IDB.GenericShading.TypeGloss:
			r = new Kinetic.Rect({
				y: 100,
				width: size,
				height: 250,
				fill: IDB.toRGBA(0xffffff, 0.3)
			});
			this._display.add(r);
			
			r = new Kinetic.Rect({
				y: 800,
				width: size,
				height: 100,
				fill: IDB.toRGBA(0xffffff, 0.15)
			});
			this._display.add(r);
			break;

		case IDB.GenericShading.TypeChrome:
			r = new Kinetic.Rect({
				width: size,
				height: size,
				fillLinearGradientStartPoint: start,
				fillLinearGradientEndPoint: end,
				fillLinearGradientColorStops: [
					0, IDB.toRGBA(0xffffff, 1/2.5),
					80/255, IDB.toRGBA(0x0, 0.4/2.5),
					160/255, IDB.toRGBA(0xffffff, 0.7/2.5),
					225/255, IDB.toRGBA(0x0, 0.5/2.5),
					1, IDB.toRGBA(0xffffff, 0.2/2.5)
				]
			});
			this._display.add(r);
			break;
	}

};

/*----------------------------------------------------------------------------------------------------*/
IDB.GenericShading.prototype._buildKineticCircle = function(type) {
	var rad = IDB.GenericShading.FullDimension/2;
	var gradY = -rad*0.9;
	var config = {
		radius: rad,
		fillRadialGradientStartPointY: gradY,
		fillRadialGradientEndPointY: gradY,
		fillRadialGradientEndRadius: rad*2,
		fillRadialGradientColorStops: [
			32/255, IDB.toRGBA(0x0, 0),
			1, IDB.toRGBA(0x0, 0.4)
		]
	};

	var r = new Kinetic.Circle(config);
	this._display.add(r);

	////

	switch ( type ) {
		case IDB.GenericShading.TypeMatte:
			config.fillRadialGradientColorStops = [
				0, IDB.toRGBA(0xffffff, 0.4),
				192/255, IDB.toRGBA(0xffffff, 0)
			];

			r = new Kinetic.Circle(config);
			this._display.add(r);
			break;

		case IDB.GenericShading.TypeGloss:
			r = new Kinetic.Ellipse({
				y: -rad*0.4,
				width: rad*1.2,
				height: rad*0.8,
				fill: IDB.toRGBA(0xffffff, 0.3)
			});
			this._display.add(r);
			
			r = new Kinetic.Ellipse({
				y: rad*0.7,
				width: rad*0.64,
				height: rad*0.4,
				fill: IDB.toRGBA(0xffffff, 0.15)
			});
			this._display.add(r);
			break;

		case IDB.GenericShading.TypeChrome:
			config.fillRadialGradientColorStops = [
				0, IDB.toRGBA(0xffffff, 1/2.5),
				100/255, IDB.toRGBA(0x0, 0.4/2.5),
				160/255, IDB.toRGBA(0xffffff, 0.7/2.5),
				205/255, IDB.toRGBA(0x0, 0.5/2.5),
				1, IDB.toRGBA(0xffffff, 0.2/2.5)
			];

			r = new Kinetic.Circle(config);
			this._display.add(r);
			break;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GenericShading.prototype.getDisplay = function() {
	return this._display;
};

/*====================================================================================================*/
IDB.GraphButton = function(text) {
	this._text = text;
	this._data = null;
	this._allowAutoRepeat = false;
	this._repeating = false;
	this._timeout = true;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();

	this._box = new Kinetic.Rect({
		strokeWidth: 2
	});
	this._box.on('click', IDB.leftClickListener(this, '_onMouseClick'));
	this._box.on('mousedown', IDB.listener(this, '_onMouseDown'));
	this._box.on('mouseover', IDB.listener(this, '_onMouseOver'));
	this._box.on('mouseout', IDB.listener(this, '_onMouseOut'));
    this._box._touchHandler = new IDB.TouchHandler(this._box);
	this._display.add(this._box);

	var lblOpt = {
		size: 12,
		color: IDB.rgbx('#000'),
		bold: false,
		align: 'center'
	};
	
	this._hover = new Kinetic.Rect({
		fill: '#09c',
		stroke: '#09c',
		strokeWidth: 2,
		opacity: 0.333,
		visible: false
	});
	this._hover.setListening(false);
	this._display.add(this._hover);
	
	this._select = new Kinetic.Rect({
		fill: '#09c',
		stroke: '#09c',
		strokeWidth: 2,
		visible: false
	});
	this._select.setListening(false);
	this._display.add(this._select);

	this._label = new IDB.GraphLabel(lblOpt, '');
	this._label.getDisplay().setListening(false);
	this._display.add(this._label.getDisplay());
	this.setText(this._text || '');

	this.setEmphasis(false);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.getTextWidth = function() {
	return this._label.textWidth;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.isHighlighted = function() {
	return this._highlighted;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.isFaded = function() {
	return this._faded;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.getData = function() {
	return this._data;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.setSize = function(width, height) {
	this._box.x(1).y(1).width(width-2).height(height-2);
	this._hover.x(1).y(1).width(width-2).height(height-2);
	this._select.x(1).y(1).width(width-2).height(height-2);

	this._label.y = (height-this._label.textHeight)/2+1;
	this._label.width = width;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.setText = function(text) {
	this._text = text;
	this._label.text = text;
	this._label.visible = (this._text && this._text.length > 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.setHighlighted = function(highlighted) {
	this._highlighted = highlighted;
	this._select.visible(highlighted);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.setFaded = function(faded) {
	this._display.opacity(faded ? 0.25 : 1.0);
	this._faded = faded;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.setEmphasis = function(emphasis) {
	this._box.fill(emphasis ? '#aaa' : '#ccc');
	this._box.stroke(emphasis ? '#888' : '#aaa');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.setData = function(data) {
	this._data = data;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.allowAutoRepeat = function(allow) {
	this._allowAutoRepeat = allow;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.onVisualUpdate = function(callback) {
	this._onVisualUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.onClick = function(callback) {
	this._clickCallback = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.onMouseOver = function(callback) {
	this._mouseOverCallback = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype.onMouseOut = function(callback) {
	this._mouseOutCallback = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype._onMouseClick = function() {
	this._repeating = false;
	clearTimeout(this._timeout);

	if ( this._clickCallback ) {
		this._clickCallback(this);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype._onMouseDown = function() {
	if ( !this._allowAutoRepeat ) {
		return;
	}

	this._repeating = true;
	this._timeout = setTimeout(IDB.listener(this, '_onMouseDownRepeat'), 500);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype._onMouseDownRepeat = function() {
	if ( !this._repeating || !this._display.visible() || !this._clickCallback ) {
		return;
	}

	this._clickCallback();
	clearTimeout(this._timeout);
	this._timeout = setTimeout(IDB.listener(this, '_onMouseDownRepeat'), 80);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype._onMouseOver = function() {
	this._hover.visible(true);
	this._sendVisualUpdate();

	if ( this._mouseOverCallback ) {
		this._mouseOverCallback(this);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype._onMouseOut = function() {
	this._hover.visible(false);
	this._sendVisualUpdate();

	if ( this._mouseOutCallback ) {
		this._mouseOutCallback(this);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButton.prototype._sendVisualUpdate = function() {
	if ( this._onVisualUpdate ) {
		this._onVisualUpdate();
	}
};
/*====================================================================================================*/
IDB.GraphButtonArrow = function(text, direction, unit, allowScrollAll) {
	this._direction = direction;
	this._unit = unit;
	this._allowScrollAll = IDB.ifUndef(allowScrollAll, true);
	this._type = IDB.GraphButtonArrow.MOVE_ONE;

	IDB.GraphButton.call(this, text);

	this.allowAutoRepeat(true);
};

IDB.GraphButtonArrow.prototype = Object.create(IDB.GraphButton.prototype);
IDB.GraphButtonArrow.prototype.constructor = IDB.GraphButtonArrow;

IDB.GraphButtonArrow.RIGHT_DIR = 1;
IDB.GraphButtonArrow.LEFT_DIR = -1;
IDB.GraphButtonArrow.MOVE_ONE = 'MoveOne';
IDB.GraphButtonArrow.MOVE_PAGE = 'MovePage';
IDB.GraphButtonArrow.MOVE_ALL = 'MoveAll';


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButtonArrow.prototype._buildKinetic = function() {
	IDB.GraphButton.prototype._buildKinetic.call(this);

	this._icon = new Kinetic.Group();
	this._icon.setListening(false);
	this._display.add(this._icon);
	
	$(window).keydown(IDB.listener(this, '_onKeyDown'));
	$(window).keyup(IDB.listener(this, '_onKeyUp'));
	this._onKeyUp();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButtonArrow.prototype.getMoveType = function() {
	return this._type;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButtonArrow.prototype.setSize = function(width, height) {
	IDB.GraphButton.prototype.setSize.call(this, width, height);

	var halfH = height/2;
	var right = (this._direction == IDB.GraphButtonArrow.RIGHT_DIR);
	var iconScale = Math.min(1, height/24);

	this._icon.x(right ? Math.max(width-halfH, width/2) : Math.min(halfH, width/2));
	this._icon.y(halfH);
	this._icon.scaleX(iconScale).scaleY(iconScale);

	this._label.x = (right ? 0 : halfH);
	this._label.y = (height-this._label.textHeight)/2+1;
	this._label.width = width-halfH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButtonArrow.prototype._onKeyDown = function(event) {
	this._ctrlKey = (event.which == 17);
	this._shiftKey = (event.which == 16);
	this._sendVisualUpdate();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButtonArrow.prototype._onKeyUp = function() {
	this._ctrlKey = false;
	this._shiftKey = false;
	this._sendVisualUpdate();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButtonArrow.prototype._updateIcon = function() {
	if ( this._ctrlKey ) {
		this._type = IDB.GraphButtonArrow.MOVE_PAGE;
	}
	else if ( this._allowScrollAll && this._shiftKey ) {
		this._type = IDB.GraphButtonArrow.MOVE_ALL;
	}
	else {
		this._type = IDB.GraphButtonArrow.MOVE_ONE;
	}
	
	var dir = (this._direction == IDB.GraphButtonArrow.RIGHT_DIR ? 1 : -1);
	var w = 8*dir;
	var h = 10;
	var w2 = w/2;
	var h2 = h/2;

	var config = {
		points: [],
		closed: true,
		stroke: '#fff',
		strokeWidth: 0,
		fill: '#fff'
	};

	this._icon.destroyChildren();

	if ( !this._hover.visible() || this._type == IDB.GraphButtonArrow.MOVE_ONE ) {
		config.points = [  -w2,-h2  ,  -w2,h2  ,  w2,0  ];
		this._icon.add(new Kinetic.Line(config));
	}
	else if ( this._type == IDB.GraphButtonArrow.MOVE_PAGE ) {
		config.points = [  -w,-h2  ,  -w,h2  ,  0,0  ];
		this._icon.add(new Kinetic.Line(config));
		
		config.points = [  0,-h2  ,  0,h2  ,  w,0  ];
		this._icon.add(new Kinetic.Line(config));
	}
	else { //moveAll
		config.points = [  -w2,-h2  ,  -w2,h2  ,  w2,0  ];
		this._icon.add(new Kinetic.Line(config));
		
		var boxW = w2+3*dir;
		config.points = [  w2,-h2  ,  w2,h2  ,  boxW,h2  ,  boxW,-h2  ];
		this._icon.add(new Kinetic.Line(config));
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphButtonArrow.prototype._sendVisualUpdate = function() {
	this._updateIcon();
	IDB.GraphButton.prototype._sendVisualUpdate.call(this);
};

/*====================================================================================================*/
IDB.GraphContext = function(settings, datapointGrid, refreshDisplay, onDatapointClicked, 
										onDatapointOver, onDatapointOut, onYAxisSelected, showWarning) {
	this.settings = settings;
	this.datapointGrid = datapointGrid;
	this.refreshDisplay = refreshDisplay;
	this.onDatapointClicked = onDatapointClicked;
	this.onDatapointOver = onDatapointOver;
	this.onDatapointOut = onDatapointOut;
	this.onYAxisSelected = onYAxisSelected;
	this.isOutgoingDatapointEvent = false;
	this.showWarning = showWarning;
	this._datapointsUpdatedListeners = [];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphContext.prototype.notifyDatapointsUpdated = function() {
	var list = this._datapointsUpdatedListeners;

	for ( var i in list ) {
		list[i]();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphContext.prototype.addDatapointsUpdatedListener = function(listener) {
	this._datapointsUpdatedListeners.push(listener);
};

/*====================================================================================================*/
IDB.GraphLabel = function(labelOptions, text) {
	this._options = labelOptions;
	this._initWithText = !IDB.isUndef(text);
	this._fullText = (this._initWithText ? text : '');
	
	this._overlap = false;
	this._offscreen = false;
	
	this._display = new Kinetic.Group();
	this._bg = this._buildBackground();
	this._label = this._buildLabel();

	//LATER: toolTip support
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype._buildBackground = function() {
	var opt = this._options;

	var bg = new Kinetic.Rect({
		x: -2,
		y: -2,
		width: 100,
		height: 100,
		fill: opt.color.css,
		visible: false
	});

	this._display.add(bg);
	return bg;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype._buildLabel = function() {
	var opt = this._options;
	
    var config = {
		fill: opt.color.css,
		fontSize: opt.size,
		fontStyle: (opt.bold ? 'bold' : 'normal'),
		align: (opt.align || 'center'),
		wrap: 'none',
		text: (this._initWithText ? this._fullText : 'X')
	};

    if(opt.shadow) {
        config.shadowEnabled = true;
        config.shadowColor = opt.shadow.color || opt.color.x.css;
        config.shadowOffsetX = IDB.ifUndef(opt.shadow.x, 1);
        config.shadowOffsetY = IDB.ifUndef(opt.shadow.y, 1);
        config.shadowBlur = IDB.ifUndef(opt.shadow.blur, 4);
        config.shadowOpacity = IDB.ifUndef(opt.shadow.opacity, 1);
    };

	var lbl = new Kinetic.Text(config);

	if ( !this._initWithText ) {
		lbl.text('');
	}

        if(opt.clickable) {
            lbl.on('click', IDB.leftClickListener(this, '_onMouseClick'));
            lbl.on('mouseover', IDB.listener(this, '_onMouseOver'));
            lbl.on('mouseout', IDB.listener(this, '_onMouseOut'));
        }

	this._display.add(lbl);
	return lbl;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.getDisplayLabel = function() {
	return this._label;
};

/*----------------------------------------------------------------------------------------------------*/
Object.defineProperties(IDB.GraphLabel.prototype, {
	text: {
		get: function() {
			return this._label.text();
		},
		set: function(text) {
			this._fullText = text;
			this._label.text(text);
		}
	},

	length: {
		get: function() {
			return this.text.length;
		}
	},

	textWidth: {
		get: function() {
			var lbl = this._label;
			var w = lbl.width();
			lbl.width(9999);

			var tw = lbl.getTextWidth();
			lbl.width(w);
			return tw;
		}
	},

	textHeight: {
		get: function() {
			return this._label.getTextHeight();
		}
	},
	
	width: {
		get: function() {
			return this._label.width()*this._display.scaleX();
		},
		set: function(width) {
			this._label.width(width);
			this._updateBg();
		}
	},
	
	height: {
		get: function() {
			return this._label.height()*this._display.scaleY();
		},
		set: function(height) {
			this._label.height(height);
			this._updateBg();
		}
	},
	
	scaleX: {
		get: function() {
			return this._display.scaleX();
		},
		set: function(scaleX) {
			this._display.scaleX(scaleX);
		}
	},
	
	scaleY: {
		get: function() {
			return this._display.scaleY();
		},
		set: function(scaleY) {
			this._display.scaleY(scaleY);
		}
	},
	
	visible: {
		get: function() {
			return this._display.visible();
		},
		set: function(visible) {
			this._display.visible(visible);
		}
	},
	
	x: {
		get: function() {
			return this._display.x();
		},
		set: function(x) {
			this._display.x(x);
		}
	},
	
	y: {
		get: function() {
			return this._display.y();
		},
		set: function(y) {
			this._display.y(y);
		}
	},
	
	wordWrap: {
		get: function() {
			return (this._label.wrap() != 'none');
		},
		set: function(wrap) {
			this._label.wrap(wrap ? 'word' : 'none');
		}
	}

});

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.getFullText = function() {
	return this._fullText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.resetText = function() {
	this.text = this._fullText;
};

/*IDB.GraphLabel.TruncTimeSum = 0;
IDB.GraphLabel.TruncCount = 0;
IDB.GraphLabel.TruncErrSum = 0;
IDB.GraphLabel.TruncUnderCount = 0;
IDB.GraphLabel.TruncOverCount = 0;*/

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.truncate = function() {
	var lbl = this._label;
	var origW = lbl.width();
	var ft = this._fullText+'';

	lbl.width(9999);
	lbl.text(ft);

	if ( lbl.getTextWidth() <= origW ) {
		lbl.width(origW);
		return false;
	}

	//Pre-optimization:  average 0.65 ms per truncation
	//Post-optimization: average 0.34 ms per truncation

	//var t0 = window.performance.now();
	var str = ft+'';

	/// ORIGINAL

	/*while ( str.length && lbl.getTextWidth() > origW ) {
		str = ft.substr(0, str.length-1);
		lbl.text(str+'...');
	}*/

	/// OPTIMIZED

	var avgCharW = lbl.getTextWidth()/ft.length;
	var startCharCount = Math.round(origW/avgCharW-3); //subtract 3 to compensate for the ellipsis
	var prevStrFit = false;

	str = str.substr(0, startCharCount);
	lbl.text(str+'...');

	while ( lbl.getTextWidth() < origW ) { //add characters until it overflows
		str = ft.substr(0, str.length+1);
		lbl.text(str+'...');
		prevStrFit = true;
	}

	var strLen = str.length;

	if ( prevStrFit ) { //this scenario occurs > 90% of the time
		str = ft.substr(0, strLen-1);
		lbl.text(str+'...');
		//IDB.GraphLabel.TruncUnderCount++;
	}
	else {
		while ( strLen && lbl.getTextWidth() > origW ) { //remove chars until it does not overflow
			strLen = str.length-1;
			str = ft.substr(0, strLen);
			lbl.text(str+'...');
		}

		//IDB.GraphLabel.TruncOverCount++;
	}

	///
	
	lbl.width(origW);

	/*var t1 = window.performance.now();
	IDB.GraphLabel.TruncTimeSum += t1-t0;
	IDB.GraphLabel.TruncErrSum += str.length-startCharCount;
	IDB.GraphLabel.TruncCount++;

	if ( (IDB.GraphLabel.TruncCount%1000) == 0 ) {
		alert(
			"Truncation Results\n\n"+
			IDB.GraphLabel.TruncCount+" t\n\n"+
			(IDB.GraphLabel.TruncTimeSum/IDB.GraphLabel.TruncCount)+" ms/t\n\n"+
			(IDB.GraphLabel.TruncErrSum/IDB.GraphLabel.TruncCount)+" err/t\n\n"+
			(IDB.GraphLabel.TruncUnderCount/IDB.GraphLabel.TruncCount)+" under %\n\n"+
			(IDB.GraphLabel.TruncOverCount/IDB.GraphLabel.TruncCount)+" over %"
		);
	}*/

	return true;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.rotateFromCenter = function(degrees) {
	var label = this._label;

	var offset = {
		x: label.getTextWidth()/2,
		y: label.getTextHeight()/2
	};

	label
		.offset(offset)
		.rotation(degrees);
	
	offset.x = (label.width()+4)/2;
	offset.y = (label.height()+4)/2;

	this._bg
		.x(0)
		.y(0)
		.offset(offset)
		.rotation(degrees);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.calculateBounds = function() {
	var lbl = this._label;

	return IDB.MathUtil.calculateRotatedBounds(
		lbl.getTextWidth(), lbl.getTextHeight(), lbl.rotation());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.getHighlight = function() {
	return this._bg.visible();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.getOffscreen = function() {
	return this._offscreen;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.getOverlap = function() {
	return this._overlap;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.setHighlight = function(highlight) {
	this._bg.visible(highlight);
	var col = this._options.color;

	if ( highlight ) {
		this._updateBg();
		this._label.fill(col.x.css).opacity(0.9);
	}
	else {
		this._label.fill(col.css).opacity(1);
	}

	this._updateVisible();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.setOffscreen = function(offscreen) {
	this._offscreen = offscreen;
	this._updateVisible();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.setOverlap = function(overlap) {
	this._overlap = overlap;
	this._updateVisible();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype._updateBg = function() {
	this._bg
		.width(this._label.width()+4)
		.height(this._label.height()+4);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype._updateVisible = function() {
	this._display.visible((this._bg.visible() || !this._overlap) && !this._offscreen);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.clampYPosition = function(label, y, containerHeight) {
	var halfH = label.calculateBounds().height/2;

	if ( y+halfH > containerHeight ) {
		y = containerHeight-halfH;
	}
	else if ( y-halfH < 0 ) {
		y = halfH;
	}

	return y;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.onClick = function(callback) {
	this._clickCallback = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.onMouseOver = function(callback) {
	this._mouseOverCallback = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype.onMouseOut = function(callback) {
	this._mouseOutCallback = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype._onMouseClick = function() {
	if ( this._clickCallback ) {
		this._clickCallback(this);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype._onMouseOver = function() {
	if ( this._mouseOverCallback ) {
		this._mouseOverCallback(this);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLabel.prototype._onMouseOut = function() {
	if ( this._mouseOutCallback ) {
		this._mouseOutCallback(this);
	}
};

/*====================================================================================================*/
IDB.GraphLang = {};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLang.get = function(key, values) {
	var text = IDB.GraphLang.Map[key];

	if ( !text || !values ) {
		return text;
	}

	for ( var i in values ) {
		text = text.replace(new RegExp('\\{'+i+'}', 'g'), values[i]);
	}

	return text;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphLang.Map = {

	LineWarningTitle: 'Scatter Graph Warning',
	LineWarning: 'Regression line cannot be drawn for "{0}", as it requires at least two data points.',

	PieWarningTitle: 'Pie Graph Warning',
	PieNegativeWarning: 'Negative values have been omitted from the "{0}" Pie graph.',
	PieRowsWarning: 'A pie chart can display a maximum of {0} rows. The current '+
		'data set contains {1}, of which only the first {0} are shown.',

	BubbleAxisWarningTitle: 'Bubble Graph Warning',
	BubbleAxisWarning: 'A bubble chart can only display data for three numeric '+
		'Y data columns. The data set for this chart contains {0}, therefore '+
		'those beyond the first three will be ignored.',

	ReportExpandAll: 'Expand All',
	ReportCollapseAll: 'Collapse All',

	PyramidName: 'Pyramid',
	FunnelName: 'Funnel',
	ConeName: 'Cone',
	PyramidWarningTitle: 'Pyramid Graph Warning',
	PyramidNegativeWarning: 'Negative values have been omitted from the "{0}" {1} graph.',
	PyramidRowsWarning: 'A {2} graph can display a maximum of {0} rows. The current ' +
		'data set contains {1}, of which only the first {0} are shown.',

	CalendarDateWarningTitle: 'Calendar X-Axis Error',
	CalendarDateWarning: 'The Calendar Graph requires that the x-axis has a "DateTime" data type.',
	CalendarBaseWarningTitle: 'Calendar Custom Date Error',
	CalendarBaseWarning: 'The "Custom Date" value must be a valid date in yyyy-MM-dd format.  The '+
		'calendar\'s "Base Position" value will default to today\'s date.',
	CalendarDayLetters: 'S,M,T,W,T,F,S',
	CalendarDayAbbrevs: 'Sun,Mon,Tue,Wed,Thu,Fri,Sat',
	CalendarDayNames: 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday',
	CalendarNoNumAxes: 'No numeric axes available.',
	
	TreemapWarningTitle: 'Treemap Graph Warning',
	TreemapNegativeWarning: 'All values not greater than zero have been omitted from the Treemap graph.'
};
/*====================================================================================================*/
IDB.GraphOdom = function(settings, numberPlaces, labelText, percentMode) {
	this._settings = settings;
	this._labelText = labelText;

	this._currVal = 0;
	this._intPlaces = numberPlaces;
	this._decPlaces = this._settings.get('OdomNumDecimals');
	this._intDigits = [];
	this._decDigits = [];

	var sett = this._settings;
	var i, d;

	this._signDigit = new IDB.GraphOdomDigit(sett, '-');
	this._dotDigit = new IDB.GraphOdomDigit(sett, '.');

	if ( percentMode ) {
		this._percDigit = new IDB.GraphOdomDigit(sett, '%');
	}

	for ( i = 0 ; i < this._intPlaces ; ++i ) {
		d = new IDB.GraphOdomDigit(sett, '0');
		this._intDigits.push(d);
	}
	
	for ( i = 0 ; i < this._decPlaces ; ++i ) {
		d = new IDB.GraphOdomDigit(sett, '0');
		this._decDigits.push(d);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.setCurrValue = function(value) {
    value = value || 0;
    if(!isFinite(value)) {
        value = 0;    
    }
	this._currVal = value;
	this._signDigit.setValueHidden(value >= 0);

	value = Math.abs(value);
	
	var num = Math.floor(value);
	var dec = value-num;
	var mult = Math.pow(10, this._decDigits.length);

	dec = Math.round(dec*mult)/mult;
	var decStr = Math.round(dec*mult)+"";
	
	if ( dec == 1 ) {
		num += 1;
		decStr = '';
	}
	
	while ( decStr.length < this._decPlaces ) {
		decStr = '0'+decStr; //pad with leading zeros
	}

	var intDigs = this._intDigits;
	var decDigs = this._decDigits;
	var intDigsLen = intDigs.length;
	var decDigsLen = decDigs.length;

	for ( var i = 0 ; i < intDigsLen ; ++i ) {
		intDigs[i].setValue(num%10);
		num = Math.floor(num/10);
	}
	
	for ( i = 0 ; i < decDigsLen ; ++i ) {
		decDigs[i].setValue(Math.floor(decStr.charAt(i)));
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.getIntegerPlacesByValue = function(value) { //static
	var s = Math.abs(value)+'';
	var i = s.indexOf('.');
	return (i == -1 ? s.length : i);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getLabelText = function() {
	return this._labelText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getIntegerPlaces = function() {
	return this._intPlaces;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getDecimalPlaces = function() {
	return this._decPlaces;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getIntegerDigit = function(index) {
	return this._intDigits[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getDecimalDigit = function(index) {
	return this._decDigits[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getSignDigit = function() {
	return this._signDigit;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getDotDigit = function() {
	return this._dotDigit;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getPercentDigit = function() {
	return this._percDigit;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdom.prototype.getLabelOptions = function() {
	return this._settings.get('OdomLabelOptions');
};
/* global IDB */
/*====================================================================================================*/
IDB.GraphOdomDigit = function(settings, value) {
	this._settings = settings;
	this._value = value;
	this._hidden = false;
	this._cellMap = this._buildCellMap();
};

IDB.GraphOdomDigit.StyleOdom = 0;
IDB.GraphOdomDigit.StyleLcd = 1;
IDB.GraphOdomDigit.StyleCalc = 2;
IDB.GraphOdomDigit.StyleScoreA = 3;		
IDB.GraphOdomDigit.StyleScoreB = 4;

IDB.GraphOdomDigit._LcdCellMap = {
	'0': [1,1,1, 0, 1,1,1],
	'1': [0,0,1, 0, 0,1,0],
	'2': [1,0,1, 1, 1,0,1],
	'3': [1,0,1, 1, 0,1,1],
	'4': [0,1,1, 1, 0,1,0],
	'5': [1,1,0, 1, 0,1,1],
	'6': [1,1,0, 1, 1,1,1],
	'7': [1,0,1, 0, 0,1,0],
	'8': [1,1,1, 1, 1,1,1],
	'9': [1,1,1, 1, 0,1,1],
	'-': [0,0,0, 1, 0,0,0],
	'.': [], //special
	'%': []  //special
};

IDB.GraphOdomDigit._CalcCellMap = {
	'0': [0,1,1,1,0, 1,0,0,0,1, 1,0,0,1,1, 1,0,1,0,1, 1,1,0,0,1, 1,0,0,0,1, 0,1,1,1,0],
	'1': [0,0,1,0,0, 0,1,1,0,0, 0,0,1,0,0, 0,0,1,0,0, 0,0,1,0,0, 0,0,1,0,0, 0,1,1,1,0],
	'2': [0,1,1,1,0, 1,0,0,0,1, 0,0,0,0,1, 0,0,0,1,0, 0,0,1,0,0, 0,1,0,0,0, 1,1,1,1,1],
	'3': [1,1,1,1,1, 0,0,0,1,0, 0,0,1,0,0, 0,0,0,1,0, 0,0,0,0,1, 1,0,0,0,1, 0,1,1,1,0],
	'4': [0,0,0,1,0, 0,0,1,1,0, 0,1,0,1,0, 1,0,0,1,0, 1,1,1,1,1, 0,0,0,1,0, 0,0,0,1,0],
	'5': [1,1,1,1,1, 1,0,0,0,0, 1,1,1,1,0, 0,0,0,0,1, 0,0,0,0,1, 1,0,0,0,1, 0,1,1,1,0],
	'6': [0,0,1,1,0, 0,1,0,0,0, 1,0,0,0,0, 1,1,1,1,0, 1,0,0,0,1, 1,0,0,0,1, 0,1,1,1,0],
	'7': [1,1,1,1,1, 0,0,0,0,1, 0,0,0,1,0, 0,0,1,0,0, 0,1,0,0,0, 0,1,0,0,0, 0,1,0,0,0],
	'8': [0,1,1,1,0, 1,0,0,0,1, 1,0,0,0,1, 0,1,1,1,0, 1,0,0,0,1, 1,0,0,0,1, 0,1,1,1,0],
	'9': [0,1,1,1,0, 1,0,0,0,1, 1,0,0,0,1, 0,1,1,1,1, 0,0,0,0,1, 0,0,0,1,0, 0,1,1,0,0],
	'.': [0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,1,1,0,0, 0,1,1,0,0],
	'-': [0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,1,1,1,1, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0],
	'%': [1,1,0,0,0, 1,1,0,0,1, 0,0,0,1,0, 0,0,1,0,0, 0,1,0,0,0, 1,0,0,1,1, 0,0,0,1,1]
};

IDB.GraphOdomDigit._ScoreACellMap = {
	'0': [1,1,1, 1,1, 1,0,1, 1,1, 1,1,1],
	'1': [0,0,1, 0,1, 0,0,1, 0,1, 0,0,1],
	'2': [1,1,1, 0,1, 1,1,1, 1,0, 1,1,1],
	'3': [1,1,1, 0,1, 0,1,1, 0,1, 1,1,1],
	'4': [0,0,1, 1,1, 1,1,1, 0,1, 0,0,1],
	'5': [1,1,1, 1,0, 1,1,1, 0,1, 1,1,1],
	'6': [1,1,1, 1,0, 1,1,1, 1,1, 1,1,1],
	'7': [1,1,1, 0,1, 0,0,1, 0,1, 0,0,1],
	'8': [1,1,1, 1,1, 1,1,1, 1,1, 1,1,1],
	'9': [1,1,1, 1,1, 1,1,1, 0,1, 0,0,1],
	'.': [0,0,0, 0,0, 0,0,0, 0,0, 0,1,0],
	'-': [0,0,0, 0,0, 1,1,1, 0,0, 0,0,0],
	'%': [1,0,1, 0,1, 0,1,0, 1,0, 1,0,1]
};		

IDB.GraphOdomDigit._ScoreBCellMap = {
	'0': [1,1,1,1, 1,1,1,1, 1,0,0,1, 1,1,1,1, 1,1,1,1],
	'1': [0,0,0,1, 0,1,0,1, 0,0,0,1, 0,1,0,1, 0,0,0,1],
	'2': [1,1,1,1, 0,1,0,1, 1,1,1,1, 1,0,1,0, 1,1,1,1],
	'3': [1,1,1,1, 0,1,0,1, 0,1,1,1, 0,1,0,1, 1,1,1,1],
	'4': [0,0,0,1, 1,1,1,1, 1,1,1,1, 0,1,0,1, 0,0,0,1],
	'5': [1,1,1,1, 1,0,1,0, 1,1,1,1, 0,1,0,1, 1,1,1,1],
	'6': [1,1,1,1, 1,0,1,0, 1,1,1,1, 1,1,1,1, 1,1,1,1],
	'7': [1,1,1,1, 0,1,0,1, 0,0,0,1, 0,1,0,1, 0,0,0,1],
	'8': [1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1],
	'9': [1,1,1,1, 1,1,1,1, 1,1,1,1, 0,1,0,1, 0,0,0,1],
	'.': [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,1,0],
	'-': [0,0,0,0, 0,0,0,0, 1,1,1,1, 0,0,0,0, 0,0,0,0],
	'%': [1,0,0,0, 0,1,0,1, 0,1,1,0, 1,0,1,0, 0,0,0,1]
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype._buildCellMap = function() {
	switch ( this.getStyle() ) {
		case IDB.GraphOdomDigit.StyleLcd:
			return IDB.GraphOdomDigit._LcdCellMap;
			
		case IDB.GraphOdomDigit.StyleCalc:
			return IDB.GraphOdomDigit._CalcCellMap;
			
		case IDB.GraphOdomDigit.StyleScoreA:
			return IDB.GraphOdomDigit._ScoreACellMap;
			
		case IDB.GraphOdomDigit.StyleScoreB:
			return IDB.GraphOdomDigit._ScoreBCellMap;
	}

	return null;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.setOnValueUpdate = function(callback) {
	this._onValueUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.setValue = function(value) {
	var prevVal = this._cellMap === null ? this._value : this._cellMap[this._value];
	this._value = value+'';

	if ( this._onValueUpdate ) {
		this._onValueUpdate(prevVal);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.setValueHidden = function(hidden) {
	this._hidden = hidden;
	
	if ( this._onValueUpdate ) {
		this._onValueUpdate();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.getLabelText = function() {
	return this._labelText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.getValue = function() {
	return this._value;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.isValueHidden = function() {
	return this._hidden;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.getValueCells = function() {
	return this._cellMap[this._value];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.getStyle = function() {
	return this._settings.get('OdomStyle');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.getFaceColor = function() {
	return this._settings.get('OdomFaceColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.getDigitColor = function() {
	return this._settings.get('OdomDigitColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigit.prototype.getDigitSize = function() {
	return this._settings.get('OdomDigitSize');
};
/* global IDB, Kinetic, $ */
/*====================================================================================================*/
IDB.GraphOdomDigitView = function(model) {
	this._model = model;
	model.setOnValueUpdate(IDB.listener(this, '_onValueUpdate'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
Object.defineProperties(IDB.GraphOdomDigitView.prototype,
{
	isAnimationEnabled:
	{
		enumerable: true,
		configurable: false,
		get: function()
		{
			return this._animationsEnabled;
		},
		set: function(val)
		{
			this._animationsEnabled = !!val;
		}
	},
	isAnimating:
	{
		enumerable: true,
		configurable: false,
		get: function()
		{
			var me = this, animation = me._animation;
			return animation !== null && animation.isRunning();
		},
		set: function(val)
		{
			var me = this, animation = me._animation;
			if (val === false)
			{
				if (animation !== null)
				{
					if (animation.isRunning && animation.isRunning())
					{
						animation.stop();
					}
				}
				me._animation = null;
			}
		}
	},
//	animationParametersChanged: {
//		enumerable: true,
//		configurable: false,
//		get: function()
//		{
//			var me = this, changed = me._paramsChangedFlag;
//			me._paramsChangedFlag = false;
//			return !!changed;
//		}
//	},
	_animation: {
		enumerable: false,
		value: null,
		configurable: false,
		writable: true
	},
	_animationsEnabled: {
		enumerable: false,
		value: false,
		configurable: false,
		writable: true
	},
//	_paramsChangedFlag: {
//		value: false,
//		enumerable: false,
//		configurable: false,
//		writable: true
//	},
//	_animationParameters: {
//		value: null,
//		enumerable: false,
//		configurable: false,
//		writable: true
//	}
});


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._buildKinetic = function() {
	var me = this, model = me._model ? me._model : null, settings = model ? model._settings || null : null,
		chartData = settings ? settings._chartData || null : null, animated = chartData ? chartData.animated || false : false;

	me.isAnimationEnabled = animated;

	this._display = new Kinetic.Group();

	this._digitColor = model.getDigitColor();
	this._digitColorCss = this._digitColor.css;

	switch ( model.getStyle() ) {
		case IDB.GraphOdomDigit.StyleOdom:
			this._buildLabel();
			break;

		case IDB.GraphOdomDigit.StyleLcd:
			this._buildLcd();
			break;

		case IDB.GraphOdomDigit.StyleCalc:
			this._buildCalc();
			break;

		case IDB.GraphOdomDigit.StyleScoreA:
			this._buildScoreA();
			break;

		case IDB.GraphOdomDigit.StyleScoreB:
			this._buildScoreB();
			break;
	}

	this._face = new Kinetic.Rect({
		width: this._width,
		height: this._height,
		fill: model.getFaceColor().css
	});
	this._display.add(this._face);
	this._face.moveToBottom();

	this._onValueUpdate();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._buildLabel = function() {
	var me = this, model = me._model;

	var lblOpt = {
		show: true,
		size: model.getDigitSize(),
		color: this._digitColor,
		align: 'center'
	};

	var val = model.getValue();
	if (me.isAnimationEnabled && (val === '' || isNaN(val)))
	{
		me.isAnimationEnabled = false;
	}

	this._label = new IDB.GraphLabel(lblOpt, val);
	this._label.x = 3;
	this._label.y = 3;
	this._display.add(this._label.getDisplay());

	this._width = this._label.width + 6;
	this._height = this._label.height + 4;
	if (me.isAnimationEnabled)
	{
		me._display.setClipWidth(me._width);
		me._display.setClipHeight(me._height);
		me._display.setClipX(me._display.getX());
		me._display.setClipY(me._display.getY());
		me._clippedLabel = new IDB.GraphLabel(lblOpt, '0');
		me._clippedLabel.x = 3;
		me._clippedLabel.y = -me._height + 2;
		me._clippedLabel.visible = false;
		me._display.add(me._clippedLabel.getDisplay());
	}

	this._shade = new Kinetic.Rect({
		x: this._width*0.02,
		width: this._width*0.96,
		height: this._height,
		fillLinearGradientStartPointY: 0,
		fillLinearGradientEndPointY: this._height,
		fillLinearGradientColorStops: [
			0.0, 'rgba(255, 255, 255, 0.75)',
			0.49, 'rgba(255, 255, 255, 0.0)',
			0.5, 'rgba(0, 0, 0, 0.0)',
			1.0, 'rgba(0, 0, 0, 0.5)'
		]
	});
	this._display.add(this._shade);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._buildLcd = function() {
	var model = this._model;
	
	this._height = model.getDigitSize()+3;
	this._width = this._height*(68/112);
	this._lcdFullWidth = this._width;
	this._lcdDotWidth = this._width*0.45;
	this._cells = [];

	this._lcdCellHold = new Kinetic.Group();
	this._display.add(this._lcdCellHold);
	
	this._lcdDotHold = new Kinetic.Group();
	this._display.add(this._lcdDotHold);
	
	this._lcdPercHold = new Kinetic.Group();
	this._display.add(this._lcdPercHold);

	var scale = this._height/112;
	var fullPoints = [
		-20*scale,  0*scale,
		-14*scale,  6*scale,
		 14*scale,  6*scale,
		 20*scale,  0*scale,
		 14*scale, -6*scale,
		-14*scale, -6*scale
	];
	var j = 0;
	var i, a, cell;

	// Cells

	for ( i = 0 ; i < 7 ; ++i ) {
		cell = new Kinetic.Line({
			points: fullPoints,
			fill: this._digitColorCss,
			closed: true
		});

		switch ( i ) {
			case 0:
				cell.x(34*scale).y(12*scale);
				break;
				
			case 1:
				cell.x(12*scale).y(34*scale).rotation(-90);
				break;
				
			case 2:
				cell.x(56*scale).y(34*scale).rotation(90);
				break;
				
			case 3:
				cell.x(34*scale).y(56*scale);
				break;
				
			case 4:
				cell.x(12*scale).y(78*scale).rotation(-90);
				break;
				
			case 5:
				cell.x(56*scale).y(78*scale).rotation(90);
				break;
				
			case 6:
				cell.x(34*scale).y(100*scale);
				break;
		}

		this._lcdCellHold.add(cell);
		this._cells.push(cell);
				
		++j;
	}

	// Percent

	fullPoints = [
		-10*scale,  0*scale,
		 -4*scale,  6*scale,
		  4*scale,  6*scale,
		 10*scale,  0*scale,
		  4*scale, -6*scale,
		 -4*scale, -6*scale
	];

	for ( a = 0 ; a < 2 ; ++a ) {
		for ( i = 0 ; i < 4 ; ++i ) {
			cell = new Kinetic.Line({
				points: fullPoints,
				fill: this._digitColorCss,
				closed: true
			});

			switch ( i ) {
				case 0:
					cell.x(24*scale).y(12*scale);
					break;
				
				case 1:
					cell.x(12*scale).y(24*scale).rotation(-90);
					break;
				
				case 2:
					cell.x(36*scale).y(24*scale).rotation(90);
					break;
				
				case 3:
					cell.x(24*scale).y(36*scale);
					break;
			}

			if ( a == 1 ) {
				cell.x(cell.x()+20*scale).y(cell.y()+64*scale);
			}

			this._lcdPercHold.add(cell);
		}
	}

	cell = new Kinetic.Line({
		points: [
			-39*scale,  0*scale,
			-33*scale,  6*scale,
			 33*scale,  6*scale,
			 39*scale,  0*scale,
			 33*scale, -6*scale,
			-33*scale, -6*scale
		],
		x: 34*scale,
		y: 56*scale,
		rotation: -45,
		fill: this._digitColorCss,
		closed: true
	});
	this._lcdPercHold.add(cell);

	// Dot

	var w = 68*0.45;

	cell = new Kinetic.Circle({
		radius: (w/2-6)*scale,
		x: w/2*scale,
		y: (112-w/2)*scale,
		fill: this._digitColorCss
	});
	this._lcdDotHold.add(cell);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._buildCalc = function() {
	var model = this._model;
	
	this._height = model.getDigitSize()+3;
	this._width = this._height*(5/7);
	this._cells = [];

	var scale = this._height/173;
	var j = 0;
	var i, cell;

	for ( i = 0 ; i < 35 ; ++i ) {
		cell = new Kinetic.Rect({
			x: (6+23*(j%5))*scale,
			y: (6+23*Math.floor(j/5))*scale,
			width: 20*scale,
			height: 20*scale,
			fill: this._digitColorCss
		});
		this._display.add(cell);
		this._cells.push(cell);
				
		++j;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._buildScoreA = function() {
	var model = this._model;
	
	this._height = model.getDigitSize()+3;
	this._width = this._height*0.6;
	this._cells = [];

	var scale = this._height/120;
	var j = 0;
	var i, cell;

	for ( i = 0 ; i < 13 ; ++i ) {
		cell = new Kinetic.Circle({
			x: (15+22*(j%3))*scale,
			y: (15+22*Math.floor(j/3))*scale,
			radius: 10*scale,
			fill: this._digitColorCss
		});
		this._display.add(cell);
		this._cells.push(cell);
				
		++j;

		if ( i == 3 || i == 8 ) {
			++j;
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._buildScoreB = function() {
	var model = this._model;
	
	this._height = model.getDigitSize()+3;
	this._width = this._height*0.6;
	this._cells = [];

	var scale = this._height/166;
	var j = 0;
	var i, cell;

	for ( i = 0 ; i < 20 ; ++i ) {
		cell = new Kinetic.Circle({
			x: (16+22*(j%4))*scale,
			y: (16+22*Math.floor(j/4))*scale,
			radius: 10*scale,
			fill: this._digitColorCss
		});
		this._display.add(cell);
		this._cells.push(cell);
				
		++j;

		if ( i == 4 || i == 6 || i == 12 || i == 14 ) {
			j += 2;
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._onValueUpdate = function(prevValue) {
	var me = this, model = me._model;
	var hide = model.isValueHidden();

	if ( this._label ) {
		if (me.isAnimationEnabled && !hide)
		{
			me._runAnimation(me._makeAnimationObj(prevValue, model.getValue()));
		}
		else if (me.isAnimationEnabled)
		{
			me.isAnimating = false;
			me._label.y = 3;
			if (me._clippedLabel)
			{
				me._clippedLabel.y = -me._height + 2;
				me._clippedLabel.visible = false;
			}
			me._label.text = '';
		}
		else
		{
			this._label.text = (hide ? '' : model.getValue());
		}
	}

	if ( this._cells ) {
		var values = model.getValueCells();
		var len = this._cells.length;
		if (me.isAnimationEnabled && (!this._lcdCellHold || ['.', '%'].indexOf(model.getValue())===-1))
		{
			if (hide) values = Array.apply(null, Array(len)).map(Number.prototype.valueOf, 0);
			if (me.isAnimating)
			{
				me.isAnimating = false;
//				me._updateAnimationParameters(values);
			}
//			else
//			{
				me._runAnimation(me._makeCellAnimationObj(prevValue, values));
//			}
		}
		else
		{

			for ( var i = 0 ; i < len ; ++i ) {
				this._cells[i].opacity(values[i] == 1 && !hide ? 1.0 : 0.1);
			}
		}
	}

	if ( this._lcdCellHold ) {
		var val = model.getValue();

		this._lcdCellHold.visible(val != '.' && val != '%');
		this._lcdDotHold.visible(val == '.');
		this._lcdPercHold.visible(val == '%');

		this._width = (this._lcdDotHold.visible() ? this._lcdDotWidth : this._lcdFullWidth);
		this._face.width(this._width);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._setAnimation = function(animationObj)
{
	var me = this, anim = me._animation;
	if (me.isAnimating && ((Object.hasOwnProperty('is') && !Object.is(anim, animationObj)) || (!Object.hasOwnProperty('is') && anim!==animationObj)))
	{
		if (anim.isRunning()) anim.stop();
	}
	me._animation = animationObj;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._runAnimation = function(animationObj)
{
	var me = this;
	if (animationObj!==undefined)
	{
		me._setAnimation(animationObj);
	}
	if (me._animation && me._animation.start)
	{
		if (me._clippedLabel) me._clippedLabel.visible = true;
		me._animation.start();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._makeCellAnimationObj = function(startVals, endVals)
{
	startVals = !!!startVals || !(startVals instanceof Array) ? Array.apply(null, Array(endVals.length)).map(Number.prototype.valueOf, 0) : startVals;

	var me = this, disp = me._display, layer = disp.getLayer(), cells = me._cells, ncells = cells.length,
		curVals = startVals.slice(),
		cellStates = $(endVals).map(function(i, v){ return { idx: i, dir: v, timeShift:0};}).filter(function(i, cell){ return cell.dir !== curVals[cell.idx]; }).toArray(),
		numCells = cellStates.length,
	    cubed = function(n) { function lx3(x) { return Math.log(x)*3; } return Math.exp(lx3(n)); }, // Firfox, IE, and Chrome all seem to agree that this is the fastest solution
	    uncubed = function(c) { function ld3(x) { return Math.log(x)/3; } return Math.exp(ld3(c)); },
		easing = function(p) { return 1 - cubed(1-p); },
		lowVal = 0.1, highVal = 1.0, range = highVal - lowVal,
		ptInRange = function(p, dir) { return dir===1 ? p * range + lowVal : highVal - p * range; },
		dur = 100,
		timeFromPt = function(pt, dir) { return dur *  (1 - uncubed(1 - (dir===1 ? pt-lowVal : highVal - pt)/range)); };

	if (numCells===0) // No digit change
	{
		return new Kinetic.Animation(function()
		{
			me.isAnimating = false;
			for (var i = 0; i<ncells; ++i)
			{
				cells[i].setOpacity(endVals[i]||0.1);
			}
		}, layer);
	}

	return new Kinetic.Animation(function(frame)
	{
//		if (me.animationParametersChanged)
//		{
//			endVals = me._animationParameters;
//			ncells = endVals.length;
//			cellStates = $(endVals).map(function(i,v){
//				return {idx: i, dir: v, timeShift:curVals[i]>lowVal ? timeFromPt(ptInRange(curVals[i], v), v) : 0
//			};}).filter(function(i, cell) { return cell.dir !== curVals[cell.idx]; }).toArray();
//			numCells = cellStates.length;
//		}
		if (numCells>0)
		{
			for (var i = 0; i < numCells; ++i)
			{
				var cellIdx = cellStates[i].idx, cellTime = frame.time + cellStates[i].timeShift;
				if(cellTime <= dur)
				{
					cells[cellIdx].setOpacity(curVals[cellIdx] = ptInRange(easing(cellTime/dur), cellStates.dir));
				}
				else
				{
					cells[cellIdx].setOpacity(curVals[cellIdx] = endVals[cellIdx]||0.1);
					if (numCells !== i+1)
					{
						cellStates[i] = cellStates[--numCells];
						i--;
						cellStates.length = numCells;
					}
					else
					{
						cellStates.length = --numCells;
					}
				}
			}
		}
		else
		{
			me.isAnimating = false;
			for (var j = 0; j<ncells; ++j)
			{
				cells[j].setOpacity(endVals[j]||0.1);
			}
		}
	}, layer);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype._makeAnimationObj = function(startValue, endValue)
{
	startValue = (Number(startValue) || 0) % 10;
	endValue = (Number(endValue) || 0) % 10;
	var me = this, disp = me._display, lbl = me._label, cLbl = me._clippedLabel, layer = disp.getLayer() || null,
		diff1 = Math.abs(endValue - startValue), diff2 = Math.min(startValue, endValue) + 10 - Math.max(startValue, endValue),
		numsign = Math.sign ? Math.sign : function getnumSign(x) { return /* jshint bitwise:false */ (x>>31) /* jshint bitwise:true */ + (x > 0 ? 1 : 0); },
		numtrunc = Math.trunc ? Math.trunc : function getTruncNum(x) { x = +x; return (x - x % 1) || (!isFinite(x) || x === 0 ? x : x < 0 ? -0 : 0); },
		dir = diff2 < diff1 ? -numsign(endValue - startValue) : numsign(endValue - startValue),
		range = Math.min(diff1, diff2) * dir,
		lblY = 3, cLblY = dir===1 ? me._height + 1 : -me._height + 2,
		fullOffset = lblY - cLblY,
		current = startValue,
		next = (startValue + dir + 10) % 10,
		dur = 600;

	if (dir===0) // No digit change... no need to truely animate, just ensure the next frame is drawn correctly in case of an interrupted animation.
	{
		return new Kinetic.Animation(function()
		{
			me.isAnimating = false;
			lbl.text = endValue;
			lbl.y = lblY;
			cLbl.y = cLblY;
			cLbl.visible = false;
		}, layer);
	}
	cLbl.text = next;

	return new Kinetic.Animation(function(frame)
	{
		if (frame.time<=dur)
		{
			var p = frame.time / dur, curVal = (p * range + startValue + 10) % 10, pOffset = (curVal - numtrunc(curVal));
			if (dir===-1)
			{
				if ((pOffset = 1 - pOffset) !== 1) curVal = (curVal + 1) % 10;
			}
			var offSet = pOffset * fullOffset;
			curVal = numtrunc(curVal);
			if (curVal !== current)
			{
				current = curVal;
				next = (curVal + dir + 10) % 10;
				lbl.text = current;
				cLbl.text = next;
			}
			lbl.y = lblY + offSet;
			cLbl.y = cLblY + offSet;
		}
		else
		{
			me.isAnimating = false;
			lbl.text = endValue;
			lbl.y = lblY;
			cLbl.y = cLblY;
			cLbl.visible = false;
		}
	}, layer);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
//IDB.GraphOdomDigitView.prototype._updateAnimationParameters = function(animationParameters)
//{
//	var me = this;
//	if (me.isAnimationEnabled && me.isAnimating)
//	{
//		var params = me._animationParameters;
//		me._animationParameters = animationParameters;
//		if (params === null || (!!animationParameters && animationParameters instanceof Array && !!params && params instanceof Array && params.join('')!==animationParameters.join('')))
//			me._paramsChangedFlag = true;
//	}
//};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype.getWidth = function() {
	return this._width;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomDigitView.prototype.getHeight = function() {
	return this._height;
};

/*====================================================================================================*/
IDB.GraphOdomSet = function(graphContext, startYIndex, numOdoms) {
	this._graphContext = graphContext;
	this._startYIndex = startYIndex;
	this._numOdoms = numOdoms;

	this._odoms = [];
	this._values = [];
	
	var dpGrid = graphContext.datapointGrid;
	var sett = graphContext.settings;
	var maxPlaces = 0;
	var i, ss, axis;
			
	for ( i = 0 ; i < numOdoms ; i++ ) {
		axis = dpGrid.getYAxis(startYIndex+i, true);
		ss = dpGrid.getStatSet(axis.axisId);
		maxPlaces = Math.max(maxPlaces, IDB.GraphOdom.getIntegerPlacesByValue(ss.max));
		maxPlaces = Math.max(maxPlaces, IDB.GraphOdom.getIntegerPlacesByValue(ss.min));
	}
			
	for ( i = 0 ; i < numOdoms ; i++ ) {
		axis = dpGrid.getYAxis(startYIndex+i, true);
		this._odoms[i] = new IDB.GraphOdom(sett, maxPlaces, axis.axisName, false);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSet.prototype.setXRowIndex = function(index) {
	var dpGrid = this._graphContext.datapointGrid;
	var startY = this._startYIndex;
	var numOdoms = this._numOdoms;
	var i, dp;

	for ( i = 0 ; i < numOdoms ; ++i ) {
		dp = dpGrid.getDatapointAt(index, startY+i, true);
		this._odoms[i].setCurrValue(dp.yValue.numberValue);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSet.prototype.resetOdoms = function() {
	for ( var i in this._odoms ) {
		this._odoms[i].setCurrValue(0);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSet.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSet.prototype.getOdomCount = function() {
	return this._numOdoms;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSet.prototype.getOdom = function(index) {
	return this._odoms[index];
};

/*====================================================================================================*/
IDB.GraphOdomSetView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSetView.prototype._buildKinetic = function() {
	var model = this._model;
	var count = model.getOdomCount();
	var i, odom;

	this._display = new Kinetic.Group();

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	this._odoms = [];
	this._heightDiff = 0;

	for ( i = 0 ; i < count ; ++i ) {
		odom = new IDB.GraphOdomView(model.getOdom(i));
		odom.setSize();
		this._hold.add(odom.getDisplay());
		this._odoms[i] = odom;
	}

	this._scroll = new IDB.GraphScroll();
	this._scroll.onVisualUpdate(IDB.listener(model, 'refreshDisplay'));
	this._scroll.onScroll(IDB.listener(this, '_onScroll'));
	this._scroll.lineScrollSize = 16;
	this._display.add(this._scroll.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSetView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSetView.prototype.getMinWidth = function(allowedHeight) {
	if ( this._odoms.length == 0 ) {
		return 0;
	}

	var odom = this._odoms[0];
	var reqH = this._odoms.length*odom.getMinHeight();
	return odom.getMinWidth()+(reqH > allowedHeight ? 18 : 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSetView.prototype.setSize = function(width, height) {
	var count = this._odoms.length;

	if ( this._odoms.length == 0 ) {
		return 0;
	}
	
	var odom = this._odoms[0];
	var odomH = odom.getMinHeight();
	var totalH = odomH*count-8;
	var scrollD = this._scroll.getDisplay();
	var i;

	for ( i = 0 ; i < count ; i++ ) {
		this._odoms[i].getDisplay().y(i*odomH);
	}

	this._display.clip({
		x: 0,
		y: 0,
		width: width,
		height: height
	});

	this._heightDiff = totalH-height;
	scrollD.visible(this._heightDiff > 0);
			
	if ( scrollD.visible() ) {
		scrollD.x(width-16);

		this._scroll.pageSize = height;
		this._scroll.maxScrollPos = totalH-height;
		this._scroll.scrollPos =
			IDB.MathUtil.clamp(-this._hold.y()/this._heightDiff, 0, 1)*this._scroll.maxScrollPos;
		this._scroll.setSize(16, height);

		this._onScroll();
	}
	else {
		this._hold.y((height-totalH)/2);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomSetView.prototype._onScroll = function() {
	var perc = this._scroll.scrollPos/this._scroll.maxScrollPos;
	this._hold.y(-perc*this._heightDiff);
};

/*====================================================================================================*/
IDB.GraphOdomView = function(model) {
	this._model = model;

	this._intDigits = [];
	this._decDigits = [];
	this._minWeight = 0;
	this._minHeight = 0;

	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomView.prototype._buildKinetic = function() {
	var model = this._model;
	var labelText = model.getLabelText();
	var intPlaces = model.getIntegerPlaces();
	var decPlaces = model.getDecimalPlaces();
	var i, d;

	this._display = new Kinetic.Group();

	if ( labelText ) {
		var lblOpt = model.getLabelOptions();
		lblOpt.align = 'left';

		this._label = new IDB.GraphLabel(lblOpt, labelText);
		this._display.add(this._label.getDisplay());
	}

	for ( i = 0 ; i < intPlaces ; ++i ) {
		d = new IDB.GraphOdomDigitView(model.getIntegerDigit(i));
		this._display.add(d.getDisplay());
		this._intDigits[i] = d;
	}
	
	for ( i = 0 ; i < decPlaces ; ++i ) {
		d = new IDB.GraphOdomDigitView(model.getDecimalDigit(i));
		this._display.add(d.getDisplay());
		this._decDigits[i] = d;
	}

	this._signDigit = new IDB.GraphOdomDigitView(model.getSignDigit());
	this._display.add(this._signDigit.getDisplay());

	this._dotDigit = new IDB.GraphOdomDigitView(model.getDotDigit());
	this._display.add(this._dotDigit.getDisplay());

	if ( model.getPercentDigit() ) {
		this._percDigit = new IDB.GraphOdomDigitView(model.getPercentDigit());
		this._display.add(this._percDigit.getDisplay());
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomView.prototype.getMinHeight = function() {
	return this._minHeight;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomView.prototype.getMinWidth = function() {
	return this._minWidth;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphOdomView.prototype.setSize = function() {
	var d = (this._intDigits.length ? this._intDigits[0] : this._decDigits[0]);
	var px = 0;
	var py = (this._label ? this._label.textHeight-1 : 0);
	var signW = this._signDigit.getWidth();
	var decW = this._dotDigit.getWidth();
	var digitW = d.getWidth();
	var intPlaces = this._intDigits.length;
	var decPlaces = this._decDigits.length;
	var i;

	this._signDigit.getDisplay().x(px).y(py);
	px += signW;

	this._dotDigit.getDisplay().visible(decPlaces > 0);

	for ( i = intPlaces-1 ; i >= 0 ; --i ) {
		this._intDigits[i].getDisplay().x(px).y(py);
		px += digitW;
	}

	if ( decPlaces > 0 ) {
		this._dotDigit.getDisplay().x(intPlaces*digitW + signW).y(py);
		px += decW;
		
		for ( i = 0 ; i < decPlaces ; ++i ) {
			this._decDigits[i].getDisplay().x(px).y(py);
			px += digitW;
		}
	}
	
	if ( this._percDigit ) {
		this._percDigit.getDisplay().x(px).y(py);
		px += this._percDigit.getWidth();
	}

	this._minWidth = px;
	this._minHeight = py+this._dotDigit.getHeight()+8;

	if ( this._label ) {
		this._label.width = this.getMinWidth();
		this._label.truncate();
	}
};
/*====================================================================================================*/
IDB.GraphPages = function(graphContext, buildItemFunc, itemCount, showCount) {
	this._graphContext = graphContext;
	this._buildItemFunc = buildItemFunc;
	this._itemCount = itemCount;
	this._showCount = showCount;
	this.setCurrentIndex(0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.setCurrentIndex = function(index) {
	this._currIndex = IDB.MathUtil.clamp(index, 0, this._itemCount-this._showCount);
	this._items = this._buildItems();
	
	if ( this._onPageUpdate ) {
		this._onPageUpdate();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.shiftCurrentIndex = function(amount) {
	this.setCurrentIndex(this._currIndex+amount);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.setOnPageUpdate = function(callback) {
	this._onPageUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.getItem = function(index) {
	return this._items[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.getItemCount = function() {
	return this._itemCount;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.getShowCount = function() {
	return this._showCount;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.getCurrentIndex = function() {
	return this._currIndex;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.hasPreviousItems = function() {
	return (this._currIndex > 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype.hasNextItems = function() {
	return (this._currIndex+this._showCount < this._itemCount);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPages.prototype._buildItems = function() {
	var items = [];
	var count = this._showCount;
	var currI = this._currIndex;
	var maxI = currI+count;
	var buildFunc = this._buildItemFunc;

	for ( var i = currI ; i < maxI ; ++i ) {
		items[i] = buildFunc(i);
	}

	return items;
};

/*====================================================================================================*/
IDB.GraphPagesView = function(model) {
	this._model = model;
	model.setOnPageUpdate(IDB.listener(this, '_onPageUpdate'));
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.build = function(buildItemFunc, updateLayoutFunc) {
	this._buildItemFunc = buildItemFunc;
	this._updateLayoutFunc = updateLayoutFunc;
	this._buildKinetic();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype._buildKinetic = function() {
	var model = this._model;
	var onVisualUpdate = IDB.listener(model, 'refreshDisplay');

	this._display = new Kinetic.Group();

	this._itemHold = new Kinetic.Group();
	this._display.add(this._itemHold);

	this._prevBtn = new IDB.GraphButtonArrow('??', IDB.GraphButtonArrow.LEFT_DIR, '');
	this._prevBtn.setSize(55, 25);
	this._prevBtn.setEmphasis(true);
	this._prevBtn.onVisualUpdate(onVisualUpdate);
	this._prevBtn.onClick(IDB.listener(this, '_onPrevClick'));
	this._display.add(this._prevBtn.getDisplay());

	this._nextBtn = new IDB.GraphButtonArrow('??', IDB.GraphButtonArrow.RIGHT_DIR, '');
	this._nextBtn.setSize(55, 25);
	this._nextBtn.setEmphasis(true);
	this._nextBtn.onVisualUpdate(onVisualUpdate);
	this._nextBtn.onClick(IDB.listener(this, '_onNextClick'));
	this._display.add(this._nextBtn.getDisplay());

	this._buttonsW = (model.getShowCount() >= model.getItemCount() ? 0 : 110);
	this._buttonsH = (this._buttonsW == 0 ? 0 : 30);
	this._onPageUpdate();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype._onPageUpdate = function() {
	var model = this._model;
	var buildFunc = this._buildItemFunc;
	var currI = model.getCurrentIndex();
	var maxI = currI+model.getShowCount();
	var i, itemView;
	
	this._prevBtn.setText(currI+'');
	this._prevBtn.getDisplay().visible(model.hasPreviousItems());

	this._nextBtn.setText((model.getItemCount()-maxI)+'');
	this._nextBtn.getDisplay().visible(model.hasNextItems());

	this._itemViews = [];
	this._itemHold.destroyChildren();

	for ( i = currI ; i < maxI ; ++i ) {
		itemView = buildFunc(i, model.getItem(i));
		this._itemViews[i] = itemView;
		this._itemHold.add(itemView.getDisplay());
	}

	if ( this._availW ) {
		this._updateLayoutFunc();
		this._model.refreshDisplay();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype._onNextClick = function() {
	this._doScroll(1, this._nextBtn.getMoveType());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype._onPrevClick = function() {
	this._doScroll(-1, this._prevBtn.getMoveType());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype._doScroll = function(direction, moveType) {
	var model = this._model;
	var amt = direction;

	if ( moveType == IDB.GraphButtonArrow.MOVE_PAGE ) {
		amt *= model.getShowCount();
	}
	else if ( moveType == IDB.GraphButtonArrow.MOVE_ALL ) {
		amt *= model.getItemCount();
	}

	model.shiftCurrentIndex(amt);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.getItemDisplay = function() {
	return this._itemHold;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.getItemView = function(index) {
	return this._itemViews[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.getAvailableWidth = function() {
	return this._availW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.getAvailableHeight = function() {
	return this._availH;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.getButtonsWidth = function() {
	return this._buttonsW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.getButtonsHeight = function() {
	return this._buttonsH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.setSize = function(width, height) {
	this._availW = width;
	this._availH = height;

	this._prevBtn.getDisplay().y(height-25);
	this._nextBtn.getDisplay().x(width-this._buttonsW/2).y(height-25);

	this._updateLayoutFunc();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphPagesView.prototype.setButtonY = function(y) {
	y = Math.min(this._nextBtn.getDisplay().y(), y);
	this._prevBtn.getDisplay().y(y);
	this._nextBtn.getDisplay().y(y);
};

/*====================================================================================================*/
IDB.GraphScroll = function(horizMode) {
	this._horiz = IDB.ifUndef(horizMode, false);

	this._scrollPos = 0;
	this._minScrollPos = 0;
	this._maxScrollPos = 100;
	this._pageSize = 10;
	this._lineScrollSize = 1;

	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();

	this._bg = new Kinetic.Rect({ fill: '#888' });
	this._bg.on('click', IDB.leftClickListener(this, '_onBgClick'));
	this._display.add(this._bg);

	this._up = new IDB.GraphButtonArrow(null, IDB.GraphButtonArrow.RIGHT_DIR);
	this._up.allowAutoRepeat(true);
	this._up.onVisualUpdate(this._onVisualUpdate);
	this._up.onClick(IDB.listener(this, '_onUpClick'));
	this._up.getDisplay().rotation(-90);
	this._display.add(this._up.getDisplay());
	
	this._down = new IDB.GraphButtonArrow(null, IDB.GraphButtonArrow.RIGHT_DIR);
	this._down.allowAutoRepeat(true);
	this._down.onClick(IDB.listener(this, '_onDownClick'));
	this._down.onVisualUpdate(this._onVisualUpdate);
	this._down.getDisplay().rotation(90);
	this._display.add(this._down.getDisplay());
	
	this._drag = new IDB.GraphButton('');
	this._drag.allowAutoRepeat(false);
	this._drag.onVisualUpdate(this._onVisualUpdate);
	this._drag.getDisplay()
		.draggable(true)
		.dragBoundFunc(IDB.listener(this, '_dragBoundFunc'));
	this._display.add(this._drag.getDisplay());

	//LATER: mouse wheel scrolling? Kinetic doesn't support this yet.
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype._onBgClick = function(event) {
	event = (event.evt ? event.evt : event); //supports either Kinetic version

	var abs = this._drag.getDisplay().getAbsolutePosition();
	var isLess;
	
	if ( this._horiz ) {
		isLess = (event.layerX <= abs.x);
	}
	else {
		isLess = (event.layerY <= abs.y);
	}

	this._changeScrollPosition(this._pageSize*(isLess ? -1 : 1));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype._onUpClick = function() {
	this._changeScrollPosition(-this._lineScrollSize);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype._onDownClick = function() {
	this._changeScrollPosition(this._lineScrollSize);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype._changeScrollPosition = function(amount) {
	var oldPos = this._scrollPos;
	this.scrollPos += amount;

	if ( this._scrollPos == oldPos ) {
		return;
	}

	if ( this._onScroll ) {
		this._onScroll();
	}

	this._updateDragger();
	
	if ( this._onVisualUpdate ) {
		this._onVisualUpdate();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
Object.defineProperties(IDB.GraphScroll.prototype, {

	scrollPos: {
		get: function() {
			return this._scrollPos;
		},
		set: function(value) {
			this._scrollPos = IDB.MathUtil.clamp(value, this._minScrollPos, this._maxScrollPos);
		}
	},

	minScrollPos: {
		get: function() {
			return this._minScrollPos;
		},
		set: function(value) {
			this._minScrollPos = value;
		}
	},

	maxScrollPos: {
		get: function() {
			return this._maxScrollPos;
		},
		set: function(value) {
			this._maxScrollPos = value;
		}
	},

	pageSize: {
		get: function() {
			return this._pageSize;
		},
		set: function(value) {
			this._pageSize = value;
		}
	},

	lineScrollSize: {
		get: function() {
			return this._lineScrollSize;
		},
		set: function(value) {
			this._lineScrollSize = value;
		}
	}

});


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype.setSize = function(width, height) {
	this._bg.y(width-2).width(width).height(height-width*2+4);
	
	this._up.setSize(width, width);
	this._up.getDisplay().y(width);
	
	this._down.setSize(width, width);
	this._down.getDisplay().x(width).y(height-width);
	
	this._updateDragger();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype.refresh = function() {
	this._updateDragger();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype._updateDragger = function() {
	var y = this._bg.y();
	var width = this._bg.width();
	var height = this._bg.height();
	var totalPos = this._maxScrollPos-this._minScrollPos+this._pageSize;
	var yPerc = this._scrollPos/totalPos;
	var hPerc = this._pageSize/totalPos;

	this._dragH = height*hPerc;
	this._drag.setSize(width, this._dragH);
	this._drag.getDisplay().y(y + height*yPerc);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype._dragBoundFunc = function(pos) {
	var abs = this._bg.getAbsolutePosition();
	var avail = this._bg.height()-this._dragH;
	var bounds, perc;

	if ( this._horiz ) {
		perc = IDB.MathUtil.clamp((pos.x-abs.x)/avail, 0, 1);
		
		bounds = {
			x: abs.x + perc*avail,
			y: abs.y
		};
	}
	else {
		perc = IDB.MathUtil.clamp((pos.y-abs.y)/avail, 0, 1);
		
		bounds = {
			x: abs.x,
			y: abs.y + perc*avail
		};
	}
	
	this.scrollPos = perc*(this._maxScrollPos-this._minScrollPos)+this._minScrollPos;

	if ( this._onScroll ) {
		this._onScroll();
	}
	
	if ( this._onVisualUpdate ) {
		this._onVisualUpdate();
	}

	return bounds;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype.onVisualUpdate = function(callback) {
	this._onVisualUpdate = callback;
	this._up.onVisualUpdate(callback);
	this._down.onVisualUpdate(callback);
	this._drag.onVisualUpdate(callback);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype.onScroll = function(callback) {
	this._onScroll = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphScroll.prototype.getDisplay = function() {
	return this._display;
};
/*global IDB */
/*====================================================================================================*/
// ._settings (Object);
IDB.GraphSettings = function(chartData) {
	this._chartData = chartData;
	this._numFormat = (chartData.numFormat || IDB.NumberUtils.DNF);
	this._settings = chartData.graphProperties;

	this.set('GraphTypeCode', chartData.graphTypeCode);
	this.set('DatapointMatchType', chartData.matchType);
	this.set('BackgroundColor', chartData.backgroundColor);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.set = function(name, value) {
	this._settings[name] = value;
};
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.getGraphTypeCode = function() {
	return this._settings['GraphTypeCode'] || null;
};
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.clone = function() {
	var gs = new IDB.GraphSettings(this._chartData);
	gs._numFormat = this._numFormat;
	gs._settings = this._cloneItem(this._settings);
	return gs;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype._cloneItem = function(item) {
	if ( typeof(item) != 'object' ) {
		return item;
	}

	var copy = {};
	
	for ( var key in item ) {
		copy[key] = this._cloneItem(item[key]);
	}

	return copy;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.get = function(name, ignoreMissing) {
	var value = this._settings[name];
	
	if ( IDB.isUndef(value) && !ignoreMissing) {
		console.log('Missing value for setting: "'+name+'".');
	}

	return value;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.getBool = function(name) {
	return (this.get(name) == true);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.getAlpha = function(name) {
	return 1-this.get(name)/100;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.getOptional = function(name) {
	return this._settings[name];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.getOptionalBool = function(name) {
	return (this.getOptional(name) == true);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.contains = function(name) {
	return !IDB.isUndef(this._settings[name]);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSettings.prototype.formatNumber = function(value, format) {
	//Format priority: 1) from parameter, 2) from chartData, 3) default
	return IDB.NumberUtils.format(value, (format || this._numFormat));
};

IDB.GraphSettings.prototype.animationEnabled = function() {
    var me = this, chartData = me._chartData;
    return chartData ? !!chartData.animated : false;
};


/*====================================================================================================*/
IDB.GraphSingleRow = function(graphContext, showRowFunc, sticky) {
	this._graphContext = graphContext;
	this._graphContext.addDatapointsUpdatedListener(IDB.listener(this, '_onDatapointsUpdated'));
	this._showRowFunc = showRowFunc;
	this._sticky = IDB.ifUndef(sticky, true);
	this._isXMatch = (graphContext.settings.get('DatapointMatchType') == IDB.MT.X_VALUE);
	this._selectedIndex = -1;

	var dpGrid = graphContext.datapointGrid;
	var i, dp;

	this._currIndex = 0;
	this._itemCount = dpGrid.getNumXRows();
	this._showCount = Math.min(this._itemCount, 1);
	this._items = [];

	for ( i = 0 ; i < this._itemCount ; ++i ) {
		dp = dpGrid.getDatapointAt(i, 0, false);

		this._items[i] = {
			first: dp,
			row: dp.datapointRow,
			label: dp.xValue.formattedValue,
			state: IDB.VS.NORMAL
		};
	}

	if ( this._sticky ) {
		this.setCurrentIndex(this._itemCount);
		this._selectRow(this._itemCount-1, false);
	}
};

//TODO: autoScrollSpeed
//TODO: blockHighlighting


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.setCurrentIndex = function(index) {
	this._currIndex = IDB.MathUtil.clamp(index, 0, this._itemCount-this._showCount);
	
	if ( this._onIndexUpdate ) {
		this._onIndexUpdate();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.shiftCurrentIndex = function(amount) {
	this.setCurrentIndex(this._currIndex+amount);

	// if the selected item is outside the active range, select a new one

	if ( this._selectedIndex != -1 && this._selectedIndex < this._currIndex ) {
		this._selectRow(this._currIndex, false, true);
	}
	else if ( this._selectedIndex >= this._currIndex+this._showCount ) {
		this._selectRow(this._currIndex+this._showCount-1, false, true);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getCappedShowCount = function(count) {
	return Math.min(count, this._itemCount);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.setShowCountFromView = function(count) {
	this._showCount = this.getCappedShowCount(count);
	this.setCurrentIndex(Math.min(this._currIndex, this._itemCount-this._showCount));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.setOnIndexUpdate = function(callback) {
	this._onIndexUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.setOnStateUpdate = function(callback) {
	this._onStateUpdate = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getLabel = function(index) {
    return this._items[index].label;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getState = function(index) {
	return this._items[index].state;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getSelectedLabel = function() {
	if ( this._selectedIndex == -1 ) {
		return '';
	}
    return this._items[this._selectedIndex].label;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getSelectedLabelText = function() {
	if ( this._selectedIndex == -1 ) {
		return '';
	}
    var index = this._selectedIndex;
    var item = this._items[index];
    var labelFunction = this._graphContext.settings.get('XLabelFunction');
    if(labelFunction) {
        return labelFunction(item.first);
    }
    else {
	    return item.label;
    }
};


/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getItemCount = function() {
	return this._itemCount;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getShowCount = function() {
	return this._showCount;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getCurrentIndex = function() {
	return this._currIndex;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getSelectedIndex = function() {
	return this._selectedIndex;
};

IDB.GraphSingleRow.prototype.setSelectedIndex = function(index) {
    if(index < 0 || index > this._itemCount-1) {
        return;
    }
    this._graphContext.onDatapointOver(this._getDatapointAtRow(index));
    this._selectRow(index, true);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.hasPreviousItems = function() {
	return (this._currIndex > 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.hasNextItems = function() {
	return (this._currIndex+this._showCount < this._itemCount);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.showItems = function() {
	return this._graphContext.settings.getBool('ButtonRowShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.isBottomPosition = function() {
	return (this._graphContext.settings.get('ButtonRowPos') == 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.getLabelOptions = function() {
    var gs = this._graphContext.settings;
	var lo = gs.get('XValueLabelOptions');
	lo.bottom = (gs.get('XValueLabelPos') == 0);

    if(gs.get('XLabelShadow', true)) {
        lo.shadow = {
            color:lo.color.x.css
        };
    }

	return lo;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype._onDatapointsUpdated = function() {
	var gc = this._graphContext;

	// skip outgoing events (these are handled by the "onItem*" functions)

	if ( gc.isOutgoingDatapointEvent ) {
		return;
	}

	// skip incoming events when not using the "X_VALUE" match type

	if ( !this._isXMatch ) {
		return;
	}

	// determine which row to highlight, and whether the others should be faded

	var dpGrid = gc.datapointGrid;
	var rowCount = this._itemCount;
	var colCount = dpGrid.getNumYCols(false);
	var fadeOthers = true;
	var row = -1;
	var i, j, vs;

	for ( i = 0 ; i < rowCount ; ++i ) {
		for ( j = 0 ; j < colCount ; ++j ) {
			vs = dpGrid.getDatapointAt(i, j, false).getVisualState();

			if ( vs == IDB.VS.HIGHLIGHTED ) {
				row = i;
				break;
			}

			if ( vs != IDB.VS.FADED ) {
				fadeOthers = false;
			}
		}
	}
	
	this._selectRow(row, fadeOthers);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype._selectRow = function(index, fadeOthers, blockCentering) {
	var prevIndex = this._selectedIndex;

	// keep the row highlighted in "sticky" scenarios

	if ( index != -1 || !this._sticky ) {
		this._selectedIndex = index;
	}

	// set the row states

	var rowCount = this._itemCount;
	var items = this._items;
	var gc = this._graphContext;

	for ( var i = 0 ; i < rowCount ; ++i ) {
		items[i].state = (i == this._selectedIndex ? IDB.VS.HIGHLIGHTED :
			(fadeOthers ? IDB.VS.FADED : IDB.VS.NORMAL));
	}

	// for incoming events, center the selected row

	if ( !blockCentering && !gc.isOutgoingDatapointEvent && this._selectedIndex != -1 ) {
		this.setCurrentIndex(this._selectedIndex-Math.floor(this._showCount/2));
	}

	// notify view, graph, and renderer about these changes

	if ( this._onStateUpdate ) {
		this._onStateUpdate();
	}
	
	if ( this._selectedIndex != prevIndex ) {
		this._showRowFunc(this._selectedIndex == -1 ? null : this._items[this._selectedIndex].row);
	}

	gc.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.onItemClick = function(index) {
	this._graphContext.onDatapointClicked(this._getDatapointAtRow(index));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.onItemMouseOver = function(index) {
	this._graphContext.onDatapointOver(this._getDatapointAtRow(index));
	this._selectRow(index, true, true);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype.onItemMouseOut = function(index) {
	this._graphContext.onDatapointOut(this._getDatapointAtRow(index));
	this._selectRow(-1, false, true);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRow.prototype._getDatapointAtRow = function(index) {
	return this._items[index].first;
};

/*====================================================================================================*/
IDB.GraphSingleRowView = function(model, sticky) {
	this._model = model;
	this._sticky = sticky;
	model.setOnIndexUpdate(IDB.listener(this, '_onIndexUpdate'));
	model.setOnStateUpdate(IDB.listener(this, '_onStateUpdate'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._buildKinetic = function() {
	var model = this._model;
	var lblOpt = model.getLabelOptions();

	this._display = new Kinetic.Group();

	this._contentRect = {
		x: 0,
		y: 0,
		width: 0, 
		height: 0
	};

	if ( model.showItems() ) {
		this._buildButtons();
	}

	if (lblOpt.show) {
            if(!lblOpt.align) {
	        lblOpt.align = 'center';
            }

            var label = this._label = new IDB.GraphLabel(lblOpt, '');
            this._display.add(label.getDisplay());
            if(lblOpt.clickable) {
                label.onClick(IDB.listener(this, '_onLabelClick'));
                label.onMouseOver(IDB.listener(this, '_onLabelOver'));
                label.onMouseOut(IDB.listener(this, '_onLabelOut'));
            }
	}

	this._onIndexUpdate();
	this._onStateUpdate();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._buildButtons = function() {
	var onVisualUpdate = IDB.listener(this._model, 'refreshDisplay');

	this._btnHold = new Kinetic.Group();
	this._display.add(this._btnHold);

	this._prevBtn = new IDB.GraphButtonArrow(null, IDB.GraphButtonArrow.LEFT_DIR, 'x');
	this._prevBtn.setEmphasis(true);
	this._prevBtn.onVisualUpdate(onVisualUpdate);
	this._prevBtn.onClick(IDB.listener(this, '_onPrevClick'));
	this._display.add(this._prevBtn.getDisplay());

	this._nextBtn = new IDB.GraphButtonArrow(null, IDB.GraphButtonArrow.RIGHT_DIR, 'x');
	this._nextBtn.setEmphasis(true);
	this._nextBtn.onVisualUpdate(onVisualUpdate);
	this._nextBtn.onClick(IDB.listener(this, '_onNextClick'));
	this._display.add(this._nextBtn.getDisplay());
	
	this._buttons = [];
	this._buttonH = 25;
	this._pad = 5;

	this._calcButtonWidth();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._calcButtonWidth = function() {
	var model = this._model;
	var count = model.getItemCount();
	var width = 0;

	var btn = new IDB.GraphButton('');
	btn.setSize(2000, 100);

	for ( var i = 0 ; i < count ; ++i ) {
		btn.setText(model.getLabel(i));
		width = Math.max(width, btn.getTextWidth());
	}

	this._fullButtonW = width+10;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onIndexUpdate = function() {
	var model = this._model;

	if ( this._btnHold ) {
		this._updateButtonIndex();
	}

	model.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._updateButtonIndex = function() {
	var model = this._model;

	this._prevBtn.getDisplay().visible(model.hasPreviousItems());
	this._nextBtn.getDisplay().visible(model.hasNextItems());

	var currI = model.getCurrentIndex();
	var itemCount = model.getItemCount();
	var btnCount = model.getShowCount();
	var btnW = this._useButtonW;
	var btnH = this._buttonH;
	var px = 0;
	var i, btn;

	for ( i = 0 ; i < itemCount ; ++i ) {
		btn = this._buttons[i];

		if ( i < currI || i >= currI+btnCount ) {
			if ( btn ) {
				delete this._buttons[i];
				btn.getDisplay().destroy();
			}

			continue;
		}

		if ( !btn ) {
			btn = new IDB.GraphButton(model.getLabel(i));
			btn.setData(i);
			btn.onClick(IDB.listener(this, '_onButtonClick'));
			btn.onMouseOver(IDB.listener(this, '_onButtonOver'));
			btn.onMouseOut(IDB.listener(this, '_onButtonOut'));
			this._btnHold.add(btn.getDisplay());
			this._buttons[i] = btn;
		}
		
		btn.getDisplay().x(px);
		btn.setSize(btnW, btnH);

		px += btnW+this._pad;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onStateUpdate = function() {
	var model = this._model;
	var itemCount = (this._btnHold ? model.getItemCount() : 0);
	var i, btn;

	for ( i = 0 ; i < itemCount ; ++i ) {
		btn = this._buttons[i];

		if ( !btn ) {
			continue;
		}

		switch ( model.getState(i) ) {
			case IDB.VS.NORMAL:
				btn.setHighlighted(false);
				btn.setFaded(false);
				break;
				
			case IDB.VS.HIGHLIGHTED:
				btn.setHighlighted(true);
				btn.setFaded(false);
				break;
				
			case IDB.VS.FADED:
				btn.setHighlighted(false);
				btn.setFaded(true);
				break;
		}
	}
	if ( this._label ) {
		this._label.text = model.getSelectedLabelText();
	}

	model.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onNextClick = function() {
	this._doScroll(1, this._nextBtn.getMoveType());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onPrevClick = function() {
	this._doScroll(-1, this._prevBtn.getMoveType());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onButtonClick = function(button) {
	this._model.onItemClick(button.getData());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onButtonOver = function(button) {
	this._model.onItemMouseOver(button.getData());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onButtonOut = function(button) {
	this._model.onItemMouseOut(button.getData());
};

/*----------------------------------------------------------------------------------------------------*/

IDB.GraphSingleRowView.prototype._onLabelClick = function() {
	this._model.onItemClick(this._model.getSelectedIndex());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onLabelOver = function() {
	this._model.onItemMouseOver(this._model.getSelectedIndex());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._onLabelOut = function() {
	this._model.onItemMouseOut(this._model.getSelectedIndex());
};
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._doScroll = function(direction, moveType) {
	var model = this._model;
	var amt = direction;

	if ( moveType == IDB.GraphButtonArrow.MOVE_PAGE ) {
		amt *= model.getShowCount();
	}
	else if ( moveType == IDB.GraphButtonArrow.MOVE_ALL ) {
		amt *= model.getItemCount();
	}

	model.shiftCurrentIndex(amt);
	model.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype.getContentRect = function() {
	return this._contentRect;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype.setSize = function(width, height) {
	var rect = this._contentRect;
	rect.x = 0;
	rect.y = 0;
	rect.width = width;
	rect.height = height;

	if ( this._btnHold ) {
		this._updateButtonsSize();
	}
	
	if ( this._label ) {
		this._updateLabelSize();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype.stepForward = function() {
    var model = this._model;
    var selectedIndex = model.getSelectedIndex();
    var count = model.getItemCount();
    if(selectedIndex != -1) {
        selectedIndex++;
        model.setSelectedIndex(selectedIndex%count);
    }
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype.stepBackward = function() {
    var model = this._model;
    var selectedIndex = model.getSelectedIndex();
    var count = model.getItemCount();
    if(selectedIndex != -1) {
        selectedIndex--;
        if(selectedIndex < 0) {
            selectedIndex += count;
        }
        model.setSelectedIndex(selectedIndex%count);
    }
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._updateButtonsSize = function() {
	var model = this._model;
	var rect = this._contentRect;
	var arrowW = (this._buttonH*2 > rect.width ? rect.width/2 : this._buttonH);
	var arrowH = this._buttonH;
	var pad = this._pad;
	var availButtonW = rect.width-arrowW*2-pad;
	var maxBtnCount = Math.floor(availButtonW/(this._fullButtonW+pad));

	if ( maxBtnCount == 0 && availButtonW-pad > 0 ) {
		maxBtnCount = 1;
	}

	var btnCount = model.getCappedShowCount(maxBtnCount);
	this._useButtonW = Math.min(availButtonW-pad, this._fullButtonW);
	rect.height -= arrowH;
	model.setShowCountFromView(maxBtnCount);

	var btnW = this._useButtonW;
	var px = arrowW+pad + (availButtonW-(btnW+pad)*btnCount)/2;
	var py = rect.y;
	
	if ( model.isBottomPosition() ) {
		py += rect.height;
	}
	else {
		rect.y += arrowH+5;
	}

	rect.height -= 5;

	this._prevBtn.setSize(arrowW, arrowH);
	this._nextBtn.setSize(arrowW, arrowH);
	this._prevBtn.getDisplay().y(py);
	this._nextBtn.getDisplay().x(rect.width-arrowW).y(py);
	this._btnHold.x(px).y(py);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype._updateLabelSize = function() {
	var model = this._model;
	var rect = this._contentRect;
	var lblH = this._label.height;
	var py = rect.y+1;

	rect.height -= lblH;
	
	if ( model.getLabelOptions().bottom ) {
		py += rect.height;
	}
	else {
		rect.y += lblH+5;
	}

	rect.height -= 5;

	this._label.width = rect.width;
	this._label.y = py;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSingleRowView.prototype.setLabelY = function(y) {
	var model = this._model, rect = this._contentRect, label = this._label;
    if(model.getLabelOptions().bottom) {
        if(y >= label.y) {
            return;
        }
    }
    else {
        if(y <= label.y) {
            return;
        }
    }

    if(this._label) {
        this._label.y = y;
    }
};

/*====================================================================================================*/
IDB.GraphSpacing = function(numItems, width, height, sizeRatio, fixedExtraVert) {
	sizeRatio = IDB.ifUndef(sizeRatio, 1);
	fixedExtraVert = IDB.ifUndef(fixedExtraVert, 0);

	var itemW = 0;
	var pad = 5;
	var cols = 1;
	var rows = 0;
	var c = 1;
	var r, size, sizeVert, colPad, rowPad;
			
	while ( c <= numItems ) {
		r = Math.ceil(numItems/c);
		colPad = (pad-pad/c);
		rowPad = (pad-pad/r);
				
		size = width/c;
		sizeVert = Math.max(0, ((height/r)-rowPad-fixedExtraVert)*sizeRatio+colPad);
				
		size = Math.min(size, sizeVert);
				
		//Has loop has passed optimal size?  Using < instead of <= so the algorithm will
		//maximize the number of columns in tie situations.
		if ( size < itemW ) { break; }
				
		itemW = size;
		cols = c;
		rows = r;
		++c;
	}
			
	if ( rows > 1 || cols > 1 ) {
		itemW -= pad-pad/cols;
	}
			
	var itemH = itemW/sizeRatio+fixedExtraVert;

	this._itemW = itemW;
	this._itemH = itemH;

	this._totalW = itemW*cols+pad*(cols-1);
	this._totalH = itemH*rows+pad*(rows-1);

	////

	this._positions = [];

	var colsInLastRow = numItems-(rows-1)*cols;
	var i, pc, pr, pushX, pushY;
	
	for ( i = 0 ; i < numItems ; ++i ) {
		c = (i%cols);
		r = Math.floor(i/cols);
		pc = pad*(cols-1);
		pr = pad*(rows-1);
		pushX = (width-itemW*cols-pc)/2;
		pushY = (height-itemH*rows-pr)/2;
			
		if ( r == rows-1 ) { //center items along the bottom row
			c += (cols-colsInLastRow)/2;
		}
			
		this._positions[i] = {
			x: c*(itemW+pad)+pushX,
			y: r*(itemH+pad)+pushY
		};
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSpacing.prototype.getPos = function(index) {
	return this._positions[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSpacing.prototype.getItemWidth = function() {
	return this._itemW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSpacing.prototype.getItemHeight = function() {
	return this._itemH;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSpacing.prototype.getTotalWidth = function() {
	return this._totalW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphSpacing.prototype.getTotalHeight = function() {
	return this._totalH;
};

/*====================================================================================================*/
IDB.GraphTicks = function(divMetrics, numMinors, numThirds) {
	this._majors = [];
	this._minors = [];
	this._thirds = [];
	
	var dm = divMetrics.largeDivisions;
	var len = dm.boundaries.length;
	var i, b;
	
	for ( i = 0 ; i < len ; ++i ) {
		this._majors.push(dm.boundaries[i]);
	}

	if ( !numMinors ) {
		return;
	}
	
	var m = numMinors+1;
	var min = dm.getMinBoundary();
	var max = dm.getMaxBoundary();
	var space = dm.divisionSize/m;
	
	for ( i = -m+1 ; ; ++i ) {
		if ( i % m == 0 ) {
			continue;
		}
		
		b = min+(space*i);
		
		if ( b < min ) {
			continue;
		}
		else if ( b > max ) {
			break;
		}

		this._minors.push(b);
	}
	
	if ( !numThirds ) {
		return;
	}

	var t = numThirds+1;
	space = space/t;
	
	for ( i = -t+1 ; ; ++i ) {
		if ( i%t == 0 ) {
			continue;
		}
		
		b = min+(space*i);

		if ( b < min ) {
			continue;
		}
		else if ( b > max ) {
			break;
		}

		this._thirds.push(b);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphTicks.prototype.getMajorValues = function() {
	return this._majors;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphTicks.prototype.getMinorValues = function() {
	return this._minors;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphTicks.prototype.getThirdValues = function() {
	return this._thirds;
};
/*global IDB */
IDB.LayoutContainer = function() {
    var me = this;
    me._layoutFunc = null;
    me._layoutFuncTimeoutID = 0;
    me._layoutEnabled = true;
};

/**
 * Schedules a call to doLayout() (implemented in subclass) if one is not already pending.
 */
IDB.LayoutContainer.prototype.invalidateLayout = function() {
    var me = this;

    if(me._layoutFunc) {
//        IDB.log(me, "LayoutContainer.invalidateLayout(): Layout already scheduled.");
        return;
    }

    // This should only ever be called by setTimeout.
    me._layoutFunc = function() {
//        IDB.log(me, "LayoutContainer._layoutFunc called.");
        me._layoutFuncTimeoutID = 0;
        if(me._layoutEnabled) {
            me.doLayout();
        }
        me._layoutFunc = null;
    };

    me._layoutFuncTimeoutID = setTimeout(me._layoutFunc, 0);
};


/**
 * Cancels any pending calls to doLayout().
 */
IDB.LayoutContainer.prototype.validateLayout = function() {
    var me = this;
    if(me._layoutFuncTimeoutID) {
        clearTimeout(me._layoutFuncTimeoutID);
    }
    me._layoutFuncTimeoutID = 0;
    me._layoutFunc = null;
};


/*global IDB, $ */
IDB.MapLocationConfig = function() {
    var me = this, I = IDB, MLC = I.MapLocationConfig;
    MLC.initProps(me);
};

IDB.MapLocationConfig.prototype.configure = function($xml, defZMin, defZMax) {
    var me = this, get = function(name) {return $xml.idbFirstChildText(name) || null;},
        num = function(name, defaultVal) {if(defaultVal === undefined) {defaultVal = NaN;} 
           return $xml.children(name).idbTextToNumber(defaultVal);},
        gps = get("latlon"),
        lon = me._lon = num("lon", NaN),
        lat = me._lat = num("lat", NaN), LatLon = IDB.LatLon, s, latLon;
    if(gps) {
        me._latLon = latLon = LatLon.parseGPS(gps);
        me._lat = latLon._lat;
        me._lon = latLon._lon;
    }
    else {
        me._latLon = new LatLon(lat, lon);
    }
    me._isValid = me._latLon._isValid;
    me._zmin = Math.max(0, $xml.idbAttrToNumber("zmin", defZMin));
    me._zmax = Math.max(me._zmin, Math.min(54, $xml.idbAttrToNumber("zmax", defZMax)));
};



IDB.MapLocationConfig.initProps = function(newInstance) {
    var props = {
        _lat:NaN,
        _lon:NaN,
        _latLon:null,
        _zmin:0,
        _zmax:54,
        _isValid:false
     };
     $.extend(newInstance, props);
};
/*global IDB, $, Kinetic */
IDB.MapObject = function(stageMgr) {
    var me = this;
    IDB.MapObject.initProps(me);
    me._stageMgr = stageMgr;
    me._group = new Kinetic.Group();
};

//////////////////////////////////////////////////////////////////
// public functions

IDB.MapObject.prototype.addToLayer = function(layer) {
    var me = this, group = me._group, built = me._built;

    me._layer = layer;
    if(!built) {
        me._build();
    }
    
    layer.add(group);
};


IDB.MapObject.prototype.setVisible = function(b) {
    this._group.visible(!!b);
};

IDB.MapObject.prototype.getVisible = function() {
    return this._group.visible();
};



IDB.MapObject.prototype.setLabelVisible = function(b) {
    var me = this, line = me._line, label = me._label, newVal = !!b, changed = (me._labelVisible !== newVal),
        mgr = me._stageMgr;
    
    me._labelVisible = newVal;
    if(!label || !changed) {
        return;
    }
    label.visible(newVal);
    if(line) {
        line.visible(newVal);
    }
    mgr.invalidateStage();
};


////////////////////////////////////////////////////////////////////////////
// private functions

IDB.MapObject.prototype._build = function() {
    var me = this, I = IDB, log = I.log;
    if(me._built) {
        log("WARNING: Already built: " + me._labelText);
    }
    me._built = true;
    me._createShape();
    me._createLabel();
    if(me._line) {
        me._line.moveToBottom();
    }
    if(me._label) {
        me._label.moveToTop();
    }

    if(me._blinkEnabled)  {
        me._blinkListener = I.listener(me, "_blink");
        setTimeout(me._blinkListener, me._blinkInterval);
    }
};

IDB.MapObject.prototype._blink = function() {
    var me = this, blinkOn = (me._blinkOn && !me._blinkPaused), 
        interval = blinkOn ? me._blinkOffInterval : me._blinkInterval,
        ks = me._kineticShape, changing = (me._blinkOn === blinkOn), opaque = me._blinkOnOpacity;
    if(me._destroyed || !me._blinkEnabled || !ks) {
        return;
    }
    if(changing) {
        me._blinkOn = !blinkOn;
        ks.setOpacity(me._blinkOn ? opaque : 0);
        me._layer.batchDraw();
    }
    me._blinkTimerID = window.setTimeout(me._blinkListener, interval);
};

IDB.MapObject.prototype._setShapeFromString = function(s) {
    var me = this, I = IDB, shape = I.normalizeShape(s), polyAttrs;
    me._shape = shape;
    if(shape === "poly") {
        polyAttrs = I.parsePoly(s);
        if(polyAttrs) {
            me._shape = "poly";
            me._polySides = polyAttrs.sides;
            me._polyAngle = polyAttrs.rotation;
        }
        else {
            me._polySides = 4;
            me._polyAngle = 0;
        }
    }
};

IDB.MapObject.prototype._createLabel = function() {
    var me = this, labelText = me._labelText, label, config = {}, n,
       color = me._labelColor || IDB.MapObject.BLACK, lw, lh, labelX, labelY, ho = me._labelHOffset, vo = me._labelVOffset,
       hasLine = me._hasLine;

    if(!labelText) {
        return;
    }

    config.visible = me._labelVisible;

    n = me._labelSize;
    if(isNaN(n)) n = 12;
    config.fontSize = Math.min(100, Math.max(5, n));
    config.fill = color.css;
    config.align = me._labelAlign;
        
    if(me._labelShadow) {
        config.shadowColor = color.x.css;
        config.shadowBlur = 5;
    }

    config.text = me._labelText;

    me._label = label = new Kinetic.Text(config);

    lw = label.width(); 
    lh = label.height();
    labelX = (-(lw/2) + ho); 
    labelY = (-(lh/2) + vo);
    label.x(labelX);
    label.y(labelY);

    if(hasLine) {
        me._createLine(lw, lh);
        if(me._line) {
            me._line.moveToBottom();
        }
    }

    me._group.add(label);
};

IDB.MapObject.prototype._createLine = function(lw, lh) {
    var me = this, group = me._group, line, color = (me._labelColor || IDB.MapObject.BLACK), 
       labelShadow = me._labelShadow, ho = me._labelHOffset, vo = me._labelVOffset,
       config = {stroke:color.css, strokeWidth:0.5, lineCap:"round", visible:(me._labelVisible)}, lineEnd, vsign, hsign, 
       halfLw = lw/2, 
       halfLh = lh/2, uLabelX = ho - halfLw, uLabelY = vo - halfLh, 
       lineSlope, labelSlope, lineSteeper, E, F;

    if(!me._hasLine) {
        return;
    }
    if(labelShadow) {
       config.shadowColor = color.x.css;
       config.shadowBlur = 5;
    }

    halfLw = lw / 2;
    halfLh = lh / 2;
    if(halfLw >= Math.abs(ho) && halfLh >= Math.abs(vo)) {
        // label covers center of point, no line.
//        line.visible(false);
        return;
    }

    vsign = (vo < 0) ? -1 : 1;
    hsign = (ho < 0) ? -1 : 1;

    if(!ho) {
        lineEnd = {x:ho, y:(vo - halfLh*vsign)};
    }
    else if(!vo) {
        lineEnd = {x:(ho - halfLw*hsign), y:0};
    }
    else {

        lineSlope = vo / ho;
        labelSlope = lh / lw;
    
        lineSteeper = Math.abs(lineSlope) > labelSlope;
    
        E = {};
        F = {};
    
        if(lineSteeper) { // intersects top or bottom side
            if(vo > 0) { // intersects top
                E.x = uLabelX;
                E.y = uLabelY;
                F.x = uLabelX + lw;
                F.y = uLabelY;
            }
            else {  // bottom
                E.x = uLabelX;
                E.y = uLabelY + lh;
                F.x = uLabelX + lw;
                F.y = uLabelY + lh;
            }
        }
        else { // intersects left or right side
            if(ho > 0) { // left
                E.x = uLabelX;
                E.y = uLabelY;
                F.x = uLabelX;
                F.y = uLabelY + lh;
            }
            else { // right
                E.x = uLabelX + lw;
                E.y = uLabelY;
                F.x = uLabelX + lw;
                F.y = uLabelY + lh;
            }
        }
    
        lineEnd = IDB.MapObject.findEndPoint({x:0, y:0},  {x:ho, y:vo}, E, F);
    }
    config.points = [0, 0, lineEnd.x, lineEnd.y];
    me._line = line = new Kinetic.Line(config);
    group.add(line);
};

IDB.MapObject.findEndPoint = function(A,B,E,F) {
    var ip = {}, a1, a2, b1, b2, c1, c2, denom;

    a1= B.y-A.y;
    b1= A.x-B.x;
    c1= B.x*A.y - A.x*B.y;
    a2= F.y-E.y;
    b2= E.x-F.x;
    c2= F.x*E.y - E.x*F.y;

    denom=a1*b2 - a2*b1;
    if (denom === 0) {
        return B;
    }
    ip.x=(b1*c2 - b2*c1)/denom;
    ip.y=(a2*c1 - a1*c2)/denom;
    return ip;
};

IDB.MapObject.prototype._createShape = function() {
    var me = this, I = IDB,   
        shape = me._shape, 
        color = me._shapeColor,
        size = me._size,
        config = {x:0, y:0, shadowColor:"#0F0F0F", shadowEnabled:false, shadowBlur:(me._haloHighlight ? me._shadowBlur : 0)},
        kineticShape;

    if(!me._hasShape) {
        return;
    }
    if(shape === "poly") {
        config = $.extend(config, {sides:me._polySides, rotation:me._polyAngle});
    }

    me._kineticShape = kineticShape = I.kShape(shape, color.css, size, config);
    me._group.add(kineticShape);
};

IDB.MapObject.prototype.move = function(x, y) {
    var me = this, group = me._group;
    group.x(x);
    group.y(y);
};


IDB.MapObject.prototype._onVisualState = function(newState) {
    
    var me = this, I = IDB, VS = I.VS, alpha = 1.0, shape = me._kineticShape, suppressLabel = (!me._label || !me._labelText),
        shadow = false, shadowBlur = 0, haloEnabled = me._haloHighlight, lVis = me._labelVisible && !suppressLabel;

    //IDB.log("_onVisualState: " + newState);

    me._visualState = newState;
    me._blinkPaused = false;

    if(newState === VS.FADED) {
        alpha = 0.3;
    }
    else if(newState === VS.HIGHLIGHTED) {
        me._blinkPaused = true;
        shadow = true;
        shadowBlur = me._shadowBlur;
        lVis = true && !suppressLabel;
    }

    shape.opacity(alpha);
    if(me._label) {
        me._label.visible(lVis);
        if(me._line) {
            me._line.visible(lVis);
        }
    }

    if(haloEnabled) {
        shape.shadowEnabled(shadow).shadowBlur(shadowBlur);
    }
};

IDB.MapObject.prototype.destroy = function() {
    var me = this;
    me._destroyed = true;

};




IDB.MapObject.BLACK = IDB.rgbx(0);
IDB.MapObject.initProps = function(newInstance) {
    var I = IDB, BLACK = I.MapObject.BLACK, 
    props = {
        _built:false,
        _destroyed:false,
        _layer:null,
        _stageMgr:null,
        _group:null,
        _scale:1.0,
        _labelScale:1.0,
        _hasLine:true,
        _line:null,
        _hasShape:true,
        _size:10,
        _shape:"circle",
        _kineticShape:null,
        _shapeColor:BLACK,
        _haloHighlight:true,
        _polySides:0,
        _polyAngle:NaN,
        _label:null,
        _labelText:null,
        _labelSize:16,
        _labelColor:BLACK,
        _labelAlign:"center",
        _labelRTL:false,
        _labelShadow:false,  
        _labelVOffset:-15,
        _labelHOffset:0,
        _labelVisible:true,
        _blinkTimerID:0,
        _blinkEnabled:false,
        _blinkOn:true,
        _blinkOnOpacity:1.0,
        _blinkInterval:1000,
        _blinkOffInterval:1000,
        _blinkFactor:1.0,
        _blinkPaused:false,
        _blinkListener:null,
        _touchHandler:null,
        _visualState:IDB.VS.NORMAL
    };

    $.extend(newInstance, props);
};


/*global IDB, $ */
IDB.PlotLIConfig = function($xml) {

    var me = this, I = IDB,
       get = function(name) {return $xml.idbFirstChildText(name) || null;},
       num = function(name, defaultVal) {if(defaultVal === undefined) {defaultVal = NaN;} 
           return $xml.children(name).idbTextToNumber(defaultVal);},
       $icon = $xml.children("icon").first(),
       s;

    me.shape = $icon.idbFirstChildText("shape") || null;
    me.color = null;
    s = $icon.idbFirstChildText("color") || null;
    if(s) {
       me.color = I.rgb(s, null);
    }
    me.text = $xml.idbFirstChildText("text") || null;
    me.isSwatch = (me.shape === null) && (me.color !== null);
    me.hasIcon = (me.isSwatch || !!me.shape);
};


IDB.PlotLIConfig.prototype.createLI = function(numColumns) {
    var me = this, I = IDB, $tr = $("<tr>"), hasIcon = me.hasIcon, $td, 
        colspan = numColumns, color = me.color, shape = (me.shape || "square");
    if(hasIcon) {
        $td  = $('<td class="idb-legend-item-icon">').appendTo($tr);
        if(color) {
            I.$kIcon(shape, color.css, 16).appendTo($td);
        }
        colspan = numColumns - 1;
    }
    $td = $('<td class="idb-legend-item-label">').appendTo($tr).text(me.text);
    if(colspan > 1) {
        $td.attr({"colspan":colspan});
    }
    return $tr;
};

/*global IDB, jQuery */

(function($, IDB) {

    $.widget("idb.slideshowButtonBar", {
        options:{
            buttonHeight:60,
            showStepButtons:true,
            showPlayPauseButton:true,
            visible:true,
            backCallback:null,
            forwardCallback:null,
            playCallback:null,
            pauseCallback:null 
        },
        _create:function() {
            var me = this, $e = me.element;
            $e.addClass("idb-slideshow-btn-bar");
            me._$btnBack = $("<a href=\"#\" class=\"idb-slideshow-btn idb-slideshow-btn-back\">")
                           .on("click", IDB.listener(me, "_onBtnClick"))
                           .on("mouseover", IDB.listener(me, "_onStepBtnMouseOver")) 
                           .on("mouseout", IDB.listener(me, "_onStepBtnMouseOut")); 
            me._$btnPlayPause = $("<a href=\"#\" class=\"idb-slideshow-btn idb-slideshow-btn-play\">")
                           .on("click", IDB.listener(me, "_onBtnClick")); 
            me._$btnForward = $("<a href=\"#\" class=\"idb-slideshow-btn idb-slideshow-btn-forward\">")
                           .on("click", IDB.listener(me, "_onBtnClick"))
                           .on("mouseover", IDB.listener(me, "_onStepBtnMouseOver")) 
                           .on("mouseout", IDB.listener(me, "_onStepBtnMouseOut")); 
            me._mouseIsOverStepButton = false;

            //
            // force a call to _setOption.
            //
            me.option(me.options);
        },
        _setOption:function(key, value) {
            var me = this, $e = me.element, showing, buttonSize;
            if(key === "buttonHeight") {
                buttonSize = value + "px";
                $e.css({"height":buttonSize});
                me._$btnBack.css({"height":buttonSize, "width":buttonSize});
                me._$btnForward.css({"height":buttonSize, "width":buttonSize});
                me._$btnPlayPause.css({"height":buttonSize, "width":buttonSize});
            }
            if(key === "visible") {
                if(value) {
                    $e.show();
                }
                else {
                    $e.hide();
                }
            }
            if(key === "showStepButtons") {
                showing = $.contains($e.get(0), me._$btnBack.get(0));
                if(value) {
                    if(!showing) {
                        me._$btnBack.appendTo($e);
                        me._$btnForward.appendTo($e);
                    }
                }
                else {
                    me._$btnBack.detach();
                    me._$btnForward.detach();
                }
            }
            if(key === "showPlayPauseButton") {
                showing = $.contains($e.get(0), me._$btnPlayPause.get(0));
                if(value) {
                    if(!showing) {
                        me._$btnPlayPause.appendTo($e);
                    }
                }
                else {
                    me._$btnPlayPause.detach();
                }
            }

            this._super(key, value);
            //this.refresh();
        },
        _layout:function() {
        },
        refresh:function() {
            this._layout();
        },
        resize:function(w, h) {
           var me = this, bh = me.options.buttonHeight, vPos = (h-bh)/2, $e = me.element, left;
           $e.css({"top":vPos + "px"}); 
           $e.width(w);
           left = (w-me._$btnPlayPause.width())/2;
           me._$btnPlayPause.css({"left":left + "px"}); 
        },
        startOrStopSlideshow:function() {
            var me = this;
            me._$btnPlayPause.trigger("click");
        },
        _onBtnClick:function(evt) {
            var me = this, o = me.options, btn = evt.currentTarget;
            if(btn === (me._$btnBack).get(0)) {
               me._maybeInvokeCallback(o.backCallback);
            }
            else if(btn === (me._$btnForward).get(0)) {
               me._maybeInvokeCallback(o.forwardCallback);
            }
            else {
               if(me._$btnPlayPause.hasClass("idb-slideshow-btn-play")){
                   me._maybeInvokeCallback(o.playCallback);
                   me._$btnPlayPause.removeClass("idb-slideshow-btn-play").addClass("idb-slideshow-btn-pause");
               }
               else {
                   me._maybeInvokeCallback(o.pauseCallback);
                   me._$btnPlayPause.removeClass("idb-slideshow-btn-pause").addClass("idb-slideshow-btn-play");
               }
            }
        },
        _maybeInvokeCallback:function(callback) {
            if(callback) {
                callback();
            }
        },
        _onStepBtnMouseOver:function(evt) {
           var me = this;
           me._mouseIsOverStepButton = true;
           if(me.options.showPlayPauseButton) {
               me._$btnPlayPause.hide(); 
           }
        },
        _onStepBtnMouseOut:function(evt) {
           var me = this;
           me._mouseIsOverStepButton = false;
           if(me.options.showPlayPauseButton) {
               me._$btnPlayPause.show(); 
           }
        },
        mouseIsOverStepButton:function() {
            return this._mouseIsOverStepButton;
        }
    });
})(jQuery, IDB);

/*====================================================================================================*/

