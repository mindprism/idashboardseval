'use strict';
IDB.BoundarySet = function($xml) {

    var me = this, _nLat, _sLat, _wLon, _eLon, _valid = false;

    IDB.log($xml[0], "mapboundaries");

    me._nLat = _nLat = $xml.idbFirstChild("north").idbTextToNumber(100);
    me._sLat = _sLat = $xml.idbFirstChild("south").idbTextToNumber(100);
    me._wLon = _wLon = $xml.idbFirstChild("west").idbTextToNumber(200);
    me._eLon = _eLon = $xml.idbFirstChild("east").idbTextToNumber(200);

    me._valid = _valid = ((_nLat > _sLat) && (_nLat <= 90) && (_nLat >= -90) && (_sLat <= 90) && (_sLat >= -90) &&
                          (_wLon != _eLon) && (_wLon <= 180) && (_wLon >= -180) && (_eLon <= 180) && (_eLon >= -180));

    me._flipped = false;
    me._widthDegrees = NaN;
    me._heightDegrees = NaN;

    if(!_valid) {
        IDB.log(me, "INVALID BOUNDARY SET");
        return;
    }

    if(_wLon > 0 && _eLon < 0) {  // map spans the 180 degrees meridian
        me._widthDegrees = (180-_wLon) + (180-Math.abs(_eLon));
        me._flipped = true;
    }
    else {
        me._widthDegrees = _eLon - _wLon;
    }
    me._heightDegrees = _nLat - _sLat;
};

IDB.BoundarySet.prototype.isValid = function() {
    return this._valid;
};

IDB.BoundarySet.prototype.isFlipped = function() {
    return this._flipped;
};

IDB.BoundarySet.prototype.getWidthDegrees = function() {
    return this._widthDegrees;
};

IDB.BoundarySet.prototype.getHeightDegrees = function() {
    return this._heightDegrees;
};

IDB.BoundarySet.prototype.getXPct = function($lon) {
//            trace("BoundarySet.as.getXPct(): $lon=" + $lon + ", _wLon=" + _wLon + ", _eLon=" + _eLon + ", _flipped=" + _flipped);
    if(isNaN($lon)) return NaN;
    var me = this, offset = 0;
    if($lon > 180 || $lon < -180) return NaN;
    if(me._flipped) {
        if($lon == 180 || $lon == -180) {
            offset = 180 - me._wLon;
        }
        else if($lon > 0) {
             offset = $lon - me._wLon;
        }
        else {
            offset = (180-me._wLon) + (-180-me._eLon);
        }
    }
    else {
        offset = $lon - me._wLon;
    }
    if(offset < 0 || offset > me._widthDegrees) return NaN; // off map
    return (offset / me._widthDegrees) * 100;
};

IDB.BoundarySet.prototype.getYPct = function($lat) {
    
    if(isNaN($lat)) return NaN;
    var me = this, offset = me._nLat - $lat;
    if(offset < 0 || offset > me._heightDegrees) return NaN; // off map
    var pct = (offset / me._heightDegrees) * 100;
//            trace("BoundarySet.as.getYPct(): $lat=" + $lat + ", _nLat=" + _nLat + ", offset=" + offset + ", pct=" + pct + ", _heightDegrees=" + _heightDegrees);
    return pct;
};

IDB.BoundarySet.prototype.contains = function(lat, lon) {
    var me = this, xpct = me.getXPct(lon), ypct = me.getYPct(lat);
    return me._valid && !isNaN(xpct) && !isNaN(ypct);
};
/*global IDB, $ */
IDB.ImagePlotConfig = function($xml, configFileUrl) {
    var me = this, I = IDB, $points = $xml.children("points").children("point"),
        $labels = $xml.children("labels").children("label"),
        $items = $xml.children("legend").first().children("items").children("item"),
        valueMap = {}, labelConfigs = [], liConfigs = [], pointConfig, liConfig, hasLegendIcon = false,
        $gps = $xml.idbFirstChild("gps"), $mb = $gps.idbFirstChild("mapboundaries"), bs, $insets = $gps.children("inset"), inset;
    me._valueMap = valueMap;
    
    
    for(var j=0, jj=$points.length; j<jj; j++) {
        pointConfig = new I.PlotPointConfig($($points[j]), valueMap);
    }

    me.labelConfigs = labelConfigs;
    for(var j=0, jj=$labels.length; j<jj; j++) {
        labelConfigs.push(new I.PlotLabelConfig($($labels[j])));
    }

    me.legendItemConfigs = liConfigs;
    for(var j=0, jj=$items.length; j<jj; j++) {
        liConfigs.push(liConfig = new I.PlotLIConfig($($items[j])));
        hasLegendIcon = hasLegendIcon || liConfig.hasIcon;
    }

    me.hasLegendIcon = hasLegendIcon;

    me.boundarySet = me._insets = null;;

    if($mb.length === 0 && $insets.length === 0) {
        return;
    }

    if($mb.length) {
        bs = new IDB.BoundarySet($mb);
        if(bs.isValid()) {
            me.boundarySet = bs;
        }
        else {
            throw new Error("The gps/mapboundaries element in " + configFileUrl + " is invalid.");  // do not localize
        }
    }

    if($insets.length) {
        me._insets = [];
        for(var j=0, jj=$insets.length; j<jj; j++) {
            inset = new IDB.PlotInset($($insets[j]));
            if(inset.isValid()) {
                me._insets.push(inset);
            }
            else {
                throw new Error("The gps/inset element at position " + (j+1) + " in " + configFileUrl + " is invalid.");  // do not localize
            }
        }
    }
};

IDB.ImagePlotConfig.prototype.findInset = function(lat, lon) {
    var me = this, insets = me._insets, inset;
    if(!insets) {
        return null;
    }
    for(var j=insets.length-1; j>=0; j--) {
        inset = insets[j];
        if(inset.contains(lat, lon)) {
            return inset;
        }
    }
    return null;
};


IDB.ImagePlotConfig.prototype.getPointConfig = function(xValue) { //(xValue:String) {
    xValue = $.trim(xValue);
    if(!xValue) return null;
    xValue = xValue.toLowerCase();
    //IDB.log(this._valueMap, xValue + ", " + this._valueMap[xValue]);
    return this._valueMap[xValue] || null;
};

/*global IDB, $, Kinetic */

IDB.ImagePlotGraph = function(chart, $div, graphProperties, dataSet) {
//    graphProperties._settings.UseRangeColors = false;
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
    var me = this, stage = me._stage, info = me._info;
    stage.add(me._lineLayer = new Kinetic.Layer());
    stage.add(me._pointLayer = new Kinetic.Layer());
    info.calloutStyle = "none";
    me._dpm.reset();
//	this._info.legendStyle = (this._graphContext.settings.getBool('BarFillRangeColors') ?
//		IDB.LS.RANGES : IDB.LS.AXES);

};

IDB.ImagePlotGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.ImagePlotGraph.prototype.constructor = IDB.ImagePlotGraph;

////////////////////////////////////////////////////////////////////////////
// static stuff


(function(IPG) {

    var shapeAxisNames = {
            _x:true,
            _y:true,
            _xpct:true,
            _ypct:true,
            _lat:true,
            _lon:true,
            _shape:true,
            _color:true,
            _size:true,
            _blink:true,
            _blinkfactor:true
        };

    IPG.shapeAxisNames = shapeAxisNames;

    /**
     * Returns true if the given axis name matches (case-INsensitive) one of the following:
     * _x, _y, _xpct, _ypct, _lat, _lon, _shape, _color, _size.
     */
    IPG.isShapeAxisName = function(axisName) {
        if(!axisName) return false;
        return !!shapeAxisNames[axisName.toLowerCase()];
    };

})(IDB.ImagePlotGraph);




//  "ImagePlot":{
//    "ImageUrlTemplate":null,
//    "UseProxy":false,
//    "ConfigXmlUrlTemplate":null,
//    "ConfigUseProxy":false,
//    "UseRangeColors":false,
//    "HaloHighlight":true,
//    "PointSize":12,
//    "PointShape":"circle",
//    "PointColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0},
//    "PointScale":false,
//    "PointLabelTemplate":null,
//    "PointLabelColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0},
//    "PointLabelSize":12,
//    "PointLabelShadow":true,
//    "PointLabelAlign":"center",
//    "PointLabelVerticalOffset":-15,
//    "PointLabelHorizontalOffset":0,
//    "PointLabelScale":false,
//    "PointLabelAlwaysVisible":false,
//    "LabelSize":16,
//    "LabelColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0},
//    "LabelAlign":"center",
//    "LabelRightToLeftText":false,
//    "LabelScale":false,
//    "ShowMouse":"none",
//    "CopyType":"sql",
//    "LocalImageFile":false,
//    "LocalConfigFile":false
//  },

////////////////////////////////////////////////////////////////////////////
// Overrides

IDB.ImagePlotGraph.prototype._build = function() {
    var me = this, I = IDB, IPG = I.ImagePlotGraph, $img, abs = {"position":"absolute"},
        gp = me.gp,
        chart = me._chart,
        builtin = (gp("Builtin") || "none"),
        bCode = (builtin == "none" ? null : builtin),
        url = (bCode ? I.resolveUrl(chart.expandMacros("map:" + bCode, 'url', null), false) : me.getUrl("ImageUrlTemplate", "UseProxy")),
        configUrl = (bCode ? I.resolveUrl(chart.expandMacros("mapconfig:" + bCode, 'url', null), false) : me.getUrl("ConfigXmlUrlTemplate", "ConfigUseProxy")),
        dataSet = me._dataSet, axes = dataSet.getGraphAxes(false), firstYAxis = axes[1],
        axis, rangeAxes = [], selectorAxes, s;

    me._imgW = 0;
    me._imgH = 0;
    me._imgAR = 0;
    
    me._imagePlotConfig = null;
    me._imgPending = true;
    me._configUrl = configUrl;
    me._configPending = false;
    me._$img = null;
    me._points = null;
    me._pointLabelShadowColor = null;
    me._destroyed = false;
    me._rangeLegendAxes = rangeAxes;
    me.rangeAxisIds = [];
    me._canUseRangeLegend = false;
    
    s = $.trim(gp("PointLabelTemplate"));
    me._suppressPointLabels = (s && s.match(/^\$\{\s*blank\s*\}$/ig));

    

    for(var j=1, jj=axes.length; j<jj; j++) {
        axis = axes[j];
        if(IPG.isShapeAxisName(axis.axisName)) {
            continue;
        }
        if(axis.dataTypeIsNumber) {
             me._firstNonShapeNumericYGraphAxis = axis;
            if( ! me._firstNonShapeYGraphAxis) {
                me._firstNonShapeYGraphAxis = axis;
            }
            break;
        }
        else {
            if( ! me._firstNonShapeYGraphAxis) {
                me._firstNonShapeYGraphAxis = axis;
            }
        }
    }

    if(me._firstNonShapeNumericYGraphAxis) {
        // A range legend is a possibility, so get the list of axes that will
        // be used in the legend.
        for(var j=0, jj=axes.length; j<jj; j++) {
            axis = axes[j];
            if(j===0 || (!IPG.isShapeAxisName(axis.axisName) && axis.dataTypeIsNumber)) {
                rangeAxes.push(axis);
                if(j > 0) {
                    me.rangeAxisIds.push(axis.axisId);
                }
            }
        }
    }

    if(gp("UseRangeColors")) {
        if(me._firstNonShapeNumericYGraphAxis) {
            me._dataYGraphAxis = me._firstNonShapeNumericYGraphAxis;
            me._canUseRangeLegend = true;
            selectorAxes = rangeAxes.concat();
            selectorAxes.shift();
            me._axisSelector = new I.AxisSelector($('<div class="idb-axis-selector">').prependTo(me._$div), me, selectorAxes);
        }
        else if(me._firstNonShapeYGraphAxis) {
            me._dataYGraphAxis = me._firstNonShapeYGraphAxis;
        }
        else {
            me._dataYGraphAxis = firstYAxis;
        }
    }
    else {
        if(me._firstNonShapeYGraphAxis) {
            me._dataYGraphAxis = me._firstNonShapeYGraphAxis;
        }
        else {
            me._dataYGraphAxis = firstYAxis;
        }
    }
    if(configUrl) {
        me._configPending = true;
         $.ajax({
             url:configUrl,
             dataType:"xml",
             type:"GET",
             success:this.onConfigLoaded,
             context:this,
             error:this.onConfigFail
             });

    }
    else {
        if(me._canUseRangeLegend) {
            setTimeout(IDB.listener(me, "_initLegend"), 0);
        }
    }

    me._$img = $img = $('<img class="idb-imageplot-img">').prependTo(me._$div).attr("src", url).css(abs).one("load", I.listener(me, "_onLoad")).css({"visibility":"hidden"});

};

IDB.ImagePlotGraph.prototype.destroy = function() {
    var me = this, destroyed = me._destroyed, points = me._points;
    IDB.log(me, "destroy(): destroyed=" + destroyed);
    if(destroyed) {
       return;
    }
    if(points) {
        for(var j=0, jj=points.length; j<jj; j++) {
            points[j].destroy();
        }
        me._points = null;
    }
    if(me._axisSelector) {
       me._axisSelector.destroy();
       me._axisSelector = false;
    }
    IDB.GraphBaseKinetic.prototype.destroy.call(me);
    me._destroyed = true;

};


IDB.ImagePlotGraph.prototype.getLegendAxes = function() {
    var me = this, info = me._info, I = IDB, LS = I.LS;
    if(info.legendStyle === LS.RANGES) {
        return me._rangeLegendAxes;
    }
    return [];

};

IDB.ImagePlotGraph.prototype.createLegendItems = function() {
    var me = this, arr = [], imagePlotConfig = me._imagePlotConfig, hasIcon, numCols, liConfigs;
    if(!imagePlotConfig) {
        return [];
    }
    hasIcon = imagePlotConfig.hasLegendIcon;
    numCols = hasIcon ? 2 : 1;
    liConfigs = imagePlotConfig.legendItemConfigs;
    for(var j=0, jj=liConfigs.length; j<jj; j++) {
        arr.push(liConfigs[j].createLI(numCols));
    }

    return arr;
};

/**
 * This overrides the method in GraphBase to prevent all the datapoints from being added
 * to the DatapointManager. This graph does that when the PlotPoints are created and when the selected
 * axis is changed.
 */
IDB.ImagePlotGraph.prototype._getRelevantDatapoints = function() {
	return [];
};


////////////////////////////////////////////////////////////////////////////


IDB.ImagePlotGraph.prototype._initLegend = function() {
    var me = this, chart = me._chart, info = me._info, I = IDB, LS = I.LS, canUseRangeLegend = me._canUseRangeLegend,
        imagePlotConfig = me._imagePlotConfig;
    if(imagePlotConfig && imagePlotConfig.legendItemConfigs.length) {
        if(canUseRangeLegend && "ranges" === me.gp("LegendPriority")) {
            info.legendStyle = LS.RANGES;
        }
        else {
            info.legendStyle = LS.CUSTOM;
        }
        chart.legendStyleChanged(me, info.legendStyle);
    }
    else if(canUseRangeLegend) {
        info.legendStyle = LS.RANGES;
        chart.legendStyleChanged(me, LS.RANGES);
    }
    else {
        IDB.log("_initLegend(): Doing nothing.");
    }
};

IDB.ImagePlotGraph.prototype.maybeCreatePoints = function() {
   var me = this, graphContext = me._graphContext, gp = me.gp,
       dpGrid = graphContext.datapointGrid, dpRows = dpGrid._datapointRows, points, dpRow, point,
       config = me._imagePlotConfig, labels, labelConfigs, label, insets,
       labelSize = gp("LabelSize"), labelColor = gp("LabelColor"), labelAlign = gp("LabelAlign"), labelRtl = gp("LabelRightToLeftText");
//   I.log("maybeCreatePoints(): me._imgPending=" + me._imgPending + ", me._configPending=" + me._configPending);

   if(me._imgPending || me._configPending) {
       return;
   }

    me._points = points = [];
    me._labels = labels = [];

    for(var j=0, jj=dpRows.length; j<jj; j++) {
        dpRow = dpRows[j];
        point = new IDB.PlotPoint(me, dpRow);
        points.push(point);
    }

    if(config) {
        labelConfigs = config.labelConfigs;
        for(var j=0, jj=labelConfigs.length; j<jj; j++) {
            label = new IDB.PlotLabel(me, labelConfigs[j], labelSize, labelColor, labelAlign, labelRtl);
            labels.push(label);
        }
        insets = config._insets;
        if(insets) {
            for(var j=0, jj=insets.length; j<jj; j++) {
                var rect = insets[j]._rect;
                if(rect) {
                    me._lineLayer.add(rect);
                }
            }
        }

    }
    me.resize();
};


IDB.ImagePlotGraph.prototype.getUrl = function(urlProp, proxyProp) {
   var me = this, I = IDB, config = I.config, gp = me.gp, url = (gp(urlProp) || ""), urlLC = url.toLowerCase(),
       isServerUrl = (urlLC.indexOf("content:") === 0 || urlLC.indexOf("map:") === 0 || urlLC.indexOf("mapconfig:") === 0),
       useProxy = (!isServerUrl && config.proxyEnabled && !!gp(proxyProp)), chart = me._chart;

   return I.resolveUrl(chart.expandMacros(url, 'url', null), useProxy);
};

IDB.ImagePlotGraph.prototype._update = function(w, h) {
    var me = this, $img = me._$img, imgAR = me._imgAR, ar = w/h, imgW, imgH, imgTop, imgLeft, points = me._points,
        pointX, pointY, labelX, labelY, lineX, lineY, imgResizer, gp = me.gp,
        point, scalePoints = gp("PointScale"), scaleLabels = (scalePoints && gp("PointLabelScale")), mapScale, labels = me._labels, label,
        scaleMapLabels = gp("LabelScale"), axisSelector = me._axisSelector, $selector, selectorHeight = 0,
        mc = me._imagePlotConfig, insets = mc ? mc._insets : null;

    if(!imgAR) {
        return;
    }

    if(axisSelector) {
        $selector = axisSelector._$div;
        selectorHeight = axisSelector.measuredHeight();
        $selector.width(w + "px").height(selectorHeight + "px");
        h = h-selectorHeight;
        ar = w/h;

        $selector.css({"top":h+"px"});
        axisSelector.resize();
    }

    if(imgAR > ar) {
       imgW = w;
       imgH = imgW / imgAR;
       imgLeft = 0;
       imgTop = (h - imgH) / 2;
    }
    else {
       imgH = h;
       imgW = imgH * imgAR;
       imgLeft = (w-imgW)/2;
       imgTop = 0;
    }

    mapScale = imgW / me._imgW;

    imgResizer = function() {
        $img.css({"top": imgTop+"px", "left":imgLeft+"px", "width":imgW+"px", "height":imgH+"px"}).css({"visibility":"visible"});
    };

    if(insets) {
        for(var j=0, jj=insets.length; j<jj; j++) {
            insets[j].pos(imgLeft, imgTop, me._imgW, me._imgH, imgW, imgH);
        }
    }

    if(points) {
        for(var j=0, jj=points.length; j<jj; j++) {
            point = points[j];
            pointX = point.getPointX(me._imgW, imgW);
            pointY = point.getPointY(me._imgH, imgH);
            point.moveTo(pointX + imgLeft, pointY + imgTop, scalePoints, scaleLabels, mapScale);
        }

        for(var j=0, jj=labels.length; j<jj; j++) {
            label = labels[j];
            labelX = imgLeft + label.getPointX(me._imgW, imgW);
            labelY = imgTop + label.getPointY(me._imgH, imgH);
            if(label._lineViewable) {
                lineX = imgLeft + label.getLinePointX(me._imgW, imgW);
                lineY = imgTop + label.getLinePointY(me._imgH, imgH);
                label.lineXOffset = lineX - labelX;
                label.lineYOffset = lineY - labelY;
            }

            label.moveTo(labelX, labelY, scaleMapLabels, mapScale);
         }

         // Delay the image resize, like the stage redraw, so they occur at roughly the same time. Otherwise
         // on iPad you see the image resize followed by a pause, followed by the points being repositioned.
         setTimeout(imgResizer, 0);
     }
     else {
        imgResizer(); // resize image immediately.
     }
};

IDB.ImagePlotGraph.prototype.width = function() {
    return this._$div.width();
};

IDB.ImagePlotGraph.prototype.height = function() {
    return this._$div.height();
};


////////////////////////////////////////////////////////////////////////////
// Public methods
IDB.ImagePlotGraph.prototype.getDataYGraphAxisId = function() {
    var me = this;
    return me._dataYGraphAxis ? me._dataYGraphAxis.axisId : -1;
};

IDB.ImagePlotGraph.prototype.getPointLabelShadowColor = function() {
    var me = this, shadow = me._pointLabelShadowColor, I = IDB;
    if(shadow) {
        return shadow;
    }
    me._pointLabelShadowColor = shadow = I.ColorHelper.getContrastingColor(me.gp("PointLabelColor")).css;
    return shadow;
};

IDB.ImagePlotGraph.prototype.axisSelected = function(axis) {
    var me = this, points = me._points, dpm = me._dpm, chart = me._chart;
    dpm.reset();  // remove all datapoints from the DPM
    for(var j=0, jj=points.length; j<jj; j++) {
        points[j].setRangeAxis(axis);  // this call will add relevant datapoints back to DPM.
    }
    chart.yAxisSelected(me, axis);
    me.refreshDisplay();
};

////////////////////////////////////////////////////////////////////////////
// Event Handlers
/* jshint unused:true */
IDB.ImagePlotGraph.prototype._onLoad = function(evt) {
/* jshint unused:false */
    var me = this, $img = me._$img;
    me._imgW = $img.width();
    me._imgH = $img.height();
    me._imgAR = me._imgH ? (me._imgW / me._imgH) : 0;
//    IDB.log(evt, "_onLoad(): " + $img.width() + ", " + $img.height());
    me._imgPending = false;
    me.resize();
    
    me.maybeCreatePoints();
};

/* jshint unused:true */
IDB.ImagePlotGraph.prototype.onConfigLoaded = function(data, status, jqXHR) {
/* jshint unused:false */

    IDB.log(data);

    var me = this, I = IDB, $xml = $(data).children("imageplot"), chart = me._chart, msg;

    if($xml.length === 0) {
        msg = IDB.format("ImagePlot.html5.configFileInvalidRoot", me._configUrl, "imageplot");
        chart.displayUserMessage(me, msg, "error", null);
        return;
    }

    try {
        me._imagePlotConfig = new I.ImagePlotConfig($xml, me._configUrl);
    }
    catch(err) {
        chart.displayUserMessage(me, err.message, "error", null);
        return;
    }

    if(me._imagePlotConfig.legendItemConfigs.length > 0 || me._canUseRangeLegend) {
        setTimeout(IDB.listener(me, "_initLegend"), 0);
    }

    me._configPending = false;
    me.maybeCreatePoints();
};


IDB.ImagePlotGraph.prototype.onConfigFail = function() {
    IDB.log(arguments, "onConfigFail()");
   var me = this, chart = me._chart,             // TODO: i18n
        msg = IDB.format("ImagePlot.html5.configFileError", me._configUrl);
        chart.displayUserMessage(me, msg, "warning", null);
};

/*global IDB, Kinetic, $ */
IDB.PlotInset = function($xml) {
    var me = this, $topleft = $xml.idbFirstChild("topleft"), $bottomright = $xml.idbFirstChild("bottomRight"), $mb = $xml.idbFirstChild("mapboundaries"),
        num = function($x, name) {
            return $x.children(name).idbTextToNumber(NaN);
        },
        dim = function(smaller, bigger) {
             if(isNaN(smaller) || isNaN(bigger)) return NaN;
             var dim = bigger - smaller;
             if(dim <= 0) return NaN;
             return dim;
        };

    me._valid = false;
    me.boundarySet =  me._xFunction = me._yFunction = me._rect = null;
    me._insetX = me._insetY = me._insetW = me._insetH = 0;
    me._leftX = me._leftXPct = me._rightX = me._rightXPct = me._topY = me._topYPct = me._bottomY = me._bottomYPct =
        me._width = me._widthPct = me._height = me._heightPct = NaN;

    if(!$topleft.length || !$bottomright.length || !$mb.length) {
        return;
    }

    me._leftX = num($topleft, "x");
    me._leftXPct = num($topleft, "xpct");

    me._rightX = num($bottomright, "x");
    me._rightXPct = num($bottomright, "xpct");

    me._topY = num($topleft, "y");
    me._topYPct = num($topleft, "ypct");

    me._bottomY = num($bottomright, "y");
    me._bottomYPct = num($bottomright, "ypct");

    me._widthPct = dim(me._leftXPct, me._rightXPct);
    if(!isNaN(me._widthPct)) {
        me._xFunction = "getXFromPct";
    }
    else {
        me._width = dim(me._leftX, me._rightX);
        if(!isNaN(me._width)) {
            me._xFunction = "getXFromPixels";
        }
    }

    me._heightPct = dim(me._topYPct, me._bottomYPct);
    if(!isNaN(me._heightPct)) {
        me._yFunction = "getYFromPct";
    }
    else {
        me._height = dim(me._topY, me._bottomY);
        if(!isNaN(me._height)) {
            me._yFunction = "getYFromPixels";
        }
    }

    if( !me._xFunction || !me._yFunction ) {
        return;
    }

    me.boundarySet = new IDB.BoundarySet($mb);

    me._valid = me.boundarySet.isValid();

    if(!me._valid) {
        return;
    }

    if("true" === $.trim($xml.attr("frame")).toLowerCase()) {
        me._rect = new Kinetic.Rect({ stroke:"#000000", strokeWidth:1, visible:true, x:0, y:0, width:100, height:100});
    }

};

IDB.PlotInset.prototype.isValid = function() {
    return this._valid;
};

IDB.PlotInset.prototype.contains = function(lat, lon) {
    var me = this, bs = me.boundarySet;
    return me._valid && bs.contains(lat, lon);
};

IDB.PlotInset.prototype.pos = function(mapX, mapY, fullW, fullH, currW, currH) {
    var me = this, rect = me._rect;
    me._insetX = me.getInsetX(fullW, currW);
    me._insetY = me.getInsetY(fullH, currH);

    if(me._xFunction === "getXFromPct") {
        me._insetW = me._widthPct * currW / 100;
    }
    else {
        me._insetW = currW / fullW * me._width;
    }

    if(me._yFunction === "getYFromPct") {
        me._insetH = me._heightPct * currH / 100;
    }
    else {
        me._insetH = currH / fullH * me._height;
    }
    IDB.log(me, "insetX=" + me._insetX + ", insetY=" + me._insetY + ", insetW=" + me._insetW + ", insetH=" + me._insetH);

    if(rect) {
        rect.x(mapX + me._insetX).y(mapY + me._insetY).width(me._insetW).height(me._insetH);
    }
};

IDB.PlotInset.prototype.getXFromPixels = function(fullMapWidth, currentMapWidth) {
    return (currentMapWidth / fullMapWidth) * this._leftX;
};

/* jshint unused:true */
IDB.PlotInset.prototype.getXFromPct = function(fullMapWidth, currentMapWidth) {
/* jshint unused:false */
    return this._leftXPct * currentMapWidth / 100;
};


IDB.PlotInset.prototype.getInsetX = function(fullMapWidth, currentMapWidth) {
    var me = this;
    if(!me._valid) return NaN;
    return me[me._xFunction](fullMapWidth, currentMapWidth);
};


IDB.PlotInset.prototype.getYFromPixels = function(fullMapHeight, currentMapHeight) {
    return (currentMapHeight / fullMapHeight) * this._topY;
};

/* jshint unused:true */
IDB.PlotInset.prototype.getYFromPct = function(fullMapHeight, currentMapHeight) {
/* jshint unused:false */
    return this._topYPct * currentMapHeight / 100;
};

IDB.PlotInset.prototype.getInsetY = function(fullMapHeight, currentMapHeight) {
    var me = this;
    if(!me._valid) return NaN;
    return me[me._yFunction](fullMapHeight, currentMapHeight);
};

/*global IDB, Kinetic */
IDB.PlotLabel = function(graph, config, size, color, align, rtl) {
    var me = this, I = IDB;
    me._graph = graph;
    me._group = null;
    me._label = null;
    me._line = null;
    me._text = config.text;
    me._align = config.align || align;
    me._size = config.getSize(size);
    me._color = config.color || color;
    me._rtl = I.ifNil(config.rtl, rtl);
    
    me._xPct = config.xPct;
    me._yPct = config.yPct;
    me._xPixels = config.xPixels;
    me._yPixels = config.yPixels;

    me._lineXPct = config.lineXPct;
    me._lineYPct = config.lineYPct;
    me._lineXPixels = config.lineXPixels;
    me._lineYPixels = config.lineYPixels;

    me._xFunction = null;
    me._yFunction = null;

    me._lineXFunction = null;
    me._lineYFunction = null;


    me.lineXOffset = 0;
    me.lineYOffset = 0;

    me._viewable = !!me._text && ((!isNaN(me._xPct) || !isNaN(me._xPixels)) && (!isNaN(me._yPct) || !isNaN(me._yPixels)));


    me._lineViewable = me._viewable &&  ((!isNaN(me._lineXPct) || !isNaN(me._lineXPixels)) && (!isNaN(me._lineYPct) || !isNaN(me._lineYPixels)));

    if(me._viewable) {
        if(!isNaN(me._xPixels)) {
            me._xFunction = "getXFromPixels";
        }
        else {
            me._xFunction = "getXFromPct";
        }

        if(!isNaN(me._yPixels)) {
            me._yFunction = "getYFromPixels";
        }
        else {
            me._yFunction = "getYFromPct";
        }

        if(me._lineViewable) {
            if(!isNaN(me._lineXPixels)) {
                me._lineXFunction = "getLineXFromPixels";
            }
            else {
                me._lineXFunction = "getLineXFromPct";
            }

            if(!isNaN(me._lineYPixels)) {
                me._lineYFunction = "getLineYFromPixels";
            }
            else {
                me._lineYFunction = "getLineYFromPct";
            }
        }


        me._build();
    }


};


IDB.PlotLabel.prototype.getXFromPixels = function(fullMapWidth, currentMapWidth) {
    return (currentMapWidth / fullMapWidth) * this._xPixels;
};

/* jshint unused:true */
IDB.PlotLabel.prototype.getXFromPct = function(fullMapWidth, currentMapWidth) {
/* jshint unused:false */
    return this._xPct * currentMapWidth / 100;
};


IDB.PlotLabel.prototype.getPointX = function(fullMapWidth, currentMapWidth) {
    var me = this;
    if(!me._viewable) {
        return NaN;
    }
    return me[me._xFunction](fullMapWidth, currentMapWidth);
};

IDB.PlotLabel.prototype.getYFromPixels = function(fullMapHeight, currentMapHeight) {
    return (currentMapHeight / fullMapHeight) * this._yPixels;
};

/* jshint unused:true */
IDB.PlotLabel.prototype.getYFromPct = function(fullMapHeight, currentMapHeight) {
/* jshint unused:false */
    return this._yPct * currentMapHeight / 100;
};

IDB.PlotLabel.prototype.getPointY = function(fullMapHeight, currentMapHeight) {
    var me = this;
    if(!me._viewable) {
        return NaN;
    }
    return me[me._yFunction](fullMapHeight, currentMapHeight);
};




IDB.PlotLabel.prototype.getLineXFromPixels = function(fullMapWidth, currentMapWidth) {
    return (currentMapWidth / fullMapWidth) * this._lineXPixels;
};

IDB.PlotLabel.prototype.getLineXFromPct = function(fullMapWidth, currentMapWidth) {
    return this._lineXPct * currentMapWidth / 100;
};


IDB.PlotLabel.prototype.getLinePointX = function(fullMapWidth, currentMapWidth) {
    var me = this;
    if(!me._lineViewable) return NaN;
    return me[me._lineXFunction](fullMapWidth, currentMapWidth);
};


IDB.PlotLabel.prototype.getLineYFromPixels = function(fullMapHeight, currentMapHeight) {
    return (currentMapHeight / fullMapHeight) * this._lineYPixels;
};

IDB.PlotLabel.prototype.getLineYFromPct = function(fullMapHeight, currentMapHeight) {
    return this._lineYPct * currentMapHeight / 100;

};

IDB.PlotLabel.prototype.getLinePointY = function(fullMapHeight, currentMapHeight) {
    var me = this;
    if(!me._lineViewable) return NaN;
    return me[me._lineYFunction](fullMapHeight, currentMapHeight);
};



IDB.PlotLabel.prototype._build = function() {
    var me = this, graph = me._graph, group,
        config = {text:me._text, visible:false, fontSize:me._size, fill:me._color.css, align:me._align},
        label = new Kinetic.Text(config), line;

//    IDB.log(config, "LABEL CONFIG.");

    me._group = group = new Kinetic.Group();
    graph._stageLayer.add(group);
    me._label = label;
    group.add(label);

    if(me._lineViewable) {
        me._line = line = new Kinetic.Line(  {stroke:me._color.css, strokeWidth:1, visible:false, lineCap:"round", points:[0,0,100,100]}  );
        group.add(line);
    }
};


IDB.PlotLabel.prototype.moveTo = function(x, y, scale, mapScale) {
    var me = this, group = me._group, label = me._label, LW, lw, LH, lh, lx, ly, line = me._line,
        lineX = me.lineXOffset, endX = -lineX, lineY = me.lineYOffset, endY = -lineY,
        labelLeft, labelTop, labelRight, labelBottom, halfLW, halfLH, lineSlope, labelSlope, E, F, endPoint;
    if(!label) {
        return;
    }
    group.x(x).y(y);


    LW = lw = label.width();
    LH = lh = label.height();
    if(scale) {
        lw = lw * mapScale;
        lh = lh * mapScale;
        label.scaleX(mapScale).scaleY(mapScale);
    }
    lx = -(lw/2);
    ly = -(lh/2);
    label.x(lx).y(ly);
    label.visible(true);

    if(!line || !me._lineViewable) {
        return;
    }

    line.visible(true);

    if(scale) {
        line.scaleX(mapScale).scaleY(mapScale);
        endX = endX / mapScale;
        endY = endY / mapScale;
    }

    halfLW = LW/2;
    halfLH = LH/2;

//    IDB.log(me, "lineX=" + lineX + ", lineY=" + lineY + ", endX=" + endX + ", endY=" + endY + ", halfLW=" + halfLW + ", halfLH=" + halfLH + " ----------------------------------------------------- " );

    if(Math.abs(endX) < halfLW && Math.abs(endY) < halfLH) {
//        IDB.log(">>>>>>>>>>>>>>>>>>>>>>NO LINE<<<<<<<<<<<<<<<<<<<<<<");
        line.visible(false);
        return;
    }

    line.visible(true);

    if(!endX) { // vertical line
        endY = endY - (endY < 0 ? -halfLH : halfLH);
    }
    else if(!endY) { // horizontal line
        endX = endX - (endX < 0 ? -halfLW : halfLW);
    }
    else {
        labelLeft = endX - halfLW;
        labelRight = endX + halfLW;
        labelTop = endY - halfLH;
        labelBottom = endY + halfLH;

        lineSlope = endY / endX;
        labelSlope = LH / LW;

        E = {};
        F = {};

        if(Math.abs(lineSlope) > labelSlope) {
            E.x =  labelLeft;
            F.x = labelRight;
            E.y = F.y = ((endY < 0) ? labelBottom : labelTop);
        }
        else {
            E.y = labelTop;
            F.y = labelBottom;
            E.x = F.x = ((endX < 0) ? labelRight : labelLeft);
        }

        endPoint = IDB.PlotPoint.findEndPoint({x:0, y:0}, {x:endX, y:endY}, E, F);
        endX = endPoint.x;
        endY = endPoint.y;
    }

    line.x(lineX).y(lineY);
    line.points([0, 0, endX, endY]);
};




/*global IDB */
IDB.PlotLabelConfig = function($xml) {

    var me = this,
       $line = $xml.idbFirstChild("line"),
       get = function(name) {return $xml.idbFirstChildText(name) || null;},
       num = function(name) {return $xml.children(name).idbTextToNumber(NaN);},
       lineNum = function(name) {return $line.children(name).idbTextToNumber(NaN);},
       s;

    me.text = get("value");
    me.align = get("align");
    me.size = num("size");
    me.color = null;
    s = get("color");
    if(s) {
        me.color = IDB.rgb(s, IDB.rgb.BLACK);
    }

    me._rtlString = get("rtl");
    me.xPct = num("xpct");
    me.yPct = num("ypct");
    me.xPixels = num("x");
    me.yPixels = num("y");

    me.lineXPct = lineNum("xpct");
    me.lineYPct = lineNum("ypct");
    me.lineXPixels = lineNum("x");
    me.lineYPixels = lineNum("y");
};

IDB.PlotLabelConfig.prototype.getSize = function(defVal) {
    var me = this, size = me.size;
    return (!size || isNaN(size)) ? defVal : size;
};

/*global Kinetic, IDB, $ */
IDB.PlotPoint = function($graph, $dpRow) {
    var me = this, gp = $graph.gp;
    me._graph = $graph;
    me._dpRow = $dpRow;
    me._visible = false;

    me._destroyed = false;
    me._datapoint = null;
    me._currDp = null;
    me._scale = 1.0;
    me._labelScale = 1.0;
    me._label = null;
    me._suppressLabel = $graph._suppressPointLabels;
    me._pointLabelAlwaysVisible =  !me._suppressLabel && gp("PointLabelAlwaysVisible");

    me._pointLabelVerticalOffset = gp("PointLabelVerticalOffset");
    me._pointLabelHorizontalOffset = gp("PointLabelHorizontalOffset");

    me._group = null;
    me._line = null;
    me._xPct = NaN;
    me._yPct = NaN;
    me._xPixels = NaN;
    me._yPixels = NaN;
    me._lat = NaN;
    me._lon = NaN;
    me._inset = null;
    me._size = NaN;
    me._shape = null;
    me._kineticShape = null;
    me._baseColor = null;
    me._polySides = 0;
    me._polyAngle = NaN;
    
    me._haloHighlight = true;
    
    me._viewable = false;
    me._viewableYAxes = {};
    
    me._xFunction = null;
    me._yFunction = null;
    
    me._blinkTimer = null;
    me._blinkEnabled = false;
    me._blinkOn = false;
    me._blinkInterval = 1000;
    me._blinkOffInterval = 1000;
    me._blinkFactor = 1.0;
    me._blinkListener = null;

    me._touchHandler = null;
    me._shadowBlur = 20;

    me._noLine = false;
    me._lineEnd = null;
    me._rangeDatapoints = [];
    me._labelTemplate = null;

    me._build();
};


IDB.PlotPoint.shapeAxisNames = {
    _x:true,
    _y:true,
    _xpct:true,
    _ypct:true,
    _lat:true,
    _lon:true,
    _shape:true,
    _color:true,
    _size:true,
    _blink:true,
    _blinkfactor:true
};


IDB.PlotPoint.prototype._build = function() {
   var me = this, I = IDB, listener = I.listener, stateListener, IPG = I.ImagePlotGraph, graph = me._graph,
      dpRow = me._dpRow, mc = graph._imagePlotConfig, pc, bs = mc ? mc.boundarySet : null, dataSet = graph._dataSet,
      axes = dataSet._axisList, datapoints = dpRow.datapoints, datapoint, dataValues = dpRow.dataRow, xFound = false, yFound = false,
      axis, name, val, gp = graph.gp, dv, $blinkEnabled = false, $blinkInterval = 1000, $blinkFactor = 1.0, lat, lon,
      n=0, rangeDatapoints = me._rangeDatapoints,
      dataAxisId = graph.getDataYGraphAxisId(), rangeAxisIds = graph.rangeAxisIds, useRangeColors = gp("UseRangeColors");


    me._haloHighlight = gp("HaloHighlight");

    for(var j=0, jj=datapoints.length; j<jj; j++) {
        datapoint = datapoints[j];
        rangeDatapoints[datapoint.yAxis.axisId] = datapoint;
    }

    me._datapoint = me._currDp = datapoint = rangeDatapoints[dataAxisId];

    if(mc) {
        pc = mc.getPointConfig(dpRow.xValue.stringValue);
        if(pc) {
            me._shape = pc.shape;
            if(me._shape === "poly") {
                me._polySides = pc.polySides;
                me._polyAngle = pc.polyAngle;
//                IDB.log(me, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + pc.blinkInterval);
            }
            me._size = pc.size;
            me._baseColor = pc.baseColor;
            me._xPct = pc.xPct;
            me._yPct = pc.yPct;
            me._xPixels = pc.xPixels;
            me._yPixels = pc.yPixels;
            me._lat = lat = pc.lat;
            me._lon = lon = pc.lon;
            $blinkEnabled = pc.blinkInterval > 0;
            if($blinkEnabled) {
                $blinkInterval = Math.max(100, pc.blinkInterval);
                $blinkFactor = pc.blinkFactor;
            }

            if(bs && bs.isValid()) {
                if(!isNaN(lat)) {
                    me._yPct = bs.getYPct(lat);
                }
                if(!isNaN(lon)) {
                    me._xPct = bs.getXPct(lon);
                }
            }
        }
    }


    for(var j=1, jj=axes.length; j<jj; j++) {
        axis = axes[j];
        if(axis.isPivot) continue;

        name = axis.axisName.toLowerCase();
        
        if(!IPG.isShapeAxisName(name)) {
            continue;
        }

        dv = dataValues[j];
        if(dv.isNothing) {
            continue;
        }

        if(axis.dataTypeIsNumber) {
//                        trace("ImageMapPoint.as.datapointRow(): name=" + name + ", " + axis.dataTypeIsNumber + ", " + ImageMapGraph.isShapeAxisName(name));
            // extract the numeric point metadata from the row.
            val = dv.numberValue;
            if(!xFound) {
                if(name === "_x") {
                    me._xPixels = val;
                    me._xFunction = "getXFromPixels";
                    xFound = true;
                }
                else if(name === "_lon") {
                    me._lon = val;
                    if(bs && bs.isValid()) {
                        n = bs.getXPct(val);
                        if(!isNaN(n)) {
                            me._xPct = n;
                            me._xFunction = "getXFromPct";
                            xFound = true;
                        }
                    }
                }
                else if(name === "_xpct") {
                    me._xPct = val;
                    me._xFunction = "getXFromPct";
                    xFound = true;
                }
            }

            if(!yFound) {
                if(name === "_y") {
                    me._yPixels = val;
                    me._yFunction = "getYFromPixels";
                    yFound = true;
                }
                else if(name === "_lat" && !dv.isNothing) {
                    me._lat = val;
                    if(bs && bs.isValid()) {
                        n = bs.getYPct(val);
                        if(!isNaN(n)) {
                            me._yPct = n;
                            me._yFunction = "getYFromPct";
                            yFound = true;
                        }
                    }
                }
                else if(name === "_ypct") {
                    me._yPct = val;
                    me._yFunction = "getYFromPct";
                    yFound = true;
                }
            }

            if(xFound && yFound) {
                me._viewable = true;
            }

            if(name === "_color") {
                me._baseColor = IDB.rgbx(val, IDB.rgb.BLACK);
            }
            else if(name === "_size") {
                me._size = Math.max(Math.min(100, val), 5);
            }
            else if(name === "_blink") {
                if(val > 0) {
                    $blinkEnabled = true;
                    $blinkInterval = Math.max(100, val);
                }
            }
            else if(name === "_blinkfactor") {
                $blinkFactor = val;
            }
        }
        else if(axis.dataTypeIsString) {
            var s = $.trim(dv.stringValue);
            if(s) {
                s = s.toLowerCase();
                if(name === "_color") {
                    me._baseColor = IDB.rgbx(s, IDB.rgb.BLACK);
                }
                if(name == "_shape") {
                    me._setShapeFromString(s);
                }
            }
        }
    }

    if(!isNaN(me._lat) && !isNaN(me._lon) && mc) {
        me._inset = mc.findInset(me._lat, me._lon);
        if(me._inset) {
            //IDB.log(me, "HAS INSET");

            bs = me._inset.boundarySet;

            me._xPct = bs.getXPct(me._lon);
            me._xFunction = "getXFromInset";

            me._yPct = bs.getYPct(me._lat);
            me._yFunction = "getYFromInset";

            me._viewable = true;
        }
    }


    if(!me._viewable) {
        me._viewable = ((!isNaN(me._xPct) || !isNaN(me._xPixels)) && (!isNaN(me._yPct) || !isNaN(me._yPixels)));
        if(me._viewable) {    // coordinates came from ImageMapPointConfig.
            if(!isNaN(me._xPixels)) {
                me._xFunction = "getXFromPixels";
            }
            else {
                me._xFunction = "getXFromPct";
            }

            if(!isNaN(me._yPixels)) {
                me._yFunction = "getYFromPixels";
            }
            else {
                me._yFunction = "getYFromPct";
            }
        }
    }

    if(useRangeColors && datapoint.yAxis.dataTypeIsNumber) {
        me._baseColor = datapoint.rangeColor;
    }

    // Fill in defaults for any attributes that haven't been provided
    // from the config file or the data
    if(!me._shape) {
        me._setShapeFromString(gp("PointShape"));
    }
    if(!me._baseColor) {
        me._baseColor = gp("PointColor");
    }
    if(!me._size) {
        me._size = Math.max(Math.min(100, gp("PointSize")), 5);
    }

    if(me._viewable) {

        graph._dpm.addDatapoint(datapoint);
        stateListener = I.listener(me, "_onVisualState");

        if(useRangeColors && datapoint.yAxis.dataTypeIsNumber) {
            for(var j=0, jj=rangeAxisIds.length; j<jj; j++) {
                rangeDatapoints[rangeAxisIds[j]].onVisualState = stateListener;
            }
        }
        else {
            datapoint.onVisualState = stateListener;
        }

        graph._pointLayer.add(me._group = new Kinetic.Group());


        me._group.on("mouseover", listener(me, "_handleOver"));
        me._group.on("mouseout", listener(me, "_handleOut"));
        me._group.on("click", IDB.leftClickListener(me, "_handleClick"));
        me._touchHandler = new I.TouchHandler(me._group);

        me._createShape();


        me._createLabel();
        me._createLine();


    }
//    else {
//        I.log(me, "<<< NOT VIEWABLE <<<<");
//    }

    if($blinkEnabled) {
        me._blinkEnabled = true;
        me._blinkInterval = $blinkInterval;
        me._blinkFactor = $blinkFactor;
        if(me._viewable) {
            me._blinkOn = false;
            me._blinkListener = I.listener(me, "blink");
            me.blink();
        }
    }
};

IDB.PlotPoint.prototype.setRangeAxis = function(axis) {
    var me = this, axisId = axis.axisId, dp = me._rangeDatapoints[axisId], graph = me._graph;
    if(!me._viewable) {return;}
    me._currDp = dp;
    graph._dpm.addDatapoint(dp);
    me._kineticShape.fill(dp.rangeColor.css);
    if(me._labelTemplate) {
        me._label.text(me.getLabelText(dp));
    }
};

IDB.PlotPoint.prototype.getLabelText = function(dp) {
    var me = this, graph = me._graph, gp = graph.gp, s = dp.map.pointLabel, lt, temp, labelTemplate = me._labelTemplate;
    if(s) {
//        IDB.log("Cached: " + s);
        return s;
    }
    if(labelTemplate) {
//       IDB.log("Template: " + labelTemplate);
       s = me.expandSelectedAxisMacros(labelTemplate, dp);
       dp.map.pointLabel = s;
       return s;
    }
    lt = $.trim(gp("PointLabelTemplate"));
    if(lt) {
        lt = lt.replace(/\$\{\s*br\s*\}/ig, "\n");
        temp = graph._chart.expandMacros(lt, null, dp);
        s = me.expandSelectedAxisMacros(temp, dp);
        if(temp != s) {
            me._labelTemplate = temp;
        }
//        IDB.log("Expanded: " + s);
    }
    else {
        s = dp.xValue.fv;
//        IDB.log("Using X Value: " + s);
    }
    dp.map.pointLabel = s;
    return s;
};

IDB.PlotPoint.prototype.expandSelectedAxisMacros = function(template, dp) {
    var s,
        yValue = dp.yValue,
        yAxis = dp.yAxis;
    if(!template) return template;
    s = template.replace(/\$\{\s*selected\s*:\s*axis\s*\}/ig, yAxis.axisName);
    s = s.replace(/\$\{\s*selected\s*:\s*value\s*\}/ig, yValue.fv);
    return s;
};

IDB.PlotPoint.prototype._setShapeFromString = function(s) {
    var me = this, I = IDB, shape = I.normalizeShape(s), polyAttrs;
    me._shape = shape;
    if(shape === "poly") {
        polyAttrs = I.parsePoly(s);
        if(polyAttrs) {
            me._shape = "poly";
            me._polySides = polyAttrs.sides;
            me._polyAngle = polyAttrs.rotation;
        }
        else {
            me._polySides = 4;
            me._polyAngle = 0;
        }
    }
};

IDB.PlotPoint.prototype._createLine = function() {
    var me = this, graph = me._graph, gp = graph.gp,
       color = gp("PointLabelColor"), line,
       config = {stroke:color, strokeWidth:0.5, lineCap:"round", visible:me._pointLabelAlwaysVisible, points:[0,0,1,1]};

    if(gp("PointLabelShadow")) {
       config.shadowColor = color.x.css;
       config.shadowBlur = 5;
    }

    me._line = line = new Kinetic.Line(config);
    graph._lineLayer.add(line);
};

IDB.PlotPoint.prototype._createShape = function() {
    var me = this, I = IDB,
        shape = me._shape,
        color = me._baseColor,
        size = me._size,
        config = {x:0, y:0, shadowColor:"#0F0F0F", shadowEnabled:false, shadowBlur:(me._haloHighlight ? me._shadowBlur : 0)},
        kineticShape;

    if(shape === "poly") {
        config = $.extend(config, {sides:me._polySides, rotation:me._polyAngle});
    }

    me._kineticShape = kineticShape = I.kShape(shape, color.css, size, config);
    me._group.add(kineticShape);
};

IDB.PlotPoint.prototype.blink = function() {
    var me = this, I = IDB, VS = I.VS, dp = me._datapoint,  visualState = dp.getVisualState(), blinkOn = (me._blinkOn && (visualState !== VS.HIGHLIGHTED)),
        interval = blinkOn ? me._blinkOffInterval : me._blinkInterval,
        ks = me._kineticShape, changing = (me._blinkOn === blinkOn), opaque = (visualState === VS.FADED ? 0.3 : 1.0);
//    IDB.log("BLINK " + blinkOn + ", " + me._blinkEnabled + ", changing=" + changing + ", destroyed=" + me._destroyed);
    if(me._destroyed || !me._blinkEnabled || !ks) {
        return;
    }
    if(changing) {
        me._blinkOn = !blinkOn;
        ks.setOpacity(me._blinkOn ? opaque : 0);
        me._graph._pointLayer.batchDraw();
    }
    window.setTimeout(me._blinkListener, interval);
};

IDB.PlotPoint.prototype._createLabel = function() {
    var me = this, graph = me._graph, gp = graph.gp, label, config = {}, n,
       dp = me._datapoint, color = gp("PointLabelColor");

    config.visible = me._pointLabelAlwaysVisible;

    n = gp("PointLabelSize");
    if(isNaN(n)) n = 12;
    config.fontSize = Math.min(100, Math.max(5, n));
    config.fill = color.css;
    config.align = gp("PointLabelAlign");
    
    if(gp("PointLabelShadow")) {
        config.shadowColor = color.x.css;
        config.shadowBlur = 5;
    }

    config.text = me.getLabelText(dp);

    me._label = label = new Kinetic.Text(config);
    me._group.add(label);
};

IDB.PlotPoint.prototype.destroy = function() {
    var me = this;
    me._destroyed = true;

};

IDB.PlotPoint.prototype._onVisualState = function(newState) {
    var me = this, I = IDB, VS = I.VS, alpha = 1.0, shape = me._kineticShape, suppressLabel = me._suppressLabel,
        shadow = false, shadowBlur = 0, haloEnabled = me._haloHighlight, lVis = me._pointLabelAlwaysVisible && !suppressLabel;

    if(newState === VS.FADED) {
        alpha = 0.3;
    }
    else if(newState === VS.HIGHLIGHTED) {
        shadow = true;
        shadowBlur = me._shadowBlur;
        lVis = true && !suppressLabel;
    }

    shape.opacity(alpha);
    me._label.visible(lVis);
    me._line.visible(lVis);

    if(haloEnabled) {
        shape.shadowEnabled(shadow).shadowBlur(shadowBlur);
    }
};


IDB.PlotPoint.prototype.maybeShowLabel = function() {
    var me = this, label = me._label, line = me._line;
    if(me._suppressLabel || me._pointLabelAlwaysVisible || !label) {
        return;
    }
    label.visible(true);
    if(line) {
        line.visible(true);
    }
    me._graph.refreshDisplay();

};

IDB.PlotPoint.prototype.maybeHideLabel = function() {
    var me = this, label = me._label, line = me._line;
    if(me._suppressLabel || me._pointLabelAlwaysVisible || !label) {
        return;
    }
    label.visible(false);
    if(line) {
        line.visible(false);
    }

    me._graph.refreshDisplay();
};

IDB.PlotPoint.prototype.setVisible = function(b) {
    var me = this;
    me._visible = !!b;
};

IDB.PlotPoint.prototype.getXFromPixels = function(fullMapWidth, currentMapWidth) {
    return (currentMapWidth / fullMapWidth) * this._xPixels;
};

/* jshint unused:true */
IDB.PlotPoint.prototype.getXFromPct = function(fullMapWidth, currentMapWidth) {
/* jshint unused:false */
    return this._xPct * currentMapWidth / 100;
};


IDB.PlotPoint.prototype.getPointX = function(fullMapWidth, currentMapWidth) {
    var me = this;
    if(!me._viewable) return NaN;
    return me[me._xFunction](fullMapWidth, currentMapWidth);
};


IDB.PlotPoint.prototype.getYFromPixels = function(fullMapHeight, currentMapHeight) {
    return (currentMapHeight / fullMapHeight) * this._yPixels;
};

/* jshint unused:true */
IDB.PlotPoint.prototype.getYFromPct = function(fullMapHeight, currentMapHeight) {
/* jshint unused:false */
    return this._yPct * currentMapHeight / 100;
};

/* jshint unused:true */
IDB.PlotPoint.prototype.getXFromInset = function(fullMapHeight, currentMapHeight) {
/* jshint unused:false */
    var me = this, inset = me._inset, xPct = me._xPct;
    return inset._insetX + (xPct * inset._insetW / 100);
};

/* jshint unused:true */
IDB.PlotPoint.prototype.getYFromInset = function(fullMapHeight, currentMapHeight) {
/* jshint unused:false */
    var me = this, inset = me._inset, yPct = me._yPct;
    return inset._insetY + (yPct * inset._insetH / 100);
};

IDB.PlotPoint.prototype.getPointY = function(fullMapHeight, currentMapHeight) {
    var me = this;
    if(!me._viewable) return NaN;
    return me[me._yFunction](fullMapHeight, currentMapHeight);
};

IDB.PlotPoint.prototype.moveTo = function(x, y, scalePoint, scaleLabel, mapScale) {
    var me = this, group = me._group, ks = me._kineticShape, label = me._label, line = me._line,
        ho = me._pointLabelHorizontalOffset,
        vo = me._pointLabelVerticalOffset,
        lw, lh, labelX, labelY, uLabelX, uLabelY, lineEnd = me._lineEnd,
        halfLw, halfLh, vsign, hsign, lineSlope, labelSlope, lineSteeper, E, F;

    if(!ks) {return;}

    group.x(x).y(y).visible(true);

    lw = label.width();
    lh = label.height();
    labelX = uLabelX = (-(lw/2) + ho);
    labelY = uLabelY = (-(lh/2) + vo);
    
    if(scalePoint) {
       ks.scaleX(mapScale).scaleY(mapScale);
    }

    if(scaleLabel) {
        label.scaleX(mapScale).scaleY(mapScale);
        line.scaleX(mapScale).scaleY(mapScale);
        labelX = mapScale * labelX;
        labelY = mapScale * labelY;
    }
    label.x(labelX).y(labelY);
    line.x(x).y(y);

    if(me._noLine) {
       line.visible(false);
//       IDB.log("no line");
       return;
    }

    if(lineEnd) {
//        IDB.log(lineEnd, "have end");
        line.points([0, 0, lineEnd.x, lineEnd.y]);
        return;
    }

    halfLw = lw / 2;
    halfLh = lh / 2;
    if(halfLw >= Math.abs(ho) && halfLh >= Math.abs(vo)) {
        // label covers center of point, no line.
        line.visible(false);
        me._noLine = true;
        return;
    }


    vsign = (vo < 0) ? -1 : 1;
    hsign = (ho < 0) ? -1 : 1;


    if(!ho) {
        lineEnd = {x:ho, y:(vo - halfLh*vsign)};
    }
    else if(!vo) {
        lineEnd = {x:(ho - halfLw*hsign), y:0};
    }
    else {

        lineSlope = vo / ho;
        labelSlope = lh / lw;
    
        lineSteeper = Math.abs(lineSlope) > labelSlope;
    
        E = {};
        F = {};
    
        if(lineSteeper) { // intersects top or bottom side
            if(vo > 0) { // intersects top
                E.x = uLabelX;
                E.y = uLabelY;
                F.x = uLabelX + lw;
                F.y = uLabelY;
            }
            else {  // bottom
                E.x = uLabelX;
                E.y = uLabelY + lh;
                F.x = uLabelX + lw;
                F.y = uLabelY + lh;
            }
        }
        else { // intersects left or right side
            if(ho > 0) { // left
                E.x = uLabelX;
                E.y = uLabelY;
                F.x = uLabelX;
                F.y = uLabelY + lh;
            }
            else { // right
                E.x = uLabelX + lw;
                E.y = uLabelY;
                F.x = uLabelX + lw;
                F.y = uLabelY + lh;
            }
        }
    
        lineEnd = IDB.PlotPoint.findEndPoint({x:0, y:0},  {x:ho, y:vo}, E, F);
    }
    me._lineEnd = lineEnd;

    line.points([0, 0, lineEnd.x, lineEnd.y]);

//    IDB.log(lineEnd, "WIDTH " + label.width() + ", mapScale=" + mapScale + ", labelX=" + labelX + ", labelY=" + labelY + ", x=" + x + ", y=" + y);
};

IDB.PlotPoint.findEndPoint = function(A,B,E,F) {
    var ip = {}, a1, a2, b1, b2, c1, c2, denom;

    a1= B.y-A.y;
    b1= A.x-B.x;
    c1= B.x*A.y - A.x*B.y;
    a2= F.y-E.y;
    b2= E.x-F.x;
    c2= F.x*E.y - E.x*F.y;

    denom=a1*b2 - a2*b1;
    if (denom === 0) {
//        IDB.log("PlotPoint.findEndPoint(): a1=" + a1 + ", b1=" + b1 + ", a2=" + a2 + ", b2=" + b2);
        return B;
    }
    ip.x=(b1*c2 - b2*c1)/denom;
    ip.y=(a2*c1 - a1*c2)/denom;
    return ip;
};


//IDB.PlotPoint.prototype.setScale = function(scale) {
//    var me = this, ks = me._kineticShape;
//    if(!ks) {
//        return;
//    }
//    ks.scaleX(scale).scaleY(scale);
//};


////////////////////////////////////////////////////////////////////////////
// Event handlers
////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
/* jshint unused:true */
IDB.PlotPoint.prototype._handleClick = function(evt) {
/* jshint unused:false */
    this._graph._graphContext.onDatapointClicked(this._currDp);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PlotPoint.prototype._handleOver = function() {
    this._graph._graphContext.onDatapointOver(this._currDp);
    this.maybeShowLabel();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PlotPoint.prototype._handleOut = function() {
    this._graph._graphContext.onDatapointOut(this._currDp);
    this.maybeHideLabel();
};

/*global IDB, $ */
IDB.PlotPointConfig = function($xml, valueMap) {
    var me = this, $values, I = IDB,
       get = function(name) {
           return $xml.idbFirstChildText(name);
       },
       num = function(name, defaultVal) {
           if(defaultVal === undefined) {
               defaultVal = NaN;
           }
           return $xml.children(name).idbTextToNumber(defaultVal);
       },
       s, pol;
    $values = $xml.children("value");
    for(var j=0, jj=$values.length; j<jj; j++) {
        s = $.trim($($values[j]).text());
        if(!s) continue;
        valueMap[s.toLowerCase()] = me;
    }

    me.size = num("size", null);

    me.xPixels = num("x");
    me.yPixels = num("y");

    me.xPct = num("xpct");
    me.yPct = num("ypct");

    me.lat = num("lat");
    me.lon = num("lon");

    me.blinkInterval = num("blink", 0);
    me.blinkFactor = num("blinkfactor", 1.0);

    me.shape = null;
    me.polySides = 0;
    me.polyAngle = 0;
    me.baseColor = null;

    s = get("shape").toLowerCase();
    if(s) {
        pol = I.parsePoly(s);
        if(pol) {
            me.shape = "poly";
            me.polyAngle = pol.rotation;
            me.polySides = pol.sides;
        }
        else {
            me.shape = I.normalizeShape(s);
            if(me.shape === "poly") {
                me.polySides = Math.min(8, Math.max(3,  num("sides", 4)));
                me.polyAngle = I.normalizeAngle(num("angle", 0));
            }
        }
    }

    s = get("color");
    if(s) {
        me.baseColor = I.rgb(s, I.rgb.BLACK);
    }

//    IDB.log(me, "PlotPointConfig");
};



/*====================================================================================================*/

