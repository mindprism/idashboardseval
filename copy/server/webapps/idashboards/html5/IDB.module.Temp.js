'use strict';
IDB.TempGraph = function(chart, $div, graphProperties, dataSet, dpGridSort) {
	dpGridSort = IDB.ifUndef(dpGridSort, null);
   
	var dpGrid = new IDB.DatapointGrid(this, dataSet, dpGridSort);
	
	var context = new IDB.GraphContext(
		graphProperties,
		dpGrid
//		IDB.listener(this, "datapointClicked"),
//		IDB.listener(this, "datapointMouseOver"),
//		IDB.listener(this, "datapointMouseOut")
	);

	this._$div = $div;
	this._chart = chart;
	this._graphContext = context;          
    this._dataSet = dataSet;
	this._info = new IDB.GraphInfo();
	this._dpm = new IDB.DatapointManager(this, true);

	var datapoints = dpGrid.getAllDatapoints();

	for ( var i in datapoints ) {
		this._dpm.addDatapoint(datapoints[i]);
	}

	//console.log("vvv DatapointGrid created vvv");
	//console.log(dpGrid);

    this.onDatapointMouseOverListener = IDB.listener(this, "onDatapointMouseOver");
    this.onDatapointMouseOutListener = IDB.listener(this, "onDatapointMouseOut");
    this.onDatapointClickedListener = IDB.listener(this, "onDatapointClicked");

	this._build();

	this.resize();


};

IDB.TempGraph.prototype._build = function() {

    var evts = {mouseover:"onDatapointMouseOver", mouseout:"onDatapointMouseOut", "click":"onDatapointClicked"}, evtName;

    this._$div.empty();
    this._$div.css({"overflow":"auto"});
    var axes = this._dataSet.getGraphAxes(false);
    // If there are ranges present, we'll use them to color the numeric cells. Otherwise
    // we'll use the axis colors.
    var hasRanges = (axes[0].getRangeList().length > 0);
    var numAxes = axes.length;
    var dpRows = this._graphContext.datapointGrid.getDatapointRows();

    var css1 = {"padding":"2px 5px", "border":"1px solid", "cursor":"default"};

    var $table = this._$table = $("<table>").css({"border-collapse":"collapse", "overflow":"scroll"}).appendTo(this._$div);
    $table.height(200);
    var $tbody = $("<tbody>").css({"overflow":"scroll"}).appendTo(this._$table);
    var rgb, rgbx;
    for(var rowIndex=0; rowIndex<dpRows.length; rowIndex++) {
        var dpRow = dpRows[rowIndex];
        var $row = $("<tr>").appendTo($tbody);
        var $xtd = $("<td>").appendTo($row).text(dpRow.xValue.fv).css(css1).css({"background-color":"#FAFAFA"});
        for(var j=1, jj=axes.length; j<jj; j++) {
            var axis = axes[j];
            if(rowIndex === 0) {
                // calculate the visual state colors and cache them on the Y axis.
                rgb = axis.axisColor;
                rgbx = rgb.x;
                axis.stateColors = [
                    [rgb.css, rgbx.css],
                    [IDB.toRGBA(rgb, 0.5), rgbx.css],
                    [rgbx.css, rgb.css]];
            }
            
            var bgColor = axis.axisColor.css;
            var textColor = axis.axisColor.x.css;

            var dp = dpRow.getDatapoint(axis.axisId);
            if(hasRanges && dp.yAxis.dataTypeIsNumber) {
               rgb = dp.rangeColor;
               rgbx = rgb.x;
               bgColor = rgb.css;
               textColor = rgb.x.css;
               dp.stateColors = [
                    [rgb.css, rgbx.css],
                    [IDB.toRGBA(rgb, 0.5), rgbx.css],
                    [rgbx.css, rgb.css]];

            }
            else {
                dp.stateColors = axis.stateColors;
            }
            dp.onVisualState = IDB.TempGraph.DatapointExtensions.onVisualState;
            if(j===1) {
                $xtd.data("DP", dp);
                for(evtName in evts) {
                   $xtd.on(evtName, this[evts[evtName] + "Listener"]);
                }
            }
            var $td = $("<td>").appendTo($row).text(dp.yValue.fv).css(css1).css({"background-color":dp.stateColors[IDB.VS.NORMAL][0], 
                                                             "color":dp.stateColors[IDB.VS.NORMAL][1], "border-color":"#000000"});
            $td.data("DP", dp);
            for(evtName in evts) {
               $td.on(evtName, this[evts[evtName] + "Listener"]);
            }
            dp.$td = $td;
        }
    }
};


IDB.TempGraph.DatapointExtensions = {
    onVisualState:function(newState) {
        var stateColors = this.stateColors;
        this.$td.css({"background-color":stateColors[newState][0], "color":stateColors[newState][1]});
    }
};

IDB.TempGraph.prototype.onDatapointMouseOver = function(evt) {
   var $td = $(evt.target);
   var dp = $td.data("DP");
   this._chart.datapointMouseOver(this, dp);
};

IDB.TempGraph.prototype.onDatapointMouseOut = function(evt) {
   var $td = $(evt.target);
   var dp = $td.data("DP");
   this._chart.datapointMouseOut(this, dp);
};

IDB.TempGraph.prototype.onDatapointClicked = function(evt) {
   var me = this,
       $td = $(evt.target), 
       dp = $td.data("DP");
   me._chart.datapointClicked(me, dp);
};




////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.getDataSet = function() {
    return this._dataSet;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.getInfo = function() {
    return this._info;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.resize = function() {
    
//    var w = this.width();
//    this._$table.width(w-20);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.destroy = function() {
    IDB.log("TempGraph.destroy()", "------------------------------------------------------");
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.highlightMatchingDatapoints = function(value, matchType, fadeOthers) {
	var points = this._dpm.highlightMatchingDatapoints(value, matchType, fadeOthers);
	return points;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.highlightDatapoint = function(dp, matchType, fadeOthers) {
    var highlighted = this._dpm.highlightDatapoint(dp,  matchType, fadeOthers);

};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.normalizeAllDatapoints = function() {
	var dpRows = this._graphContext.datapointGrid.getDatapointRows();
	var i, j, datapoints, dp;

	for ( i in dpRows ) {
		datapoints = dpRows[i].datapoints;

		for ( j in datapoints ) {
			dp = datapoints[j];
			dp.setVisualState(IDB.VS.NORMAL);
		}
	}

};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.width = function() {
    return this._$div.width();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.height = function() {
    return this._$div.height();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.datapointHighlighted = function(dp, previousState, matchType) {
   var newState = IDB.VS.HIGHLIGHTED;
   var stateColors = dp.stateColors;
   dp.$td.css({"background-color":stateColors[newState][0], "color":stateColors[newState][1]});
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.datapointFaded = function(dp, previousState, matchType) {
   var newState = IDB.VS.FADED;
   var stateColors = dp.stateColors;
   dp.$td.css({"background-color":stateColors[newState][0], "color":stateColors[newState][1]});

};
	
/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.datapointUnhighlighted = function(dp) {
   var newState = IDB.VS.NORMAL;
   var stateColors = dp.stateColors;
   dp.$td.css({"background-color":stateColors[newState][0], "color":stateColors[newState][1]});

};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.datapointUnfaded = function(dp) {
   var newState = IDB.VS.NORMAL;
   var stateColors = dp.stateColors;
   dp.$td.css({"background-color":stateColors[newState][0], "color":stateColors[newState][1]});

};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.datapointUpdatesCompleted = function() {
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.datapointMouseOver = function(dp) {
	this._chart.datapointMouseOver(this, dp);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.datapointMouseOut = function(dp) {
	this._chart.datapointMouseOut(this, dp);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.datapointClicked = function(dp) {
	this._chart.datapointClicked(this, dp);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.getAbsMinW = function() {
    return 20;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TempGraph.prototype.getAbsMinH = function() {
    return 20;
};


/*====================================================================================================*/

