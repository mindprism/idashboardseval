/* global IDB, Kinetic */
/**
 * Kinetic Bubble module.
 * @module Bubble
 * @see IDB.BubbleCircle
 * @see IDB.BubbleCircleView
 * @see IDB.BubbleGraph
 * @see IDB.BubbleGrid
 * @see IDB.BubbleGridView
 * @see IDB.BubblePlot
 * @see IDB.BubblePlotView
 */
'use strict';
// Z_DOT ctor BubbleCircle(graphContext, datapoint, relX, relY, relZ):BubbleCircle  ::IDB.module.Bubble.js  z.11566490006683301.2019.04.10.02.37.46.511|class
  /**
   * BubbleCircle constructor
   * @class BubbleCircle
   * @constructor
   * @memberof IDB
   *
   * @param graphContext
   * @param datapoint
   * @param relX
   * @param relY
   * @param relZ
   *
   * @see z.11566490006683301
   * @see IDB.BubbleCircleView
   * @see module:Bubble
   */
  IDB.BubbleCircle = function(graphContext, datapoint, relX, relY, relZ) {
    this._graphContext = graphContext;
    this._datapoint = datapoint;
    this._relX = relX;
    this._relY = relY;
    this._relZ = relZ;
    }; //-IDB.BubbleCircle
  // Z_DOT cmd setOnDatapointsUpdated(callback):  ::IDB.module.Bubble.js  z.85952201006683301.2019.04.10.02.50.25.958|+!
    /**
     * On Datapoints Updated
     * @memberof IDB.BubbleCircle
     * @method
     *
     * @param {Function} callback
     *
     * @see z.85952201006683301
     */
    IDB.BubbleCircle.prototype.setOnDatapointsUpdated = function(callback) {
      this._graphContext.addDatapointsUpdatedListener(callback);
      }; //-IDB.BubbleCircle.prototype.setOnDatapointsUpdated
  // Z_DOT ppt getDatapoint():Point               ::IDB.module.Bubble.js  z.72198201006683301.2019.04.10.02.51.29.127|+@.
    /**
     * Datapoint
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Point}
     *
     * @see z.72198201006683301
     */
    IDB.BubbleCircle.prototype.getDatapoint = function() {
      return this._datapoint;
      }; //-IDB.BubbleCircle.prototype.getDatapoint
  // Z_DOT ppt getRelativeX():Number              ::IDB.module.Bubble.js  z.76165301006683301.2019.04.10.02.52.36.167|+@.
    /**
     * Relative X
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Number}
     *
     * @see z.76165301006683301
     */
    IDB.BubbleCircle.prototype.getRelativeX = function() {
      return this._relX;
      }; //-IDB.BubbleCircle.prototype.getRelativeX
  // Z_DOT ppt getRelativeY():Number              ::IDB.module.Bubble.js  z.45265401006683301.2019.04.10.02.54.16.254|+@.
    /**
     * Relative Y
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Number}
     *
     * @see z.45265401006683301
     */
    IDB.BubbleCircle.prototype.getRelativeY = function() {
      return this._relY;
      }; //-IDB.BubbleCircle.prototype.getRelativeY
  // Z_DOT ppt getSize():Number                   ::IDB.module.Bubble.js  z.81374501006683301.2019.04.10.02.55.47.318|+@.
    /**
     * Size
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Number}
     *
     * @see z.81374501006683301
     */
    IDB.BubbleCircle.prototype.getSize = function() {
      if ( this._relZ == null ) {
        return 0;
      }
      //
      var sett = this._graphContext.settings;
      var min = sett.get('BubbleMinSize');
      var max = sett.get('BubbleMaxSize');
      return this._relZ*(max-min)+min;
      }; //-IDB.BubbleCircle.prototype.getSize
  // Z_DOT ppt getColor():Number                  ::IDB.module.Bubble.js  z.55421601006683301.2019.04.10.02.56.52.455|+@.
    /**
     * Color
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Number}
     *
     * @see z.55421601006683301
     */
    IDB.BubbleCircle.prototype.getColor = function() {
      return this._datapoint.axisColor;
      }; //-IDB.BubbleCircle.prototype.getColor
  // Z_DOT ppt getLabelText():String              ::IDB.module.Bubble.js  z.59608601006683301.2019.04.10.02.58.00.695|+@.
    /**
     * Label Text
     * @memberof IDB.BubbleCircle
     * @method
     * @return {String}
     *
     * @see z.59608601006683301
     */
    IDB.BubbleCircle.prototype.getLabelText = function() {
      return this._datapoint.xValue.formattedValue;
      }; //-IDB.BubbleCircle.prototype.getLabelText
  // Z_DOT ppt showShadow():Boolean               ::IDB.module.Bubble.js  z.26057701006683301.2019.04.10.02.59.35.062|+@.
    /**
     * Show Shadow
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Boolean}
     *
     * @see z.26057701006683301
     */
    IDB.BubbleCircle.prototype.showShadow = function() {
      return this._graphContext.settings.get('BubbleShadowShow');
      }; //-IDB.BubbleCircle.prototype.showShadow
  // Z_DOT ppt showShading():Boolean              ::IDB.module.Bubble.js  z.53594801006683301.2019.04.10.03.00.49.535|+@.
    /**
     * Show Shading
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Boolean}
     *
     * @see z.53594801006683301
     */
    IDB.BubbleCircle.prototype.showShading = function() {
      return this._graphContext.settings.get('BubbleShadingShow');
      }; //-IDB.BubbleCircle.prototype.showShading
  // Z_DOT cmd createClickListener():Object       ::IDB.module.Bubble.js  z.93461901006683301.2019.04.10.03.01.56.439|+!
    /**
     * Create Click Listener
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Object}
     *
     * @see z.93461901006683301
     */
    IDB.BubbleCircle.prototype.createClickListener = function() {
      return IDB.leftClickListener(this, '_handleClick');
      }; //-IDB.BubbleCircle.prototype.createClickListener
  // Z_DOT cmd createOverListener():Object        ::IDB.module.Bubble.js  z.59443211006683301.2019.04.10.03.07.14.495|+!
    /**
     * Create Over Listener
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Object}
     *
     * @see z.59443211006683301
     */
    IDB.BubbleCircle.prototype.createOverListener = function() {
      return IDB.listener(this, '_handleOver');
      }; //-IDB.BubbleCircle.prototype.createOverListener
  // Z_DOT cmd createOutListener():Object         ::IDB.module.Bubble.js  z.19100311006683301.2019.04.10.03.08.20.191|+!
    /**
     * Create Out Listener
     * @memberof IDB.BubbleCircle
     * @method
     * @return {Object}
     *
     * @see z.19100311006683301
     */
    IDB.BubbleCircle.prototype.createOutListener = function() {
      return IDB.listener(this, '_handleOut');
      }; //-IDB.BubbleCircle.prototype.createOutListener
  // Z_DOT evt _handleClick():void                ::IDB.module.Bubble.js  z.51675311006683301.2019.04.10.03.09.17.615|-\
    /**
     * Handle Click
     * @memberof IDB.BubbleCircle
     * @method
     *
     * @see z.51675311006683301
     */
    IDB.BubbleCircle.prototype._handleClick = function() {
      this._graphContext.onDatapointClicked(this._datapoint);
      }; //-IDB.BubbleCircle.prototype._handleClick
  // Z_DOT evt _handleOver():void                 ::IDB.module.Bubble.js  z.70221411006683301.2019.04.10.03.10.12.207|-\
    /**
     * Handle Over
     * @memberof IDB.BubbleCircle
     * @method
     *
     * @see z.70221411006683301
     */
    IDB.BubbleCircle.prototype._handleOver = function() {
      this._graphContext.onDatapointOver(this._datapoint);
      }; //-IDB.BubbleCircle.prototype._handleOver
  // Z_DOT evt _handleOut():void                  ::IDB.module.Bubble.js  z.62166411006683301.2019.04.10.03.11.06.126|-\
    /**
     * Handle Out
     * @memberof IDB.BubbleCircle
     * @method
     *
     * @see z.62166411006683301
     */
    IDB.BubbleCircle.prototype._handleOut = function() {
      this._graphContext.onDatapointOut(this._datapoint);
      }; //-IDB.BubbleCircle.prototype._handleOut
// Z_DOT ctor BubbleCircleView(model):BubbleCircleView                              ::IDB.module.Bubble.js  z.53199490006683301.2019.04.10.02.38.19.135|class
  /**
   * BubbleCircleView constructor
   * @class BubbleCircleView
   * @constructor
   * @memberof IDB
   *
   * @param model
   *
   * @see z.53199490006683301
   * @see IDB.BubbleCircle
   * @see module:Bubble
   */
  IDB.BubbleCircleView = function(model) {
    this._model = model;
    model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
    this._buildKinetic();
    }; //-IDB.BubbleCircleView
Object.defineProperties(IDB.BubbleCircleView.prototype, {
  // Z_DOT ppt isAnimationEnabled:Boolean ::IDB.module.Bubble.js  z.83397851006683301.2019.04.10.04.24.39.338|+.
    /**
     * Is Animation Enabled
     * @memberof IDB.BubbleCircleView.prototype
     * @type {Boolean}
     *
     * @see z.83397851006683301
     */
    isAnimationEnabled: {
      enumerable: true,
      configurable: false,
      get: function() {
        var me = this;
        if (me._animationsEnabled !== undefined) {return me._animationsEnabled;}
        var model = me._model, context = model && model._graphContext ? model._graphContext : null,
          settings = context && context.settings ? context.settings : null;
        //
        // noinspection AssignmentResultUsedJS
        return (me._animationsEnabled = settings && settings.animationEnabled ? settings.animationEnabled() : false);
      },
      set: function(val) {
        this._animationsEnabled = !!val;
      }
    },
  // Z_DOT ppt isAnimating:Boolean ::IDB.module.Bubble.js  z.78361951006683301.2019.04.10.04.25.16.387|+.
    /**
     * Is Animating
     * @memberof IDB.BubbleCircleView.prototype
     * @type {Boolean}
     *
     *
     * @see z.78361951006683301
     */
    isAnimating: {
      enumerable: true,
      configurable: false,
      get: function() {
        var me = this, animation = me._animation;
        return animation !== null && animation.isRunning();
      },
      set: function(val) {
        var me = this, animation = me._animation;
        if (val === false){
          if (animation !== null){
            if (animation.isRunning && animation.isRunning()){
              animation.stop();
            }
            else{
              me.doneAnimating = true;
            }
          }
          me._animation = null;
        }
      }
    },
  // Z_DOT ppt willAnimate:Boolean ::IDB.module.Bubble.js  z.76416951006683301.2019.04.10.04.26.01.467|+@.
    /**
     * Will Animate
     * @memberof IDB.BubbleCircleView.prototype
     * @type {Boolean}
     *
     * @see z.76416951006683301
     */
    willAnimate: {
      enumerable: true,
      configurable: false,
      get: function() {
        return this._animationTimeout !== null;
      }
    },
  // Z_DOT ppt animationParametersChanged:Boolean z.53278951006683301.2019.04.10.04.26.27.235|+@.
    /**
     * Animation Parameters Changed
     * @memberof IDB.BubbleCircleView.prototype
     * @type {Boolean}
     *
     * @see z.53278951006683301
     */
    animationParametersChanged: {
      enumerable: true,
      configurable: false,
      get: function() {
        var me = this, changed = me._paramsChangedFlag;
        me._paramsChangedFlag = false;
        return !!changed;
      }
    },
  // Z_DOT ppt doneAnimating:Boolean ::IDB.module.Bubble.js  z.66481061006683301.2019.04.10.04.26.58.466|+.
    /**
     * Done Animating
     * @memberof IDB.BubbleCircleView.prototype
     * @type {Boolean}
     *
     * @see z.66481061006683301
     */
    doneAnimating: {
      value: false,
      enumerable: true,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _animation:Object ::IDB.module.Bubble.js  z.53283061006683301.2019.04.10.04.27.18.235|+.
    /**
     * Animation
     * @memberof IDB.BubbleCircleView.prototype
     * @type {(null|Object)}
     *
     * @see z.53283061006683301
     */
    _animation: {
      enumerable: false,
      value: null,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _animationTimeout:Object ::IDB.module.Bubble.js  z.36366061006683301.2019.04.10.04.27.46.363|+.
    /**
     * Animation Timeout
     * @memberof IDB.BubbleCircleView.prototype
     * @type {(null|Object)}
     *
     * @see z.36366061006683301
     */
    _animationTimeout: {
      enumerable: false,
      value: null,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _animationsEnabled:Boolean ::IDB.module.Bubble.js  z.32319061006683301.2019.04.10.04.28.11.323|+.
    /**
     * Animations Enabled
     * @memberof IDB.BubbleCircleView.prototype
     * @type {Boolean}
     *
     * @see z.32319061006683301
     */
    _animationsEnabled: {
      enumerable: false,
      value: undefined,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _paramsChangedFlag:Boolean ::IDB.module.Bubble.js  z.78101161006683301.2019.04.10.04.28.30.187|+.
    /**
     * Params Changed Flag
     * @memberof IDB.BubbleCircleView.prototype
     * @type {Boolean}
     *
     * @see z.78101161006683301
     */
    _paramsChangedFlag: {
      value: false,
      enumerable: false,
      configurable: false,
      writable: true
    },
  // Z_DOT ppt _animationParameters:Object ::IDB.module.Bubble.js  z.64365161006683301.2019.04.10.04.29.16.346|+.
    /**
     * Animation Parameters
     * @memberof IDB.BubbleCircleView.prototype
     * @type {{groupX:Number, width:Number, opac:Number, barHighVis:Boolean, lblVis:Boolean, plotHeight:Number}}
     *
     * @see z.64365161006683301
     */
    _animationParameters: {
      value: null,
      enumerable: false,
      configurable: false,
      writable: true
    }
});
  // Z_DOT cmd _buildKinetic():void ::IDB.module.Bubble.js  z.70652911006683301.2019.04.10.03.18.45.607|-!
    /**
     * Build Kinetic
     * @memberof IDB.BubbleCircleView
     * @method
     *
     * @see z.70652911006683301
     */
    IDB.BubbleCircleView.prototype._buildKinetic = function() {
      var model = this._model;
      var col = model.getColor();
      //
      this._display = new Kinetic.Group(undefined);
      this._display.on('click', model.createClickListener());
      this._display.on('mouseover', model.createOverListener());
      this._display.on('mouseout', model.createOutListener());
      this._touchHandler = new IDB.TouchHandler(this._display);
      //
      // noinspection MagicNumberJS
      this._circle = new Kinetic.Circle({
        fill: col.css,
        radius: model.getSize()/2,
        stroke: IDB.toRGBA(col.x, 0.3),
        strokeWidth: 2,
        strokeEnabled: false,
        strokeScaleEnabled: false,
        shadowEnabled: model.showShadow(),
        shadowOffsetX: 2,
        shadowOffsetY: 2,
        shadowBlur: 0,
        shadowColor: 'rgba(0, 0, 0, 0.2)'
      });
      this._display.add(this._circle);
      //
      if ( model.showShading() ) {
        var scale = model.getSize()/IDB.GenericShading.FullDimension;
        var shade = new IDB.GenericShading(IDB.GenericShading.TypeGloss, false);
        //
        shade.getDisplay()
          .scaleX(scale)
          .scaleY(scale)
          .listening(false);
        //
        this._display.add(shade.getDisplay());
      }
      }; //-IDB.BubbleCircleView.prototype._buildKinetic
  // Z_DOT ppt setLabelRef(labelRef): ::IDB.module.Bubble.js  z.55696911006683301.2019.04.10.03.19.29.655|+.
    /**
     * Label Ref
     * @memberof IDB.BubbleCircleView
     * @method
     *
     * @param labelRef
     *
     * @see z.55696911006683301
     */
    IDB.BubbleCircleView.prototype.setLabelRef = function(labelRef) {
      this._labelRef = labelRef;
      }; //-IDB.BubbleCircleView.prototype.setLabelRef
  // Z_DOT ppt getDisplay():Kinetic.Group ::IDB.module.Bubble.js  z.30733021006683301.2019.04.10.03.20.33.703|+@.
    /**
     * Display
     * @memberof IDB.BubbleCircleView
     * @method
     * @return {Kinetic.Group}
     *
     * @see z.30733021006683301
     */
    IDB.BubbleCircleView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.BubbleCircleView.prototype.getDisplay
  // Z_DOT cmd runInitialAnimation(delay):void ::IDB.module.Bubble.js  z.34768021006683301.2019.04.10.03.21.26.743|+!
    /**
     * Run Initial Animation
     * @memberof IDB.BubbleCircleView
     * @method
     *
     * @param delay
     *
     * @see z.34768021006683301
     */
    IDB.BubbleCircleView.prototype.runInitialAnimation = function(delay) {
      var me = this;
      // noinspection AssignmentToFunctionParameterJS
      delay = typeof delay === 'number' ? delay : 0;
      me._setAnimation(me._makeAnimationObj());
      if (delay === 0) {me._runAnimation();}
      else
      {
        me._animationTimeout = window.setTimeout(function() { me._runAnimation(); }, delay);
      }
      }; //-IDB.BubbleCircleView.prototype.runInitialAnimation
  // Z_DOT ppt _setAnimation(animationObj):void ::IDB.module.Bubble.js  z.07805121006683301.2019.04.10.03.22.30.870|+.
    /**
     * Animation
     * @memberof IDB.BubbleCircleView
     * @method
     *
     * @param animationObj
     *
     * @see z.07805121006683301
     */
    IDB.BubbleCircleView.prototype._setAnimation = function(animationObj) {
      var me = this, anim = me._animation;
      // noinspection OverlyComplexBooleanExpressionJS
      if (me.isAnimating
          && ((Object.hasOwnProperty('is') && !Object.is(anim, animationObj))
          || (!Object.hasOwnProperty('is') && anim!==animationObj))) {
        if (anim.isRunning()) {anim.stop();}
      }
      me._animation = animationObj;
      }; //-IDB.BubbleCircleView.prototype._setAnimation
  // Z_DOT cmd _runAnimation():void ::IDB.module.Bubble.js  z.38591221006683301.2019.04.10.03.23.39.583|-!
    /**
     * Run Animation
     * @memberof IDB.BubbleCircleView
     * @method
     *
     * @see z.38591221006683301
     */
    IDB.BubbleCircleView.prototype._runAnimation = function() {
      var me = this;
      me._display.setOpacity(1);
      me._animation.start();
      }; //-IDB.BubbleCircleView.prototype._runAnimation
  // Z_DOT ppt _updateAnimationParameters(animationParameters):void ::IDB.module.Bubble.js  z.11148221006683301.2019.04.10.03.24.44.111|-.
    /**
     * Update Animation Parameters
     * @memberof IDB.BubbleCircleView
     * @method
     *
     * @param animationParameters
     *
     * @see z.11148221006683301
     */
    IDB.BubbleCircleView.prototype._updateAnimationParameters = function(animationParameters) {
      var me = this;
      if (me.isAnimationEnabled && !me.doneAnimating) {
        var params = me._animationParameters;
        me._animationParameters = animationParameters;
        // noinspection OverlyComplexBooleanExpressionJS
        if (params !== null && ((Object.hasOwnProperty('is') && !Object.is(animationParameters, params)) || (!Object.hasOwnProperty('is') && animationParameters !== params)))
          {me._paramsChangedFlag = true;}
      }
      }; //-IDB.BubbleCircleView.prototype._updateAnimationParameters
  // Z_DOT fn _makeAnimationObj():Kinetic.Animation ::IDB.module.Bubble.js  z.78672321006683301.2019.04.10.03.25.27.687|-?
    /**
     * Make Animation Object
     * @memberof IDB.BubbleCircleView
     * @method
     * @return {Kinetic.Animation}
     *
     * @see z.78672321006683301
     */
    IDB.BubbleCircleView.prototype._makeAnimationObj = function() {
      // noinspection MagicNumberJS
      var me = this, cir = me._circle, lblDisp = me._labelRef ? me._labelRef.getDisplay() : null, disp = me._display,
        dur = 1000, faded = false, ep = Math.PI * 8,
        easing = function bouncyEasing(p) {
          // Sanity check first that p is a pure number, not +/- Infinity, and finally within the range [0...1] (inclusive)
          //
          // This is a locally defined method with a very particular intended use which happens to be in a context where
          // performance counts. Even publicly availble easing functions don't typically have sanity checks, but if a coding
          // style where assumptions aren't made is valued more than performance, the following can be uncommented.
          //
          //var u, n = null;
          //p = ([u,n].indexOf(p) === -1 && !!p.constructor && p.constructor === Number && p-0 === p && 1/+p !== 0) ? +p : 0;
          //p = Math.min(1,Math.max(0, p))
          //
          // This is the simpliest/most efficient form I managed to approximate the flash version,
          // since the easing function needs to produce a number quickly, it's best to avoid logical constructs.
          //
          // It could still be tweaked to change some aspects of the animation (e.g., if it's too bouncy).
          // graphr.org is a useful resource for developing a single equation for this purpose.
          //
          // noinspection MagicNumberJS
          return Math.pow(2,-8*(p-0.022))*Math.sin((p-0.042)*ep)+1;
        },
        retAnim = new Kinetic.Animation(function(frame) {
          var anim = this, view = me;
          if (view.animationParametersChanged) {
            var param = view._animationParameters;
            faded = param === IDB.VS.FADED;
            // noinspection JSUnresolvedFunction
            cir.setShadowOpacity(faded ? 0.0 : 1.0);
            // noinspection MagicNumberJS
            disp.setOpacity(faded ? 0.2 : 1.0);
            // noinspection JSUnresolvedFunction
            cir.setStrokeEnabled(param === IDB.VS.HIGHLIGHTED);
          }
          //
          if (frame.time<=dur)
          {
            var scale = easing(frame.time/dur);
            disp.setScale({x: scale, y: scale});
          }
          else
          {
            anim.stop();
            disp.setScale({x:1.0, y:1.0});
            if (lblDisp) {lblDisp.setVisible(!faded);}
            view.isAnimating = false;
          }
        }, cir.getLayer() || null);
      //
      // Prepare to be animated, possibly after a delay
      disp.setScale({x: 0.015, y: 0.015});
      disp.setOpacity(0);
      if (lblDisp){
        lblDisp.setVisible(false);
      }
      //
      return retAnim;
      }; //-IDB.BubbleCircleView.prototype._makeAnimationObj
  // Z_DOT evt _onDatapointsUpdated():void ::IDB.module.Bubble.js  z.53514421006683301.2019.04.10.03.27.21.535|-\
    /**
     * On Datapoints Updated
     * @memberof IDB.BubbleCircleView
     * @method
     *
     * @see z.53514421006683301
     */
    IDB.BubbleCircleView.prototype._onDatapointsUpdated = function() {
      var model = this._model;
      var state = model.getDatapoint().getVisualState();
      // noinspection EqualityComparisonWithCoercionJS
      var isFaded = (state == IDB.VS.FADED);
      var me = this, changeAnimation = me.isAnimationEnabled && !me.doneAnimating;
      //
      if (changeAnimation){
        me._updateAnimationParameters(state);
      }
      else{
        // noinspection MagicNumberJS
        this._display.opacity(isFaded ? 0.2 : 1.0);
        //
        // noinspection EqualityComparisonWithCoercionJS
        this._circle
          .strokeEnabled(state == IDB.VS.HIGHLIGHTED)
          .shadowOpacity(isFaded ? 0 : 1);
        //
        if ( this._labelRef ) {
          this._labelRef.visible = !isFaded;
        }
      }
      }; //-IDB.BubbleCircleView.prototype._onDatapointsUpdated
// Z_DOT ctor BubbleGraph(chart, $div, graphProperties, dataSet):BubbleGraph        ::IDB.module.Bubble.js  z.30727590006683301.2019.04.10.02.39.32.703|class
  /**
   * BubbleGraph constructor
   * @class BubbleGraph
   * @constructor
   * @memberof IDB
   *
   * @param chart
   * @param $div
   * @param graphProperties
   * @param dataSet
   *
   * @see z.30727590006683301
   * @see module:Bubble
   */
  IDB.BubbleGraph = function(chart, $div, graphProperties, dataSet) {
    IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
    this._info.legendStyle = IDB.LS.X_VALUES;
    }; //-IDB.BubbleGraph
    IDB.BubbleGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
    IDB.BubbleGraph.prototype.constructor = IDB.BubbleGraph;
  // Z_DOT cmd _build():void ::IDB.module.Bubble.js  z.28968421006683301.2019.04.10.03.28.06.982|-!
    /**
     * Build
     * @memberof IDB.BubbleGraph
     * @method
     *
     * @see z.28968421006683301
     */
    IDB.BubbleGraph.prototype._build = function() {
      var gc = this._graphContext;
      var numY = gc.datapointGrid.getNumYCols(true);
      if ( numY > 3 ) {
        gc.showWarning(IDB.GraphLang.get('BubbleAxisWarningTitle'),
          IDB.GraphLang.get('BubbleAxisWarning', [numY]));
      }
      //
      var grid = new IDB.BubbleGrid(gc);
      var model = new IDB.BubblePlot(gc, grid.getXBounds(), grid.getYBounds());
      //
      this._gridView = new IDB.BubbleGridView(grid);
      this._stageLayer.add(this._gridView.getDisplay());
      //
      this._plotView = new IDB.BubblePlotView(model);
      this._stageLayer.add(this._plotView.getDisplay());
      }; //-IDB.BubbleGraph.prototype._build
  // Z_DOT cmd _update(width, height):void ::IDB.module.Bubble.js  z.91992521006683301.2019.04.10.03.28.49.919|-!
    /**
     * Update
     * @memberof IDB.BubbleGraph
     * @method
     *
     * @see z.91992521006683301
     */
    IDB.BubbleGraph.prototype._update = function(width, height) {
      var gv = this._gridView;
      var pv = this._plotView;
      var pvd = pv.getDisplay();
      //
      gv.setSize(width, height);
      //
      pvd.x(gv.getPlotPosX());
      pvd.y(gv.getPlotPosY());
      pv.setSize(gv.getPlotWidth(), gv.getPlotHeight());
      //
      pvd.clip({
        x: 0,
        y: -3,
        width: gv.getPlotWidth(),
        height: gv.getPlotHeight()+6
      });
      }; //-IDB.BubbleGraph.prototype._update
// Z_DOT ctor BubbleGrid(graphContext):BubbleGrid                                   ::IDB.module.Bubble.js  z.59460690006683301.2019.04.10.02.40.06.495|class
  /**
   * BubbleGrid constructor
   * @class BubbleGrid
   * @constructor
   * @memberof IDB
   *
   * @param graphContext
   *
   * @see z.59460690006683301
   * @see IDB.BubbleGridView
   * @see module:Bubble
   */
  IDB.BubbleGrid = function(graphContext) {
    this._graphContext = graphContext;
    this._xDivs = this._buildXDivs();
    this._yDivs = this._buildYDivs();
    this._xText = this._buildXTexts();
    this._yText = this._buildYTexts();
    }; //-IDB.BubbleGrid
  // Z_DOT cmd _buildXDivs():Object ::IDB.module.Bubble.js  z.81139521006683301.2019.04.10.03.29.53.118|-!
    /**
     * Build X Divs
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Object}
     *
     * @see z.81139521006683301
     */
    IDB.BubbleGrid.prototype._buildXDivs = function() {
      var sett = this._graphContext.settings;
      var dpGrid = this._graphContext.datapointGrid;
      var axis = dpGrid.getYAxis(0, true);
      var ss = dpGrid.getStatSet(axis.axisId);
      var zoom = sett.getBool('GridXZoom');
      var min = (zoom ? sett.get('GridXZoomMin') : ss.min);
      var max = (zoom ? sett.get('GridXZoomMax') : ss.max);
      //
      // noinspection EqualityComparisonWithCoercionJS
      if ( min == max ) {
        if ( min > 0 ) {
          min = 0;
        }
        else if ( max < 0 ) {
          max = 0;
        }
        else {
          max = 10;
        }
      }
      //
      return IDB.Calculator.getDivisionMetrics(min, max, sett.get('GridXNumDivs')).largeDivisions;
      }; //-IDB.BubbleGrid.prototype._buildXDivs
  // Z_DOT cmd _buildYDivs():Object ::IDB.module.Bubble.js  z.74063621006683301.2019.04.10.03.30.36.047|-!
    /**
     * Build Y Divs
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Object}
     *
     * @see z.74063621006683301
     */
    IDB.BubbleGrid.prototype._buildYDivs = function() {
      var sett = this._graphContext.settings;
      var dpGrid = this._graphContext.datapointGrid;
      var axis = dpGrid.getYAxis(1, true);
      var ss = dpGrid.getStatSet(axis.axisId);
      var zoom = sett.getBool('GridYZoom');
      var min = (zoom ? sett.get('GridYZoomMin') : ss.min);
      var max = (zoom ? sett.get('GridYZoomMax') : ss.max);
      //
      // noinspection EqualityComparisonWithCoercionJS
      if ( min == max ) {
        if ( min > 0 ) {
          min = 0;
        }
        else if ( max < 0 ) {
          max = 0;
        }
        else {
          max = 10;
        }
      }
      //
      return IDB.Calculator.getDivisionMetrics(min, max, sett.get('GridYNumDivs')).largeDivisions;
      }; //-IDB.BubbleGrid.prototype._buildYDivs
  // Z_DOT cmd _buildXTexts():[String] ::IDB.module.Bubble.js  z.49831721006683301.2019.04.10.03.31.53.894|-!
    /**
     * Build X Texts
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Array<String>}
     *
     * @see z.49831721006683301
     */
    IDB.BubbleGrid.prototype._buildXTexts = function() {
      if ( !this.getXLabelOptions().show ) {
        return null;
      }
      //
      var texts = [];
      var sett = this._graphContext.settings;
      var len = this._xDivs.boundaries.length;
      var numFmt = this._graphContext.datapointGrid.getYAxis(0, true).numFmt;
      //
      for ( var i = 0 ; i < len ; ++i ) {
        texts[i] = sett.formatNumber(this._xDivs.boundaries[i], numFmt);
      }
      //
      return texts;
      }; //-IDB.BubbleGrid.prototype._buildXTexts
  // Z_DOT cmd _buildYTexts():[String] ::IDB.module.Bubble.js  z.91349721006683301.2019.04.10.03.33.14.319|-!
    /**
     * Build Y Texts
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Array<String>}
     *
     * @see z.91349721006683301
     */
    IDB.BubbleGrid.prototype._buildYTexts = function() {
      if ( !this.getYLabelOptions().show ) {
        return null;
      }
      //
      var texts = [];
      var sett = this._graphContext.settings;
      var len = this._yDivs.boundaries.length;
      var numFmt = this._graphContext.datapointGrid.getYAxis(1, true).numFmt;
      //
      for ( var i = 0 ; i < len ; ++i ) {
        texts[i] = sett.formatNumber(this._yDivs.boundaries[i], numFmt);
      }
      //
      return texts;
      }; //-IDB.BubbleGrid.prototype._buildYTexts
  // Z_DOT ppt getXBounds():Bounds ::IDB.module.Bubble.js  z.76944821006683301.2019.04.10.03.34.04.967|+@.
    /**
     * X Bounds
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Bounds}
     *
     * @see z.76944821006683301
     */
    IDB.BubbleGrid.prototype.getXBounds = function() {
      return {
        min: this._xDivs.getMinBoundary(),
        max: this._xDivs.getMaxBoundary()
      };
      }; //-IDB.BubbleGrid.prototype.getXBounds
  // Z_DOT ppt getYBounds():Bounds ::IDB.module.Bubble.js  z.28322921006683301.2019.04.10.03.35.22.382|+@.
    /**
     * Y Bounds
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Bounds}
     *
     * @see z.28322921006683301
     */
    IDB.BubbleGrid.prototype.getYBounds = function() {
      return {
        min: this._yDivs.getMinBoundary(),
        max: this._yDivs.getMaxBoundary()
      };
      }; //-IDB.BubbleGrid.prototype.getYBounds
  // Z_DOT ppt getXDivisionCount():Number ::IDB.module.Bubble.js  z.55488921006683301.2019.04.10.03.36.28.455|+@.
    /**
     * X Division Count
     * @memberof IDB.BubbleGrid
     * @method
     *
     * @return {Number}
     *
     * @see z.55488921006683301
     */
    IDB.BubbleGrid.prototype.getXDivisionCount = function() {
      return this._xDivs.boundaries.length;
      }; //-IDB.BubbleGrid.prototype.getXDivisionCount
  // Z_DOT ppt getXDivisionValue(index):Number ::IDB.module.Bubble.js  z.32050331006683301.2019.04.10.03.41.45.023|+@.
    /**
     * X Division Value
     * @memberof IDB.BubbleGrid
     * @method
     *
     * @param {Number} index
     *
     * @return {Number}
     *
     * @see z.32050331006683301
     */
    IDB.BubbleGrid.prototype.getXDivisionValue = function(index) {
      return this._xDivs.boundaries[index];
      }; //-IDB.BubbleGrid.prototype.getXDivisionValue
  // Z_DOT ppt getYDivisionCount():Number ::IDB.module.Bubble.js  z.41895331006683301.2019.04.10.03.42.39.814|+@.
    /**
     * Y Division Count
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Number}
     *
     *
     * @see z.41895331006683301
     */
    IDB.BubbleGrid.prototype.getYDivisionCount = function() {
      return this._yDivs.boundaries.length;
      }; //-IDB.BubbleGrid.prototype.getYDivisionCount
  // Z_DOT ppt getYDivisionValue(index): ::IDB.module.Bubble.js  z.51050431006683301.2019.04.10.03.43.25.015|+@.
    /**
     * Y Division Value
     * @memberof IDB.BubbleGrid
     * @method
     *
     * @param {Number} index
     *
     * @return {Number}
     *
     * @see z.51050431006683301
     */
    IDB.BubbleGrid.prototype.getYDivisionValue = function(index) {
      return this._yDivs.boundaries[index];
      }; //-IDB.BubbleGrid.prototype.getYDivisionValue
  // Z_DOT ppt getXText(index):String ::IDB.module.Bubble.js  z.20741531006683301.2019.04.10.03.45.14.702|+@.
    /**
     * X Text
     * @memberof IDB.BubbleGrid
     * @method
     *
     * @param {Number} index
     *
     * @return {String}
     *
     * @see z.20741531006683301
     */
    IDB.BubbleGrid.prototype.getXText = function(index) {
      return this._xText[index];
      }; //-IDB.BubbleGrid.prototype.getXText
  // Z_DOT ppt getYText(index):String ::IDB.module.Bubble.js  z.03220631006683301.2019.04.10.03.46.42.230|+@.
    /**
     * Y Text
     * @memberof IDB.BubbleGrid
     * @method
     *
     * @param {Number} index
     *
     * @return {String}
     *
     * @see z.03220631006683301
     */
    IDB.BubbleGrid.prototype.getYText = function(index) {
      return this._yText[index];
      }; //-IDB.BubbleGrid.prototype.getYText
  // Z_DOT ppt getXTitleText():String ::IDB.module.Bubble.js  z.81785631006683301.2019.04.10.03.47.38.718|+@.
    /**
     * X Title Text
     * @memberof IDB.BubbleGrid
     * @method
     *
     * @return {String}
     *
     * @see z.81785631006683301
     */
    IDB.BubbleGrid.prototype.getXTitleText = function() {
      return this._graphContext.datapointGrid.getYAxis(0, true).axisName;
      }; //-IDB.BubbleGrid.prototype.getXTitleText
  // Z_DOT ppt getYTitleText():String ::IDB.module.Bubble.js  z.26211731006683301.2019.04.10.03.48.31.262|+@.
    /**
     * Y Title Text
     * @memberof IDB.BubbleGrid
     * @method
     *
     * @return {String}
     *
     * @see z.26211731006683301
     */
    IDB.BubbleGrid.prototype.getYTitleText = function() {
      return this._graphContext.datapointGrid.getYAxis(1, true).axisName;
      }; //-IDB.BubbleGrid.prototype.getYTitleText
  // Z_DOT ppt getXLabelOptions():Object ::IDB.module.Bubble.js  z.36606731006683301.2019.04.10.03.49.20.663|+@.
    /**
     * X Label Options
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Object}
     *
     * @see z.36606731006683301
     */
    IDB.BubbleGrid.prototype.getXLabelOptions = function() {
      return this._graphContext.settings.get('XLabelsOptions');
      }; //-IDB.BubbleGrid.prototype.getXLabelOptions
  // Z_DOT ppt getYLabelOptions():Object ::IDB.module.Bubble.js  z.70069731006683301.2019.04.10.03.49.56.007|+@.
    /**
     * Y Label Options
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Object}
     *
     * @see z.70069731006683301
     */
    IDB.BubbleGrid.prototype.getYLabelOptions = function() {
      return this._graphContext.settings.get('YLabelsOptions');
      }; //-IDB.BubbleGrid.prototype.getYLabelOptions
  // Z_DOT ppt getXTitleLabelOptions():Object ::IDB.module.Bubble.js  z.22898041006683301.2019.04.10.03.54.49.822|+@.
    /**
     * X Title Label Options
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Object}
     *
     * @see z.22898041006683301
     */
    IDB.BubbleGrid.prototype.getXTitleLabelOptions = function() {
      return this._graphContext.settings.get('XAxisTitleOptions');
      }; //-IDB.BubbleGrid.prototype.getXTitleLabelOptions
  // Z_DOT ppt getYTitleLabelOptions():Object ::IDB.module.Bubble.js  z.03403141006683301.2019.04.10.03.55.30.430|+@.
    /**
     * Y Title Label Options
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Object}
     *
     * @see z.03403141006683301
     */
    IDB.BubbleGrid.prototype.getYTitleLabelOptions = function() {
      return this._graphContext.settings.get('YAxisTitleOptions');
      }; //-IDB.BubbleGrid.prototype.getYTitleLabelOptions
  // Z_DOT ppt getGridColor():Number ::IDB.module.Bubble.js  z.46273241006683301.2019.04.10.03.57.17.264|+@.
    /**
     * Grid Color
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Number}
     *
     * @see z.46273241006683301
     */
    IDB.BubbleGrid.prototype.getGridColor = function() {
      return this._graphContext.settings.get('GridColor');
      }; //-IDB.BubbleGrid.prototype.getGridColor
  // Z_DOT ppt showXLines():Boolean ::IDB.module.Bubble.js  z.95759241006683301.2019.04.10.03.58.15.759|+@.
    /**
     * Show X Lines
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Boolean}
     *
     * @see z.95759241006683301
     */
    IDB.BubbleGrid.prototype.showXLines = function() {
      return this._graphContext.settings.getBool('GridXLinesShow');
      }; //-IDB.BubbleGrid.prototype.showXLines
  // Z_DOT ppt showXBars():Boolean ::IDB.module.Bubble.js  z.19516341006683301.2019.04.10.03.59.21.591|+@.
    /**
     * Show X Bars
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Boolean}
     *
     * @see z.19516341006683301
     */
    IDB.BubbleGrid.prototype.showXBars = function() {
      return this._graphContext.settings.getBool('GridXBars');
      }; //-IDB.BubbleGrid.prototype.showXBars
  // Z_DOT ppt getXNumMinorDivs():Number ::IDB.module.Bubble.js  z.81952441006683301.2019.04.10.04.00.25.918|+@.
    /**
     * X Num Minor Divs
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Number}
     *
     * @see z.81952441006683301
     */
    IDB.BubbleGrid.prototype.getXNumMinorDivs = function() {
      return this._graphContext.settings.get('GridXNumMinorDivs');
      }; //-IDB.BubbleGrid.prototype.getXNumMinorDivs
  // Z_DOT ppt showYLines():Boolean ::IDB.module.Bubble.js  z.19757441006683301.2019.04.10.04.01.15.791|+@.
    /**
     * Show Y Lines
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Boolean}
     *
     * @see z.19757441006683301
     */
    IDB.BubbleGrid.prototype.showYLines = function() {
      return this._graphContext.settings.getBool('GridYLinesShow');
      }; //-IDB.BubbleGrid.prototype.showYLines
  // Z_DOT ppt showYBars():Boolean ::IDB.module.Bubble.js  z.78691541006683301.2019.04.10.04.01.59.687|+@.
    /**
     * Show Y Bars
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Boolean}
     *
     * @see z.78691541006683301
     */
    IDB.BubbleGrid.prototype.showYBars = function() {
      return this._graphContext.settings.getBool('GridYBars');
      }; //-IDB.BubbleGrid.prototype.showYBars
  // Z_DOT ppt getYNumMinorDivs():Number ::IDB.module.Bubble.js  z.53985541006683301.2019.04.10.04.02.38.935|+@.
    /**
     * Y Num Minor Divs
     * @memberof IDB.BubbleGrid
     * @method
     * @return {Number}
     *
     * @see z.53985541006683301
     */
    IDB.BubbleGrid.prototype.getYNumMinorDivs = function() {
      return this._graphContext.settings.get('GridYNumMinorDivs');
      }; //-IDB.BubbleGrid.prototype.getYNumMinorDivs
// Z_DOT ctor BubbleGridView(model):BubbleGridView                                  ::IDB.module.Bubble.js  z.70235690006683301.2019.04.10.02.40.53.207|class
  /**
   * BubbleGridView constructor
   * @class BubbleGridView
   * @constructor
   * @memberof IDB
   *
   * @param model
   *
   * @see z.70235690006683301
   * @see IDB.BubbleGrid
   * @see module:Bubble
   */
  IDB.BubbleGridView = function(model) {
    this._model = model;
    this._plotW = 0;
    this._plotH = 0;
    this._xLabelMaxH = 0;
    this._yLabelMaxW = 0;
    this._buildKinetic();
    }; //-IDB.BubbleGridView
  // Z_DOT cmd _buildKinetic():void ::IDB.module.Bubble.js  z.55449541006683301.2019.04.10.04.03.14.455|+@.
    /**
     * Build Kinetic
     * @memberof IDB.BubbleGridView
     * @method
     *
     * @see z.55449541006683301
     */
    IDB.BubbleGridView.prototype._buildKinetic = function() {
      this._display = new Kinetic.Group(undefined);
      this._grid = new Kinetic.Group(undefined);
      this._display.add(this._grid);
      //
      var model = this._model;
      var xLblOpt = model.getXLabelOptions();
      var yLblOpt = model.getYLabelOptions();
      var xtLblOpt = model.getXTitleLabelOptions();
      var ytLblOpt = model.getYTitleLabelOptions();
      var i, lbl;
      //
      if ( xLblOpt.show ) {
        var xCount = model.getXDivisionCount();
        this._xLabels = [];
        //
        xLblOpt.align = 'center';
        //
        for ( i = 0 ; i < xCount ; ++i ) {
          lbl = new IDB.GraphLabel(xLblOpt, model.getXText(i));
          this._display.add(lbl.getDisplay());
          this._xLabels[i] = lbl;
        }
        this._xLabelMaxH = (lbl ? lbl.height+5 : 0);
      }
      //
      if ( yLblOpt.show ) {
        var yCount = model.getYDivisionCount();
        this._yLabels = [];
        //
        yLblOpt.align = 'right';
        //
        for ( i = 0 ; i < yCount ; ++i ) {
          lbl = new IDB.GraphLabel(yLblOpt, model.getYText(i));
          this._display.add(lbl.getDisplay());
          this._yLabels[i] = lbl;
          //
          this._yLabelMaxW = Math.max(this._yLabelMaxW, lbl.width+5);
        }
      }
      //
      if ( xtLblOpt.show ) {
        xtLblOpt.align = 'center';
        //
        this._xTitle = new IDB.GraphLabel(xtLblOpt, model.getXTitleText());
        this._display.add(this._xTitle.getDisplay());
        this._xLabelMaxH += this._xTitle.height+5;
      }
      //
      if ( ytLblOpt.show ) {
        ytLblOpt.align = 'center';
        //
        this._yTitle = new IDB.GraphLabel(ytLblOpt, model.getYTitleText());
        // noinspection MagicNumberJS
        this._yTitle.getDisplay().rotation(-90);
        this._display.add(this._yTitle.getDisplay());
        this._yLabelMaxW += this._yTitle.height+5;
      }
      }; //-IDB.BubbleGridView.prototype._buildKinetic
  // Z_DOT ppt getDisplay():Kinetic.Group ::IDB.module.Bubble.js  z.74234641006683301.2019.04.10.04.04.03.247|+@.
    /**
     * Display
     * @memberof IDB.BubbleGridView
     * @method
     * @return {Kinetic.Group}
     *
     * @see z.74234641006683301
     */
    IDB.BubbleGridView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.BubbleGridView.prototype.getDisplay
  // Z_DOT ppt getPlotPosX():Number ::IDB.module.Bubble.js  z.15578641006683301.2019.04.10.04.04.47.551|+@.
    /**
     * Plot Pos X
     * @memberof IDB.BubbleGridView
     * @method
     * @return {Number}
     *
     * @see z.15578641006683301
     */
    IDB.BubbleGridView.prototype.getPlotPosX = function() {
      return this._grid.x();
      }; //-IDB.BubbleGridView.prototype.getPlotPosX
  // Z_DOT ppt getPlotPosY():Number ::IDB.module.Bubble.js  z.53123741006683301.2019.04.10.04.05.32.135|+@.
    /**
     * Plot Pos Y
     * @memberof IDB.BubbleGridView
     * @method
     * @return {Number}
     *
     * @see z.53123741006683301
     */
    IDB.BubbleGridView.prototype.getPlotPosY = function() {
      return this._grid.y();
      }; //-IDB.BubbleGridView.prototype.getPlotPosY
  // Z_DOT ppt getPlotWidth():Number ::IDB.module.Bubble.js  z.34196741006683301.2019.04.10.04.06.09.143|+@.
    /**
     * Plot Width
     * @memberof IDB.BubbleGridView
     * @method
     * @return {Number}
     *
     * @see z.34196741006683301
     */
    IDB.BubbleGridView.prototype.getPlotWidth = function() {
      return this._plotW;
      }; //-IDB.BubbleGridView.prototype.getPlotWidth
  // Z_DOT ppt getPlotHeight():Number ::IDB.module.Bubble.js  z.95910841006683301.2019.04.10.04.06.41.959|+@.
    /**
     * Plot Height
     * @memberof IDB.BubbleGridView
     * @method
     * @return {Number}
     *
     * @see z.95910841006683301
     */
    IDB.BubbleGridView.prototype.getPlotHeight = function() {
      return this._plotH;
      }; //-IDB.BubbleGridView.prototype.getPlotHeight
  // Z_DOT ppt setSize(width, height):void ::IDB.module.Bubble.js  z.70265841006683301.2019.04.10.04.07.36.207|+@.
    /**
     * Size
     * @memberof IDB.BubbleGridView
     * @method
     *
     * @param width
     * @param height
     *
     * @see z.70265841006683301
     */
    IDB.BubbleGridView.prototype.setSize = function(width, height) {
      var model = this._model;
      var pad = 4;
      //
      this._plotW = Math.max(10, width-this._yLabelMaxW-pad*2);
      this._plotH = Math.max(0, height-this._xLabelMaxH-pad);
      //
      this._grid.x(this._yLabelMaxW+pad);
      this._grid.destroyChildren();
      //
      var border = new Kinetic.Rect({
        width: this._plotW,
        height: this._plotH,
        stroke: model.getGridColor().css,
        strokeWidth: 1
      });
      this._grid.add(border);
      //
      if ( model.showXLines() ) {
        this._drawXLines();
      }
      //
      if ( model.showYLines() ) {
        this._drawYLines();
      }
      //
      this._positionLabels(height, pad);
      }; //-IDB.BubbleGridView.prototype.setSize
  // Z_DOT cmd _drawXLines(): ::IDB.module.Bubble.js  z.34349841006683301.2019.04.10.04.08.14.343|-!
    /**
     * Draw X Lines
     * @memberof IDB.BubbleGridView
     * @method
     *
     * @see z.34349841006683301
     */
    IDB.BubbleGridView.prototype._drawXLines = function() {
      var model = this._model;
      var gridColorHtml = model.getGridColor().css;
      var len = model.getXDivisionCount();
      var space = this._plotW/(len-1);
      var minors = model.getXNumMinorDivs();
      var i, j, px, bar, line;
      //
      for ( i = 0 ; i < len ; ++i ) {
        px = space*i;
        //
        if ( model.showXBars() ) {
          if ( i%2 === 0 || i === len-1 ) {
            continue;
          }
          //
          bar = new Kinetic.Rect({
            x: px,
            width: space,
            height: this._plotH,
            fill: gridColorHtml,
            opacity: 0.333
          });
          this._grid.add(bar);
          continue;
        }
        //
        // noinspection EqualityComparisonWithCoercionJS
        line = new Kinetic.Line({
          points: [px, 0, px, this._plotH],
          stroke: gridColorHtml,
          strokeWidth: (model.getXDivisionValue(i) == 0 ? 2 : 1),
          opacity: 1
        });
        this._grid.add(line);
        //
        if ( !minors || i >= len-1 ) {
          continue;
        }
        //
        for ( j = 0 ; j < minors ; ++j ) {
          px = space*(i+(j+1)/(minors+1));
          //
          line = new Kinetic.Line({
            points: [px, 0, px, this._plotH],
            stroke: gridColorHtml,
            strokeWidth: 1,
            opacity: 0.3
          });
          this._grid.add(line);
        }
      }
      }; //-IDB.BubbleGridView.prototype._drawXLines
  // Z_DOT cmd _drawYLines(): ::IDB.module.Bubble.js  z.19383941006683301.2019.04.10.04.08.58.391|-!
    /**
     * Draw Y Lines
     * @memberof IDB.BubbleGridView
     * @method
     *
     * @see z.19383941006683301
     */
    IDB.BubbleGridView.prototype._drawYLines = function() {
      var model = this._model;
      var gridColorHtml = model.getGridColor().css;
      var len = model.getYDivisionCount();
      var space = this._plotH/(len-1);
      var minors = model.getYNumMinorDivs();
      var i, j, py, bar, line;
      //
      for ( i = 0 ; i < len ; ++i ) {
        py = space*(len-i-1);
        //
        if ( model.showYBars() ) {
          if ( i%2 === 0 || i === len-1 ) {
            continue;
          }
          //
          bar = new Kinetic.Rect({
            y: py-space,
            width: this._plotW,
            height: space,
            fill: gridColorHtml,
            opacity: 0.333
          });
          this._grid.add(bar);
          continue;
        }
        //
        // noinspection EqualityComparisonWithCoercionJS
        line = new Kinetic.Line({
          points: [0, py, this._plotW, py],
          stroke: gridColorHtml,
          strokeWidth: (model.getYDivisionValue(i) == 0 ? 2 : 1),
          opacity: 1
        });
        this._grid.add(line);
        //
        if ( !minors || i >= len-1 ) {
          continue;
        }
        //
        for ( j = 0 ; j < minors ; ++j ) {
          // noinspection OverlyComplexArithmeticExpressionJS
          py = space*((len-i-1)-(j+1)/(minors+1));
          //
          line = new Kinetic.Line({
            points: [0, py, this._plotW, py],
            stroke: gridColorHtml,
            strokeWidth: 1,
            opacity: 0.3
          });
          this._grid.add(line);
        }
      }
      }; //-IDB.BubbleGridView.prototype._drawYLines
  // Z_DOT cmd _positionLabels(height, pad):void ::IDB.module.Bubble.js  z.55047941006683301.2019.04.10.04.09.34.055|-!
    /**
     * Position Labels
     * @memberof IDB.BubbleGridView
     * @method
     *
     * @param height
     * @param pad
     *
     * @see z.55047941006683301
     */
    IDB.BubbleGridView.prototype._positionLabels = function(height, pad) {
      var i, len, lbl;
      //
      if ( this._xLabels ) {
        len = this._xLabels.length;
        //
        var lastX = 0;
        var hw;
        //
        for ( i = 0 ; i < len ; ++i ) {
          lbl = this._xLabels[i];
          // noinspection OverlyComplexArithmeticExpressionJS
          lbl.x = (i/(len-1))*this._plotW+this._yLabelMaxW+pad - lbl.width/2;
          lbl.y = this._plotH+lbl.height/2 - lbl.height/2 + 5;
          //
          if ( i === 0 ) {
            lbl.x += lbl.width/2-2;
          }
          else if ( i === len-1 ) {
            lbl.x -= lbl.width/2-2;
          }
          //
          hw = lbl.width/2;
          //
          if ( lbl.x-hw < lastX ) {
            lbl.visible = false;
          }
          else {
            lastX = lbl.x+hw;
            lbl.visible = true;
          }
        }
      }
      //
      if ( this._yLabels ) {
        len = this._yLabels.length;
        //
        for ( i = 0 ; i < len ; ++i ) {
          lbl = this._yLabels[i];
          lbl.x = this._yLabelMaxW-lbl.width/2 - lbl.width/2 - 5;
          // noinspection OverlyComplexArithmeticExpressionJS
          lbl.y = ((len-i-1)/(len-1))*this._plotH - lbl.height/2;
          //
          if ( i === 0 ) {
            lbl.y -= lbl.height/2-2;
          }
          else if ( i === len-1 ) {
            lbl.y += lbl.height/2-1;
          }
        }
      }
      //
      if ( this._xTitle ) {
        this._xTitle.width = this._plotW;
        this._xTitle.x = this._yLabelMaxW+pad;
        this._xTitle.y = height-this._xTitle.height;
      }
      //
      if ( this._yTitle ) {
        this._yTitle.width = this._plotH;
        this._yTitle.x = 0;
        this._yTitle.y = this._plotH;
      }
      }; //-IDB.BubbleGridView.prototype._positionLabels
// Z_DOT ctor BubblePlot(graphContext, xBounds, yBounds):BubblePlot                 ::IDB.module.Bubble.js  z.11301790006683301.2019.04.10.02.41.50.311|class
  /**
   * BubblePlot constructor
   * @class BubblePlot
   * @constructor
   * @memberof IDB
   *
   * @param graphContext
   * @param xBounds
   * @param yBounds
   *
   * @see z.11301790006683301
   * @see IDB.BubblePlotView
   * @see module:Bubble
   */
  IDB.BubblePlot = function(graphContext, xBounds, yBounds) {
    this._graphContext = graphContext;
    this._xBounds = xBounds;
    this._yBounds = yBounds;
    this._circles = this._buildCircles();
    }; //-IDB.BubblePlot
  // Z_DOT cmd _buildCircles():[IDB.BubbleCircle] ::IDB.module.Bubble.js  z.78201051006683301.2019.04.10.04.10.10.287|-!
    /**
     * Build Circles
     * @memberof IDB.BubblePlot
     * @method
     * @return {Array<IDB.BubbleCircle>}
     *
     * @see z.
     */
    IDB.BubblePlot.prototype._buildCircles = function() {
      var circles = [];
      var gc = this._graphContext;
      var dpGrid = gc.datapointGrid;
      var xAxesCount = dpGrid.getNumXRows();
      var yAxesCount = dpGrid.getNumYCols(true);
      var axis3 = (yAxesCount >= 3 ? dpGrid.getYAxis(2, true) : null);
      var stats3 = (axis3 ? dpGrid.getStatSetNN(axis3.axisId) : null);
      var xBounds = this._xBounds;
      var yBounds = this._yBounds;
      var xRange = xBounds.max-xBounds.min;
      var yRange = yBounds.max-yBounds.min;
      var zRange = (stats3 ? stats3.max-stats3.min : 0);
      var i, j, b, dp1, dp2, dp3, relX, relY, relZ, dpRow, dpRowLen;
      //
      for ( i = 0 ; i < xAxesCount ; ++i ) {
        dp1 = dpGrid.getDatapointAt(i, 0, true);
        dp2 = dpGrid.getDatapointAt(i, 1, true);
        dp3 = dpGrid.getDatapointAt(i, 2, true);
        //
        relX = (dp1.yValue.numberValue-xBounds.min)/xRange;
        relY = (dp2.yValue.numberValue-yBounds.min)/yRange;
        relZ = 0;
        //
        if ( dp3 && dp3.yValue.numberValue && zRange ) {
          relZ = (dp3.yValue.numberValue-stats3.min)/zRange;
        }
        //
        dp1.axisColor = dpGrid.getColorForXValue(dp1.xValue.stringValue);
        //
        dpRow = dp1.datapointRow.datapoints;
        dpRowLen = dpRow.length;
        //
        for ( j = 0 ; j < dpRowLen ; ++j ) {
          dpRow[j].axisColor = dp1.axisColor;
        }
        //
        b = new IDB.BubbleCircle(gc, dp1, relX, relY, relZ);
        circles[i] = b;
      }
      //
      return circles;
      }; //-IDB.BubblePlot.prototype._buildCircles
  // Z_DOT ppt getCircleCount():Number ::IDB.module.Bubble.js  z.34599051006683301.2019.04.10.04.11.39.543|+@.
    /**
     * Circle Count
     * @memberof IDB.BubblePlot
     * @method
     * @return {Number}
     *
     * @see z.34599051006683301
     */
    IDB.BubblePlot.prototype.getCircleCount = function() {
      return this._circles.length;
      }; //-IDB.BubblePlot.prototype.getCircleCount
  // Z_DOT ppt getCircle(index):IDB.Circle ::IDB.module.Bubble.js  z.19315151006683301.2019.04.10.04.12.31.391|+@.
    /**
     * Circle
     * @memberof IDB.BubblePlot
     * @method
     *
     * @param index
     *
     * @return {IDB.Circle}
     *
     * @see z.19315151006683301
     */
    IDB.BubblePlot.prototype.getCircle = function(index) {
      return this._circles[index];
      }; //-IDB.BubblePlot.prototype.getCircle
  // Z_DOT ppt getLabelOptions():Object ::IDB.module.Bubble.js  z.53320251006683301.2019.04.10.04.13.22.335|+@.
    /**
     * Label Options
     * @memberof IDB.BubblePlot
     * @method
     *
     * @return {Object}
     *
     * @see z.53320251006683301
     */
    IDB.BubblePlot.prototype.getLabelOptions = function() {
      return this._graphContext.settings.get('BubbleLabelsOptions');
      }; //-IDB.BubblePlot.prototype.getLabelOptions
// Z_DOT ctor BubblePlotView(model):BubblePlotView                                  ::IDB.module.Bubble.js  z.76794790006683301.2019.04.10.02.42.29.767|class
  /**
   * BubblePlotView constructor
   * @class BubblePlotView
   * @constructor
   * @memberof IDB
   *
   * @param model
   *
   * @see z.76794790006683301
   * @see IDB.BubblePlot
   * @see module:Bubble
   */
  IDB.BubblePlotView = function(model) {
    this._model = model;
    this._buildKinetic();
    }; //-IDB.BubblePlotView
  // Z_DOT cmd _buildKinetic(): ::IDB.module.Bubble.js  z.62323251006683301.2019.04.10.04.13.52.326|-!
    /**
     * Build Kinetic
     * @memberof IDB.BubblePlotView
     * @method
     *
     * @see z.62323251006683301
     */
    IDB.BubblePlotView.prototype._buildKinetic = function() {
      var model = this._model;
      var cirCount = model.getCircleCount();
      var lblOpt = model.getLabelOptions();
      var i, cir, lbl;
      //
      this._display = new Kinetic.Group(undefined);
      this._circles = [];
      //
      for ( i = 0 ; i < cirCount ; ++i ) {
        cir = new IDB.BubbleCircleView(model.getCircle(i));
        this._display.add(cir.getDisplay());
        this._circles[i] = cir;
      }
      //
      if ( !lblOpt.show ) {
        return;
      }
      //
      this._labels = [];
      //
      lblOpt.align = 'center';
      //
      this._labelHold = new Kinetic.Group(undefined);
      this._display.add(this._labelHold);
      //
      for ( i = 0 ; i < cirCount ; ++i ) {
        lbl = new IDB.GraphLabel(lblOpt, model.getCircle(i).getLabelText());
        this._labelHold.add(lbl.getDisplay());
        this._labels[i] = lbl;
        this._circles[i].setLabelRef(lbl);
      }
      }; //-IDB.BubblePlotView.prototype._buildKinetic
  // Z_DOT ppt getDisplay():Kinetic.Group ::IDB.module.Bubble.js  z.59629251006683301.2019.04.10.04.14.52.695|+@.
    /**
     * Display
     * @memberof IDB.BubblePlotView
     * @method
     * @return {Kinetic.Group}
     *
     * @see z.59629251006683301
     */
    IDB.BubblePlotView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.BubblePlotView.prototype.getDisplay
  // Z_DOT ppt setSize(width, height):void ::IDB.module.Bubble.js  z.85553351006683301.2019.04.10.04.15.35.558|+.
    /**
     * Size
     * @memberof IDB.BubblePlotView
     * @method
     *
     * @param width
     * @param height
     *
     * @see z.85553351006683301
     */
    IDB.BubblePlotView.prototype.setSize = function(width, height) {
      var model = this._model;
      var circles = this._circles;
      var labels = this._labels;
      var i, cir, cirModel, size, cirX, cirY, lbl;
      // noinspection MagicNumberJS,NestedConditionalExpressionJS
      var animationDelay = circles.length <= 10 ? 100 : circles.length <= 15 ? 70 : circles.length <= 20 ? 50 : 35;
      //
      for ( i in circles ) {
        cir = circles[i];
        cirModel = model.getCircle(i);
        size = cirModel.getSize();
        cirX = (cirModel.getRelativeX()*width);
        cirY = ((1-cirModel.getRelativeY())*height);
        //
        cir.getDisplay().x(cirX).y(cirY);
        //
        // noinspection OverlyComplexBooleanExpressionJS
        if (cir.isAnimationEnabled && !cir.doneAnimating && !cir.isAnimating && !cir.willAnimate) {
          cir.runInitialAnimation(animationDelay * i);
        }
        //
        if ( !labels ) {
          continue;
        }
        //
        lbl = (labels ? labels[i] : null);
        lbl.setOffscreen(cirY+size <= -2);
        //
        if ( !lbl.getOffscreen() ) {
          lbl.x = cirX-lbl.width/2;
          lbl.y = cirY-size/2-lbl.height-3;
        }
      }
      }; //-IDB.BubblePlotView.prototype.setSize
//EOF
