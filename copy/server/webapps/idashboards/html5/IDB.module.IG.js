'use strict';
IDB.IG = function(chart, $div, graphProperties, dataSet) {
    var me = this, lo = graphProperties.get("XValueLabelOptions"), mt = graphProperties.get('DatapointMatchType');
    lo.align = graphProperties.get("XValueLabelTextAlign");
    graphProperties.set('XLabelFunction', IDB.listener(me, "_labelFunction"));
    me._currentRow = null;

    if(mt !== IDB.MT.NONE) {
        graphProperties.set('DatapointMatchType', IDB.MT.X_VALUE);
    }

    IDB.GraphBaseKinetic.call(me, chart, $div, graphProperties, dataSet, null);
};

IDB.IG.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.IG.prototype.constructor = IDB.IG;

IDB.IG.prototype._labelFunction  = function(dp) {
    var me = this, gp = me.gp, template = gp("XLabelTemplate"), chart = me._chart;

    if(template) {
        return chart.expandMacros(template, null, dp);
    }
    else {
        return dp.xValue.formattedValue;
    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.IG.prototype._build = function() {
    var me = this, I = IDB, gc = me._graphContext, gp = me.gp, ds = me._dataSet, stage = me._stage, stageLayer = me._stageLayer,
    axes = ds.getGraphAxes(), renderer;

    axes.shift();

    me._info.xMatchOnly = true;
    me._info.numericYOnly = false;
    me._info.displaysMultipleXValues = false;
    me._info.calloutStyle = 'none';
    me._info.legendStyle = IDB.LS.NONE;

	stage.add(me._dpLayer = new Kinetic.Layer());



    me._renderers = [];
    me._rendererMap = {};
    for(var j=0, jj=axes.length; j<jj; j++) {
        renderer = new I.IGR(me, axes[j]);
        me._rendererMap[axes[j].axisId] = renderer;
        me._renderers.push(renderer);
    }

    me._sr = new I.GraphSingleRow(gc, I.listener(me, '_showRow'), gp('StickySelect'));
    me._srView = new I.GraphSingleRowView(me._sr);
    stageLayer.add(me._srView.getDisplay());
};



IDB.IG.prototype._update = function(width, height) {
    var me = this, rect, w, h, gm, renderers = me._renderers, numRenderers = renderers.length;

    me._srView.setSize(width, height);
    rect = me._srView.getContentRect();
    w = rect.width;
    h = rect.height;

    me._dpLayer.x(rect.x).y(rect.y);

    gm = me.getGridMetrics(w,h,me._renderers.length,1);

    var rows = gm.rows, cols = gm.cols, rowLens = gm.rowLens, cellW = (w / cols), cellH = (h / rows), numLaidOut = 0, nextRendererIndex = 0,
        rowNum = 0, startX, rowLen;

    for(rowNum = 0; rowNum < rows && numLaidOut < numRenderers; rowNum++) {

        startX = 0;
        rowLen = rowLens[rowNum];
        if(rowLen < cols) {
            startX = (cols*cellW)/2 - (rowLen*cellW)/2;
        }

        var rowY = rowNum * cellH;
       
        for(var colNum = 0; colNum < rowLen && numLaidOut < numRenderers; colNum++) {
            var renderer = renderers[nextRendererIndex++];
            numLaidOut++;
            renderer.layout(startX + (colNum * cellW) + 3,  rowY + 3, cellW-6, cellH-6);
        }
    }


};


IDB.IG.prototype._showRow = function(datapointRow) {
    var me = this, renderers = me._renderers, map = me._rendererMap, renderer, j=0, jj = 0, dps = datapointRow ? datapointRow.datapoints : null, dp;

    if(!datapointRow) {
        for(j=0, jj=renderers.length; j<jj; j++) {
            renderers[j].setDP(null);
        }
        return;
    }

    for(j=0, jj=dps.length; j<jj; j++) {
        dp = dps[j];
        renderer = map[dp.yAxis.axisId];
        if(!renderer) {
            continue;
        }
        renderer.setDP(dp);
    }
};

IDB.IG.prototype.destroy = function() {
    IDB.GraphBaseKinetic.prototype.destroy.call(this);
};

IDB.IG.prototype.getGridMetrics = function(w, h, cellsNeeded, targetAspectRatio) {
    targetAspectRatio = targetAspectRatio || 1;
    var tests = [], rows = 1, cols = 1, numCells = 1, ar = 1, diff = 0, test, gm, rowLens = [], totalCells, blanks, oddNumRows,
        smallHalf, bigHalf, half;
    while(true) {
        cols = 1;
        while(true) {
            numCells = rows * cols;
            if(numCells >= cellsNeeded) {
                ar = (w/cols) / (h/rows);
                diff = Math.abs(ar - targetAspectRatio);
                test = {rows:rows, cols:cols, ar:ar, diff:diff};
                tests.push(test);
                break;
            }
            cols++;
        }
        rows++;
        if(rows > cellsNeeded) {
            break;
        }
    }

    gm = tests[0];
    for(var j=1, jj=tests.length; j<jj; j++) {
        if(tests[j].diff < gm.diff) {
            gm = tests[j];
        }
    }

    gm.rowLens = rowLens;
    rows = gm.rows;
    cols = gm.cols;

    if(rows === 1) {
        rowLens.push(cellsNeeded);
        return gm;
    }

    if(cols === 1) {
        for(j=0; j<rows; j++) {
            rowLens.push(1);
        }
        return gm;
    }
    
    totalCells = rows * cols;
    blanks = totalCells - cellsNeeded;

    for(j=0; j<rows; j++) {
        if(j === (rows-1)) {
            rowLens.push(cols-blanks);
        }
        else {
            rowLens.push(cols);
        }
    }

    oddNumRows = (rows % 2) !== 0;

    if(oddNumRows) {
        smallHalf = Math.floor(rows/2);
        bigHalf = rows - smallHalf;
//                trace("InfographicGraph.as.getGridMetrics(): blanks=" + blanks + ", bigHalf=" + bigHalf + ", smallHalf=" + smallHalf);
        if(blanks === smallHalf) {
            for(j=0; j<rows; j++) {
                if((j % 2) === 0) { // even
                    rowLens[j] = (cols);
                }
                else {
                    rowLens[j] = (cols-1);
                }
            }
            return gm;
        }
        if(blanks === bigHalf) {
            for(j=0; j<rows; j++) {
                if((j % 2) === 0) { // even
                    rowLens[j] = (cols-1);
                }
                else {
                    rowLens[j] = (cols);
                }
            }
            return gm;
        }
    }

    // even number of rows
    half = rows / 2;
    if(blanks === half) {
        for(j=0; j<rows; j++) {
            if((j % 2) === 0) { // even
                rowLens[j] = (cols);
            }
            else {
                rowLens[j] = (cols-1);
            }
        }
        return gm;
    }

    if((blanks % 2) === 0) {
        // even number of blanks, split between top and bottom row
        rowLens[0] = cols - (blanks/2);
        rowLens[rows-1] = cols - (blanks/2);
        return gm;
    }

    return gm;
};




/*global IDB, Kinetic, $ */
IDB.IGR = function(graph, axis) {
    var me = this, I = IDB, listener = I.listener, labelColor = axis.get("GraphicLabelColor"), group, url = I.resolveUrl(axis.imageUrl || "", axis.imageUseProxy),
       isShape = (url.indexOf("shape:") === 0), shadow = axis.get("GraphicLabelShadow"), image,
       labelOpts = {
           fontSize:axis.get("GraphicLabelSize"),
           fill:labelColor.css,
           align:"center",
           shadowEnabled:shadow,
           shadowColor:shadow ? ((labelColor.lum > 127) ? "#444444" : "#EEEEEE") : "none",
           shadowOffsetX:0,
           shadowOffsetY:0,
           shadowOpacity:1,
           shadowBlur:5
       };

    me._graph = graph;
    me._axis = axis;

    me._group = group = new Kinetic.Group();
    group.on("mouseover", listener(me, "_handleOver"));
    group.on("mouseout", listener(me, "_handleOut"));
    group.on("click", I.leftClickListener(me, "_handleClick"));
    me._touchHandler = new I.TouchHandler(me._group);
    graph._dpLayer.add(me._group);

    me._isShape = isShape;
    me._x = me._y = me._w = me._h = me._imgH = me._imgW = me._imgAR = 0;
    me._nameW = me._valW = -1;
    me._maxH = axis.get("GraphicMaxHeight");
    me._maxW = axis.get("GraphicMaxWidth");
    me._loaded = false;
    me._kImage = me._shape = me._currDp = null;

    if(!isShape) {
        image = new Image();
        image.onload = function() {
            me._imgW = image.width;
            me._imgH = image.height;
            me._imgAR = me._imgH ? (me._imgW / me._imgH) : 0;
            me._loaded = true;

            group.add( me._kImage = new Kinetic.Image({
               x:0,
               y:0,
               image:image,
               width:image.width,
               height:image.height,
               visible:false
            }));
            me._kImage.setZIndex(0);
            me._layoutParts();
            graph._stage.draw();
        };
        image.src = url;
    }
    else {
        group.add(me._shape = me.createShape(url));
    }

    group.add(me._kNameLabel = new Kinetic.Text($.extend({text:axis.axisName}, labelOpts)));
    group.add(me._kValLabel = new Kinetic.Text($.extend({text:""}, labelOpts)));
};

IDB.IGR.prototype.createShape = function(url) {
    var regex = /^shape:(\w+)(\/([0-9a-fx]+))?/i, shape = "circle", color = "#000000",
        tokens = regex.exec(url), pp = IDB.parsePoly, config = null;
//            trace("IDBGraphic.createShape(): tokens=" + tokens);
    if(tokens) {
        shape = tokens[1];
        config = pp(shape);
        if(config) {
            shape = "poly";
        }
        if(tokens[3]) {
            color = IDB.rgb(tokens[3]).css;
        }
    }
    config = $.extend({width:100}, config);
    return IDB.kShape(shape, color, 100, config);

};

IDB.IGR.prototype.layout = function(x, y, w, h) {
    var me = this;
    me._x = x;
    me._y = y;
    me._w = w = Math.max(1, w);
    me._h = h = Math.max(1, h);
    if(me._nameW === -1) {
        me._nameW = me._kNameLabel.width();
    }
    me._valW = me._kValLabel.width();
    me._group.x(x).y(y).visible(true);
    me._layoutParts();
};

IDB.IGR.prototype.setDP = function(dp) {
    var me = this, val = me._kValLabel;
    me._currDp = dp;
    me._valW = -1;
    val.text(dp ? dp.yValue.formattedValue : "").width("auto");
    me._layoutParts();
};

IDB.IGR.prototype._layoutParts = function() {
    var me = this, isShape = me._isShape, loaded = me._loaded, w = me._w, h = me._h,
    img = me._kImage, name = me._kNameLabel, val = me._kValLabel, nameW = me._nameW,
    iW = me._imgW, iH = me._imgH, iAR = me._imgAR, valW = me._valW, shape = me._shape,
    maxW = me._maxW, maxH = me._maxH,
    vGap = 5,
    glp = (me._axis.get("GraphicLabelPlacement") || "auto"),
    labelPlacement = (glp === "auto" ? (isShape ? "center" : "aboveBelow") : glp),
    isCenter = (labelPlacement === "center"),
    nameH = name.height(),
    valH = val.height(),
    vSpace = isCenter ? h : (h - nameH - valH - 2*vGap),
    textH = Math.min(nameH + valH,  h), nameY = (h-textH)/2,
    valY = nameY + nameH,
    iHAdjustment= 1.0, iWAdjustment = 1.0,
    imageY, shapeTop,
    maxSize = Math.min(maxW || maxH, maxH || maxW),
    shapeSize =  maxSize > 0 ? Math.min(maxSize,  Math.min(w,  vSpace)) : Math.min(w,  vSpace);

    if(!w) {
        return;
    }

    if(valW === -1) {
        valW = val.width();
    }

    if(isCenter || !loaded) {
        name.setAttrs({width:nameW, x:((w-nameW)/2), y:nameY});
        val.setAttrs({width:valW, x:((w-valW)/2), y:valY});
    }

    if(isShape) {

        IDB.kShapeSize(shape, shapeSize);
        shapeTop = (h-shapeSize)/2;
        shape.x(w/2).y(h/2);
        if(!isCenter) {
            name.setAttrs({x:(w-nameW)/2, y:Math.max(0,shapeTop-vGap-nameH), width:nameW});
            val.setAttrs({x:(w-valW)/2, y:Math.max(0, Math.min(shapeTop+shapeSize+vGap, h-valH)), width:valW});
        }
        return;
    }

    if(!loaded) {
        return;
    }

    if(vSpace < 10 || w < 10) {
        img.visible(false);
        return;
    }
    
    iH = Math.min(iH, vSpace);
    if(maxH && maxH < iH) {
        iH = maxH;
    }
    iHAdjustment = iH / me._imgH;
    
    iW = Math.min(iW, w);
    if(maxW && maxW < iW) {
        iW = maxW;
    }
    iWAdjustment = iW / me._imgW;
    
    if(iWAdjustment != 1.0 || iHAdjustment != 1.0) {
        if(iWAdjustment < iHAdjustment) {
            iH = iW / iAR;
        }
        else {
            iW = iH * iAR;
        }
    }
    
    imageY = (h-iH)/2;
    img.setAttrs({x:(w-iW)/2, y:imageY, width:iW, height:iH, visible:true});

    if(!isCenter) {
        name.setAttrs({x:(w-nameW)/2, y:Math.max(0,imageY-vGap-nameH), width:nameW});
        val.setAttrs({x:(w-valW)/2, y:Math.max(0, Math.min(imageY+iH+vGap, h-valH)), width:valW});
    }
};


/* jshint unused:true */
IDB.IGR.prototype._handleClick = function(evt) {
/* jshint unused:false */
    this._graph._graphContext.onDatapointClicked(this._currDp);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.IGR.prototype._handleOver = function() {
    var me = this, dp = me._currDp, gc = me._graph._graphContext;
    if(!dp) {
        return;
    }
    gc.onDatapointOver(dp);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.IGR.prototype._handleOut = function() {
    var me = this, dp = me._currDp, gc = me._graph._graphContext;
    if(!dp) {
        return;
    }
    gc.onDatapointOut(dp);
};



/*global IDB, $ */

