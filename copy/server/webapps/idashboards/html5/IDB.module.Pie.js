'use strict';
IDB.PieCircle = function(graphContext, datapoints, sumAcrossXValue) {
	this._graphContext = graphContext;
	this._datapoints = datapoints;
	this._sumAcrossXValue = sumAcrossXValue;

	this._prepareData();
	this._buildSlices();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype._prepareData = function() {
	var datapoints = this._datapoints;
	var gc = this._graphContext;
	var sett = gc.settings;
	var max = 70;
	var hasNeg = false;
	var sum = 0;
	var i, val;
			
	if ( datapoints.length > max ) {
		gc.showWarning(IDB.GraphLang.get('PieWarningTitle'),
			IDB.GraphLang.get('PieRowsWarning', [max, datapoints.length]));
		datapoints.splice(max, datapoints.length-max);
	}

	for ( i = 0 ; i < datapoints.length ; ++i ) {
		val = datapoints[i].yValue.numberValue;
				
		if ( val <= 0 ) {
			datapoints.splice(i--, 1);
			
			if ( val < 0 ) {
				hasNeg = true;
			}

			continue;
		}
				
		sum += val;
	}

	var dp = datapoints[0];
	this._labelText = (dp ? dp.yAxis.axisName : '');
	
	if ( dp && sett.getBool('SumAcross') ) {
		this._labelText = this._sumAcrossXValue;
	}
	
	if ( hasNeg ) {
		gc.showWarning(IDB.GraphLang.get('PieWarningTitle'),
			IDB.GraphLang.get('PieNegativeWarning', [this._labelText]));
	}

	this._valueSum = sum;

	//TODO: update the DatapointRow for "Sum Across" mode?
};


/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype._buildSlices = function() {
	this._slices = [];
	this._slices2 = [];
	this._labels = [];

	var gc = this._graphContext;
	var sett = gc.settings;
	var datapoints = this._datapoints;
	var dpCount = datapoints.length;
	var valSum = this._valueSum;
	var aStart = sett.get('PieRotation');
	var sumAngles = aStart;
	var i, dp, perc, angle, a0, a1, a2, a3, range, explodeAngle, temp, slice, slice2, label;

	if ( aStart < 0 ) {
		aStart += 360;
	}

	for ( i = 0 ; i < dpCount ; ++i ) {
		dp = datapoints[i];
				
		perc = dp.yValue.numberValue/valSum;

        // These 3 properties are used in PiGraph.updateCallout().
        dp.sliceIndex = i;
        dp.dpGroup = datapoints;
        dp.percStr = (perc*100).toFixed(1) + "%";

		angle = perc*360;

		if ( angle+sumAngles > 360+aStart ) {
			angle = 360+aStart-sumAngles; //avoid floating pt errors
		}

		a0 = sumAngles%360;
		a1 = (sumAngles+angle)%360;
		slice2 = null;
				
		if ( a1 > a0 ) {
			slice = new IDB.PieSlice(gc, dp, a0, a1);
		}
		else {
			range = (360-a0)+a1;
			explodeAngle = (a0+range/2)%360;
			a3 = a1;
			a1 = 360;
			a2 = 0;
					
			if ( a3 > 180 ) {
				temp = a0;
				a0 = a2;
				a2 = temp;
				temp = a1;
				a1 = a3;
				a3 = temp;
			}
					
			slice = new IDB.PieSlice(gc, dp, a0, a1, explodeAngle);
					
			if ( a2 || a3 ) {
				slice2 = new IDB.PieSlice(gc, dp, a2, a3, explodeAngle);
			}
		}

		label = new IDB.PieSliceLabel(gc, perc, (sumAngles+angle/2)%360, dp);
		sumAngles += angle;
		
		this._slices.push(slice);
		this._slices2.push(slice2);
		this._labels.push(label);
	}
	
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getSliceCount = function() {
	return this._slices.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getSlice = function(index) {
	return this._slices[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getSlice2 = function(index) {
	return this._slices2[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getSliceLabel = function(index) {
	return this._labels[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getLabelText = function() {
	return this._labelText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('PieLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getCameraHeight = function() {
	return this._graphContext.settings.get('CameraHeight');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getExplodeLevel = function() {
	return this._graphContext.settings.get('PieExplodeLevel');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.get3dHeight = function() {
	return this._graphContext.settings.get('Pie3dHeight');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircle.prototype.getSliceLabelPos = function() {
	return (!this._graphContext.settings.get('SliceLabelOptions').show ? 0.85 : 
		this._graphContext.settings.get('SliceLabelPos'));
};
/* global IDB, Kinetic */
/*====================================================================================================*/
IDB.PieCircleView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircleView.prototype._buildKinetic = function() {
	var model = this._model;
	var sliceCount = model.getSliceCount();
	var i, label, sliceModel, slice;

	this._display = new Kinetic.Group();

	this._sliceHold = new Kinetic.Group();
	this._display.add(this._sliceHold);

	this._labelHold = new Kinetic.Group();
	this._display.add(this._labelHold);

	this._slices = [];
	this._slices2 = [];
	this._sliceLabels = [];

	for ( i = 0 ; i < sliceCount ; ++i ) {
		label = new IDB.PieSliceLabelView(model.getSliceLabel(i));
		this._labelHold.add(label.getDisplay());
		this._sliceLabels[i] = label;

		sliceModel = model.getSlice(i);
		slice = new IDB.PieSliceView(sliceModel);
		slice.setLabelRef(label);
		this._sliceHold.add(slice.getDisplay());
		this._slices[i] = slice;
		
		sliceModel = model.getSlice2(i);

		if ( sliceModel ) {
			slice = new IDB.PieSliceView(sliceModel);
			slice.setLabelRef(label);
			this._sliceHold.add(slice.getDisplay());
			this._slices2[i] = slice;
		}

	}
	
	var lblOpt = model.getLabelOptions();
	lblOpt.align = 'center';

	if ( lblOpt.show ) {
		this._label = new IDB.GraphLabel(lblOpt, model.getLabelText());
		this._display.add(this._label.getDisplay());
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircleView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircleView.prototype.setSize = function(width, height) {
	var model = this._model;
	var labelPerc = 1-(this._label ? (this._label.height+4)/height : 0);
	var rad = (width*labelPerc)/2;
	var explode = model.getExplodeLevel()*0.2;
	var useRad = rad*(1-explode);
	var ph = model.get3dHeight();
	var ch = model.getCameraHeight();
	var midY = rad*ch;
	var sliceLblPos = model.getSliceLabelPos();
	var slices = this._slices;
	var slices2 = this._slices2;
	var sliceLabels = this._sliceLabels;
	var len = slices.length;
	var maxY = 0;
	var i, expRad, pos, slice, sliceLabel;
	var animationDelay = 35;
	
	if ( sliceLblPos > 1 ) {
		useRad *= 1/sliceLblPos;
	}

	for ( i = 0 ; i < len ; ++i ) {
		expRad = model.getSlice(i).getExplodeAngle()/180*Math.PI;

		pos = {
			x: Math.sin(expRad)*rad*explode,
			y: -Math.cos(expRad)*rad*explode*ch
		};

		slice = slices[i];
		slice.getDisplay().position(pos);
		slice.setWidth(useRad*2);
		
		sliceLabel = sliceLabels[i];

		var lblDisp = sliceLabel.getDisplay();
		lblDisp.position(pos);
		sliceLabel.setWidth(useRad*2);
		lblDisp.setOpacity(1);

		if (slice.isAnimationEnabled && !slice.doneAnimating && !slice.isAnimating && !slice.willAnimate)
		{
			slice.runInitialAnimation(animationDelay*i);
		}
		
		slice = slices2[i];
		
		if ( slice ) {
			slice.getDisplay().position(pos);
			slice.setWidth(useRad*2);
		}
		
		maxY = Math.max(maxY, useRad*ph*(1-ch));
	}

	//center vertically within space, if necessary
	pos = {
		x: width/2,
		y: height/2
	};

	if ( this._label) {
		pos.y = midY+rad*ch+maxY;

		var	pushY = (height-(pos.y+this._label.height+5))/2;
	
		this._label.y = pos.y+pushY+5;
		this._label.width = width;

		pos.y = midY+pushY;
	}
	
	this._sliceHold.position(pos);
	this._labelHold.position(pos);

	this._sortChildren();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircleView.prototype._sortChildren = function() {
	var model = this._model;
	var slices = this._slices;
	var slices2 = this._slices2;
	var len = slices.length;
	var hasSlice2 = false;
	var items = [];
	var i, sliceModel;
			
	for ( i = 0 ; i < len ; ++i ) {
		sliceModel = model.getSlice(i);

		items[i] = {
			slice: slices[i],
			angle: sliceModel.getAngleNearest180(),
			covers: sliceModel.covers180()
		};
	}
			
	items.sort(this._angleCompare);
			
	for ( i = 0 ; i < len ; ++i ) {
		if ( slices2[i] ) {
			hasSlice2 = true;
			slices2[i].getDisplay().setZIndex(0);
			break;
		}
	}
			
	for ( i = 0 ; i < len ; ++i ) {
		items[i].slice.getDisplay().setZIndex(hasSlice2 ? i+1 : i);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieCircleView.prototype._angleCompare = function(a, b) {
	var aDist = Math.abs(180-a.angle);
	var bDist = Math.abs(180-b.angle);
			
	if ( aDist > bDist ) {
		//Slices nearer the top (further from degree 180) get lower depth
		return -1;
	}
			 
	if ( aDist == bDist ) {
		//Happens only when one slice covers degree 180, give covering slice higher depth
		return (a.covers ? 1 : -1);
	}
			
	//Slice must be nearer 180 so give it higher depth
	return 1;
};
/*global IDB, $ */
/*====================================================================================================*/
IDB.PieGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	this._info.calloutStyle = 'custom';
	this._info.legendStyle = (this._graphContext.settings.getBool('SumAcross') ?
		IDB.LS.AXES : IDB.LS.X_VALUES);
};

IDB.PieGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.PieGraph.prototype.constructor = IDB.PieGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieGraph.prototype._build = function() {
	this._pies = new IDB.PieSet(this._graphContext);
	this._piesView = new IDB.PieSetView(this._pies);
	this._stageLayer.add(this._piesView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieGraph.prototype._update = function(width, height) {
	this._piesView.setSize(width, height);
};


IDB.PieGraph.prototype.updateCallout = function(pDp, $callout) {

    var dpts = pDp.dpGroup, dpt, isPie, color, name, graphContext = this._graphContext, 
        settings = graphContext.settings, sumAcross = settings.getBool('SumAcross'), datapointGrid = graphContext.datapointGrid,
        mousedOverSliceIndex = pDp.sliceIndex, i = 0, maxRows = 11, $t, $tr, $td, yAxis, xValue, dptsLength = dpts.length;
    if(dptsLength > maxRows) {
        var half = Math.floor(maxRows / 2), firstIndex = Math.min(dptsLength-maxRows, Math.max(0, mousedOverSliceIndex-half));
        dpts = dpts.slice(firstIndex, firstIndex+maxRows);
        dptsLength = dpts.length;

        // cache this result, since it should be the same each time this slice is moused over.
        pDp.dpGroup = dpts;
        
    }

    $t = $("<table>").appendTo($callout);
    $tr = $("<tr>").appendTo($t);
    $td = $("<td colspan=\"4\" class=\"idb-header\">").appendTo($tr).text(pDp.xValue.formattedValue).css({"text-align":"center"});

    for(i=0; i<dptsLength; ++i) {
        dpt = dpts[i];
        isPie = (mousedOverSliceIndex === dpt.sliceIndex);
        if (sumAcross ) {
            yAxis = dpt.yAxis;
            color = yAxis.axisColor;
            name = yAxis.axisName;
        }
        else {
            xValue = dpt.xValue;
            color = datapointGrid.getColorForXValue(xValue.stringValue); // super.dataProvider.getColorForXValue(dpt.xValue.stringValue);
            name = xValue.formattedValue;//vSettings.formatValueWithAxis(dpt.xValue, dpt.xAxis);
        }

        $tr = $("<tr>").appendTo($t);
        
        if ( isPie && dptsLength > 1) { 
            $tr.addClass("idb-callout-highlighted"); 
        }

        $td = $("<td>").appendTo($tr);
        $("<div>").html("&nbsp;").css({"background-color":color.css, "width":"15px"}).appendTo($td);
        $td = $("<td>").appendTo($tr).text(name);
        $td = $("<td>").appendTo($tr).text(dpt.yValue.formattedValue);

        $td = $("<td>").appendTo($tr);
        
        if ( isPie ) {
            $td.text( dpt.percStr );
        }
    }

};

/*====================================================================================================*/
IDB.PieSet = function(graphContext) {
	this._graphContext = graphContext;
	graphContext.addDatapointsUpdatedListener(IDB.listener(this, '_onDatapointsUpdated'));
	
	var sett = graphContext.settings;
	var dpGrid = graphContext.datapointGrid;

	this._isSumAcross = sett.getBool('SumAcross');

	var itemCount = (this._isSumAcross ? dpGrid.getNumXRows() : dpGrid.getNumYCols(true));
	var showCount = Math.min(sett.get('NumShownPies'), itemCount);
	var buildItemFunc = IDB.listener(this, '_buildPie');
	
	var lo = graphContext.settings.get('SliceLabelOptions');
	lo.position = graphContext.settings.get('SliceLabelPos');

	this._datapoints = this._buildDatapoints(itemCount);
	this._pages = new IDB.GraphPages(graphContext, buildItemFunc, itemCount, showCount);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSet.prototype._buildDatapoints = function(itemCount) {
	var datapoints = [];
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var xCount = dpGrid.getNumXRows();
	var yCount = dpGrid.getNumYCols(true);
	var sumAcross = this._isSumAcross;
	var i, j, dp;
	
	for ( i = 0 ; i < itemCount ; ++i ) {
		datapoints[i] = [];
				
		if ( sumAcross ) {
			for ( j = 0 ; j < yCount ; ++j ) {
				dp = dpGrid.getDatapointAt(i, j, true);
				datapoints[i][j] = dp;
			}
		}
		else {
			for ( j = 0 ; j < xCount ; ++j ) {
				dp = dpGrid.getDatapointAt(j, i, true);
				dp.axisColor = dpGrid.getColorForXValue(dp.xValue.stringValue);
				datapoints[i][j] = dp;
			}
		}
	}

	return datapoints;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSet.prototype._buildPie = function(index) {
	var dpRow = this._datapoints[index];
	var label = (this._isSumAcross && dpRow.length > 0 ? dpRow[0].xValue.formattedValue : '');
	return new IDB.PieCircle(this._graphContext, dpRow, label);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSet.prototype._onDatapointsUpdated = function() {
	var pages = this._pages;
	var itemCount = pages.getItemCount();
	var datapoints = this._datapoints;
	var i, j, dpRow, rowCount;
	
	// Does a visible Pie already contain a match?

	for ( i = 0 ; i < itemCount ; ++i ) {
		if ( !pages.getItem(i) ) {
			continue;
		}

		dpRow = datapoints[i];
		rowCount = dpRow.length;

		for ( j = 0 ; j < rowCount ; ++j ){
			if ( dpRow[j].getVisualState() == IDB.VS.HIGHLIGHTED ) {
				return;
			}
		}
	}
	
	// Jump to the first Pie with a match.

	for ( i = 0 ; i < itemCount ; ++i ) {
		if ( pages.getItem(i) ) {
			continue;
		}

		dpRow = datapoints[i];
		rowCount = dpRow.length;

		for ( j = 0 ; j < rowCount ; ++j ){
			if ( dpRow[j].getVisualState() == IDB.VS.HIGHLIGHTED ) {
				pages.setCurrentIndex(i);
				return;
			}
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSet.prototype.getGraphPages = function() {
	return this._pages;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSet.prototype.showPieLabel = function() {
	return this._graphContext.settings.get('PieLabelOptions').show;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSet.prototype.getCameraHeight = function() {
	return this._graphContext.settings.get('CameraHeight');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSet.prototype.get3dHeight = function() {
	return this._graphContext.settings.get('Pie3dHeight');
};
/* global IDB, Kinetic */
/*====================================================================================================*/
IDB.PieSetView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSetView.prototype._buildKinetic = function() {
    var model = this._model;

	var buildFunc = IDB.listener(this, '_buildPie');
	var layoutFunc = IDB.listener(this, '_updatePieLayout');
	
	this._display = new Kinetic.Group();

	this._pagesView = new IDB.GraphPagesView(model.getGraphPages());
	this._pagesView.build(buildFunc, layoutFunc);
	this._display.add(this._pagesView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSetView.prototype._buildPie = function(index, itemModel) {
	return new IDB.PieCircleView(itemModel);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSetView.prototype._updatePieLayout = function() {
	var model = this._model;
	var pages = model.getGraphPages();
	var currI = pages.getCurrentIndex();
	var maxI = currI+pages.getShowCount();
	var pv = this._pagesView;

	var ch = model.getCameraHeight();
	var fullHRatio = 0.5357*ch*ch - 1.4729*ch + 1.85;
	var noHRatio = 1/ch;
			
	var ph = model.get3dHeight();
	var ratio = fullHRatio+(noHRatio-fullHRatio)*Math.pow(1-ph, 2);

	if ( this._model.showPieLabel() ) {
		ratio *= 0.96; //always "square-up" slighly to give more room for pie label
	}

	var spacing = new IDB.GraphSpacing(pages.getShowCount(), 
		pv.getAvailableWidth(), pv.getAvailableHeight(), ratio);
	var itemW = spacing.getItemWidth();
	var itemH = spacing.getItemHeight();
	var i, itemView, display;

	for ( i = currI ; i < maxI ; ++i ) {
		itemView = pv.getItemView(i);
		display = itemView.getDisplay();

		itemView.setSize(itemW, itemH);
		display.position(spacing.getPos(i-currI));
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSetView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSetView.prototype.setSize = function(width, height) {
	this._pagesView.setSize(width, height);
};

/*====================================================================================================*/
IDB.PieSlice = function(graphContext, datapoint, angle0, angle1, explodeAngle) {
	this._graphContext = graphContext;
	this._datapoint = datapoint;
	this._angle0 = angle0;
	this._angle1 = angle1;
	this._range = (angle0 > angle1 ? (360-angle0)+angle1 : angle1-angle0);
	this._explodeAngle = IDB.ifUndef(explodeAngle, (angle0+this._range/2)%360);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getDatapoint = function() {
	return this._datapoint;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getAngle0 = function() {
	return this._angle0;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getAngle1 = function() {
	return this._angle1;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getRange = function() {
	return this._range;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getExplodeAngle = function() {
	return this._explodeAngle;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getAngleNearest180 = function() {
	return (Math.abs(180-this._angle0) < Math.abs(180-this._angle1) ? this._angle0 : this._angle1);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.covers180 = function() {
	return (this._angle0 <= 180 && this._angle1 >= 180);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getInnerRadius = function() {
	return this._graphContext.settings.get('PieInnerRadius');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getCameraHeight = function() {
	return this._graphContext.settings.get('CameraHeight');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.getExplodeLevel = function() {
	return this._graphContext.settings.get('PieExplodeLevel');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.get3dHeight = function() {
	return this._graphContext.settings.get('Pie3dHeight');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, "_handleClick");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.createOverListener = function() {
	return IDB.listener(this, "_handleOver");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype.createOutListener = function() {
	return IDB.listener(this, "_handleOut");
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype._handleClick = function() {
	this._graphContext.onDatapointClicked(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype._handleOver = function() {
	this._graphContext.onDatapointOver(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSlice.prototype._handleOut = function() {
	this._graphContext.onDatapointOut(this._datapoint);
};
/*====================================================================================================*/
IDB.PieSliceLabel = function(graphContext, percent, angle, datapoint) {
	this._graphContext = graphContext;
	this._angle = angle;
	this._datapoint = datapoint;
	this._text = this._buildText(percent);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabel.prototype._buildText = function(percent) {
	if ( !this.getLabelOptions().show ) {
		return null;
	}

	var text = '';
	var sett = this._graphContext.settings;
	var sumAcross = sett.getBool('SumAcross');
	var showX = sett.getBool('SliceLabelShowXVal');
	var showY = sett.getBool('SliceLabelShowYVal');
	var showPerc = sett.getBool('SliceLabelShowPerc');
	var dp = this._datapoint;
			
	if ( showX ) {
		text += (sumAcross ? dp.yAxis.axisName : dp.xValue.formattedValue);
		text += (showY || showPerc ? ': ' : '');
	}
			
	if ( showY ) {
		text += dp.yValue.formattedValue;
		text += (showPerc ? ' / ' : '');
	}
			
	if ( showPerc ) {
		var mult = Math.pow(10, sett.get('SliceLabelPercDec'));
		text += Math.round(percent*100*mult)/mult+'%';
	}
			
	return text;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabel.prototype.getAngle = function() {
	return this._angle;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabel.prototype.getDatapoint = function() {
	return this._datapoint;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabel.prototype.getText = function() {
	return this._text;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabel.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('SliceLabelOptions');
};
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabel.prototype.getSliceLabelShadow = function() {
    var b = this._graphContext.settings.get('SliceLabelShadow');
    return (b === undefined ? true : !!b);
};
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabel.prototype.getCameraHeight = function() {
	return this._graphContext.settings.get('CameraHeight');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabel.prototype.getInnerRadius = function() {
	return this._graphContext.settings.get('PieInnerRadius');
};

/*====================================================================================================*/
IDB.PieSliceLabelView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabelView.prototype._buildKinetic = function() {
	var model = this._model;
	var lblOpt = model.getLabelOptions();

	this._display = new Kinetic.Group();
	this._display.setListening(false);

	if ( !lblOpt.show ) {
		return;
	}

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	this._label = new IDB.GraphLabel(lblOpt, model.getText());
	this._hold.add(this._label.getDisplay());

	//LATER: add text glow once its available in KineticJS
	var dl = this._label.getDisplayLabel();
	dl.shadowEnabled(model.getSliceLabelShadow());
	dl.shadowColor(lblOpt.color.x.css);
	dl.shadowOpacity(1.0);
	dl.shadowBlur(3);
	dl.shadowOffsetX(1);
	dl.shadowOffsetY(1);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabelView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceLabelView.prototype.setWidth = function(width) {
	var lbl = this._label;

	if ( !lbl ) {
		return;
	}

	var model = this._model;
	var rad = width/2;
	var lblPos = model.getLabelOptions().position;
	var labelRad = rad*lblPos;
	var angleR = model.getAngle()/180*Math.PI;
	var yScale = model.getCameraHeight();
	var halfW = lbl.width/2;
	var halfH = lbl.textHeight/2;
	var r = rad;
	var pos = {
		x:  IDB.MathCache.sin(angleR)*labelRad,
		y: -IDB.MathCache.cos(angleR)*labelRad*yScale
	};
	var lineConfig = {
		points: [],
		stroke: '#000',
		strokeWidth: 1,
		opacity: 0.25
	};
	
	if ( lblPos > 1 ) {
		r *= lblPos;
	}
	
	lbl.width = lbl.textWidth+5;
	lbl.x = -halfW;
	lbl.y = -halfH;

	if ( pos.x-halfW < -r ) {
		pos.x = halfW-r;
	}
	else if ( pos.x+halfW > r ) {
		pos.x = r-halfW;
	}
			
	if ( pos.y-halfH < -r ) {
		pos.y = halfH-r;
	}
	else if ( pos.y+halfH > r ) {
		pos.y = r-halfH;
	}

	this._hold.position(pos);

	////

	if ( this._line ) {
		this._line.destroy();
	}
			
	if ( lblPos > 1 ) {
		lineConfig.points = [
			Math.sin(angleR)*rad, -Math.cos(angleR)*rad*yScale,
			pos.x, pos.y
		];
	}
	else if ( lblPos < model.getInnerRadius() ) {
		var innerRad = rad*model.getInnerRadius();

		lineConfig.points = [
			Math.sin(angleR)*innerRad, -Math.cos(angleR)*innerRad*yScale,
			pos.x, pos.y
		];
	}

	this._line = new Kinetic.Line(lineConfig);
	this._display.add(this._line);
	this._line.moveToBottom();
};
/* global IDB, Kinetic */
/*====================================================================================================*/
IDB.PieSliceView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
	this._buildKinetic();
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
Object.defineProperties(IDB.PieSliceView.prototype, 
{
	isAnimationEnabled:
	{
		enumerable: true,
		configurable: false,
		get: function()
		{
			return this._animationsEnabled;
		},
		set: function(val)
		{
			this._animationsEnabled = !!val;
		}
	},
	isAnimating:
	{
		enumerable: true,
		configurable: false,
		get: function()
		{
			var me = this, animation = me._animation;
			return animation !== null && animation.isRunning();
		},
		set: function(val)
		{
			var me = this, animation = me._animation;
			if (val === false)
			{
				if (animation !== null)
				{
					if (animation.isRunning && animation.isRunning())
					{
						animation.stop();
					}
					else
					{
						me.doneAnimating = true;
					}
				}
				me._animation = null;
			}
		}
	},
	willAnimate: {
		enumerable: true,
		configurable: false,
		get: function()
		{
			return this._animationTimeout !== null;
		}
	},
	animationParametersChanged: {
		enumerable: true,
		configurable: false,
		get: function()
		{
			var me = this, changed = me._paramsChangedFlag;
			me._paramsChangedFlag = false;
			return !!changed;
		}
	},
	doneAnimating: {
		value: false,
		enumerable: true,
		configurable: false,
		writable: true
	},
	_animation: {
		enumerable: false,
		value: null,
		configurable: false,
		writable: true
	},
	_animationTimeout: {
		enumerable: false,
		value: null,
		configurable: false,
		writable: true
	},
	_animationsEnabled: {
		enumerable: false,
		value: false,
		configurable: false,
		writable: true
	},
	_paramsChangedFlag: {
		value: false,
		enumerable: false,
		configurable: false,
		writable: true
	},
	_animationParameters: {
		value: null,
		enumerable: false,
		configurable: false,
		writable: true
	}
});

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype._buildKinetic = function() {
	var me = this, model = me._model, context = model ? model._graphContext : null,
		settings = context ? context.settings : null;

	me.isAnimationEnabled = settings && settings.animationEnabled ? settings.animationEnabled() : false;
	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());
    this._display._touchHandler = new IDB.TouchHandler(this._display);

	this._sides = new Kinetic.Group();
	this._display.add(this._sides);

	this._top = new Kinetic.Group();
	this._display.add(this._top);

	//NOTE: the Flash slice adds a "cover" slice for displaying the faded visual state. This is 
	//currently omitted from HTML5, but may be added back in later.
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype.setLabelRef = function(label) {
	this._label = label;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype.setWidth = function(width) {
	if ( width == this._currW ) {
		return;
	}

	this._currW = width;
	this._drawSlice(width/2, this._top, this._sides);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype.runInitialAnimation = function(delay)
{
	var me = this;
	delay = typeof delay === 'number' ? delay : 0;
	me._setAnimation(me._makeAnimationObj());
	if (delay === 0) me._runAnimation();
	else
	{
		me._animationTimeout = window.setTimeout(function() { me._runAnimation(); }, delay);
	}
};


/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype._runAnimation = function()
{
	var me = this;
	me._display.setOpacity(1);
	me._animation.start();
};


/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype._setAnimation = function(animationObj)
{
	var me = this, anim = me._animation;
	if (me.isAnimating && !Object.is(anim, animationObj))
	{
		if (anim.isRunning()) anim.stop();
	}
	me._animation = animationObj;
};


/*----------------------------------------------------------------------------------------------------*/
/*
 * Potentially useful for other classes that manage the PieSliceViews

IDB.PieSliceView.prototype.cancelPendingAnimation = function()
{
	var me = this, timeout = me._animationTimeout;
	if (timeout !== null)
	{
		window.clearTimeout(timeout);
		me._animationTimeout = null;
	}
};*/


/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype._updateAnimationParameters = function(animationParameters)
{
	var me = this;
	if (me.isAnimationEnabled && !me.doneAnimating)
	{
		var params = me._animationParameters;
		me._animationParameters = animationParameters;
		if (params !== null && !Object.is(animationParameters, params))
			me._paramsChangedFlag = true;
	}
};



/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype._makeAnimationObj = function()
{
	var me = this, disp = me._display, lblDisp = me._label ? me._label.getDisplay() : null, dur = 450, finalScale = 1.0, finalLabelVisible = true,
		retAnim = new Kinetic.Animation(function(frame)
	{
		if (me.animationParametersChanged)
		{
			if (me._animationParameters.faded)
			{
				finalScale = 0.95;
				finalLabelVisible = false;
				disp.setOpacity(0.2);
			}
			else
			{
				finalScale = 1.0;
				finalLabelVisible = true;
				disp.setOpacity(1.0);
			}
		}

		if (frame.time<=dur)
		{
			var perc = frame.time / dur, scale = perc * 0.875 + 0.125;
			disp.setScaleX(scale*finalScale);
			disp.setScaleY(scale*finalScale);
		}
		else
		{
			retAnim.stop();
			disp.setScaleX(finalScale);
			disp.setScaleY(finalScale);
			if (lblDisp)
			{
				lblDisp.setVisible(finalLabelVisible);
			}
			me.isAnimating = false;
		}
	}, disp.getLayer() || null);

	// Prepare to be animated, possibly after a delay
	disp.setScaleX(0.125);
	disp.setScaleY(0.125);
	disp.setOpacity(0);
	lblDisp.setVisible(false);

	return retAnim;
};


/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype._drawSlice = function(outerRad, topHold, sidesHold) {
	var model = this._model;
	var a0 = model.getAngle0();
	var a1 = model.getAngle1();
	var innerRad = outerRad*model.getInnerRadius();
	var baseA = a0/180.0*Math.PI;
	var steps = Math.round(Math.max(2, model.getRange()/2)); //one every 4 degrees, at least 2 total
	var inc = (Math.PI*model.getRange()/180.0)/steps;
	var yScale = model.getCameraHeight();
	var axisColor = model.getDatapoint().axisColor;
	var sin = [];
	var cos = [];
	var angles = [];
	var i, a;

	for ( i = 0 ; i <= steps ; ++i ) {
		a = baseA+inc*i;
		sin[i] = IDB.MathCache.sin(a);
		cos[i] = -IDB.MathCache.cos(a);
		angles[i] = a*180/Math.PI;
	}
			
	sidesHold.destroyChildren();
	topHold.destroyChildren();
	
	var config = {
		fill: null,
		closed: true,
		points: []
	};
			
	if ( yScale != 1 ) {
		var h = (outerRad*model.get3dHeight())*(1-yScale);
		var covers180 = model.covers180();

		//Draw inner-radius wall only if necessary (with some bounds padding)

		config.fill = 'rgb('+
			Math.round(axisColor.r*0.7)+','+
			Math.round(axisColor.g*0.7)+','+
			Math.round(axisColor.b*0.7)+')';

		if ( a0 < 90 || a1 > 270 ) {
			config.points = [];

			for ( i = 0 ; i <= steps ; ++i ) {
				if ( covers180 || angles[i] <= 90 || angles[i] >= 270 ) {
					config.points.push(sin[i]*innerRad, cos[i]*innerRad*yScale);
				}
			}

			for ( i = steps ; i >= 0 ; --i ) {
				if ( covers180 || angles[i] <= 90 || angles[i] >= 270 ) {
					config.points.push(sin[i]*innerRad, cos[i]*innerRad*yScale+h);
				}
			}

			sidesHold.add(new Kinetic.Line(config));
		}
				
		//Draw outer-radius wall

		config.points = [];

		for ( i = 0 ; i <= steps ; ++i ) {
			if ( angles[i] >= 90 && angles[i] <= 270 ) {
				config.points.push(sin[i]*outerRad, cos[i]*outerRad*yScale);
			}
		}

		for ( i = steps ; i >= 0 ; --i ) {
			if ( angles[i] >= 90 && angles[i] <= 270 ) {
				config.points.push(sin[i]*outerRad, cos[i]*outerRad*yScale+h);
			}
		}

		sidesHold.add(new Kinetic.Line(config));
				
		//Only draw first angle wall if necessary

		config.fill = 'rgb('+
			Math.round(axisColor.r*0.5)+','+
			Math.round(axisColor.g*0.5)+','+
			Math.round(axisColor.b*0.5)+')';

		if ( a0 > 180 ) {
			config.points = [
				sin[0]*outerRad, cos[0]*outerRad*yScale,
				sin[0]*outerRad, cos[0]*outerRad*yScale+h,
				sin[0]*innerRad, cos[0]*innerRad*yScale+h,
				sin[0]*innerRad, cos[0]*innerRad*yScale
			];

			sidesHold.add(new Kinetic.Line(config));
		}
				
		//Only draw second angle wall if necessary

		if ( a1 < 180 ) {
			config.points = [
				sin[steps]*outerRad, cos[steps]*outerRad*yScale,
				sin[steps]*outerRad, cos[steps]*outerRad*yScale+h,
				sin[steps]*innerRad, cos[steps]*innerRad*yScale+h,
				sin[steps]*innerRad, cos[steps]*innerRad*yScale
			];

			sidesHold.add(new Kinetic.Line(config));
		}
	}
			
	//Draw top of slice

	config.fill = axisColor.css;
	config.points = [sin[0]*outerRad, cos[0]*outerRad*yScale];

	for ( i = 1 ; i <= steps ; ++i ) {
		config.points.push(sin[i]*outerRad, cos[i]*outerRad*yScale);
	}

	for ( i = steps ; i >= 0 ; --i ) {
		config.points.push(sin[i]*innerRad, cos[i]*innerRad*yScale);
	}

	topHold.add(new Kinetic.Line(config));
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PieSliceView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getDatapoint().getVisualState();
	var isFaded = (state == IDB.VS.FADED);
	var me = this, changeAnimation = me.isAnimationEnabled && !me.doneAnimating;

	if (changeAnimation)
	{
		me._updateAnimationParameters({faded: isFaded});
	}
	else
	{
		this._display.opacity(isFaded ? 0.2 : 1.0);
		this._display.scaleX(isFaded ? 0.95 : 1.0);
		this._display.scaleY(isFaded ? 0.95 : 1.0);
		if ( this._label ) {
			this._label.getDisplay().visible(!isFaded);
		}
	}
};

/*====================================================================================================*/

