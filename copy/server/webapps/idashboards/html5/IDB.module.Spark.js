'use strict';
IDB.SparkGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	this._info.legendStyle = IDB.LS.AXES;
};

IDB.SparkGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.SparkGraph.prototype.constructor = IDB.SparkGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkGraph.prototype._build = function() {
	var gc = this._graphContext;
	var scrollModel = new IDB.SparkScroll(gc);

	this._scrollView = new IDB.SparkScrollView(scrollModel);
	this._stageLayer.add(this._scrollView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkGraph.prototype._update = function(width, height) {
	this._scrollView.setSize(width, height);
};

/*====================================================================================================*/
IDB.SparkPlot = function(graphContext, yAxisCol) {
	this._graphContext = graphContext;
	this._yAxisCol = yAxisCol;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype._build = function() {
	var gc = this._graphContext;
	var yAxisCol = this._yAxisCol;
	var dpGrid = gc.datapointGrid;
	var sett = gc.settings;
	var yAxis = dpGrid.getYAxis(yAxisCol, true);
	var statSet = dpGrid.getStatSet(yAxis.axisId);
	var min = statSet.min;
	var max = statSet.max;
	var numRows = dpGrid.getNumXRows();
	var xWidth = 1/numRows;
	var datapoints = [];
	var dp = null;
	var i;

	this._barMode = sett.getBool('SparkBarMode');

	if ( this._barMode ) {
		min = Math.min(0, min);
		max = Math.max(0, max);
	}
	
	for ( i = 0 ; i < numRows ; ++i ) {
		dp = dpGrid.getDatapointAt(i, yAxisCol, true);
		datapoints[i] = dp;
		
		if ( dp.yValue.isNothing ) {
			if ( min > 0 ) { min = 0; }
			if ( max < 0 ) { max = 0; }
		}
	}
	
	var range = Math.max(1, max-min);
	var zero = 1-(Math.max(-min,0)/range);
	var skipNulls = sett.getBool('SparkPlotSkipNulls');
	var yCoords = [];
	var segments = [];
	var seg, i0, i2, p0, p1, p2;

	for ( i = 0 ; i < numRows ; ++i ) {
		yCoords[i] = 1-(datapoints[i].yValue.numberValue-min)/range;
	}
	
	if ( numRows == 1 ) {
		p1 = { x: xWidth, y: 0.5 };
		seg = new IDB.SparkSegment(gc, datapoints[0], xWidth, p1, p1, p1, zero);
		segments.push(seg);
	}
	else {
		for ( i = 0 ; i < numRows ; ++i ) {
			i0 = Math.max(0, i-1);
			i2 = Math.min(i+1, numRows-1);
			
			p0 = { x: xWidth*i0, y: yCoords[i0] };
			p1 = { x: xWidth*i, y: yCoords[i] }; //maintain for the dot
			p2 = { x: xWidth*i2, y: yCoords[i2] };
			
			if ( skipNulls ) {
				if ( datapoints[i].yValue.isNothing ) {
					continue;
				}

				if ( datapoints[i0].yValue.isNothing ) {
					p0 = p1;
				}

				if ( datapoints[i2].yValue.isNothing ) {
					p2 = p1;
				}
			}
			
			seg = new IDB.SparkSegment(gc, datapoints[i], xWidth, p0, p1, p2, zero);
			segments.push(seg);
		}
	}

	this._segments = segments;
	this._dotColor = (sett.getBool('SparkDotUseRange') ? dp.rangeColor : sett.get('SparkDotColor'));
	this._axisText = yAxis.axisName;
	this._valueText = dp.yValue.formattedValue;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.isBarMode = function() {
	return this._barMode;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getSegmentCount = function() {
	return this._segments.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getSegment = function(index) {
	return this._segments[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getDotColor = function() {
	return this._dotColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getAxisText = function() {
	return this._axisText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getValueText = function() {
	return this._valueText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.showDot = function() {
	return this._graphContext.settings.getBool('SparkDotShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getAxisLabelOptions = function() {
	return this._graphContext.settings.get('SparkAxisLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getAxisLabelPosition = function() {
	return this._graphContext.settings.get('SparkAxisLabelPosition');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getValueLabelOptions = function() {
	return this._graphContext.settings.get('SparkValueLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getMaxWidth = function() {
	return this._graphContext.settings.get('SparkPlotMaxWidth');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlot.prototype.getDotSize = function() {
	return this._graphContext.settings.get('SparkDotSize');
};

/*====================================================================================================*/
IDB.SparkPlotView = function(model) {
	this._model = model;
	this._build();
};

IDB.SparkPlotView.FullSize = 1000;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlotView.prototype._build = function() {
	var model = this._model;
	var segCount = model.getSegmentCount();
	var segConstruct = (model.isBarMode() ? IDB.SparkSegmentBarView : IDB.SparkSegmentLineView);
	var segModel = null;
	var axisLblOpt = model.getAxisLabelOptions();
	var valLblOpt = model.getValueLabelOptions();
	var size = IDB.SparkPlotView.FullSize;
	var i, seg;

	this._display = new Kinetic.Group();

	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	for ( i = 0 ; i < segCount ; ++i ) {
		segModel = model.getSegment(i);

		seg = new segConstruct(segModel);
		this._hold.add(seg.getDisplay());
	}
	
	if ( model.showDot() ) {
		var p1 = segModel.getPositions()[1];

		this._dot = new Kinetic.Circle({
			x: p1.x*size,
			y: p1.y*size,
			radius: 0.5,
			fill: model.getDotColor().css
		});
		this._hold.add(this._dot);
	}
			
	if ( axisLblOpt.show ) {
		axisLblOpt.align = 'left';

		this._axisLabel = new IDB.GraphLabel(axisLblOpt, model.getAxisText());
		this._display.add(this._axisLabel.getDisplay());
	}
			
	if ( valLblOpt.show ) {
		valLblOpt.align = 'left';

		this._valueLabel = new IDB.GraphLabel(valLblOpt, model.getValueText());
		this._display.add(this._valueLabel.getDisplay());
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlotView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlotView.prototype.calcWidths = function(width) {
	var model = this._model;
	var axisLbl = this._axisLabel;
	var valLbl = this._valueLabel;
	var hasLbl = (axisLbl || valLbl);

	var widths = {
		plot: Math.min(model.getMaxWidth(), width*(hasLbl ? 0.75 : 1)),
		axis: 0,
		value: 0
	};

	var allowLblW = width-widths.plot;
	
	if ( axisLbl ) {
		axisLbl.resetText();
		widths.axis = Math.min(allowLblW, axisLbl.textWidth+7);
	}
	
	if ( valLbl ) {
		valLbl.resetText();
		widths.value = Math.min(allowLblW-widths.axis, valLbl.textWidth+7);
	}
	
	return widths;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkPlotView.prototype.setSize = function(widths, maxWidth, height) {
	var model = this._model;
	var axisLbl = this._axisLabel;
	var valLbl = this._valueLabel;
	var plotX = 0;
	var axisX = 0;
	var plotW = widths.plot;
	var axisW = widths.axis;
	var valW = widths.value;
	var lblRight = (model.getAxisLabelPosition() == 0);
	var valX;
	
	if ( axisLbl ) {
		axisLbl.resetText();
	}

	if ( valLbl ) {
		valLbl.resetText();
	}
	
	if ( lblRight ) {
		var allowLblW = maxWidth-plotW;
		
		if ( axisLbl ) {
			axisW = Math.min(axisLbl.textWidth, allowLblW);
		}

		allowLblW -= axisW;
		valW = allowLblW;
		axisX = plotW;
		valX = axisX+axisW;
	}
	else {
		plotX = axisW;
		valX = plotX+plotW;
	}

	if ( !model.isBarMode() ) {
		plotW += plotW/(model.getSegmentCount()+1); //expand the line plot to fill the entire space
	}

	this._hold
		.x(Math.round(plotX))
		.scaleX(plotW/IDB.SparkPlotView.FullSize)
		.scaleY(height/IDB.SparkPlotView.FullSize);
	
	if ( this._dot ) {
		var dotSize = model.getDotSize();

		this._dot
			.scaleX(dotSize/this._hold.scaleX())
			.scaleY(dotSize/this._hold.scaleY());
	}
	
	if ( axisLbl ) {
		axisLbl.x = axisX;
		axisLbl.y = (height-axisLbl.textHeight)/2+1;
		axisLbl.width = axisW;
		axisLbl.truncate();
		axisLbl.visible = (axisW > 0);
	}
	
	if ( valLbl ) {
		valLbl.x = (lblRight && axisLbl ? axisLbl.x+axisLbl.textWidth+7 : valX);
		valLbl.y = (height-valLbl.textHeight)/2+1;
		valLbl.width = valW;
		valLbl.truncate();
		valLbl.visible = (valW > 0);
	}
};

/*====================================================================================================*/
IDB.SparkScroll = function(graphContext) {
	this._graphContext = graphContext;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScroll.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var numCols = gc.datapointGrid.getNumYCols(true);
	var i, plot;

	this._plots = [];
	
	for ( i = 0 ; i < numCols ; ++i ) {
		plot = new IDB.SparkPlot(gc, i);
		this._plots.push(plot);
	}

	this._maxLabelSize = sett.get('SparkAxisLabelOptions').size;
	var valLblOpt = sett.get('SparkValueLabelOptions');

	if ( valLblOpt.show ) {
		this._maxLabelSize = Math.max(this._maxLabelSize, valLblOpt.size);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScroll.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScroll.prototype.getPlotCount = function() {
	return this._plots.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScroll.prototype.getPlot = function(index) {
	return this._plots[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScroll.prototype.getMaxLabelSize = function() {
	return this._maxLabelSize;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScroll.prototype.getVerticalGap = function() {
	return this._graphContext.settings.get('SparkPlotVertSpace');
};

/*====================================================================================================*/
IDB.SparkScrollView = function(model) {
	this._model = model;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScrollView.prototype._build = function() {
	var model = this._model;
	var plotCount = model.getPlotCount();
	var i, plot;

	this._display = new Kinetic.Group();
	this._plots = [];
	
	this._hold = new Kinetic.Group();
	this._display.add(this._hold);

	for ( i = 0 ; i < plotCount ; ++i ) {
		plot = new IDB.SparkPlotView(model.getPlot(i));
		this._hold.add(plot.getDisplay());
		this._plots.push(plot);
	}
	
	this._scroll = new IDB.GraphScroll();
	this._scroll.onVisualUpdate(IDB.listener(model, 'refreshDisplay'));
	this._scroll.onScroll(IDB.listener(this, '_onScroll'));
	this._scroll.lineScrollSize = 16;
	this._display.add(this._scroll.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScrollView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScrollView.prototype.setSize = function(width, height) {
	var model = this._model;
	var hold = this._hold;
	var scroll = this._scroll;
	var plots = this._plots;
	var plotCount = plots.length;
	var vertSpace = model.getVerticalGap();
	var plotH = model.getMaxLabelSize();
	var totalH = Math.round(plotH*plotCount + vertSpace*(plotCount-1));
	var scrollDiff = totalH-height;
	var scrollVis = (scrollDiff > 0);
	var scrollThick = 16;
	var availW = width-(scrollVis ? scrollThick+10 : 0);
	var i, plot, widths;
	
	var maxWidths = {
		plot: 0,
		axis: 0,
		value: 0
	};
	
	for ( i = 0 ; i < plotCount ; ++i ) {
		plot = plots[i];
		plot.getDisplay().y(Math.round((plotH+vertSpace)*i));
		
		widths = plot.calcWidths(availW);
		maxWidths.plot = Math.max(maxWidths.plot, widths.plot);
		maxWidths.axis = Math.max(maxWidths.axis, widths.axis);
		maxWidths.value = Math.max(maxWidths.value, widths.value);
	}

	var maxPlotW = maxWidths.plot + maxWidths.axis + maxWidths.value;
	
	if ( maxPlotW > availW ) {
		var extra = maxPlotW-availW;
		var lblW = maxPlotW-maxWidths.plot;
		var ratio = 1-extra/lblW;
		maxWidths.axis *= ratio;
		maxWidths.value *= ratio;
		maxPlotW = availW;
	}

	for ( i = 0 ; i < plotCount ; ++i ) {
		plot = plots[i];
		plot.setSize(maxWidths, maxPlotW, plotH);
	}

	hold.x(Math.round((availW-maxPlotW)/2));

	this._display.clip({
		x: 0,
		y: 0,
		width: width,
		height: height
	});

	scroll.getDisplay().visible(scrollVis);

	if ( scrollVis ) {
		scroll.getDisplay().x(hold.x()+maxPlotW+10);
		scroll.setSize(scrollThick, height);
		scroll.pageSize = height;
		scroll.maxScrollPos = scrollDiff;
		scroll.scrollPos = IDB.MathUtil.clamp(-hold.y()/scrollDiff, 0, scroll.maxScrollPos);
		scroll.refresh();
		this._onScroll();
	}
	else {
		hold.y(Math.round((height-totalH)/2));
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkScrollView.prototype._onScroll = function() {
	this._hold.y(-Math.round(this._scroll.scrollPos));
};

/*====================================================================================================*/
IDB.SparkSegment = function(graphContext, datapoint, xWidth, p0, p1, p2, zero) {
	this._graphContext = graphContext;
	this._datapoint = datapoint;
	this._xWidth = xWidth;
	this._positions = [ p0, p1, p2 ];
	this._zero = zero;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.getColor = function() {
	return this._datapoint.yAxis.axisColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.getVisualState = function() {
	return this._datapoint.getVisualState();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.getPositions = function() {
	return this._positions;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.getZero = function() {
	return this._zero;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.getXWidth = function() {
	return this._xWidth;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.getAlpha = function() {
	return this._graphContext.settings.getAlpha('SparkPlotFillTrans');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.getRelativeBarThickness = function() {
	return this._graphContext.settings.get('SparkPlotBarThickness')/100;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, '_handleClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.createOverListener = function() {
	return IDB.listener(this, '_handleOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype.createOutListener = function() {
	return IDB.listener(this, '_handleOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype._handleClick = function() {
	this._graphContext.onDatapointClicked(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype._handleOver = function() {
	this._graphContext.onDatapointOver(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegment.prototype._handleOut = function() {
	this._graphContext.onDatapointOut(this._datapoint);
};

/*====================================================================================================*/
IDB.SparkSegmentBarView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegmentBarView.prototype._build = function() {
	var model = this._model;
	var positions = model.getPositions();
	var size = IDB.SparkPlotView.FullSize;
	var zero = model.getZero()*size;
	var xWidthFull = model.getXWidth()*size;
	var xWidth = xWidthFull*model.getRelativeBarThickness();
	var p = positions[1];
	var px = p.x*size + (xWidthFull-xWidth)/2;
	var py = p.y*size;
	var h = zero-py;

	this._display = new Kinetic.Group();
	this._display.on("click", model.createClickListener());
	this._display.on("mouseover", model.createOverListener());
	this._display.on("mouseout", model.createOutListener());
    this._touchHandler = new IDB.TouchHandler(this._display);
	
	if ( Math.abs(h) <= 200 ) {
		this._cover = new Kinetic.Rect({
			fill: 'rgba(0, 0, 0, 0.001)',
			x: px,
			y: zero-size/10,
			width: xWidth,
			height: size/5
		});
		this._display.add(this._cover);
	}

	this._box = new Kinetic.Rect({
		fill: model.getColor().css,
		opacity: model.getAlpha(),
		x: px,
		y: py,
		width: xWidth,
		height: h
	});
	this._display.add(this._box);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegmentBarView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegmentBarView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getVisualState();
	this._display.opacity(state == IDB.VS.FADED ? 0.2 : 1.0);
};

/*====================================================================================================*/
IDB.SparkSegmentLineView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegmentLineView.prototype._build = function() {
	var model = this._model;
	var positions = model.getPositions();
	var size = IDB.SparkPlotView.FullSize;
	var zero = model.getZero()*size;
	var p0 = positions[0];
	var p1 = positions[1];
	var p2 = positions[2];
	var px0 = (p0 ? p1.x+(p0.x-p1.x)/2 : p1.x)*size;
	var py0 = (p0 ? p1.y+(p0.y-p1.y)/2 : p1.y)*size;
	var px1 = p1.x*size;
	var py1 = p1.y*size;
	var px2 = (p2 ? p1.x+(p2.x-p1.x)/2 : p1.x)*size;
	var py2 = (p2 ? p1.y+(p2.y-p1.y)/2 : p1.y)*size;
	var color = model.getColor().css;
	var alpha = model.getAlpha();
	var linePoints = [
		px0, py0,
		px1, py1,
		px2, py2
	];
	var line;

	this._display = new Kinetic.Group();
	this._display.on("click", model.createClickListener());
	this._display.on("mouseover", model.createOverListener());
	this._display.on("mouseout", model.createOutListener());
	this._touchHandler = new IDB.TouchHandler(this._display);

	if ( alpha == 0 ) {
		alpha = 0.0001; //workaround for Kinetic bug: use transparent "area" mode for the hover area
	}

	//if ( alpha ) { //"area" mode

		line = new Kinetic.Line({
			stroke: color,
			strokeWidth: 1,
			strokeScaleEnabled: false,
			points: linePoints
		});
		this._display.add(line);

		var fill = new Kinetic.Line({
			fill: color,
			opacity: alpha,
			closed: true,
			points: linePoints.concat([
				px2, zero,
				px0, zero
			])
		});
		this._display.add(fill);

	/*}
	else { //"line" mode
		
		line = new Kinetic.Line({
			stroke: color,
			strokeWidth: 1,
			strokeScaleEnabled: false,
			points: linePoints
		});
		this._display.add(line);

		//TODO:BUG this will not generate mouse events due to a "strokeScaleEnabled" KineticJS bug ...
		// ... ZK has posted a fix, see issue #530 at GitHub

		var lineCover = new Kinetic.Line({
			stroke: 'rgba(0, 0, 0, 0.001)',
			strokeWidth: 5,
			strokeScaleEnabled: false,
			points: linePoints
		});
		this._display.add(lineCover);

	}*/

	this._high = new Kinetic.Line({
		stroke: color,
		strokeWidth: 2,
		strokeScaleEnabled: false,
		points: linePoints,
		listening: false,
		visible: false
	});
	this._display.add(this._high);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegmentLineView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SparkSegmentLineView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getVisualState();
	this._display.opacity(state == IDB.VS.FADED ? 0.2 : 1.0);
	this._high.visible(state == IDB.VS.HIGHLIGHTED);
};

/*====================================================================================================*/

