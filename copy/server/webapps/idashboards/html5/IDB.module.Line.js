'use strict';
IDB.LineAxis = function(graphContext, cartGridYAxis, yAxisIndex) {
	this._graphContext = graphContext;
	this._cartGridYAxis = cartGridYAxis;
	this._yAxisIndex = yAxisIndex;

	this._graphContext.addDatapointsUpdatedListener(IDB.listener(this, '_onDatapointsUpdated'));
	
	this._yAxis = graphContext.datapointGrid.getYAxis(this._yAxisIndex, true);
	this._stats = cartGridYAxis.getStatsByAxis(this._yAxis.axisId);

	this._datapoints = [];
	this._segments = this._buildSegments();
	
	var sett = this._graphContext.settings;
	var is3D = sett.getBool('Is3dGraph');

	if ( !is3D && sett.getBool('SmoothMode') ) {
		this._buildSmoothPoints();
	}
	else {
		this._buildPoints();
	}
	
	if ( !is3D && sett.getBool('ScatterRegressionLine') ) {
		this._buildRegression();
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype._buildSegments = function() {
	var segments = [];
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var yIndex = this._yAxisIndex;
	var rowCount = dpGrid.getNumXRows();
	var i, dp, seg;

	for ( i = 0 ; i < rowCount ; ++i ) {
		dp = dpGrid.getDatapointAt(i, yIndex, true);

		seg = new IDB.LineSegment(gc, dp);
		segments.push(seg);
		this._datapoints.push(dp);
	}

	return segments;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype._buildPoints = function() {
	var segments = this._segments;
	var datapoints = this._datapoints;
	var gc = this._graphContext;
	var xCount = segments.length;
	var stats = this._stats;
	var range = stats.max-stats.min;
	var skipNull = gc.settings.getBool('LineSkipNulls');
	var i, seg, dv0, dv1, dv2, y0, y1, y2, p0, p1, p2;

	for ( i = 0 ; i < xCount ; ++i ) {
		seg = segments[i];

		dv0 = datapoints[Math.max(0, i-1)].yValue;
		dv1 = datapoints[i].yValue;
		dv2 = datapoints[Math.min(i+1, xCount-1)].yValue;

		if ( skipNull && dv1.isNothing ) {
			seg.setActive(false);
			continue;
		}

		y0 = dv0.numberValue;
		y1 = dv1.numberValue;
		y2 = dv2.numberValue;

		if ( !skipNull ) {
			if ( dv0.isNothing ) { y0 = 0; }
			if ( dv1.isNothing ) { y1 = 0; }
			if ( dv2.isNothing ) { y2 = 0; }
		}

		y0 = (y0-y1)/2+y1;
		y2 = (y2-y1)/2+y1;

		y0 = 1-(y0-stats.min)/range;
		y1 = 1-(y1-stats.min)/range;
		y2 = 1-(y2-stats.min)/range;

		p0 = { x: Math.max(0, i-0.5), y: y0 };
		p1 = { x: i, y: y1 };
		p2 = { x: Math.min(i+0.5, xCount-1), y: y2 };

		if ( skipNull ) {
			if ( dv0.isNothing ) { p0 = p1; }
			if ( dv2.isNothing ) { p2 = p1; }
		}

		seg.setPoints(p0, p1, p2, i);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype._buildSmoothPoints = function() {
	var segments = this._segments;
	var datapoints = this._datapoints;
	var gc = this._graphContext;
	var stats = this._stats;
	var range = stats.max-stats.min;
	var skipNull = gc.settings.getBool('LineSkipNulls');
	var n1 = { x: 0, y: 0 };
	var xCount = segments.length;
	var storedPoints = [];
	var i, j, seg, curve, firstWasNull, n0, x0, x1, x2, dv0, dv1, dv2, p0, p1, p2, max, setPoints;

	for ( i = 0 ; i < xCount ; ++i ) {
		j = i+1;
		seg = segments[i];

		x0 = i;
		x1 = i+1;
		x2 = i+2;

		dv0 = datapoints[Math.max(0, j-1)].yValue;
		dv1 = datapoints[Math.min(j, xCount-1)].yValue;
		dv2 = datapoints[Math.min(j+1, xCount-1)].yValue;

		if ( skipNull ) {
			firstWasNull = dv0.isNothing;

			if ( dv0.isNothing ) {
				dv0 = dv1;
				x0 = x1;
				n1 = { x: 0, y: 0 };
				seg.setActive(false);
			}

			if ( dv1.isNothing ) {
				dv1 = dv2 = dv0;
				x1 = x2 = x0;
				n1 = { x: 0, y: 0 };
			}

			if ( dv2.isNothing ) {
				dv2 = dv1;
				x2 = x1;
			}
		}

		p0 = { x: x0, y: 1-(dv0.numberValue-stats.min)/range };
		p1 = { x: x1, y: 1-(dv1.numberValue-stats.min)/range };
		p2 = { x: x2, y: 1-(dv2.numberValue-stats.min)/range };

		n0 = { x: -n1.x, y: -n1.y };
		n1 = { x: (p0.x-p2.x)/7, y: (p0.y-p2.y)/7 };

		max = (i == 0 ? 11 : storedPoints.length+10);
		
		if ( i < xCount-1 ) {
			curve = new IDB.LineCurve(20, p0, n0, p1, n1);
			storedPoints = storedPoints.concat(curve.getPoints());
		}
		
		max = Math.min(max, storedPoints.length);
		setPoints = [];

		for ( var n = 0 ; n < max ; ++n ) {
			setPoints.push(storedPoints.shift());
		}

		if ( storedPoints[0] ) {
			setPoints.push(storedPoints[0]);
		}

		seg.setCurvePoints(setPoints, p0, i);

		if ( firstWasNull ) {
			storedPoints = [];
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype._buildRegression = function() {
	if ( this._datapoints.length <= 1 ) {
		this._graphContext.showWarning(IDB.GraphLang.get('LineWarningTitle'),
			IDB.GraphLang.get('LineWarning', [this._yAxis.axisName]));
			return ;
	}

	var sumX = 0;
	var sumY = 0;
	var sumXX = 0;
	var sumYY = 0;
	var sumXY = 0;
	var datapoints = this._datapoints;
	var n = datapoints.length;
	var x, y;
			
	for ( x = 0 ; x < n ; ++x ) {
		y = datapoints[x].yValue.numberValue;
		sumX += x;
		sumY += y;
		sumXX += x*x;
		sumYY += y*y;
		sumXY += x*y;
	}
			
	var m = (n*sumXY-sumX*sumY)/(n*sumXX-sumX*sumX);
	var b = (sumY-m*sumX)/n;
	
	var stats = this._stats;
	var range = stats.max-stats.min;
	var relY0 = (b-stats.min)/range;
	var relY1 = ((b+m*(this._segments.length-1))-stats.min)/range;
	
	var n2 = n*n;
	var s2x = sumXX/n - (sumX*sumX)/n2;
	var s2y = sumYY/n - (sumY*sumY)/n2;
	var s2xy = sumXY/n - (sumX*sumY)/n2;
	var r = s2xy/Math.pow(s2x*s2y, 0.5);

	this._regObj = {
		corr: r*r,
		m: m,
		b: b,
		relSlope: m/range,
		relY0: relY0,
		relY1: relY1
	};
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype._onDatapointsUpdated = function() {
	if ( !this._onRegLineFade ) {
		return;
	}

	var datapoints = this._datapoints;
	var fade = true;

	for ( var i in datapoints ) {
		if ( datapoints[i].getVisualState() != IDB.VS.FADED ) {
			fade = false;
			break;
		}
	}

	this._onRegLineFade(fade);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype.setOnRegressionLineFade = function(callback) {
	this._onRegLineFade = callback;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype.refreshDisplay = function() {
	return this._graphContext.refreshDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype.getSegmentCount = function() {
	return this._segments.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype.getSegment = function(index) {
	return this._segments[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype.getRegressionObject = function() {
	return this._regObj;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype.getRegressionFormattedValue = function(x) {
	var val = this._regObj.m*x + this._regObj.b;
	return this._graphContext.settings.formatNumber(val, this._yAxis.numFmt);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype.getYAxisIndex = function() { //for 3D mode
	return this._yAxisIndex;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxis.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('LineLabelOptions');
};

/*====================================================================================================*/
IDB.LineAxisView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxisView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();
	this._segments = [];

	var model = this._model;
	var segCount = model.getSegmentCount();
	var lblOpt = model.getLabelOptions();
	var i, segModel, seg, lbl;

	for ( i = 0 ; i < segCount ; ++i ) {
		segModel = model.getSegment(i);

		seg = new IDB.LineSegmentView(segModel);
		this._display.add(seg.getDisplay());

		this._segments[i] = seg;
	}

	if ( lblOpt.show ) {
		this._labelHold = new Kinetic.Group();
		this._display.add(this._labelHold);

		for ( i = 0 ; i < segCount ; ++i ) {
			segModel = model.getSegment(i);

			if ( !segModel.getActive() ) {
				continue;
			}

			lbl = new IDB.GraphLabel(lblOpt, segModel.getLabelText());
			lbl.rotateFromCenter(lblOpt.rotation);
			lbl.getDisplay().listening(false);
			this._labelHold.add(lbl.getDisplay());

			this._segments[i].setLabelRef(lbl);
		}
	}

	this._buildRegression();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxisView.prototype._buildRegression = function() {
	var model = this._model;

	if ( !model.getRegressionObject() ) {
		return;
	}
	
	var lblOpt = model.getLabelOptions();
	var segModel = model.getSegment(0);

	var lineConfig = {
		points: [],
		stroke: IDB.toRGBA(0x0, 0.2),
		strokeWidth: segModel.getThickness()
	};

	var regLblOpt = {
		size: lblOpt.size,
		color: lblOpt.color,
		bold: false,
		align: 'center'
	};

	if ( segModel.showShadow() ) {
		this._regLineShadow = new Kinetic.Line(lineConfig);
		this._display.add(this._regLineShadow);
	}

	lineConfig.stroke = segModel.getDatapoint().axisColor.css;
	lineConfig.opacity = 1;

	this._regLine = new Kinetic.Line(lineConfig);
	this._display.add(this._regLine);

	lineConfig.strokeWidth = 8;
	lineConfig.opacity = 0;

	this._regLineCover = new Kinetic.Line(lineConfig);
	this._regLineCover.on("mouseover mousemove tap", IDB.listener(this, '_onRegLineOver'));
	this._regLineCover.on("mouseout", IDB.listener(this, '_onRegLineOut'));
	this._display.add(this._regLineCover);
	
	this._regLabel = new IDB.GraphLabel(regLblOpt, 'hello!');
	this._regLabel.getDisplay().listening(false);
	this._regLabel.visible = false;
	this._display.add(this._regLabel.getDisplay());

	model.setOnRegressionLineFade(IDB.listener(this, '_onRegressionLineFade'));
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxisView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxisView.prototype.setSize = function(width, height) {
	var model = this._model;
	var segments = this._segments;
	var segCount = segments.length;
	var segW = width/segCount;
	var lblPos = model.getLabelOptions().position;
	var i, seg, segModel, segDisp, segX, relDot, lbl;

	for ( i = 0 ; i < segCount ; ++i ) {
		seg = segments[i];
		segModel = model.getSegment(i);
		segDisp = seg.getDisplay();
		segX = segW*(i+0.5);

		segDisp.x(segX);
		seg.setSize(segW, height);

		lbl = seg.getLabelRef();

		if ( lbl ) {
			relDot = segModel.getRelativeDotPoint();

			lbl.x = segX+relDot.x*segW;
			lbl.y = IDB.GraphLabel.clampYPosition(lbl, relDot.y*height-lblPos, height);
		}
	}

	if ( this._regLine ) {
		this._setRegressionSize(width, height);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxisView.prototype._setRegressionSize = function(width, height) {
	var model = this._model;
	var regObj = model.getRegressionObject();
	var segCount = this._segments.length;
	var segW = width/segCount;
	var startXIndex = 0; //LATER: start x index

	var px0 = -startXIndex*segW;
	var px1 = (segCount-startXIndex)*segW;
	var py0 = (1-regObj.relY0+regObj.relSlope/2)*height;
	var py1 = (1-regObj.relY1-regObj.relSlope/2)*height;
	
	if ( this._regLineShadow ) {
		var offset = 2.5;
		this._regLineShadow.points([px0+offset, py0+offset, px1+offset, py1+offset]);
	}

	var points = [px0, py0, px1, py1];
	
	this._regLine.points(points);
	this._regLineCover.points(points);

	this._regLineW = width;
	this._regLineX0 = startXIndex-0.5;
	this._regLineX1 = (segCount-startXIndex-1)+0.5;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxisView.prototype._onRegressionLineFade = function(fade) {
	this._regLine.opacity(fade ? 0.2 : 1.0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxisView.prototype._onRegLineOver = function(event) {
	event = (event.evt ? event.evt : event); //supports either Kinetic version

	var model = this._model;
	var pos = this._display.getAbsolutePosition();

	var valX = (event.layerX-pos.x)/this._regLineW;
	valX = valX*(this._regLineX1-this._regLineX0)+this._regLineX0;

	this._regLabel.width = 1000;
	this._regLabel.height = 1000;
	this._regLabel.text = 'x='+Math.round(valX*100)/100+',\n'+
		'y='+model.getRegressionFormattedValue(valX);
	this._regLabel.width = this._regLabel.textWidth+5;
	this._regLabel.height = model.getLabelOptions().size*2.5;

	var px = event.layerX-pos.x-this._regLabel.width/2;
	var py = event.layerY-pos.y-this._regLabel.height;

	px = IDB.MathUtil.clamp(px, 0, this._regLineW-this._regLabel.width);
	py = Math.max(0, py);

	this._regLabel.x = px;
	this._regLabel.y = py;
	this._regLabel.visible = true;
	
	model.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineAxisView.prototype._onRegLineOut = function() {
	this._regLabel.visible = false;
	this._model.refreshDisplay();
};

/*====================================================================================================*/
IDB.LineCurve = function(stepCount, v0, n0, v1, n1) {
	var p0 = v0;
	var p1 = { x: v0.x+n0.x, y: v0.y+n0.y };
	var p2 = { x: v1.x+n1.x, y: v1.y+n1.y };
	var p3 = v1;
	
	var dx = p0.x;
	var cx = 3*(p1.x-dx);
	var bx = 3*(p2.x-p1.x)-cx;
	var ax = p3.x-dx-cx-bx;
			
	var dy = p0.y;
	var cy = 3*(p1.y-dy);
	var by = 3*(p2.y-p1.y)-cy;
	var ay = p3.y-dy-cy-by;

	this._points = [];

	var i, t, t2, t3;
	
	for ( i = 0 ; i <= stepCount ; ++i ) {
		t = i/stepCount;
		t2 = t*t;
		t3 = t2*t;
		this._points[i] = { x: ax*t3+bx*t2+cx*t+dx, y: ay*t3+by*t2+cy*t+dy };
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineCurve.prototype.getPoints = function() {
	return this._points;
};

/*====================================================================================================*/
IDB.LineGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	this._info.legendStyle = IDB.LS.AXES;
};

IDB.LineGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.LineGraph.prototype.constructor = IDB.LineGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineGraph.prototype._build = function() {
	this._grid = new IDB.CartesianGrid(this._graphContext);
	this._plot = new IDB.LinePlot(this._graphContext, this._grid.getAxes());
	
	this._limitHold = new Kinetic.Group();

	this._gridView = new IDB.CartesianGridView(this._grid, this._limitHold);
	this._stageLayer.add(this._gridView.getDisplay());
	
	this._plotView = new IDB.LinePlotView(this._plot);
	this._stageLayer.add(this._plotView.getDisplay());
	
	this._stageLayer.add(this._limitHold);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineGraph.prototype._update = function(width, height) {
	var gv = this._gridView;
	var pv = this._plotView;
	var pvd = pv.getDisplay();

	gv.setSize(width, height);
	
	pvd.x(gv.getPlotPosX());
	pvd.y(gv.getPlotPosY());
	pv.setSize(gv.getPlotWidth(), gv.getPlotHeight());
	
	pvd.clip({
		x: 0,
		y: -3,
		width: gv.getPlotWidth(),
		height: gv.getPlotHeight()+6
	});
};

/*====================================================================================================*/
IDB.LinePlot = function(graphContext, cartesianGridAxes, startYIndex, yAxisDisplayCount) {
	this._graphContext = graphContext;
	this._graphContext.addDatapointsUpdatedListener(IDB.listener(this, '_onDatapointsUpdated'));
	this._cartesianGridAxes = cartesianGridAxes;
	this._startYIndex = IDB.ifUndef(startYIndex, 0);
	
	this._onActiveYAxisChange = null;
	this._yYAxisDisplayCount = IDB.ifUndef(yAxisDisplayCount, -1);

	var sett = graphContext.settings;
	var is3D = sett.getBool('Is3dGraph');

	if ( !is3D ) {
		var lo = sett.get('LineLabelOptions');
		lo.position = sett.get('LineLabelPos');
		lo.rotation = sett.get('LineLabelRotation');
	}

	this._axisMap = {};
	this._eqLabels = null;
	this._axes = this._buildAxes();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype._buildAxes = function() {
	var axes = [];
	var gc = this._graphContext;
	var sett = gc.settings;
	var dpGrid = gc.datapointGrid;
	var cartGridAxes = this._cartesianGridAxes;
	var yAxisCount = dpGrid.getNumYCols(true);
	var is3D = sett.getBool('Is3dGraph');

	if ( this._yYAxisDisplayCount == -1 ) {
		this._yYAxisDisplayCount = yAxisCount;
	}
	
	if ( this._yYAxisDisplayCount == 0 ) {
		return axes;
	}

	var startY = this._startYIndex;
	var displayYCount = this._yYAxisDisplayCount;
	var axis = null;
	var i, pa, reg;

	if ( !is3D && sett.getBool('ScatterRegressionLine') && sett.getBool('ScatterEquationsShow') ) {
		this._eqLabels = [];
	}
	
	for ( i = 0 ; i < yAxisCount ; ++i ) {
		if ( i < startY || i-startY >= displayYCount ) {
			continue;
		}
		
		axis = dpGrid.getYAxis(i, true);

		pa = new IDB.LineAxis(gc, cartGridAxes.getYGridAxisById(axis.axisId), i);
		this._axisMap[axis.axisId] = axes.length;
		axes.push(pa);

		if ( !this._eqLabels ) {
			continue;
		}

		reg = pa.getRegressionObject();

		this._eqLabels.push({
			show: true,
			size: sett.get('ScatterEquationsSize'),
			color: axis.axisColor,
			bold: false,
			text: "r\u00b2 = "+Math.round(reg.corr*10000)/10000+', '+
				'y = '+sett.formatNumber(reg.m, axis.numFmt)+'x '+
				(reg.b < 0 ? '- ' : '+ ')+
				sett.formatNumber(Math.abs(reg.b), axis.numFmt)

		});
	}

	this._currYAxisId = axis.axisId;
	return axes;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype._onDatapointsUpdated = function() {
	var dpGrid = this._graphContext.datapointGrid;
	var rowCount = dpGrid.getNumXRows();
	var colCount = dpGrid.getNumYCols(true);
	var startY = this._startYIndex;
	var i, j, dp;

	// Find the highlighted Y-axis with the highest axisId

	for ( j = colCount-1 ; j >= startY ; --j ) {
		for ( i = 0 ; i < rowCount ; ++i ) {
			dp = dpGrid.getDatapointAt(i, j, true);

			if ( !dp) {
                IDB.log("Datapoint not found at row " + i + ", col " + j + ", startY=" + startY);
				continue;
			}


			if ( !dp || dp.getVisualState() != IDB.VS.HIGHLIGHTED ) {
				continue;
			}

			this._setCurrYAxisId(dp.yAxis.axisId);
			return;
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype.getAxisCount = function() {
	return this._axes.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype.getAxis = function(index) {
	return this._axes[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype.getAxisIndexById = function(axisId) {
	return this._axisMap[axisId];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype.getEquationCount = function() {
	return (this._eqLabels ? this._eqLabels.length : 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype.getEquationLabelOptions = function(index) {
	return this._eqLabels[index];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype._setCurrYAxisId = function(axisId) {
	if ( this._onActiveYAxisChange ) {
		this._onActiveYAxisChange(this._currYAxisId, axisId);
	}

	this._currYAxisId = axisId;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlot.prototype.setOnActiveYAxisChange = function(callback) {
	this._onActiveYAxisChange = callback;
};

/*====================================================================================================*/
IDB.LinePlotView = function(model) {
	this._model = model;
	this._model.setOnActiveYAxisChange(IDB.listener(this, '_onActiveYAxisChange'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlotView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();
	this._axes = [];

	var plotModel = this._model;
	var axisCount = plotModel.getAxisCount();
	var regValCount = plotModel.getEquationCount();
	var i, axisModel, axis, lblOpt, lbl;

	for ( i = 0 ; i < axisCount ; ++i ) {
		axisModel = plotModel.getAxis(i);

		axis = new IDB.LineAxisView(axisModel);
		this._display.add(axis.getDisplay());

		this._axes[i] = axis;
	}

	if ( !regValCount ) {
		return;
	}

	this._eqLabels = [];

	for ( i = 0 ; i < regValCount ; ++i ) {
		lblOpt = plotModel.getEquationLabelOptions(i);

		lbl = new IDB.GraphLabel(lblOpt, lblOpt.text);
		this._display.add(lbl.getDisplay());
		this._eqLabels.push(lbl);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlotView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlotView.prototype.setSize = function(width, height) {
	var axes = this._axes;
	var labels = this._eqLabels;
	var i, axis, lbl;

	for ( i in axes ) {
		axis = axes[i];
		axis.setSize(width, height);
	}

	if ( !labels ) {
		return;
	}

	for ( i in labels ) {
		lbl = labels[i];
		lbl.x = 5;
		lbl.y = height-(lbl.height+5)*(labels.length-i);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LinePlotView.prototype._onActiveYAxisChange = function(prevId, currId) {
	var index = this._model.getAxisIndexById(currId);
	var curr = this._axes[index];
	curr.getDisplay().moveToTop();
};

/*====================================================================================================*/
IDB.LineSegment = function(graphContext, datapoint) {
	this._graphContext = graphContext;
	this._datapoint = datapoint;
	this._labelText = datapoint.yValue.getFV(true);
	this._active = true;
	this._points = null;
	this._dotPoint = null;
	this._lineColor = datapoint.axisColor;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.setPoints = function(p0, p1, p2, offsetX) {
	this._points = [
		{ x: p0.x-offsetX, y: p0.y },
		{ x: p1.x-offsetX, y: p1.y },
		{ x: p2.x-offsetX, y: p2.y }
	];

	this._dotPoint = this._points[1];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.setCurvePoints = function(curvePoints, dotPoint, offsetX) {
	this._dotPoint = { x: dotPoint.x-offsetX, y: dotPoint.y };
	this._points = [];
	var i, cp, p;

	for ( i in curvePoints ) {
		cp = curvePoints[i];
		p = { x: cp.x-offsetX, y: cp.y };
		this._points.push(p);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.setActive = function(active) {
	this._active = active;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.setLineColor = function(rgb) {
	this._lineColor = rgb;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.setLabelText = function(text) {
	this._labelText = text;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.setLabelText = function(text) {
	this._labelText = text;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getActive = function() {
	return this._active;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getDatapoint = function() {
	return this._datapoint;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getRelativePoints = function() {
	return this._points;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getRelativeDotPoint = function() {
	return this._dotPoint;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getLineColor = function() {
	return this._lineColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getLabelText = function() {
	return this._labelText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.showLines = function() {
	return (!this._graphContext.settings.getBool('ScatterMode'));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getThickness = function() {
	return this._graphContext.settings.get('LineThickness');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getDotSize = function() {
	return this._graphContext.settings.get('LineDotSize');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.showShadow = function() {
	return this._graphContext.settings.getBool('LineShadow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getFillAlpha = function() { //for 3D graphs
	return this._graphContext.settings.getAlpha('LineFillTransparency');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getLabelOptions = function() { //for 3D graphs
	return this._graphContext.settings.get('LineLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.getCameraHorizRotation = function() { //for 3D graphs
	return this._graphContext.settings.get('CameraHorizRotation');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, "_handleClick");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.createOverListener = function() {
	return IDB.listener(this, "_handleOver");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype.createOutListener = function() {
	return IDB.listener(this, "_handleOut");
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype._handleClick = function() {
	this._graphContext.onDatapointClicked(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype._handleOver = function() {
	this._graphContext.onDatapointOver(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegment.prototype._handleOut = function() {
	this._graphContext.onDatapointOut(this._datapoint);
};

/*====================================================================================================*/
IDB.LineSegmentView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegmentView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	if ( !model.getActive() ) {
		return;
	}

	var shadColor = 'rgba(0, 0, 0, 0.2)';
	var axisColor = model.getLineColor().css;

	this._labelRef = null;

	if ( model.showLines() ) {
		var lineConfig = {
			points: [],
			stroke: shadColor,
			strokeWidth: model.getThickness(),
			lineJoin: 'round',
			lineCap: 'butt'
		};

		if ( model.showShadow() ) {
			this._lineShadow = new Kinetic.Line(lineConfig);
			this._display.add(this._lineShadow);
		}
	
		lineConfig.stroke = axisColor;

		this._line = new Kinetic.Line(lineConfig);
		this._display.add(this._line);

		lineConfig.strokeWidth += 1;
		lineConfig.visible = false;

		this._lineHigh = new Kinetic.Line(lineConfig);
		this._display.add(this._lineHigh);

		lineConfig.strokeWidth = 8;
		lineConfig.opacity = 0;
		lineConfig.visible = true;
	
		this._lineCover = new Kinetic.Line(lineConfig);
		this._lineCover.on("click", model.createClickListener());
		this._lineCover.on("mouseover", model.createOverListener());
		this._lineCover.on("mouseout", model.createOutListener());
        this._lineCover._touchHandler = new IDB.TouchHandler(this._lineCover);
		this._display.add(this._lineCover);
	}

	if ( model.getDotSize() > 0 ) {
		var dotConfig = {
			fill: shadColor,
			radius: model.getDotSize()/2
		};

		if ( model.showShadow() ) {
			this._dotShadow = new Kinetic.Circle(dotConfig);
			this._display.add(this._dotShadow);
		}

		dotConfig.fill = axisColor;

		this._dot = new Kinetic.Circle(dotConfig);
		this._dot.on("click", model.createClickListener());
		this._dot.on("mouseover", model.createOverListener());
		this._dot.on("mouseout", model.createOutListener());
        this._dot._touchHandler = new IDB.TouchHandler(this._dot);
		this._display.add(this._dot);

		dotConfig.radius += 0.5;
		dotConfig.visible = false;

		this._dotHigh = new Kinetic.Circle(dotConfig);
		this._dotHigh.listening(false);
		this._display.add(this._dotHigh);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegmentView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegmentView.prototype.getLabelRef = function() {
	return this._labelRef;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegmentView.prototype.setLabelRef = function(labelRef) {
	this._labelRef = labelRef;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegmentView.prototype.setSize = function(width, height) {
	var model = this._model;
	
	if ( !model.getActive() ) {
		return;
	}

	var relPoints = model.getRelativePoints();
	var relDotPoint = this._model.getRelativeDotPoint();
	var dotPoint = { x: relDotPoint.x*width, y: relDotPoint.y*height };
	var points = [];
	var i, rp;

	for ( i in relPoints ) {
		rp = relPoints[i];
		points.push(rp.x*width, rp.y*height);
	}

	if ( this._line ) {
		this._line.points(points);
		this._lineHigh.points(points);
		this._lineCover.points(points);
	}

	if ( this._lineShadow ) {
		this._lineShadow.points(points);
	}

	if ( this._dot ) {
		this._dot.position(dotPoint);
		this._dotHigh.position(dotPoint);
	}

	var offset = 2.5;
	
	if ( this._lineShadow ) {
		this._lineShadow.position({ x: offset, y: offset });
		this._lineShadow.points(points);
	}

	if ( this._dotShadow ) {
		dotPoint.x += offset;
		dotPoint.y += offset;
		this._dotShadow.position(dotPoint);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.LineSegmentView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getDatapoint().getVisualState();
	var isHigh = (state == IDB.VS.HIGHLIGHTED);
	var isFade = (state == IDB.VS.FADED);
	var alpha = (isFade ? 0.2 : 1.0);

	if ( this._line ) {
		this._line.opacity(alpha);
		this._lineHigh.visible(isHigh);
	}

	if ( this._dot ) {
		this._dot.opacity(alpha);
		this._dotHigh.visible(isHigh);
	}
	
	if ( this._lineShadow ) {
		this._lineShadow.opacity(alpha);
	}
	
	if ( this._dotShadow ) {
		this._dotShadow.opacity(alpha);
	}
	
	if  ( this._labelRef ) {
		this._labelRef.visible = !isFade;
	}
};

/*====================================================================================================*/

