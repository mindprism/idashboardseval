'use strict';
IDB.CalendarData = function(graphContext) {
	this._graphContext = graphContext;
	this._buildGroups();
	this._updateSettings();
	this._buildBaseDate();
	this._buildAxisShapeMap();
	this._buildWeekendList();
	this._findHolidayAxis();
};

IDB.CalendarData.PosDataEnd = 0;
IDB.CalendarData.PosDataStart = 1;
IDB.CalendarData.PosYesterday = 2;
IDB.CalendarData.PosToday = 3;
IDB.CalendarData.PosTomorrow = 4;
IDB.CalendarData.PosSunday = 5;
IDB.CalendarData.PosMonday = 6;
IDB.CalendarData.PosTuesday = 7;
IDB.CalendarData.PosWednesday = 8;
IDB.CalendarData.PosThursday = 9;
IDB.CalendarData.PosFriday = 10;
IDB.CalendarData.PosSaturday = 11;
IDB.CalendarData.PosCustomDate = 12;

IDB.CalendarData.ShapeRectangle = 0;
IDB.CalendarData.ShapeEllipse = 1;
IDB.CalendarData.ShapeDiamond = 2;
IDB.CalendarData.ShapeTriangle = 3;
IDB.CalendarData.ShapeInvertedTriangle = 4;
IDB.CalendarData.ShapeDollarSign = 5;
IDB.CalendarData.ShapeEuroSign = 6;
IDB.CalendarData.ShapeYenSign = 7;
IDB.CalendarData.ShapePoundSign = 8;
IDB.CalendarData.ShapeSpadeSign = 9;
IDB.CalendarData.ShapeClubSign = 10;
IDB.CalendarData.ShapeHeartSign = 11;
IDB.CalendarData.ShapeMusicSign = 12;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype._buildGroups = function() {
	this._minDateStr = '9999-99-99';
	this._maxDateStr = '0000-00-00';
	
	var gc = this._graphContext;
	var dg = gc.datapointGrid;
	var numX = dg.getNumXRows();
	var daily = (gc.settings.get('CalMode') == IDB.CalendarGrid.ModeDaily);
	var groupRows = {};
	var i, dp, dateParts, key, dateStr;
	var boundsSet = false;
	
	for ( i = 0 ; i < numX ; ++i ) {
		dp = dg.getDatapointAt(i, 0, false);
		dateParts = (dp.xValue.stringValue).split(' ');
		dateStr = dateParts[0];
		
		if ( daily && dateParts.length > 1 ) {
			key = dateStr+' '+(dateParts[1].substring(0, 2));
		}
		else {
			key = dateStr;
		}
		
		if ( !key ) {
			continue;
		}

		if ( dateStr < this._minDateStr ) {
			this._minDateStr = dateStr;
		}

		if ( dateStr > this._maxDateStr ) {
			this._maxDateStr = dateStr;
		}

		boundsSet = true;
		
		if ( !groupRows[key] ) {
			groupRows[key] = [];
		}

		groupRows[key].push(dp.datapointRow);
	}

	this._groupRows = groupRows;
	
	if ( !numX || !boundsSet ) {
		this._minDateStr = null;
		this._maxDateStr = null;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype._updateSettings = function() {
	var gc = this._graphContext;
	var sett = gc.settings;

	if ( !sett.get('CalMonthLabelFormat') ) {
		sett.set('CalMonthLabelFormat', 'MMM yy');
	}
	
	if ( !sett.get('CalDayLabelFormat') ) {
		sett.set('CalDayLabelFormat', 'MM/dd/yyyy');
	}

	sett.set('CalMCols', IDB.MathUtil.clamp(sett.get('CalMCols'), 1, 12));
	sett.set('CalMRows', IDB.MathUtil.clamp(sett.get('CalMRows'), 1, 12));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype._buildBaseDate = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var shiftDay = -1;
	var dateParts, dateStr;

	this._baseDate = new Date();
	
	switch ( sett.get('CalBasePosition') ) {
		case IDB.CalendarData.PosToday:
			return;
			
		case IDB.CalendarData.PosTomorrow:
			this._baseDate.setDate(this._baseDate.getDate()+1);
			return;
			
		case IDB.CalendarData.PosYesterday:
			this._baseDate.setDate(this._baseDate.getDate()-1);
			return;
			
		case IDB.CalendarData.PosDataStart:
			dateStr = this._minDateStr;

			if ( dateStr ) {
				dateParts = dateStr.split('-');

				if ( dateParts ) {
					this._baseDate = new Date(dateParts[1]+'/'+dateParts[2]+'/'+dateParts[0]);
				}
			}

			return;
			
		case IDB.CalendarData.PosDataEnd:
			dateStr = this._maxDateStr;

			if ( dateStr ) {
				dateParts = dateStr.split('-');
				if ( dateParts ) {
					this._baseDate = new Date(dateParts[1]+'/'+dateParts[2]+'/'+dateParts[0]);
				}
			}

			return;
			
		case IDB.CalendarData.PosSunday:
			shiftDay = 0;
			break;
			
		case IDB.CalendarData.PosMonday:
			shiftDay = 1;
			break;
			
		case IDB.CalendarData.PosTuesday:
			shiftDay = 2;
			break;
			
		case IDB.CalendarData.PosWednesday:
			shiftDay = 3;
			break;
			
		case IDB.CalendarData.PosThursday:
			shiftDay = 4;
			break;
			
		case IDB.CalendarData.PosFriday:
			shiftDay = 5;
			break;
			
		case IDB.CalendarData.PosSaturday:
			shiftDay = 6;
			break;
			
		case IDB.CalendarData.PosCustomDate:
			this._baseDate = this._getValidCustomDate(sett.get('CalBaseDate'));
			
			if ( this._baseDate == null ) {
				this._baseDate = new Date();
				gc.showWarning(IDB.GraphLang.get('CalendarBaseWarningTitle'),
					IDB.GraphLang.get('CalendarBaseWarning'));
			}

			return;
	}
	
	if ( shiftDay != -1 ) {
		var baseTypeDir = (sett.get('CalBaseType') ? 1 : -1);

		while ( this._baseDate.getDay() != shiftDay ) {
			this._baseDate.setDate(this._baseDate.getDate()+baseTypeDir);
		}
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype._getValidCustomDate = function(dateStr) {
	var parts = dateStr.split('-');
	
	if ( parts.length != 3 ) {
		return null;
	}

	if ( parts[0].length != 4 ) { //yyyy
		return null;
	}

	if ( parts[1].length != 1 && parts[1].length != 2 ) { //mm
		return null;
	}

	if ( parts[2].length != 1 && parts[2].length != 2 ) { //dd
		return null;
	}

	var date = new Date(parts[1]+'/'+parts[2]+'/'+parts[0]);
	
	if ( date.getFullYear() != Number(parts[0]) ) {
		return null;
	}

	if ( date.getMonth()+1 != Number(parts[1]) ) {
		return null;
	}

	if ( date.getDate() != Number(parts[2]) ) {
		return null;
	}

	return date;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype._buildAxisShapeMap = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var pairsStr = sett.get('CalMetricShapes');
	var pairs = IDB.StringUtils.parseTokens(pairsStr, '|', '/');

	if ( !pairs.length ) {
		return;
	}
	
	var map = {};
	var i, pair;
	
	for ( i = 0 ; i < pairs.length ; ++i ) {
		pair = IDB.StringUtils.parseTokens(pairs[i], '=', '$');
		map[pair[0]] = Number(pair[1]);
	}
	
	this._axisShapeMap = map;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype._buildWeekendList = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var days = (sett.get('CalWeekendDays') || '');

	while ( days.length < 7 ) {
		//days += (i == 0 || i == 6 ? '1' : '0'); //this is in the AS3 code, but "i" is not defined
		days += '0';
	}
	
	var list = days.split('');
	var len = list.length;

	for ( var i = 0 ; i < len ; ++i ) {
		list[i] = (list[i] == '1');
	}

	this._weekendList = list;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype._findHolidayAxis = function() {
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;

	this._holidayAxisId = -1;
	
	var n = dpGrid.getNumYCols(false);
	var i, axis;

	for ( i = 0 ; i < n ; ++i ) {
		axis = dpGrid.getYAxis(i,false);

		if ( axis.axisName.toLowerCase() == 'isholiday' ) {
			this._holidayAxisId = axis.axisId;
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.getNow = function() {
	return new Date();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.getBaseDate = function() {
	return this._baseDate;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.getMinDateString = function() {
	return this._minDateStr;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.getMaxDateString = function() {
	return this._maxDateStr;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.getMetricShape = function(axisName) {
	if ( !this._axisShapeMap ) {
		return null;
	}

	return this._axisShapeMap[axisName];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.isWeekendDay = function(dayIndex) {
	return this._weekendList[dayIndex];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.getHolidayAxisId = function() {
	return this._holidayAxisId;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.getRowsByDate = function(date) {
	var dateStr = this._toDateString(date);
	return this._groupRows[dateStr];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype.getRowsByDatetime = function(date, hour) {
	var dateStr = this._toDateString(date)+' '+(hour < 10 ? '0' : '')+hour;
	return this._groupRows[dateStr];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarData.prototype._toDateString = function(date) {
	var m = date.getMonth()+1;
	var d = date.getDate();
	return date.getFullYear()+'-'+(m < 10 ? '0' : '')+m+'-'+(d < 10 ? '0' : '')+d;
};

/*====================================================================================================*/
IDB.CalendarDay = function(graphContext, calendarData, indexOffset) {
	this._graphContext = graphContext;
	this._calendarData = calendarData;
	this._indexOffset = indexOffset;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var calData = this._calendarData;
	var now = calData.getNow();

	var date = new Date(calData.getBaseDate());
	date.setDate(date.getDate()+this._indexOffset);

	this._isToday = (now.getFullYear() == date.getFullYear() &&
		now.getMonth() == date.getMonth() && now.getDate() == date.getDate());
	this._titleText = IDB.DateUtil.format(date, sett.get('CalDayLabelFormat'));
	this._items = new IDB.CalendarItems(gc, calData, date, null, false);

	if ( this._items.isHoliday() ) {
		this._bgColor = sett.get('CalCellHolidayColor');
	}
	else {
		this._bgColor = sett.get(calData.isWeekendDay(date.getDay()) ?
			'CalCellWkndColor' : 'CalCellColor');
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getActiveDatapoints = function() {
	return this._items.getActiveDatapoints();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getIndexOffset = function() {
	return this._indexOffset;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getItems = function() {
	return this._items;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getTitleText = function() {
	return this._titleText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getBackgroundColor = function() {
	return this._bgColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.isToday = function() {
	return this._isToday;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getTitleLabelOptions = function() {
	return this._graphContext.settings.get('CalTitleOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.showHeaderShading = function() {
	return this._graphContext.settings.getBool('CalHeaderShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getHeaderColor = function() {
	return this._graphContext.settings.get('CalHeaderColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getGridColor = function() {
	return this._graphContext.settings.get('CalGridColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getTodayHighlightColor = function() {
	return this._graphContext.settings.get('CalTodayHighColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDay.prototype.getScrollerColor = function() {
	return this._graphContext.settings.get('CalCellScrollerColor');
};

/*====================================================================================================*/
IDB.CalendarDayItemsView = function(model, showShadedBg) {
	this._model = model;
	this._showShadedBg = showShadedBg;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayItemsView.prototype._build = function() {
	var model = this._model;
	var boxCount = model.getBoxCount();
	var textCount = model.getTextCount();
	var rangeColor = (model.useMetricsBg() ? model.getRangeColor() : null);
	var i, box, text;

	this._display = new Kinetic.Group();
	this._boxes = [];
	this._texts = [];

	this._bg = new Kinetic.Rect({
		fill: (rangeColor ? rangeColor.css : 'rgba(0, 0, 0, 0.05)'),
		visible: (rangeColor || this._showShadedBg)
	});
	this._display.add(this._bg);
	
	this._line = new Kinetic.Line({
		points: [],
		stroke: model.getCellLabelOptions().color.css,
		strokeWidth: 1,
		opacity: 0.2
	});
	this._display.add(this._line);

	for ( i = 0 ; i < boxCount ; ++i ) {
		box = new IDB.CalendarDayNumItemView(model.getBox(i));
		this._display.add(box.getDisplay());
		this._boxes.push(box);
	}

	for ( i = 0 ; i < textCount ; ++i ) {
		text = new IDB.CalendarDayTextItemView(model.getText(i));
		this._display.add(text.getDisplay());
		this._texts.push(text);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayItemsView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayItemsView.prototype.getTrueHeight = function() {
	return this._trueH;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayItemsView.prototype.getResetY = function() {
	return 0;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayItemsView.prototype.setSize = function(width, height) {
	var model = this._model;
	var boxes = this._boxes;
	var texts = this._texts;
	var lblSize = model.getCellLabelOptions().size;
	var py = 2;
	var lineY = 0;
	var i, text, box;
	
	for ( i = 0 ; i < boxes.length ; ++i ) {
		box = boxes[i];
		box.setSize(width-6, lblSize);
		box.getDisplay()
			.x(3)
			.y(py);

		py += lblSize+4;
	}
	
	if ( i ) {
		lineY = py;
		py += 3;
	}
	
	for ( i = 0 ; i < texts.length ; ++i ) {
		text = texts[i];
		text.setSize(width-6, lblSize);
		text.getDisplay()
			.x(3)
			.y(py);

		py += Math.round(text.getTextHeight())+3;
	}
	
	this._trueH = py;
	
	this._bg
		.width(width)
		.height(Math.max(height, this._trueH));
	
	this._line
		.visible(lineY != 0)
		.points([
			3, lineY,
			width-6, lineY
		]);
};

/*====================================================================================================*/
IDB.CalendarDayNumItemView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayNumItemView.prototype._build = function() {
	var model = this._model;
	var showItem = model.showItem();

	this._origSize = 100;

	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());
	this._touchHandler = new IDB.TouchHandler(this._display);

	this._bg = new Kinetic.Rect({
		width: this._origSize,
		height: this._origSize,
		fill: 'rgba(0, 0, 0, 0.001)'
	});
	this._display.add(this._bg);

	if ( showItem ) {
		this._shape = this._buildShape();
		this._shape.listening(false);
		this._display.add(this._shape);
	}
	
	var lblOpt = model.getCellLabelOptions();
	lblOpt.align = 'left';

	var text = (model.showAxisName() ? model.getAxisName()+': ' : '')+
		(showItem ? model.getFormattedValue() : '');

	this._label = new IDB.GraphLabel(lblOpt, text);
	this._label.getDisplay().listening(false);
	this._display.add(this._label.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayNumItemView.prototype._buildShape = function() {
	var model = this._model;
	var help = new IDB.CalendarViewHelper(this._origSize, model.getRangeColor());

	switch ( model.getShape() ) {
		case IDB.CalendarItem.ShapeRectangle:
			return help.buildRectangle();
			
		case IDB.CalendarItem.ShapeEllipse:
			return help.buildEllipse();
			
		case IDB.CalendarItem.ShapeDiamond:
			return help.buildDiamond();
			
		case IDB.CalendarItem.ShapeTriangle:
			return help.buildTriangle(false);
			
		case IDB.CalendarItem.ShapeTriangleInverted:
			return help.buildTriangle(true);
			
		case IDB.CalendarItem.ShapeDollar:
			return help.buildDollarLabel().getDisplay();

		case IDB.CalendarItem.ShapeEuro:
			return help.buildEuroLabel().getDisplay();
			
		case IDB.CalendarItem.ShapeYen:
			return help.buildYenLabel().getDisplay();
		
		case IDB.CalendarItem.ShapePound:
			return help.buildPoundLabel().getDisplay();
		
		case IDB.CalendarItem.ShapeSpade:
			return help.buildSpadeLabel().getDisplay();
		
		case IDB.CalendarItem.ShapeClub:
			return help.buildClubLabel().getDisplay();
		
		case IDB.CalendarItem.ShapeHeart:
			return help.buildHeartLabel().getDisplay();
		
		case IDB.CalendarItem.ShapeMusic:
			return help.buildMusicLabel().getDisplay();
	}
	
	console.log('CalendarDayNumItemView: Unknown Shape '+model.getShape());
	return help.buildRectangle();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayNumItemView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayNumItemView.prototype.setSize = function(width, height) {
	var model = this._model;
	var lbl = this._label;
	var size = model.getShapeSize();

	this._bg
		.y(-2)
		.width(width)
		.height(height+6);

	if ( this._shape ) {
		var pos = ((1-size)*height)/2;
		var scale = height*size/this._origSize;

		this._shape
			.x(pos)
			.y(pos)
			.scaleX(scale)
			.scaleY(scale);
	}

	lbl.x = height+3;
	lbl.y = (height-lbl.textHeight)/2+0.5;
	lbl.width = width-lbl.x;
	lbl.truncate();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayNumItemView.prototype._onDatapointsUpdated = function() {
	this._display.opacity(this._model.getVisualState() == IDB.VS.FADED ? 0.2 : 1.0);
};

/*====================================================================================================*/
IDB.CalendarDayTextItemView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayTextItemView.prototype._build = function() {
	var model = this._model;
	var showItem = model.showItem();

	this._origSize = 100;

	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());
	this._touchHandler = new IDB.TouchHandler(this._display);

	this._bg = new Kinetic.Rect({
		width: this._origSize,
		height: this._origSize,
		fill: 'rgba(0, 0, 0, 0.001)'
	});
	this._display.add(this._bg);

	this._shape = new Kinetic.Circle({
		radius: 50,
		fill: model.getAxisColor().css,
		listening: false
	});
	this._display.add(this._shape);
	
	var lblOpt = model.getCellLabelOptions();
	lblOpt.align = 'left';

	var text = (model.showAxisName() ? model.getAxisName()+': ' : '')+
		(showItem ? model.getFormattedValue() : '');

	this._label = new IDB.GraphLabel(lblOpt, text);
	this._label.wordWrap = true;
	this._label.getDisplayLabel()
		.lineHeight(1.2)
		.listening(false);
	this._display.add(this._label.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayTextItemView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayTextItemView.prototype.getTextHeight = function() {
	return this._label.height;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayTextItemView.prototype.setSize = function(width, lineHeight) {
	var lbl = this._label;

	lbl.x = lineHeight;
	lbl.y = 0.5;
	lbl.width = width-lbl.x;

	this._bg
		.y(-3)
		.width(width)
		.height(lbl.height+4);

	this._shape
		.radius(lineHeight/4)
		.x(lineHeight/2)
		.y(lineHeight/2);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayTextItemView.prototype._onDatapointsUpdated = function() {
	this._display.opacity(this._model.getVisualState() == IDB.VS.FADED ? 0.2 : 1.0);
};

/*====================================================================================================*/
IDB.CalendarDayView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayView.prototype._buildKinetic = function() {
	var model = this._model;
	var titleLblOpt = model.getTitleLabelOptions();

	titleLblOpt.align = 'center';

	this._display = new Kinetic.Group();

	this._bg = new Kinetic.Rect({
		fill: model.getBackgroundColor().css,
		stroke: model.getGridColor().css,
		strokeWidth: 0.5
	});
	this._display.add(this._bg);
	
	this._header = new Kinetic.Rect({
		fill: model.getHeaderColor().css,
		stroke: model.getGridColor().css,
		strokeWidth: 1
	});
	this._display.add(this._header);

	this._title = new IDB.GraphLabel(titleLblOpt, model.getTitleText());
	this._display.add(this._title.getDisplay());

	if ( model.showHeaderShading() ) {
		this._shade = new IDB.GenericShading(IDB.GenericShading.TypeGloss, true);
		this._display.add(this._shade.getDisplay());
	}
	
	if ( model.isToday() ) {
		this._todayHigh = new Kinetic.Rect({
			stroke: model.getTodayHighlightColor().css,
			strokeWidth: 2.5
		});
		this._display.add(this._todayHigh);
	}
	
	this._items = new IDB.CalendarDayItemsView(model.getItems(), false);

	this._scroll = new IDB.CalendarScrollView(this._items, model.getScrollerColor());
	this._display.add(this._scroll.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarDayView.prototype.setSize = function(width, height) {
	if ( this._width == width && this._height == height ) {
		return;
	}

	this._width = width;
	this._height = height;

	var titleLbl = this._title;
	var headerH = Math.min(titleLbl.textHeight+8, height*0.18);
	
	this._header
		.x(0.5)
		.y(0.5)
		.width(width-1)
		.height(headerH-1);
	
	this._bg
		.x(0.5)
		.y(headerH+0.5)
		.width(width-1)
		.height(height-headerH-1);
	
	if ( this._shade ) {
		var size = IDB.GenericShading.FullDimension;
		
		this._shade.getDisplay()
			.scaleX(width/size)
			.scaleY(headerH/size);
	}

	if ( this._todayHigh ) {
		var t = 2.5;

		this._todayHigh
			.x(t/2+0.5)
			.y(headerH+t/2+0.5)
			.width(width-t-1)
			.height(height-headerH-t-1);
	}
	
	titleLbl.y = (headerH-titleLbl.textHeight)/2;
	titleLbl.width = width;
	titleLbl.truncate();

	this._scroll.setSize(width, height-headerH);
	this._scroll.getDisplay().y(headerH);
};

/*====================================================================================================*/
IDB.CalendarGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
};

IDB.CalendarGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.CalendarGraph.prototype.constructor = IDB.CalendarGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGraph.prototype._build = function() {
	var gc = this._graphContext;
	var noNumAxis = (gc.datapointGrid.getNumYCols(true) == 0);
	var data = new IDB.CalendarData(gc);

	this._info.numericYOnly = false;
	this._info.legendStyle = (noNumAxis ? IDB.LS.NONE : IDB.LS.RANGES);
	this._info.calloutStyle = 'custom';

	this._grid = new IDB.CalendarGrid(gc, data);
	this._grid.setOnIndexChange(IDB.listener(this, '_handleGridIndexChange'));

	this._gridView = new IDB.CalendarGridView(this._grid);
	this._stageLayer.add(this._gridView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGraph.prototype._update = function(width, height) {
	this._gridView.setSize(width, height);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGraph.prototype._handleGridIndexChange = function() {
	this._resetDatapointManager();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGraph.prototype._getRelevantDatapoints = function() {
	return this._grid.getActiveDatapoints();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGraph.prototype.updateCallout = function(datapoint, $callout) {
	if ( datapoint.yAxis.dataTypeIsString ) {
		this._updateTextCallout(datapoint, $callout);
		return;
	}

	this._updateMixedCallout(datapoint, $callout);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGraph.prototype._updateTextCallout = function(datapoint, $callout) {
	var dpList = datapoint.datapointRow.datapoints;
	var dpListLen = dpList.length;
	var showList = [];
	var showListLen = 0;
	var i, dp, $table, $row;
	
	for ( i = 0 ; i < dpListLen ; ++i ) {
		dp = dpList[i];
		
		if ( dp.yAxis.isHidden || dp.yAxis.dataTypeIsNumber || dp.yValue.isNothing ) {
			continue;
		}

		showList.push(dp);
		showListLen++;
	}

	// Create table and its header row.

	$table = $('<table>')
		.css('max-width', '200px')
		.appendTo($callout);

	$('<tr>')
		.append(
			$('<td colspan="1" class="idb-header">')
				.text(datapoint.xValue.formattedValue)
				.css('text-align', 'center')
		)
		.appendTo($table);

	// Create datapoint rows.

	for ( i = 0 ; i < showListLen ; ++i ) {
		dp = showList[i];

		$row = $('<tr>')
			.appendTo($table);

		if ( dp == datapoint && showListLen > 1 ) {
			$row.addClass('idb-callout-highlighted');
		}

		$('<td>')
			.text(dp.yAxis.axisName+': '+dp.yValue.formattedValue)
			.appendTo($row);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGraph.prototype._updateMixedCallout = function(datapoint, $callout) {
	var dpList = datapoint.datapointRow.datapoints;
	var dpListLen = dpList.length;
	//var maxRows = 11;
	var i, dp, $table, $row, $bullet;

	// Truncate the datapoint group (if necessary)

	/*if ( dpListLen > maxRows ) {
		var dpIndex = 0;

		for ( i = 0 ; i < dpListLen ; ++i ) {
			if ( dpList[i] == datapoint ) {
				dpIndex = i;
				break;
			}
		}

		var halfMax = Math.floor(maxRows/2);
		var startI = IDB.MathUtil.clamp(dpIndex-halfMax, 0, dpListLen-maxRows);

		dpList = dpList.slice(startI, startI+maxRows);
		dpListLen = dpList.length;
	}*/

	// Create table and its header row.

	$table = $('<table>')
		.css('max-width', '200px')
		.appendTo($callout);

	$('<tr>')
		.append(
			$('<td colspan="3" class="idb-header">')
				.text(datapoint.xValue.formattedValue)
				.css('text-align', 'center')
		)
		.appendTo($table);

	// Create datapoint rows.

	for ( i = 0 ; i < dpListLen ; ++i ) {
		dp = dpList[i];

		if ( dp.yAxis.isHidden ) {
			continue;
		}

		$row = $('<tr>')
			.appendTo($table);

		if ( dp == datapoint && dpListLen > 1 ) {
			$row.addClass('idb-callout-highlighted');
		}

		if ( dp.yAxis.dataTypeIsNumber ) {
			$bullet = $('<div>')
				.html('&nbsp;')
				.css('background-color', dp.rangeColor.css)
				.css('width', '15px');
		}
		else {
			$bullet = $('<div>')
				.html('&bull;')
				.css('color', dp.axisColor.css)
				.css('font-size', '30px')
				.css('text-align', 'center')
				.css('margin-top', '-21px')
				.css('width', '15px')
				.css('height', '15px');
		}

		$('<td>')
			.append($bullet)
			.appendTo($row);
			
		$('<td>')
			.text(dp.yAxis.axisName)
			.appendTo($row);

		$('<td>')
			.append(
				$('<div>')
					.text(dp.yValue.formattedValue)
					.css('text-align', 'right')
					.css('height', '18px')
					.css('overflow', 'hidden')
			)
			.appendTo($row);
	}
};

/*====================================================================================================*/
IDB.CalendarGrid = function(graphContext, calendarData) {
	this._graphContext = graphContext;
	this._calendarData = calendarData;
	this._build();
};

IDB.CalendarGrid.ModeMonthly = 'monthly';
IDB.CalendarGrid.ModeMonthlyEvent = 'monthlyevent';
IDB.CalendarGrid.ModeMultiday = 'multiday';
IDB.CalendarGrid.ModeDaily = 'daily';


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype._build = function() {
	var sett = this._graphContext.settings;
	this._mode = sett.get('CalMode');
	this._calendars = [];
	
	switch ( this._mode ) {
		case IDB.CalendarGrid.ModeMonthly:
		case IDB.CalendarGrid.ModeMonthlyEvent:
			this._numCols = sett.get('CalMCols');
			this._numRows = sett.get('CalMRows');
			break;
			
		case IDB.CalendarGrid.ModeMultiday:
			this._numCols = sett.get('CalMDCols');
			this._numRows = sett.get('CalMDRows');
			break;
			
		case IDB.CalendarGrid.ModeDaily:
			this._numCols = sett.get('CalDCols');
			this._numRows = sett.get('CalDRows');
			break;
	}
	
	var numCells = this._numCols*this._numRows;
	var currOff = (sett.get('CalBaseType') ? 1-numCells : 0);
	var i, cal;
	
	this._currOffset = currOff;
	
	for ( i = 0 ; i < numCells ; ++i ) {
		cal = this._buildNewCalendar(currOff+i);
		this._calendars.push(cal);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype._buildNewCalendar = function(index) {
	var gc = this._graphContext;
	var data = this._calendarData;

	switch ( this._mode ) {
		case IDB.CalendarGrid.ModeMonthly:
		case IDB.CalendarGrid.ModeMonthlyEvent:
			return new IDB.CalendarMonth(gc, data, index);
			
		case IDB.CalendarGrid.ModeMultiday:
			return new IDB.CalendarDay(gc, data, index);
			
		case IDB.CalendarGrid.ModeDaily:
			return new IDB.CalendarIntraday(gc, data, index);
	}

	return null;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.shiftCurrentIndex = function(amount) {
	var dir = (amount > 0 ? 1 : -1);
	var cals = this._calendars;
	var currOff = this._currOffset;
	var indexShift = (dir > 0  ? cals.length : -1);
	var count = 0;
	var cal;
	
	while ( count != amount ) {
		cal = this._buildNewCalendar(currOff+indexShift+count);
		
		if ( dir > 0 ) {
			cals.shift();
			cals.push(cal);
		}
		else {
			cals.pop();
			cals.unshift(cal);
		}

		count += dir;
	}

	this._currOffset += amount;

	if ( this._onIndexChange ) {
		this._onIndexChange();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.setOnIndexChange = function(callback) {
	this._onIndexChange = callback;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.getMode = function() {
	return this._mode;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.getActiveDatapoints = function() {
	var dpList = [];
	var cals = this._calendars;
	var calCount = cals.length;
	var i;

	for ( i = 0 ; i < calCount ; ++i ) {
		dpList = dpList.concat(cals[i].getActiveDatapoints());
	}

	return dpList;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.getRowCount = function() {
	return this._numRows;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.getColCount = function() {
	return this._numCols;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.getCalendarCount = function() {
	return this._calendars.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.getCalendar = function(index) {
	return this._calendars[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.getButtonUnitText = function() {
	return (this._mode.indexOf('month') == 0 ? 'month' : 'day');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.showPagingButtons = function() {
	return this._graphContext.settings.getBool('CalShowPagingBtns');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGrid.prototype.getUnitText = function() {
	var mode = this._graphContext.settings.get('CalMode');
	return (mode.indexOf('month') == 0 ? 'month' : 'day');
};

/*====================================================================================================*/
IDB.CalendarGridView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGridView.prototype._buildKinetic = function() {
	var model = this._model;
	var numCals = model.getCalendarCount();
	var i;

	this._display = new Kinetic.Group();
	this._calendarCache = {};
	this._calendars = [];

	for ( i = 0 ; i < numCals ; ++i ) {
		this._buildNewCalendar(i);
	}
	
	if ( model.showPagingButtons() ) {
		var unit = model.getButtonUnitText();
		
		this._prevBtn = new IDB.GraphButtonArrow('', IDB.GraphButtonArrow.LEFT_DIR, unit, false);
		this._prevBtn.setSize(50, 25);
		this._prevBtn.setEmphasis(true);
		this._prevBtn.onClick(IDB.listener(this, '_onPrevClick'));
		this._display.add(this._prevBtn.getDisplay());
		
		this._nextBtn = new IDB.GraphButtonArrow('', IDB.GraphButtonArrow.RIGHT_DIR, unit, false);
		this._nextBtn.setSize(50, 25);
		this._nextBtn.setEmphasis(true);
		this._nextBtn.onClick(IDB.listener(this, '_onNextClick'));
		this._display.add(this._nextBtn.getDisplay());
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGridView.prototype._buildNewCalendar = function(index) {
	var model = this._model;
	var mode = model.getMode();
	var calModel = model.getCalendar(index);
	var offset = calModel.getIndexOffset();
	var cal = this._calendarCache[offset];

	if ( !cal ) {
		switch ( mode ) {
			case IDB.CalendarGrid.ModeMonthly:
			case IDB.CalendarGrid.ModeMonthlyEvent:
				cal = new IDB.CalendarMonthView(calModel, index);
				break;

			case IDB.CalendarGrid.ModeMultiday:
				cal = new IDB.CalendarDayView(calModel, index);
				break;

			case IDB.CalendarGrid.ModeDaily:
				cal = new IDB.CalendarIntradayView(calModel, index);
				break;
		}

		this._display.add(cal.getDisplay());
		this._calendarCache[offset] = cal;
	}

	this._calendars[index] = cal;
	return cal;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGridView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGridView.prototype.setSize = function(width, height) {
	this._width = width;
	this._height = height;

	this._display.clip({
		x: 0,
		y: 0,
		width: width,
		height: height
	});

	this._updateGrid();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGridView.prototype._updateGrid = function() {
	var model = this._model;
	var cals = this._calendars;
	var numCols = model.getColCount();
	var numRows = model.getRowCount();
	var nextBtn = this._nextBtn;
	var prevBtn = this._prevBtn;
	var width = this._width;
	var height = this._height;
	var pad = 5;

	var cw = (width-pad*(numCols-1)-1)/numCols;
	var ch = ((height-(nextBtn ? 25 : 0)-pad)-pad*(numRows-1)-1)/numRows;
	var i, j, cal;
	
	for ( i = 0 ; i < numCols ; ++i ) {
		for ( j = 0 ; j < numRows ; ++j ) {
			cal = cals[i+j*numCols];
			cal.setSize(cw, ch);
			//cal.cacheAsBitmap = true;

			cal.getDisplay()
				.x(Math.round(i*(cw+pad)))
				.y(Math.round(j*(ch+pad)));
		}
	}
	
	if ( nextBtn ) {
		var btnY = height-25;

		nextBtn.getDisplay()
			.x(width-50)
			.y(btnY);
			
		prevBtn.getDisplay()
			.y(btnY);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGridView.prototype._onNextClick = function() {
	this._doScroll(1, this._nextBtn.getMoveType());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGridView.prototype._onPrevClick = function() {
	this._doScroll(-1, this._prevBtn.getMoveType());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarGridView.prototype._doScroll = function(direction, moveType) {
	var model = this._model;
	var amt = direction;

	if ( moveType == IDB.GraphButtonArrow.MOVE_PAGE ) {
		amt *= model.getCalendarCount();
	}

	model.shiftCurrentIndex(amt);

	////

	this._calendars = [];

	var numCals = model.getCalendarCount();
	var offsetMap = {};
	var cache = this._calendarCache;
	var i, cal, offset;

	for ( i = 0 ; i < numCals ; ++i ) {
		this._buildNewCalendar(i);
		offset = model.getCalendar(i).getIndexOffset();
		offsetMap[offset] = true;
	}

	for ( offset in cache ) {
		if ( offsetMap[offset] ) {
			continue;
		}

		cal = cache[offset];
		cal.getDisplay().destroy();
		cache[offset] = null;
		delete cache[offset];
	}

	this._updateGrid();
	model.refreshDisplay();
};
/*====================================================================================================*/
IDB.CalendarIntraday = function(graphContext, calendarData, indexOffset) {
	this._graphContext = graphContext;
	this._calendarData = calendarData;
	this._indexOffset = indexOffset;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var calData = this._calendarData;
	var now = calData.getNow();
	var isHoliday = false;
	var i, hour;

	var date = new Date(calData.getBaseDate());
	date.setDate(date.getDate()+this._indexOffset);

	this._isToday = (now.getFullYear() == date.getFullYear() &&
		now.getMonth() == date.getMonth() && now.getDate() == date.getDate());
	this._titleText = IDB.DateUtil.format(date, sett.get('CalDayLabelFormat'));
	this._hours = [];

	for ( i = 0 ; i < 24 ; ++i ) {
		hour = new IDB.CalendarIntradayHour(gc, calData, date, i);
		this._hours.push(hour);

		if ( !isHoliday && hour.getItems().isHoliday() ) {
			isHoliday = true;
		}
	}

	if ( isHoliday ) {
		this._bgColor = sett.get('CalCellHolidayColor');
	}
	else {
		this._bgColor = sett.get(calData.isWeekendDay(date.getDay()) ?
			'CalCellWkndColor' : 'CalCellColor');
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getActiveDatapoints = function() {
	var dpList = [];
	var hours = this._hours;
	var hourCount = hours.length;
	var i;

	for ( i = 0 ; i < hourCount ; ++i ) {
		dpList = dpList.concat(hours[i].getActiveDatapoints());
	}

	return dpList;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getIndexOffset = function() {
	return this._indexOffset;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getHourCount = function() {
	return this._hours.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getHour = function(index) {
	return this._hours[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getTitleText = function() {
	return this._titleText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getBackgroundColor = function() {
	return this._bgColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.isToday = function() {
	return this._isToday;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getTitleLabelOptions = function() {
	return this._graphContext.settings.get('CalTitleOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.showHeaderShading = function() {
	return this._graphContext.settings.getBool('CalHeaderShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getHeaderColor = function() {
	return this._graphContext.settings.get('CalHeaderColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getGridColor = function() {
	return this._graphContext.settings.get('CalGridColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getTodayHighlightColor = function() {
	return this._graphContext.settings.get('CalTodayHighColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getScrollerColor = function() {
	return this._graphContext.settings.get('CalCellScrollerColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntraday.prototype.getStartHour = function() {
	return this._graphContext.settings.get('CalDailyStartHour');
};

/*====================================================================================================*/
IDB.CalendarIntradayHour = function(graphContext, calendarData, date, hour) {
	this._graphContext = graphContext;
	this._calendarData = calendarData;
	this._date = date;
	this._hour = hour;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHour.prototype._build = function() {
	var gc = this._graphContext;
	var calData = this._calendarData;
	var hour = this._hour;

	if ( hour % 12 == 0 ) {
		this._titleText = '12';
	}
	else {
		this._titleText = (hour % 12)+'';
	}

	this._titleText += (hour >= 12 ? 'p' : 'a');
	this._items = new IDB.CalendarItems(gc, calData, this._date, hour, false);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHour.prototype.getActiveDatapoints = function() {
	return this._items.getActiveDatapoints();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHour.prototype.getItems = function() {
	return this._items;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHour.prototype.getHour = function() {
	return this._hour;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHour.prototype.getTitleText = function() {
	return this._titleText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHour.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('CalCellLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHour.prototype.getGridColor = function() {
	return this._graphContext.settings.get('CalGridColor');
};

/*====================================================================================================*/
IDB.CalendarIntradayHourView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHourView.prototype._buildKinetic = function() {
	var model = this._model;
	var itemsModel = model.getItems();
	var rowsLen = itemsModel.getRowCount();
	var lblOpt = model.getLabelOptions();
	var gridColor = model.getGridColor();
	var hour = model.getHour();
	var i, row;
	
	var titleOpt = {
		size: lblOpt.size*1.65,
		color: lblOpt.color,
		bold: lblOpt.bold,
		align: 'right'
	};

	this._display = new Kinetic.Group();
	this._rows = [];
	this._trueH = 0;

	if ( hour % 2 ) {
		this._bg = new Kinetic.Rect({
			fill: gridColor.css,
			opacity: 0.06
		});
		this._display.add(this._bg);
	}
	
	if ( !rowsLen ) {
		this._noItemsLine = new Kinetic.Line({
			points: [],
			stroke: gridColor.css,
			strokeWidth: 1,
			opacity: 0.15
		});
		this._display.add(this._noItemsLine);
	}
	
	if ( hour < 23 ) {
		this._botLine = new Kinetic.Line({
			points: [],
			stroke: gridColor.css,
			strokeWidth: 1,
			opacity: 0.5
		});
		this._display.add(this._botLine);
	}

	this._title = new IDB.GraphLabel(titleOpt, '12p');
	this._title.x = 3;
	this._title.y = 5;
	this._title.width = this._title.textWidth;
	this._title.text = model.getTitleText();
	this._display.add(this._title.getDisplay());

	for ( i = 0 ; i < rowsLen ; ++i ) {
		row = new IDB.CalendarIntradayItemsView(itemsModel, i, gridColor);
		this._display.add(row.getDisplay());
		this._rows.push(row);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHourView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHourView.prototype.getTrueHeight = function() {
	return this._trueH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHourView.prototype.setWidth = function(width) {
	var title = this._title;
	var rows = this._rows;
	var rowsLen = rows.length;
	var px = title.x+title.width+7;
	var py = 0;
	var i, row;

	for ( i = 0 ; i < rowsLen ; ++i ) {
		row = rows[i];
		row.setWidth(width-px);
		row.getDisplay()
			.x(px)
			.y(py);

		py += row.getTrueHeight();
	}

	var trueH = Math.round(Math.max(title.height+title.y+3, py));
	this._trueH = trueH;

	if ( this._bg ) {
		this._bg
			.width(width)
			.height(trueH);
	}

	if ( this._noItemsLine ) {
		var halfH = Math.round(trueH/2);

		this._noItemsLine.points([
			px, halfH+0.5,
			width, halfH+0.5
		]);
	}

	if ( this._botLine ) {
		this._botLine.points([
			0, trueH+0.5,
			width, trueH+0.5
		]);
	}
};

/*====================================================================================================*/
//This class implements the interface required by IDB.CalendarScrollView.
IDB.CalendarIntradayHoursView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHoursView.prototype._buildKinetic = function() {
	var model = this._model;
	var hourCount = model.getHourCount();
	var i, hour;

	this._display = new Kinetic.Group();
	this._hours = [];
	this._trueH = 0;
	this._resetY = 0;
	this._startHour = model.getStartHour();

	for ( i = 0 ; i < hourCount ; ++i ) {
		hour = new IDB.CalendarIntradayHourView(model.getHour(i));
		this._display.add(hour.getDisplay());
		this._hours.push(hour);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHoursView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHoursView.prototype.getTrueHeight = function() {
	return this._trueH;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHoursView.prototype.getResetY = function() {
	return this._resetY;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayHoursView.prototype.setSize = function(width, height) {
	var hours = this._hours;
	var startHour = this._startHour;
	var hourCount = hours.length;
	var py = 0;
	var i, hour;

	for ( i = 0 ; i < hourCount ; ++i ) {
		hour = hours[i];
		hour.setWidth(width);
		hour.getDisplay().y(py);

		if ( i == startHour ) {
			this._resetY = py;
		}

		py += hour.getTrueHeight();
	}

	this._trueH = py;
};

/*====================================================================================================*/
IDB.CalendarIntradayItemsView = function(model, rowIndex, gridColor) {
	this._model = model;
	this._rowIndex = rowIndex;
	this._gridColor = gridColor;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayItemsView.prototype._buildKinetic = function() {
	var model = this._model;
	var rowIndex = this._rowIndex;
	var row = model.getRow(rowIndex);
	var rowLen = row.length;
	var lblOpt = model.getCellLabelOptions();
	var firstDp = model.getActiveDatapoints()[0];
	var lblText = firstDp.xValue.stringValue.split(' ')[1];
	var firstNumItemModel = null;
	var i, itemModel, box, text;
	
	this._display = new Kinetic.Group();
	this._boxes = [];
	this._texts = [];
	this._trueH = 0;

	lblOpt.align = 'left';

	this._label = new IDB.GraphLabel(lblOpt, lblText.substring(0, 5));
	this._display.add(this._label.getDisplay());

	for ( i = 0 ; i < rowLen ; ++i ) {
		itemModel = row[i];

		if ( !itemModel.isNumeric() ) {
			continue;
		}

		box = new IDB.CalendarDayNumItemView(itemModel);
		this._display.add(box.getDisplay());
		this._boxes.push(box);

		if ( !firstNumItemModel ) {
			firstNumItemModel = itemModel;
		}
	}
	
	for ( i = 0 ; i < rowLen ; ++i ) {
		itemModel = row[i];

		if ( itemModel.isNumeric() ) {
			continue;
		}

		text = new IDB.CalendarDayTextItemView(itemModel);
		this._display.add(text.getDisplay());
		this._texts.push(text);
	}

	if ( model.useMetricsBg() && firstNumItemModel != null ) {
		this._metricBg = new Kinetic.Rect({
			fill: firstNumItemModel.getRangeColor().css
		});
		this._display.add(this._metricBg);
		this._metricBg.moveToBottom();
	}

	if ( rowIndex < model.getRowCount()-1 ) {
		this._line = new Kinetic.Line({
			points: [],
			stroke: this._gridColor.css,
			strokeWidth: 1,
			opacity: 0.15
		});
		this._display.add(this._line);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayItemsView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayItemsView.prototype.getTrueHeight = function() {
	return this._trueH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayItemsView.prototype.setWidth = function(width) {
	var model = this._model;
	var lbl = this._label;
	var boxes = this._boxes;
	var texts = this._texts;
	var lblSize = model.getCellLabelOptions().size;
	var boxCount = boxes.length;
	var textCount = texts.length;
	var py = 3;
	var i, item;

	lbl.x = 1;
	lbl.y = py;
	lbl.width = width-lbl.x;

	py += lbl.height+3;

	for ( i = 0 ; i < boxCount ; ++i ) {
		item = boxes[i];
		item.setSize(width, lblSize);
		item.getDisplay().y(py);

		py += lblSize+4;
	}

	for ( i = 0 ; i < textCount ; ++i ) {
		item = texts[i];
		item.setSize(width, lblSize);
		item.getDisplay().y(py);

		py += Math.round(item.getTextHeight())+3;
	}

	if ( this._metricBg ) {
		this._metricBg
			.y(2)
			.width(width)
			.height(py-4);
	}

	if ( this._line ) {
		this._line.points([
			0, py+0.5,
			width, py+0.5
		]);
	}

	this._trueH = py;
};

/*====================================================================================================*/
IDB.CalendarIntradayView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayView.prototype._buildKinetic = function() {
	var model = this._model;
	var titleLblOpt = model.getTitleLabelOptions();

	titleLblOpt.align = 'center';

	this._display = new Kinetic.Group();
	this._hours = [];

	this._bg = new Kinetic.Rect({
		fill: model.getBackgroundColor().css,
		stroke: model.getGridColor().css,
		strokeWidth: 0.5
	});
	this._display.add(this._bg);
	
	this._header = new Kinetic.Rect({
		fill: model.getHeaderColor().css,
		stroke: model.getGridColor().css,
		strokeWidth: 1
	});
	this._display.add(this._header);

	this._title = new IDB.GraphLabel(titleLblOpt, model.getTitleText());
	this._display.add(this._title.getDisplay());

	if ( model.showHeaderShading() ) {
		this._shade = new IDB.GenericShading(IDB.GenericShading.TypeGloss, true);
		this._display.add(this._shade.getDisplay());
	}
	
	if ( model.isToday() ) {
		this._todayHigh = new Kinetic.Rect({
			stroke: model.getTodayHighlightColor().css,
			strokeWidth: 2.5
		});
		this._display.add(this._todayHigh);
	}
	
	this._hours = new IDB.CalendarIntradayHoursView(model);

	this._scroll = new IDB.CalendarScrollView(this._hours, model.getScrollerColor());
	this._display.add(this._scroll.getDisplay());
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarIntradayView.prototype.setSize = function(width, height) {
	if ( this._width == width && this._height == height ) {
		return;
	}

	this._width = width;
	this._height = height;

	var titleLbl = this._title;
	var headerH = Math.min(titleLbl.textHeight+8, height*0.18);
	
	this._header
		.x(0.5)
		.y(0.5)
		.width(width-1)
		.height(headerH-1);
	
	this._bg
		.x(0.5)
		.y(headerH+0.5)
		.width(width-1)
		.height(height-headerH-1);
	
	if ( this._shade ) {
		var size = IDB.GenericShading.FullDimension;
		
		this._shade.getDisplay()
			.scaleX(width/size)
			.scaleY(headerH/size);
	}

	if ( this._todayHigh ) {
		var t = 2.5;

		this._todayHigh
			.x(t/2+0.5)
			.y(headerH+t/2+0.5)
			.width(width-t-1)
			.height(height-headerH-t-1);
	}
	
	titleLbl.y = (headerH-titleLbl.textHeight)/2;
	titleLbl.width = width;
	titleLbl.truncate();

	this._scroll.setSize(width, height-headerH);
	this._scroll.getDisplay().y(headerH);
};

/*====================================================================================================*/
IDB.CalendarItem = function(graphContext, calendarData, datapoint, ignoreDatapointEvents) {
	this._graphContext = graphContext;
	this._calendarData = calendarData;
	this._datapoint = datapoint;
	this._ignoreDatapointEvents = ignoreDatapointEvents;
	this._build();
};

IDB.CalendarItem.ShapeRectangle = 0;
IDB.CalendarItem.ShapeEllipse = 1;
IDB.CalendarItem.ShapeDiamond = 2;
IDB.CalendarItem.ShapeTriangle = 3;
IDB.CalendarItem.ShapeTriangleInverted = 4;
IDB.CalendarItem.ShapeDollar = 5;
IDB.CalendarItem.ShapeEuro = 6;
IDB.CalendarItem.ShapeYen = 7;
IDB.CalendarItem.ShapePound = 8;
IDB.CalendarItem.ShapeSpade = 9;
IDB.CalendarItem.ShapeClub = 10;
IDB.CalendarItem.ShapeHeart = 11;
IDB.CalendarItem.ShapeMusic = 12;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype._build = function() {
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.setOnDatapointsUpdated = function(callback) {
	if ( this._ignoreDatapointEvents ) {
		return;
	}

	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.showItem = function() {
	return !this._datapoint.yValue.isNothing;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.isNumeric = function() {
	return this._datapoint.yAxis.dataTypeIsNumber;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.getRangeColor = function() {
	return this._datapoint.rangeColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.getAxisName = function() {
	return this._datapoint.yAxis.axisName;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.getAxisColor = function() {
	return this._datapoint.yAxis.axisColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.getFormattedValue = function() {
	return this._datapoint.yValue.formattedValue;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.getVisualState = function() {
	return this._datapoint.getVisualState();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.getShape = function() {
	return this._calendarData.getMetricShape(this._datapoint.yAxis.axisName);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.getShapeSize = function() {
	return this._graphContext.settings.get('CalMetricShapeSize')/100;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.getCellLabelOptions = function() {
	return this._graphContext.settings.get('CalCellLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.showAxisName = function() {
	return this._graphContext.settings.getBool('CalCellLabelAxisName');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, "_handleClick");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.createOverListener = function() {
	return IDB.listener(this, "_handleOver");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype.createOutListener = function() {
	return IDB.listener(this, "_handleOut");
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype._handleClick = function() {
	if ( this._ignoreDatapointEvents ) {
		return;
	}

	this._graphContext.onDatapointClicked(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype._handleOver = function() {
	if ( this._ignoreDatapointEvents ) {
		return;
	}

	this._graphContext.onDatapointOver(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItem.prototype._handleOut = function() {
	if ( this._ignoreDatapointEvents ) {
		return;
	}

	this._graphContext.onDatapointOut(this._datapoint);
};

/*====================================================================================================*/
IDB.CalendarItems = function(graphContext, calendarData, date, hour, isOutsideMonth) {
	this._graphContext = graphContext;
	this._calendarData = calendarData;
	this._date = date;
	this._hour = hour;
	this._isOutsideMonth = isOutsideMonth;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype._build = function() {
	var date = this._date;
	var hour = this._hour;
	var calData = this._calendarData;
	var dataRows = (hour == null ? calData.getRowsByDate(date) : calData.getRowsByDatetime(date, hour));

	this._boxes = [];
	this._texts = [];
	this._datapoints = [];
	this._rows = [];
	this._isHoliday = false;
	this._firstTextDatapoint = null;

	if ( !dataRows || !dataRows.length ) {
		return;
	}

	var numRows = (hour == null ? 1 : dataRows.length);

	for ( var i = 0 ; i < numRows ; ++i ) {
		this._buildWithRow(dataRows[i]);
	}
}

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype._buildWithRow = function(dpRow) {
	if ( !dpRow ) {
		return;
	}

	var gc = this._graphContext;
	var calData = this._calendarData;
	var isOutsideMonth = this._isOutsideMonth;
	var numDatapoints = dpRow.datapoints.length;
	var holidayAxisId = calData.getHolidayAxisId();
	var newRow = [];
	var i, item, dp;

	this._rows.push(newRow);

	for ( i = 0 ; i < numDatapoints ; ++i ) {
		dp = dpRow.datapoints[i];

		if ( dp.yAxis.isHidden || dp.yAxis.isPivot ) {
			continue;
		}
		
		if ( !this._isHoliday && dp.yAxis.axisId == holidayAxisId ) {
			this._isHoliday = (dp.yValue.stringValue == '1');
			continue;
		}
		
		if ( !dp.yAxis.dataTypeIsNumber ) {
			if ( dp.yValue.stringValue.length ) {
				item = new IDB.CalendarItem(gc, calData, dp, isOutsideMonth);
				this._texts.push(item);
				this._datapoints.push(dp);
				newRow.push(item);

				if ( this._firstTextDatapoint == null ) {
					this._firstTextDatapoint = dp;
				}
			}

			continue;
		}
		
		item = new IDB.CalendarItem(gc, calData, dp, isOutsideMonth);
		this._boxes.push(item);
		this._datapoints.push(dp);
		newRow.push(item);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getActiveDatapoints = function() {
	return this._datapoints;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getRowCount = function() {
	return this._rows.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getRow = function(index) {
	return this._rows[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.isHoliday = function() {
	return this._isHoliday;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getRangeColor = function() {
	return (this._boxes.length ? this._boxes[0].getRangeColor() : null);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getBoxCount = function() {
	return this._boxes.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getBox = function(index) {
	return this._boxes[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getTextCount = function() {
	return this._texts.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getText = function(index) {
	return this._texts[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getFirstTextDatapoint = function() {
	return this._firstTextDatapoint;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.useMetricsBg = function() {
	return this._graphContext.settings.getBool('CalUseMetricsBg');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarItems.prototype.getCellLabelOptions = function() {
	return this._graphContext.settings.get('CalCellLabelOptions');
};

/*====================================================================================================*/
IDB.CalendarMonth = function(graphContext, calendarData, indexOffset) {
	this._graphContext = graphContext;
	this._calendarData = calendarData;
	this._indexOffset = indexOffset;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype._build = function() {
	this._cells = [];

	var gc = this._graphContext;
	var calData = this._calendarData;
	var sett = gc.settings;
	var useDayLetter = sett.getBool('CalDayLabelLetter');
	var dayLetters = IDB.GraphLang.get('CalendarDayLetters').split(',');
	var dayAbbrevs = IDB.GraphLang.get('CalendarDayAbbrevs').split(',');
	var dayOffset = sett.get('CalMonthStartDay'); //between 0 (sun) and 6 (sat)
	var baseDate = this._calendarData.getBaseDate();
	var monthDate = new Date(baseDate.getFullYear(), baseDate.getMonth()+this._indexOffset, 1);
	var monthNum = monthDate.getMonth();
	var i, mc, dayI;

	var d = new Date(monthDate);
	d.setDate(monthDate.getDate()+(dayOffset-d.getDay()-7)%7);
	
	for ( i = 0 ; i < 42 ; ++i ) {
		mc = new IDB.CalendarMonthCell(gc, calData, monthNum, new Date(d));
		this._cells[i] = mc;

		d.setDate(d.getDate()+1);
	}

	this._titleText = IDB.DateUtil.format(monthDate, sett.get('CalMonthLabelFormat'));
	this._dayTexts = [];
	
	for ( i = 0 ; i < 7 ; ++i ) {
		dayI = (i+dayOffset+7)%7;
		this._dayTexts[i] = (useDayLetter ? dayLetters[dayI] : dayAbbrevs[dayI]);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getActiveDatapoints = function() {
	var dpList = [];
	var cells = this._cells;
	var cellCount = cells.length;
	var i;

	for ( i = 0 ; i < cellCount ; ++i ) {
		dpList = dpList.concat(cells[i].getActiveDatapoints());
	}

	return dpList;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getIndexOffset = function() {
	return this._indexOffset;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getCellCount = function() {
	return this._cells.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getCell = function(index) {
	return this._cells[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getTitleText = function() {
	return this._titleText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getDayColumnText = function(index) {
	return this._dayTexts[index];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getTitleLabelOptions = function() {
	return this._graphContext.settings.get('CalTitleOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getDayColumnLabelOptions = function() {
	return this._graphContext.settings.get('CalDayLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.showHeaderShading = function() {
	return this._graphContext.settings.getBool('CalHeaderShading');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getHeaderColor = function() {
	return this._graphContext.settings.get('CalHeaderColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getGridColor = function() {
	return this._graphContext.settings.get('CalGridColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonth.prototype.getCellColor = function() {
	return this._graphContext.settings.get('CalCellColor');
};

/*====================================================================================================*/
IDB.CalendarMonthCell = function(graphContext, calendarData, monthNum, date) {
	this._graphContext = graphContext;
	this._calendarData = calendarData;
	this._monthNum = monthNum;
	this._date = date;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype._build = function() {
	var gc = this._graphContext;
	var calData = this._calendarData;
	var sett = gc.settings;
	var date = this._date;
	var now = calData.getNow();

	this._isWithinMonth = (this._monthNum == this._date.getMonth());
	this._bgColor = sett.get(calData.isWeekendDay(date.getDay()) ? 'CalCellWkndColor' : 'CalCellColor');
	this._isToday = (now.getFullYear() == date.getFullYear() &&
		now.getMonth() == date.getMonth() && now.getDate() == date.getDate());
	this._isEventMode = (sett.get('CalMode') == IDB.CalendarGrid.ModeMonthlyEvent);
	
	this._items = new IDB.CalendarItems(gc, calData, date, null, !this._isWithinMonth);
	this._hasTextData = (this._items.getTextCount() > 0);
	this._firstTextData = this._items.getFirstTextDatapoint();
	
	if ( this._items.isHoliday() ) {
		this._bgColor = sett.get('CalCellHolidayColor');
	}
	
	this._labelText = date.getDate()+'';
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.getActiveDatapoints = function() {
	return (this._isWithinMonth ? this._items.getActiveDatapoints() : []);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.getItems = function() {
	return this._items;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.getLabelText = function() {
	return this._labelText;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.isEventMode = function() {
	return this._isEventMode;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.isWithinMonth = function() {
	return this._isWithinMonth;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.isToday = function() {
	return this._isToday;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.getBackgroundColor = function() {
	return this._bgColor;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.hasTextDatapoint = function() {
	return this._hasTextData;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('CalCellLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.getScrollerColor = function() {
	return this._graphContext.settings.get('CalCellScrollerColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.getEventIconColor = function() {
	return this._graphContext.settings.get('CalEventIconColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.getTodayHighlightColor = function() {
	return this._graphContext.settings.get('CalTodayHighColor');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.createTextClickListener = function() {
	return IDB.leftClickListener(this, '_handleTextClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.createTextOverListener = function() {
	return IDB.listener(this, '_handleTextOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype.createTextOutListener = function() {
	return IDB.listener(this, '_handleTextOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype._handleTextClick = function() {
	if ( !this._isWithinMonth ) {
		return;
	}

	this._graphContext.onDatapointClicked(this._firstTextData);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype._handleTextOver = function() {
	if ( !this._isWithinMonth ) {
		return;
	}

	this._graphContext.onDatapointOver(this._firstTextData);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCell.prototype._handleTextOut = function() {
	if ( !this._isWithinMonth ) {
		return;
	}

	this._graphContext.onDatapointOut(this._firstTextData);
};

/*====================================================================================================*/
IDB.CalendarMonthCellView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCellView.prototype._buildKinetic = function() {
	var model = this._model;
	var isEventMode = model.isEventMode();
	var isWithinMonth = model.isWithinMonth();
	var itemsModel = model.getItems();
	var lblOpt = model.getLabelOptions();

	lblOpt = {
		size: lblOpt.size,
		color: lblOpt.color,
		bold: lblOpt.bold,
		align: 'right'
	};

	this._display = new Kinetic.Group();

	this._bg = new Kinetic.Rect({
		fill: model.getBackgroundColor().css
	});
	this._display.add(this._bg);

	if ( model.isToday() ) {
		this._todayHigh = new Kinetic.Rect({
			stroke: model.getTodayHighlightColor().css,
			strokeWidth: 2.5
		});
		this._display.add(this._todayHigh);
	}
	
	if ( isEventMode ) {
		this._items = new IDB.CalendarDayItemsView(itemsModel, true);
		
		this._scroll = new IDB.CalendarScrollView(this._items, model.getScrollerColor());
		this._display.add(this._scroll.getDisplay());
	}
	else {
		this._items = new IDB.CalendarMonthItemsView(itemsModel);
		this._display.add(this._items.getDisplay());
	}

	this._dateLabel = new IDB.GraphLabel(lblOpt, model.getLabelText());
	this._display.add(this._dateLabel.getDisplay());
	
	if ( !isEventMode && model.hasTextDatapoint() ) {
		this._eventIcon = new Kinetic.Line({
			fill: model.getEventIconColor().css,
			closed: true,
			points: [
				0, 0,
				0, 1000,
				1000, 0
			]
		});
		this._eventIcon.on('click', model.createTextClickListener());
		this._eventIcon.on('mouseover', model.createTextOverListener());
		this._eventIcon.on('mouseout', model.createTextOutListener());
		this._touchHandler = new IDB.TouchHandler(this._eventIcon);
		this._display.add(this._eventIcon);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCellView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthCellView.prototype.setSize = function(width, height) {
	this._bg
		.width(width)
		.height(height);

	var lbl = this._dateLabel;
	lbl.x = 3;
	lbl.y = Math.min(2, ((height/2)-lbl.height)/2);
	lbl.width = width-lbl.x*2;
	
	var itemsH = Math.max(height/2, height-lbl.textHeight-2);
	var itemsY = Math.round(height-itemsH);
	
	if ( this._scroll ) {
		this._scroll.setSize(width, itemsH);
		this._scroll.getDisplay().y(itemsY);
	}
	else {
		this._items.setSize(width, itemsH);
		this._items.getDisplay().y(itemsY);
	}

	if ( this._todayHigh ) {
		var t = 2.5;

		this._todayHigh
			.x(t/2)
			.y(t/2)
			.width(width-t)
			.height(height-t);
	}

	if ( this._eventIcon ) {
		var evtScale = itemsY/1000;

		this._eventIcon
			.scaleX(evtScale)
			.scaleY(evtScale);
	}
};

/*====================================================================================================*/
IDB.CalendarMonthItemView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemView.prototype._build = function() {
	var model = this._model;

	this._origSize = 100;

	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());
	this._touchHandler = new IDB.TouchHandler(this._display);

	this._bg = new Kinetic.Rect({
		fill: 'rgba(0, 0, 0, 0.001)'
	});
	this._display.add(this._bg);

	if ( model.showItem() ) {
		this._shape = this._buildShape();
		this._shape.listening(false);
		this._display.add(this._shape);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemView.prototype._buildShape = function() {
	var model = this._model;
	
	var help = new IDB.CalendarViewHelper(this._origSize, model.getRangeColor());

	switch ( model.getShape() ) {
		case IDB.CalendarItem.ShapeRectangle:
			return help.buildRectangle();
			
		case IDB.CalendarItem.ShapeEllipse:
			return help.buildEllipse();
			
		case IDB.CalendarItem.ShapeDiamond:
			return help.buildDiamond();
			
		case IDB.CalendarItem.ShapeTriangle:
			return help.buildTriangle(false);
			
		case IDB.CalendarItem.ShapeTriangleInverted:
			return help.buildTriangle(true);
			
		case IDB.CalendarItem.ShapeDollar:
			return help.buildDollarLabel().getDisplay();

		case IDB.CalendarItem.ShapeEuro:
			return help.buildEuroLabel().getDisplay();
			
		case IDB.CalendarItem.ShapeYen:
			return help.buildYenLabel().getDisplay();
		
		case IDB.CalendarItem.ShapePound:
			return help.buildPoundLabel().getDisplay();
		
		case IDB.CalendarItem.ShapeSpade:
			return help.buildSpadeLabel().getDisplay();
		
		case IDB.CalendarItem.ShapeClub:
			return help.buildClubLabel().getDisplay();
		
		case IDB.CalendarItem.ShapeHeart:
			return help.buildHeartLabel().getDisplay();
		
		case IDB.CalendarItem.ShapeMusic:
			return help.buildMusicLabel().getDisplay();
	}
	
	console.log('CalendarMonthItemView: Unknown Shape '+model.getShape());
	return help.buildRectangle();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemView.prototype.setSize = function(width, height) {
	var model = this._model;
	var size = model.getShapeSize();
	var s = this._origSize;
	var pad = Math.min(width, height)*(1-size);

	this._bg
		.width(width)
		.height(height);

	if ( this._shape ) {
		this._shape
			.x(pad/2)
			.y(pad/2)
			.scaleX((width-pad)/s)
			.scaleY((height-pad)/s);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemView.prototype._onDatapointsUpdated = function() {
	if ( !this._shape ) {
		return;
	}

	this._shape.opacity(this._model.getVisualState() == IDB.VS.FADED ? 0.2 : 1.0);
};

/*====================================================================================================*/
IDB.CalendarMonthItemsView = function(model) {
	this._model = model;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemsView.prototype._build = function() {
	var model = this._model;
	var boxCount = model.getBoxCount();
	var i, box;

	this._display = new Kinetic.Group();
	this._boxes = [];

	this._bg = new Kinetic.Rect();
	this._display.add(this._bg);

	for ( i = 0 ; i < boxCount ; ++i ) {
		box = new IDB.CalendarMonthItemView(model.getBox(i));
		this._display.add(box.getDisplay());
		this._boxes.push(box);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemsView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthItemsView.prototype.setSize = function(width, height) {
	var model = this._model;
	var boxes = this._boxes;
	var rangeColor = (model.useMetricsBg() ? model.getRangeColor() : null);

	this._bg
		.fill(rangeColor ? rangeColor.css : 'rgba(0, 0, 0, 0.05)')
		.width(width)
		.height(height);
	
	var boxCount = boxes.length;

	if ( !boxCount ) {
		return;
	}
	
	var space = new IDB.CalendarSpacingUtil(boxCount, width, height);
	var itemW = space.getItemWidth();
	var itemH = space.getItemHeight();
	var i, p, box;
	
	for ( i = 0 ; i < boxCount ; ++i ) {
		p = space.getPos(i);

		box = boxes[i];
		box.setSize(itemW, itemH);
		box.getDisplay()
			.x(p.x)
			.y(p.y);
	}
};

/*====================================================================================================*/
IDB.CalendarMonthView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthView.prototype._buildKinetic = function() {
	var model = this._model;
	var cellCount = model.getCellCount();
	var titleLblOpt = model.getTitleLabelOptions();
	var dayColLblOpt = model.getDayColumnLabelOptions();
	var headerCol = model.getHeaderColor();
	var gridCol = model.getGridColor();
	var cellCol = model.getCellColor();
	var cols = 7;
	var rows = 6;
	var i, cell, line, lbl;

	titleLblOpt.align = 'center';
	dayColLblOpt.align = 'center';

	this._display = new Kinetic.Group();
	this._cells = [];
	this._columnLabels = [];
	this._gridLinesH = [];
	this._gridLinesV = [];

	for ( i = 0 ; i < cellCount ; ++i ) {
		cell = new IDB.CalendarMonthCellView(model.getCell(i));
		this._display.add(cell.getDisplay());
		this._cells[i] = cell;
	}

	////

	this._grid = new Kinetic.Group();
	this._display.add(this._grid);
	
	this._gridHeader = new Kinetic.Rect({
		fill: headerCol.css
	});
	this._grid.add(this._gridHeader);
	
	this.gridHeaderDayA = new Kinetic.Rect({
		fill: cellCol.css
	});
	this._grid.add(this.gridHeaderDayA);
	
	this.gridHeaderDayB = new Kinetic.Rect({
		fill: headerCol.css,
		opacity: 0.7
	});
	this._grid.add(this.gridHeaderDayB);
	
	for ( i = 0 ; i < cols+1 ; ++i ) {
		line = new Kinetic.Line({
			stroke: gridCol.css,
			strokeWidth: 1
		});
		this._grid.add(line);
		this._gridLinesV.push(line);
	}
	
	for ( i = -2 ; i < rows+1 ; ++i ) {
		line = new Kinetic.Line({
			stroke: gridCol.css,
			strokeWidth: 1
		});
		this._grid.add(line);
		this._gridLinesH.push(line);
	}

	////

	this._title = new IDB.GraphLabel(titleLblOpt, model.getTitleText());
	this._display.add(this._title.getDisplay());

	for ( i = 0 ; i < 7 ; ++i ) {
		lbl = new IDB.GraphLabel(dayColLblOpt, model.getDayColumnText(i));
		this._display.add(lbl.getDisplay());
		this._columnLabels[i] = lbl;
	}

	if ( model.showHeaderShading() ) {
		this._shade1 = new IDB.GenericShading(IDB.GenericShading.TypeGloss, true);
		this._display.add(this._shade1.getDisplay());

		this._shade2 = new IDB.GenericShading(IDB.GenericShading.TypeGloss, true);
		this._shade2.getDisplay().opacity(0.5);
		this._display.add(this._shade2.getDisplay());
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarMonthView.prototype.setSize = function(width, height) {
	if ( this._width == width && this._height == height ) {
		return;
	}

	this._width = width;
	this._height = height;

	var titleLbl = this._title;
	var colLbls = this._columnLabels;
	var cells = this._cells;
	var gridLinesH = this._gridLinesH;
	var gridLinesV = this._gridLinesV;
	var cols = 7;
	var rows = 6;
	var cellLen = cells.length;
	var pad = 0;
	var headerH = Math.round(Math.min(titleLbl.textHeight+8, height*0.18));
	var dayH = Math.min(colLbls[0].textHeight+8, height*0.12);
	var cw = (width-pad*(cols-1))/cols;
	var ch = ((height-headerH-dayH)-pad*(rows-1))/rows;
	var xPos = [];
	var yPos = [];
	var i, lbl, mc, px, px2, py, py2;
	
	this._gridHeader
		.width(width)
		.height(headerH);

	this.gridHeaderDayA
		.y(headerH)
		.width(width)
		.height(dayH);

	this.gridHeaderDayB
		.y(headerH)
		.width(width)
		.height(dayH);
	
	if ( this._shade1 ) {
		var size = IDB.GenericShading.FullDimension;
		
		this._shade1.getDisplay()
			.scaleX(width/size)
			.scaleY(headerH/size);

		this._shade2.getDisplay()
			.y(headerH)
			.scaleX(width/size)
			.scaleY(dayH/size);
	}
	
	for ( i = 0 ; i < cols+1 ; ++i ) {
		px = Math.round(i*(cw+pad));
		px2 = (i == cols ? -0.5 : 0.5);
		xPos[i] = px;
		
		gridLinesV[i].points([
			px+px2, (i == 0 || i == cols ? 0 : headerH),
			px+px2, height
		]);

		if ( i == cols ) {
			continue;
		}

		lbl = colLbls[i];
		lbl.x = px;
		lbl.y = headerH+(dayH-lbl.textHeight)/2+1;
		lbl.width = cw;
	}
	
	for ( i = -2 ; i < rows+1 ; ++i ) {
		py = Math.round((i >= -1 ? headerH : 0) + (i >= 0 ? dayH+i*(ch+pad) : 0));
		py2 = (i == rows ? -0.5 : 0.5);
		yPos[i] = py;
		
		gridLinesH[i+2].points([
			0, py+py2,
			width, py+py2
		]);
	}

	for ( i = 0 ; i < cellLen ; ++i ) {
		mc = cells[i];
		mc.setSize(cw, ch);
		mc.getDisplay()
			.x(xPos[i%cols])
			.y(yPos[Math.floor(i/cols)]);
	}
	
	titleLbl.y = (headerH-titleLbl.textHeight)/2;
	titleLbl.width = width;
	titleLbl.truncate();
};

/*====================================================================================================*/
IDB.CalendarScrollView = function(view, scrollerColor) {
	this._view = view;
	this._scrollerColor = scrollerColor;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarScrollView.prototype._buildKinetic = function() {
	var scrollCss = this._scrollerColor.css;

	this._display = new Kinetic.Group();

	this._scrollHold = new Kinetic.Group();
	this._display.add(this._scrollHold);

	this._scrollHold.add(this._view.getDisplay());

	this._track = new Kinetic.Rect({
		fill: scrollCss,
		opacity: 0.333
	});
	this._display.add(this._track);
	
	this._dragger = new Kinetic.Rect({
		fill: scrollCss,
		draggable: true,
		dragBoundFunc: IDB.listener(this, '_getDraggerBounds')
	});
	this._display.add(this._dragger);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarScrollView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarScrollView.prototype.setSize = function(width, height) {
	this._width = width;
	this._height = height;

	var track = this._track;
	var dragger = this._dragger;
	var scrHold = this._scrollHold;

	track
		.width(8)
		.height(height)
		.x(width-track.width())
		.visible(false);
	
	dragger
		.x(track.x())
		.y(0)
		.width(track.width())
		.visible(false);
	
	this._updateScroll();
	scrHold.y(0);

	var scrDist = this._scrollDist;

	if ( scrDist > 0 ) {
		var holdY = Math.round(Math.min(this._view.getResetY(), scrDist));

		scrHold.y(-holdY);
		dragger.y(holdY/scrDist*(track.height()-dragger.height()));
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarScrollView.prototype._updateScroll = function(finalUpdate) {
	var width = this._width;
	var height = this._height;
	var view = this._view;
	var track = this._track;
	var dragger = this._dragger;

	view.setSize(width-(track.visible() ? track.width() : 0), height);

	var itemH = view.getTrueHeight();
	this._scrollDist = itemH-height;
	
	this._display.clip({
		x: 0,
		y: 0,
		width: width,
		height: height
	});
	
	if ( finalUpdate ) {
		return;
	}
	
	if ( !track.visible() && itemH > height ) {
		track.visible(true);

		dragger
			.visible(true)
			.y(track.y())
			.height((height/itemH)*track.height());

		this._updateScroll(true);
	}
	else if ( track.visible() && itemH < height ) {
		track.visible(false);
		dragger.visible(false);

		this._updateScroll(true);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarScrollView.prototype._getDraggerBounds = function(pos) {
	var track = this._track;
	var abs = track.getAbsolutePosition();
	var avail = track.height()-this._dragger.height();
	var perc = IDB.MathUtil.clamp((pos.y-abs.y)/avail, 0, 1);

	this._scrollHold.y(Math.round(-perc*this._scrollDist));

	return {
		x: abs.x,
		y: abs.y+perc*avail
	};
};

/*====================================================================================================*/
IDB.CalendarSpacingUtil = function(numItems, width, height) {
	var cols = 0;
	var rows, size, sizeVert, i;

	this._size = 0;
	
	while ( cols <= numItems ) {
		rows = Math.ceil(numItems/cols);
		
		size = width/cols;
		sizeVert = Math.max(0, height/rows);
		size = Math.min(size, sizeVert);
		
		//Has loop has passed optimal size?  Using < instead of <= so the algorithm will
		//maximize the number of columns in tie situations.
		if ( size < this._size ) {
			--cols;
			break;
		}
		
		this._size = size;
		++cols;
	}

	////

	this._positions = [];
	this._size = Math.round(this._size);
	size = this._size;

	for ( i = 0 ; i < numItems ; ++i ) {
		this._positions[i] = {
			x: Math.round((i%cols)*size),
			y: Math.round(Math.floor(i/cols)*size)
		};
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarSpacingUtil.prototype.getPos = function(index) {
	return this._positions[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarSpacingUtil.prototype.getItemWidth = function() {
	return this._size;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarSpacingUtil.prototype.getItemHeight = function() {
	return this._size;
};

/*====================================================================================================*/
IDB.CalendarViewHelper = function(size, color) {
	this._size = size;
	this._color = color;
};

//TODO: update Metrics graph to use this helper


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildRectangle = function() {
	var size = this._size;

	return new Kinetic.Rect({
		width: size,
		height: size,
		fill: this._color.css
	});
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildEllipse = function() {
	var size = this._size;

	return new Kinetic.Ellipse({
		offsetX: -size/2,
		offsetY: -size/2,
		width: size,
		height: size,
		fill: this._color.css
	});
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildDiamond = function() {
	var size = this._size;

	return new Kinetic.Line({
		closed: true,
		points: [
			size/2, 0,
			size, size/2,
			size/2, size,
			0, size/2
		],
		fill: this._color.css
	});
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildTriangle = function(inverted) {
	var size = this._size;

	return new Kinetic.Line({
		closed: true,
		points: [
			size/2, (inverted ? size : 0),
			size, (inverted ? 0 : size),
			0, (inverted ? 0 : size)
		],
		fill: this._color.css
	});
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildDollarLabel = function() {
	return this._buildCharShape('$', -6, -3, 1.16, 1.14);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildEuroLabel = function() {
	return this._buildCharShape('\u20ac', 3, -10, 1.03, 1.34);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildYenLabel = function() {
	return this._buildCharShape('\u00a5', 0, -12, 1.0, 1.39);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildPoundLabel = function() {
	return this._buildCharShape('\u00a3', -1, -10, 1.05, 1.34);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildSpadeLabel = function() {
	return this._buildCharShape('\u2660', -6, -35, 1.11, 1.68);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildClubLabel = function() {
	return this._buildCharShape('\u2663', -4, -34, 1.09, 1.66);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildHeartLabel = function() {
	return this._buildCharShape('\u2665', -6, -34, 1.11, 1.66);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CalendarViewHelper.prototype.buildMusicLabel = function() {
	return this._buildCharShape('\u266b', -6, -8, 1.22, 1.24);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
//adapted from IDB.MetricsBoxView
IDB.CalendarViewHelper.prototype._buildCharShape = function(character, x, y, xs, ys) {
	var size = this._size;

	var lblOpt = {
		size: size,
		color: this._color,
		bold: true,
		align: 'center'
	};

	var lbl = new IDB.GraphLabel(lblOpt, character);

	lbl.getDisplayLabel()
		.x(x)
		.y(y)
		.scaleX(size/lbl.getDisplayLabel().width()*xs)
		.scaleY(size/lbl.getDisplayLabel().height()*ys);

	return lbl;
};

/*====================================================================================================*/

