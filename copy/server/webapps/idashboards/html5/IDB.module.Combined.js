'use strict';
IDB.CombinedGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	this._info.legendStyle = IDB.LS.AXES;
};

IDB.CombinedGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.CombinedGraph.prototype.constructor = IDB.CombinedGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.CombinedGraph.prototype._build = function() {
	var gc = this._graphContext;
	
	var yAxesCount = gc.datapointGrid.getNumYCols(true);
	var lines = gc.settings.get('CombinedNumLineAxes');
	lines = IDB.MathUtil.clamp(yAxesCount-1, 0, lines);
	gc.settings.set('CombinedNumLineAxes', lines);
	var bars = yAxesCount-lines;

	this._grid = new IDB.CartesianGrid(gc);
	var barModel = new IDB.BarPlot(gc, this._grid.getAxes(), 0, bars);
	var lineModel = new IDB.LinePlot(gc, this._grid.getAxes(), bars, lines);
	
	this._limitHold = new Kinetic.Group();

	this._gridView = new IDB.CartesianGridView(this._grid, this._limitHold);
	this._stageLayer.add(this._gridView.getDisplay());

	this._barPlotView = new IDB.BarPlotView(barModel);
	this._stageLayer.add(this._barPlotView.getDisplay());

	this._linePlotView = new IDB.LinePlotView(lineModel);
	this._stageLayer.add(this._linePlotView.getDisplay());
	
	this._stageLayer.add(this._limitHold);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.CombinedGraph.prototype._update = function(width, height) {
	var gv = this._gridView;
	var barView = this._barPlotView;
	var lineView = this._linePlotView;
	var barDisp = barView.getDisplay();
	var lineDisp = lineView.getDisplay();

	gv.setSize(width, height);

	var clip = {
		x: 0,
		y: -3,
		width: gv.getPlotWidth(),
		height: gv.getPlotHeight()+6
	};

	barDisp.x(gv.getPlotPosX());
	barDisp.y(gv.getPlotPosY());
	barView.setSize(gv.getPlotWidth(), gv.getPlotHeight());
	barDisp.clip(clip);

	lineDisp.x(gv.getPlotPosX());
	lineDisp.y(gv.getPlotPosY());
	lineView.setSize(gv.getPlotWidth(), gv.getPlotHeight());
	lineDisp.clip(clip);
};

/*====================================================================================================*/

