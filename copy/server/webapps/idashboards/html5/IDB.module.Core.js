/*global $*/
"use strict";
var IDB;
if(!IDB) {IDB = {};}
var $IDB = IDB;
IDB.debug = false;
//IDB.userdata and IDB.config are set elsewhere.
(function($) {

    $.fn.idbHide = function() {
        return this.css("visibility", "hidden");
    };
    $.fn.idbShow = function() {
        return this.css("visibility", "visible");
    };
    $.fn.idbVisible = function(visible) {
        return this.css("visibility", visible ? "visible" : "hidden");
    };
    $.fn.idbPosAbs = function() {
        return this.css("position", "absolute");
    };
    $.fn.idbTextToNumber = function(defaultVal) {
        var s, n;
        if(defaultVal === undefined) {
            defaultVal = -1;
        }
        s = $.trim(this.first().text());  // Read the text from the first element in this set only.
        if(!s) {
            return defaultVal;
        }
        n = Number(s);
        return isNaN(n) ? defaultVal : n;
    };
    $.fn.idbTextToBool = function(defaultVal) {
        var s;
        defaultVal = !!defaultVal;
        s = $.trim(this.first().text()).toLowerCase(); // Read the text from the first element in this set only.
        if(!s) {
            return defaultVal;
        }
        return (s === "true");
    };
    $.fn.idbFirstChild = function(selector) {
        if(selector === undefined) {
            selector = "*";
        }
        return this.children(selector).first();
    };
    $.fn.idbFirstChildText = function(selector) {
        return $.trim(this.idbFirstChild(selector).text());
    };
    $.fn.idbAttrToNumber = function(name, defaultVal) {
        var s, n;
        if(defaultVal === undefined) {
            defaultVal = -1;
        }
        s = this.attr(name);  // Read the text from the first element in this set only.
        if(!s) {
            return defaultVal;
        }
        n = Number(s);
        return isNaN(n) ? defaultVal : n;
    };
    $.fn.idbAttrToBool = function(name, defaultVal) {
        var s;
        defaultVal = !!defaultVal;
        s = $.trim(this.attr(name)).toLowerCase(); 
        if(!s) {
            return defaultVal;
        }
        return (s === "true");
    };
    $.fn.idbXYWH = function(xywh) {
        var px = "px";
        return this.css({"left":xywh.x+px, "top":xywh.y+px, "width":xywh.w+px, "height":xywh.h+px});
    };
    $.fn.idbXY = function(x, y) {
        var px = "px";
        return this.css({"left":x+px, "top":y+px});
    };
    $.fn.idbGlobalToLocal = function(pageX, pageY) {
        var me = this, offset = me.offset();
        return ({
            x:(pageX - offset.left),
            y:(pageY - offset.top)
        });
    };
    $.fn.idbLocalToGlobal = function(localX, localY) {
        var me = this, offset = me.offset();
        return ({
            x:(localX + offset.left),
            y:(localY + offset.top)
        });
    };

}(jQuery));
(function() {
    
    IDB.catsTitleClicked = function() {

    };
    IDB.center = function($of) {
        return {my:"center", at:"center", of:$of};
    };
    IDB.log = function(msg, header, footer) {
        
        if(console) {
            if(arguments.length > 1) {console.log(header);}
            console.log(msg);
            if(arguments.length > 2) {console.log(footer);}
         } 
    };
    IDB.toInt = function(val, min, max) { // used to simulate integer arithmetic, and optionally normalize to a given range.
        val = Math.floor(val);
        if(min !== undefined && val < min) {
            return min;
        }
        if(max !== undefined && val > max) {
            return max;
        }
        return val;
    };
    IDB.toBool = function(val, defaultVal) { 
        var s = $.trim(val || "").toLowerCase(); 
        if(!s) {
            return !!defaultVal;
        }
        return (s === "true");
    };

    IDB.footerMsg = function(msg) {
        $("#db-page #db-footer h4").text(msg);
    };
    IDB.listener = function(listenerObject, methodName){ 
        return function(){ 
            return listenerObject[methodName].apply(listenerObject, arguments); 
        };
    };
    IDB.leftClickListener = function(listenerObject, methodName){ 
        return function(evt){ 
            if(evt && evt.which && evt.which !== 1) {
                return;
            }
            return listenerObject[methodName].apply(listenerObject, arguments); 
        };
    };
    IDB.format = function(key) {
       if(!IDB.bundle) {return key;}
       var temp = IDB.bundle[key];
       if(!temp) {return key;}
       if(arguments.length > 1) {
           for(var j=1, jj=arguments.length; j<jj; j++) {
               var val = arguments[j];
               var token = "{" + (j-1) + "}";
               temp = temp.replace(token, val);
           }
       }
       return temp;
    };
    IDB.resolveUrl = function(url, useProxy) {
        if(!url) {return url;}
        if(url.indexOf("content:") === 0) {
            return url.replace(/^content:/, IDB.userdata.baseUrl + "/");
        }
        if(url.indexOf("graphic:") === 0) {
            return url.replace(/^graphic:/, IDB.config.contextRoot + "graphics/");
        }

        if(useProxy) {
            return IDB.config.proxyUrl.replace(/\{0\}/, encodeURIComponent(url));
        }
        return url;
    };
    IDB.resizeContent = function() {

        var dvp = (window.devicePixelRatio || 1 ), 
           mww = $.mobile.window.width(), mwh = $.mobile.window.height(),
           W = Math.max(160, (window.innerWidth || mww)), 
           H = Math.max(100, (window.innerHeight || mwh)), multiple = 1, $page = $("#db-page");

        function footerDbgMsg() {
            return ("W=" + W + ", H=" + H + ", DPR=" + dvp + ", KPR=" + Kinetic.pixelRatio + ", multiple=" + multiple + ", KV=" + Kinetic.version + ", M=" + !!Kinetic.UA.mobile);
        }

        if(IDB.debug) {
            IDB.footerMsg(footerDbgMsg());
        }
        IDB.log(footerDbgMsg());

        if($page.length === 0) {
            return;
        }


        // If screen is less than 800 and not in embedded viewer mode and noscale=true URL parameter was not found 
        if(W < 800 && !IDB.userdata.embedded && !IDB.NOSCALE) {
             multiple = Math.ceil(2048/W)/2;
             $("body").css({"overflow-x":"auto", "overflow-y":"auto"});
        }
        else {
             $("body").css({"overflow-x":"hidden", "overflow-y":"initial"});
        }
        if(IDB.debug) {
            IDB.footerMsg(footerDbgMsg());
        }

        $page.width(W*multiple).height(H*multiple);

        var $dbc = $("#db-content"), $dbh = $('#db-header'), $dbf = $('#db-footer'),
           cheight = $page.height() - $dbh.outerHeight() - $dbf.outerHeight();
        IDB.log("WINDOW HEIGHT: " + $page.height());
        $dbc.outerHeight(cheight);
        $('#curdash').outerHeight(cheight);
        IDB.log(footerDbgMsg());
    };

    IDB.isStartupDashboard = function() {
        return (IDB.userdata && IDB.currentDashboard && (IDB.userdata.favdash1 == IDB.currentDashboard.data.dashId));
    };
    IDB.toggleStartupDashboard = function() {
        $('div#db-settings-popup').popup('close');
        IDB.log("toggleStartupDashboard called.");
        var wasStartup = IDB.isStartupDashboard();
        var currDashId = IDB.currentDashboard.data.dashId;
        var cmd, req;
        if(wasStartup) {
            cmd = "clearStartupDashboard";
            req = IDB.RB.clearStartupDashboard();
        }
        else {
            cmd = "setStartupDashboard";
            req = IDB.RB.setStartupDashboard(currDashId);
        }

        

        var callback = function(result) {
             IDB.log("wasStartup: " + wasStartup + ", currDashId=" + currDashId + ", " + result.message);
             IDB.userdata.favdash1 = wasStartup ? -1 : currDashId;
             IDB.updateStartupItem();
        }; 
        //IDB.CQ.enQ = function(cmd, payloadPlusAdtlArgs, successCallback, failCallback, callbackContext, errFlags, warnFlags, okFlags) {

        IDB.CQ.enQ(cmd, req, callback);
    };
    IDB.updateStartupItem = function() {
       if(IDB.userdata.guest) return;
       var $smList = $("ul#db-settings-list", "div#db-settings-popup");
       var $item = $("a#startupMenuItem", $smList);
       if(!$item.length) {
           $item = $("<li data-icon=\"false\"><a class=\"ui-btn\" id=\"startupMenuItem\" href=\"#\" data-ajax=\"false\">XXX</a></li>");
           $item.appendTo($smList);
           $item = $("#startupMenuItem", $item);
           IDB.log("Found it: " + $item.length);
           $item.click(IDB.toggleStartupDashboard);
           //smList.listview("refresh");

            var $logout = $("<li data-icon=\"false\">");
            $("<a class=\"ui-btn\" data-ajax=\"false\" data-icon=\"false\" id=\"logoutMenuItem\">").appendTo($logout).html(IDB.format("dashboard.html5.logout", IDB.userdata.username)).attr("href", IDB.config.cmdPath + "logout");
            $logout.appendTo($smList);
       }
//       IDB.log("cmdPath: " + IDB.config.cmdPath);
       var txt = IDB.format(IDB.isStartupDashboard() ? "dashboard.html5.remove_startup" : "dashboard.html5.set_startup");
       $item.text(txt);


       
    };

    IDB.updateDebugBordersItem = function() {
       if(!IDB.debug) {
           return;
       }
       var $smList = $("ul#db-settings-list", "div#db-settings-popup");
       var $item = $("a#debugBordersItem", $smList);
       if(!$item.length) {
           $item = $("<li data-icon=\"false\"><a class=\"ui-btn\" id=\"debugBordersItem\" href=\"#\" data-ajax=\"false\">XXX</a></li>");
           $item.appendTo($smList);
           $item = $("#debugBordersItem", $item);
           IDB.log("Found it: " + $item.length);
           $item.click(IDB.toggleDebugBorders);
           //smList.listview("refresh");
           
       }
//       IDB.log("cmdPath: " + IDB.config.cmdPath);
       var txt = IDB.format(IDB.debugBorders ? "Hide Debug UI" : "Show Debug UI");
       if(IDB.debugBorders) {
           $("#db-content").addClass("idb-debug-ui");
       }
       else {
           $("#db-content").removeClass("idb-debug-ui");
       }
       $item.text(txt);
    };

    IDB.toggleDebugBorders = function() {
       IDB.debugBorders = !IDB.debugBorders;
       IDB.updateDebugBordersItem();
    };

    IDB.addDashboardRefreshItem = function() {
       var $sList = $("ul#db-settings-list", "div#db-settings-popup");
       var $item = $("a#dashboardRefreshItem", $sList);

       if(!$item.length) {
           $item = $("<li data-icon=\"false\"><a class=\"ui-btn\" id=\"dashboardRefreshItem\" href=\"#\" data-ajax=\"false\"></a></li>");
           $item.find('a').text(IDB.format("dashboard.html5.refresh_dashboard"));
           $item.click(IDB.refreshCurrentDashboardFrames);
           var $logoutMenuItem = $('a#logoutMenuItem', $sList);
           if($logoutMenuItem.length) { 
               $item.insertBefore($logoutMenuItem.parent('li'));
           }
           else {
               $item.appendTo($sList);
           }
       }
    };
    IDB.refreshCurrentDashboardFrames = function() {
        $('div#db-settings-popup').popup('close');
        IDB.currentDashboard.refreshFrames();
    };
    IDB.addDashboardViewAsReportItem = function() {
       var $sList = $("ul#db-settings-list", "div#db-settings-popup");
       var $item = $("a#dashboardViewAsReportItem", $sList);

       if(!$item.length) {
           $item = $("<li data-icon=\"false\"><a class=\"ui-btn\" id=\"dashboardViewAsReportItem\" href=\"#\" data-ajax=\"false\"></a></li>");
           $item.find('a').text(IDB.format("dashboard.html5.view_as_report"));
           $item.click(IDB.viewDashboardAsReport);
           var $logoutMenuItem = $('a#logoutMenuItem', $sList);
           if($logoutMenuItem.length) { 
               $item.insertBefore($logoutMenuItem.parent('li'));
           }
           else {
               $item.appendTo($sList);
           }
       }
    };

    IDB.viewDashboardAsReport = function() {
        $('div#db-settings-popup').popup('close');
        IDB.openDashboardAsReport();
    };
    IDB.openDashboardAsReport = function(format, token)
    {
        var currentDashboardBase = IDB.currentDashboard;
        if (!currentDashboardBase)
        {
            return;
        }
        if(!IDB.userdata.reportsAvailable)
        {
           IDB.MM.show(IDB.format("reports.html5.no_reports_available"));
           return;
        }
        var currentDashboardContext = currentDashboardBase.data;

        if (!currentDashboardContext.reportEnabled)
        {
            IDB.MM.show(IDB.format("dashboard.html5.reports_disabled"));
            return;
        }

        var dashId = currentDashboardContext.dashId;

        if (!format)
        {
            format = currentDashboardContext.reportFormat;
        }

        var urlFromSettings = IDB.ReportUtils.getDashReportUrl(dashId, format, true); 

        //
        // When this method is called, all parameters in the charts should be resolved, so there isn't any need to pass any additional value sources in the
        // request.
        //
        var url = 
            IDB.ReportUtils.buildLoadDashboardReportURL(currentDashboardBase, urlFromSettings, currentDashboardBase.drilldownFilters, currentDashboardBase.drilldownParamSet, token); 

        var target = IDB.getReportWindowName();
        var settings = IDB.getReportWindowSettings();
        var win = window.open(url, target, settings);
        if(win && target !== "_blank")
        {
            win.focus();
        }
    };

    IDB.addDisplayModeItems = function() {
        var $sList = $("ul#db-settings-list", "div#db-settings-popup");
        var $automaticModeItem = $("a#autoModeItem", $sList);

        if(!$automaticModeItem.length) {
            $automaticModeItem = $("<li data-icon=\"false\"><a class=\"ui-btn\" id=\"autoModeItem\" href=\"#\" data-ajax=\"false\"></a></li>");
            $automaticModeItem.find('a').text(IDB.format("dashboard.html5.display_mode_auto"));
            $automaticModeItem.on("click", IDB.setDisplayMode.bind(undefined, "auto"));
            var $noScaleModeItem = $("<li data-icon=\"false\"><a class=\"ui-btn\" id=\"noscaleModeItem\" href=\"#\" data-ajax=\"false\"></a></li>");
            $noScaleModeItem.find('a').text(IDB.format("dashboard.html5.display_mode_no_scale"));
            $noScaleModeItem.on("click", IDB.setDisplayMode.bind(undefined, "noscale"));
            var $phoneModeItem = $("<li data-icon=\"false\"><a class=\"ui-btn\" id=\"phoneModeItem\" href=\"#\" data-ajax=\"false\"></a></li>");
            $phoneModeItem.find('a').text(IDB.format("dashboard.html5.display_mode_phone"));
            $phoneModeItem.on("click", IDB.setDisplayMode.bind(undefined, "phone"));
            var $logoutMenuItem = $('a#logoutMenuItem', $sList);
            if($logoutMenuItem.length) { 
                $automaticModeItem.insertBefore($logoutMenuItem.parent('li'));
                $noScaleModeItem.insertBefore($logoutMenuItem.parent('li'));
                $phoneModeItem.insertBefore($logoutMenuItem.parent('li'));
            }
            else {
                $automaticModeItem.appendTo($sList);
                $noScaleModeItem.appendTo($sList);
                $phoneModeItem.appendTo($sList);
            }
        }
        var itemId = "autoModeItem";
        if (IDB.PHONEMODE)
        {
            itemId = "phoneModeItem";
        }
        else
        {
            if ((IDB.NOSCALE))
            {
                itemId = "noscaleModeItem";
            }
        }
        $("#" + itemId).addClass("ui-btn-icon-right ui-icon-check");
    };
    IDB.setDisplayMode = function(displayMode) {
        $("#db-settings-list .ui-btn-icon-right.ui-icon-check").removeClass("ui-btn-icon-right ui-icon-check");
        $("#" + displayMode + "ModeItem").addClass("ui-btn-icon-right ui-icon-check"); 
        $('div#db-settings-popup').popup('close');
        var prevPhoneMode = IDB.PHONEMODE;
        IDB.NOSCALE = false;
        IDB.PHONEMODE = false;
        if (displayMode === "auto")
        {
            if (prevPhoneMode)
            {
                IDB.phoneModeExit();
                IDB.reloadCurrentDashboard();
                return;
            }
            IDB.resizeContent();
            var dashFrames = IDB.currentDashboard.dashframes;
            for (var i = 0; i < dashFrames.length; i++)
            {
                dashFrames[i].resize();
            }
            $(window).resize();
            return;
        }
        if (displayMode === "noscale")
        {
            IDB.NOSCALE = true;
            if (prevPhoneMode)
            {
                IDB.phoneModeExit();
                IDB.reloadCurrentDashboard();
                return;
            }
            IDB.resizeContent();
            var dashFrames = IDB.currentDashboard.dashframes;
            for (var i = 0; i < dashFrames.length; i++)
            {
                dashFrames[i].resize();
            }
            $(window).resize();
            return;
        }
        if (displayMode === "phone")
        {
            IDB.NOSCALE = true;
            IDB.PHONEMODE = true;
            IDB.reloadCurrentDashboard();
            IDB.phoneModeInit();
            $(window).resize();
        }
    };
    IDB.updateDashboardParameterItem = function() {
        if(IDB.currentDashboard.dashboardParams && IDB.currentDashboard.dashboardParams.length > 0 && 
                  !IDB.currentDashboard.dashboardParameterInputRequired) {
            IDB.maybeAddDashboardParameterItem();
        }
        else {
            IDB.maybeRemoveDashboardParameterItem();
        }
    };

    IDB.maybeAddDashboardParameterItem = function() {
       var $sList = $("ul#db-settings-list", "div#db-settings-popup");
       var $item = $("a#dashboardParameterItem", $sList);

       if(!$item.length) {
           $item = $("<li data-icon=\"false\"><a class=\"ui-btn\" id=\"dashboardParameterItem\" href=\"#\" data-ajax=\"false\"></a></li>");
           $item.find('a').text(IDB.format("dashboard.html5.enter_parameters"));
           $item.click(IDB.showDashboardParameterInput);
           var $startupMenuItem = $('#startupMenuItem', $sList);
           if($startupMenuItem.length) {
               $item.insertAfter($startupMenuItem.parent('li'));
           }
           else {
               $item.prependTo($sList);
           }
       }
    };

    IDB.maybeRemoveDashboardParameterItem = function() {
       var $sList = $("ul#db-settings-list", "div#db-settings-popup");
       var $item = $("#dashboardParameterItem", $sList);
       if($item.length) {
          $item.parent('li').remove();
       }
    };

    IDB.showDashboardParameterInput = function() {
        $('div#db-settings-popup').popup('close');
        IDB.currentDashboard.maybeShowDashboardParameterInput();
    };

    IDB.setLoginInfo = function() {
        if(!IDB.userdata) {
            return;
        }
        var loginType = IDB.userdata.loginType = IDB.userdata.loginType || "normal";
        var username = IDB.userdata.username;
        var loginString = IDB.userdata.loginString;

        if(loginType === "normal") {
           return;
        }

        if(loginType === "sso") {
           if(loginString) {
               IDB.userdata.loginInfo = loginString;
           }
       }
       else if(username) {
           IDB.userdata.loginInfo = username;
       }
    };

    IDB.initDashboardPage = function(data) {
        if(!data) {return;}
        IDB.userdata = data;
        IDB.setLoginInfo();
        IDB.log("Hide cats: " + data.hideCats);
        var html = "<a id=\"db-back-btn\" class=\"ui-btn ui-btn-right ui-shadow ui-corner-all ui-icon-arrow-l ui-btn-icon-notext\" data-enhanced=\"true\" data-transition=\"slide\" data-role=\"button\" role=\"button\"></a>";
        var $a = $(html);
        $a.appendTo($("#db-header")).click(IDB.onBack);

        if(!data.hideCats) {
            var tip = IDB.format("dashboard.html5.back_to_categories");
            $a.text(tip);
            if($("#cats-page").length > 0) {
                // The cats-page is already present in the DOM, so just use its ID to
                // go back to it.
                if (IDB.getDashboardView(false) === "preview" && window.innerWidth >= IDB.galleryPreviewThreshold)
                {
                    $a.attr("href", "#gallery-page").attr("data-ajax", "true");
                }
                else
                {
                    $a.attr("href", "#cats-page").attr("data-ajax", "true");
                }
            }
            else {
               // If the document was intially loaded by a fresh load of the dashboard page
               // from a CMD=loadDashboard URL, then the dashboard page is only intialized once
               // and is never unloaded from the DOM. (See http://demos.jquerymobile.com/1.4.0/pages/, which says
               // "Note that the contents of the first page isn't removed from the DOM, only pages loaded in via AJAX.")
               // If we then go to the category page and 
               // load a dashboard from there, the new dashboard won't get initialized and you'll see
               // only the header and footer at the top of the page.
               // This will reload the entire document with the categories page, causing it to be the
               // "first page" of our single document, multi-page app.
               //
               // Login information for guest user and sso are provided in case the session times out and
               // relogin is required. Even though the server in the Flash client seems to support sso
               // autologin, it isn't supported in 7.5 and beyond so it will be omitted in the HTML5 client.
               //
               var homeUrl = IDB.config.contextRoot + "html5/reload/categories";
               if(IDB.userdata.guest) {
                   homeUrl = homeUrl + "?guestuser=" + IDB.userdata.username;
               }
/*
               else if(IDB.userdata.loginType === "sso") {
                   homeUrl = homeUrl + "?sso=" + IDB.userdata.loginInfo;
               }
*/
               $a.attr("href", homeUrl).attr("data-ajax", "false");
            }
        }
        else {
            $a.hide();
        }
        //$("ul#db-settings-list").listview();
    };

    IDB.loadDashboardPage = function(dashId) {
        sessionStorage.setItem("idb-dashId", dashId);
        var url = IDB.config.cmdPath + "loadDashboard&lf=category&dashID=" + dashId;
        $("body").pagecontainer("change", url, {transition:"slide", changeHash:false});
    };

    IDB.getPosition = function(event)
    {
        if (typeof event.originalEvent !== "undefined")
        {
            event = event.originalEvent;
        }
        var position = {x: 0, y: 0};
        if (typeof event.touches !== "undefined" && event.touches.length > 0)
        {
            if (typeof event.touches[0].pageX !== "undefined")
            {
                position.x = Math.floor(event.touches[0].pageX);
                position.y = Math.floor(event.touches[0].pageY);
                return position;
            }
        }
        if (typeof event.pageX !== "undefined")
        {
            position.x = event.pageX;
            position.y = event.pageY;
            return position;
        }
        throw "IDB.getPosition(): position properties are undefined";
    };

    IDB.positionPreview = function(x, y)
    {
        var offset = 4;
        var preview = document.getElementById("preview");
        if (typeof x === "undefined")
        {
            x = IDB.saveX;
            y = IDB.saveY;
        }
        var img = document.getElementById("preview-img");
        if (img.complete)
        {
            if (x + preview.offsetWidth + offset <= window.innerWidth)
            {
                preview.style.left = x + offset + "px";
            }
            else
            {
                preview.style.left = x - preview.offsetWidth - offset + "px";
            }
            if (y - window.pageYOffset - preview.offsetHeight - offset >= 0)
            {
                preview.style.top = y - preview.offsetHeight - offset + "px";
            }
            else
            {
                preview.style.top = y + offset + "px";
            }
            preview.style.visibility = "";
            img.style.visibility = "";
            document.getElementById("preview-header").style.visibility = "";
        }
        else
        {
            preview.style.left = x + "px";
            preview.style.top = y + "px";
        }
    };
    IDB.previewTimeout = null;
    IDB.touchPreview = function(event)
    {
        IDB.previewTimeout = setTimeout(function(){IDB.showPreview(event);}, 400);
    };
    
    IDB.previewNeverUrl = IDB.config.contextRoot + "html5/images/generic_dashboard_thumbnail_slash.jpg";
    IDB.previewNoneUrl = IDB.config.contextRoot + "html5/images/generic_dashboard_thumbnail.jpg";
    IDB.saveX = null;
    IDB.saveY = null;
    IDB.showPreview = function(event)
    {
        var preview = document.getElementById("preview");
        preview.style.visibility = "hidden";
        preview.style.display = "";
        var position = IDB.getPosition(event);
        IDB.saveX = position.x;
        IDB.saveY = position.y;
        var img = document.getElementById("preview-img");
        img.style.visibility = "hidden";
        if (event.data.previewPolicy === "never")
        {
            img.src = IDB.previewNeverUrl;
        }
        else
        {
            if (!event.data.hasPreview)
            {
                img.src = IDB.previewNoneUrl;
            }
            else
            {
                img.src = IDB.config.contextRoot + "snapshot/" + event.data.dashId;
            }
        }
        var header = document.getElementById("preview-header");
        header.style.visibility = "hidden";
        header.innerHTML = event.data.dashname;
        IDB.positionPreview(position.x, position.y);
    };
    
    IDB.movePreview = function(event)
    {
        var position = IDB.getPosition(event);
        IDB.positionPreview(position.x, position.y);
        event.preventDefault();
    };

    IDB.inhibitLoad = false;
    IDB.moveTouch = function(event)
    {
        IDB.inhibitLoad = true;
    };

    IDB.endPreview = function(event)
    {
        clearTimeout(IDB.previewTimeout);
        var preview = document.getElementById("preview");
        if (preview.style.display === "none" && !IDB.inhibitLoad)
        {
            IDB.loadDashboardPage(event.data.dashId);
        }
        else
        {
            preview.style.display = "none";
        }
        IDB.inhibitLoad = false;
    };
    IDB.previewError = function()
    {
        var img = document.getElementById("preview-img");
        if (img.src !== IDB.previewNeverUrl && img.src !== IDB.previewNoneUrl)
        {
            var dashId = parseInt(img.src.substring(img.src.lastIndexOf("/") + 1));
            for (var c = 0; c < categoryList.length; c++)
            {
                var category = categoryList[c];
                for (var d = 0; d < category.dashboards.length; d++)
                {
                    var dashboard = category.dashboards[d];
                    if (dashboard.dashId === dashId)
                    {
                        dashboard.hasPreview = false;
                        break;
                    }
                }
            }
            img.src = IDB.previewNoneUrl;
        }
    };
    IDB.cancelMenu = function(event)
    {
        event.preventDefault();
    };

    IDB.disableMouseEvent =  function(event)
    {
        // Check if event was dispatched by mouse action on devices that support both mouse and touch
        if ((typeof event.mozInputSource !== "undefined" && event.mozInputSource === 1) ||                              // firefox
            (typeof event.sourceCapabilities !== "undefined" && event.sourceCapabilities.firesTouchEvents === false) || // chrome
            (typeof event.pointerType !== "undefined" && event.pointerType === "mouse")                                 // IE
           )
        {
            return true;
        }
        event.preventDefault();
        event.stopPropagation();
        return false;
    };

    IDB.buildCategoryList = function($div, categoryList, selectedCategoryId)
    {

        categoryList.forEach(function(category) {
            var catDescr = category.categoryDescr;
            var dashboards = category.dashboards;
            var $catDiv = $("<div class='category-name'>").attr({"id": "category-" + category.categoryId,
                                           "data-role":"collapsible",
                                           "data-collapsed-icon":"arrow-r",
                                           "data-expanded-icon":"arrow-d",
                                           "data-inset":"false",
                                           "data-theme":"c",
                                           "data-content-theme":"d",
                                           "data-iconpos":"right"}).appendTo($div);
            $catDiv.on("collapsibleexpand", IDB.enableAllCategories);
            $catDiv.on("collapsiblecollapse", IDB.enableAllCategories);
            $("<h1>").text(catDescr).appendTo($catDiv);
            var $dashList = $("<ul class='dashList'>").attr({"data-role":"listview", 
                                             "data-inset":"true"}).appendTo($catDiv);
            if(dashboards.length > 5) {
                $dashList.attr({"data-filter":"true"}); 
            }

            dashboards.forEach(function(dashboard) {
                var anchor =
                    $("<a>").attr({id: "category-dashboard-" + dashboard.dashId})
                    .text(dashboard.dashname)
                    .on("click", IDB.loadDashboardPage.bind(undefined, dashboard.dashId));
                if (IDB.config.dashboardPreviewEnabled)
                {
                    anchor
                        .on("mouseover", dashboard, IDB.showPreview)
                        .on("touchstart", dashboard, IDB.touchPreview)
                        .on("mousemove", IDB.movePreview)
                        .on("touchmove", IDB.moveTouch)
                        .on("mouseout", dashboard, IDB.endPreview)
                        .on("touchend", dashboard, IDB.endPreview)
                        .on("contextmenu", IDB.cancelMenu);
                }
                anchor.appendTo($("<li>").appendTo($dashList));
             });
        });
        // Disable mouse events for dashboards on touch devices
        if (typeof ontouchstart !== "undefined")
        {
            var lists = document.getElementsByClassName("dashList");
            for (var i = 0; i < lists.length; i++)
            {
                lists[i].addEventListener("click", IDB.disableMouseEvent, true);
                lists[i].addEventListener("mouseover", IDB.disableMouseEvent, true);
                lists[i].addEventListener("mouseout", IDB.disableMouseEvent, true);
            }
        }
    };

    IDB.galleryPreviewNeverUrl = IDB.config.contextRoot + "html5/images/generic_dashboard_thumbnail_slash-wide.jpg";
    IDB.galleryPreviewNoneUrl = IDB.config.contextRoot + "html5/images/generic_dashboard_thumbnail-wide.jpg";
    IDB.galleryPreviewThreshold = 600;
    IDB.dashviewPending = false;
    IDB.dashboardViewMap =
    {
        "normal":        {view: "normal",  open: false},
        "normalopened":  {view: "normal",  open: true},
        "preview":       {view: "preview", open: false},
        "previewopened": {view: "preview", open: true}
    };
    IDB.getDashboardViewKey = function()
    {
        var dashboardView = "normal";
        if (IDB.config.dashboardPreviewEnabled)
        {
            dashboardView = IDB.config.dashboardListDisplayMode;
            if (IDB.config.dashboardListDisplayModeModifiable)
            {
                var savedView = localStorage.getItem("idb-dashboardView");
                if (savedView !== null)
                {
                    dashboardView = savedView;
                }
            }
        }
        return dashboardView;
    };
    IDB.getOpenAllCategories = function()
    {
        return IDB.dashboardViewMap[IDB.getDashboardViewKey()].open;
    };
    IDB.getCategoriesToOpen = function()
    {
        var categories = [];
        var open = IDB.HtmlUtils.getUrlParameter("open");
        if (open === null)
        {
            if (IDB.getOpenAllCategories())
            {
                open = "all";
            }
            else
            {
                open = "none";
            }
        }
        if (open !== "none")
        {
            if (open === "all")
            {
                for (var i = 0; i < categoryList.length; i++)
                {
                    categories.push(categoryList[i].categoryId);
                }
            }
            else
            {
                var categoryIds = open.split(",");
                for (var i = 0; i < categoryIds.length; i++)
                {
                    var id = parseFloat(categoryIds[i]);
                    if (isNaN(id))
                    {
                        IDB.MM.show(IDB.format("categories.html5.param.invalid_category") + categoryIds[i]);
                    }
                    else
                    {
                        var c;
                        for (c = 0; c < categoryList.length; c++)
                        {
                            if (id === categoryList[c].categoryId)
                            {
                                categories.push(id);
                                break;
                            }
                        }
                        if (c >= categoryList.length)
                        {
                            IDB.MM.show(IDB.format("categories.html5.param.category_not_found") + id);
                        }
                    }
                }
            }
        }
        return categories;
    };
    IDB.getDashboardView = function(showError)
    {
        if (typeof showError === "undefined")
        {
            showError = false;
        }
        var view = IDB.dashboardViewMap[IDB.getDashboardViewKey()].view;
        var preview = IDB.HtmlUtils.getUrlParameter("preview");
        if (preview !== null)
        {
            if (preview === "true")
            {
                view = "preview";
            }
            else
            {
                if (preview === "false")
                {
                    view = "normal";
                }
                else
                {
                    if (preview === "auto")
                    {
                        if (window.innerWidth < IDB.galleryPreviewThreshold)
                        {
                            view = "normal";
                        }
                        else
                        {
                            view = "preview";
                        }
                    }
                    else
                    {
                        var msg = IDB.format("categories.html5.param.invalid_preview") + preview;
                        if (showError)
                        {
                            IDB.MM.show(msg);
                        }
                        else
                        {
                            IDB.log(msg);
                        }
                    }
                }
            }
        }
        return view;
    };
    IDB.setDashboardView = function(showError)
    {
        if (typeof showError === "undefined")
        {
            showError = false;
        }
        if ($("#db-page").hasClass("ui-page-active"))
        {
            return;
        }
        var dashboardView = IDB.getDashboardView(showError);
        var previewParam = IDB.HtmlUtils.getUrlParameter("preview");
        if (dashboardView === "normal" ||
            ((previewParam === null || previewParam === "auto") && window.innerWidth < IDB.galleryPreviewThreshold))
        {
            if (!$("#cats-page").hasClass("ui-page-active"))
            {
                if (!IDB.dashviewPending)
                {
                    IDB.dashviewPending = true;
                    IDB.synchCollapsible("gallery-page", "cats-page");
                    $("body").pagecontainer("change", "#cats-page", {transion: "none", changeHash: false});
                }
            }
            else
            {
                 IDB.dashviewPending = false;
            }
            return;
        }
        if (dashboardView === "preview")
        {
            if (!$("#gallery-page").hasClass("ui-page-active"))
            {
                if (!IDB.dashviewPending)
                {
                    IDB.dashviewPending = true;
                    IDB.synchCollapsible("cats-page", "gallery-page");
                    $("body").pagecontainer("change", "#gallery-page", {transion: "none", changeHash: false});
                }
            }
            else
            {
                 IDB.dashviewPending = false;
            }
        }
    };
    IDB.synchCollapsible = function(srcPage, dstPage)
    {
        var srcCats = $("#" + srcPage).find(".ui-collapsible-content");
        var dstCats = $("#" + dstPage).find(".ui-collapsible");
        for (var i = 0; i < srcCats.length; i++)
        {
            if ($(srcCats[i]).attr("aria-hidden") === "true")
            {
                $(dstCats[i]).collapsible("collapse");
            }
            else
            {
                $(dstCats[i]).collapsible("expand");
            }
            var srcFilter = $(srcCats[i]).find(".ui-filterable");
            if (srcFilter.length > 0)
            {
                var srcFilterInput = $(srcCats[i]).find("input");
                var dstFilterInput = $(dstCats[i]).find("input");
                dstFilterInput.val(srcFilterInput.val());
                dstFilterInput.trigger("change");
            }
        }
    };
    IDB.galleryTimeout = null;
    IDB.touchGallery = function(event)
    {
        IDB.galleryTimeout = setTimeout(function(){IDB.moveTouch(event);}, 400);
    };
    IDB.endGallery = function(event)
    {
        clearTimeout(IDB.galleryTimeout);
        if (!IDB.inhibitLoad)
        {
            IDB.loadDashboardPage(event.data.dashId);
        }
        IDB.inhibitLoad = false;
    };
    IDB.buildGalleryDashboards = function(event)
    {
        var $dashList = $(event.target).find(".idb-gallery-container");
        if ($dashList.children().length !== 0)
        {
            return;
        }
        var categoryId = parseInt(event.target.id.substring(event.target.id.indexOf("-") + 1));
        var index;
        for (index = 0; index < categoryList.length; index++)
        {
            if (categoryList[index].categoryId === categoryId)
            {
                break;
            }
        }
        var dashboards = categoryList[index].dashboards;
        for (var i = 0; i < dashboards.length; i++)
        {   
            var dashboard = dashboards[i];
            var item = $("<div class='idb-gallery-item'>").attr({id: "gallery-dashboard-" + dashboard.dashId});
            var anchor = $("<a>").appendTo(item);
            var itemContainer = $("<div class='idb-gallery-item-container'>");
            var imageContainer = $("<div class='idb-gallery-image-container'>").appendTo(itemContainer);
            imageContainer.on("click", IDB.loadDashboardPage.bind(undefined, dashboard.dashId))
                          .on("touchstart", IDB.touchGallery)
                          .on("touchmove", IDB.moveTouch)
                          .on("touchend", dashboard, IDB.endGallery);

            var aspectRatio = $("<img class='idb-gallery-aspect-ratio'/>").appendTo(imageContainer);
            aspectRatio.attr({src: IDB.config.contextRoot + "html5/images/aspect-ratio.svg"});
            if (i === dashboards.length - 1)
            {
                aspectRatio.on("load", IDB.loadGalleryPreviews);
            }
            var previewContainer = $("<div class='idb-gallery-preview-container'>").appendTo(imageContainer);
            var preview = $("<img class='idb-gallery-preview'/>").attr({draggable: false}).appendTo(previewContainer);
            preview.on('dragstart', function(){return false;});
            var dataSrc = IDB.config.contextRoot + "snapshot/" + dashboard.dashId;
            if (!dashboard.hasPreview)
            {
                if (dashboard.previewPolicy === "never")
                {
                    dataSrc = IDB.galleryPreviewNeverUrl;
                }
                else
                {
                    dataSrc = IDB.galleryPreviewNoneUrl;
                }
            }
            else
            {
                preview.on("error", IDB.galleryPreviewError.bind(preview));
            }
            preview.attr({"data-src": dataSrc});
            preview.on("load", IDB.galleryFitPreview);
            $("<div class='idb-gallery-name-container idb-abbreviated'>").text(dashboard.dashname).appendTo(itemContainer);
            itemContainer.appendTo(anchor);
            item.appendTo($dashList);
        }
        setTimeout(IDB.loadGalleryPreviews, 1000);
        setTimeout(IDB.loadGalleryPreviews, 2000);
        setTimeout(IDB.loadGalleryPreviews, 3000);
        setTimeout(IDB.galleryAbbreviateName, 1000);
        setTimeout(IDB.galleryAbbreviateName, 2000);
    };
    IDB.buildGalleryList = function($div, categoryList, selectedCategoryId)
    {

        for (var i = 0; i < categoryList.length; i++)
        {
            var category = categoryList[i];
            var catDescr = category.categoryDescr;
            var dashboards = category.dashboards;
            var $catDiv = $("<div class='category-name'>").attr({"id":"gallery-" + category.categoryId,
                                           "data-role":"collapsible",
                                           "data-collapsed-icon":"arrow-r",
                                           "data-expanded-icon":"arrow-d",
                                           "data-inset":"false",
                                           "data-theme":"c",
                                           "data-content-theme":"d",
                                           "data-iconpos":"right"}).appendTo($div);
            $catDiv.on("collapsibleexpand", IDB.buildGalleryDashboards);
            $catDiv.on("collapsiblecollapse", IDB.loadGalleryPreviews);
            $catDiv.on("collapsibleexpand", IDB.enableAllCategories);
            $catDiv.on("collapsiblecollapse", IDB.enableAllCategories);

            $("<h1>").text(catDescr).appendTo($catDiv);
            var $dashList = $("<div class='idb-gallery-container'>").appendTo($catDiv);
            if(dashboards.length > 5) {
                $dashList.attr({"data-filter":"true"});
                $dashList.on("filterablefilter", IDB.loadGalleryPreviews);
            }
            $("<br style='clear: both'/>").appendTo($catDiv);

        }
        // Disable mouse events for dashboards on touch devices
        if (typeof ontouchstart !== "undefined")
        {
            var lists = document.getElementsByClassName("idb-gallery-container");
            for (var i = 0; i < lists.length; i++)
            {
                lists[i].addEventListener("click", IDB.disableMouseEvent, true);
                lists[i].addEventListener("mouseover", IDB.disableMouseEvent, true);
                lists[i].addEventListener("mouseout", IDB.disableMouseEvent, true);
            }
        }
    };
    
    IDB.setGalleryBackgroundColor = function()
    {
        var backgroundColor = null;
        for (var s = 0; s < document.styleSheets.length && backgroundColor === null; s++)
        {
            var styleSheet = document.styleSheets[s];
            if (styleSheet.href === null)
            {
                for (var r = 0; r < styleSheet.cssRules.length; r++)
                {
                    var cssRule = styleSheet.cssRules[r];
                    if (cssRule.selectorText.indexOf(".ui-page-theme-a") >= 0)
                    {
                        backgroundColor = cssRule.style["background-color"];
                        break;
                    }
                }
            }
        }
        if (backgroundColor !== null)
        {
            backgroundColor = backgroundColor.replace("rgb(", "rgba(").replace(")", ", 0.5)");
        }
        else
        {
            backgroundColor = "#C0C0C0";
        }
        $("#gallery-page").find(".ui-collapsible-content").css({"background-color": backgroundColor});
    };
    
    IDB.galleryPreviewError = function()
    {
        $(this).off("error", IDB.galleryPreviewError);
        $(this).attr({src: IDB.galleryPreviewNoneUrl});
    };
    IDB.galleryAbbreviateName = function()
    {
        var measureContainer = $("#measure-container");
        var measureHeight = measureContainer.height();
        
        $(".ui-collapsible-content[aria-hidden=false]").find(".idb-gallery-name-container").each
        (
            function()
            {
                var nameContainer = $(this);
                measureContainer.css({width:  nameContainer[0].offsetWidth + "px"});
                measureContainer.text(nameContainer.text());
                
                if (measureContainer.height() > measureHeight)
                {
                    nameContainer.attr({title: nameContainer.text()});
                    nameContainer.tooltip({duration: 1000});
                    nameContainer.css({overflow: ""});
                    
                    nameContainer.on("touchstart", IDB.galleryToggleTooltip)
                                 .on("touchend", IDB.galleryToggleTooltip);
                }
                else
                {
                    nameContainer.attr({title: null});
                    nameContainer.css({overflow: "visible"});
                    nameContainer.off("touchstart", IDB.galleryToggleTooltip)
                                 .off("touchend", IDB.galleryToggleTooltip);
                }
            }
        );    
    };
    IDB.galleryToggleTooltip = function(event)
    {
        if (event.type === "touchstart")
        {
            $(event.target).tooltip("open");
        }
        if (event.type === "touchend")
        {
            $(event.target).tooltip("close");
        }
    };
    IDB.galleryFitPreview = function(event)
    {
        var img = $(event.target);
        var container = img.parents(".idb-gallery-image-container")[0];
        if (img.height() > $(container).height())
        {
            img.addClass("idb-gallery-fit-height");
            var left = ($(container).width() - img.width()) / $(container).width() / 2 * 100;
            if (left > 0)
            {
                img.css({"left": left + "%"});
            }
        }
        else
        {
            var top = ($(container).height() - img.height()) / $(container).height() / 2 * 100;
            if (top > 0)
            {
                img.css({"top": top + "%"});
            }
        }
    };

    IDB.loadGalleryPreviews = function()
    {
        var windowRect = {top: 0, right: $(window).width(), bottom: $(window).height(), left: 0};
        var galleryHeader = $("#gallery-header");
        if (!galleryHeader.hasClass("ui-fixed-hidden"))
        {
            windowRect.top = galleryHeader.height();
        }
        $(".ui-collapsible-content[aria-hidden=false]").find(".idb-gallery-item").each
        (
            function()
            {
                var preview = $(this).find(".idb-gallery-preview");
                if (typeof preview.attr("data-src") !== "undefined")
                {
                    var rect = $(this)[0].getBoundingClientRect();
                    if (rect.bottom > windowRect.top &&
                        rect.top < windowRect.bottom &&
                        rect.right > windowRect.left &&
                        rect.left < windowRect.right
                       )
                    {
                        preview.prop({src: preview.attr("data-src")});
                        preview.attr({"data-src": null});
                    }
                }
            }
        );
    };
    IDB.galleryResizeWait = function()
    {
        clearTimeout(IDB.galleryTimeout);
        IDB.galleryTimeout = setTimeout(IDB.galleryResize, 50);
    };
    
    IDB.galleryResize = function()
    {
        IDB.setDashboardView();
        IDB.loadGalleryPreviews();
        IDB.galleryAbbreviateName();
    };
    
    
    IDB.allCategoriesClicked = function(event)
    {
        IDB.toggleAllCategories(event.target, $(event.target).hasClass("ui-icon-arrow-r"));
    };
    IDB.setAllCategoriesButtonState = function(button, state)
    {
        if (state === "opened")
        {
            button.addClass("ui-icon-arrow-d");
            button.removeClass("ui-icon-arrow-r");
        }
        if (state === "closed")
        {
            button.addClass("ui-icon-arrow-r");
            button.removeClass("ui-icon-arrow-d");
        }
    };
    IDB.toggleAllCategories = function(element, show)
    {
        if (!$(element).hasClass("ui-disabled"))
        {
            $(element).parents(".ui-popup").popup("close");
            var categories = $(element).parents(".ui-page").find("[data-role=collapsible]");
            if (show)
            {
                categories.collapsible("expand");
            }
            else
            {
                categories.collapsible("collapse");
            }
        }
    };
    
    IDB.enableAllCategories = function(event, ui)
    {
        var page = $(event.target).parents(".ui-page");
        if (page.find(".ui-collapsible-collapsed").length === page.find(".ui-collapsible").length) // all closed
        {
            page.find(".close-all-categories").addClass("ui-disabled");
            page.find(".open-all-categories").removeClass("ui-disabled");
            IDB.setAllCategoriesButtonState(page.find(".all-categories-btn"), "closed");
            return;
        }
        if (page.find(".ui-collapsible-collapsed").length === 0) // all opened
        {
            page.find(".open-all-categories").addClass("ui-disabled");
            page.find(".close-all-categories").removeClass("ui-disabled");
            IDB.setAllCategoriesButtonState(page.find(".all-categories-btn"), "opened");
            return;
        }
        page.find(".open-all-categories").removeClass("ui-disabled");
        page.find(".close-all-categories").removeClass("ui-disabled");
    };
    
    IDB.initDashboard = function(div, data) {
        IDB.currentDashboard = new IDB.DashboardBase(div, data, 
                                                 {dashboardParameterValuesChanged:IDB.dashboardParameterValuesChanged});
        IDB.addDashboardViewAsReportItem();
        IDB.updateStartupItem();
        IDB.updateDebugBordersItem();
        IDB.updateDashboardParameterItem();
        IDB.addDashboardRefreshItem();
        IDB.addDisplayModeItems();
    };

    IDB.dashboardParameterValuesChanged = function() {
        var currentDashboard = IDB.currentDashboard; 
        IDB.loadDashboard(currentDashboard.div, currentDashboard.data.dashId, false, 
                           currentDashboard.drilldownFilters, currentDashboard.drilldownParamSet, 
                                       currentDashboard.dashboardParams, false);
    };

    IDB.onBack = function(evt) {
        IDB.log(evt, "ON BACK");
        if(IDB.popDbStack()) {
            evt.preventDefault();
            if (!IDB.PHONEMODE)
            {
                IDB.phoneModeExit();
            }
            IDB.resizeContent();
            var dashFrames = IDB.currentDashboard.dashframes;
            for (var i = 0; i < dashFrames.length; i++)
            {
                dashFrames[i].resize();
            }
            $(window).resize();
            window.scrollTo(0, 0);
        }
    };

    IDB.popDbStack = function() {
        if(IDB.currentDashboard) {
            IDB.currentDashboard.destroy();
        }
        $("#curdash").remove();
        if(IDB.dbstack.length > 0) {
            IDB.currentDashboard = IDB.dbstack.pop();
            $(IDB.currentDashboard.div).appendTo($("#db-content"));
            $("h1#dashtitle", "div#db-header").text(IDB.currentDashboard.getTitle());
            IDB.currentDashboard.maybeStartRefresh();
            IDB.currentDashboard.maybeStartFrameRefresh();
            IDB.maybeShowBackButton();
            if (IDB.PHONEMODE)
            {
                IDB.phoneModeInit();
            }
            return true;
       }
       return false;
    };

    IDB.dbstack = [];

    IDB.maybeShowBackButton = function() {
        if(IDB.dbstack.length > 0 || !IDB.userdata.hideCats) {
            $("#db-back-btn").show();
        }
        else {
            $("#db-back-btn").hide();
        }
    };

    IDB.loadDashboard = function(div, dashId, applyAlwaysPrompt, drilldownFilters, drilldownParamSet, 
                                    dashboardParams, isDrilldown) {
        var request = IDB.RB.loadDashboard2(dashId, applyAlwaysPrompt, drilldownFilters, drilldownParamSet,
                                    dashboardParams, isDrilldown);
        IDB.log(request, "DASHBOARD REQUEST");

        var drilldownInfo = {"drilldownFilters":drilldownFilters, "drilldownParamSet":drilldownParamSet}; 
        //IDB.CQ.enQ = function(cmd, payloadPlusAdtlArgs, successCallback, failCallback, callbackContext, errFlags, warnFlags, okFlags) {
        if(isDrilldown) {
            IDB.CQ.enQ("loadDashboard2", [request, div, drilldownInfo], this.onCmdLoadDashboard2, null, this, (IDB.CQ.INVOKE_CALLBACK | IDB.CQ.DISPLAY_MESSAGE)); 
        }
        else {
            IDB.CQ.enQ("loadDashboard2", [request, div], this.onCmdLoadDashboard, null, this);
         }
    };

    IDB.drillToDashboard = function(caller, dashId, filters, drilldownParamSet) {
    
        IDB.log(caller, "DRILLDOWN TO " + dashId);
        IDB.log(filters, "FILTERS");
        IDB.log(drilldownParamSet, "DRILLDOWN PARAMETERS");


        var $newdiv = $("<div id=\"curdash\" style=\"width:100%;height:100%;\">");
        IDB.currentDashboard.detach();
        IDB.dbstack.push(IDB.currentDashboard);
        IDB.currentDashboard = null;
        $newdiv.appendTo($("#db-content"));
        IDB.resizeContent();

        IDB.loadDashboard($newdiv[0], dashId, true, filters, drilldownParamSet, null,  true);
    
    
//    //IDB.RB.loadDashboard2 = function(dashId, applyAlwaysPrompt, drilldownFilters, drilldownParamSet, dashboardParams,
//    //                                 isDrilldown, context, loginType, username, loadLoginInfo) {
//        var req = IDB.RB.loadDashboard2(dashId, false, filters, drilldownParamSet, null, true);
    };

    IDB.launchDashboard = function(caller, dashId, sourceDashboardParameters) {
        var $newdiv = $("<div id=\"curdash\" style=\"width:100%;height:100%;\">");
        IDB.currentDashboard.detach();
        IDB.dbstack.push(IDB.currentDashboard);
        IDB.currentDashboard = null;
        $newdiv.appendTo($("#db-content"));
        IDB.resizeContent();

        var sourceParamSet = {dashboardDrilldownParams:sourceDashboardParameters, chartDrilldownParams:[]};
        IDB.loadDashboard($newdiv[0], dashId, true, null, sourceParamSet, null, true);
    };

    IDB.replaceCurrentDashboard = function(dashId) {
        IDB.MM.closeAllDialogs();
        var $newdiv = $("<div id=\"curdash\" style=\"width:100%;height:100%;\">");
        if(IDB.currentDashboard) {
            IDB.currentDashboard.destroy();
        }
        IDB.currentDashboard = null;
        $newdiv.appendTo($("#db-content"));
        IDB.resizeContent();
        IDB.loadDashboard($newdiv[0], dashId);
    };


    IDB.reloadCurrentDashboard = function() {
        var $newdiv = $("<div id=\"curdash\" style=\"width:100%;height:100%;\">");
        IDB.currentDashboard.destroy();
        $newdiv.appendTo($("#db-content"));
        IDB.initDashboard($("#curdash")[0], IDB.currentDashboard.data);
        IDB.resizeContent();
        IDB.currentDashboard.load();
    };


    IDB.onCmdLoadDashboard = function(result, div) {

        if(!div) {
            if(IDB.currentDashboard) {
                div = IDB.currentDashboard.div;
            }
        }
        // check status
        $(div).replaceWith('<div id="curdash" style="width:100%;height:100%"/>');
        div = $('#curdash')[0];

        var data = result.dashboard;
        data.dashboardParameterInputRequired = (result.status == 1400); // PARAMETER_VALUE_REQUIRED
        IDB.currentDashboard = new IDB.DashboardBase(div, data,
                                         {dashboardParameterValuesChanged:IDB.dashboardParameterValuesChanged});
        IDB.updateDashboardParameterItem();
        IDB.resizeContent();
        IDB.currentDashboard.load();
//        if (IDB.PHONEMODE)
//        {
//            IDB.phoneModeInit();
//        }
    };

    IDB.onCmdLoadDashboard2 = function(result, div, drilldownInfo) {
        var I = IDB, CQ = I.CQ, data = result.dashboard;
        if(result.confirmation === CQ.ERR) {
            I.popDbStack();
            return;
        }


        IDB.log(result, "onCmdLoadDashboard2");
//      if(!div) {
//         if(IDB.currentDashboard) {
//             div = IDB.currentDashboard.div;
//         }
//      }
//      // check status
//      $(div).replaceWith('<div id="content" data-role="content"/>');
//      div = $('#db-content')[0];
      
        data.dashboardParameterInputRequired = (result.status == 1400); // PARAMETER_VALUE_REQUIRED
        I.currentDashboard = new IDB.DashboardBase(div, data, {dashboardParameterValuesChanged:IDB.dashboardParameterValuesChanged}, drilldownInfo);
        I.resizeContent();
        I.currentDashboard.load();
//        if (IDB.PHONEMODE)
//        {
//            IDB.phoneModeInit();
//        }
    };

    IDB.phoneModeFrameTypeSeq =
    {
        "chart": 3,     // sorted descending
        "image": 2,
        "undefined": 1, // unknown frame type
        "empty": 0      // specify null to remove frame
    };
    IDB.phoneModeSort = function(frameA, frameB)
    {
        //Use sequence information to order frames
        if (frameA.sequence !== frameB.sequence)
        {
            return (frameA.sequence - frameB.sequence) * -1; // Descending
        }
        return (frameA.area - frameB.area) * -1;  // Descending
    };
    IDB.phoneModeInit = function()
    {
        document.getElementById("db-page").classList.add("idb-phone-mode");
        var container = IDB.currentDashboard.$div[0];
        var frames = IDB.currentDashboard.dashframes;
        if (typeof IDB.currentDashboard.phoneModeFrames === "undefined")
        {
            IDB.currentDashboard.phoneModeFrames = [];
            
            for (var i = 0; i < frames.length; i++)
            {
                var frameType = frames[i].data.frameType;
                //if (IDB.phoneModeFrameTypeSeq[frameType] !== null)
                //{
                    // Add sequence information here
                    var phoneModeFrame = {layoutPane: frames[i].$div[0].parentNode, dashFrame: frames[i], sequence: 0, area: 0};
                    var sequence = IDB.phoneModeFrameTypeSeq[frameType];
                    if (typeof sequence !== "undefined")
                    {
                        phoneModeFrame.sequence = sequence;
                    }
                    else
                    {
                        phoneModeFrame.sequence = IDB.phoneModeFrameTypeSeq["undefined"];
                    }
                    var width = parseFloat(frames[i].$div[0].parentNode.style.width);
                    if (isNaN(width))
                    {
                        width = 0;
                    }
                    var height = parseFloat(frames[i].$div[0].parentNode.style.height);
                    if (isNaN(height))
                    {
                        height = 0;
                    }
                    phoneModeFrame.area = width * height;
                    IDB.currentDashboard.phoneModeFrames.push(phoneModeFrame);
                //}
            }
            //IDB.currentDashboard.phoneModeFrames.sort(IDB.phoneModeSort);
            while (container.children.length > 0)
            {
                container.removeChild(container.firstChild);
            }
            for (var i = 0; i < IDB.currentDashboard.phoneModeFrames.length; i++)
            {
                container.appendChild(IDB.currentDashboard.phoneModeFrames[i].layoutPane);
                IDB.currentDashboard.phoneModeFrames[i].dashFrame.phoneModeNavStack = [];

            }
        }

        container.style.width = (IDB.currentDashboard.phoneModeFrames.length * 100) + "%";

        var styleId = "phonemode-pane-width";
        var style = document.getElementById(styleId);
        if (style !== null)
        {
            style.parentNode.removeChild(style);
        }
        var width = Math.floor(100 / IDB.currentDashboard.phoneModeFrames.length * 1000) / 1000;
        style = document.createElement("STYLE");
        style.id = styleId;
        var css = document.createTextNode(".idb-phone-mode .ui-layout-pane:not(.param-pane):not(.idb-gf-div) {width: " + width + "% !important;}");
        style.appendChild(css);
        document.head.appendChild(style);
    
        var navbar = document.getElementsByClassName("idb-phone-mode-nav")[0];
        navbar.addEventListener("touchstart", IDB.phoneModeSwipeStart, true);
        navbar.addEventListener("mousedown", IDB.phoneModeSwipeStart, true);
        if (typeof IDB.currentDashboard.phoneModeIndex === "undefined")
        {
            IDB.currentDashboard.phoneModeIndex = 0;
        }
        else
        {
            IDB.phoneModeGoTo(IDB.currentDashboard.phoneModeFrames[IDB.currentDashboard.phoneModeIndex].dashFrame.frameId);
        }
        IDB.phoneModeResize();
        $(window).on("resize", IDB.phoneModeResize);
        IDB.phoneModeShowNav();
    };
    IDB.phoneModeExit = function()
    {
        $("#db-page").removeClass("idb-phone-mode");
        var container = IDB.currentDashboard.$div[0];
        container.style.width = "100%";
        $(window).off("resize", IDB.phoneModeResize);
    };
    IDB.phoneModeResize = function()
    {
        var container = document.getElementsByClassName("ui-layout-container")[0];
        container.style.marginLeft = (-container.children[IDB.currentDashboard.phoneModeIndex].offsetLeft) + "px";
        for (var i = 0; i < IDB.currentDashboard.phoneModeFrames.length; i++)
        {
            IDB.currentDashboard.phoneModeFrames[i].dashFrame.resize();
        }
    };
    IDB.phoneModeHideCallout = function()
    {
        var dashFrame = IDB.currentDashboard.phoneModeFrames[IDB.currentDashboard.phoneModeIndex].dashFrame;
        if (dashFrame.containsChart)
        {
            IDB.currentDashboard.datapointMouseOut(null);
            if (dashFrame._chart.$callout)
            {
                dashFrame._chart.$callout.callout("hide");
            }
        }
    };
    IDB.phoneModePrev = function()
    {
        IDB.phoneModeHideCallout();
        if (IDB.currentDashboard.phoneModeIndex > 0)
        {
            IDB.currentDashboard.phoneModeIndex -= 1;
            var container = document.getElementsByClassName("ui-layout-container")[0];
            container.style.marginLeft = (-container.children[IDB.currentDashboard.phoneModeIndex].offsetLeft) + "px";
            IDB.phoneModeShowNav();
        }
    };
    IDB.phoneModeNext = function()
    {
        IDB.phoneModeHideCallout();
        if (IDB.currentDashboard.phoneModeFrames.length > 0 && IDB.currentDashboard.phoneModeIndex < IDB.currentDashboard.phoneModeFrames.length - 1)
        {
            IDB.currentDashboard.phoneModeIndex += 1;
            var container = document.getElementsByClassName("ui-layout-container")[0];
            container.style.marginLeft = (-container.children[IDB.currentDashboard.phoneModeIndex].offsetLeft) + "px";
            IDB.phoneModeShowNav();
        }
    };
    IDB.phoneModeFrameIndex = function(frameId)
    {
        for (var i = 0; i < IDB.currentDashboard.phoneModeFrames.length; i++)
        {
            if (IDB.currentDashboard.phoneModeFrames[i].dashFrame.frameId === frameId)
            {
                return i;
            }
        }
        return null;
    };
    IDB.phoneModeGoTo = function(frameId)
    {
        IDB.phoneModeHideCallout();
        var frameIndex = IDB.phoneModeFrameIndex(frameId);
        if (frameIndex !== null)
        {
            IDB.currentDashboard.phoneModeIndex = frameIndex;
            document.getElementsByClassName("ui-layout-container")[0].style.marginLeft = (IDB.currentDashboard.phoneModeIndex * -100) + "%";
            IDB.phoneModeShowNav();
            return IDB.currentDashboard.phoneModeIndex;
        }
        return null;
    };
    IDB.phoneModeNavTo = function(frameId)
    {
        var frames = IDB.currentDashboard.phoneModeFrames;
        var index = IDB.currentDashboard.phoneModeIndex;
        var frameIndex = IDB.phoneModeFrameIndex(frameId);
        var stack = frames[frameIndex].dashFrame.phoneModeNavStack;
        var currentId = frames[index].dashFrame.frameId;
        if (IDB.phoneModeGoTo(frameId) !== null)
        {
            if (stack.length === 0 || stack[stack.length - 1] !== currentId)
            {
                stack.push(currentId);
            }
        }
    };
    IDB.phoneModeNavToPivot = function(axisName)
    {
        var frames = IDB.currentDashboard.phoneModeFrames;
        for (var f = 0; f < frames.length; f++)
        {
            var pivotAxes = frames[f].dashFrame._chart.pivotAxes;
            for (var p = 0; p < pivotAxes.length; p++)
            {
                if (pivotAxes[p].axisName === axisName)
                {
                    break;
                }
            }
            if (p < pivotAxes.length)
            {
                break;
            }
        }
        if (f < frames.length)
        {
            IDB.phoneModeGoTo(frames[f].dashFrame.frameId);
        }
    };
    IDB.phoneModeNavToParams = function()
    {
        if (IDB.phoneModeParamFrameId !== null)
        {
            IDB.phoneModeGoTo(IDB.phoneModeParamFrameId);
        }
    };
    IDB.phoneModeNavBack = function(dashFrame)
    {
        if (dashFrame.phoneModeNavStack.length > 0)
        {
            IDB.phoneModeGoTo(dashFrame.phoneModeNavStack.pop());
        }
    };
    IDB.phoneModeShowNav = function()
    {
        var button = document.getElementsByClassName("idb-phone-mode-prev")[0];
        var img = button.getElementsByTagName("IMG")[0];
        if (IDB.currentDashboard.phoneModeIndex === 0)
        {
            img.style.visibility = "hidden";
            button.addEventListener("touchstart", IDB.phoneModeNoSwipe, false);
            button.addEventListener("touchmove", IDB.phoneModeNoSwipe, false);
            button.addEventListener("mousedown", IDB.phoneModeNoSwipe, false);
        
        }
        else
        {
            img.style.visibility = "visible";
            button.removeEventListener("touchstart", IDB.phoneModeNoSwipe);
            button.removeEventListener("touchmove", IDB.phoneModeNoSwipe);
            button.removeEventListener("mousedown", IDB.phoneModeNoSwipe);
        }
        button = document.getElementsByClassName("idb-phone-mode-next")[0];
        img = button.getElementsByTagName("IMG")[0];
        if (IDB.currentDashboard.phoneModeFrames.length === 0 || IDB.currentDashboard.phoneModeIndex === IDB.currentDashboard.phoneModeFrames.length - 1)
        {
            img.style.visibility = "hidden";
            button.addEventListener("touchstart", IDB.phoneModeNoSwipe, false);
            button.addEventListener("touchmove", IDB.phoneModeNoSwipe, false);
            button.addEventListener("mousedown", IDB.phoneModeNoSwipe, false);
        }
        else
        {
            img.style.visibility = "visible";
            button.removeEventListener("touchstart", IDB.phoneModeNoSwipe);
            button.removeEventListener("touchmove", IDB.phoneModeNoSwipe);
            button.removeEventListener("mousedown", IDB.phoneModeNoSwipe);
        }
    };
    IDB.isDraggable = function(element)
    {
        var current = element;
        while (current.tagName !== "BODY")
        {
            if (current.classList.contains("ui-draggable"))
            {
                return true;
            }
            current = current.parentNode;
        }
        return false;
    };
    IDB.phoneModeSwipeStartX = null;
    IDB.phoneModeSwipeStart = function(event)
    {
        IDB.phoneModeHideCallout();
        if (!IDB.isDraggable(event.target))
        {
            IDB.phoneModeSwipeStartX = IDB.getPosition(event).x;
            var navbar = document.getElementsByClassName("idb-phone-mode-nav")[0];
            navbar.addEventListener("touchmove", IDB.phoneModeSwipeMove, false);
            navbar.addEventListener("mousemove", IDB.phoneModeSwipeMove, false);
            navbar.addEventListener("touchend", IDB.phoneModeSwipeEnd, false);
            navbar.addEventListener("mouseup", IDB.phoneModeSwipeEnd, false);
            navbar.addEventListener("touchcancel", IDB.phoneModeSwipeEnd, false);
            navbar.addEventListener("mouseout", IDB.phoneModeSwipeEnd, false);
            IDB.currentDashboard.$div[0].style.transition = "left 0s";
            IDB.currentDashboard.$div[0].style.webkitTransition = "left 0s";
            IDB.currentDashboard.$div[0].style.mozTransition = "left 0s";
        }
    };
    IDB.phoneModeSwipeMove = function(event)
    {
        IDB.currentDashboard.$div[0].style.left = (IDB.getPosition(event).x - IDB.phoneModeSwipeStartX) + "px";
        event.preventDefault();
    };
    IDB.phoneModeSwipeEnd = function(event)
    {
        var navbar = document.getElementsByClassName("idb-phone-mode-nav")[0];
        navbar.removeEventListener("touchmove", IDB.phoneModeSwipeMove);
        navbar.removeEventListener("mousemove", IDB.phoneModeSwipeMove);
        navbar.removeEventListener("touchend", IDB.phoneModeSwipeEnd);
        navbar.removeEventListener("mouseup", IDB.phoneModeSwipeEnd);
        navbar.removeEventListener("touchcancel", IDB.phoneModeSwipeEnd);
        navbar.removeEventListener("mouseout", IDB.phoneModeSwipeEnd);
        
        IDB.currentDashboard.$div[0].style.transition = "";
        IDB.currentDashboard.$div[0].style.webkitTransition = "";
        IDB.currentDashboard.$div[0].style.mozTransition = "";
        var left = parseFloat(IDB.currentDashboard.$div[0].style.left);
        IDB.currentDashboard.$div[0].style.left = 0;
        if (Math.abs(left) > IDB.currentDashboard.$parent[0].offsetWidth * 0.25)
        {
            if (left < 0)
            {
                IDB.phoneModeNext();
            }    
            if (left > 0)
            {
                IDB.phoneModePrev();
            }    
        }
    };
    IDB.phoneModeNoSwipe = function(event)
    {
        event.preventDefault();
        event.stopPropagation();
    };

    IDB.rgb = function(val, defaultVal) {
       var rgb = null;
       if(Array.isArray(val)) {
          rgb = {r:val[0], g:val[1], b:val[2]};
       }
       if(typeof val === "object") {return val;}

       if(typeof val === "number") {
           rgb = {r:(val & 0xFF0000) >> 16, g:(val & 0x00FF00) >> 8, b:(val & 0x0000FF)};
       }
       if(typeof val === "string") {
           val = val.replace(/^#/, "0x");
           var n = parseInt(val);
           if(isNaN(n)) {return defaultVal;}
           rgb = {r:(n & 0xFF0000) >> 16, g:(n & 0x00FF00) >> 8, b:(n & 0x0000FF)};
       }
       if(rgb) {
           rgb.css = IDB.rgbToCSS(rgb);
           return rgb;
       }
       return defaultVal;
    };

    IDB.rgb.BLACK = {r:0,g:0,b:0,i:0,css:"#000000",x:{r:255,g:255,b:255,i:16777215,css:"#FFFFFF"}};
    IDB.rgb.WHITE = {r:255,g:255,b:255,i:16777215,css:"#FFFFFF",x:{r:0,g:0,b:0,i:0,css:"#000000"}};

    /**
     * Works like IDB.rgb(), however the returned rgb will have a property "x" that will
     * be its contrasting color, unless the property "x" prexisted, in which case it will remain whatever it was.
     */
    IDB.rgbx = function(val, defaultVal) {
        var rgb = IDB.rgb(val, defaultVal);
        if(rgb && !rgb.x) {
            rgb.x = IDB.ColorHelper.getContrastingColor(rgb);
        }
        return rgb;
    };

    /**
     * Returns a string in the form "#RRGGBB" based on the
     * given RGB object.
     */
    IDB.rgbToCSS = function(rgb) {
       function hh(n) {
          var s = n.toString(16);
          return (s.length == 1 ? "0" + s : s).toUpperCase();
       }
       return ( "#" + hh(rgb.r) + hh(rgb.g) + hh(rgb.b));
    };

    IDB.toRGBA = function(color, alpha) {
        var rgb = IDB.rgb(color);
        return "rgba(" + rgb.r + "," + rgb.g + "," + rgb.b + "," + alpha + ")";
    };
    IDB.toRGB = function(color) {
        var rgb = IDB.rgb(color);
        return "rgb(" + rgb.r + "," + rgb.g + "," + rgb.b + ")";       
    };   
    IDB.getRGB = function(color) {
        var rgb = [];
        rgb[0] = ((color & 0xFF0000) >> 16);
        rgb[1] = ((color & 0x00FF00) >> 8);
        rgb[2] = (color & 0x0000FF);
        return rgb;
    };
    IDB.normalizeUrl = function(url) {
        if(!url) {return url;}
        if(url.indexOf("/") !== 0 && url.indexOf("http://") !== 0 && 
            url.indexOf("https://") !== 0) {
            url = "http://" + url;
        }
        return url;
    };

    IDB.compNull = function(v1, v2) {
        var nv1 = (v1===null || v1===undefined);
        var nv2 = (v2===null || v2===undefined);
        if(nv1) return nv2 ? 0 : -1;
        return (nv2 ? 1 : 0);
    };

    IDB.isUndef = function(val) {
        return (typeof(val) === 'undefined');
    };

    IDB.ifUndef = function(val, valIfUndef) {
        return (IDB.isUndef(val) ? valIfUndef : val);
    };

    IDB.isNil = function(val) {
        return ((typeof(val) === 'undefined') || (val === null));
    };

    IDB.ifNil = function(val, valIfNil) {
        return (IDB.isNil(val) ? valIfNil : val);
    };

    /**
     * <p>Returns an object containing position and size information (top, left, width, height, css) 
     * for a child object, based on the parent object's width and height, desired margins (top, left, bottom, right),
     * and constraints on the size of the child object (maxWidth, maxHeight, minWidth, minHeight, and optionally absMinWidth and absMinHeight). The top, left,
     * width and height properties of the returned object will all be numbers, however the returned object will
     * also have a property named "css", which is an object containing the top, left, width and height properties
     * in CSS-compatible "Npx" format.</p>
     *
     * <p>There will always be minWidth and minHeight constraints in effect, and neither will be below 1px. (Default is 10).
     * If the maxWidth is missing or less than the applied minWidth, then the child will be allowed to extend to the
     * left and right margins, and accordingly, if the maxHeight is missing or less than the applied minHeight,
     * then the child will be allowed to extend to the top and bottom margins.</p>
     * 
     * <p>When minWidth or minHeight is reached, the margins will be shrunk, however, when the margins are 0, the object will be shrunk, but not smaller
     * than absMinWidth X absMinHeight.</p>
     */
    IDB.getChildPos = function(parentWidth, parentHeight, margins, constraints) {
        
        margins = $.extend({top:0, right:0, bottom:0, left:0}, margins);
        constraints = $.extend({maxWidth:0, maxHeight:0, minWidth:10, minHeight:10, absMinWidth:0, absMinHeight:0}, constraints);
    
        var w = parentWidth, h = parentHeight, M = margins, C = constraints, 
            tm = M.top, bm = M.bottom, lm = M.left, rm = M.right,
            verM = tm + bm, horM = lm + rm,
            maxW = C.maxWidth, maxH = C.maxHeight,
            minW = Math.max(C.minWidth, 1), 
            minH = Math.max(C.minHeight, 1),  
            absMinW = (C.absMinWidth > 0 ? Math.min(C.absMinWidth,  minW) : minW),
            absMinH = (C.absMinHeight > 0 ? Math.min(C.absMinHeight,  minH) : minH),
            availW = w - horM, availH = h - verM, shortage = 0,
            A, G, childWidth, childHeight, leftPart, topPart;
    
    //        IDB.log("w=" + w + ", h=" + h + ", minH=" + minH + ", minW=" + minW + ", maxH=" + maxH + ", maxW=" + maxW
    //           + ", availW=" + availW + ", availH=" + availH);
    
        // If there isn't enough width between the margins, reduce them proportionally to gain the needed space.
        if((availW < minW) && (horM > 0)) {
            shortage = minW - availW;
            leftPart = ((lm / horM) * shortage );
//            IDB.log("GFrame.resize(): shortage=" + shortage + ", leftPart=" + leftPart);
            lm = Math.max(0, lm - leftPart);
            rm = Math.max(0, rm - (shortage - leftPart));
            horM = lm + rm;
        }
    
        if((availH < minH) && (verM > 0)) {
            shortage = minH - availH;
            topPart = ((tm / verM) * shortage);
//            IDB.log("GFrame.resize(): topPart=" + topPart + ", shortage=" + shortage);
    
            tm = Math.max(0, tm - topPart);
            bm = Math.max(0, bm - (shortage - topPart));
            verM = tm + bm;
        }
    
        // Rectangle for the margins.
        A = {x:lm, y:tm, width:w-rm-lm, height:h-tm-bm};
    
        // always honor the minimum dimensions
        if(horM === 0) {
            childWidth = Math.max(absMinW, A.width);
        }
        else {
            childWidth = Math.max(minW, A.width);
        }
        if(verM === 0) {
            childHeight = Math.max(absMinH, A.height);
        }
        else {
            childHeight = Math.max(minH, A.height);
        }
    
        // apply a max dimension only if it is >= the corresponding min dimension,
        // otherwise, let it fill out the margins in that direction.
        if(maxW >= minW) {childWidth = Math.min(childWidth, maxW);}
        if(maxH >= minH) {childHeight = Math.min(childHeight, maxH);}
    
        var G = {
             left:Math.max(0, A.x + (0.5*A.width) - (0.5*childWidth)), 
             top:Math.max(0, A.y + (0.5*A.height) - (0.5*childHeight)),
             width:childWidth, 
             height:childHeight
        };
    
        G.css = {
            left:G.left + "px",
            top:G.top + "px",
            width:G.width + "px",
            height:G.height + "px"
        };
    
        return G;
    
    };

    IDB.show = function(c, sf) {

        IDB.log("sf=" + sf);

        return function(e) {
            if ($("#" + sf).hasClass("ui-page-active"))
            {
                var u = IDB.config.cmdPath + c;
                if (sf) {
                    u = u + "&sf=" + sf;
                }
                IDB.log(e, "Invoking " + u);
                $("body").pagecontainer("change", u, {
                    type:"get",
                    ajax:true,
                    transition:"slide",
                    changeHash:false
                    });
                e.preventDefault();
            }
        };
    };

    IDB.bmenu = function(b, m) {
       var $b = $("#" + b);
       var $m = $("#" + m);
       IDB.log($b, "BUTTON");
       IDB.log($m, "MENU");
       $b.on("click", function(e) {
          setTimeout(function() {
               IDB.log("About to open......");
               $m.popup("open", {
                   positionTo:"#" + b,
                   changeHash:false
               });
             }, 1000);
           e.preventDefault();
       });
    };


    IDB.Util = {
        copyProps:function(fromObj, toObj, preserve) {
           for(var propName in fromObj) {
               if(!preserve) {
                   toObj[propName] = fromObj[propName];
               }
               else if(!(propName in toObj)) {
                   toObj[propName] = fromObj[propName];
               }
           }
           return toObj;
        },
        objToString:function(obj) {
           if(obj === undefined) {return "undefined";}
           if(obj === null) {return "null";}
           var s = "{";
           var started = false;
           for(var propName in obj) {
               if(obj.hasOwnProperty(propName)) {
                   if(started) {
                       s += ", ";
                   }
                   started = true;
                   s += (propName + "=" + obj[propName]);
               }
           }
           return (s + "}");
        }
      }; 

})();

(function(IDB, Kinetic) {
    var normalizeAngle,
        validShapes = {circle:true,square:true,poly:true,star:true},
        regex = /poly_([3-8])(_(-?\d+(\.\d+)?))?$/,
        isValidShape = function(shape) {
            return !!validShapes[shape];
        }, 
        parsePoly, normalizeShape, kShape, kIcon,
        constructors = {
            "poly":Kinetic.RegularPolygon,
            "square":Kinetic.Rect,
            "star":Kinetic.Star,
            "circle":Kinetic.Circle
        };

    IDB.isValidShape = isValidShape;

    IDB.normalizeAngle = normalizeAngle = function(angle) {
        angle = Number(angle);
        if(isNaN(angle)) {
            return 0;
        }
        angle = angle % 360;
        if(angle > 180) {
           angle -= 360;
        }
        else if(angle < -180) {
           angle += 360;
        }
        return angle;
    };

    IDB.parsePoly = parsePoly = function(shape, defaultAngle) {
        var match = shape.match(regex), angle;
        if(!match || match.length === 0) {
            return null;
        }
        defaultAngle = defaultAngle || 0;
        if(match.length > 3) {
            angle = normalizeAngle(Number(match[3]));
        }
        else {
            angle = defaultAngle;
        }
        return {sides:Number(match[1]), rotation:angle};
    };

    IDB.normalizeShape = normalizeShape = function(shape) {
        if(!shape) return "circle";
        shape = $.trim(shape).toLowerCase();
        if(isValidShape(shape)) return shape;
        if("star".indexOf(shape) === 0 && shape.indexOf("st") === 0) return "star";
        if("square".indexOf(shape) === 0 && shape.indexOf("sq") === 0) return "square";
        if("circle".indexOf(shape) === 0) return "circle";
        if("polygon".indexOf(shape) === 0) return "poly";
        var match = shape.match(regex);
        if(match && match.length > 0) {
            return "poly";
        }
        return "circle";
    };

    IDB.kShape = kShape = function(shape, colorCss, size, config) {
       var color = (colorCss || "#000000"), s = normalizeShape(shape),
           cons = constructors[s],
           polyAttrs, params = {fill:color}, halfSize;

       size = size || 14;
       halfSize = size / 2;

       switch(s) {
           case "poly":
               params = $.extend(params, {radius:halfSize, sides:4, rotation:0}, parsePoly(shape), config);
               break;
           case "square" :
               params = $.extend(params, {width:size, height:size, offsetX:halfSize, offsetY:halfSize}, config);
               break;
            case "star" :
               params = $.extend(params, {numPoints:5, outerRadius:halfSize, innerRadius:(0.18163563199227753*size)}, config);
               break
            default:
               params = $.extend(params, {radius:halfSize}, config);
       }

       return new cons(params);
    };

    IDB.kShapeSize = function(ks, size) {
        var halfSize = size/2;
        switch(ks.className) {
            case "RegularPolygon" :
            case "Circle" :
               ks.setAttrs({radius:halfSize});
               break;
            case "Star" :
               ks.setAttrs({outerRadius:halfSize, innerRadius:(0.18163563199227753*size)});
               break;
            case "Rect" :
               ks.setAttrs({width:size, height:size, offsetX:halfSize, offsetY:halfSize});
        }
    };


    IDB.$kIcon = function(shape, colorCss, size, config) {
        var theSize = (size || 16), halfSize = theSize/2, theConfig = $.extend((config || {}), {x:halfSize, y:halfSize}),
           kineticShape = kShape((shape || "square"), colorCss, theSize, theConfig),
           $div = $('<div>').css({width:theSize+"px", height:theSize+"px"}),
           stage = new Kinetic.Stage({container:$div[0], width:theSize, height:theSize}),
           layer = new Kinetic.Layer();
        layer.add(kineticShape);
        stage.add(layer);
        stage.draw();
        return $div;
    };

})(IDB, Kinetic);


IDB.$dataRowsToTable = function(dataRows) {
    var $table = $("<table>").css({"border-collapse":"collapse"});
    for(var rowIndex=0; rowIndex<dataRows.length; rowIndex++) {
        var dataRow = dataRows[rowIndex];
        var $row = $("<tr>").appendTo($table);
        for(var j=0, jj=dataRow.length; j<jj; j++) {
            $("<td>").appendTo($row).text(dataRow[j].fv).css({"padding":"2px 5px", "border":"1px solid"});
        }
    }
    return $table;
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.CRI (Chart Request Info)
IDB.CRI = function(originator, chartLoadFrom, request, filters, drilldownParamSet) {
    this.originator = originator;
    this.chartLoadFrom = chartLoadFrom;
    this.request = request;
    this.filters = filters || [];
    this.drilldownParamSet = drilldownParamSet || null;
    this.isDrillback = false;
    IDB.log(this, "CRI");
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.CLF (Chart Loaded From)
IDB.CLF = {
    DASHBOARD_LOAD:"dashboardLoad",
    DRILLDOWN:"drilldown",
    DIALOG:"dialog",
    CHART_SAVE:"chartSave"
};

IDB.jqmevents = [
"hashchange",
"mobileinit",
"navigate",
"orientationchange",
"pagebeforechange",
"pagebeforecreate",
"pagebeforehide",
"pagebeforeload",
"pagebeforeshow",
"pagechange",
"pagechangefailed",
"pagecreate",
"pagehide",
"pageinit",
"pageload",
"pageloadfailed",
"pageremove",
"pageshow",
"scrollstart",
"scrollstop",
"swipe",
"swipeleft",
"swiperight",
"tap",
"taphold",
"throttledresize",
"updatelayout",
"vclick"
//"vmousecancel",
//"vmousedown",
//"vmousemove",
//"vmouseout",
//"vmouseover",
//"vmouseup",
];

IDB.logJqmEvents = function(pageId) {
   var $page = $("#" + pageId);
   for(var j=0, jj=IDB.jqmevents.length; j<jj; j++) {
      var evtname = IDB.jqmevents[j];
      $page.on(evtname, xx(evtname));   
   }

   function xx(ename) {
       return function() {
          IDB.log(arguments, "################################################# EVENT: " + pageId + " - " + ename);
       };
   }
};

IDB.saveUserSettings = function(settings) {
   

};

IDB.launcherNum = 1;

IDB.getReportWindowSettings = function() {return null;}; // null opens window as new tab
IDB.getReportWindowName = function() {return "report_launcher_" + IDB.launcherNum++;};
/*global IDB */
IDB.ColorHelper = {};

Object.defineProperties(IDB.ColorHelper, {

	LUM_R: {get: function() { return 0.212671; }},
    LUM_G: {get: function() { return 0.715160; }},
    LUM_B: {get: function() { return 0.072169; }}
});

IDB.ColorHelper._BLACK_RGB = { r: 0, g: 0, b: 0, css: '#000000', i: 0 };
IDB.ColorHelper._WHITE_RGB = { r: 255, g: 255, b: 255, css: '#FFFFFF', i: 16777215 };

/**
 * Given the Red, Green and Blue components of an RGB color, this function returns the color's
 * Luminance value by using the formula:
 * <code>Luminance = 0.212671 ~~ R + 0.715160 ~~ G + 0.072169 ~~ B</code>
 *
 * @param R a Number from 0 to 255 representing the red component of a color in the RGB color space.
 * @param G a Number from 0 to 255 representing the green component of a color in the RGB color space.
 * @param B a Number from 0 to 255 representing the blue component of a color in the RGB color space.
 * 
 * @return a Number from 0 to 255 (max 2 decimal places) representing the Luminance value of the 
 * color that would be created by combining the given R, G and B values into an RGB color.
 * 
 * @see http://www.faqs.org/faqs/graphics/colorspace-faq/
 */
IDB.ColorHelper.rgbToLuminance = function(R, G, B) {

    return ((0.212671 * R + 0.715160 * G + 0.072169 * B).toFixed(2));

};

/**
 * Given the Red, Green and Blue components of an RGB color, this function calculates the equivalant
 * HSV color and returns an Object with the properties "h" &#x2014; the Hue component, "s" &#x2014; the Saturation component)
 * and "v" &#x2014; the Value component of the resulting HSV value.
 *
 * @param R a Number from 0 to 255 representing the red component of a color in the RGB color space.
 * @param G a Number from 0 to 255 representing the green component of a color in the RGB color space.
 * @param B a Number from 0 to 255 representing the blue component of a color in the RGB color space.
 * 
 * @return an Object with the properties "h" &#x2014; the Hue component, "s" &#x2014; the Saturation component,
 * and "v" &#x2014; the Value component of the resulting HSV value.
 */
IDB.ColorHelper.rgbToHSV = function(R, G, B) {

    var h = -1, s = 0,  
        rVal = R / 255, 
        gVal = G / 255, 
        bVal = B / 255, 
        maxVal = Math.max(rVal, Math.max(gVal, bVal)), 
        minVal = Math.min(rVal, Math.min(gVal, bVal)),
        delta = maxVal - minVal,
        v = maxVal * 100;

    if(maxVal !== 0) {
        s = (delta / maxVal) * 100;
    }
    else { 
        // r = g = b = 0, black.
        s = 0;
        h = -1;
        return {h:h, s:s, v:v};
    }

    if(s === 0) { // happens when r == g == b.
        // hue is undefined, so we're done.
        return {h:-1, s:s, v:v};
    }

    if(rVal == maxVal) {
        h = (gVal - bVal) / delta;   // between yellow & magenta
    }
    else if(gVal == maxVal) {
        h = 2 + (bVal - rVal) / delta;  // between cyan & yellow
    }
    else {
        h = 4 + (rVal - gVal) / delta;  // between magenta & cyan
    }

    h = h * 60;
    if(h < 0) h += 360;

    return {h:h, s:s, v:v};

};

IDB.ColorHelper.normalize = function(val, min, max) {
    if(min===undefined){min = 0;}
    if(max===undefined){max=255;}
    if(val < min) return min;
    if(val > max) return max;
    return val;
};

/**
 * Converts an HSV color to RGB.
 * 
 * @param h hue - 0 to 359 are valid hues; -1 means "undefined", which is the case for shades of gray.
 * @param s saturation - 0 to 100 (Saturation is represented as a percentage, rather than a fraction.)
 * @param v value - 0 to 100 (Value is represented as a percentage, rather than a fraction.)
 * 
 * @return an RGB representing the color.
 */
IDB.ColorHelper.hsvToColor = function(h, s, v) {
    var I = IDB, CH = I.ColorHelper, vFract, sFract, val, r = 0, g = 0, b = 0, i, f, p, q, t, rgb;
    h = h % 360;
    s = CH.normalize(s, 0, 100);
    v = CH.normalize(v, 0, 100);

//            trace("ColorHelper.hsvToRGB(): h=" + h + ",s=" + s + ",v=" + v);

    // convert v to fraction for calculations:
    vFract = v / 100;

    if(s === 0) {  // some shade of gray, determined by v.
        val = Math.floor(vFract * 255);
        return {r:val,g:val,b:val};
    }

    // convert s to a fraction for calculations
    sFract = s / 100;

    h = h / 60;  // sector 0 to 5

    i = Math.floor(h);  // integer part of h
    f = h - i; // fractional part of h
    p = vFract * (1 - sFract);
    q = vFract * (1 - sFract * f);
    t = vFract * (1 - sFract * (1 - f));
//            trace("ColorHelper.hsvToRGB(): h=" + h + ",i=" + i + ",f=" + f + ",p=" + p + ",q=" + q + ",t=" + t);
    switch(i) {
        case 0 : 
            r = vFract; 
            g = t; 
            b = p; 
            break;
        case 1 :
            r = q;
            g = vFract;
            b = p;
            break;
        case 2 :
            r = p;
            g = vFract;
            b = t;
            break;
        case 3 :
            r = p;
            g = q;
            b = vFract;
            break;
        case 4 :
            r = t;
            g = p;
            b = vFract;
            break;
        default:
            r = vFract;
            g = p;
            b = q;
            break;
    }

//            trace("ColorHelper.hsvToRGB(): r=" + r + ",g=" + g + ",b=" + b);


    rgb = {r:Math.round(r*255), g:Math.round(g*255), b:Math.round(b*255)};
    rgb.css = I.rgbToCSS(rgb);
    return rgb;

};


/**
 * Returns a color that contrasts sufficiently from the given color so
 * that that when the two colors are combined as text and background colors,
 * the text will be readable against the background. First, the given color is classified as a
 * "light" or "dark" color, by calculating its Luminance value on a scale of 0 to 255, and
 * considering it "light" if its Luminance is greater than 127 and "dark" otherwise.
 * 
 * <p>If the color is "light", the returned color is close to black. (Black with a little
 * of the original color mixed in.) If the color is "dark", then the returned color is close
 * to white. (White with a little of the original color mixed in.)</p>
 * 
 * @param color an RGB or a uint representing an RGB color value.
 * @return an RGB representing a contrasting color.
 * 
 * @see #rgbToLuminance()
 */
IDB.ColorHelper.getContrastingColor = function(color) {

    var I = IDB, CH = I.ColorHelper,
        rgb = I.rgb(color), 
        lum = CH.rgbToLuminance(rgb.r,  rgb.g,  rgb.b),
        hsv = CH.rgbToHSV(rgb.r,  rgb.g,  rgb.b);
    
    if(lum > 127) { // light color

        // reduce the Value component of the HSV color 1o 10. This will make it near-black
        // while preserving the hue.
        hsv.v = 10;
    }
    else { // dark color

        // increase the Value to 90 but reduce the Saturation to 10. This
        // will make it near-white while preserving the hue.
        hsv.v = 90;
        hsv.s = Math.min(10, hsv.s);
    }
        
    return CH.hsvToColor(hsv.h,  hsv.s,  hsv.v);


};

IDB.ColorHelper.getSimpleContrastingColor = function(color) {
    var I = IDB, CH = I.ColorHelper,
	lum = CH.rgbToLuminance(color.r, color.g, color.b);
	return (lum > 127 ? CH._BLACK_RGB: CH._WHITE_RGB);
};

IDB.ColorHelper.getHSVColorProgression = function(beginColor, endColor, numColors, reverse) { // (beginColor:uint, endColor:uint, numColors:int, reverse:Boolean):Array {
    var I = IDB, CH = I.ColorHelper;
    beginColor = I.rgb(beginColor);
    endColor = I.rgb(endColor);

    numColors = numColors || 5;
    reverse = !!reverse;

    if(numColors <=2) return [beginColor, endColor];
    var beginHSV = CH.rgbToHSV(beginColor.r, beginColor.g, beginColor.b),
        endHSV = CH.rgbToHSV(endColor.r, endColor.g, endColor.b),
        beginH = beginHSV.h,
        beginS = beginHSV.s,
        beginV = beginHSV.v,
        endH = endHSV.h,
        endS = endHSV.s,
        endV = endHSV.v,
        numSteps, moveH, stepH, stepS, stepV, results;

    if(beginH < 0) { // we're starting from a shade of gray, so move directly to the end color
        beginH = endH;
    }
    else if(endH < 0) {
        endH = beginH;
    }

//            trace("ColorHelper.getHSVColorProgression(): beginH=" + beginH + ", beginS=" + beginS 
//                + ", beginV=" + beginV + ", endH=" + endH + ", endS=" + endS + ", endV=" + endV);

    numSteps = numColors - 1;

    if(endH <= beginH) {
        moveH = reverse ? (endH - beginH) : (360 + endH - beginH);
    }
    else {
        moveH = reverse ? (-360 + endH - beginH) : (endH - beginH);
    }

    stepH = moveH / numSteps;
    stepS = (endS-beginS) / numSteps;
    stepV = (endV-beginV) / numSteps;

//            trace("ColorHelper.getHSVColorProgression(): moveH=" + moveH + ", stepH=" + stepH);

    results = [];
    for(var j=0; j<numColors; j++) {

        results.push(CH.hsvToColor(beginH, beginS, beginV));
//                trace("ColorHelper.getHSVColorProgression(): beginH before =" + beginH);
        beginH += stepH;
        if(beginH > 359) {beginH -= 360;}
        else if(beginH < 0) {beginH += 360;}
//                trace("ColorHelper.getHSVColorProgression(): beginH after =" + beginH);
        beginS += stepS;
        beginV += stepV;
    }

    return results;

};

IDB.ColorHelper.normalizeAlpha = function(n) {
    if(isNaN(n)) n = 1;
    if(n < 0) n = 0;
    if(n > 1) n = 1;
    return (Math.round(n * 100) / 100);
};

/*global IDB, jQuery */

/**
 * This is a base widget for widgets that float over a chart, like a legend or a note. It provides
 * common functionality like the background color and alpha, and repositioning the widget when the
 * chart frame is resized.
 */
(function($, IDB) {
    var bind = IDB.listener;

    $.widget("idb.floatingPanel", {
        options:{
           name:null,
           bgColor:IDB.rgb(0xFFFFFF),
           color:IDB.rgb(0),
           bgAlpha:1.0,
           xPct:0.5,
           yPct:0.5,
           vis:"show", // initial state. "show" means visible, anything else means hidden.
           visibilityChange:null,
           activate:null,
           zIndex:200
        },
        _create:function() {
             // note: see the idb-fp-* classes in idbhtml.css. The styles they apply to 
             // the main div and the background div are needed for the code to function correctly.
             var me = this, $e = me.element, o = me.options,
                colorcss = {"background-color":o.bgColor.css, "opacity":o.bgAlpha}, isVisible = (o.vis === "show"), 
                resizeListener = bind(me, "_locateCloseButton");
             me.$_bg = $("<div class=\"idb-fp-background\">").appendTo($e).css(colorcss);
             $e.addClass("idb-fp").draggable({"containment":"parent", "stop":bind(me, "_onDragStop")}).css({"color":o.color.css, "z-index":o.zIndex}).idbVisible(isVisible)
                 .on("mousedown touchtart", bind(me, "_onActivate"));

             me.$_close = $('<a data-enhanced="true" data-role="button" role="button" href="#">CLOSE</a>')
                 .addClass("ui-link ui-btn ui-btn-a ui-icon-delete ui-btn-icon-notext ui-shadow ui-corner-all")
                 .appendTo($e).on("click touchstart", bind(me, "_closeClicked")).idbHide();

             me._opaque = false;
             me._isVisible = isVisible;
             me._delay(resizeListener); // call close button after subclass's _create has finisthed.
             $e.on("resize", resizeListener);
             me._lastClick = 0;
             
        },
        _setOption:function(k, v) {
           var me = this, o = me.options, $bg = me.$_bg, $e = me.element;
           o[k] = v;
           if(k === "bgAlpha") {
               $bg.css({"opacity":v});
           }
           else if(k === "bgColor") {
               o[k] = IDB.rgb(v);
               $bg.css({"background-color":o[k].css});
           }
           else if(k === "color") {
               o[k] = IDB.rgb(v);
               $e.css({"color":o[k].css});
           }
           else if(k === "zIndex") {
               $e.css({"z-index":v});
           }
           else {
               me._super(k,v);
           }
        },
        _toggleOpaque:function(evt) {
            var me = this, now, diff, lastClick = me._lastClick;
            if(evt) {
                now = (new Date()).valueOf();
                diff = now - lastClick;
                me._lastClick = now;
                if(diff < 250) {
                    me._closeClicked();
                    return;
                }
            }
            me._locateCloseButton();
            me.setOpaque(!me._opaque);
        },
        _locateCloseButton:function() {
           var me = this, $e = me.element, $closeButton = me.$_close; 
           $closeButton.position({my:"left bottom", at:"right top", of:$e});
        },
        ////////////////////////////////////////////////////////////////////
        // Event Listeners
        _onActivate:function(evt) {
            var me = this, o = me.options;
            if(evt.target === me.$_close[0]) {
               return;
            }
            me._trigger("activate", null, {name:o.name, visible:true});
        },
        _onDragStop:function() {
            var me = this, $e = me.element, $p = $e.parent(), o = me.options, c = $e.position(),
                pw = $p.width(), ph = $p.height(), ew = $e.width(), eh = $e.height();
            o.xPct = (c.left / ((pw-ew) || 0.1));
            o.yPct = (c.top / ((ph-eh) || 0.1));
        },
        _closeClicked:function() {
            this.setIsVisible(false);
        },
        ////////////////////////////////////////////////////////////////////
        // Public Methods
        adjustLocation:function() {
           var me = this, $e = me.element, o = me.options, $p = $e.parent(),
             pw = $p.width(), ph = $p.height(), ew = $e.width(), eh = $e.height(),
             T = o.yPct * (ph-eh), 
             L = o.xPct * (pw-ew);
           $e.css({"top":T+"px", "left":L+"px"});
        },
        setOpaque:function(newOpaque) {
            var me = this, o = me.options, alpha = (newOpaque ? 1 : o.bgAlpha);
            me._opaque = !!newOpaque;
            me.$_bg.css({"opacity":alpha});
            me.$_close.idbVisible(newOpaque);
        },
        _isVisible:true,
        setIsVisible:function(b) {
            var me = this, $e = me.element, o = me.options, visible = !!b,
               closeVisible = visible && me._opaque;
            me._isVisible = visible;
            $e.idbVisible(visible);
            me.$_close.idbVisible(closeVisible);
            me._opaque = me._opaque && visible;
            me._trigger("visibilityChange", null, {name:o.name, visible:me._isVisible});
        },
        isVisible:function() {
            return this._isVisible;
        }
    });
})(jQuery, IDB);



////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.RB ("Request Builder - Namespace)
/*global IDB */
IDB.RB = {};
IDB.RB.clearStartupDashboard = function() {
    var obj = {};
    obj.userId = IDB.userdata.userId;
    return obj;
};

IDB.RB.setStartupDashboard = function(dashId) {
    var obj = {};
    obj.userId = IDB.userdata.userId;
    obj.dashId = dashId;
    return obj;
};


IDB.RB.loadChart2 = function(chartId, drilldownFilters, drilldownParamSet, dashboardParams, params, applyAlwaysPrompt) {
    if (chartId < 1) {
        throw new Error("Invalid chartId: " + chartId);
    }

    drilldownFilters = drilldownFilters || [];
    drilldownParamSet = drilldownParamSet || null; 
    var dashboardParamValues = IDB.Param.toParamValues(dashboardParams || []); 
    var paramValues = IDB.Param.toParamValues(params || []);
    applyAlwaysPrompt = IDB.ifNil(applyAlwaysPrompt, true);

    var chartDrilldownParamValues = [];
    var dashboardDrilldownParamValues = [];
    if(drilldownParamSet) {
       chartDrilldownParamValues = IDB.Param.toParamValues(drilldownParamSet.chartDrilldownParams || []);
       dashboardDrilldownParamValues = IDB.Param.toParamValues(drilldownParamSet.dashboardDrilldownParams || []);
    }

    var obj = {
               chartId:chartId,
               requestTime:new Date(),
               applyAlwaysPrompt:applyAlwaysPrompt,
               drilldown:{
                   filters:drilldownFilters,
                   chartParams:chartDrilldownParamValues,
                   dashParams:dashboardDrilldownParamValues
               },
               params:paramValues,
               dashParams:dashboardParamValues
    };

    IDB.RB.maybeAddModeInfo(obj, false);

    return obj;
};

IDB.RB.refreshChartData = function(chartId, valueSourceIndicator, drilldownFilters, drilldownParamSet, dashboardParams, params) {
    if (chartId < 1) {
        throw new Error("Invalid chartId: " + chartId);
    }

    drilldownFilters = drilldownFilters || [];
    drilldownParamSet = drilldownParamSet || null;
    var dashboardParamValues = IDB.Param.toParamValues(dashboardParams || []);
    var paramValues = IDB.Param.toParamValues(params || []);

    var chartDrilldownParamValues = [];
    var dashboardDrilldownParamValues = [];
    if(drilldownParamSet) {
       chartDrilldownParamValues = IDB.Param.toParamValues(drilldownParamSet.chartDrilldownParams || []);
       dashboardDrilldownParamValues = IDB.Param.toParamValues(drilldownParamSet.dashboardDrilldownParams || []);
    }

    var obj = {
               chartId:chartId,
               requestTime:new Date(),
               drilldown:{
                   filters:drilldownFilters,
                   chartParams:chartDrilldownParamValues,
                   dashParams:dashboardDrilldownParamValues
               },
               params:paramValues,
               dashParams:dashboardParamValues
    };

    if(valueSourceIndicator) {
        obj.overrideOnDrilldown = valueSourceIndicator;
    }

    IDB.RB.maybeAddModeInfo(obj, false);

    return obj;
}; 

IDB.RB.exportChartData = function(chartId, exportAllData, valueSourceIndicator, drilldownFilters, drilldownParamSet, dashboardParams, params) {
    var obj = IDB.RB.refreshChartData(chartId, valueSourceIndicator, drilldownFilters, drilldownParamSet, dashboardParams, params);
    obj.exportAllData = exportAllData;
   return obj;
};

IDB.RB.loadDashboard2 = function(dashId, applyAlwaysPrompt, drilldownFilters, drilldownParamSet, dashboardParams, isDrilldown) {
    if (dashId < 1) {
        throw new Error("Invalid dashId: " + dashId);
    }

    drilldownFilters = drilldownFilters || [];
    drilldownParamSet = drilldownParamSet || null;
    var dashboardParamValues = IDB.Param.toParamValues(dashboardParams || []);
    applyAlwaysPrompt = IDB.ifNil(applyAlwaysPrompt, true);
    isDrilldown = !!isDrilldown;

    var chartDrilldownParamValues = [];
    var dashboardDrilldownParamValues = [];
    if(drilldownParamSet) {
       chartDrilldownParamValues = IDB.Param.toParamValues(drilldownParamSet.chartDrilldownParams || []);
       dashboardDrilldownParamValues = IDB.Param.toParamValues(drilldownParamSet.dashboardDrilldownParams || []);
    }
        
    var obj = {
                dashId:dashId,
                requestTime:new Date(),
                applyAlwaysPrompt:applyAlwaysPrompt,
                isdd:isDrilldown,
                drilldown:{
                  filters:drilldownFilters,
                  chartParams:chartDrilldownParamValues,
                  dashParams:dashboardDrilldownParamValues
                },
                params:dashboardParamValues
     };

      IDB.RB.maybeAddModeInfo(obj, false);
    return obj;
};

IDB.RB.getToken = function(objId, objType, queryString, inputRequired) {
    var obj = {};
    obj.objId = objId;
    obj.objType = objType || "chart";
    obj.inputRequired = !!inputRequired;
    obj.query = queryString;
    return obj;
};

IDB.RB.maybeAddModeInfo = function(request, loadLoginInfo) {
    if(!IDB.userdata) {
        return;
    }

    var loginType = IDB.userdata.loginType || "normal";
    var context = IDB.userdata.context;
    var username = IDB.userdata.username;
    var loginString = IDB.userdata.loginString;

    if(loginType === "normal" || loginType === "sso") {
        return;
    }

    if(context) {
        request.context = context;
    }

    //
    // The mode information is primarily to handle session expiration. The server will attempt to relogin the     // user in particular modes. Even though the server code indicates we do attempt to relogin expired sso 
    // sessions, that isn't how 7.5 behaves. We want to preserve the existing behavior, so sso session 
    // expiration will not be handled.
    //
/*
    if(loginType === "sso") {
        if(loginString) {
            request[loginType] = loginString;
        }
    }

    else if(username) {
*/
    if(username) {
        request[loginType] = username;
    }

    
    if(!!loadLoginInfo) {
        request.loadLoginInfo = true;
    }
};


/*global IDB, $ */
IDB.ButtonBar = function($div, chart) {
    var me = this;
    me.$div = $div;
    me.chart = chart;
    me.btnMap = {};
    me._hideListener = IDB.listener(me, "_onBtnHide");
    me._showListener = IDB.listener(me, "_onBtnShow");

    me._build();
};

IDB.ButtonBar.prototype._build = function() {
    var me = this, $div = me.$div,  
       $button, code, codes = ["menu", "db", "legend", "paramLegend", "note"], 
       listener = IDB.listener(this, "onBtnClick");

    $div.css({"position":"absolute", "top":"0px", "left":"0px", "padding-right":"5px"});
    for(var j=0, jj=codes.length; j<jj; j++) {
        code = codes[j];
        // buttons initially have display:none from idb-cb.
        me.btnMap[code] = $button = $("<a>").addClass("idb-cb idb-cb-" + code).appendTo($div).on("click", {code:code}, listener).attr("title", code).css({"display":"none"});
    }
};

IDB.ButtonBar.prototype._onBtnHide = function() {
    var me = this;
    me.chart.onBtnHide(me.numVisBtns());
};

IDB.ButtonBar.prototype._onBtnShow = function() {
    var me = this;
    me.chart.onBtnShow(me.numVisBtns());
};

IDB.ButtonBar.prototype.measuredWidth = function() {
    var me = this, $div = me.$div, hasVisibleButtons = !!me.numVisBtns();
    return hasVisibleButtons ? $div.outerWidth() : 0;
};

IDB.ButtonBar.prototype.measuredHeight = function() {
    var me = this, $div = me.$div, hasVisibleButtons = !!me.numVisBtns();
    return hasVisibleButtons ? $div.outerHeight() : 0;
};

IDB.ButtonBar.prototype.numVisBtns = function() {
    return $("a.idb-cb", this.$div)
        .filter(function() {
                   return ($(this).css("display") === "inline-block");
                }).length;
};

IDB.ButtonBar.prototype.onBtnClick = function(evt) {
    this.chart.btnClicked(evt.data.code);
};

IDB.ButtonBar.prototype.btnVisible = function(code, visible) {
    
    var me = this, $btn = me.btnMap[code], funcName = visible ? "show" : "hide",
       newDisplay = visible ? "inline-block" : "none", listener = visible ? me._showListener : me._hideListener;

    // There seems to be a bug in jQuery UI. If hide() is called on a button that is already invisible (display:none)
    // the button ends up visible instead. So we have to make sure it doesn't happen.
    if($btn.css("display") === newDisplay) {
       IDB.log("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> No change, returning. " + newDisplay);
    }
    $btn[funcName](200, listener);
};

IDB.ButtonBar.prototype.maybeShow = function(code, show) {
    if(!show){
        return;
    }
    this.btnMap[code].css({display:"inline-block"});
};

IDB.ButtonBar.prototype.hide = function() {
    this.$div.addClass("idb-display-none");
};

IDB.ButtonBar.prototype.show = function() {
    this.$div.removeClass("idb-display-none");
};


/*global IDB, jQuery */

(function($) {
    $.widget("idb.callout", {
        options:{
           colorField:"axisColor",
           datapoint:null
        },
        _create:function() {
             this.element.addClass("idb-callout").text("CALLOUT").css({"display":"none",
             "position":"fixed", "z-index":"50"});
        },
        _setOption:function(key, value) {
            if(key === "datapoint") {
                this.setDatapoint(value);
                return;
            }
            this.options[key] = value;
        },

        destroy: function() {
            IDB.log("//////// Callout.destroy().");
            // Call the base destroy function.
            $.Widget.prototype.destroy.call( this );
        },

        clear:function() {
            this.element.empty();
        },
        setDatapoint:function(dp) {
            this.options.datapoint = dp;
            var axisId = dp.yAxis.axisId;
            this.clear();
            if(!dp) return;
            var dps = this._getDatapoints(dp, false);
            var $t = $("<table>").appendTo(this.element);
            var $tr = $("<tr>").appendTo($t);
            var $td = $("<td colspan=\"3\" class=\"idb-header\">").appendTo($tr).text(dp.xValue.formattedValue).css({"text-align":"center"});
            var colorField = this.options.colorField;
            var found = false;
            for(var j=0, jj=dps.length; j<jj; j++) {
                dp = dps[j];
                $tr = $("<tr>").appendTo($t);
                if(axisId === dp.yAxis.axisId) {
                    $tr.addClass("idb-callout-highlighted");
                    found = true;
                }
                $td = $("<td>").appendTo($tr);
                if(colorField != 'rangeColor' || (dp.yAxis.dataTypeIsNumber && !dp.yValue.isNothing)) {
                    $("<div>").html("&nbsp;").css({"background-color":dp[colorField].css, "width":"15px"}).appendTo($td);
                }
                $td = $("<td>").appendTo($tr).text(dp.yAxis.axisName);
                $td = $("<td>").appendTo($tr).text(dp.yValue.formattedValue);
            }

            if(!found) {
                IDB.log(this.options.datapoint, "NOT FOUND.....");
            }
        },
        move:function(e) {
            if(!e || IDB.isNil(e.pageX)) return;
            var me = this, $e = me.element, $p = $e.parent(), pw = $(window).width() + $(window).scrollLeft(), ph = $p.height(),
                w = $e.width(), h = $e.height(),
                top = e.pageY - $(window).scrollTop() - 4 - h, left = e.pageX - $(window).scrollLeft() + 3;
            if(top < 0) {
                top = e.pageY - $(window).scrollTop() + 4;
            }
            if(left + $(window).scrollLeft() > pw - w - 3) {
                left = e.pageX - $(window).scrollLeft() - w - 3;
            }
            $e.css({"left":left+"px", "top":top+"px"});
        },
        _visible:false,
        show:function() {
           this.element.css({"visibility":"visible", "display":"block"});
           this._visible = true;
        },
        hide:function() {
            this.element.css({"visibility":"hidden", "display":"none"});
            this._visible = false;
        },
        isVisible:function() {
            return this._visible;
        },
        _getDatapoints:function(dp, pSkipMax) { //(dp:IDatapoint, pSkipMax:Boolean=false):Array { //ZK: skipMax

            var maxRows = 11;

            var axisId = dp.yAxis.axisId;
            var foundAtIndex = -1;
            var dpRow = dp.datapointRow;
            var points = dpRow ? dpRow.datapoints : null;

            
//            if(points == null || points.length == 0) {
//                if(dp.yAxis.dataTypeIsNumber) return [dp];
//                else return [];
//            }

            if(points === null || points.length === 0) return [dp];

//            if(maxRows == 1 && dp.yAxis.dataTypeIsNumber) return [dp];
            if(maxRows == 1) return [dp];

            var results = [];
            var len = points.length;
            var j;
            for(j=0; j<len; j++) {
                var point = points[j];
                var yAxis = point.yAxis;
                if(yAxis.isHidden || yAxis.isPivot) continue;
                var isPoint = yAxis.axisId == axisId;
//                if(!point.yAxis.dataTypeIsNumber) {
//                    if(isPoint) {
//                        foundAtIndex = Math.max(0, results.length - 1);
//                    }
//                    continue;
//                }
                results.push(point);
                if(isPoint) {
                    foundAtIndex = results.length - 1;
                }
            }

            if(pSkipMax || maxRows < 1 || results.length <= maxRows) return results;
            var half = Math.floor(maxRows / 2);
            var firstIndex = Math.min(results.length-maxRows, Math.max(0, foundAtIndex-half));
            return results.slice(firstIndex, firstIndex+maxRows);
        }




        });
    
})(jQuery);

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ChartBase
/*global $, IDB */
IDB.ChartBase = function($div, dashframe, datapacket, showDrillbackButton) {
    IDB.log(datapacket, "CHART DATA");
    var me = this, dashboard = dashframe.dashboard;

    me.$div = $div;
    me.dashframe = dashframe;
    me.dashboard = dashboard;
    dashboard.addChart(me);
    me.$frameDiv = dashframe.$div;
    me.fadeOtherValues = dashboard.fadeOtherValues;
    me.showDbButton = !!showDrillbackButton;
    me.pivotBar = null;

    me.fps = {}; // floating panels
    me._panelChangeListener = IDB.listener(me, "_panelChanged");
    me._panelClickListener = IDB.listener(me, "_panelClicked");
    me._panelListeners = {visibilityChange:me._panelChangeListener, activate:me._panelClickListener};

    // create all of the instance properties that come from the datapacket
    me._setProps(datapacket);
    me._build(datapacket);
};

IDB.ChartBase.MODULE_MODE = true; //ZK

IDB.ChartBase.prototype.onMouseMove = function(e) { 
//   IDB.log(e, "onMouseMove");
   var me = this, $callout = me.$callout;
   if(!$callout || !$callout.callout("isVisible")) {return true;}
   $callout.callout("move", e);
   return true;
};

IDB.ChartBase.prototype.onTitleClick = function() {
//    var me = this, I = IDB;
};

IDB.ChartBase.prototype.destroy = function() {
    var me = this, db = me.$dashboard, g = me.graph, $co = me.$callout, $n = me.$note, 
        $lg = me.$legend, $div = me.$div, $plg = me.$paramLegend, 
        ip = me.chartParameterInputPrompt, pi = me.chartParamInputPanel, 
        props = ["$dashboard", "graph", "$callout", "$note", "$legend", "$paramLegend", "chartParameterInputPrompt"];

    if(db) {
        db.removeChart(me);
    }
    if(g) {
       g.destroy();
    }
    if($co) {
       $co.remove();
    }
    if($n) {
        $n.remove();
    }
    if($lg) {
       $lg.remove();
    }
    if($plg) {
       $plg.remove();
    }
    if(pi) {
        pi.remove();
    }
    if(ip) {
      ip.close();
    }

    for(var j=0, jj=props.length; j<jj; j++) {
        me[props[j]] = null;
    }
    $div.empty();
};

IDB.ChartBase.prototype.handCursor = function(b) {
    var me = this;
    if(!me.graphClickable) return;
    me.$gdiv.css({ "cursor":(b ? "pointer" : "default")});
};
////////////////////////////////////////////////////////////////////////////
// BEGIN IGraphManager methods

// The methods in this section are called by the graph.

IDB.ChartBase.prototype.datapointMouseOver = function(caller, dp) {
    var me = this, mt = me.matchType, ginfo = caller.getInfo(),
       matchValue, dashboard = me.dashboard, fadeOtherValues = me.fadeOtherValues,
       $co = me.$callout;
    me.handCursor(true);
    if(ginfo.xMatchOnly && mt !== IDB.MT.NONE) {
        mt = IDB.MT.X_VALUE;
    }
    matchValue = dp.getMatchValue(mt);
    if (mt !== IDB.MT.NONE)
    {
        if(ginfo.displaysMultipleXValues) {
            caller.highlightMatchingDatapoints(matchValue, mt, fadeOtherValues);
        }
        else {
            caller.highlightDatapoint(dp, mt, fadeOtherValues);
        }
        dashboard.datapointMouseOver(this,  mt,  dp);
    }
    if($co) {
        if(ginfo.calloutStyle === "custom") {
            $co.callout("clear");
            caller.updateCallout(dp, $co);
        }
        else {
            $co.callout("setDatapoint", dp);
            
        }
        $co.callout("show");
    }
};

/* jshint unused:false */
IDB.ChartBase.prototype.datapointMouseOut = function(caller, dp) {
/* jshint unused:true */
//   IDB.log(dp, "MOUSEOUT");
   var me = this, graph = me.graph, dashboard = me.dashboard, matchType = me.matchType,
       $callout = me.$callout;
   me.handCursor(false);
   graph.normalizeAllDatapoints();
   dashboard.datapointMouseOut(me,  matchType,  dp);
   if($callout) {
       $callout.callout("hide");
   }
};

IDB.ChartBase.prototype.datapointClicked = function(graph, dp) {
    var me = this, linkType = me.linkType, dataSet = me.dataSet,
        dashboard = me.dashboard, axis, dv;

    if(linkType === "p") {
        axis = dataSet.fetchPivotDrilldownAxis(dp, me.sendC);
        dv = dp.dataRow[axis.axisId];
        dashboard.pivotSelectionChanged(me, axis.axisName, dv.stringValue, dataSet.getPivotSelectionMap());
        if(IDB.PHONEMODE)
        {
            IDB.phoneModeNavToPivot(axis.axisName);
        }
        
    }
    else {
        me.dashframe.datapointClicked(me, dp);
    }
};

IDB.ChartBase.prototype.yAxisSelected = function(caller, yAxis) {
    IDB.log(yAxis, "ChartBase.yAxisSelected");
    var $legend = this.$legend;
    if($legend) {
        $legend.legend("setSelectedYAxis", yAxis);
    }
};

IDB.ChartBase.prototype.displayUserMessage = function(caller, message, messageType, caption) { //(caller:IGraph, message:String, messageType:String, caption:String=null):void;
    IDB.log(caller, "ChartBase.displayUserMessage(): message=" + message + ", messageType=" + messageType + ", caption=" + caption);
    var me = this, I = IDB;
    I.MM.show(message, caption, {type:(messageType || "info"), position:I.center(me.$div), modal:false, appendTo:me.dashboard.$div});
};

IDB.ChartBase.prototype.legendStyleChanged = function(caller, newLegendStyle) { //(caller:IGraph, newLegendStyle:String):void;
//   IDB.log("ChartBase.legendStyleChanged(): newLegendStyle=" + newLegendStyle);
   this.maybeShowOrUpdateLegend();
   this.resize();
};

IDB.ChartBase.prototype.expandMacros = function(str, escapeType, dp) { //(str:String, escapeType:String, dp:IDatapoint=null):String;
    var me = this, dataSet = me.dataSet, I = IDB, userdata = I.userdata, 
        username = userdata.username, contentBaseUrl = userdata.baseUrl, 
        pivots, chartParams, drilldownFilters, len, pat, re, j, mapRoot;

    if(escapeType === "url") {
        mapRoot = I.config.contextRoot + "stdmaps/";
        re = /^\s*map:/i;
        if(null !== str.match(re)) {
            str = $.trim(str.replace(re, mapRoot)) + ".png";
        }
        re = /^\s*mapconfig:/i;
        if(null !== str.match(re)) {
            str = $.trim(str.replace(re, mapRoot)) + ".xml";
        }
    }

    str = I.ChartUtils.expandMacros(str, escapeType, username, contentBaseUrl, this, dp);

    //
    // Expand the deprecated ${pivot<N>label} and ${pivot<N>value} macros.
    //
    pivots = dataSet.getPivotSelectionFilters();
    for(j=0, len=pivots.length; j < len; j++) {
        pat = "\\$\\{\\s*pivot" + (j+1) + "label\\s*\\}";
        re = new RegExp(pat, "ig");
        str = str.replace(re, I.ChartUtils.maybeEscape(pivots[j].axisName, escapeType));
        
        pat = "\\$\\{\\s*pivot" + (j+1) + "value\\s*\\}";
        re = new RegExp(pat, "ig");
        str = str.replace(re, I.ChartUtils.maybeEscape(pivots[j].value, escapeType));
    }

    //
    // Expand the param macros. They are of the form ${param:<paramName>}.
    // 
    // Param.setMissingValuesToDefaults(chartParams);
    chartParams = me.params || [];
    for(j=0, len=chartParams.length; j < len; j++) {
        var paramValue = chartParams[j].value;
        if(Array.isArray(chartParams[j].value)) {
            paramValue = paramValue.join(',');
        }
        pat = "\\$\\{\\s*param\\s*:\\s*" + I.StringUtils.escapeRegEx(chartParams[j].paramName) + "\\s*\\}";
        re = new RegExp(pat, "g"); // case-SENSITIVE
        str = str.replace(re, I.ChartUtils.maybeEscape(paramValue, escapeType));

        pat = "\\$\\{\\s*ueparam\\s*:\\s*" + I.StringUtils.escapeRegEx(chartParams[j].paramName) + "\\s*\\}";
        re = new RegExp(pat, "g"); // case-SENSITIVE
        str = str.replace(re, paramValue);
    }

    // Handle ${value:} macros for drilldown filters that led up to this chart. 
    // If this has been called for a drill-to-webpage, then the filter macros for 
    // the clicked-on datapoint have already been handled. In most cases, 
    // the filters from previous charts in the stack should have names matching 
    // axis names in this chart, so the macros that would normally be expanded here 
    // have likely already been expanded.
    if(dp) {
        drilldownFilters = me.dashframe.getAllFilters(dp, dataSet, me.sendC);
        for(j = 0, len=drilldownFilters.length; j < len; j++) {
            pat = "\\$\\{\\s*value\\s*:\\s*" + I.StringUtils.escapeRegEx(drilldownFilters[j].axisName) + "\\s*\\}"; 
            re = new RegExp(pat, "g"); // case-SENSITIVE
            str = str.replace(re, I.ChartUtils.maybeEscape(drilldownFilters[j].value, escapeType));

            pat = "\\$\\{\\s*uevalue\\s*:\\s*" + I.StringUtils.escapeRegEx(drilldownFilters[j].axisName) + "\\s*\\}"; 
            re = new RegExp(pat, "g"); // case-SENSITIVE
            str = str.replace(re, drilldownFilters[j].value);
                                                                                                                  }
    }
    return str;
};

// END IGraphManager methods
////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////
// BEGIN IChartSynchronizerClient methods

IDB.ChartBase.prototype.highlightDatapoint = function(incomingMatchType, dp, fadeOthers) {
    var me = this, graph = me.graph, $callout = me.callout, matchType = me.matchType,
        val, points;
    if(!graph) {return;}
    var ginfo = graph.getInfo();
    if (ginfo.xMatchOnly && matchType !== IDB.MT.NONE)
    {
        matchType = IDB.MT.X_VALUE;
    }
    val = dp.getMatchValue(matchType);
    points = graph.highlightMatchingDatapoints(val, matchType, fadeOthers);
    // TODO: Update Ribbon Legend

    if($callout) {
        $callout.callout("hide");
    }
    return points;
};

IDB.ChartBase.prototype.normalizeAllDatapoints = function() {
    var me = this, graph = me.graph, $callout = me.callout;
    if(graph === null) {return;}
    graph.normalizeAllDatapoints();
    if($callout) {
        $callout.callout("hide");
    }
};

IDB.ChartBase.prototype.changePivotSelections = function(axisName, valueAsString, allSelections) {
    var me = this, pivotBar = me.pivotBar, changed = me.dataSet.maybeChangePivotSelections(axisName, valueAsString, allSelections);
    if(changed) {
        if(pivotBar) {
            pivotBar.updateSelections();
        }
        me.createOrReplaceGraph(function() {});
    }
};


// END IChartSynchronizerClient methods
////////////////////////////////////////////////////////////////////////////





/**
 * This takes a full chart datapacket (JSON object) from the server and copies various properties from it to the ChartBase object.
 * As a general rule, any instance properties that come from the datapacket should be created inside this method. 
 * This method is called once, from the constructor,
 */
IDB.ChartBase.prototype._setProps = function(datapacket) {
    var me = this, chartProps = datapacket.chart, axes = datapacket.axes, linkType, linkId,
       linkTypes1 = {p:1, q:1}, linkTypes2 = {c:1, s:1, d:1, r:1};



    me.chartId = chartProps.chartId;
    me.categoryId = chartProps.categoryId;

    me._coVis = chartProps.coVis;  // calloutVisible
    me._coDP = chartProps.coDP;    // calloutDecimalPlaces

    me._noteSettings = chartProps.noteSettings;
    me._legendSettings = chartProps.legendSettings;
    me._paramLegendSettings = chartProps.paramLegendSettings;

    me.$callout = null;
    me.graph = null;
    me.$legend = null;
    me.$note = null;

    me.graphTypeCode = chartProps.graphTypeCode;
    me.matchType = chartProps.matchType;
    linkType = me.linkType = chartProps.linkType || null;
    linkId = me.linkId = chartProps.linkId;
    me.linkUrl = chartProps.linkUrl;
    me.linkUrlAppend = chartProps.linkUrlAppend;
    me.linkUrlSettings = chartProps.linkUrlSettings;
    me.linkTarget = chartProps.linkTarget;
    me.sendC = chartProps.sendC;
    me.linkRF = chartProps.linkRF;
    me.linkReportUrl = chartProps.linkReportUrl; 
    me.linkReportInline = chartProps.linkReportInline; 

    me.graphClickable = !!(linkType && (linkTypes1[linkType] || (linkTypes2[linkType] && linkId) ||
            (linkType === 'l' && me.linkUrl)));

    me.paramSettings = chartProps.paramSettings;
    me.paramLocation = chartProps.paramSettings.paramLocation || "right";
    me.textColor = chartProps.textColor;
    me.pivotSettings = chartProps.pivots; // was pivotSettings;
    me.pivotBottom = (me.pivotSettings.loc === "bottom");

    me.chartTitle = chartProps.chartTitle;
    me.title = chartProps.expandedChartTitle?chartProps.expandedChartTitle:me.chartTitle;

    me.noDataMessage = chartProps.noDataMessage;
    me._maybeSetExpandedNoDataMessage(datapacket.expandedNoDataMessage);

    me.refreshMinutes = chartProps.refreshMinutes;
    me.imageSettings = chartProps.imageSettings;

    me.chartParameterInputRequired = datapacket.chartParameterInputRequired;
    me.params = datapacket.params;

    if(!me.params || !me.params.length) {
        me._paramLegendSettings.vis = "hide"; // This has potential to cause confusion, but makes the rest of the code simpler.
    }

    me.chartSettings = new IDB.ChartSettings(datapacket);

    me.axisList = [];
    me.pivotAxes = [];
    for(var j=0, jj=axes.length; j<jj; j++) {
        var axis = new IDB.Axis(axes[j]);
        me.axisList.push(axis);
        if(axis.isPivot) {
            me.pivotAxes.push(axis);
        }
    }

    me.pivotAxes.sort(function(a,b) {return a.pivotRank - b.pivotRank;});
    me.dataSet = new IDB.DataSet(datapacket, me.axisList, me.pivotAxes);
    me.menuButtonHtml5 = datapacket.chart.menuButtonHtml5;
    me.reportEnabled = datapacket.chart.reportEnabled;
    me.reportFormat = datapacket.chart.reportFormat;
    me.exportAllData = datapacket.chart.exportAllData;
    me.maxDataRows = datapacket.chart.maxDataRows;
};

IDB.ChartBase.nextGraphId = 1;

/**
 * This assembles all of the visual child objects of the chart. Until this is called, the chart's div is completely empty.
 * This is called once, from the constructor, and it must be called after _setProps() is called.
 */
IDB.ChartBase.prototype._build = function(datapacket) {
    var me = this, textColor = me.textColor,
        HSLICE = '<div class="idb-hslice">', $contentPane,
        hasPivots = (me.pivotAxes.length > 0), ps = me.pivotSettings, chartProps = datapacket.chart,
        $div = me.$div, bga = chartProps.bgAlpha, bgc = chartProps.backgroundColor, title = me.title, $pivotDiv,
        pivotBottom = me.pivotBottom, imageSettings = me.imageSettings, url, bgSize, dataSet = me.dataSet;

    $div.addClass("idb-chart");

    if(imageSettings) {
        url = "url('" + IDB.resolveUrl(imageSettings.imageUrl, imageSettings.useProxy) + "')";
        bgSize = imageSettings.fitImage ?"100% 100%":"auto";
        me.$bgImage = $("<div>").addClass("idb-chart").appendTo($div).css({"background-image":url, "background-repeat":"no-repeat","background-position":"center", "background-size":bgSize});
    }
    else {
       me.$bgImage = null;
    }


    // Configure the content pane first. It holds all of the fixed-position, non-floating objects.
    $contentPane = me.$contentPane = $('<div class="idb-chart">').appendTo($div)
       .on("mousemove", IDB.listener(me, "onMouseMove"));


    if(bga === 1.0) {
        $contentPane.css("background-color", bgc.css);
    }
    else if(bga) {
        $contentPane.css("background-color", IDB.toRGBA(bgc, bga));
    }
    // if bgAlpha is 0, do nothing to background.

    if(title) {
        me.$titleDiv = $(HSLICE).appendTo($contentPane)
            .css({"padding":"12px 10px"});
        me.$title = $("<p>").text(title)
            .on("click", IDB.listener(this, "onTitleClick"))
            .css({"font-weight":"bold", 
                  "text-align":chartProps.titleAlign, 
                  "margin":"0px", 
                  "color":textColor.css, 
                  "text-shadow":"none",
                  "overflow":"hidden", 
                  "font-size":datapacket.chart.chartTitleSize + "px",
                  "text-overflow":chartProps.titleWrap ? "clip" : "ellipsis",
                  "white-space":chartProps.titleWrap ? "normal" : "nowrap"
                  })
            .appendTo(me.$titleDiv);
    }

    me.$gslice = $(HSLICE).addClass("idb-hslice-g").appendTo($contentPane).css({padding:"0px"});//.css({"background-color":"#000000"});
    me.$gframe = $("<div>").appendTo(me.$gslice).css({"position":"absolute",
        "top":"5px", "bottom":"5px", "left":"5px", "right":"5px"});
    me.$gframe.data({frameId:me.dashframe.frameId});
    me.gframe = new IDB.GFrame(me.$gframe, chartProps.gm);

    me.$gdiv = $("<div>").css({"overflow":"hidden"});
    me.gframe.setGDiv(me.$gdiv);
    me.graphId = "graph-" + (++IDB.ChartBase.nextGraphId);
    me.$gdiv.attr("id", me.graphId);


    // Add the bottom slice and put the buttonbar in it.
    var $bslice = me.$bslice = $(HSLICE).appendTo($contentPane).css({padding:"0px", "z-index":"20"});
    me.buttonBar = new IDB.ButtonBar($('<div>').appendTo($bslice), me);
    me.buttonBar.maybeShow("db", me.showDbButton);
    me.buttonBar.maybeShow("menu", me.menuButtonHtml5);

    if (me.menuButtonHtml5)
    {
        me.popupMenuId = "chart-menu-popup-"  + me.chartId;
        $div.find(".idb-cb-menu").attr("data-rel", "popup").attr("href", "#" + me.popupMenuId);

        var $popup = $("<div data-role=\"popup\" data-history=\"false\">").attr("id", me.popupMenuId);
        $popup.on("popupbeforeposition", me.positionMenu);
        var $list = $("<ul class=\"chart-menu-popup-list\" data-role=\"listview\">").appendTo($popup);
        var $item = $("<li data-icon=\"false\"><a class=\"ui-btn\" href=\"#\" data-ajax=\"false\"></a></li>");
        $item.find("a").text(IDB.format("chart.html5.view_as_report"));
        $item.on("click", null, IDB.listener(me, "openChartAsReport"));
        $item.appendTo($list);

        if(IDB.userdata.exportAllowed) {
            $item = $("<li data-icon=\"false\"><a class=\"ui-btn\" href=\"#\" data-ajax=\"false\"></a></li>");
            $item.find("a").text(IDB.format("chart.html5.export_chart_data"));
            $item.on("click", null, IDB.listener(me, "exportChartData"));
            $item.appendTo($list);
        }

        $popup.appendTo($div);
        $div.trigger("create");
        $popup.popup("option", "tolerance", "4");
    }

    if(dataSet.serverStatus == 1600) { // insufficient axes
        me.graph = new IDB.ErrorGraph(me, me.$gdiv, dataSet, dataSet.serverMessage, me.textColor);
        me.resize();
        return;
     }     

    if(hasPivots && ps.visible) {
        $pivotDiv = me.$pivotDiv = $('<div>').addClass(pivotBottom ? "idb-pivotbar" : "idb-pivotbar idb-hslice").css("text-align", ps.aln)
            .appendTo(pivotBottom ? $bslice : $contentPane);

        if(ps.opaque) {
            $pivotDiv.css("background-color", bgc.css);
        }
    }

    if(me.chartParameterInputRequired) {
        if(me.$title) {
            me.$title.idbHide();
        }

        if(me.$pivotDiv) {
            me.$pivotDiv.idbHide();
        }

        me.showBSlice(false);

        var callbacks = {chartParameterValuesChanged:IDB.listener(me, "chartParameterValuesChanged")};
        me.chartParameterInputPrompt = new IDB.ChartParameterInputPrompt(me.$div, $.extend({chartTitle:title}, datapacket), callbacks);
        return;
    }
    else {
        if($pivotDiv) {
            me.pivotBar = new IDB.PivotBar($pivotDiv, me);
        }
        me.createOrReplaceGraph(function() {
			me.maybeShowOrUpdateLegend();
			me.maybeShowOrUpdateParamLegend();
			me.maybeShowNote();
			me.resize();
			me.maybeAddOrUpdateParamInputPanel();
		});
    }
};

IDB.ChartBase.prototype._maybeSetExpandedNoDataMessage = function(msg) {
    var me = this;
    if(!IDB.isNil(msg)) {
        me.expandedNoDataMessage = msg;
    }
    else {
        if(!IDB.isNil(me.noDataMesssage)) {
            me.expandedNoDataMessage = me.noDataMessage;
        }
        else {
            me.expandedNoDataMessage = null;
        }
    }
};

IDB.ChartBase.prototype.positionMenu = function(event, ui) {
    var menu = $(event.target);
    ui.x = ui.x + (menu.width() / 2) - 16;
    ui.y = ui.y - (menu.height() / 2) - 19;
};
IDB.ChartBase.prototype.openChartAsReport = function() {
    var chart = this;
    $("#" + chart.popupMenuId).popup('close');
    if(!IDB.userdata.reportsAvailable)
    {
       IDB.MM.show(IDB.format("reports.html5.no_reports_available"));
       return;
    }
    if (!chart.reportEnabled)
    {
        IDB.MM.show(IDB.format("chart.html5.reports_disabled"));
        return;
    }
    var format = chart.reportFormat;

    var url = chart.dashframe.buildLoadChartReportURL(format);

    var target = IDB.getReportWindowName();
    var settings = IDB.getReportWindowSettings();
    var win = window.open(url, target, settings);
    if(win && target !== "_blank")
    {
        win.focus();
    }
};
IDB.ChartBase.prototype.exportChartData = function() {
    var chart = this;
    $("#" + chart.popupMenuId).popup('close');
    chart.dashframe.exportChartData();
};
IDB.ChartBase.prototype.showBSlice = function(show) {
   var me = this, func = show ? "removeClass" : "addClass";
   me.$bslice[func]("idb-display-none").idbVisible(show);
};
IDB.ChartBase.prototype.onBtnHide = function() {
     this.resize();
};

IDB.ChartBase.prototype.onBtnShow = function() {
     this.resize();
};


IDB.ChartBase.prototype.btnClicked = function(code) {
    var me = this, dashframe = me.dashframe;
    if(code === "db") {
        dashframe.dbClicked();
        return;
    }

    if(code === "menu") {
        return;
    }

    var $fp = me.fps[code];
    if($fp) {
        $fp[code]("setIsVisible", true);
    }
    me._panelToTop(code);

};

IDB.ChartBase.prototype._panelChanged = function(evt, evtData) {
    this.buttonBar.btnVisible(evtData.name, !evtData.visible);
};

IDB.ChartBase.prototype._panelClicked = function(evt, evtData) {
    this._panelToTop(evtData.name);
};

IDB.ChartBase.prototype._panelToTop = function(code) {
    var me = this, fps = me.fps, name, panel, 
        topPanel = fps[code], 
        z = topPanel[code]("option", "zIndex"), max = z;

    for(name in fps) {
        if(fps.hasOwnProperty(name)) {
            if(name === code){continue;}
            panel = fps[name];
            max = Math.max(max, panel[name]("option", "zIndex"));
            panel[name]("setOpaque", false);
        }
    }
    if(z === max) {return;} // already on top
    topPanel[code]("option", "zIndex", max+1);
};


IDB.ChartBase.prototype.maybeShowNote = function() {
    var me = this, noteSettings = $.extend({"zIndex":203}, me._panelListeners, me._noteSettings);
    if(!me.$note) {
        if(!noteSettings) {return;}
        if(noteSettings.vis === "hide") {return;}
        me.fps.note = me.$note = $("<div>").appendTo(me.$div).note(noteSettings);
        me.buttonBar.maybeShow("note", noteSettings.vis === "min");
    }
};

IDB.ChartBase.prototype.maybeShowOrUpdateLegend = function() {
  var me = this, g = me.graph, gi = g.getInfo(), ls = me._legendSettings, legendStyle = gi.legendStyle,
      dataSet = me.dataSet;
  if(!me.$legend) {
      if(legendStyle === IDB.LS.NONE) {return;}
      if(ls.vis === "hide") {return;}
      ls = $.extend(ls, me._panelListeners, {"zIndex":201, axisList:g.getLegendAxes(), legendStyle:legendStyle, dataSet:dataSet});
      if(legendStyle === IDB.LS.CUSTOM) {
          ls.customItems = g.createLegendItems();
      }
      me.fps.legend = me.$legend = $("<div>").appendTo(this.$div).legend(ls);
      me.buttonBar.maybeShow("legend", ls.vis === "min");
  }
  else {
      me.$legend.legend("refresh", dataSet);
  }
};

IDB.ChartBase.prototype.maybeShowOrUpdateParamLegend = function() {
    var me = this, ps = me._paramLegendSettings, params = me.params, dataSet = me.dataSet;

    if(dataSet.serverStatus === 1410 || // parameter definition error
       dataSet.serverStatus === 1300) { // chart data error
        if(me.$paramLegend) {
            me.$paramLegend.remove();
            me.$paramLegend = null;
        }
        return;
    }

    if(!me.$paramLegend) {
        if(!params || params.length === 0) {return;}
        if(!ps) {return;}
        if(ps.vis === "hide") {return;}
        ps = $.extend({}, me._panelListeners, {"zIndex":202, paramList:this.params}, ps);
        me.fps.paramLegend = me.$paramLegend = $("<div>").appendTo(this.$div).paramLegend(ps);
        me.buttonBar.maybeShow("paramLegend", ps.vis === "min");
    }
    else {
        me.$paramLegend.paramLegend("refresh");
    }
};


IDB.ChartBase.PARAM_PANES_MAP = {
  right:"east",
  left:"west",
  top:"north",
  bottom:"south"
};

/* jshint laxcomma:true */
IDB.ChartBase.PARAM_LAYOUT_SETTINGS = {
         defaults: {                 
             togglerClass:"param-toggler"
             ,buttonClass:"param-button"
             ,togglerTip_open:"Hide parameters"
             ,spacing_closed:21 // wider space when closed
             ,togglerLength_closed:21 // make toggler 'square' - 21x21
             ,togglerLength_open:0 // NONE - using custom togglers INSIDE panes
             ,togglerTip_closed:"Show parameters"
             ,slidable:false
             ,resizable:false
             ,spacing_open:0
             ,fxName: "slide"
             ,fxSpeed_open:45
             ,fxSpeed_close:200
             ,fxSettings_open:{easing: "easeInQuint"}
             ,fxSettings_close:{easing: "easeOutQuint"}        
         },
         east: {
             size:0.15
             ,togglerAlign_closed:"top"
         },
         west: {
             size:0.15
             ,togglerAlign_closed:"top"
         },
         north: {
             size:0.10
             ,togglerAlign_closed:"right"
         },
         south: {
             size:0.10
             ,togglerAlign_closed:"right"
         }
 };
 /* jshint laxcomma:false */
IDB.ChartBase.prototype.maybeAddOrUpdateParamInputPanel = function() {
    var  me = this, paramSettings, paramVisibility = (me.paramSettings.paramVisibility || "showParams"),
         params = me.params || [], chartParamInputPanel = me.chartParamInputPanel, 
         dataSet = me.dataSet, bgc = me.chartSettings.data.chart.backgroundColor, 
         bga = me.chartSettings.data.chart.bgAlpha;

    if(this.chartParameterInputRequired || params.length === 0 || (paramVisibility == "hideParams") ||
          (dataSet.serverStatus === 1410) || (dataSet.serverStatus === 1300)) { // parameter definition error or chart data error
        if(chartParamInputPanel) {
            chartParamInputPanel.remove();
        }
        return;
    }

    if(!chartParamInputPanel) {
        var callbacks = {chartParameterValuesChanged:IDB.listener(this, "chartParameterValuesChanged"),
                         resize:IDB.listener(this, "resize")};
        paramSettings = $.extend({"textColor":me.textColor, "backgroundColor":bgc, "backgroundAlpha":bga}, me.paramSettings);
        chartParamInputPanel = me.chartParamInputPanel = new IDB.ChartParameterInputPanel(me.$gframe, paramSettings, params, callbacks);
        me.resize();
    }
    else {
       chartParamInputPanel.update();
    }
};

IDB.ChartBase.prototype.onPaneResize = function(paneName, $element, state, options, layout) {
    IDB.log("\n========================================================\nonPaneResize: " + this +", paneName=" + paneName + ", state=" + IDB.Util.objToString(state) + ", \noptions=" + IDB.Util.objToString(options)  + ", \n$element=" + $element.html());
    this.resize();
};

IDB.ChartBase.prototype.refreshChartData = function(result) {
    var me = this, dataSet = me.dataSet;
    me.title = result.expandedChartTitle?result.expandedChartTitle:me.chartTitle;
    me._maybeSetExpandedNoDataMessage(result.expandedNoDataMessage);

    if(me.params && me.params.length > 0) {
        IDB.Param.updateParamValues(me.params, result.params);
    }
    me.chartParameterInputRequired = result.chartParameterInputRequired;
    if(me.chartParameterInputRequired) {
        if(me.chartParameterInputPrompt) {
            me.chartParameterInputPrompt.update();
        }
        else { 
            me.showChartParameterInputPrompt({params:me.params}); 
        }
    }
    else {
        me.maybeCloseChartParameterInputPrompt();
        dataSet.createOrReplaceDataRows(result);
        if(me.$title) {
            me.$title.css({"visibility":"visible"}).text(me.title);
        }

        if(me.$pivotDiv) {
            if(!me.pivotBar) {
                me.pivotBar = new IDB.PivotBar(me.$pivotDiv, this);
            }
            else {
                me.pivotBar.updateSelections();    
            }
            me.pivotBar.setVisible(dataSet.numDataRows > 0);
        }
        me.createOrReplaceGraph(function() {
			me.maybeShowOrUpdateParamLegend();
			me.maybeShowNote();
			me.maybeShowOrUpdateLegend();
			me.resize();
			me.maybeAddOrUpdateParamInputPanel();
		});
    }
};

IDB.ChartBase.prototype.chartParameterValuesChanged = function() {
   var me = this;
    if(me.chartParameterInputPrompt) {
        me.chartParameterInputPrompt.close();
        me.chartParameterInputPrompt = null;
    }
    me.dashframe.refreshChartData();
};

IDB.ChartBase.prototype.showChartParameterInputPrompt = function(input) {
      var me = this, callbacks = {chartParameterValuesChanged:IDB.listener(me,"chartParameterValuesChanged")}; 
      me.chartParameterInputPrompt = new IDB.ChartParameterInputPrompt(me.$div, input, callbacks);
};

IDB.ChartBase.prototype.maybeCloseChartParameterInputPrompt = function() {
    var me = this;
    if(me.chartParameterInputPrompt) {
        me.chartParameterInputPrompt.close();
        me.chartParameterInputPrompt = null;
    }
};

IDB.ChartSettings = function(chartData) {
    this.data = chartData;

};

IDB.ChartBase.prototype.resize = function() {

   var me = this, $div = me.$div, frameWidth = $div.width(), frameHeight = $div.height(),
       titleHeight = 0, $titleDiv = me.$titleDiv, 
       $pivotDiv = me.$pivotDiv, 
       pivotHeight, gheight, gwidth, chartParamInputPanel = me.chartParamInputPanel, $gframe = me.$gframe, gFrame = me.gframe,
       graph = me.graph, chartParameterInputPrompt = me.chartParameterInputPrompt, $note = me.$note,
       $legend = me.$legend, titleY = 0, pivotY, graphY = 0, ps = me.pivotSettings, pivotLoc = ps.loc,
       pivotBottom = me.pivotBottom,
       $bslice = me.$bslice, 
       buttonBar = me.buttonBar,
       bbHeight, bsHeight,
       hasBottomPivot = (me.pivotBar && me.pivotBottom);

   me.showBSlice(buttonBar.numVisBtns() || hasBottomPivot);
   bbHeight = buttonBar.measuredHeight();
   pivotHeight = $pivotDiv ? $pivotDiv.outerHeight() : 0; 
   bsHeight = hasBottomPivot ? Math.max(bbHeight,  pivotHeight) : bbHeight;

   $bslice.css({"height":bsHeight+"px"});

   if($titleDiv) {
        titleHeight = $titleDiv.innerHeight();
        graphY = titleHeight;
   }

   if($pivotDiv) {
       graphY = titleHeight + pivotHeight;
       if(pivotLoc === "top") {
          pivotY = 0;
          titleY = pivotHeight;
       }
       else if(pivotLoc === "title") {
          pivotY = titleHeight;
       }
       else {
          pivotY = bsHeight - pivotHeight;
          $pivotDiv.css({"left":buttonBar.measuredWidth()+"px", "right":"0px"});
          graphY = titleHeight;
       }
       $pivotDiv.css({"top":pivotY+"px"});
   }



   if($titleDiv) {
      $titleDiv.css({"top":titleY + "px"});
   }

   if(bsHeight) {
      $bslice.css({"height":bsHeight+"px", "top":frameHeight-bsHeight + "px"});
   }
   else {
      $bslice.idbHide();
   }

   gheight = frameHeight - (pivotBottom ? bsHeight : (bsHeight + pivotHeight)) - titleHeight;
   gwidth = frameWidth;
   var hpad = 10, vpad = 10;
   this.$gslice.css({"top":graphY + "px", "height":gheight+"px"});
   //IDB.log("gheight=" + gheight + ", pivotHeight=" + pivotHeight + ", titleHeight=" + titleHeight + ", frameHeight=" + frameHeight);
    if(chartParamInputPanel) {
         var adjusted = chartParamInputPanel.adjustDimensions(gheight, gwidth);
         gheight = adjusted.height;
         gwidth = adjusted.width;
         hpad = adjusted.hpad;
         vpad = adjusted.vpad;
     }

   if(graph) {
       try {
           gFrame.setAbsMinGW(graph.getAbsMinW());
           gFrame.setAbsMinGH(graph.getAbsMinH());
       } catch(err) {
           IDB.log(err, "ERROR ************************");
       }
   }
        
   $gframe.css({"max-width":gwidth-hpad, "max-height":(gheight-vpad)+"px"});
   $gframe.outerHeight(gheight);
   $gframe.outerWidth(gwidth);
   gFrame.resize();
   if(graph) {
       try {
           graph.resize();
       } catch(err) {
           IDB.log(err.stack, "ERROR **************************************************");
           me.displayUserMessage(me.graph, "An error occurred while calling graph.resize(): " + err);
       }
    }

    if(chartParameterInputPrompt) {
        chartParameterInputPrompt.resize();
    }
 
    if(chartParamInputPanel) {
        chartParamInputPanel.resize();
    }

    if($note) {
       $note.note("adjustLocation");
    }

    if($legend) {
       $legend.legend("adjustLocation");
    }

};


IDB.ChartBase.prototype.pivotSelectionChanged = function(index, selection) {
    IDB.log(selection, "PIVOT SELECTION");
    var me = this, dataSet = me.dataSet, dashboard = me.dashboard;
    dataSet.setPivotSelection(index, selection);
    me.pivotBar.updateSelections();
    me.createOrReplaceGraph(function() {
		dashboard.pivotSelectionChanged(me, dataSet.getPivotAxis(index).axisName, 
			selection.stringValue, dataSet.getPivotSelectionMap());
	});
};

IDB.ChartBase.prototype.createOrReplaceGraph = function(onComplete) {
    
	var me = this, $legend = me.$legend, $callout = me.$callout, 
		dataSet = me.dataSet, $gdiv = me.$gdiv, errMsg;

    $gdiv.empty();

    if(dataSet.numDataRows === 0 || (dataSet.serverStatus === 1410)) { // parameter definition error
        errMsg = dataSet.serverMessage;
        if(!errMsg && me.expandedNoDataMessage !== null) {
            errMsg = me.expandedNoDataMessage;
        }
        else { 
            errMsg = IDB.format("chart.html5.no_data_found"); 
        }
        me.graph = new IDB.ErrorGraph(me, $gdiv, dataSet, errMsg, me.textColor);
        var replace = (me.graph !== null);
        if ( replace ) {
            me.graph.resize();
        }
        if($legend) {
           $legend.remove();
           me.$legend = null;
        }
        if($callout) {
            $callout.remove();
            me.$callout = null;
        }
        onComplete();
        return;
    }

	if ( IDB.ChartBase.MODULE_MODE ) { //ZK
		this.createOrReplaceGraphInModuleMode(onComplete);
	}
	else {
		this.createOrReplaceGraphContinued();
		onComplete();
	}
};

IDB.ChartBase.prototype.createOrReplaceGraphContinued = function() {
	var me = this;
	var replace = (me.graph !== null);
    //me.chartSettings.data.chart.graphTypeCode = "unknown";
    var graph = me.graph = IDB.GraphFactory.buildGraph(me, me.$gdiv, me.chartSettings.data.chart, me.dataSet);
	var graphInfo = graph.getInfo();
    var calloutStyle = graphInfo.calloutStyle;
	
	if ( replace ) {
		graph.resize();
	}

    if(calloutStyle != "none" && me._coVis) {
        me.maybeCreateCallout(calloutStyle);
    }

    if(replace && me.$legend && (graphInfo.legendStyle === IDB.LS.X_VALUES)) {
        me.$legend.legend("refresh");
    }
};

IDB.ChartBase.prototype.createOrReplaceGraphInModuleMode = function(onComplete) { //ZK
	var moduleInfo = IDB.GraphFactory.getScriptModuleInfo(this.chartSettings.data.chart);
	var me = this;

	var handleSuccess = function() {
		me.createOrReplaceGraphContinued();
		onComplete();
	};

	var handleFailure = function() {
		me.graph = new IDB.ErrorGraph(me, me.$gdiv, me.dataSet, 
			'Error: could not load all scripts for this graph module.', me.textColor);
		me.resize();
	};

	IDB.ModuleLoader.loadWithModuleInfo(moduleInfo, handleSuccess, handleFailure);
};

IDB.ChartBase._coColMap = {
   "axisColors":"axisColor",
   "rangeColors":"rangeColor",
   "pie":"rangeColor",
   "none":"axisColor",
   "custom":"axisColor"
};

IDB.ChartBase.prototype.maybeCreateCallout = function(calloutStyle) {
    var me = this;
    if(!me.$callout) {
        me.$callout = $("<div>").appendTo(me.dashboard.$div).callout();
    }
    me.$callout.callout("option", "colorField", IDB.ChartBase._coColMap[calloutStyle] || "axisColor");

};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.PivotBar

IDB.PivotBar = function($div, chart) {
   var me = this, dataSet = chart.dataSet, numDataRows = dataSet.numDataRows,
       numPivots = dataSet.numPivots;
   me.$div = $div;
   me.chart = chart;
//   this.$div.css("border", "1px solid blue");
   me.selectors = [];
   for(var K=0; K<numPivots; K++) {
       var $sel = $("<select>").appendTo(me.$div);
       me.selectors.push($sel);
       $sel.on("change", K, IDB.listener(me, "onChange"));
   }
   me.updateSelections();
   me.setVisible(numDataRows > 0);
};

IDB.PivotBar.prototype.setVisible = function(visible) {
    this.$div.css({"visibility":(visible ? "visible" : "hidden")});
};

IDB.PivotBar.prototype.updateSelections = function() {

   var me = this, chart = me.chart, dataSet = chart.dataSet, pivotLists = dataSet.getPivotLists(), 
       selections = dataSet.pivotSelections, list, $sel, selectedVal, selectors = me.selectors, $opt;
   for(var K=0; K<pivotLists.length; K++) {
       list = pivotLists[K];
       $sel = selectors[K];
       $sel.empty();
       selectedVal = selections[K];
       for(var j=0, jj=list.length; j<jj; j++) {
           $opt = $("<option>").appendTo($sel).text(list[j].formattedValue);
           $opt.attr("value", list[j]);
           $opt.data("DV", list[j]);
           if(list[j].stringValue == selectedVal.stringValue) {
//             IDB.log(K + " selecting " + list[j]);
             $opt.attr("selected", true);
           }
       }
   }
};

IDB.PivotBar.prototype.onChange = function(evt) {
    var $opt = $(evt.target).find(":selected");
    this.chart.pivotSelectionChanged(evt.data, $opt.data("DV"));
};



/*global IDB */
IDB.GFrame = function($div, gm) {
   var me = this;
   me.$div = $div;
   $div.addClass("idb-gf-div");
   $div.css({"padding":"0px", "overflow":"hidden"});
   me._tm = gm.T;
   me._bm = gm.B;
   me._lm = gm.L;
   me._rm = gm.R;
   me._mw = gm.MW;
   me._mh = gm.MH;
   me._mgw = ((20 <= me._mw && me._mw <= 100) ? me._mw : 100);
   me._mgh = ((20 <= me._mh && me._mh <= 100) ? me._mh : 100);
   me._absMinGW = 20;
   me._absMinGH = 20;
};

IDB.GFrame.prototype.setAbsMinGW = function(n) {
    n = n || 20;
    this._absMinGW = Math.min(100, Math.max(20, n));
};

IDB.GFrame.prototype.setAbsMinGH = function(n) {
    n = n || 20;
    this._absMinGH = Math.min(100, Math.max(20, n));
};

IDB.GFrame.prototype.setGDiv = function($gdiv) {
    var me = this, $div = me.$div;
    // If there is a current $gdiv that is a child of $div, its jQuery data will be destroyed
    // by using .empty() to remove it.
    $div.empty(); 
    me.$gdiv = $gdiv;
    $gdiv.appendTo($div);
    $gdiv.addClass("idb-g-div").css({"position":"absolute"});
};

IDB.GFrame.prototype.resize = function() {
    var me = this, $div = me.$div, 
        w = $div.innerWidth(), h = $div.innerHeight(),  
        G = IDB.getChildPos(w, h, {top:me._tm, bottom:me._bm, left:me._lm, right:me._rm}, 
                                 {maxWidth:me._mw, maxHeight:me._mh, minWidth:me._mgw, minHeight:me._mgh, absMinWidth:me._absMinGW, absMinHeight:me._absMinGH});

    me.$gdiv.css(G.css);

};

/*global IDB, jQuery */

(function($, IDB) {

    /**
     * LegendStyle constants - Since these are referenced inside a self-executing function, they have to 
     * exist before the function is defined/executed.
     */
    Object.defineProperty(IDB, "LS", {
        value:{
            NONE:"none",
            AXES:"axes",
            X_VALUES:"xValues",
            RANGES:"ranges",
            CUSTOM:"custom",
            CUSTOM_PER_AXIS:"customPerAxis"
        },
        writable:false,
        enumerable:false,
        configurable:false
    });

    IDB.LS.isCustom = function(ls) {
        return (ls === "custom" || ls === "customPerAxis");
    };

    IDB.LS.isPerAxis = function(ls) {
        return (ls === "ranges" || ls === "customPerAxis");
    };

    
    Object.freeze(IDB.LS);

    var LS = IDB.LS;


    $.widget("idb.legend", $.idb.floatingPanel, {
        options:{
           name:"legend",
           legendTitle:"Legend",
           bgColor:IDB.rgb(0xFFFFFF),
           axisList:[],
           legendStyle:LS.AXES,
           customItems:null,
           dataSet:null
        },
        _create:function() {
            var me = this, $e = me.element;
            me._super(); // call _create() in the floatingPanel widget.

            me._yAxisIndex = 1;

            // note: see the idb-legend-* classes in idbhtml.css. The styles they apply to the child elements are needed for
            // the Legend to function correctly.
            $e.addClass("idb-legend");
            me.$_titlebar = $("<div class=\"idb-legend-titlebar idb-header\">").appendTo($e);
            
            me.$_content = $("<div class=\"idb-legend-content\">").appendTo($e)
                  .on("click", IDB.listener(me,  "_toggleOpaque"));
            me._buildHeader();
            me._layout();
            me.adjustLocation();
        },
        _setOption:function(key, value) {
            var me = this, options = me.options;
            options[key] = value;
            if(key === "legendTitle") {
                me._setTitle(value);
                return;
            }
            me._super(key, value);
        },
        _setTitle:function(title) {
            var me = this, $titletext = me.$_titletext;
            if(!$titletext) {
               return;
            }
            $titletext.html(title);
        },
//        destroy: function() {
//
//        },

        // private functions
        _buildHeader:function() {
            var me = this, o = me.options, legendStyle = o.legendStyle, axisList = o.axisList, $header = me.$_titlebar, $leftButton, $rightButton,
               atag = '<a href="#">', partialClasses = "idb-legend-btn idb-legend-btn-",
               needButtons = (((legendStyle === LS.RANGES) || (legendStyle === LS.CUSTOM_PER_AXIS)) && axisList.length > 2);
            if(needButtons) {
                $leftButton = $(atag).appendTo($header).addClass(partialClasses + "l").click(IDB.listener(me,  "prevAxis"));
            }
            me.$_titletext = $('<div class="idb-legend-titletext">').appendTo($header);
            // See http://phrogz.net/CSS/vertical-align/ (Method 2) for how the text gets vertically aligned in the titlebar.
            if(needButtons) {
                $rightButton = $(atag).appendTo($header).addClass(partialClasses + "r").click(IDB.listener(me,  "nextAxis"));
            }
            me._setTitle(o.legendTitle);
        },
        _layout:function() {
            var me = this, $e = me.element, $c = me.$_content, o = me.options, aa = (o.axisList || [null]), 
              cw = 50, lineH = 24, // lineH must agree with idbhtml.css. 
              $t, $tr, $td, $titlebar = me.$_titlebar, $titletext = me.$_titletext, $bg = me.$_bg, legendStyle = o.legendStyle, legendItems = [], legendItem,
              hpad = 13, needScroll, titletextWidth, yAxisIndex, yAxis, unit, title, customItems = o.customItems, numItems = 0;
            $c.empty();
            $t = $('<table class="idb-legend-table">').appendTo($c);
            if(customItems && customItems.length) {
                numItems = customItems.length;
                for(var j=0; j<numItems; j++) {
                    $tr = customItems[j];
                    $tr.appendTo($t);
                    cw = Math.max(cw, $tr.width());
                }
            }
            else {
                if(legendStyle === LS.AXES) {
                    legendItems = aa.slice(1);    
                }
                else if(legendStyle === LS.RANGES) {
                    yAxisIndex = me._yAxisIndex;
                    yAxis = aa[yAxisIndex];
                    
                    if(yAxis) {
                        unit = yAxis.unit; 
                        title = yAxis.axisName + ( unit ? ( " (" + unit + ")" ) : '' );
                        me._setTitle(title);
                        legendItems = yAxis.getRangeList();
                        if(legendItems.length === 0) {
                            legendItems = aa[0].getRangeList();
                        }
                        if(legendItems.length === 0) {
                            legendItems = aa[0].getDefaultRangeList();
                        }
                    }
               }
               else if(legendStyle === LS.X_VALUES) {
                   legendItems = o.dataSet.getXValueLegendItems();
               }
    
               numItems = legendItems.length;
    
               for(var j=0, jj=legendItems.length; j<jj; j++) {
                  legendItem = legendItems[j];
                  $tr = $("<tr>").appendTo($t);
                  $td = $('<td class="idb-legend-item-icon">').appendTo($tr);
                  IDB.$kIcon("square", legendItem.color.css).appendTo($td);
                  $td = $('<td class="idb-legend-item-label">').appendTo($tr).text(legendItem.label);
                  cw = Math.max(cw, $tr.width());
               }
          }
          needScroll = numItems > 10;
          if(needScroll) {
             $c.addClass("idb-scroll-y");
             hpad += 12;
          }
          else {
            $c.removeClass("idb-scroll-y");
          }

          // Removing any previously set explict width will cause
          // the value returned by $titletext.width() to be the value
          // based on the actual text width.
          $titletext.css({"width":"auto"});
          titletextWidth = $titletext.width();

//          IDB.log("Header width: " + titletextWidth);

          var headerHeight = 22, /* (20 h) + (2 padding) */
              contentHeight = (Math.min(10, numItems)*(lineH)) + 10, /* padding top and bottom = 10 */
              W = cw+hpad, H = contentHeight + headerHeight; // (5+5+20)
//          IDB.log("cw=" + cw + ", hpad=" + hpad + ", W=" + W + ", H=" + H);

          // Let the width be as wide as it needs to be to accomodate long range labels, but don't let axis names
          // (in the legend items or in the header text) or X values push the width past 250.
          if(legendStyle === LS.RANGES) {
             W = Math.max(W, Math.min(250, titletextWidth+44));
          }
          else {
             W = Math.min(250, Math.max(W, titletextWidth+44));
          }
          if(titletextWidth > (W-44)) {
             $titletext.width(W-44);
          }

          $e.width(W).height(H);
          $c.outerHeight(contentHeight).outerWidth(W);
          $titlebar.outerWidth(W);
          $bg.outerWidth(W).outerHeight(H);
          me._locateCloseButton();
        },
        refresh:function(dataSet) {
           var me = this, options = me.options;
           if(dataSet) {
               options.dataSet = dataSet;
           }
           me._layout();
        },
        setSelectedYAxis:function(yAxis) {
           var me = this, o = me.options, axisList = o.axisList, found = false;
           for(var j=0, jj=axisList.length; j<jj; j++) {
               if(axisList[j] === yAxis) {
                   me._yAxisIndex = j;
                   found = true;
                   break;
               }
           }
           if(found) {
             me._layout();
           }
        },
        nextAxis:function() {
           var me = this, o = me.options, aa = (o.axisList || []), index = me._yAxisIndex, legendStyle = o.legendStyle;
           if(legendStyle !== LS.RANGES) {
              return;
           }
           index++;
           if(index > (aa.length-1)) {
             index = 1;
           }
           me._yAxisIndex = index;
           me._layout();
        },
        prevAxis:function() {
           var me = this, o = me.options, aa = (o.axisList || []), index = me._yAxisIndex, legendStyle = o.legendStyle;
           if(legendStyle !== LS.RANGES) {
              return;
           }
           index--;
           if(index < 1) {
             index = aa.length-1;
           }
           me._yAxisIndex = index;
           me._layout();
        }
    });
})(jQuery, IDB);


/*====================================================================================================*/
IDB.ModuleLoader = {};
IDB.ModuleLoader.Cache = {};
IDB.ModuleLoader.JsDir = 'html5/';
IDB.ModuleLoader.PreloadI = 0;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ModuleLoader.nextPreload = function() {
	if ( !IDB.ChartBase.MODULE_MODE ) {
		return;
	}

	var modLoad = IDB.ModuleLoader;
	var allMods = IDB.GraphFactory.AllModules;

	if ( modLoad.PreloadI >= allMods.length ) {
		return;
	}

	var name = allMods[modLoad.PreloadI++];

	var handleLoad = function() {
		modLoad.nextPreload();
	};
	
	modLoad._load(name, handleLoad, handleLoad);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.ModuleLoader.loadWithModuleInfo = function(moduleInfo, onSuccess, onFailure) {
	console.log("Load With Module Info: %o", moduleInfo);

	var loadCount = 0;
	var names = [ moduleInfo.module ].concat(moduleInfo.depends);
	var failed = false;
	var i;
	
	var handleLoad = function() {
		if ( ++loadCount < names.length ) {
			return;
		}

		(failed ? onFailure : onSuccess)();
	};

	var handleSuccess = function() {
		handleLoad();
	};
	
	var handleFailure = function() {
		failed = true;
		handleLoad();
	};

	for ( i in names ) {
		IDB.ModuleLoader._load(names[i], handleSuccess, handleFailure);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ModuleLoader._load = function(name, onSuccess, onFailure, isRetry) {
	var suffix = (IDB.MinJs ? '.min' : '');
	var fileUrl = IDB.config.contextRoot + IDB.ModuleLoader.JsDir + 'IDB.module.'+name+suffix+'.js';
	var cacheItem = IDB.ModuleLoader.Cache[name];
	
	if ( !cacheItem ) {
		cacheItem = {
			loading: false,
			success: false,
			failureCount: 0,
			successListeners: [],
			failureListeners: []
		};

		IDB.ModuleLoader.Cache[name] = cacheItem;
	}

	if ( cacheItem.success ) {
		console.log('Loading skipped (completed): %o', fileUrl);
		onSuccess(fileUrl);
		return;
	}
	
	if ( !isRetry ) {
		cacheItem.successListeners.push(onSuccess);
		cacheItem.failureListeners.push(onFailure);
	}
	
	if ( cacheItem.loading ) {
		console.log('Loading skipped (in progress): %o (listeners: %o)', 
			fileUrl, cacheItem.successListeners.length);
		return;
	}

	////

	var handleDone = function(script, textStatus) {
		cacheItem.success = true;
//		console.log('Success: %o (textStatus: %o)', fileUrl, textStatus);
		IDB.ModuleLoader._callAll(cacheItem.successListeners, fileUrl);
		cacheItem.successListeners = null;
		cacheItem.failureListeners = null;
	};

	var handleFail = function(jqxhr, settings, exception) {
		++cacheItem.failureCount;
		console.log('Failure: %o (fail count: %o) \n- jqxhr: %o \n- settings: %o '+
			'\n- exception: %o', fileUrl, cacheItem.failureCount, jqxhr, settings, exception);

		if ( cacheItem.failureCount >= 3 ) {
			IDB.ModuleLoader._callAll(cacheItem.failureListeners, fileUrl);
			cacheItem.successListeners = null;
			cacheItem.failureListeners = null;
		}
		else {
			cacheItem.loading = false;
			IDB.ModuleLoader._load(name, onSuccess, onFailure, true); //retry
		}
	};

	////

//	console.log('Loading '+(isRetry ? 're-' : '')+'started: %o', fileUrl);
	cacheItem.loading = true;

	jQuery
		.ajaxPrefilter('script', function(options) {
			options.crossDomain = true;
		});

	cacheItem.jqxhr = jQuery
		.ajax({
			dataType: 'script',
			timeout: 3000,
			cache: true,
			url: fileUrl
		})
		.done(handleDone)
		.fail(handleFail);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.ModuleLoader._callAll = function(listeners, fileUrl) {
	for ( var i in listeners ) {
		listeners[i](fileUrl);
	}
};

if ( IDB.skipModuleLoad !== true ) {
	setTimeout(IDB.ModuleLoader.nextPreload, 1); //wait one frame
};

/*global IDB, $ */

(function(IDB) {
    $.widget("idb.note", $.idb.floatingPanel, {
        options:{
           name:"note",
           noteText:"",
           bgColor:IDB.rgb(0xEEEE00),
           title:""
        },
        _create:function() {
             // note: see the idb-fp-* and idb-note-* classes in idbhtml.css. The styles they apply to the child elements are needed for
             // the Note to function correctly.
             this._super();
             var me = this, $e = this.element, o = this.options, txt = (o.noteText || "").replace(/\n/g, "<br/>"),
                 $header = me.$_header = $('<div class="idb-note-titlebar">').appendTo($e);
             me.$_titletext = $('<div class="idb-titletext idb-note-titletext">').appendTo($header).text(o.title || "");
             me.$_content = $('<div class="idb-note-content">').appendTo($e).html(txt).on("click", IDB.listener(me,  "_toggleOpaque"));
             $e.resizable({"containment":"parent"}).width(o.W).height(o.H).css({"min-height":"35px"});
             me.adjustLocation();
        }/*,
        _setOption:function(k, v) {
            var o = this.options;

            o[k] = v;
            if(k === "title") {
                this.$_header.text(v);
                o[k] = v;
            }
            else if(k === "noteText") {
               this.$_content.html((v || "").replace(/\n/g, "<br/>"));
               o[k] = v;
            }
            else {
                this._super(k, v);
            }
        } */
    });
})(IDB);

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.DataSet
/*global IDB */
IDB.DataSet = function(datapacket, axisList, pivotAxes) {
//    IDB.log(datapacket, "CHART DATA");
    var me = this, chart = datapacket.chart, pivots = chart.pivots;

    // First, set the properties that will never change, regardless of whether or not
    // we have data when this constructor is invoked.
    me._axisList = axisList;
    me._pivotAxes = (pivotAxes || null);
    me._numPivots = pivotAxes ? pivotAxes.length : 0;
    me._pivotSortOrder = pivots.sort;
    me._userXColors = chart.xColors;
    me._chartTitle = chart.chartTitle;
    me._graphTypeCode = chart.graphTypeCode;

    if(datapacket.chartParameterInputRequired){
        me._initProps(datapacket);
        return;
    }
    else {
        me.createOrReplaceDataRows(datapacket); 
    }
};

IDB.DataSet.prototype._initProps = function(datapacket) {
    var me = this;
    me._complete = false;
    me._pivotMap = null;
    me._pivotSelections = null;
    me._pivotSelectionsInitialized = false;
    me._nextRowId = 0;
    me._xColorMap = null;
    me._xValueSet = {};
    me._xValues = [];
    me._xValueLegendItems = null;

    // Following properties all come from the datapacket.
    me.serverStatus = datapacket.status;
    me.serverConfirmation = datapacket.confirmation;
    me.serverMessage = datapacket.message;
    me._numDataRows = datapacket.numDataRows || 0;
    me._maxRowsExceeded = !!datapacket.maxRowsExceeded;
};


IDB.DataSet.prototype.createOrReplaceDataRows = function(datapacket) {
    var me = this, numPivots = me.numPivots, hasPivots = me.hasPivots,
        overallStats = datapacket.overallStats || [], numDataRows, hasData, pivotArr, pivotKey, xAxis = me._axisList[0],
        xAxisRanges = xAxis._ranges; // must be ._ranges, not .ranges.

    me._initProps(datapacket);

    numDataRows = me._numDataRows;
    hasData = numDataRows > 0;

    if(hasPivots) {
        me._pivotMap = new IDB.MultikeyMap(numPivots);
        me._pivotSelections = new Array(numPivots); 
        pivotArr = datapacket.pivots || [];
        pivotKey = new Array(numPivots);
        for(var j=0, jj=pivotArr.length; j<jj; j++) {
            this._parsePivot(this._pivotMap, numPivots, pivotKey, pivotArr[j], this._pivotAxes, this._axisList);
        } 
    }
    else {
        if(hasData) {
            me._topLevelDataRows = me._parseRows(datapacket.rows);
//            IDB.log("_topLevelDataRows:");
//            IDB.log(this._topLevelDataRows);
        }
        else {
            me._topLevelDataRows = [];
        }
    }

    if(hasPivots) {
        me.maybeRestoreSavedPivotSelections();
        me.maybeSavePivotSelections();
    }

    me.topLevelNullsAreZeroStatSets = [];
    me.topLevelNonNullValuesStatSets = [];
    for(var j=0, jj=overallStats.length; j<jj; j++) {
        var o = overallStats[j];
        if(!o) {
            me.topLevelNullsAreZeroStatSets.push(null);
            me.topLevelNonNullValuesStatSets.push(null);
        }
        else {
            me.topLevelNullsAreZeroStatSets.push(o.nullsAreZero);
            me.topLevelNonNullValuesStatSets.push(o.nonNullValues);
        }
    }

    if(!xAxisRanges || xAxisRanges.length === 0) {
        xAxis.setDefaultRangeList(me.getDefaultRangeList());
    }

    me._complete = true;
};



Object.defineProperties(IDB.DataSet.prototype,
{
   axisList:{get:function() {return this._axisList;}},
   complete:{get:function() {return this._complete;}},
   hasPivots:{get:function() {return (this._numPivots > 0);}},
   numDataRows:{get:function() {return this._numDataRows;}},
   numPivots:{get:function() {return this._numPivots;}},
   pivotSelections:{get:function() {return this._pivotSelections;}},
   pivotSortOrder:{get:function() {return this._pivotSortOrder;}},
   maxRowsExceeded:{get:function() {return this._maxRowsExceeded;}}
});

IDB.DataSet.prototype.getPivotAxis = function(pivotAxisIndex) {
     return this._pivotAxes[pivotAxisIndex];
};


IDB.DataSet.prototype.getYGraphAxisWithName = function(axisName, numericYOnly) {

    var axisList = this._axisList;
    for(var j=1, jj=axisList.length; j<jj; j++) {
        var axis = axisList[j];
        if(axis.isGraphAxis(numericYOnly) && (axisName == axis.axisName)) {
            return axis;
        }
    }
    return null;
};


/**
 * Returns the highest Y value for the current pivot slice, or the entire dataset
 * if there are no pivots. Pivot and hidden axes are not included.
 */
IDB.DataSet.prototype.getMaxYValue = function() { // ():Number {
    if(this._numDataRows === 0) return NaN;
    if(this.hasPivots) {
        var pd = this.getPivotData();
        return this._getHighestMax( pd.nullsAreZeroStatSets);
    }
    else {
        return this._getHighestMax(this.topLevelNullsAreZeroStatSets);
    }
};

/**
 * Returns the highest Y value for the the entire dataset.
 */
IDB.DataSet.prototype.getOverallMaxYValue = function() { // ():Number {
    return this._getHighestMax(this.topLevelNullsAreZeroStatSets);
};

/**
 * Returns the lowest Y value for the the entire dataset.
 */
IDB.DataSet.prototype.getOverallMinYValue = function() { // ():Number {
    return this._getLowestMin(this.topLevelNullsAreZeroStatSets);
};



/**
 * Returns the lowest Y value for the current pivot slice, or the entire dataset
 * if there are no pivots. Pivot and hidden axes are not included.
 */
IDB.DataSet.prototype.getMinYValue = function() { // ():Number {

    if(this.hasPivots && this._numDataRows > 0) {
        var pd = this.getPivotData();
        return this._getLowestMin( pd.nullsAreZeroStatSets );
    }
    else {
        return this._getLowestMin( this.topLevelNullsAreZeroStatSets );
    }
};


IDB.DataSet.prototype._getHighestMax = function(statSets) { //(statSets:Array):Number {
    if(statSets === null) return NaN;
    var max = Number.NEGATIVE_INFINITY;
    for(var j=0, jj=statSets.length; j<jj; j++) {
        var s = statSets[j];
        if(s) {
            var axis = this._axisList[j];
            if(!axis.isGraphAxis(true)) continue;
            if(s.max > max) max = s.max;
        }
    }
    if(max === Number.NEGATIVE_INFINITY) return NaN;
    return max;
};

IDB.DataSet.prototype._getLowestMin = function(statSets) { //(statSets:Array):Number {
    if(statSets === null) return NaN;
    var min = Number.POSITIVE_INFINITY;
    for(var j=0, jj=statSets.length; j<jj; j++) {
        var s = statSets[j];
        if(s) {
            var axis = this._axisList[j];
            if(!axis.isGraphAxis(true)) continue;
            if(s.min < min) min = s.min;
        }
    }
    if(min === Number.POSITIVE_INFINITY) return NaN;
    return min;
};





IDB.DataSet.prototype.getOverallMaxY = function() {
   var max = 0;
   var axes = this.getGraphAxes(true);
//   IDB.log(axes, "AXES");
   for(var j=1, jj=axes.length; j<jj; j++) {
       var stats = this.overallStats[axes[j].axisId];
       if(stats) max = Math.max(max, stats.nullsAreZero.max);
       if(stats) stats = stats.nullsAreZero;
       max = Math.max(max, stats.max);
   }
   return max;
};

IDB.DataSet.prototype.getOverallMinY = function() {
   var min = 0;
   var axes = this.getGraphAxes(true);
   for(var j=1, jj=axes.length; j<jj; j++) {
       var stats = this.overallStats[axes[j].axisId];
       if(stats) min = Math.min(min, stats.nullsAreZero.min);
   }
   return min;
};

IDB.DataSet.prototype.getGraphAxes = function(numericYOnly) {
    var axes = [this._axisList[0]];
    for(var j=1, jj=this._axisList.length; j<jj; j++) {
        if(this._axisList[j].isGraphAxis(numericYOnly)) {
           axes.push(this._axisList[j]);
        }
    }
    return axes;
};

IDB.DataSet.prototype.getNumYGraphAxes = function(numericYOnly) {
     return this.getGraphAxes(numericYOnly).length - 1;
};

IDB.DataSet.prototype.getPivotSelections = function() {
    if(this.numPivots === 0) return [];
    if(!this._pivotSelectionsInitialized) this.getPivotLists();
    return this._pivotSelections.concat();
};

IDB.DataSet.prototype.getPivotSelectionFilters = function() {
    var results = [];
    if(this.numPivots === 0) return results;
    var selections = this.getPivotSelections();
    var pivotAxisList = this._pivotAxes;
    for(var j=0, jj=pivotAxisList.length; j<jj; j++) {
        var axis = pivotAxisList[j];
        if(!axis.sendOnDrilldown) {
//            IDB.log("DataSet.getPivotSelectionFilters(): Skipping pivot axis " + axis.axisName);
            continue;
        }
        var dv = selections[j];
        results.push(new IDB.Filter(axis.axisName, dv.stringValue, dv.fv));
    }
    return results;
};

IDB.DataSet.prototype.getFirstPivotAxisName = function() {
    if(!this.hasPivots) return null;
    return this._pivotAxes[0].axisName;
};

IDB.DataSet.prototype.getFirstPivotSelection = function() { // returns String
    if(!this.hasPivots) return null;
    return this._pivotSelections[0].stringValue;
};



IDB.DataSet.prototype.getPivotSelectionMap = function() {
    var selections = this.getPivotSelections();
    var map = {};
    if(this.numPivots === 0) {return map;}
    var j=0;
    var pivotAxisList = this._pivotAxes;
    for(j=0; j<pivotAxisList.length; j++) {
        var axis = pivotAxisList[j];
        map[axis.axisName] = selections[j].stringValue;
    }
    return map;

};


IDB.DataSet.prototype.maybeRestoreSavedPivotSelections = function() {
    var pivotAxisList = this._pivotAxes;
    if(!pivotAxisList || pivotAxisList.length === 0 || this._numDataRows === 0) return;
    var pivotAxis = pivotAxisList[0];
    var axisName = pivotAxis.axisName;
    if(!pivotAxis.pivotSelection) return;
    var sel = pivotAxis.pivotSelection;
    var allSelections = {};

    for(var j=0, jj=pivotAxisList.length; j<jj; j++) {
        pivotAxis = pivotAxisList[j];
        allSelections[pivotAxis.axisName] = pivotAxis.pivotSelection || "";
    }

    this.maybeChangePivotSelections(axisName, sel, allSelections);

};

IDB.DataSet.prototype.maybeSavePivotSelections = function() {
    if(this._numPivots < 1) return;

    if(!this._pivotSelectionsInitialized) this.getPivotLists();
    var pivotAxisList = this._pivotAxes;
    var pivotSelections = this._pivotSelections;
    for(var j=0; j<pivotAxisList.length; j++) {
        pivotAxisList[j].pivotSelection = pivotSelections[j] ? pivotSelections[j].stringValue : null;
    }
};


IDB.DataSet.prototype.maybeChangePivotSelections = function(axisName, valueAsString, allSelections) {
    if(this.numPivots < 1) return false;
    if(this._numDataRows < 2) return false;
    var index = -1;
    var pivotAxisList = this._pivotAxes;
    var j=0;
    for(j=0; j<pivotAxisList.length; j++) {
        var axis = pivotAxisList[j];
        if(axis.axisName == axisName) {
            index = j;
            break;
        }
    }

    if(index == -1) return false;
    var thisMap = this._pivotMap;
    // we have to traverse down the branch of the tree of maps to
    // get the one for this axis.
    var selections = this._pivotSelections;
    for(j=0; j<index; j++) {
        var pivotSelection = selections[j];
        thisMap = thisMap.getObject(pivotSelection);
    }

    IDB.log(thisMap, "FOUND MAP:");

    var key = thisMap.getRealKeyFromStringKey(valueAsString);
    if(key === undefined) return false;

    // now that we know this pivot value exists for this axis, set the selection
    // for that axis. This will change the pivot selections for all of the child-ward pivots.

    this.setPivotSelection(index, key);

    // Now, we want to try to synchronize as many child-ward pivots to values
    // contained in the allSelections map.

    if(index == (this.numPivots-1)) {
        return true; // no more pivots after this one.
    }

    // get the next axis after this one
    var nextPivotAxis = pivotAxisList[index+1];

    // do we have a potential selection for the next pivot axis?
    if(nextPivotAxis.axisName in allSelections) { 
        // If so, call this function recursively.
        this.maybeChangePivotSelections(nextPivotAxis.axisName, "" + allSelections[nextPivotAxis.axisName], allSelections);
    }

    return true;

};


IDB.DataSet.prototype.setPivotSelection = function(pivotIndex, val) {
//            debug("DataSet.setPivotSelection(): pivotIndex=" + pivotIndex + ",val=" + val);
    if(!this._pivotSelectionsInitialized) this.getPivotLists();
    this._pivotSelections[pivotIndex] = val;
    this._pivotAxes[pivotIndex].pivotSelection = this._pivotSelections[pivotIndex].stringValue;
    return this.getPivotLists();
};


IDB.DataSet.prototype.getDataRows = function() {
    if(this._numDataRows === 0) return [];
    if(this.numPivots > 0) {
        if(!this._pivotSelectionsInitialized) {
            this.getPivotLists();
        }
        var pd = this._pivotMap.get(this._pivotSelections);
        return pd.dataRows;
    }
    else {
        return this._topLevelDataRows;
    }
};

IDB.DataSet.prototype.getPivotLists = function() {
    var me = this,  pivotLists = [], numPivots = me.numPivots, thisMap = me._pivotMap,
       pivotSortOrder = me._pivotSortOrder, pivotSelection, pivotSelections = me._pivotSelections,
       levelKeys, obj;

    if(numPivots === 0) {return pivotLists;}
    var j=0;
    if(me._numDataRows === 0) {
        for(j=0; j<this.numPivots; j++) {
            pivotLists.push([]);
        }
        return pivotLists;
    }

    for(j=0; j<numPivots; j++) {
        levelKeys = thisMap.getLevelKeys().concat();
        if(this._pivotSortOrder == "A") {
            levelKeys.sort(IDB.DataValue.compare);
        }
        else {
            levelKeys.sort(IDB.DataValue.rcompare);
        }
        pivotLists.push(levelKeys);
        pivotSelection = pivotSelections[j];
        if(pivotSelection === undefined) {
            pivotSelection = levelKeys[0];
            pivotSelections[j] = pivotSelection;
        }

        obj = thisMap.getObject(pivotSelection);
        if(obj === undefined || obj === null) {
            pivotSelection = levelKeys[0];
            pivotSelections[j] = pivotSelection;
            obj = thisMap.getObject(pivotSelection);
        }

        if(j == (numPivots-1)) break;
        if(obj === null || obj === undefined) {
            throw new Error("Can't select pivot list. level=" + j + ", pivotSelection=" + pivotSelection);
        }

        thisMap = obj;
    }
    this._pivotSelectionsInitialized = true;
    return pivotLists;
};


IDB.DataSet.prototype._parsePivot = function(pivotMap, numLevels, pivotKey, pivotJSON, pivotAxes, axes) {
    var axisList = this._axisList;
    var numPivots = pivotMap.numKeys;
    var thisLevel = numPivots - numLevels;
    var pivotAxis = pivotAxes[thisLevel];
    var pivotVal = new IDB.DataValue(pivotAxis.dataType, pivotJSON.value);
    pivotKey[thisLevel] = pivotVal;
    var j=0;
    if(numLevels == 1) { // terminal
//        IDB.log(pivotJSON.rows, "JSON Rows: " + pivotKey);
        var rows = this._parseRows(pivotJSON.rows);
        var pivotStats = pivotJSON.pivotStats;
        var nonNullValuesStatSets = new Array(axisList.length);
        var nullsAreZeroStatSets = new Array(axisList.length);
        for(j=0; j<axisList.length; j++) {
            var axis = axisList[j];
            if(axis.dataTypeIsNumber) {
                nonNullValuesStatSets[j] = pivotStats[j].nonNullValues;
                nullsAreZeroStatSets[j] = pivotStats[j].nullsAreZero;
            }
        }

        pivotMap.put(pivotKey, new IDB.PivotData(rows, nullsAreZeroStatSets, nonNullValuesStatSets));
        return;
    }

    var pivots = pivotJSON.pivots;
    for(j=0; j<pivots.length; j++) {
        var p = pivots[j];
        this._parsePivot(pivotMap, numLevels-1, pivotKey, p, pivotAxes, axes);
    }


};

IDB.DataSet.prototype._parseRows = function(rows) {
    var axisList = this._axisList;
    var newRowList = [];
    for(var rowIndex=0; rowIndex<rows.length; rowIndex++) {
        var row = rows[rowIndex];
        var newRow = new Array(axisList.length);
        newRow.rowId = this._nextRowId++;
        for(var j=0, jj=axisList.length; j<jj; j++) {
            var axis = axisList[j];
            newRow[j] = new IDB.DataValue(axis.dataType, row[j]);
            
        }
        newRowList.push(newRow);
        var xVal = newRow[0].stringValue;
        if(!this._xValueSet[xVal]) {
            this._xValueSet[xVal] = true;
            this._xValues.push(xVal);    
        }
    }
    return newRowList;
};


IDB.DataSet.prototype._buildXColorMap = function() {
   var colorSet = new IDB.ColorSet(11);
   var xColorMap = this._xColorMap = {};
   var xVals = this._xValues, xVal = null;
   var userColors = this._userXColors;
   var hasUserColors = userColors ? true : false; // faster evaluation?
   for(var j=0, jj=xVals.length; j<jj; j++) {
       xVal = xVals[j];
       var rgb = hasUserColors && userColors[xVal];
       if(rgb) {
          xColorMap[xVal] = rgb;
       }
       else {
          xColorMap[xVal] = colorSet.getNextColor();
       }
   }
};

/**
 * When a graph uses a particular color to represent a particular X value, it should use
 * the color returned by this function. The argument should be the string returned by the stringValue
 * property of the DataValue object holding the X value.
 * 
 * @see com.iDashboards.graphs.models.DataValue#stringValue
 */ 
IDB.DataSet.prototype.getColorForXValue = function(xStringValue) {
   if(!this._xColorMap) {
       this._buildXColorMap();
   }
   return this._xColorMap[xStringValue];
};

IDB.DataSet.prototype.createXValueLegendItems = function() {
   var items = [], rows = this.getDataRows(), xValue;
   for(var j=0, jj=rows.length; j<jj; j++) {
       xValue = rows[j][0];
       items.push({
           color:this.getColorForXValue(xValue.stringValue),
           label:xValue.formattedValue
       });
   }
   return items;
};

IDB.DataSet.prototype.getXValueLegendItems = function() {
    var items, pd;
    if(this.hasPivots) {
        pd = this.getPivotData();
        items = pd.xValueLegendItems;
        if(!items) {
           items = this.createXValueLegendItems();
           pd.xValueLegendItems = items;
        }
    }
    else {
        items = this._xValueLegendItems;
        if(!items) {
           items = this.createXValueLegendItems();
           this._xValueLegendItems = items;
        }
    }
    return items;

};



IDB.DataSet.prototype.getGraphDataColumnIndices = function(numericYOnly) {
    var list = [];

    for ( var j = 0 ; j < this._axisList.length ; j++ ) {
        var axis = this._axisList[j];

        if ( axis.isGraphAxis(numericYOnly) ) {
            list.push(j);
        }
    }

    return list;
};

IDB.DataSet.prototype.getStatSets = function() {
	var obj = {};

	if (this.hasPivots && this._numDataRows > 0 ) {
		var pd = this.getPivotData();
		obj.statSets = pd.nullsAreZeroStatSets;
		obj.nullsAreZeroStatSets = pd.nullsAreZeroStatSets;
		obj.nonNullValuesStatSets = pd.nonNullValuesStatSets;
	}
	else {
		obj.statSets = this.topLevelNullsAreZeroStatSets;
		obj.nullsAreZeroStatSets = this.topLevelNullsAreZeroStatSets;
		obj.nonNullValuesStatSets = this.topLevelNonNullValuesStatSets;
	}

	return obj;
};


IDB.DataSet.prototype.getPivotData = function() {   // Returns PivotData

    if(this.hasPivots) {
        if(this._numDataRows === 0) return new IDB.PivotData([], this.topLevelNullsAreZeroStatSets, this.topLevelNonNullValuesStatSets);
        if(!this._pivotSelectionsInitialized) this.getPivotLists();
        return this._pivotMap.get(this._pivotSelections);
    }
    else {
        return null;
    }
};

IDB.DataSet.prototype.getDefaultRangeList = function() { //():Array {
    
    var hiVal = Math.max(0, this.getOverallMaxYValue());
    var loVal = Math.min(0, this.getOverallMinYValue());
    if(isNaN(hiVal) || isNaN(loVal) || (hiVal <= loVal)) {
        return [this.getSingleRange()];
    }
    return IDB.DataSet.makeDefaultRangeList(loVal, hiVal);
};

IDB.DataSet.makeDefaultRangeList = function (loVal, hiVal) {
    var obj = IDB.Calculator.getDivisionMetrics(loVal, hiVal, 5, true);
    var dm = obj.smallDivisions;
    if(dm.boundaries.length > 6) dm = obj.largeDivisions;
    var ranges = IDB.Range.makeAutoRanges(dm.getMinBoundary(), dm.getMaxBoundary(), dm.divisionSize, 0xFF0000, 0x0000FF, false);
    return ranges;
};

IDB.DataSet.prototype.getSingleRange = function() {
    return new IDB.Range({rangeId:0, value:0, color:IDB.rgbx(0)});
};


IDB.DataSet.prototype.fetchPivotDrilldownAxis = function(dp, sendC) { //(dp:IDatapoint, sendClickedAxisOnDrilldown:Boolean):IGraphAxis {
    var me = this, axisList = me._axisList, axis;
    if(sendC) {
        return dp.yAxis;
    }
    for(var j=0, jj=axisList.length; j<jj; j++) {
        axis = axisList[j];
        if(axis.sendOnPivotDrilldown) {
            return axis;
        }
    }
    IDB.log("DataSet.fetchPivotDrilldownAxis(): ERROR: No axis found with sendOnPivotDrilldown=true");
    return axisList[0];

};

////////////////////////////////////////////////////////////////////////////
// Code below is based on what is believed to be design-time only code in the
// Flash client. Remove it before release if it's not used.

IDB.DataSet.prototype.getOverallDivisionMetrics = function(axisId) { //(axisId:int):DivisionMetrics {
    var stats = this.getOverallStatSet(axisId), hiVal, loVal, obj, dm, X = IDB;
    if(stats === null) {
        IDB.log("DataSet.getOverallDivisionMetrics(): StatSet not found for axisId: " + axisId);
        return null;
    }
    hiVal = Math.max(0, stats.max);
    loVal = Math.min(0, stats.min);
    if(isNaN(hiVal) || isNaN(loVal) || (hiVal <= loVal)) {
        return null;
    }

    obj = X.Calculator.getDivisionMetrics(loVal, hiVal, 5, true);
    dm = obj.smallDivisions;
    if(dm.boundaries.length > 6) dm = obj.largeDivisions;
    return dm;
};


IDB.DataSet.prototype.getOverallStatSet = function(axisId) { // (axisId:int):StatSet {
    var topLevelNullsAreZeroStatSets = this.topLevelNullsAreZeroStatSets;
    if(topLevelNullsAreZeroStatSets === null || topLevelNullsAreZeroStatSets.length === 0) {
        //IDB.log("DataSet.getOverallDivisionMetrics(): topLevelNullsAreZeroStatSets is null, returning null.");
        return null;
    }
    return topLevelNullsAreZeroStatSets[axisId];
};






////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.Filter
/*global IDB */
IDB.Filter = function(axisName, value, formattedValue) {
    this.axisName = axisName;
    this.value = value ? value.toString() : "";
    this.formattedValue = formattedValue;
};

IDB.Filter.overlayFilterSets = function(baseSet, overlay) {

    // start by making a copy of the base set
    var results = baseSet.concat();
    var overlayLen = overlay.length;
    var baseLen = results.length;
    for (var overlayIndex=0; overlayIndex<overlayLen; overlayIndex++) {
        var filter = overlay[overlayIndex];
        // does a filter with this axisName already exist in the base set?
        var found = false;
        for (var baseIndex=0; baseIndex<baseLen; baseIndex++) {
            var baseFilter = results[baseIndex];

            // if we find a filter with the same name, overlay its value with the new one
            if (filter.axisName == baseFilter.axisName) {
                baseFilter.value = filter.value;
                baseFilter.formattedValue = filter.formattedValue;
                baseFilter.sourceProperties = filter.sourceProperties;
                found = true;
                break;
            }
        }
        if (!found) {
            // if one wasn't found, add it to the list.
            results.push(filter);
        }
    }

    return results;
};

IDB.Filter.prototype.toValueRequestParameters = function(prefix, increment)
{
    var requestStr = prefix + "n" + increment + "=" + encodeURIComponent(this.axisName);

    requestStr += ("&" + prefix + "v" + increment + "=" + encodeURIComponent(this.value));

    return requestStr;
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.PivotData
/*global IDB */
IDB.PivotData = function(dataRows, nullsAreZeroStatSets, nonNullValuesStatSets) {
    this.dataRows = dataRows;
    this.nullsAreZeroStatSets = nullsAreZeroStatSets;
    this.nonNullValuesStatSets = nonNullValuesStatSets;
    this.xValueLegendItems = null;
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.Range
/*global IDB */
IDB.Range = function(data) {
   data = $.extend({}, IDB.Range._defaultData, data);
   var me = this;
   me.rangeId = data.rangeId;
   me.color = IDB.rgbx(data.color, {r:0,g:0,b:0});
   me.label = data.label;
   me.val = data.val;
   me.next = null;
};


Object.defineProperties(IDB.Range.prototype, {
    nextVal:{
       get:function() {
           if(!this.next){return null;}
           return this.next.val;
       }
    }
});

IDB.Range._defaultData = {
   rangeId:-1,
   color:0,
   label:null,
   val:0
};


IDB.Range.prototype.toString = function() {
    var s = ("Range[rangeId=" + this.rangeId + ", color=" + this.color + ", val=" + this.val + ", label=" + this.label + "]"); 
    return s;
};


/**
 * Returns the number of decimal places for n, if and only if its toString() method
 * does not return a string representing the number in exponential notation.
 */
IDB.Range.decimalPlaces = function(n) { // (n:Number):int {

    n = Number(n);
    var s = n.toString().toLowerCase();
    var dotIndex = s.indexOf(".");
    if(dotIndex < 0) return 0;
    var eIndex = s.indexOf("e");
    if(eIndex > 0) return 0;
    var lastIndex = s.length - 1;
    return (lastIndex - dotIndex)
};

/**
 * Rounds to the number of decimal places indicated by "places" only if
 * "places" is between 1 and 20 inclusive. Otherwise n is returned.
 */
IDB.Range.maybeRound = function(n, places) { //(n:Number, places:int):Number {
    if(places < 1 || places > 20) return n;
    return Number(n.toFixed(places));
};

/**
 * Sets the "next" field in each Range in the Array to the Range immediately following
 * it; the "next" field in the last Range is set to null.
 */
IDB.Range.makeLinkedList = function(rangeList) { //(rangeList:Array):void {

    var last = null;
    for(var j=0, jj=rangeList.length; j<jj; j++) {
        var range = rangeList[j];
        if(last != null) last.next = range;
        last = range;
    }
    if(last) last.next = null;
};

if(!IDB.bundle) {
   var b = {};
   b["range.html5.value_and_above"] = "{0} and above";
   b["range.html5.lower_value_to_higher_value"] = "{0} - {1}";
   IDB.bundle = b;
}

IDB.Range.setAutoLabels = function(rangeList) { // (rangeList:Array):void {
    for(var j=0, jj=rangeList.length; j<jj; j++) {
        var range = rangeList[j];
        if(range.nextVal === null) {
            range.label = IDB.format("range.html5.value_and_above",  range.val);
        }
        else {
            range.label = IDB.format("range.html5.lower_value_to_higher_value", range.val, range.nextVal);
        }
    }
};


IDB.Range.setAutoColors = function(rangeList, beginColor, endColor) { //(rangeList:Array, beginColor:uint, endColor:uint, progressionType:int=0):void {
    var colors = IDB.ColorHelper.getHSVColorProgression(beginColor,  endColor,  rangeList.length, false);
    for(var j=0, jj=rangeList.length; j<jj; j++) {
        rangeList[j].color = IDB.rgbx(colors[j]);
    }
};



/**
 * @param encloseHiVal determines behavior when hiVal hiVal falls on a range boundary. If true, hiVal will be the lowerBound of the highest range,
 * and if false, hiVal will be above (by one rangeSize) the lowerBound of the highest range.
 */
IDB.Range.makeAutoRanges = function(loVal, hiVal, rangeSize, beginColor, endColor, encloseHiVal, maxRanges) { //(loVal:Number, hiVal:Number, rangeSize:Number, beginColor:uint, endColor:uint, encloseHiVal:Boolean, maxRanges:int=101):Array {
    maxRanges = maxRanges || 101;

    if(hiVal < loVal) throw new Error("loVal must be <= hiVal");
    var places = Math.max(IDB.Range.decimalPlaces(loVal), IDB.Range.decimalPlaces(rangeSize));
    var ranges = [];
    var val = loVal;
    var rangeNum = 0;
    var range;
    while((encloseHiVal ? val <= hiVal : val < hiVal) && (maxRanges < 1 || rangeNum<maxRanges)) {
        range = new IDB.Range();
        ranges.push(range);
        range.val = val;
        rangeNum++;
        val += rangeSize;
        val = IDB.Range.maybeRound(val, places);
    } 

    // if hiVal == loVal and encloseHiVal is false,
    // no ranges will have been created.
    if(ranges.length == 0) {
        range = new IDB.Range();
        ranges.push(range);
        range.val = String(loVal);
    }

    IDB.Range.makeLinkedList(ranges);
    IDB.Range.setAutoLabels(ranges);
    IDB.Range.setAutoColors(ranges, beginColor, endColor);
    return ranges;
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.MultikeyMap
/*global IDB */
IDB.MultikeyMap = function(numKeys) {
    this.numKeys = numKeys;
    this.valueMap = {};
    this.keyMap = {};
    this.levelKeys = null;
};


IDB.MultikeyMap.prototype.getLevelKeys = function(doSort) {

    if(doSort === undefined) doSort = true;
    var cached = this.levelKeys;
    if(!cached) {
        cached = [];
        /* jshint ignore:start */
        for(var key in this.keyMap) {
            var val = this.keyMap[key];
            cached.push(val);
        }
        /* jshint ignore:end */
        this.levelKeys = cached;
    }
    return this.levelKeys;
};
    
 

IDB.MultikeyMap.prototype.put = function(keys, value, startIndex) {

    if(startIndex === undefined) startIndex = 0;

    var numKeys = this.numKeys;
    var valueMap = this.valueMap;
    var keyMap = this.keyMap;
    this.levelKeys = null;

    if((keys.length-startIndex) != numKeys) {
        throw new Error("The number of keys after and including \"startIndex\" must equal the number of keys required by this MultikeyMap. keys.length=" + 
                                     keys.length + ", numKeys=" + this.numKeys + ", startIndex=" + startIndex);
    }

    var thisKey = keys[startIndex];
    if(thisKey === null || thisKey === undefined) thisKey = "";
    if(startIndex == (keys.length-1)) {
        valueMap[thisKey] = value;
        keyMap[thisKey] = thisKey;
    }
    else {
       var child = valueMap[thisKey];
       if(!child) {
           child = new IDB.MultikeyMap(numKeys-1);
           valueMap[thisKey] = child;
           keyMap[thisKey] = thisKey;
       }
       child.put(keys, value, startIndex+1);
    }
};

IDB.MultikeyMap.prototype.get = function(keys, startIndex) {
    var valueMap = this.valueMap;
    if(arguments.length == 1) startIndex = 0;
    var thisKey = keys[startIndex];
    if(thisKey === null || thisKey === undefined) thisKey = "";
    if(startIndex == (keys.length-1)) return valueMap[thisKey];
    var child = valueMap[thisKey];
    if(!child) return null;
    return child.get(keys, startIndex+1);
};

IDB.MultikeyMap.prototype.getObject = function(key) {
    if(key === undefined) key = "";
    return this.valueMap[key];
};

IDB.MultikeyMap.prototype.getRealKeyFromStringKey = function(stringKey) {
    return this.keyMap[stringKey];
};
(function(IDB) {

    var bind = IDB.listener;
    var bindings = [ 
       ["click", "_click"],
       ["mouseover", "_mouseOver"],
       ["mouseout", "_mouseOut"]
    ];

    var nextNum = 1;

    $.widget("idb.autoclose", {
        options:{
           position:null,
           fadeMillis:5000,
           parent:null,
           text:null,
           appendTo:null,
           pinned:false,
           width:300,
           height:100
        },
        _create:function() {
             var me = this, $e = me.element, o = me.options, appendTo = (o.appendTo || "body"),
                position = (o.position || me._defaultPosition());

             me.mynum = nextNum++;
             $e.addClass("idb-autoclose ui-front").appendTo($(appendTo)).height(o.height).width(o.width)
                 .draggable().resizable({handles:"se"}).on("resize", bind(me, "_onResize")).position(position);

             me.$_text = $('<div>').appendTo($e).addClass("idb-autoclose-content").html((o.text || "")
                 .replace(/\n/g, '<br/>')).on("click touchstart", bind(me, "_click"));

             me.$_close = $('<a data-enhanced="true" data-role="button" role="button" href="#">CLOSE</a>')
                 .addClass("ui-link ui-btn ui-btn-a ui-icon-delete ui-btn-icon-notext ui-shadow ui-corner-all")
                 .appendTo($e).on("click touchstart", bind(me, "_closeClicked"));

             me._layout();
             for(var j=0, jj=bindings.length; j<jj; j++) {
                 $e.on(bindings[j][0], bind(me, bindings[j][1]));
             }
             if(!o.pinned) {
                me._startFade();
             }
        },
        _setOption:function(name, value) {
           var me = this, options = me.options, $e = me.element;
           options[name] = value;
           if(name === "pinned") {
               me._setPinned(value);
           }
        },
        destroy:function() {
           var me = this, $e = me.element;
           me._super();
           $e.remove();
//           $.Widget.prototype.destroy.call( this );  // DON'T CALL THIS HERE LIKE SOME DOCS SAY - INFINITE RECURSION
        },
        _setPinned:function(pinned) {
            var me = this, o = me.options, $e = me.element;
            //IDB.log(me, "_setPinned: " + me.mynum + ", " + pinned);
            if(pinned) {
                me._stopFade();    
            }
            else {
                me._startFade();
            }
            o.pinned = !!pinned;
        },
        _togglePinned:function() {
            var me = this;
            me._setPinned(!me.options.pinned);
        },
        _defaultPosition:function() {
             return {my:"center", at:"center", of:"body"};
        },
        _startFade:function() {
             //IDB.log("_startFade..." + this.mynum + ", " + (new Error()).stack );
             var me = this, $e = me.element, o = me.options;
             $e.fadeOut(o.fadeMillis, bind(me, "destroy"));
        },
        _stopFade:function() {
            ////IDB.log("STOP FADE..........................................");
            this.element.css({"opacity":1}).stop();
        },
        _mouseOver:function(event) {
            var me = this, $e = me.element;
            me._stopFade();
        },
        _mouseOut:function(event) {
            //IDB.log(event, "_mouseOut: " + this.mynum + ", " + this.options.pinned);
            var me = this, o = me.options, $e = me.element;
            if(o.pinned) {
               return;
            }
            me._startFade();
        },
        _click:function(event) {
            //IDB.log(event, "click: " + this.mynum);
            if(event.eventPhase === Event.AT_TARGET) {
                this._setPinned(true);
            }
        },
        _closeClicked:function(evt) {
            //IDB.log(evt, "_closeClicked.");
            if(evt) {
                evt.stopImmediatePropagation();
                evt.preventDefault();
            }
            this.destroy();
        },
        _onResize:function(event) {
           this._layout();
        },
        _layout:function() {
           var me = this, $e = me.element, $closeButton = me.$_close; 
           $closeButton.position({my:"center", at:"right+3 top-3", of:$e});
        }

    });
})(IDB);
////////////////////////////////////////////////////////////////////////////
// CommandQueue
/*global IDB, $ */
IDB.CQ = {};

Object.defineProperties(IDB.CQ,
    {
        OK:{value:"ok", writable:false, enumerable:false, configurable:false},
        ERR:{value:"err", writable:false, enumerable:false, configurable:false},
        WARNING:{value:"warn", writable:false, enumerable:false, configurable:false},
        TIMEOUT:{value:"timeout", writable:false, enumerable:false, configurable:false},
        INVOKE_CALLBACK:{value:1, writable:false, enumerable:false, configurable:false},
        DISPLAY_MESSAGE:{value:2, writable:false, enumerable:false, configurable:false},
        DEFAULT_OK_FLAGS:{value:1, writable:false, enumerable:false, configurable:false},
        DEFAULT_WARN_FLAGS:{value:3, writable:false, enumerable:false, configurable:false},
        DEFAULT_ERR_FLAGS:{value:2, writable:false, enumerable:false, configurable:false},
        timedOut:{value:false, writable:true, enumerable:false, configurable:false},
        _blocking:{value:false, writable:true, enumerable:false, configurable:false}
    }
);

IDB.CQ._cmds = [];

IDB.CQ._block = function() {
    IDB.CQ._blocking = true;
    var timerId = setTimeout(function() {IDB.CQ._unblock(timerId);}, 1000);
    return timerId;
};

IDB.CQ._unblock = function(timerId) {
//   IDB.log("TIMER ID: " + timerId + "---------------------------------------");
   clearTimeout(timerId);
   IDB.CQ._blocking = false;
   IDB.CQ.deQ();
};

IDB.CQ.enQ = function(cmd, payloadPlusAdtlArgs, successCallback, failCallback, callbackContext, errFlags, warnFlags, okFlags) {
    payloadPlusAdtlArgs = payloadPlusAdtlArgs || {};
    callbackContext = callbackContext || null;
    errFlags = IDB.ifNil(errFlags, IDB.CQ.DEFAULT_ERR_FLAGS);
    warnFlags = IDB.ifNil(warnFlags, IDB.CQ.DEFAULT_WARN_FLAGS);
    okFlags = IDB.ifNil(okFlags, IDB.CQ.DEFAULT_OK_FLAGS);

    var cmdObj = new IDB.Cmd(cmd, payloadPlusAdtlArgs, successCallback, failCallback, callbackContext, errFlags, warnFlags, okFlags);
    
    this._cmds.push(cmdObj);
    
    if(1 === this._cmds.length) {
      IDB.CQ.deQ();
    }
};

IDB.CQ.deQ = function() {
    var cmds = this._cmds;
    if(0 < cmds.length && !IDB.CQ._blocking) {
       var cmd = cmds.shift();
       cmd.timerId = IDB.CQ._block();
       cmd.exec();
    }
};

IDB.CQ.handleTimeout = function(result) {
    document.location = IDB.config.contextRoot + "html5/reload/msg/" + result.messageNum;
};

////////////////////////////////////////////////////////////////////////////
// IDB.Cmd

IDB.Cmd = function(cmd, payloadPlusAdtlArgs, successCallback, failCallback, callbackContext, errFlags, warnFlags, okFlags) {
  this._cmd = cmd;
  this._payload = null;
  this._adtlArgs = null;

  if(Object.prototype.toString.call(payloadPlusAdtlArgs) === '[object Array]') {
     payloadPlusAdtlArgs = payloadPlusAdtlArgs.concat(); // don't modify passed in array.
     this._payload = payloadPlusAdtlArgs.shift();
     this._adtlArgs = payloadPlusAdtlArgs;
  }
  else {
     this._payload = payloadPlusAdtlArgs;
  }
  this._payload = this._payload || {};
  this._successCallback = successCallback;
  this._failCallback = failCallback;
  this._callbackContext = callbackContext;
  this._errFlags = errFlags;
  this._warnFlags = warnFlags;
  this._okFlags = okFlags;
};

Object.defineProperties(IDB.Cmd.prototype, {
    timerId:{value:null, writable:true, configurable:false}
});

IDB.Cmd.prototype.exec = function() {
    if(IDB.CQ.timedOut) {
       return;
    }

    var url = IDB.config.cmdPath + this._cmd;

    $.ajax({
         url:url,
         dataType:"json",
         type:"POST",
         contentType:"text/json; charset=UTF-8",
         data:JSON.stringify(this._payload),
         success:this.onComplete,
         context:this,
         err:this.onFail
         });


};

IDB.Cmd.prototype.onComplete = function(result, status, jqXHR) {

    IDB.log(result, "RETURNED FROM SERVER>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    if(IDB.CQ.timedOut) {
       return;
    }

    var conf = result.confirmation;
    var msg = result.message;
    var flags = 0;
    var title = null;
    if(conf === IDB.CQ.WARNING) {
        flags = this._warnFlags;
        // title = ...
    }
    else if(conf === IDB.CQ.OK) {
        flags = this._okFlags;
        // title = ...
    }
    else if(conf === IDB.CQ.ERR) {
        flags = this._errFlags;
        // title = ...
    }
    else if(conf === IDB.CQ.TIMEOUT) {
       IDB.CQ.handleTimeout(result);
       return;
    }
    else {
        // handle invalid conf
        IDB.MM.show("Invalid confirmation code returned from server: " + conf, "Server Error", this._posOptions());
    }

    // Let the next server request proceed while this response is
    // being processed in the callback.
    IDB.CQ._unblock(this.timerId);

    try {

        IDB.log("FLAGS = " + flags);
    
        if(msg && this._shouldMsg(flags)) {
            IDB.MM.show(result.message, title, this._posOptions());
        }
        if(this._shouldCall(flags)) {
            this._doCallback(result, status, jqXHR);
        }
    } catch(err) {
        IDB.log(err.stack, "IDB.Cmd.onComplete(): ERROR");

        IDB.MM.show(err.name + ": " + err.message, "ERROR", this._posOptions() );
    }

    
};

/**
 * If the callback context has been provided, this function checks for a method on it named "getMsgPos". If it
 * exists, it calls it with the cmd name as the parameter. The return value is assumed to be a jQuery position object,
 * which is used to locate a modal message dialog that is displayed as a result of the command.
 */
IDB.Cmd.prototype._posOptions = function() {
    var me = this, context = me._callbackContext, pos;
    if(context && context.getMsgPos) {
        try {
            pos = context.getMsgPos(this._cmd);
            if(pos) {
                return {position:pos};
            }
        } catch(err) {
            IDB.log(err, "Error calling getMsgPos():\n" + err.stack);
        }
    }
    return null;
};

IDB.Cmd.prototype.onFail = function(jqXHR, status, error) {
 
     var me = this, _failCallback = me._failCallback, I = IDB;
     if(_failCallback !== null) {
        _failCallback.call(me._callbackContext, jqXHR, status, error);
     }
     else {
        I.MM.show("Server Request Failed: " + status + "\n\n" + error, null, this._posOptions());
     }
};

IDB.Cmd.prototype._doCallback = function(result, status, jqXHR) {
    var cb = this._successCallback;
    if(!cb) {
       return;
    }

    var args = this._adtlArgs;
    
    if(!args) {
       args = [result, status, jqXHR];
    }
    else {
       args.unshift(result);
       args.push(status, jqXHR);
    }
    
    cb.apply(this._callbackContext, args);

};

IDB.Cmd.prototype._shouldMsg = function(flags) {
    return ((flags & IDB.CQ.DISPLAY_MESSAGE) !== 0);
};

IDB.Cmd.prototype._shouldCall = function(flags) {
    return ((flags & IDB.CQ.INVOKE_CALLBACK) !== 0);
};







/*global IDB, jQuery */
(function(IDB, $) {

    /**
     * An instance of this class is instantiated for a DOM element for which touch events (excluding multi-touch events)
     * should be translated to mouse events.
     * It listens for touch events and programattically dispatches corresponding mouse
     * events, so that the event handler code needs only to listen for mouse events. If a user holds a finger
     * to the screen for longer than 500 milliseconds before lifting it, then no mouse click event is dispatched;
     * instead it's treated like hovering the mouse. If the touch is lifted within 500 milliseconds, and without
     * having been dragged, then a mouse click event is dispatched.
     */

    ////////////////////////////////////////////////////////////////////////
    // Begin closure data
    var TOUCH_SUPPORTED = !!('ontouchend' in document), 
        CLICK_WINDOW = 500;

    IDB.log("TOUCH_SUPPORTED: " + TOUCH_SUPPORTED);


    // The property names are touch event types, and the property values
    // are arrays of the mouse event types that get dispatch when
    // the corresponding touch events occur.
    var defaultMappings = {
       "touchstart":["mouseover", "mousedown"],
       "touchmove":["mousemove"],
       "touchend":["mouseup", "mouseout"]
    };

    var primaryListeners = [
       ["touchstart", "_handleTouchstart"],
       ["touchmove", "_handleTouchmove"],
       ["touchend", "_handleTouchend"]
    ];

    /**
     * Given a touch event, this creates a mouse event the type indicated by simulatedType that has the same X and Y location
     * coordinates as where the touch event occurred.
     */
    function createMouseEvent(event, simulatedType) {

      var touch = event.originalEvent.changedTouches[0],
          simulatedEvent = document.createEvent('MouseEvents');
      
      // Initialize the simulated mouse event using the touch event's coordinates
      simulatedEvent.initMouseEvent(
        simulatedType,    // type
        true,             // bubbles                    
        true,             // cancelable                 
        window,           // view                       
        1,                // detail                     
        touch.screenX,    // screenX                    
        touch.screenY,    // screenY                    
        touch.clientX,    // clientX                    
        touch.clientY,    // clientY                    
        false,            // ctrlKey                    
        false,            // altKey                     
        false,            // shiftKey                   
        false,            // metaKey                    
        0,                // button                     
        null              // relatedTarget              
      );

//      simulatedEvent["0-translatedEvent"] = event;
      return simulatedEvent;
    }

    var $footer = null;

    function footerMsg(e) {
        IDB.log(e, "---------------------------------------------------");
        if(!$footer) {
           $footer = $("#db-footer h4");
           if($footer.length === 0) {
               $footer = null;
               return;
           }
        }
        var msg = "ERROR";
        try {
            var t = e.changedTouches[0];
            msg = e.type + ": (" + t.pageX + ", " + t.pageY + "), (" + t.clientX + ", " + t.clientY + ")";

        } catch(err) {
            msg = "" + err;
        }
        $footer.text(msg);
    }

    // End closure data
    ////////////////////////////////////////////////////////////////////////


    IDB.DOMTouchHandler = function(domObj, pMappings) {
        var me = this, $domObj = me._$domObj = $(domObj);
        this._domObj = domObj;
        this._inClickWindow = false;
        if(!TOUCH_SUPPORTED) {
            return;
        }
        this._mappings = $.extend({}, defaultMappings, pMappings);
        this._pendingMouseOut = null;
        for(var j=0, jj=primaryListeners.length; j<jj; j++) {
            $domObj.on(primaryListeners[j][0], IDB.listener(this, primaryListeners[j][1]));
        }

        this._windowListener = IDB.listener(this, "_onWindowTouchEnd"); 

        this._preventDefaultOn = {
            touchstart:true,
            touchmove:true,
            touchend:true
        };
        Object.preventExtensions(this._preventDefaultOn);
    };

    IDB.DOMTouchHandler.prototype._simulateEvents = function(touchEvt) {
       var mouseEvents = this._mappings[touchEvt.type];
       for(var j=0, jj=mouseEvents.length; j<jj; j++) {
           this._simulateMouseEvent(touchEvt, mouseEvents[j]);
       }
    };

    /**
     * Given a touch event, this function will create and dispatch a mouse event
     * with the type simulatedType. preventDefault() will be called on the touch event.
     */
    IDB.DOMTouchHandler.prototype._simulateMouseEvent = function(jqEvent, simulatedType) {

      var event = jqEvent.originalEvent;
      if(!event.touches) {
          IDB.log(event, "Not a touch event: ");
          return;
      } // not a touch event
    
      try {
          // Ignore multi-touch events
          if (event.touches.length > 1) {
            return;
          }
      } 
      catch(err) {
          IDB.log(err, "ERROR");
          return;
      }

      if(this._preventDefaultOn[event.type]) {
          event.preventDefault();
      }
    
      var simulatedEvent = createMouseEvent(jqEvent, simulatedType);

      //IDB.log(simulatedEvent, "Simulated Event - " + simulatedType);
    
      // Dispatch the simulated event to the target element
      event.target.dispatchEvent(simulatedEvent);
    }


    IDB.DOMTouchHandler.prototype.startWindowListening = function() {
        $(window).on("touchend", this._windowListener);
    };

    IDB.DOMTouchHandler.prototype.stopWindowListening = function() {
        this._pendingMouseOut = null;
        $(window).off("touchend", this._windowListener);
    };

    IDB.DOMTouchHandler.prototype._openClickWindow = function() {
        clearTimeout(this._timerId);
        this._inClickWindow = true;
        this._timerId = setTimeout(IDB.listener(this, "_closeClickWindow"), CLICK_WINDOW);
    };


    IDB.DOMTouchHandler.prototype._closeClickWindow = function() {
        this._inClickWindow = false;
        clearTimeout(this._timerId);
    };

    IDB.DOMTouchHandler.prototype._handleTouchstart = function(e) {
//        footerMsg(e);
        this._pendingMouseOut = createMouseEvent(e, "mouseout");
        this._pendingMouseOut._target = e.target;
        this.startWindowListening(); 
        this._simulateEvents(e);
        this._openClickWindow();
    };

    IDB.DOMTouchHandler.prototype._handleTouchmove = function(e) {
        if(this._inClickWindow && this._pendingMouseOut) {
            // This touchmove might have been generated by the mousemove event we dispatched
            // from the touchstart handler, in which case the mouse will not have actually 
            // moved since the touch began. We'll compare the coordinates of this event
            // with the ones stored in the _pendingMouseOut event.
            var mo = this._pendingMouseOut, 
                t = (e.changedTouches && e.changedTouches[0]);
            if(t && (t.pageX !== mo.pageX || t.pageY !== mo.pageY)) {
                //IDB.log("Movement detected.......");
                this._closeClickWindow();
            }
        }
        this._simulateEvents(e);
    };

    IDB.DOMTouchHandler.prototype._handleTouchend = function(e) {
        this._simulateEvents(e);
        if(this._inClickWindow) {
            this._simulateMouseEvent(e, "click");
        }
        this._closeClickWindow();
        this.stopWindowListening();
    };

    IDB.DOMTouchHandler.prototype._onWindowTouchEnd = function(e) {
        var mo = this._pendingMouseOut;
        this._closeClickWindow();
        this.stopWindowListening();
        if(mo) {
            var target = mo._target;
            mo._target = null;
            target.dispatchEvent(mo);
        }
        this._pendingMouseOut = null;
    };

})(IDB, jQuery);
////////////////////////////////////////////////////////////////////////////
// MM.js (MsgMgr)
IDB.MM = {}

IDB.MM.show = function(msg, title, options) {
    $('<div class="idb-dialog-content idb-msg-dialog"></div>').text(msg).dialog($.extend({title:title},IDB.MM.show.defaultOptions, options)); 
};

IDB.MM.maybeShowSuppressible = function(name, title, msg, closeCallback, options) {
    $('<div>').suppressibleMessage($.extend({}, {name:name, title:title, text:msg, closeCallback:closeCallback}, options));
};

IDB.MM.isSuppressed = function(name) {
    return localStorage.getItem(name) == "true";
};

IDB.MM.autoclose = function(msg, options) {
    return $('<div>').autoclose($.extend({}, {text:msg}, options));
};

IDB.MM.show.defaultOptions = {
    buttons:{
        "OK":function() {
            $(this).dialog("close");
        }
    },
    close:function(event, ui) {
        $(this).remove();
    },
    resizable:true,
    modal:true,
    dialogClass:"jqui-dialog"
};

IDB.MM.closeAllDialogs = function() {
    $(".idb-msg-dialog").dialog("close");
};
(function(IDB) {
    var checked
    $.widget("idb.suppressibleMessage", $.ui.dialog, {
        options:{
            name:"suppressDialog",
            modal:true,
            dialogClass:"jqui-dialog",
            text:"",
            title:"",
            width:375,
            closeCallback:null
        },
        _create:function() {
             var me = this, $e = me.element, o=me.options;
             me._super();
             $e.addClass('idb-dialog-content idb-msg-dialog');
             $e.append("<p style='padding-bottom:10px;'>"+o.text+"</p>");
             $e.append("<div><input type='checkbox'>" + 
                         IDB.format('param.html5.dont_show_again') + "</div>")
               .find(':checkbox')
               .on('click', function(evt) {
                                if(evt.target.checked) {
                                    me.checked = true;
                                    localStorage.setItem(o.name, "true");
                                    me.close(); 
                                }
                            });
             this.widget().closest('.ui-front').css({'z-index':300});

        },
        close:function() {
                if(this.options.closeCallback != null) {
                    this.options.closeCallback(!!this.checked);
                }
                this._super();
                $(this).remove();
        }
    });
})(IDB);
/*global IDB, Kinetic, jQuery */
(function(IDB, $) {

    /**
     * An instance of this class is instantiated for each Kinetic visual object that represents
     * a data point. It listens for touch events and programattically dispatches corresponding mouse
     * events, so that the event handler code needs only to listen for mouse events. If a user holds a finger
     * to the screen for longer than 500 milliseconds before lifting it, then no mouse click event is dispatched;
     * instead it's treated like hovering the mouse. If the touch is lifted within 500 milliseconds, and without
     * having been dragged, then a mouse click event is dispatched.
     */

    ////////////////////////////////////////////////////////////////////////
    // Begin closure data
    var TOUCH_SUPPORTED = !!('ontouchend' in document), isK501 = (Kinetic.version === "5.0.1"),
        CLICK_WINDOW = 500;

    if(!isK501) {
        Kinetic.UA.mobile = false; // experimental
    }

    IDB.log("TOUCH_SUPPORTED: " + TOUCH_SUPPORTED + ", isK501: " + isK501);

//    var Kinetic_inDblClickWindow = false;
//    Object.defineProperties(Kinetic,  {
//        inDblClickWindow:{
//           get:function() {
//               return !TOUCH_SUPPORTED && Kinetic_inDblClickWindow;
//           },
//           set:function(b) {
//              Kinetic_inDblClickWindow = b;
//           }
//        }
//    });

    // The property names are touch event types, and the property values
    // are arrays of the mouse event types that get dispatch when
    // the corresponding touch events occur.
    var defaultMappings = {
       "touchstart":["mouseover", "mousedown", "mousemove"],
       "touchmove":["mousemove"],
       "touchend":["mouseup", "mouseout"]
    };

    var useKineticFire = {
       // For some reason, if we use dispatchEvent() to dispatch a click event, it never reaches the 
       // event handler. jQuery intercepts the event during the capture phase, and somewhere it gets lost.
       // The _kineticObject.fire method will invoke the click handler, but if the bubble argument is true, it does
       // not "bubble" beyond any ancestors that are not themselves Kinetic objects.

       "mouseup":true,
       "click":true
    };

    var primaryListeners = [
       ["touchstart", "_handleTouchstart"],
       ["touchmove", "_handleTouchmove"],
       ["touchend", "_handleTouchend"]
    ];

    /**
     * Given a touch event, this creates a mouse event the type indicated by simulatedType that has the same X and Y location
     * coordinates as where the touch event occurred.
     */
    function createMouseEvent(event, simulatedType) {

      var touch = (isK501 ? event : event.evt).changedTouches[0],
          simulatedEvent = document.createEvent('MouseEvents');
      
      // Initialize the simulated mouse event using the touch event's coordinates
      simulatedEvent.initMouseEvent(
        simulatedType,    // type
        true,             // bubbles                    
        true,             // cancelable                 
        window,           // view                       
        1,                // detail                     
        touch.screenX,    // screenX                    
        touch.screenY,    // screenY                    
        touch.clientX,    // clientX                    
        touch.clientY,    // clientY                    
        false,            // ctrlKey                    
        false,            // altKey                     
        false,            // shiftKey                   
        false,            // metaKey                    
        0,                // button                     
        null              // relatedTarget              
      );

//      simulatedEvent["0-translatedEvent"] = event;
      return simulatedEvent;
    }

    var $footer = null;

    function footerMsg(e) {
        IDB.log(e, "---------------------------------------------------");
        if(!$footer) {
           $footer = $("#db-footer h4");
           if($footer.length === 0) {
               $footer = null;
               return;
           }
        }
        var msg = "ERROR";
        try {
            var t = e.changedTouches[0];
            msg = e.type + ": (" + t.pageX + ", " + t.pageY + "), (" + t.clientX + ", " + t.clientY + ")";

        } catch(err) {
            msg = "" + err;
        }
        $footer.text(msg);
    }

    // End closure data
    ////////////////////////////////////////////////////////////////////////


    IDB.TouchHandler = function(kineticObj, pMappings) {
        this._kineticObj = kineticObj;
        this._inClickWindow = false;
        if(!TOUCH_SUPPORTED) {
            return;
        }
        this._mappings = $.extend({}, defaultMappings, pMappings);
        this._pendingMouseOut = null;
        for(var j=0, jj=primaryListeners.length; j<jj; j++) {
            kineticObj.on(primaryListeners[j][0], IDB.listener(this, primaryListeners[j][1]));
        }

        this._windowListener = IDB.listener(this, "_onWindowTouchEnd"); 
    };

    IDB.TouchHandler.prototype._simulateEvents = function(touchEvt) {
       var mouseEvents = this._mappings[touchEvt.type];
       for(var j=0, jj=mouseEvents.length; j<jj; j++) {
           this._simulateMouseEvent(touchEvt, mouseEvents[j]);
       }
    };

    /**
     * Given a touch event, this function will create and dispatch a mouse event
     * with the type simulatedType. preventDefault() will be called on the touch event.
     */
    IDB.TouchHandler.prototype._simulateMouseEvent = function(kineticEvent, simulatedType) {

      var event = isK501 ? kineticEvent : kineticEvent.evt;
      if(!event.touches) {
          IDB.log(event, "Not a touch event: ");
          return;
      } // not a touch event
    
      try {
          // Ignore multi-touch events
          if (event.touches.length > 1) {
            return;
          }
      } 
      catch(err) {
          IDB.log(err, "ERROR");
          return;
      }

      event.preventDefault();
    
      var simulatedEvent = createMouseEvent(kineticEvent, simulatedType);

      //IDB.log(simulatedEvent, "Simulated Event - " + simulatedType);
    
      // Dispatch the simulated event to the target element
      //event.target.dispatchEvent(simulatedEvent);
      if(useKineticFire[simulatedType]) {
          this._kineticObj.fire(simulatedType, simulatedEvent, true);
      }
      else {
          event.target.dispatchEvent(simulatedEvent);
      }
    }


    IDB.TouchHandler.prototype.startWindowListening = function() {
        $(window).on("touchend", this._windowListener);
    };

    IDB.TouchHandler.prototype.stopWindowListening = function() {
        this._pendingMouseOut = null;
        $(window).off("touchend", this._windowListener);
    };

    IDB.TouchHandler.prototype._openClickWindow = function() {
        clearTimeout(this._timerId);
        this._inClickWindow = true;
        this._timerId = setTimeout(IDB.listener(this, "_closeClickWindow"), CLICK_WINDOW);
    };


    IDB.TouchHandler.prototype._closeClickWindow = function() {
        this._inClickWindow = false;
        clearTimeout(this._timerId);
    };

    IDB.TouchHandler.prototype._handleTouchstart = function(e) {
//        footerMsg(e);
        this._pendingMouseOut = createMouseEvent(e, "mouseout");
        this._pendingMouseOut._target = e.target;
        this.startWindowListening(); 
        this._simulateEvents(e);
        this._openClickWindow();
    };

    IDB.TouchHandler.prototype._handleTouchmove = function(e) {
        if(this._inClickWindow && this._pendingMouseOut) {
            // This touchmove might have been generated by the mousemove event we dispatched
            // from the touchstart handler, in which case the mouse will not have actually 
            // moved since the touch began. We'll compare the coordinates of this event
            // with the ones stored in the _pendingMouseOut event.
            var mo = this._pendingMouseOut, 
                t = (e.changedTouches && e.changedTouches[0]);
            if(t && (t.pageX !== mo.pageX || t.pageY !== mo.pageY)) {
                //IDB.log("Movement detected.......");
                this._closeClickWindow();
            }
        }
        var stage = this._kineticObj;
        while (!(stage instanceof Kinetic.Stage))
        {
            stage = stage.parent;
        }
        stage._setPointerPosition(e);
        var shape = stage.getIntersection(stage.getPointerPosition());
        if(shape && shape.isListening()) {
            this._pendingMouseOut = createMouseEvent(e, "mouseout");
            this._pendingMouseOut._target = e.target;
            this.startWindowListening();
        }
        this._simulateEvents(e);
    };

    IDB.TouchHandler.prototype._handleTouchend = function(e) {
        this._simulateEvents(e);
        if(this._inClickWindow) {
            this._simulateMouseEvent(e, "click");
        }
        this._closeClickWindow();
        this.stopWindowListening();
    };

    IDB.TouchHandler.prototype._onWindowTouchEnd = function(e) {
        var mo = this._pendingMouseOut;
        this._closeClickWindow();
        this.stopWindowListening();
        if(mo) {
            var target = mo._target;
            mo._target = null;
            this._kineticObj.fire("mouseout", mo, true);
            //target.dispatchEvent(mo);
        }
        this._pendingMouseOut = null;
    };

})(IDB, jQuery);
  (function($, IDB) {
   $.widget("idb.combobox", {
      options: {
         source:null, // an array of objects with fields label and value.
         editable:false,
         prompt:IDB.format("param.html5.select_prompt"),
         equals:null
      },

      _create: function() {
        this.input = this.element;
        this.wrapper = this.element.wrap("<span class='idb-combobox'></span>").parent();
        this._createAutocomplete();
        this._createShowAllButton();
        this._changed = false;
      },
 
      _createAutocomplete: function() {
          var acOptions = $.extend({delay:0, minLength:0}, this.options);
          this.source = acOptions.source.concat();
          if(!acOptions.editable) {
              this.source.unshift({label:this.options.prompt, value:"SELECT_PROMPT"});
              acOptions.source = this.source;
          }
 
          this.input.addClass("idb-combobox-input ui-widget-content").autocomplete(acOptions);

          var widget = this.input.autocomplete("widget");
          widget.addClass('idb-combobox-autocomplete');

          this._on(this.input, {
              autocompletechange: IDB.listener(this, "_itemChanged"),
              autocompleteselect: IDB.listener(this, "_itemSelected"),
              autocompleteopen: IDB.listener(this, "_openSelection"),
              autocompleteclose: IDB.listener(this, "_closeSelection")
            });

         if(!acOptions.editable) {
             this.value("SELECT_PROMPT");
         }
      },
 
      _createShowAllButton: function() {
        var input = this.input,
          wasOpen = false;
 
        $("<a>")
          .attr("tabIndex", -1)
          .appendTo(this.wrapper)
          .button({
            icons: {
              primary: "ui-icon-triangle-1-s"
            },
            text: false
          })
          .removeClass("ui-corner-all")
          .addClass("idb-combobox-toggle")
          .mousedown(function() {
            wasOpen = input.autocomplete("widget").is(":visible");
          })
          .click(function() {
            input.focus();
 
            // Close if already visible
            if (wasOpen) {
              return;
            }
 
            // Pass empty string as value to search for, displaying all results
            input.autocomplete("search", "");
          });
      },

      _itemSelected: function(event, ui) {
         event.preventDefault();
         this.selected = ui.item; 
         this._changed = true;
         this.input.val(ui.item.label);
      },
 
      _openSelection: function(event, ui) {
          //
          // Blank entries don't size correctly, so we will manually set the size.
          //
          var $widget = this.input.autocomplete("widget");
          $widget.find('.ui-menu-item a').each(function() {
                                                 if(!$(this).text().length) {
                                                      $(this).css({height:"12px"});
                                                  }
                                               });
          //
          // Listen for mouse clicks while the menu is open.
          //
          this._on(this.window, {
                         mousedown:"_maybeCloseAutocomplete",
                         resize:"_closeAutocomplete"});
      },

      _closeSelection: function(event, ui) {
          this._off(this.window, "mousedown resize");
          if(this._changed) {
             this._trigger("change");
             this._changed = false;
          }
      },

      _maybeCloseAutocomplete: function(event) {
          var $target = $(event.target);
          if(!($target.is(".ui-autocomplete")) && ($target.parents(".ui-autocomplete").length === 0)) {
              this._closeAutocomplete(event);
          }
      },

      _closeAutocomplete: function(event) {
         this.input.autocomplete("close");
      },
      
      _itemChanged: function(event, ui) {
          if(ui.item) {
              return;
          }
          this._changed = true;
          if(this.options.editable) {
              this.selected = null;
          }
          else {
              _removeIfInvalid(event, ui);
          }
      },

      _removeIfInvalid: function(event, ui) {
        // Selected an item, nothing to do
        if (ui.item || this.options.editable) {
          return;
        }

        this.selected = null;
 
        // Search for a match (case-insensitive)
        var value = this.input.val(),
          valueLowerCase = value.toLowerCase(),
          valid = false;
        this.source.forEach(function(item) {
          if (item.label.toLowerCase() === valueLowerCase) {
            return false;
          }
        });
 
        // Found a match, nothing to do
        if (valid) {
          return;
        }
 
        // Remove invalid value
        this.input
          .val("")
          .attr("title", value + " didn't match any item")
          .tooltip("open");
        this.element.val("");
        this._delay(function() {
          this.input.tooltip("close").attr("title", "");
        }, 2500);
        this.input.data("ui-autocomplete").term = "";
      },
 
      _destroy: function() {
        this.wrapper.remove();
        this.element.show();
      },

      value: function(value) {
          //
          // No value passed, act as getter.
          //
          if(value === undefined) {
              if(this.selected) {
                  return this.selected.value;
              }
              else {
                  return this.input.val();
              }
          }
          else {
              //
              // Value passed, act as setter.
              //
              var equals = this.options.equals;
              var items = this.source.filter(function(item) {return equals?equals(item.value, value):(item.value == value);});
              this.selected = (items.length > 0)?items[0]:null;
              if(this.selected) {
                 this.input.val(this.selected.label);
              }
              else {
                  if(this.options.editable) {
                      this.input.val(value);
                  } 
              }
          }
      }
    });
  })(jQuery,IDB);
IDB.DashboardSlideshowControls = function($parent, dashboardInfoList, sticky, showCountdown, defaultRefreshSeconds, previousCallback, nextCallback) {
    this.$parent = $(parent);
    this.dashboardInfoList = dashboardInfoList || [];
    this.panelVisible = this.sticky = !!sticky;
    this.showCountdown = !!showCountdown;
    this.defaultRefreshSeconds = defaultRefreshSeconds || 60;
    this.previousCallback = previousCallback;
    this.nextCallback = nextCallback;
    this.running = true;
    this.currentDashboardIndex = 0;
    this.secondsRemaining = this.dashboardInfoList[this.currentDashboardIndex].refreshSeconds || this.defaultRefreshSeconds; // put in list
    this.$controls = this.$div = $("<div id='dss-controls'/>").appendTo($parent);
    this.$div.css({"visibility":this.panelVisible?"visible":"hidden",
                  "display":this.panelVisible?"inline":"none"}); 
    var $form = $("<form/>").appendTo(this.$div);
    this.$toggleButton = $("<input type='button' id='dss-toggle-button' value='||'>").on("click", IDB.listener(this, "toggleSlideshow")).appendTo($form);
    this.$counter = $("<span id='dss-counter'>").appendTo($form);
    this.$counter.css({"display":(this.showCountdown?"inline":"none")});
    $("<input type='button' id='dss-prev-button' value='<<'>").on("click", IDB.listener(this, "prevDB")).appendTo($form);
    $("<input type='button' id='dss-next-button' value='>>'>").on("click", IDB.listener(this, "nextDB")).appendTo($form);

     if(!this.sticky) {
         document.onmousemove = IDB.listener(this,"maybeShowControls");
         this.$controls.on("mouseout", IDB.listener(this, "onDivMouseOut"));
     }
}; 

IDB.DashboardSlideshowControls.prototype.start = function() {
    this.stop();
    this.$counter.html(this.secondsToHMS(this.secondsRemaining));
    this.timer = window.setInterval(IDB.listener(this, "tick"), 1000);
};

IDB.DashboardSlideshowControls.prototype.stop = function() {
    if(this.timer) {
        window.clearInterval(this.timer);
    }
}

IDB.DashboardSlideshowControls.prototype.tick = function() {
   var me = this, running = this.running, panelVisible = me.panelVisible;
   if(!running) return;
   me.secondsRemaining--;
   if(me.secondsRemaining <= 0) {
      me.nextDB();
   }
   else {
      if(panelVisible) {
          me.$counter.html(me.secondsToHMS(me.secondsRemaining));
      }
   }
};

IDB.DashboardSlideshowControls.prototype.nextDB = function() {
    var me = this, currIndex = me.currentDashboardIndex;
    if(currIndex < me.dashboardInfoList.length-1) {
        currIndex = ++me.currentDashboardIndex;
    }
    else {
        currIndex = me.currentDashboardIndex = 0;
    }
    var currDashboard = me.dashboardInfoList[currIndex];
    me.secondsRemaining = currDashboard.refreshSeconds || me.defaultRefreshSeconds;
    if(me.nextCallback) {
        me.nextCallback(currDashboard.dashId);
    }
};

IDB.DashboardSlideshowControls.prototype.prevDB = function() {
    var me = this, currIndex = me.currentDashboardIndex;
    if(currIndex > 0) {
        currIndex = --me.currentDashboardIndex; 
    }
    else {
        currIndex = me.currentDashboardIndex = me.dashboardInfoList.length-1;
    }
    var currDashboard = me.dashboardInfoList[currIndex];
    me.secondsRemaining = currDashboard.refreshSeconds || me.defaultRefreshSeconds;
    if(me.previousCallback) {
        me.previousCallback(currDashboard.dashId);
    }
};




IDB.DashboardSlideshowControls.prototype.toggleSlideshow = function() {
    var me = this, $button = this.$toggleButton;
    if(me.running) {
        me.running = false;
        $button.val(">"); // better graphics
    }
    else {
        me.running = true;
        $button.val("||");
    }
};

IDB.DashboardSlideshowControls.prototype.hideControls = function() {
    var me = this, $controls = this.$controls;
    $controls.css({"visibility":"hidden",
                       "top":"-1000px",
                       "left":"-1000px",
                       "display":"none"});
    me.panelVisible = false;
    document.onmousemove = IDB.listener(this, "maybeShowControls");
};

IDB.DashboardSlideshowControls.prototype.showControls = function() {
    var me = this, $counter = this.$counter, $controls = this.$controls;
    $counter.html(me.secondsToHMS(me.secondsRemaining));
    $controls.css({"top":0,
                   "left":0,
                   "visibility":"visible",
                   "display":"inline"
                  });
    me.panelVisible = true;
    document.onmousemove = null;
};

IDB.DashboardSlideshowControls.prototype.maybeShowControls = function(e) {
    var ns6=document.getElementById && !document.all;
    var curX=(ns6)?e.pageX : event.clientX;
    var curY=(ns6)?e.pageY : event.clientY;
    if(curX <= 20 && curY <= 20) {
        this.showControls();
    }
    else {
        this.hideControls();
    }
};

IDB.DashboardSlideshowControls.prototype.onDivMouseOut = function(e) {
    if(document.all) {
        if(event.srcElement == $controls.get(0)) this.hideControls();
    }
    else {
        if($(e.relatedTarget).parents('#dss-controls').length == 0) this.hideControls();
    }
};

IDB.DashboardSlideshowControls.prototype.secondsToHMS = function(seconds) {
    if(seconds < 60) return this.pad(seconds);
    var mins = Math.floor(seconds/60);
    var hours = Math.floor(mins/60);
    if(hours) mins = mins % 60;
    var secs = seconds % 60;
    if(hours) return hours + ":" + this.pad(mins) + ":" + this.pad(secs);
    return this.pad(mins) + ":" + this.pad(secs);
};

IDB.DashboardSlideshowControls.prototype.pad = function(num) {
    return num > 9 ? num : "0" + num;
}

IDB.DashboardSlideshowControls.prototype.onLoad = function() {
    var me = this, $controls = this.$controls, $counter = this,$counter;
    if(!this.sticky) {
        document.onmousemove = this.maybeShowControls;
        $controls.on("mouseout", IDB.listener(this, "onDivMouseOut"));
    }
    $counter.html(me.secondsToHMS(this.secondsRemaining));
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ImagePanel
/*global $, IDB */

/**
 * @param $div a $div which is or will be parented by the Dashframe div.
 */
IDB.ImagePanel = function($div, data, dashboard) {
   this.$div = $div;
   // The ImagePanel div will always fill out the frame, and have no scrollbars.
   $div.css({width:"100%", height:"100%", overflow:"hidden"});
   this.data = data;
   this.dashboard = dashboard;
   this.loadImage();
};

Object.defineProperties(IDB.ImagePanel.prototype, {
    isAttached:{
        get:function() {return(this.$div.parent().length > 0);}
    }
});

IDB.ImagePanel.prototype.resize = function() {
    if(this.jImg) {this.sizeAndPositionImage();}
};

IDB.ImagePanel.prototype.sizeAndPositionImage = function() {
    IDB.log("sizeAndPositionImage(): " + this.$div.width() + ", " + this.$div.height());
    if(!this.isAttached) return;
    if(this.data.imageSettings.sizing === "center") {
        this.jImg.css("max-width", this.$div.width());
        this.jImg.css("max-height", this.$div.height());
    }
    else {
        var dimensions = this.getImageSize();
 
        this.jImg.width(dimensions.width);   
        this.jImg.height(dimensions.height);   
     }

     var left = -(this.jImg.width() - this.$div.width())/2;
     var top = -(this.jImg.height() - this.$div.height())/2;
     IDB.log("left=" + left + ", top=" + top);
     this.jImg.css({
         'position':'relative',
         'left':(left+'px'),
         'top':(top+'px')
     });

};
IDB.ImagePanel.prototype.loadImage = function() {
   IDB.log("loadImage " + this.data.imageUrl);

    var $div = this.$div;
    var imageSettings = this.data.imageSettings;
    var url = IDB.resolveUrl( this.data.imageUrl, imageSettings.useProxy);
    this.jImg = $("<img class='frame-content'>").attr("src", url).appendTo($div);
//    this.$parent.parent().css("overflow", "hidden");
//    this.$parent.css("overflow", "hidden");

    this.jImg.on("load", IDB.listener(this, "sizeAndPositionImage"));

     // Set the background color.
     $div.css({'background-color': imageSettings.bgRGBA});

     // Set the tooltip.
     var toolTip = $.trim(imageSettings.toolTip);
     if(toolTip) {this.jImg.attr("title", toolTip);}
     // Set the link.
     var linkUrl = this.data.linkUrl;
     if(linkUrl) {
         linkUrl = IDB.normalizeUrl(linkUrl);
         var linkTarget = $.trim(this.data.linkTarget);
         if(!linkTarget) {
             linkTarget = "_blank";
         }
         var urlSettings = this.data.linkUrlSettings;
         this.jImg.click(function() {
             window.open(linkUrl, linkTarget, urlSettings); 
         });

         this.jImg.hover(function() {$(this).css('cursor','pointer');},
                  function() {$(this).css('cursor','auto');});
     }
};

IDB.ImagePanel.prototype.getImageSize = function() {

    var imgHeight = this.jImg.height();
    var imgWidth = this.jImg.width();
    var containerHeight = this.$div.height();
    var containerWidth = this.$div.width();

    var newWidth = containerWidth;
    var newHeight = containerHeight;
    var preserveAspectRatio = this.data.imageSettings.aspect;

    if(preserveAspectRatio) { 
        var wScale = containerWidth/imgWidth;
        var hScale = containerHeight/imgHeight;

        var scale = (wScale > hScale)?hScale:wScale;
        
        newWidth = scale*imgWidth;
        newHeight = scale*imgHeight;
    }

    return {width:newWidth, height:newHeight};
};


IDB.ImagePanel.prototype.toString = function() {
   return ("ImagePanel[frameId=" + this.data.frameId + ", imageUrl=" + this.data.imageUrl + "]");
};
/* jshint forin:true, noarg:true, noempty:true, eqeqeq:true, boss:true, undef:true, curly:true, browser:true, jquery:true */
/*
 * jQuery MultiSelect UI Widget 1.14pre
 * Copyright (c) 2012 Eric Hynds
 *
 * http://www.erichynds.com/jquery/jquery-ui-multiselect-widget/
 *
 * Depends:
 *   - jQuery 1.4.2+
 *   - jQuery UI 1.8 widget factory
 *
 * Optional:
 *   - jQuery UI effects
 *   - jQuery UI position utility
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 */
(function($) {

  var multiselectID = 0;
  var $doc = $(document);

  $.widget("ech.multiselect", {

    // default options
    options: {
      header: true,
      height: 175,
      minWidth: 225,
      classes: '',
      checkAllText: 'Check all',
      uncheckAllText: 'Uncheck all',
      noneSelectedText: 'Select...',
      selectedText: '# selected',
      selectedList: 0,
      show: null,
      hide: null,
      autoOpen: false,
      multiple: true,
      position: {},
      appendTo: "body",
      showClose: false
    },

    _create: function() {
      var el = this.element.hide();
      var o = this.options;

      this.speed = $.fx.speeds._default; // default speed for effects
      this._isOpen = false; // assume no
      this._changed = false;

      // create a unique namespace for events that the widget
      // factory cannot unbind automatically. Use eventNamespace if on
      // jQuery UI 1.9+, and otherwise fallback to a custom string.
      this._namespaceID = this.eventNamespace || ('multiselect' + multiselectID);

      var button = (this.button = $('<button type="button"><span class="ui-icon ui-icon-triangle-1-s"></span></button>'))
      var button = (this.button = $('<button type="button"></button>'))
        .addClass('ui-multiselect ui-widget ui-state-default ui-corner-all ui-multiselect-button')
        .addClass(o.classes)
        .attr({ 'title':el.attr('title'), 'aria-haspopup':true, 'tabIndex':el.attr('tabIndex') })
        .text(o.noneSelectedText)
        .insertAfter(el),
         
         arrow = (this.arrow =  $('<span class="ui-icon ui-icon-triangle-1-s">'))
         .insertAfter(button),

/*        buttonlabel = (this.buttonlabel = $('<span/>'))
          .addClass('ui-multiselect-button-label')
          .html(o.noneSelectedText)
          .appendTo(button),
*/
        menu = (this.menu = $('<div />'))
          .addClass('ui-multiselect-menu ui-widget ui-widget-content ui-corner-all')
          .addClass(o.classes)
          .appendTo($(o.appendTo)),

        header = (this.header = $('<div />'))
          .addClass('ui-widget-header ui-corner-all ui-multiselect-header ui-helper-clearfix')
          .appendTo(menu),

        headerLinkContainer = (this.headerLinkContainer = $('<ul />'))
          .addClass('ui-helper-reset')
          .html(function() {
            if(o.header === true) {
              return '<li><a class="ui-multiselect-all" href="#"><span class="ui-icon ui-icon-check"></span><span>' + o.checkAllText + '</span></a></li><li><a class="ui-multiselect-none" href="#"><span class="ui-icon ui-icon-closethick"></span><span>' + o.uncheckAllText + '</span></a></li>';
            } else if(typeof o.header === "string") {
              return '<li>' + o.header + '</li>';
            } else {
              return '';
            }
          })
          .append(function() {
              if(o.showClose == true) {
                  return '<li class="ui-multiselect-close"><a href="#" class="ui-multiselect-close"><span class="ui-icon ui-icon-circle-close"></span></a></li>'}
               else {
                   return ""
               }
               })
          .appendTo(header),

        checkboxContainer = (this.checkboxContainer = $('<ul />'))
          .addClass('ui-multiselect-checkboxes ui-helper-reset')
          .appendTo(menu);

        // perform event bindings
        this._bindEvents();

        // build menu
        this.refresh(true);

        // some addl. logic for single selects
        if(!o.multiple) {
          menu.addClass('ui-multiselect-single');
        }

        // bump unique ID
        multiselectID++;
    },

    _init: function() {
      if(this.options.header === false) {
        this.header.hide();
      }
      if(!this.options.multiple) {
        this.headerLinkContainer.find('.ui-multiselect-all, .ui-multiselect-none').hide();
      }
      if(this.options.autoOpen) {
        this.open();
      }
      if(this.element.is(':disabled')) {
        this.disable();
      }
    },

    refresh: function(init) {
      var el = this.element;
      var o = this.options;
      var menu = this.menu;
      var checkboxContainer = this.checkboxContainer;
      var optgroups = [];
      var html = "";
      var id = el.attr('id') || multiselectID++; // unique ID for the label & option tags

      // build items
      el.find('option').each(function(i) {
        var $this = $(this);
        var parent = this.parentNode;
        var description = this.innerHTML;
        var title = this.title;
        var value = this.value;
        var inputID = 'ui-multiselect-' + (this.id || id + '-option-' + i);
        var isDisabled = this.disabled;
        var isSelected = this.selected;
        var labelClasses = [ 'ui-corner-all' ];
        var liClasses = (isDisabled ? 'ui-multiselect-disabled ' : ' ') + this.className;
        var optLabel;

        // is this an optgroup?
        if(parent.tagName === 'OPTGROUP') {
          optLabel = parent.getAttribute('label');

          // has this optgroup been added already?
          if($.inArray(optLabel, optgroups) === -1) {
            html += '<li class="ui-multiselect-optgroup-label ' + parent.className + '"><a href="#">' + optLabel + '</a></li>';
            optgroups.push(optLabel);
          }
        }

        if(isDisabled) {
          labelClasses.push('ui-state-disabled');
        }

        // browsers automatically select the first option
        // by default with single selects
        if(isSelected && !o.multiple) {
          labelClasses.push('ui-state-active');
        }

        html += '<li class="' + liClasses + '">';

        // create the label
        html += '<label for="' + inputID + '" title="' + title + '" class="' + labelClasses.join(' ') + '">';
        html += '<input id="' + inputID + '" name="multiselect_' + id + '" type="' + (o.multiple ? "checkbox" : "radio") + '" value="' + value + '" title="' + title + '"';

        // pre-selected?
        if(isSelected) {
          html += ' checked="checked"';
          html += ' aria-selected="true"';
        }

        // disabled?
        if(isDisabled) {
          html += ' disabled="disabled"';
          html += ' aria-disabled="true"';
        }

        // add the title and close everything off
        html += ' /><span>' + description + '</span></label></li>';
      });

      // insert into the DOM
      checkboxContainer.html(html);

      // cache some moar useful elements
      this.labels = menu.find('label');
      this.inputs = this.labels.children('input');

      // set widths
      this._setButtonWidth();
      this._setMenuWidth();

      // remember default value
      this.button[0].defaultValue = this.update();

      // broadcast refresh event; useful for widgets
      if(!init) {
        this._trigger('refresh');
      }
    },

    // updates the button text. call refresh() to rebuild
    update: function() {
      var o = this.options;
      var $inputs = this.inputs;
      var $checked = $inputs.filter(':checked');
      var numChecked = $checked.length;
      var value;

      if(numChecked === 0) {
        value = o.noneSelectedText;
      } else {
        if($.isFunction(o.selectedText)) {
          value = o.selectedText.call(this, numChecked, $inputs.length, $checked.get());
        } else if(/\d/.test(o.selectedList) && o.selectedList > 0 && numChecked <= o.selectedList) {
          value = $checked.map(function() { return $(this).next().html(); }).get().join(', ');
        } else {
          value = o.selectedText.replace('#', numChecked).replace('#', $inputs.length);
        }
      }

      this._setButtonValue(value);
      if($.trim(value) == "") {
         this.button.outerHeight(23); 
      }

      return value;
    },

    // this exists as a separate method so that the developer 
    // can easily override it.
    _setButtonValue: function(value) {
      //this.buttonlabel.text(value);
      this.button.text(value);
      this.button.attr("title", value);
    },

    // binds events
    _bindEvents: function() {
      var self = this;
      var button = this.button;
      var arrow = this.arrow;

      function clickHandler() {
        self[ self._isOpen ? 'close' : 'open' ]();
        return false;
      }

      // webkit doesn't like it when you click on the span :(
      button
        .find('span')
        .bind('click.multiselect', clickHandler);
      
      arrow.bind({click:clickHandler}); 

      // button events
      button.bind({
        click: clickHandler,
        keypress: function(e) {
          switch(e.which) {
            case 27: // esc
              case 38: // up
              case 37: // left
              self.close();
            break;
            case 39: // right
              case 40: // down
              self.open();
            break;
          }
        },
        mouseenter: function() {
          if(!button.hasClass('ui-state-disabled')) {
            $(this).addClass('ui-state-hover');
          }
        },
        mouseleave: function() {
          $(this).removeClass('ui-state-hover');
        },
        focus: function() {
          if(!button.hasClass('ui-state-disabled')) {
            $(this).addClass('ui-state-focus');
          }
        },
        blur: function() {
          $(this).removeClass('ui-state-focus');
        }
      });

      // header links
      this.header.delegate('a', 'click.multiselect', function(e) {
        // close link
        if($(this).hasClass('ui-multiselect-close')) {
          self.close();

          // check all / uncheck all
        } else {
          self[$(this).hasClass('ui-multiselect-all') ? 'checkAll' : 'uncheckAll']();
        }

        e.preventDefault();
      });

      // optgroup label toggle support
      this.menu.delegate('li.ui-multiselect-optgroup-label a', 'click.multiselect', function(e) {
        e.preventDefault();

        var $this = $(this);
        var $inputs = $this.parent().nextUntil('li.ui-multiselect-optgroup-label').find('input:visible:not(:disabled)');
        var nodes = $inputs.get();
        var label = $this.parent().text();

        // trigger event and bail if the return is false
        if(self._trigger('beforeoptgrouptoggle', e, { inputs:nodes, label:label }) === false) {
          return;
        }

        // toggle inputs
        self._toggleChecked(
          $inputs.filter(':checked').length !== $inputs.length,
          $inputs
        );

        self._trigger('optgrouptoggle', e, {
          inputs: nodes,
          label: label,
          checked: nodes[0].checked
        });
      })
      .delegate('label', 'mouseenter.multiselect', function() {
        if(!$(this).hasClass('ui-state-disabled')) {
          self.labels.removeClass('ui-state-hover');
          $(this).addClass('ui-state-hover').find('input').focus();
        }
      })
      .delegate('label', 'keydown.multiselect', function(e) {
        e.preventDefault();

        switch(e.which) {
          case 9: // tab
            case 27: // esc
            self.close();
          break;
          case 38: // up
            case 40: // down
            case 37: // left
            case 39: // right
            self._traverse(e.which, this);
          break;
          case 13: // enter
            $(this).find('input')[0].click();
          break;
        }
      })
      .delegate('input[type="checkbox"], input[type="radio"]', 'click.multiselect', function(e) {
        var $this = $(this);
        var val = this.value;
        var checked = this.checked;
        var tags = self.element.find('option');

        // bail if this input is disabled or the event is cancelled
        if(this.disabled || self._trigger('click', e, { value: val, text: this.title, checked: checked }) === false) {
          e.preventDefault();
          return;
        }

        // make sure the input has focus. otherwise, the esc key
        // won't close the menu after clicking an item.
        $this.focus();

        // toggle aria state
        $this.attr('aria-selected', checked);

        // change state on the original option tags
        tags.each(function() {
          if(this.value === val) {
            this.selected = checked;
          } else if(!self.options.multiple) {
            this.selected = false;
          }
        });

        // some additional single select-specific logic
        if(!self.options.multiple) {
          self.labels.removeClass('ui-state-active');
          $this.closest('label').toggleClass('ui-state-active', checked);

          // close menu
          self.close();
        }

        // fire change on the select box
        self.element.trigger("change");
        self._changed = true;
        

        // setTimeout is to fix multiselect issue #14 and #47. caused by jQuery issue #3827
        // http://bugs.jquery.com/ticket/3827
        setTimeout($.proxy(self.update, self), 10);
      });

      // close each widget when clicking on any other element/anywhere else on the page
      $doc.bind('mousedown.' + this._namespaceID, function(event) {
        var target = event.target;

        if(self._isOpen
            && target !== self.button[0]
            && target !== self.menu[0]
            && target !== self.arrow[0]
            && !$.contains(self.menu[0], target)
            && !$.contains(self.button[0], target)
            && !$.contains(self.arrow[0], target)
          ) {
          self.close();
        }
      });

      // deal with form resets.  the problem here is that buttons aren't
      // restored to their defaultValue prop on form reset, and the reset
      // handler fires before the form is actually reset.  delaying it a bit
      // gives the form inputs time to clear.
      $(this.element[0].form).bind('reset.multiselect', function() {
        setTimeout($.proxy(self.refresh, self), 10);
      });
    },

    value:function(newValue) {
        if(newValue === undefined) {
            return this.element.val();
        }
        else {
            this.element.val(newValue);
            this.refresh();
        } 
    },

    // set button width
    _setButtonWidth: function() {
      var width = this.element.outerWidth();
      var o = this.options;

      if(/\d/.test(o.minWidth) && width < o.minWidth) {
        width = o.minWidth;
      }

      // set widths
      this.button.outerWidth(width);
      this._positionArrow();
    },

    _positionArrow:function() {
        var buttonPos = this.button.offset();
        var arrowWidth = this.arrow.width();
        var arrowHeight = this.arrow.height();
        var arrowTop = (buttonPos.top > 0)?buttonPos.top:-(arrowHeight+6);
        this.arrow.offset({top:arrowTop, left:(buttonPos.left+this.button.width()-arrowWidth)});
    },

    // set menu width
    _setMenuWidth: function() {
      var m = this.menu;
      m.outerWidth(this.button.outerWidth());
    },

    // move up or down within the menu
    _traverse: function(which, start) {
      var $start = $(start);
      var moveToLast = which === 38 || which === 37;

      // select the first li that isn't an optgroup label / disabled
      var $next = $start.parent()[moveToLast ? 'prevAll' : 'nextAll']('li:not(.ui-multiselect-disabled, .ui-multiselect-optgroup-label)').first();

      // if at the first/last element
      if(!$next.length) {
        var $container = this.menu.find('ul').last();

        // move to the first/last
        this.menu.find('label')[ moveToLast ? 'last' : 'first' ]().trigger('mouseover');

        // set scroll position
        $container.scrollTop(moveToLast ? $container.height() : 0);

      } else {
        $next.find('label').trigger('mouseover');
      }
    },

    // This is an internal function to toggle the checked property and
    // other related attributes of a checkbox.
    //
    // The context of this function should be a checkbox; do not proxy it.
    _toggleState: function(prop, flag) {
      return function() {
        if(!this.disabled) {
          this[ prop ] = flag;
        }

        if(flag) {
          this.setAttribute('aria-selected', true);
        } else {
          this.removeAttribute('aria-selected');
        }
      };
    },

    _toggleChecked: function(flag, group) {
      var $inputs = (group && group.length) ?  group : this.inputs;
      var self = this;

      // toggle state on inputs
      $inputs.each(this._toggleState('checked', flag));

      // give the first input focus
      $inputs.eq(0).focus();

      // update button text
      this.update();

      // gather an array of the values that actually changed
      var values = $inputs.map(function() {
        return this.value;
      }).get();

      // toggle state on original option tags
      this.element
        .find('option')
        .each(function() {
          if(!this.disabled && $.inArray(this.value, values) > -1) {
            self._toggleState('selected', flag).call(this);
          }
        });

      // trigger the change event on the select
      if($inputs.length) {
        this.element.trigger("change");
        this._changed = true;
      }
    },

    _toggleDisabled: function(flag) {
      this.button.attr({ 'disabled':flag, 'aria-disabled':flag })[ flag ? 'addClass' : 'removeClass' ]('ui-state-disabled');
      this.arrow.attr({ 'disabled':flag, 'aria-disabled':flag })[ flag ? 'addClass' : 'removeClass' ]('ui-state-disabled');

      var inputs = this.menu.find('input');
      var key = "ech-multiselect-disabled";

      if(flag) {
        // remember which elements this widget disabled (not pre-disabled)
        // elements, so that they can be restored if the widget is re-enabled.
        inputs = inputs.filter(':enabled').data(key, true)
      } else {
        inputs = inputs.filter(function() {
          return $.data(this, key) === true;
        }).removeData(key);
      }

      inputs
        .attr({ 'disabled':flag, 'arial-disabled':flag })
        .parent()[ flag ? 'addClass' : 'removeClass' ]('ui-state-disabled');

      this.element.attr({
        'disabled':flag,
        'aria-disabled':flag
      });
    },

    // open the menu
    open: function(e) {
      var self = this;
      var button = this.button;
      var menu = this.menu;
      var speed = this.speed;
      var o = this.options;
      var args = [];

      // bail if the multiselectopen event returns false, this widget is disabled, or is already open
      if(this._trigger('beforeopen') === false || button.hasClass('ui-state-disabled') || this._isOpen) {
        return;
      }

      var $container = menu.find('ul').last();
      var effect = o.show;

      // figure out opening effects/speeds
      if($.isArray(o.show)) {
        effect = o.show[0];
        speed = o.show[1] || self.speed;
      }

      // if there's an effect, assume jQuery UI is in use
      // build the arguments to pass to show()
      if(effect) {
        args = [ effect, speed ];
      }

      // set the scroll of the checkbox container
      $container.scrollTop(0).height(o.height);

      // positon
      this.position();

      // show the menu, maybe with a speed/effect combo
      $.fn.show.apply(menu, args);

      // select the first not disabled option
      // triggering both mouseover and mouseover because 1.4.2+ has a bug where triggering mouseover
      // will actually trigger mouseenter.  the mouseenter trigger is there for when it's eventually fixed
      this.labels.filter(':not(.ui-state-disabled)').eq(0).trigger('mouseover').trigger('mouseenter').find('input').trigger('focus');

      button.addClass('ui-state-active');
      this._isOpen = true;
      this._trigger('open');
    },

    // close the menu
    close: function() {
      if(this._trigger('beforeclose') === false) {
        return;
      }

      var o = this.options;
      var effect = o.hide;
      var speed = this.speed;
      var args = [];

      // figure out opening effects/speeds
      if($.isArray(o.hide)) {
        effect = o.hide[0];
        speed = o.hide[1] || this.speed;
      }

      if(effect) {
        args = [ effect, speed ];
      }

      $.fn.hide.apply(this.menu, args);
      this.button.removeClass('ui-state-active').trigger('blur').trigger('mouseleave');
      this._isOpen = false;
      this._trigger('close');
      if(this._changed) {
          this._trigger('change');
          this._changed = false;
      }
    },

    enable: function() {
      this._toggleDisabled(false);
    },

    disable: function() {
      this._toggleDisabled(true);
    },

    checkAll: function(e) {
      this._toggleChecked(true);
      this._trigger('checkAll');
    },

    uncheckAll: function() {
      this._toggleChecked(false);
      this._trigger('uncheckAll');
    },

    getChecked: function() {
      return this.menu.find('input').filter(':checked');
    },

    destroy: function() {
      // remove classes + data
      $.Widget.prototype.destroy.call(this);

      // unbind events
      $doc.unbind(this._namespaceID);

      this.button.remove();
      this.menu.remove();
      this.element.show();

      return this;
    },

    isOpen: function() {
      return this._isOpen;
    },

    widget: function() {
      return this.menu;
    },

    getButton: function() {
      return this.button;
    },

    position: function() {
      var o = this.options;

      // use the position utility if it exists and options are specifified
      if($.ui.position && !$.isEmptyObject(o.position)) {
        o.position.of = o.position.of || this.button;

        this.menu
          .show()
          .position(o.position)
          .hide();

        // otherwise fallback to custom positioning
      } else {
        var pos = this.button.offset();

        this.menu.css({
          top: pos.top + this.button.outerHeight(),
          left: pos.left
        });
      }
    },

    // react to option changes after initialization
    _setOption: function(key, value) {
      var menu = this.menu;

      switch(key) {
        case 'header':
          menu.find('div.ui-multiselect-header')[value ? 'show' : 'hide']();
          break;
        case 'checkAllText':
          menu.find('a.ui-multiselect-all span').eq(-1).text(value);
          break;
        case 'uncheckAllText':
          menu.find('a.ui-multiselect-none span').eq(-1).text(value);
          break;
        case 'height':
          menu.find('ul').last().height(parseInt(value, 10));
          break;
        case 'minWidth':
          this.options[key] = parseInt(value, 10);
          this._setButtonWidth();
          this._setMenuWidth();
          break;
        case 'selectedText':
        case 'selectedList':
        case 'noneSelectedText':
          this.options[key] = value; // these all needs to update immediately for the update() call
          this.update();
          break;
        case 'classes':
          menu.add(this.button).removeClass(this.options.classes).addClass(value);
          break;
        case 'multiple':
          menu.toggleClass('ui-multiselect-single', !value);
          this.options.multiple = value;
          this.element[0].multiple = value;
          this.refresh();
          break;
        case 'position':
          this.position();
      }

      $.Widget.prototype._setOption.apply(this, arguments);
    }
  });

})(jQuery);
  (function($, IDB) {
   $.widget("idb.multiselecteditable", $.ech.multiselect,  {
      options: {
          isDate:false,
                   // height, width, cols = textArea cols
          change: null,
          value:null,
          classes: '',
          css:{}
      },

      _create: function() {
        var o = this.options;
        o.noneSelectedText = "";
        this._shouldPositionArrow = true;
        this._super();
        var me = this;
        me._isTextAreaOpen = false;
        me._enteredValues = null;
        var $button = this.$button = this.button.addClass(o.classes).css(o.css);
        $button.outerHeight(23);
        var $downArrow = this.$downArrow = $button.next('.ui-icon');
        var $textAreaDiv = this.$textAreaDiv = $('<div>').css({position:"absolute", "z-index":10000}).appendTo($('body')); 
        var $textArea = this.$textArea = $('<textarea rows="4" cols="50" placeholder="Enter values (one per line)"></textarea>').appendTo($textAreaDiv); 
        $textAreaDiv.addClass(o.classes).css(o.css);
        $textAreaDiv.hide();
        var value = this.options.value;
        this._shouldPositionArrow = false;
        this.value(value);
        this._shouldPositionArrow = true;
        var offset = $button.offset();
        if(offset.top > 0 && offset.left > 0) {
            this._positionArrow(); 
        }

        if(!o.isDate) {
            $textAreaDiv.focusout(function(e) {
                me._valueCommit(); 
            });
         } else {
            this._addDatepicker();
        }
        $button.click(function(e) {
            var mouseX = e.pageX;
            var offset = $button.offset();
            var arrowWidth = $downArrow.get(0).offsetWidth + 8;
            me._positionArrow();
             
            //
            // The arrow was clicked. Don't do anything.
            //
            if(mouseX > (offset.left + this.offsetWidth - arrowWidth)) {
                return;
            }

            me._openTextArea();
            e.stopPropagation();
            if(me._isOpen) {
              me.close();
            }
        });

        $textArea.on("input", function() {
            if(me._isTextAreaOpen) {
                me.uncheckAll();
            }
        });

        me.element.on("multiselecteditableclick multiselecteditablecheckall multiselecteditableuncheckall", function() {
           me._enteredValues = null;  
        }); 

        // close each widget when clicking on any other element/anywhere else on the page
        $(document).bind('mousedown.' + this._namespaceID, function(event) {
          var target = event.target;
          if(me._isTextAreaOpen && target !== $textAreaDiv[0] && !$.contains($textAreaDiv[0], target)) {
              me._valueCommit();
          }
        });

        

        this._position();

        this._creationComplete = true;
      },

      _positionArrow:function() {
          if(this._shouldPositionArrow) {
             this._super();
          }
      },
      _openTextArea: function() {
          var me = this;
          me._isTextAreaOpen = true;
          me._setTextAreaWidth();

          if(me._enteredValues == null) {
              me.$textArea.val(me.button.text().replace(/,\s?/g,'\n'));
          }
          me.$textAreaDiv.show();
          me.$textArea.focus();
          if(me.options.isDate) {
              me._addDateClickHandler();
          }
          me._position();
      },

      _init: function() {

             },
      _valueCommit : function() {
           this._setButtonValue(this._formatForTextInput(this.$textArea.val()));
           this._enteredValues = this.$textArea.val().split('\n');
           this._isTextAreaOpen = false;
           this.$textAreaDiv.hide();
           this._trigger('change');
      },
      _dateClickHandler: function(e) {
          var me = e.data.me;
          if(me._partOfDatepicker(e.target)) {
             return;
          } 
          else {
              me._valueCommit();
              me._removeDateClickHandler(); 
          }
      },

      _partOfDatepicker: function(target) {
          var classList = target.classList;
          for(var i=0; i < classList.length; i++)  {
              var item = classList.item(i);
              if(item.indexOf("datepick") >= 0) {
                 return true;
              }
          }
          return false;
      },

      _addDateClickHandler: function() {
         $('body').on("click", null, {me:this}, this._dateClickHandler);
      },
      _removeDateClickHandler: function() {
         $('body').off("click", this._dateClickHandler);
      },

      _formatForTextInput:function(v) {
          return v.replace(/\n/g,",");
      },
      _setTextInput:function() {
         var v = this.$textArea.val();
         this._setButtonValue(this._formatForTextInput(v));
      },
      _position:function() {
          var pos = this.$button.offset();
          this.$textAreaDiv.css({
              top:pos.top-1,
              left:pos.left
          });
          if(this.$pickerButton) {
              var nY = this.$textArea.outerHeight();
              this.$pickerButton.css({top:-nY+22});
              console.log(nY);
          }
      },
      _setTextAreaWidth: function() {
          var width = this.$button.outerWidth();
          var o = this.options;

          if(/\d/.test(o.minWidth) && width < o.minWidth) {
              width = o.minWidth;
          }
          this.$textArea.outerWidth(width+30);
      },

      _addDatepicker: function() {
          var $textArea = this.$textArea;
          $textArea.datepick({multiSelect:1000,
                              multiSeparator:'\n',
                              showTrigger:'<img src="' + IDB.config.contextRoot + 'html5/images/calendar.gif">',
                   //        showTrigger:'<img src="calendar.gif">',
                              showOnFocus:false,
                              dateFormat:'yyyy-mm-dd',
                              constrainInput:false,
                              changeMonth:false});
          var $pickerButton = this.$pickerButton = $(".datepick-trigger");
          $pickerButton.css({position:"relative", "margin-left":"-20px"});

          var taX = $textArea.outerWidth();
          var taY = $textArea.outerHeight();

          $textArea.on("mousedown mousemove", function() {
                //if(!dragging) return;
                var $this = $(this);

                var nX = $this.outerWidth(); 
                var nY = $this.outerHeight(); 
                if(nX != taX || nY != taY) {
                    $pickerButton.css({top:-nY+22});
                }
                taX = nX;
                taY = nY;
         });
      },

      value: function(v) {
          //
          // No value passed, act as getter.
          //
          if(v === undefined) {
              if(this._enteredValues == null) {
                  return this.element.val();
              }
              else {
                  //return this.$textArea.val().split('\n');
                  return this._enteredValues;
              }
          }
          else {
              //
              // Value passed, act as setter.
              //
              if(v === null) {
                  return;
              }
              if(!(v instanceof Array)) {
                  v = [v];
              }


              // 
              // if all values are selection items, then just set them to selected.
              //
              var items = this.element.find('option').toArray(); 
              if(v.every(function(val) {
                             return items.some(function(o) {
                                            return o.value == val; 
                                        })})) {
                this._super(v);
              } else {
                  this.uncheckAll();
                  this._enteredValues = v;
                  v = v.join('\n');
                  this.$textArea.val(v);
                  this._setTextInput();
              }
          }
      },
      destroy: function() {
          this.$textAreaDiv.remove();
          return this._super();
      },
      _toggleDisabled: function(flag) {
          this._super(flag);
          this.$textArea.attr({'disabled':flag, 
                                 'aria-disabled':flag});
      }
    });
  })(jQuery, IDB);
  (function($, IDB) {

   var multitextinputID = 0;
   var $doc = $(document);

   $.widget("idb.multitextinput", {
      options: {
          isDate:false,
                   // height, width, cols = textArea cols
          change: null,
          value:null,
          classes: '',
          css:{}
      },

      _create: function() {
        var me = this;
        var o = this.options;

       // create a unique namespace for events that the widget
       // factory cannot unbind automatically. Use eventNamespace if on
       // jQuery UI 1.9+, and otherwise fallback to a custom string.
       me._namespaceID = me.eventNamespace || ('multitextinput' + multitextinputID);
       me._isOpen = false;

        var $textInput = this.$textInput = $(this.element).addClass(o.classes).css(o.css);
        var $textAreaDiv = this.$textAreaDiv = $('<div>').css({position:"absolute", "z-index":10000}).appendTo($('body')); 
        var $textArea = this.$textArea = $('<textarea rows="4" cols="50" placeholder="Enter values (one per line)"></textarea>').appendTo($textAreaDiv); 
        $textAreaDiv.addClass(o.classes).css(o.css);
        $textAreaDiv.hide();
        var value = this.options.value;
        this.value(value);
        if(!o.isDate) {
            $textAreaDiv.focusout(function(e) {
                me._valueCommit(); 
            });
         } else {
            this._addDatepicker();
        }
        $textInput.click(function(e) {
            me._setTextAreaWidth();
            $textAreaDiv.show();
            me._isOpen = true;
            $textArea.focus();
            if(me.options.isDate) {
                me._addDateClickHandler();
            }
            me._position();
            e.stopPropagation();
        });

        // close each widget when clicking on any other element/anywhere else on the page
        $(document).bind('mousedown.' + this._namespaceID, function(event) {
            var target = event.target;
            if(me._isOpen && target !== $textAreaDiv[0] && !$.contains($textAreaDiv[0], target)
                   && !me._partOfDatepicker(target)) {
                me._valueCommit();
            }
        }); 

        this._position();

          // bump unique ID
          multitextinputID++;
      },
      _init: function() {

             },
      _valueCommit : function() {
           this.$textInput.val(this._formatForTextInput(this.$textArea.val()));
           this.$textAreaDiv.hide();
           this._isOpen = false;
           this._trigger('change');
      },
      _dateClickHandler: function(e) {
          var me = e.data.me;
          if(me._partOfDatepicker(e.target)) {
             return;
          } 
          else {
              me._valueCommit();
              me._removeDateClickHandler(); 
          }
      },

      _partOfDatepicker: function(target) {
          var classList = target.classList;
          for(var i=0; i < classList.length; i++)  {
              var item = classList.item(i);
              if(item.indexOf("datepick") >= 0) {
                 return true;
              }
          }
          return false;
      },

      _addDateClickHandler: function() {
         $('body').on("click", null, {me:this}, this._dateClickHandler);
      },
      _removeDateClickHandler: function() {
         $('body').off("click", this._dateClickHandler);
      },

      _formatForTextInput:function(v) {
          return v.replace(/\n/g,",");
      },
      _setTextInput:function() {
         var v = this.$textArea.val();
         this.$textInput.val(this._formatForTextInput(v));
      },
      _position:function() {
          var pos = this.$textInput.offset();
          this.$textAreaDiv.css({
              top:pos.top-1,
              left:pos.left
          });
          if(this.$pickerButton) {
              var nY = this.$textArea.outerHeight();
              this.$pickerButton.css({top:-nY+22});
              console.log(nY);
          }
      },
      _setTextAreaWidth: function() {
          var width = this.$textInput.outerWidth();
          var o = this.options;

          if(/\d/.test(o.minWidth) && width < o.minWidth) {
              width = o.minWidth;
          }
          this.$textArea.outerWidth(width+30);
      },

      _addDatepicker: function() {
          var $textArea = this.$textArea;
          $textArea.datepick({multiSelect:1000,
                              multiSeparator:'\n',
                              showTrigger:'<img src="' + IDB.config.contextRoot + 'html5/images/calendar.gif">',
                   //        showTrigger:'<img src="calendar.gif">',
                              showOnFocus:false,
                              dateFormat:'yyyy-mm-dd',
                              constrainInput:false,
                              changeMonth:false});
          var $pickerButton = this.$pickerButton = $(".datepick-trigger");
          $pickerButton.css({position:"relative", "margin-left":"-20px"});

          var taX = $textArea.outerWidth();
          var taY = $textArea.outerHeight();

          $textArea.on("mousedown mousemove", function() {
                //if(!dragging) return;
                var $this = $(this);

                var nX = $this.outerWidth(); 
                var nY = $this.outerHeight(); 
                if(nX != taX || nY != taY) {
                    $pickerButton.css({top:-nY+22});
                }
                taX = nX;
                taY = nY;
         });
      },

      value: function(v) {
          //
          // No value passed, act as getter.
          //
          if(v === undefined) {
             return this.$textArea.val().split('\n');
          }
          else {
              //
              // Value passed, act as setter.
              //
              if(v instanceof Array) {
                  v = v.join('\n');
              }
              this.$textArea.val(v);
              this._setTextInput();
          }
      },

      destroy: function() {
          // remove classes + data
          $.Widget.prototype.destroy.call(this);

          // unbind events
          $(document).unbind(this._namespaceID);

          this.$textAreaDiv.remove();

          this.element.show();

          return this;
      }
    });
  })(jQuery, IDB);
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.DashboardBase
/*global $, IDB, Kinetic*/
IDB.DashboardBase = function(div, data, callbacks, drilldownInfo) {
  IDB.log(data, "\n>>>>> DASHBOARD DASHBOARD DASHBOARD DASHBOARD DASHBOARD DASHBOARD DASHBOARD DASHBOARD <<<<<"); 
  IDB.log("\n");
  var me = this, url, bgSize;
  me.div = div;
  me.$div = $(div);
  me.data = data;
  me.$paramDiv = null;
  me.$parent = me.$div.parent();

  //
  // DrilldownInfo is null when this not a drilldown. This is not the same as an empty object. An empty
  // object means this is a drilldown without parameters or filters.
  //
  me.drilldownInfo = drilldownInfo;
  me.isDrilldown = !!drilldownInfo; // null or undefined
  me.callbacks = callbacks?callbacks:{};
  me.charts = [];
  me.fadeOtherValues = data.fade;
  me.pivotSyncEnabled = data.syncPivots;
  me.dashboardParams = data.params;
  me.autoRefreshInterval = data.autoRefreshInterval || 0;
  me.drilldownFilters = me.isDrilldown?(drilldownInfo.drilldownFilters || []):null;
  me.drilldownParamSet = me.isDrilldown?(drilldownInfo.drilldownParamSet || []):null;
  me.dashboardParameterInputRequired = data.dashboardParameterInputRequired;
  $("h1#dashtitle", "div#db-header").text(me.getTitle());
  IDB.maybeShowBackButton();
  if(me.dashboardParameterInputRequired) {
      me.maybeShowDashboardParameterInput();
  }
  else {
      me.writeLayoutDivs(data.dashLayout, me.div);

      $(div).css({"background-color":data.settings.backgroundColor});
    
      if(data.imageUrl) {
          url = "url('" + IDB.resolveUrl(data.imageUrl, data.imageSettings.useProxy) + "')";
          bgSize = data.imageSettings.fitImage ?"100% 100%":"auto";
         $(div).css({"background-image":url,
         "background-repeat":"no-repeat", "background-attachment":"scroll","background-position":"center",
         "background-size":bgSize});
      }
      me.maybeStartRefresh();
   }
};

IDB.DashboardBase.prototype.maybeShowDashboardParameterInput = function() {
   var me = this, callbacks;
   if(!me.dashboardParams || me.dashboardParams.length === 0) {
       return;
   }

   callbacks = {dashboardParameterValuesChanged:IDB.listener(me,"dashboardParameterValuesChanged")};
   
   if(!me.$paramDiv) { 
       me.$paramDiv = $("<div style=\"width:100%;height:100%;\">");
       me.$paramDiv.attr("id", me.$div.attr("id"));
   }

   me.detach();
   me.$paramDiv.appendTo(me.$parent);

   if(!me.dashboardParameterInputPanel) {
       me.dashboardParameterInputPanel = new IDB.DashboardParameterInputPanel(me.$paramDiv, me.data, callbacks); 
   }
   me.dashboardParameterInputPanel.position();
};
    
IDB.DashboardBase.prototype.dashboardParameterValuesChanged = function() {
    var me = this, dashboardParameterValuesChanged;
    if(me.$paramDiv) {
        me.$paramDiv.detach();
        me.$div.appendTo(me.$parent);
    }
    //
    // The dashboard will manage it's own changes if the prompt is not displayed.
    //
    if(me.dashboardParameterInputPanel && me.dashboardParameterInputRequired) {
        dashboardParameterValuesChanged = me.callbacks.dashboardParameterValuesChanged;
        if(dashboardParameterValuesChanged) {
            dashboardParameterValuesChanged();
        }
    }
    else {
        me.dashframes.forEach(function(dashframe) {dashframe.dashboardParameterValuesChanged();});

        if(me.dashboardParameterInputPanel) {
            me.dashboardParameterInputPanel.update();
        }
    }
};

IDB.DashboardBase.prototype.pivotSelectionChanged = function(caller, axisName, selectionAsString, allSelections) {
    var me = this, len, j, client;
    if(!me.pivotSyncEnabled) return;
    len = me.charts.length;
    for(j=0; j<len; j++) {
        client = me.charts[j];
        if(client != caller) {
            client.changePivotSelections(axisName, selectionAsString, allSelections);
        }
    }
};

IDB.DashboardBase.prototype.drilldownToFramesWithTag = function(originator, frameTag, chartId, filters, drilldownParamSet, dashboardParams) {

    var me = this, found = false, dfs = me.dashframes, j, jj, df;
    for(j=0, jj=dfs.length; j<jj; j++) {
        df = dfs[j];
        if(frameTag === df.frameTag) {
            found = true;
            df.loadChartFromDrilldown(originator, chartId, filters, drilldownParamSet, dashboardParams);
            if (IDB.PHONEMODE)
            {
                IDB.phoneModeNavTo(dfs[j].frameId);
            }
        }
    }
    if(!found) {
        originator.loadChartFromDrilldown(originator, chartId, filters, drilldownParamSet, dashboardParams);
    }
};


IDB.DashboardBase.prototype.addChart = function(chart) {
   this.charts.push(chart);
};

IDB.DashboardBase.prototype.removeChart = function(chart) {
   var charts = this.charts;
   this.charts = charts.filter(function(e,i,a) {return (e!=chart);});
};


IDB.DashboardBase.PANES_MAP = {
    h:["north", "center", "south"],
    v:["west", "center", "east"],
    n:["center"]
};

IDB.DashboardBase.prototype.writeLayoutDivs = function(layout, target) {
    var children = layout.children, j, jj, panes, childLayout, html, $layoutDiv, frameDiv;
    for(j=0, jj=children.length; j<jj; j++) {
        panes = IDB.DashboardBase.PANES_MAP[layout.splitDir];
        childLayout = children[j];
        html = "<div class=\"ui-layout-" + panes[j] + "\"></div>";
        $layoutDiv = $(html).appendTo(target);
        if(childLayout.children.length === 0) {
            frameDiv = $("<div id=\"frame-" + childLayout.frameId + "\" data-frame-id=\"" + childLayout.frameId + "\" class=\"frame-content-container\"></div>");
            frameDiv.appendTo($layoutDiv);

        }
        else {
            this.writeLayoutDivs(childLayout, $layoutDiv);
        }
    }
};

IDB.DashboardBase.prototype.load = function () {
    var me = this, j, jj, data, div, df, fs; 
    IDB.log(me.data, "DashboardBase.load()");
    if(me.dashboardParameterInputRequired) {
       me.$div.trigger("create");
       me.dashboardParameterInputPanel.position();
       me.dashboardParameterInputPanel.applyStyles();
       return;
    }
    me.createLayouts();
    fs = me.data.dashframes;
    me.dashframes = [];
    me.dashframeMap = {};
    for(j=0, jj=fs.length; j<jj; j++) {
        data = fs[j];
        div = $("div#frame-" + data.frameId, me.div)[0];
        df = new IDB.Dashframe(div, data, me);
        me.dashframes.push(df);
        me.dashframeMap[data.frameId] = df;
    }
    if (IDB.PHONEMODE)
    {
        IDB.phoneModeInit();
    }
};

IDB.DashboardBase.prototype.createLayouts = function() {
    var me = this, rootLayout, jql;
    me.layouts = [];
    rootLayout = me.data.dashLayout;
    jql = rootLayout.jql;
    jql.frameId = rootLayout.frameId;
    jql.onresize = IDB.listener(me, "onPaneResize");
    IDB.log("jql: " + IDB.Util.objToString(jql));
    me.layouts[0] = $(me.div).layout(jql);
    me.createChildLayouts(rootLayout, 0);
    $(".ui-layout-resizer", me.div).css(me.data.settings.dividerCSS);
};

IDB.DashboardBase.prototype.toString = function() {
   return ("Dashboard[dashId=" + this.data.dashId + "]");
};


IDB.DashboardBase.prototype.createChildLayouts = function(parentLayout, parentLayoutIndex) {
    var me = this, j, jj, jql, childLayout,
        panes = IDB.DashboardBase.PANES_MAP[parentLayout.splitDir],
        children = parentLayout.children,
        layoutIndex = parentLayoutIndex;

    for(j=0, jj=children.length; j<jj; j++) {
        childLayout = children[j];
        if(!childLayout.children || !childLayout.children.length) {continue;}
        jql = childLayout.jql;
        jql.onresize = IDB.listener(me, "onPaneResize");
        me.layouts[++layoutIndex] = me.layouts[parentLayoutIndex].panes[panes[j]].layout(jql);
        layoutIndex = me.createChildLayouts(childLayout, layoutIndex);
    }
    return layoutIndex;
};

IDB.DashboardBase.prototype.onPaneResize = function(paneName, $element, state, options, layout) {
//    IDB.log("\n========================================================\nonPaneResize: " + this
//      +", paneName=" + paneName + ", state=" + IDB.Util.objToString(state) + ", \noptions=" + IDB.Util.objToString(options)
//      + ", \n$element=" + $element.html());
//
      var fId = $("div.frame-content-container", $element).attr("data-frame-id");
      var df = this.dashframeMap[fId];
      df.resize();
};

IDB.DashboardBase.prototype.getTitle = function() {
    var me = this, 
        hideCategory = me.data.settings.hideTitleCategory;

    if(hideCategory) {
        return me.data.dashname;
    }
    return me.data.category.categoryDescr + "::" + me.data.dashname;
};


IDB.DashboardBase.prototype.datapointMouseOver = function(caller, matchType, datapoint) {
    var me = this, j, jj,
        charts = me.charts;
//    IDB.log(charts, "chart.length=" + charts.length);
    for(j=0, jj=charts.length; j<jj; j++) {
        if(charts[j] == caller) continue;
        charts[j].highlightDatapoint(matchType, datapoint, me.fadeOtherValues);
    }

};

IDB.DashboardBase.prototype.datapointMouseOut = function(caller, matchType, datapoint) {
    var charts = this.charts, j, jj;
    for(j=0, jj=charts.length; j<jj; j++) {
        if(charts[j] == caller) continue;
        charts[j].normalizeAllDatapoints();
    }
};

IDB.DashboardBase.prototype.drillToDashboard = function(caller, dashId, filters, drilldownParamSet) {
    IDB.drillToDashboard(caller, dashId, filters, drilldownParamSet);
};

IDB.DashboardBase.prototype.launchDashboard = function(caller, dashId, sourceDashboardParameters) {
    IDB.launchDashboard(caller, dashId, sourceDashboardParameters);
};

IDB.DashboardBase.prototype.detach = function() {
    this.$div.detach();
    this.maybeStopRefresh();
    this.maybeStopFrameRefresh();
};

IDB.DashboardBase.prototype.refreshFrames = function(chartsOnly) {
    var me = this, dfs = me.dashframes, j, jj, df;

    if(IDB.isNil(chartsOnly)) {
       chartsOnly = true;
    } 
       
    for(j=0, jj=dfs.length; j<jj; j++) {
      df = dfs[j];
      if(!chartsOnly || df.containsChart) {
          df.reload(true);
      }
    }
};

IDB.DashboardBase.prototype.destroyFrames = function() {
    var me = this, dfs = me.dashframes, j, jj, df;
    if(!dfs) {
        return;
    }
    for(j=0, jj=dfs.length; j<jj; j++) {
      df = dfs[j];
      df.destroy();
    }
};

IDB.DashboardBase.prototype.maybeStartRefresh = function() {
    var me = this;

    if(me.autoRefreshInterval === 0) {
        return;
    }

    me.maybeStopRefresh();

    var delay = me.autoRefreshInterval * 60 * 1000;
    me.refreshTimer = window.setInterval(IDB.listener(me, "onRefreshTimer"), delay);
};

IDB.DashboardBase.prototype.maybeStartFrameRefresh = function() {
    var me = this, dfs = me.dashframes, j, jj, df;
    for(j=0, jj=dfs.length; j<jj; j++) {
      df = dfs[j];
      df.maybeStartRefresh();
    }
};

IDB.DashboardBase.prototype.maybeStopRefresh = function() {
    var me = this;

    if(!IDB.isNil(me.refreshTimer)) {
        window.clearInterval(me.refreshTimer);
        me.refreshTimer = null;
    }
};

IDB.DashboardBase.prototype.maybeStopFrameRefresh = function() {
    var me = this, dfs = me.dashframes, j, jj, df;
    if(!dfs) {
        return;
    }
    for(j=0, jj=dfs.length; j<jj; j++) {
      df = dfs[j];
      df.maybeStopRefresh();
    }
};

IDB.DashboardBase.prototype.onRefreshTimer = function() {
    var me = this;
    
    if(me.dashboardParameterInputRequired) {
        return;
    }

    me.refreshFrames(false);
};

IDB.DashboardBase.prototype.destroy = function() {
    var me = this;

    me.maybeStopRefresh();
    me.destroyFrames();
    me.$div.remove();
};
///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.Dashframe
/*global $, IDB, Event */
IDB.Dashframe = function(div, data, dashboard) {
   var me = this, dashboardParams, drilldownFilters, drilldownParamSet;
   me.div = div;
   me.$div = $(div);
   me.$div.css("position", "relative");
   
   
   me.data = data;
   me.dashboard = dashboard;
   me.frameId = data.frameId;
   me.frameTag = data.frameTag || null;

   me.$chartDiv = null;
   me._nonChartContent = null;
   me.stack = [];
   me.$div.on("resize", IDB.listener(this, "resize")).css({"overflow":"hidden"});
   IDB.log(data, "FRAME DATA");

    switch(data.frameType) {
       case 'chart':
             dashboardParams = dashboard.dashboardParams;
             if(!dashboard.isDrilldown) {
                 me.loadChart(data.chartId, null, null, dashboardParams);
             } 
             else { 
                 drilldownFilters = dashboard.drilldownFilters;
                 drilldownParamSet = dashboard.drilldownParamSet;
                 me.loadChart(data.chartId, drilldownFilters, drilldownParamSet, dashboardParams);
             }
             break;
       case 'image': 
            me._nonChartContent = new IDB.ImagePanel(me.createImageDiv(true), data, dashboard);
            break;
       case 'dashPanel':
            me.loadDashPanel();
            break;
        default:
    } 
};

IDB.Dashframe.prototype.loadContent = function(data) {
    var me = this, dashboard = me.dashboard, dashboardParams, drilldownFilters, drilldownParamSet;
    switch(data.frameType) {
       case 'chart':
             dashboardParams = dashboard.dashboardParams;
             if(!dashboard.isDrilldown) {
                 me.loadChart(data.chartId, null, null, dashboardParams);
             }
             else {
                 drilldownFilters = dashboard.drilldownFilters;
                 drilldownParamSet = dashboard.drilldownParamSet;
                 me.loadChart(data.chartId, drilldownFilters, drilldownParamSet, dashboardParams);
             }
             break;
       case 'image':
            me._nonChartContent = new IDB.ImagePanel(me.createImageDiv(true), data, dashboard);
            break;
       case 'dashPanel':
            me.loadDashPanel();
            break;
        default:
    }
};

IDB.Dashframe.prototype.dbClicked = function() {
    var me = this, stack = me.stack, cri, request;
    if(stack.length == 1) { // bottom chart on the stack
        me.destroyChart();
        me.stack.pop();
//        removeChartBase();
//        removeChartSettings();
//        _requestInfoStack.pop();
        me.maybeRestoreNonChartContent();
//    
//        if(_nonChartContent) {
//            if(_nonChartContent is DashframeImage) {
//                updateDashframeState(DashframeState.EXTERNAL_IMAGE);
//            }
//            else if(_nonChartContent is IDashPanel) {
//                updateDashframeState(DashframeState.DASH_PANEL);
//            }                    
//        }
//        else {
//            updateDashframeState(DashframeState.EMPTY_FRAME);
//        }
    } else {
        // the top of the stack is the requestInfo for the chart that
        // was clicked for drillback. The second from the top is
        // the one that gets loaded.
        cri = stack[stack.length - 2];
        request = cri.request;
//        removeParamInputDlg();
        
//        var request:Object = requestInfo.request;
//        request['applyAlwaysPrompt'] = "false";
//        
//        //
//        // Update parameters in request.
//        //
//        if(requestInfo.currentParams != null) {
//            request['params'] = Param.buildParamValueXml(requestInfo.currentParams);
//        }
        cri.isDrillback = true;
        request.applyAlwaysPrompt = false;
        request.params = cri.currentParams;
        me.sendLoadChart(cri);
//        queueCommand(CMD.loadChart2, [request, requestInfo], onCmdChartLoaded, CommandQueue.DEFAULT_ERR_FLAGS, CommandQueue.INVOKE_CALLBACK, CommandQueue.DEFAULT_OK_FLAGS);
    }
    if (IDB.PHONEMODE)
    {
        IDB.phoneModeNavBack(me);
    }
};

IDB.Dashframe.prototype.maybeRestoreNonChartContent = function() {
  var me = this, _nonChartContent = me._nonChartContent;
  if(!_nonChartContent || _nonChartContent.isAttached) return;
  _nonChartContent.$div.appendTo(me.$div);
  _nonChartContent.resize();
};

IDB.Dashframe.prototype.updateStack = function(cri) {
    var me = this, stack = me.stack, lastRequestInfo, CLF = IDB.CLF;
    if(cri.isDrillback) {
        stack.pop();
        stack.pop();
        cri.isDrillback = false;
    }
    lastRequestInfo = this.getLastCRI();


    switch(cri.chartLoadFrom) {
        case undefined :
        case CLF.DASHBOARD_LOAD :
        case CLF.DIALOG :
        case CLF.CHART_SAVE :
            stack = this.stack = [];
            stack.push(cri);
            break;
        case CLF.DRILLDOWN :
            if(cri.originator == this) {
                stack.push(cri);
            }
            else {
                if(lastRequestInfo === null) {
                    stack.push(cri);
                }
                else if(lastRequestInfo.originator == cri.originator) {
                    // subsequent drilldown from same neighboring frame, so replace top of stack
                    stack[stack.length-1] = cri;
                }
                else {
                    // first drilldown from this neighbor, add to top of stack
                    stack.push(cri);
                }
            }
            break;
    }


};

IDB.Dashframe.prototype.getLastCRI = function() {
    var me = this, stack = me.stack;
    if(!stack || stack.length===0) return null;
    return stack[stack.length-1];
};

// chart params?, dashboard params? applyAlwaysPrompt?
IDB.Dashframe.prototype.loadChart = function(chartId, drilldownFilters, drilldownParamSet, dashboardParams) {
    var me = this, I = IDB, request, cri;

    request = I.RB.loadChart2(chartId, drilldownFilters,  drilldownParamSet, dashboardParams);
    cri = new I.CRI(me, I.CLF.DASHBOARD_LOAD, request, drilldownFilters, drilldownParamSet);
    me.sendLoadChart(cri);
};

IDB.Dashframe.prototype.sendLoadChart = function(cri) {
    var me = this, request = cri.request, CQ = IDB.CQ;
    //(cmd, payloadPlusAdtlArgs, successCallback, failCallback, callbackContext, errFlags, warnFlags, okFlags)
    CQ.enQ("loadChart2", [request, cri], me.onCmdLoadChart, me.onChartLoadFail, me, CQ.DEFAULT_ERR_FLAGS, CQ.INVOKE_CALLBACK, CQ.DEFAULT_OK_FLAGS);
};
IDB.Dashframe.prototype.onChartLoadFail = function(jqXHR, status, error) {
   IDB.MM.show("An error occurred while communicating with the server: " + error);
};

IDB.Dashframe.prototype.refreshChartData = function(valueSourceIndicator) {
    var me = this, cri = me.getLastCRI(), I = IDB, CQ = I.CQ, chartBase = me._chart,
        params = chartBase.params, dashboardParams = me.getDashboardParams(), 
        VSI = (valueSourceIndicator || chartBase.paramSettings.overrideOnDrilldown),
        request = I.RB.refreshChartData(chartBase.chartId, VSI, cri.filters,
                          cri.drilldownParamSet, dashboardParams, params);

    IDB.log(cri, "Refresh chart data stack request.");

    me.maybeStopRefresh();

    //(cmd, payloadPlusAdtlArgs, successCallback, failCallback, callbackContext, errFlags, warnFlags, okFlags)
    CQ.enQ("refreshChartData", [request, cri], me.onCmdRefreshChartData, null, me, CQ.DEFAULT_ERR_FLAGS, CQ.INVOKE_CALLBACK, CQ.DEFAULT_OK_FLAGS); 
};

IDB.Dashframe.prototype.exportChartData = function() {
    var me = this, cri = me.getLastCRI(), I = IDB, chartBase = me._chart, maxRows = chartBase.maxDataRows,
        params = chartBase.params, dashboardParams = me.getDashboardParams(), dataSet = chartBase.dataSet, 
        VSI = chartBase.paramSettings.overrideOnDrilldown,
        url = IDB.config.cmdPath + "exportChartData", request, exportAllData = chartBase.exportAllData;


        var finishExportChartData = function(result) {
            var request = I.RB.exportChartData(chartBase.chartId, result, VSI, cri.filters,
                          cri.drilldownParamSet, dashboardParams, params);
            I.ChartUtils.downloadFile(url, request);
        };

        if(!exportAllData && maxRows !== -1 && 
                     ((dataSet.numDataRows === maxRows) || dataSet.maxRowsExceeded)) {
            IDB.MM.show(IDB.format("chart.html5.export_displayed", IDB.config.maxExportDataRows), 
                        IDB.format("chart.html5.export_chart_data"),
                          {buttons:[
                              {text:IDB.format("chart.html5.displayed"),
                               click:function() {$(this).dialog("close"); 
                                                 finishExportChartData(false);}},
                              {text:IDB.format("chart.html5.all"),
                               click:function() {$(this).dialog("close");
                                                 finishExportChartData(true);}},
                              {text:IDB.format("chart.html5.cancel"),
                              click:function() {$(this).dialog("close");}}]});
        }
        else {
            finishExportChartData(exportAllData);
        }
};


IDB.Dashframe.prototype.createChartDiv = function(chartId, append) {
    var $chartDiv = $("<div data-chart-id=\"" + chartId + "\" class=\"frame-_chart\"/>");
    if(append) {$chartDiv.appendTo(this.$div);}
    return $chartDiv;
};

IDB.Dashframe.prototype.createImageDiv = function(append) {
   var me = this, $imageDiv = $("<div>");
   if(append) {
       $imageDiv.appendTo(me.$div);
   }
   return $imageDiv;
};

IDB.Dashframe.prototype.onCmdLoadChart = function(result, cri) {//, status, jqXHR) {
    IDB.log(cri, "CHART LOAD RESULT");
    var me = this, I = IDB, CLF = I.CLF, chartId, chartLoadFrom = cri.chartLoadFrom,
        isDrilldown = (chartLoadFrom === CLF.DRILLDOWN);

    me.maybeWarnMaxRows(result);
    if(isDrilldown) {
       if(this._nonChartContent) {
           IDB.log(this._nonChartContent, "HERE.");
           this._nonChartContent.$div.detach();
           IDB.log("done");
       }
    }

    me.destroyChart();
    chartId = result.chart.chartId;
    if(me.$chartDiv) {
        me.$chartDiv.detach();
    }
    me.$chartDiv = me.createChartDiv(chartId, true);
    result.chartParameterInputRequired = (result.status == 1400);
    me._chart = new IDB.ChartBase(this.$chartDiv, this, result, isDrilldown);
    me._chart.drilldown = cri.request.drilldown;
    cri.currentParams = me.getChartParams();
    me.updateStack(cri);
    if(!result.chartParameterInputRequired) {
        me.maybeStartRefresh();
    }
};


/* jshint unused:true */
IDB.Dashframe.prototype.onCmdRefreshChartData = function(result, cri, status, jqXHR) {
/* jshint unused:false */
    var me = this, chartBase = me._chart;
    me.maybeWarnMaxRows(result);

//    IDB.log(cri, "onCmdRefreshChartData(): CRI ///////////////////////");
//    IDB.log(status, "STATUS");
//    IDB.log(jqXHR, "jqXHR");

    result.chartParameterInputRequired = (result.status == 1400);

    if(result.chartParameterInputRequired) {
       me.maybeStopRefresh();
    } 
    else {
        me.maybeStartRefresh();
    }

    chartBase.refreshChartData(result);
};

IDB.Dashframe.prototype.maybeWarnMaxRows = function(result) {
    var me = this, I = IDB, CQ = I.CQ;
    if(!result || result.confirmation !== CQ.WARNING || result.status !== 1320){
       return;
    }

    IDB.log(result.status,  "maybeWarnMaxRows");
    me.autoclose(result.message, 15000);
};

IDB.Dashframe.prototype.autoclose = function(message, fadeMillis, options) {
    var me = this, pinned = false, I = IDB;
    if(fadeMillis === 0) {
         pinned = true;
         fadeMillis = 15000;
    }
    else if(!fadeMillis) {
        fadeMillis = 15000;
    }
    I.MM.autoclose(message, $.extend({pinned:pinned, fadeMillis:fadeMillis, position:I.center(me.$div), appendTo:me.dashboard.$div}, options));
};


IDB.Dashframe.prototype.destroyChart = function() {
    var me = this, $chartDiv = me.$chartDiv;
    if(!$chartDiv) {return;}
    me.maybeStopRefresh();
    me.dashboard.removeChart(this._chart);
    me._chart.destroy();
    $chartDiv.detach();
    me.$chartDiv = null;
};



IDB.Dashframe.prototype.resize = function(evt) {
    var me = this, _chart = me._chart, _nonChartContent = me._nonChartContent;
    if(evt) {
       // This could be a resize event from a floating panel contained by this frame. We don't
       // want to respond to those events here.
       if(evt.eventPhase !== Event.AT_TARGET) {
           return;
       }
    }
    if(_chart) {_chart.resize();}
    if(_nonChartContent && _nonChartContent.isAttached) {_nonChartContent.resize();}
};

IDB.Dashframe.prototype.loadDashPanel = function() {
   var me = this, dashPanelSettings = me.data.dashPanel;
   me._nonChartContent = new IDB.DashPanel(dashPanelSettings, this);
   me.resize();
};


IDB.Dashframe.prototype.datapointClicked = function(chart, dp) {
    var me = this, _chart = me._chart, linkType = _chart.linkType;
    if(linkType === null) {return;}

//    IDB.log(dp, "CLICKED: " + linkType);
    switch(linkType) {
        case "c" :
            me.drillToChart(dp);
            break;
        case "s" :
            me.drillToChartReport(dp);
            break;
        case "d" : 
            me.drillToDashboard(dp);
            break;
        case "r" : 
            me.drillToDashboardReport(dp);
            break;
        case "l" :
            me.drillToWebPage(dp);
            break;
        case "q" :
            me.changeDashboardParamsFromDrilldown(dp); 
             break;
        default :
            me.autoclose("Drilldowns of type '" + linkType + "' have not yet been implemented.", 3000);
    }
};

IDB.Dashframe.prototype.getAllFilters = function(dp, dataSet, sendC) {
    // build the set of drilldown filters. Start with the filters
    // that lead up to this point, which are the ones on top of the requestInfoStack.
    var me = this, _chart = me._chart, filters = [], stack = this.stack,
        dataSet = dataSet || _chart.dataSet,
        pivotSelectionFilters = dataSet.getPivotSelectionFilters(),
        axisFilters = [], axisList = dataSet.axisList,
        dataRow = dp.dataRow, axis, axisName, val, formattedVal,
        sendC = !IDB.isUndef(sendC)?sendC:_chart.sendC;

    if(stack.length > 0) {
        filters = stack[stack.length - 1].filters.concat();
    } 

    // now overlay the pivot selections, if any, for this chart.
    filters = IDB.Filter.overlayFilterSets(filters, pivotSelectionFilters);

    if(!dataRow) {
        throw new Error("dataRow is null on IDatapoint.");
    }
    for(var j=0, jj=axisList.length; j<jj; j++) {
        axis = axisList[j];
        if(axis.isPivot) continue; // already handled.
        if(!axis.sendOnDrilldown) continue;
        axisName = axis.axisName;
        val = dataRow[j].stringValue;
        formattedVal = dataRow[j].fv;
//        IDB.log("ChartView.getAllFilters(): Adding filter " + axisName + "=" + val);
        axisFilters.push(new IDB.Filter(axisName, val, formattedVal));
    }

    if(sendC) {
        axis = dp.yAxis;
        if(!axis.isPivot && !axis.sendOnDrilldown) {
            axisFilters.push(new IDB.Filter(axis.axisName, dp.yValue.stringValue, dp.yValue.fv));
        }
        else {
            IDB.log("ChartView.getAllFilters(): Skipping axis " + axis);
        }
    }

    // now overlay the filter built from the clicked-on data point.
    filters = IDB.Filter.overlayFilterSets(filters, axisFilters);
    return filters;
};

IDB.Dashframe.prototype.getDrilldownParamSet = function() {
    // build the set of drilldown source parameters. Start with the parameters
    // that lead up to this point, which are the ones on top of the requestInfoStack.
    var me = this, dps = null,
        stack = this.stack,
        _chart = me._chart, chartParams, dashboardParams, Param = IDB.Param;

    if(stack.length > 0) {
        dps = stack[stack.length - 1].drilldownParamSet;

        if(dps) {
            dps = {chartDrilldownParams:dps.chartDrilldownParams.concat(), 
                   dashboardDrilldownParams:dps.dashboardDrilldownParams.concat()};
        }
    }

    if(!dps) {
        dps = {chartDrilldownParams:[], dashboardDrilldownParams:[]};  
    }

    // now overlay the parameters if any, from this chart if the params are to be sent to drilldowns.
    if(_chart.paramSettings.sendToDrilldown) {
        chartParams = me.getChartParams();
        if(chartParams) {
            dps.chartDrilldownParams = Param.overlayParamSets(dps.chartDrilldownParams, chartParams);
        }

        dashboardParams = me.getDashboardParams();
        if(dashboardParams) {
            dps.dashboardDrilldownParams = Param.overlayParamSets(dps.dashboardDrilldownParams, dashboardParams);
        }
    }

    return dps;
};

IDB.Dashframe.prototype.getChartParams = function() {
    return this._chart.params;
};

IDB.Dashframe.prototype.getDashboardParams = function() {
    return this.dashboard.dashboardParams;
};

IDB.Dashframe.prototype.getDashboardFilters = function() {
    return this.dashboard.filters;
};

IDB.Dashframe.prototype.getDashboardDrilldownParamSet = function() {
    return this.dashboard.drilldownParamSet;
};

IDB.Dashframe.prototype.drillToDashboard = function(dp) {
     var me = this, 
         chart = me._chart,
         dashId = chart.linkId,
         filters = me.getAllFilters(dp),
         drilldownParamSet = me.getDrilldownParamSet();
     me.dashboard.drillToDashboard(me, dashId, filters, drilldownParamSet);
};

IDB.Dashframe.prototype.launchDashboard = function(dashId, sendParameters) {
    var me = this, dashboard = me.dashboard;
    dashboard.launchDashboard(me, dashId, sendParameters ? me.getDashboardParams() : []);
};

IDB.Dashframe.prototype.drillToChart = function(dp) {
    var me = this, chart = me._chart, tag = chart.linkTarget,
        targetChartId = chart.linkId, filters = me.getAllFilters(dp),
        drilldownParamSet = me.getDrilldownParamSet(),
        dashboardParams = me.getDashboardParams();

    // If there is no frame tag for this drilldown, then it will happen in
    // this and only this frame.
    if(!tag) {
        me.loadChartFromDrilldown(me, targetChartId, filters, drilldownParamSet, dashboardParams);
    }
    else {
//        // otherwise let the _dashframe handle it. This call may result in a call back to this
//        // controller's maybeLoadChartFromDrilldown method.
        me.dashboard.drilldownToFramesWithTag(me, tag, targetChartId, filters, drilldownParamSet, dashboardParams);
    }
};



IDB.Dashframe.prototype.drillToWebPage = function(dp) { //(dp:IDatapoint):void {
    var me = this, chart = me._chart, url = chart.linkUrl,
        settings = chart.linkUrlSettings, target = chart.linkTarget;
    if(!url) return;
    try {
        var isIframeUrl = (url.toLowerCase().indexOf("iframe:") == 0);
        if(isIframeUrl) {
            url = url.replace(/^iframe:/,"");
        }

        url = chart.expandMacros(url, 'url', dp);
        if(!target) target = "_blank";
        var win = window.open(url, target, settings);
        if(win && target !== "_blank") {
            win.focus();
        }
    } catch(err) {
       IDB.MM.show("An error occurred while launching the external web page:\n" + err.stack);
    }

};


IDB.Dashframe.prototype.changeDashboardParamsFromDrilldown = function(dp) {
    var me = this, filters,
        dashboardParams = me.getDashboardParams(); 

    if(!dashboardParams || dashboardParams.length == 0) {
        me.autoclose(IDB.format("param.html5.no_dash_parameters", 4000));
        return;
    }

    IDB.phoneModeParamFrameId = null;
    filters = me.getAllFilters(dp);
    IDB.Param.setParameterValuesFromFilters(dashboardParams, filters);
    me.dashboard.dashboardParameterValuesChanged();
    if (IDB.PHONEMODE)
    {
        IDB.phoneModeNavToParams();
    }
};



IDB.Dashframe.prototype.loadChartFromDrilldown = function(originator, chartId, filters, drilldownParamSet, dashboardParams) {
   var me = this, request, stack = me.stack, dataSet, ri, cri, I = IDB;
   
   me.maybeStopRefresh();

   request = IDB.RB.loadChart2(chartId, filters, drilldownParamSet, dashboardParams, null, true);
   IDB.log(stack, "STACK ------------------------------------------");
   
   if(stack.length > 0)  {
        dataSet = me._chart.dataSet;
        ri = stack[stack.length-1];
        // save the current pivot selections on the current ChartRequestInfo object
        // so we can restore them on a drillback.
        if(dataSet.hasPivots && dataSet.numDataRows > 0) {
            ri.firstPivotAxisName = dataSet.getFirstPivotAxisName();
            ri.firstPivotSelection = dataSet.getFirstPivotSelection();
            ri.allPivotSelections = dataSet.getPivotSelectionMap();
        }

        ri.currentParams = me.getChartParams();
   }

   cri = new I.CRI(originator, I.CLF.DRILLDOWN, request, filters, drilldownParamSet);
   me.sendLoadChart(cri);

            
//            var xml:XML = PacketBuilder.loadChart2(targetChartId, filters, drilldownParamSet, dashboardParams, null, true);
//            var cri:ChartRequestInfo = new ChartRequestInfo(originator, ChartLoadedFrom.DRILLDOWN, xml, filters, drilldownParamSet);
//            queueCommand(CMD.loadChart2, [xml, cri], onCmdChartLoaded, CommandQueue.DEFAULT_ERR_FLAGS, CommandQueue.INVOKE_CALLBACK, CommandQueue.DEFAULT_OK_FLAGS);

};

IDB.Dashframe.prototype.dashboardParameterValuesChanged = function() {
    var me = this, dashValueSourceIndicator,
            chartBase = me._chart;

    IDB.log(chartBase, "dashboardParameterValuesChanged");

    if(chartBase) {
        if(!chartBase.params || !chartBase.params.length) {
            return;
        }

        if(!IDB.Param.applicableDashboardParams(chartBase.params, me.getDashboardParams())) {
            return;
        }

        //
        // Adjust the valueSourceIndicator to give precedence to the dashboard parameters.
        //
        dashValueSourceIndicator = chartBase.paramSettings.overrideOnDrilldown.substring(1);
        dashValueSourceIndicator = "D" + dashValueSourceIndicator.replace("D", "T");

        me.refreshChartData(dashValueSourceIndicator);
        if (IDB.phoneModeParamFrameId === null)
        {
            IDB.phoneModeParamFrameId = me.frameId;
        }
        return;
    }

    if((me._nonChartContent instanceof IDB.DashPanel) && (me._nonChartContent.panelCode == "PARAM")) {
          me._nonChartContent.update();
    } 
};

IDB.Dashframe.prototype.getMsgPos = function() {
    return IDB.center(this.$div);
};

IDB.Dashframe.prototype.canDrilldownToReport = function() {
    if(!IDB.userdata.reportsAvailable) {
       IDB.MM.show(IDB.format("reports.html5.no_drill_to_report"));
       return false;
    }
    else {
       return true;
    }
};


IDB.Dashframe.prototype.drillToChartReport = function(dp) {
    var me = this, queryString, request, 
        I = IDB, CQ = I.CQ, chart = me._chart, targetChartId = chart.linkId;

    if(targetChartId < 1) return;

    if(!me.canDrilldownToReport()) return;
   
    if(!chart.linkReportUrl) return;

    queryString = me.buildDrillToReportQueryString(dp);
    request = I.RB.getToken(targetChartId, "chart", queryString, true);
    CQ.enQ("getToken", [request, targetChartId, dp, queryString], me.finishDrillToChartReport, null, me, CQ.DEFAULT_ERR_FLAGS, CQ.INVOKE_CALLBACK, CQ.DEFAULT_OK_FLAGS); 
};

IDB.Dashframe.prototype.finishDrillToChartReport = function(datapacket, chartId, dp, queryString) {
    var me = this, ddParams, hasParamsRequiringInput, url, target, settings, chart = me._chart;

    ddParams = "dtk=" + datapacket.token;
    
    if(queryString) {
       ddParams = ddParams + '&' + queryString;
    }  

    hasParamsRequiringInput = !!datapacket.isInputRequired;
    url = chart.linkReportUrl;
    url = url.replace("$DASH_ID$", IDB.currentDashboard.data.dashId);
    
    if(url.indexOf("?") < 0) {
       url = url + "?" + ddParams;
    }
    else {
       url = url + "&" + ddParams;
    }

    target = "_blank";
    settings = null;

    if(!chart.linkReportInline) {
        if(!hasParamsRequiringInput) {
            target = IDB.getReportWindowName();
            settings = IDB.getReportWindowSettings();
        }
    }

    var win = window.open(url, target, settings);
    if(win && target !== "_blank") {
        win.focus();
    }
};

IDB.Dashframe.prototype.buildDrillToReportQueryString = function(dp) {
    var me = this, j, jj, ddParams, chart = me._chart, filters = me.getAllFilters(dp),
      drilldownParamSet = me.getDrilldownParamSet(), param,
      chartDrilldownParams = drilldownParamSet.chartDrilldownParams,
      dashboardDrilldownParams = drilldownParamSet.dashboardDrilldownParams,
      dashboardParams = me.getDashboardParams();
    
    ddParams = "aap=true";

    for(j=0, jj = filters.length; j < jj; j++) {
        ddParams += ("&" + "fn" + j + "=" + encodeURIComponent(filters[j].axisName));
        ddParams += ("&" + "fv" + j + "=" + encodeURIComponent(filters[j].value));
    }

    for(j=0, jj = chartDrilldownParams.length; j < jj; j++) {
        param = chartDrilldownParams[j];
        ddParams += "&" + IDB.Param.toValueRequestParameters(param, "s", j, true);
    }

    for(j=0, jj = dashboardDrilldownParams.length; j < jj; j++) {
        param = dashboardDrilldownParams[j];
        ddParams += "&" + IDB.Param.toValueRequestParameters(param, "r", j, true);
    }

    return ddParams;
};   

IDB.Dashframe.prototype.drillToDashboardReport = function(dp) {
    var me = this, queryString, request, 
        I = IDB, CQ = I.CQ, chart = me._chart, targetDashId = chart.linkId;

    if(targetDashId < 1) return;
   
    if(!chart.linkReportUrl) return;

    if(!me.canDrilldownToReport()) return;

    queryString = me.buildDrillToReportQueryString(dp);
    request = I.RB.getToken(targetDashId, "dashboard", queryString, true);
    CQ.enQ("getToken", [request, targetDashId, dp, queryString], me.finishDrillToDashboardReport, null, me, CQ.DEFAULT_ERR_FLAGS, CQ.INVOKE_CALLBACK, CQ.DEFAULT_OK_FLAGS); 
};

IDB.Dashframe.prototype.finishDrillToDashboardReport = function(datapacket, dashId, dp, queryString) {
    var me = this, ddParams, hasParamsRequiringInput, url, target, settings, chart = me._chart;

    ddParams = "dtk=" + datapacket.token;
    
    if(queryString) {
       ddParams = ddParams + '&' + queryString;
    }  

    hasParamsRequiringInput = !!datapacket.isInputRequired;
    url = chart.linkReportUrl;
    
    if(url.indexOf("?") < 0) {
       url = url + "?" + ddParams;
    }
    else {
       url = url + "&" + ddParams;
    }

    target = "_blank";
    settings = null;

    if(!chart.linkReportInline) {
        if(!hasParamsRequiringInput) {
            target = IDB.getReportWindowName();
            settings = IDB.getReportWindowSettings();
        }
    }

    var win = window.open(url, target, settings);
    if(win && target !== "_blank") {
        win.focus();
    }

};
IDB.Dashframe.prototype.buildLoadChartReportURL = function(format, token)
{            
    //
    // All parameters should be resolved at this point. Only need filters and current parameter values to generate the data.
    //				
    var chart = this._chart;
    var requestParameters = "";

    var req = this.getLastCRI();	
    if (req)
    {
        var drilldownFilters = req.filters;

        var prefix = IDB.Param.SOURCE_FILTERS.toLowerCase();

        for(var j = 0; drilldownFilters !== null && j < drilldownFilters.length; j++)
        {
            if (requestParameters.length > 0) requestParameters += "&";
            requestParameters += drilldownFilters[j].toValueRequestParameters(prefix, j);
        }
    }

    if (token)
    {
        if (requestParameters.length > 0)
        {
            requestParameters += "&";
        } 
        requestParameters += "dtk=" + token;    
    }

    var chartRequestParameters = this.buildReportRequestParameters("chart");

    if (requestParameters.length > 0 && chartRequestParameters.length > 0) requestParameters += "&";

    requestParameters += chartRequestParameters;

    var url = IDB.ReportUtils.getChartReportUrl(this.dashboard.data.dashId, chart.chartId, format, true); 

    if (requestParameters.length > 0)
    {
        if (url.indexOf("?") < 0)
        {
            url = url + "?" + requestParameters;
        }
        else
        {
            url = url + "&" + requestParameters;
        }
    }
    return url;
};	

IDB.Dashframe.prototype.maybeStartRefresh = function() { 
    var me = this, chart = me._chart;

    if(!chart) {
        return;
    }

    if(chart.refreshMinutes === 0) { //deal with on server for static charts, canAutoRefreshChart
        return;
    }

    me.maybeStopRefresh();

    var delay = chart.refreshMinutes * 60 * 1000;
    me.refreshTimer = window.setInterval(IDB.listener(me, "refreshChartData"), delay);
};


IDB.Dashframe.prototype.maybeStopRefresh = function() { 
    var me = this;

    if(!IDB.isNil(me.refreshTimer)) {
        window.clearInterval(me.refreshTimer);
        me.refreshTimer = null;
    }
};

Object.defineProperty(IDB.Dashframe.prototype, "containsChart", {
  get: function() {
      return !!this._chart;
  }
}); 

IDB.Dashframe.prototype.reload = function(initial) {
    var me = this, stack = me.stack, cri, request;
    initial = !IDB.isNil(initial) || false;

    //
    // We want the initial contents of the frame.
    //
    if(initial) {
        //
        // Obtain in the initial request. If it isn't from a drilldown, reload it. If it is from a drilldown,
        // reload the frame from the original config that contains image or panel settings.
        //
        if(stack.length > 0) {
            cri = stack[0];                    
            request = cri.request;
            request.applyAlwaysPrompt = false;                                        
            request.params = IDB.Param.toParamValues(cri.currentParams, true);
            stack.length = 0;
            if(cri.chartLoadFrom !== IDB.CLF.DRILLDOWN) {
                me.sendLoadChart(cri);
            }
            else {
                me.destroyChart();
                me.loadContent(me.data);
            }
         }
         //
         // The frame doesn't contain a chart request, so just reload it (primarily for image refresh).
         //
         else {
             me.loadContent(me.data);
         }
      }
      //
      // We want a refresh of what is currently loaded in the frame.
      //
      else {
         //
         // If the current contents is a chart, reload it with the 'applyAlwaysPrompt' set to false so we don't
         // prompt on refresh.
         //
        if(stack.length > 0) {
            cri = stack[stack.length-1];                    
            request = cri.request;
            request.applyAlwaysPrompt = false;  
            request.params = IDB.Param.toParamValues(cri.currentParams, true);
            stack.pop();
            me.sendLoadChart(cri);
        }
        else {
            me.destroyChart();
            me.loadContent(me.data);
        }
     }
};


IDB.Dashframe.prototype.destroy = function() {
    this.destroyChart();
};

IDB.Dashframe.prototype.buildReportRequestParameters = function(reportType)
{

    var requestParameters = "";

    var params = null;

    //
    // For dashboard reports, use the parameters for the chart initially loaded in the frame.
    // For chart reports, use the parameters of the chart currently loaded in the frame.
    //
    if (reportType === "dashboard")
    {
        params = this.getInitialChartParams();
    }
    else
    {
        params = this.getChartParams();
    }

    var prefix = IDB.Param.TARGET_PARAMS.toLowerCase();

    for(var j =0; params !== null && j<params.length; j++) {

        var param = params[j];

        if (!IDB.Param.valueIsNothingForType(param.value, param.dataType)) {

            var chartPrefix = prefix;

            if (reportType === "chart")
            {
                chartPrefix += 0;
            }
            else
            {
                chartPrefix += this.frameId;
            }

            if (requestParameters.length > 0) requestParameters += "&";

            requestParameters += IDB.Param.toValueRequestParameters(param, chartPrefix, j, false);
        }
    }
    return requestParameters;
};

IDB.Dashframe.prototype.getInitialChartParams = function()
{
    var params = null;
    if (this.stack.length > 1) {
        params = this.stack[0].currentParams;
    }
    else
    {
        params = this.getChartParams();
    }
    return params;
};
//////////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////////// 
// IDB.DashLauncherDashPanel

IDB.DashLauncherDashPanel = function(panelSettings, dashframe) {
    var me = this, dashIdList, headerText, span, $dashListTable;
    me.panelSettings = panelSettings;
    me.panelProperties = panelSettings.panelProperties;
    me.dashframe = dashframe;
    dashIdList = me.panelProperties.dashIdList || [];
    
    me.$div = $("<div>").appendTo(dashframe.$div);
    me.$contentDiv = $("<div>").appendTo(me.$div).css({position:"absolute"}); 
    me.$headerDiv = $("<div>").css({"margin":"auto"}).appendTo(me.$contentDiv);
    headerText = (me.panelProperties.headerText || "").replace(/\r\n/g, '\n');
    span = $("<span>").appendTo(me.$headerDiv);
    span.text(headerText);

    me.$dashListDiv = $("<div>").css({"overflow-x":"hidden", "overflow-y":"auto", "margin":"auto"}).appendTo(me.$contentDiv);
    $dashListTable = me.$dashListTable = $("<table>").css({"borderCollapse":"collapse", "margin":"auto"}).appendTo(me.$dashListDiv);
    
    dashIdList.forEach(function(v) {
        var $link = $("<td>" + v.dashname + "</td>").appendTo($("<tr>").appendTo($dashListTable)); 
        $link.on("click", function() {
                            dashframe.launchDashboard(v.dashId, Boolean(me.panelProperties.sendParams));
                          });
        $link.addClass("dashlauncher-dash-link");
    });

    me.setStylesFromSettings();
    me.headerWidth = span.width();
    if(headerText.length > 0) {
        me.$headerDiv.css({"paddingBottom":"4px","wordWrap":"break-word"}); 
    }

    me.size();
    me.position();
};

IDB.DashLauncherDashPanel.prototype.resize = function() {
    this.size();
    this.position();
};

IDB.DashLauncherDashPanel.prototype.position = function() {
   var me = this,
       top = me.padding.top + (me.$div.height() - me.$contentDiv.height())/2 + "px",
       left = me.padding.left + (me.$div.width() - me.$contentDiv.width())/2 + "px";

   me.$contentDiv.css({top:top, left:left});
};

IDB.DashLauncherDashPanel.prototype.size = function() {
    var me = this, dashListHeight, dashListWidth, headerWidth,
        height = me.dashframe.$div.height() - me.padding.top - me.padding.bottom,
        width = me.dashframe.$div.width() - me.padding.left - me.padding.right; 

    me.$div.height(height);
    me.$div.width(width);
    me.$contentDiv.width(width);

    dashListHeight = height - me.$headerDiv.height() - 16;
    me.$dashListDiv.css({"maxHeight":dashListHeight});

    $(".dashlauncher-dash-link", me.$dashListTable).css({"max-width":width-4});
};

IDB.DashLauncherDashPanel.prototype.setStylesFromSettings = function() {
    var me = this, 
        panelProperties = me.panelProperties,
        divStyles = {backgroundColor:IDB.toRGBA(panelProperties.backgroundColor.i, panelProperties.backgroundAlpha)},

        headerStyles = {},
        dashListStyles = {},
        padding = me.padding = {top:0, bottom:0, left:0, right:0};

    headerStyles.fontSize = panelProperties.headerSize + "px";
    headerStyles.color = panelProperties.headerColor.css;
    headerStyles.textAlign = panelProperties.headerAlign;
    dashListStyles.fontSize = panelProperties.textSize + "px";
    dashListStyles.color = panelProperties.textColor.css;
    dashListStyles.textAlign = panelProperties.textAlign;

    padding.left = panelProperties.margins.left;
    divStyles.paddingLeft = padding.left + "px";

    padding.right = panelProperties.margins.right;
    divStyles.paddingRight = padding.right + "px";

    padding.top = panelProperties.margins.top;
    divStyles.paddingTop = padding.top + "px";

    padding.bottom = panelProperties.margins.bottom;
    divStyles.paddingBottom = padding.bottom + "px";

    me.$headerDiv.css(headerStyles);
    me.$dashListDiv.css(dashListStyles);
    me.$div.css(divStyles);
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.DashPanel

IDB.DashPanel = function(panelSettings, dashframe) {
    var me = this,
        panelCode = me.panelCode = panelSettings.panelCode;

    me.panelSettings = panelSettings;
    me.dashframe = dashframe;
    me.dashPanel = null;

    switch(panelCode) {
        case "PARAM":  
           me.dashPanel = new IDB.ParamDashPanel(panelSettings, dashframe);
           break;
        case "TEXT":  
           me.dashPanel = new IDB.TextDashPanel(panelSettings, dashframe);
           break;
        case "HTML":  
           me.dashPanel = new IDB.HTMLDashPanel(panelSettings, dashframe);
           break;
        case "VIDEO":
           me.dashPanel = new IDB.ErrorDashPanel(panelSettings, IDB.format("dashpanel.html5.video_not_supported"), dashframe);
           break;
        case "PANELAPP":
           me.dashPanel = new IDB.ErrorDashPanel(panelSettings, IDB.format("dashpanel.html5.panelapp_not_supported"), dashframe);
           break;
        case "DASHLAUNCHER":
           me.dashPanel = new IDB.DashLauncherDashPanel(panelSettings, dashframe);
           break;
        default:
           me.dashPanel = new IDB.ErrorDashPanel(panelSettings, IDB.format("dashpanel.html5.panel_not_supported"), dashframe);
           break;
    }
};
Object.defineProperties(IDB.DashPanel.prototype, {
    $div:{
        get:function() {
            var me = this, dp = me.dashPanel, div = dp ? dp.$div : null;
            return div;
        }
    },
    isAttached:{
        get:function() {
            var me = this, dp = me.dashPanel, div = dp ? dp.$div : null;
            if(!div) return false;
            return(div.parent().length > 0);
        }
    }
});

IDB.DashPanel.prototype.resize = function() {
    var me = this;
    if(me.dashPanel && me.dashPanel.resize) {
        me.dashPanel.resize();
    }
};

IDB.DashPanel.prototype.update = function() {
    var me = this;
    if(me.dashPanel && me.dashPanel.update) {
        me.dashPanel.update();
    }
};
//////////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////////// 
// IDB.ErrorDashPanel

IDB.ErrorDashPanel = function(panelSettings, errorMessage, dashframe) {
    var me = this;

    me.panelSettings = panelSettings;
    me.panelProperties = panelSettings.panelProperties;
    me.dashframe = dashframe;
    me.$div = $("<div>").appendTo(dashframe.$div);
    me.$div.css({"height":"100%", "width":"100%"});
    me.$div.addClass("idb-dashpanel");
    me.$textDiv = $("<div>").appendTo(me.$div);
    me.$textDiv.text(errorMessage);

    me.$textDiv.css({wordWrap:"break-word", whiteSpace:"pre-wrap", textAlign:"center"});

    me.setStylesFromSettings();
    me.$div.css({"overflow":"hidden"});
    me.$textDiv.position({
       my:"center",
       at:"center",
       of:me.$div});
};

IDB.ErrorDashPanel.prototype.setStylesFromSettings = function() {
    var me = this,
        textStyles = {},
        divStyles = {},
        panelProperties = me.panelProperties;

    textStyles.color = me.panelSettings.textColor.css; 
    divStyles.backgroundColor = IDB.toRGBA(panelProperties.backgroundColor.i, panelProperties.backgroundAlpha);

    me.$textDiv.css(textStyles);
    me.$div.css(divStyles);
};

IDB.ErrorDashPanel.prototype.resize = function() {
};

//////////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////////// 
// IDB.HTMLDashPanel

IDB.HTMLDashPanel = function(panelSettings, dashframe) {
    var me = this, html;

    me.panelSettings = panelSettings;
    me.panelProperties = panelSettings.panelProperties;
    me.dashframe = dashframe;
    me.$div = $("<div>").appendTo(dashframe.$div);
    me.$containerDiv = $("<div>").css({"overflow-x":"hidden"}).appendTo(me.$div);
    me.$div.addClass("idb-dashpanel");

    html = me.panelProperties.htmlText;
    html = html.replace(/\r\n/g, '\n');
    html = IDB.ChartUtils.expandMacros(html, "html", IDB.userdata.username, IDB.userdata.baseUrl);
    var $htmlDiv = me.$htmlDiv = $("<div>").html(html);

    //
    // Remove any JavaScript before adding the div to the DOM.
    //
    IDB.HtmlUtils.removeScript($htmlDiv);

    //
    // If the HTML is 'flashified' HTML it needs to be standardized for browser presentation.
    //
    if(IDB.HtmlUtils.isFlashHtml($htmlDiv)) {
        IDB.HtmlUtils.toStandardHtml($htmlDiv);
    }

    $htmlDiv.appendTo(me.$containerDiv);
    me.setStylesFromSettings();
    me.size();
};


IDB.HTMLDashPanel.prototype.size = function() {
    var me = this,
        height = me.dashframe.$div.height() - me.padding.top - me.padding.bottom,
        width = me.dashframe.$div.width();

    me.$div.height(height);
    me.$containerDiv.height(height);
    me.$containerDiv.width(width);
};

IDB.HTMLDashPanel.prototype.setStylesFromSettings = function() {
    var me = this,
        panelProperties = me.panelProperties,
        divStyles = {},
        htmlStyles = {},
        containerStyles = {},
        padding = me.padding = {top:0, bottom:0, left:0, right:0};

    divStyles.backgroundColor = IDB.toRGBA(panelProperties.backgroundColor.i, panelProperties.backgroundAlpha);
    padding.left = panelProperties.margins.left;
    htmlStyles.paddingLeft = padding.left + "px";

    padding.right = panelProperties.margins.right;
    htmlStyles.paddingRight = padding.right + "px";

    padding.top = panelProperties.margins.top;
    divStyles.paddingTop = padding.top + "px";

    padding.bottom = panelProperties.margins.bottom;
    divStyles.paddingBottom = padding.bottom + "px";

    // "ltr" is default
    if(panelProperties.advancedTextRendering && panelProperties.rightToLeftText) {
        htmlStyles.direction = "rtl";
        htmlStyles["unicode-bidi"] = "bidi-override";
    }

    if(panelProperties.preventScrollbars) {
        containerStyles.overflowY = "hidden";
    }
    else {
        containerStyles.overflowY = "auto";
    }

    me.$htmlDiv.css(htmlStyles);
    me.$containerDiv.css(containerStyles);
    me.$div.css(divStyles);
};

IDB.HTMLDashPanel.prototype.resize = function() {
    this.size();
};

//////////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////////// 
// IDB.ParamDashPanel

IDB.ParamDashPanel = function(panelSettings, dashframe) {
    var me = this, callbacks,
        params = dashframe.dashboard.dashboardParams;

    me.panelSettings = panelSettings;
    me.panelProperties = panelSettings.panelProperties;
    me.dashframe = dashframe;
    me.paramInputPanel = null;
    me.$div = $("<div>").css({width:"100%", height:"100%"}).addClass("idb-dashpanel").appendTo(dashframe.$div);

    if(params && params.length > 0) {
        callbacks = {paramValuesChanged:IDB.listener(dashframe.dashboard,"dashboardParameterValuesChanged")};
        me.paramInputPanel = new IDB.ParamInputPanel(me.$div, 
                                   {params:params, direction:me.panelProperties.direction}, 
                                   callbacks);
    }
    else {
       me.$msgDiv = $("<div>").css({"textAlign":"center", "overflow":"hidden"}).appendTo(me.$div);
       me.$msgDiv.text(IDB.format("dashpanel.html5.no_dashboard_params"));
    }

    me.setStylesFromSettings();
};

IDB.ParamDashPanel.prototype.resize = function() {
    var me = this;

    if(me.paramInputPanel) {
        me.paramInputPanel.resize();
    }
    me.position();
};

IDB.ParamDashPanel.prototype.position = function() {
    var me = this,
        alignmentStyles = {};

    if(me.paramInputPanel) {
        alignmentStyles = {};
        //
        // The paramInputPanel handles center alignment itself.
        //
        switch(me.alignment.horizontal) {
            case "left":
                alignmentStyles.left = "0px";
                alignmentStyles.right = "";
                break;
            case "right":
                alignmentStyles.right = "0px";
                alignmentStyles.left = "";
                break;
        }

        switch(me.alignment.vertical) {
            case "top":
                alignmentStyles.top = "0px";
                alignmentStyles.bottom = "";
                break;
            case "bottom":
                alignmentStyles.bottom = "0px";
                alignmentStyles.top = "";
                break;
        }
        
        me.paramInputPanel.$div.css(alignmentStyles);
    }
    else if(me.$msgDiv) {
        me.$msgDiv.position({
        my:"center",
        at:"center",
        of:me.$div});
    } 
};

IDB.ParamDashPanel.prototype.setStylesFromSettings = function() {
    var me = this, 
        panelProperties = me.panelProperties,
        alignment = me.alignment = {},
        divStyles = {};

    divStyles.color = panelProperties.textColor.css;
    divStyles.backgroundColor = IDB.toRGBA(panelProperties.backgroundColor.i, panelProperties.backgroundAlpha);
    alignment.vertical = panelProperties.verticalAlignment;
    alignment.horizontal = panelProperties.horizontalAlignment;

    me.$div.css(divStyles);
};

IDB.ParamDashPanel.prototype.update = function() {
        this.paramInputPanel.update();
};

//////////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////////// 
// IDB.TextDashPanel

IDB.TextDashPanel = function(panelSettings, dashframe) {
    var me = this, text;

    me.panelSettings = panelSettings;
    me.panelProperties = panelSettings.panelProperties;
    me.dashframe = dashframe;
    me.$div = $("<div>").appendTo(dashframe.$div);
    me.$containerDiv = $("<div>").css({"overflow-x":"hidden"}).appendTo(me.$div);
    me.$textDiv = $("<div>").appendTo(me.$containerDiv);
//    me.$textDiv.css({border:"solid #FF0000"}); 

    text = me.panelProperties.text;
    text = IDB.StringUtils.rtrim(text);
    text = text.replace(/\r\n/g, '\n');
    text = IDB.ChartUtils.expandMacros(text, "html", IDB.userdata.username, IDB.userdata.baseUrl);
    me.$textDiv.text(text);

    me.$textDiv.css({wordWrap:"break-word", whiteSpace:"pre-wrap"});

    me.setStylesFromSettings();
    me.size();
};

IDB.TextDashPanel.prototype.size = function() {
    var me = this,
        height = me.dashframe.$div.height() - me.padding.top - me.padding.bottom,
        width = me.dashframe.$div.width();

    me.$div.height(height);
    me.$containerDiv.height(height);
    me.$containerDiv.width(width);
};

IDB.TextDashPanel.prototype.resize = function() {
    this.size();
};

IDB.TextDashPanel.prototype.setStylesFromSettings = function() {
    var me = this,
        panelProperties = me.panelProperties,
        textStyles = {},
        divStyles = {},
        containerStyles = {},
        padding = me.padding = {top:0, bottom:0, left:0, right:0};

    divStyles.backgroundColor = IDB.toRGBA(panelProperties.backgroundColor.i, panelProperties.backgroundAlpha);

    textStyles.fontSize = panelProperties.textSize;
    textStyles.color = panelProperties.textColor.css; 

    padding.left = panelProperties.margins.left;
    textStyles.paddingLeft = padding.left + "px";

    padding.right = panelProperties.margins.right;
    textStyles.paddingRight = padding.right + "px";

    padding.top = panelProperties.margins.top;
    divStyles.paddingTop = padding.top + "px";

    padding.bottom = panelProperties.margins.bottom;
    divStyles.paddingBottom = padding.bottom + "px";

    textStyles.textAlign = panelProperties.textAlign;

    // "ltr" is default
    if(panelProperties.advancedTextRendering && panelProperties.rightToLeftText) {
        textStyles.direction = "rtl";
        textStyles["unicode-bidi"] = "bidi-override";
    }

    if(panelProperties.preventScrollbars) {
        containerStyles.overflowY = "hidden";
    }
    else {
        containerStyles.overflowY = "auto";
    }

    me.$textDiv.css(textStyles);
    me.$containerDiv.css(containerStyles);
    me.$div.css(divStyles);
};


/*====================================================================================================*/
IDB.GraphFactory = {};

IDB.GraphFactory.AllModules = [ //order this list by pre-loading priority
	'Shared',
	'Cartesian',
	'Gauge',
	'Report',
	'Linear',
	'Bar',
	'Line',
	'Area',
	'Pie',
	'Metrics',
	'Ticker',
	'Calendar',
	'Cartesian3d',
	'Bar3d',
	'Line3d',
	'Area3d',
	'Bubble',
	'Combined',
	'Gallery',
	'Imageplot',
	'Pareto',
	'Placard',
	'Pyramid',
	'Slideshow',
	'Spark',
	'BarDist',
	'Stoplight',
	'IG',
	'Treemap',
    'Geomap',
	'Temp'
];


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphFactory.buildGraph = function(chart, $div, chartSettings, dataSet) {
	var graphSett = new IDB.GraphSettings(chartSettings);

	switch ( chartSettings.graphTypeCode ) {
		case 'Area':
		case 'AreaSmooth':
		case 'AreaStacked':
		case 'AreaRatio':
			return new IDB.AreaGraph(chart, $div, graphSett, dataSet);

		case 'Area3d':
		case 'Area3dStacked':
		case 'Area3dRatio':
			return new IDB.Area3dGraph(chart, $div, graphSett, dataSet);

		case 'Bar':
		case 'BarStacked':
		case 'BarRatio':
		case 'Column':
		case 'ColumnStacked':
		case 'ColumnRatio':
			return new IDB.BarGraph(chart, $div, graphSett, dataSet);
			
		case 'Column3d':
		case 'Column3dStacked':
		case 'Column3dRatio':
		case 'Cylinder3d':
		case 'Cylinder3dStacked':
		case 'Cylinder3dRatio':
			return new IDB.Bar3dGraph(chart, $div, graphSett, dataSet);

		case 'BarDist':
			return new IDB.BarDistGraph(chart, $div, graphSett, dataSet);
			
		case 'Bubble':
			return new IDB.BubbleGraph(chart, $div, graphSett, dataSet);

		case 'CalendarMonthly':
		case 'CalendarMonthlyEvent':
		case 'CalendarMultiday':
		case 'CalendarDaily':
			return new IDB.CalendarGraph(chart, $div, graphSett, dataSet);

		case 'Combined':
		case 'CombinedStacked':
			return new IDB.CombinedGraph(chart, $div, graphSett, dataSet);

		case 'Gallery':
			return new IDB.GalleryGraph(chart, $div, graphSett, dataSet);

		case 'GaugeFull':
		case 'GaugeFullOdom':
		case 'GaugeSquare':
		case 'GaugeSquareOdom':
		case 'GaugeHalf':
		case 'GaugeHalfOdom':
		case 'GaugeCustom':
		case 'GaugeTarget':
		case 'GaugeCluster2':
		case 'GaugeCluster3':
		case 'GaugeCluster4':
		case 'GaugeMultiNeedle':
			return new IDB.GaugeGraph(chart, $div, graphSett, dataSet);

		case 'ImagePlot':
			return new IDB.ImagePlotGraph(chart, $div, graphSett, dataSet);

		case 'Line':
		case 'SmoothLine':
		case 'Scatter':
			return new IDB.LineGraph(chart, $div, graphSett, dataSet);
		
		case 'Line3d':
			return new IDB.Line3dGraph(chart, $div, graphSett, dataSet);
		
		case 'Linear':
		case 'LinearVert':
		case 'LinearOdom':
		case 'LinearOdomVert':
		case 'LinearTarget':
		case 'LinearTargetVert':
		case 'LinearThermom':
		case 'LinearThermomVert':
		case 'LinearThermomOdom':
		case 'LinearThermomOdomVert':
		case 'LinearThermomTarget':
		case 'LinearThermomTargetVert':
			return new IDB.LinearGraph(chart, $div, graphSett, dataSet);
			
		case 'Metrics':
		case 'MetricsFlip':
		case 'MetricsTarget':
		case 'MetricsFlipTarget':
			return new IDB.MetricsGraph(chart, $div, graphSett, dataSet);

		case 'Pareto':
			return new IDB.ParetoGraph(chart, $div, graphSett, dataSet);

		case 'Pie2d':
		case 'Pie2dDonut':
		case 'Pie2dExp':
		case 'Pie2dDonutExp':
		case 'Pie3d':
		case 'Pie3dDonut':
		case 'Pie3dExp':
		case 'Pie3dDonutExp':
			return new IDB.PieGraph(chart, $div, graphSett, dataSet);

		case 'Placard':
			return new IDB.PlacardGraph(chart, $div, graphSett, dataSet);

		case 'Pyramid2d':
		case 'Funnel2d':
		case 'Pyramid3d':
		case 'Funnel3d':
		case 'PyramidCone':
		case 'FunnelCone':
			return new IDB.PyramidGraph(chart, $div, graphSett, dataSet);

		case 'Report':
		case 'ReportTarget':
		case 'Tabular':
		case 'Scorecard':
			return new IDB.ReportGraph(chart, $div, graphSett, dataSet);

		case 'Slideshow':
			return new IDB.SlideshowGraph(chart, $div, graphSett, dataSet);
			
		case 'Sparkline':
		case 'Sparkbar':
			return new IDB.SparkGraph(chart, $div, graphSett, dataSet);

		case 'Stoplight':
		case 'StoplightVert':
			return new IDB.StoplightGraph(chart, $div, graphSett, dataSet);

		case 'Ticker':
		case 'TickerVert':
			return new IDB.TickerGraph(chart, $div, graphSett, dataSet);
		case 'IG':
			return new IDB.IG(chart, $div, graphSett, dataSet);
			
		case 'Treemap':
			return new IDB.TreemapGraph(chart, $div, graphSett, dataSet);

		case 'GeoPlot':
			return new IDB.GeoMapGraph(chart, $div, graphSett, dataSet);


		default: //defaults to TempGraph for now...
			return new IDB.TempGraph(chart, $div, chartSettings, dataSet);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphFactory.getScriptModuleInfo = function(chartSettings) {
	switch ( chartSettings.graphTypeCode ) {

		case 'Area3d':
		case 'Area3dStacked':
		case 'Area3dRatio':
			return {
				module: 'Area3d',
				depends: [
					'Area',
					'Cartesian3d',
					'Cartesian',
					'Shared'
				]
			};

		case 'Area':
		case 'AreaSmooth':
		case 'AreaStacked':
		case 'AreaRatio':
			return {
				module: 'Area',
				depends: [
					'Cartesian',
					'Line',
					'Shared'
				]
			};

		case 'Column3d':
		case 'Column3dStacked':
		case 'Column3dRatio':
		case 'Cylinder3d':
		case 'Cylinder3dStacked':
		case 'Cylinder3dRatio':
			return {
				module: 'Bar3d',
				depends: [
					'Bar',
					'Cartesian3d',
					'Cartesian',
					'Shared'
				]
			};

		case 'BarDist':
			return {
				module: 'BarDist',
				depends: [
					'Bar',
					'Cartesian',
					'Shared'
				]
			};

		case 'Bar':
		case 'BarStacked':
		case 'BarRatio':
		case 'Column':
		case 'ColumnStacked':
		case 'ColumnRatio':
			return {
				module: 'Bar',
				depends: [
					'Cartesian',
					'Shared'
				]
			};

		case 'Bubble':
			return {
				module: 'Bubble',
				depends: [
					'Shared'
				]
			};

		case 'CalendarMonthly':
		case 'CalendarMonthlyEvent':
		case 'CalendarMultiday':
		case 'CalendarDaily':
			return {
				module: 'Calendar',
				depends: [
					'Shared'
				]
			};

		case 'Combined':
		case 'CombinedStacked':
			return {
				module: 'Combined',
				depends: [
					'Bar',
					'Cartesian',
					'Line',
					'Shared'
				]
			};

		case 'Gallery':
			return {
				module: 'Gallery',
				depends: [
					'Shared'
				]
			};

		case 'GaugeFull':
		case 'GaugeFullOdom':
		case 'GaugeSquare':
		case 'GaugeSquareOdom':
		case 'GaugeHalf':
		case 'GaugeHalfOdom':
		case 'GaugeCustom':
		case 'GaugeTarget':
		case 'GaugeCluster2':
		case 'GaugeCluster3':
		case 'GaugeCluster4':
		case 'GaugeMultiNeedle':
			return {
				module: 'Gauge',
				depends: [
					'Shared'
				]
			};
		case 'IG':
			return {
				module: 'IG',
				depends: [
					'Shared'
				]
			};
		case 'ImagePlot':
			return {
				module: 'Imageplot',
				depends: [
					'Shared'
				]
			};

		case 'Line3d':
			return {
				module: 'Line3d',
				depends: [
					'Cartesian3d',
					'Cartesian',
					'Line',
					'Shared'
				]
			};

		case 'Line':
		case 'SmoothLine':
		case 'Scatter':
			return {
				module: 'Line',
				depends: [
					'Cartesian',
					'Shared'
				]
			};

		case 'Linear':
		case 'LinearVert':
		case 'LinearOdom':
		case 'LinearOdomVert':
		case 'LinearTarget':
		case 'LinearTargetVert':
		case 'LinearThermom':
		case 'LinearThermomVert':
		case 'LinearThermomOdom':
		case 'LinearThermomOdomVert':
		case 'LinearThermomTarget':
		case 'LinearThermomTargetVert':
			return {
				module: 'Linear',
				depends: [
					'Shared'
				]
			};

		case 'Metrics':
		case 'MetricsFlip':
		case 'MetricsTarget':
		case 'MetricsFlipTarget':
			return {
				module: 'Metrics',
				depends: [
					'Cartesian',
					'Shared'
				]
			};

		case 'Pareto':
			return {
				module: 'Pareto',
				depends: [
					'Bar',
					'Cartesian',
					'Line',
					'Shared'
				]
			};

		case 'Pie2d':
		case 'Pie2dDonut':
		case 'Pie2dExp':
		case 'Pie2dDonutExp':
		case 'Pie3d':
		case 'Pie3dDonut':
		case 'Pie3dExp':
		case 'Pie3dDonutExp':
			return {
				module: 'Pie',
				depends: [
					'Shared'
				]
			};

		case 'Placard':
			return {
				module: 'Placard',
				depends: [
					'Shared'
				]
			};

		case 'Pyramid2d':
		case 'Funnel2d':
		case 'Pyramid3d':
		case 'Funnel3d':
		case 'PyramidCone':
		case 'FunnelCone':
			return {
				module: 'Pyramid',
				depends: [
					'Shared'
				]
			};

		case 'Report':
		case 'ReportTarget':
		case 'Tabular':
		case 'Scorecard':
			return {
				module: 'Report',
				depends: [
					'Shared'
				]
			};

		case 'Slideshow':
			return {
				module: 'Slideshow',
				depends: [
					'Shared'
				]
			};

		case 'Sparkline':
		case 'Sparkbar':
			return {
				module: 'Spark',
				depends: [
					'Shared'
				]
			};

		case 'Stoplight':
		case 'StoplightVert':
			return {
				module: 'Stoplight',
				depends: [
					'Linear',
					'Shared'
				]
			};

		case 'Ticker':
		case 'TickerVert':
			return {
				module: 'Ticker',
				depends: [
					'Shared'
				]
			};

		case 'Treemap':
			return {
				module: 'Treemap',
				depends: [
					'Shared'
				]
			};
		case 'GeoPlot':
			return {
				module: 'Geomap',
				depends: [
					'Shared'
				]
			};
		default:
			return {
				module: 'Temp',
				depends: [
					'Shared'
				]
			};
	}
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.Axis
/*global IDB */
IDB.Axis = function(data) {
   var me = this, ranges = data.ranges;
   me.axisId = data.axisId;
   me.axisName = data.axisName;
   me.dataType = data.dataType;
   me.dataTypeIsNumber = ("NUMBER" === me.dataType);
   me.dataTypeIsDatetime = ("DATETIME" === me.dataType);
   me.dataTypeIsString = ("STRING" === me.dataType);
   me.pivotRank = data.pivotRank;
   me.sendOnDrilldown = data.sendOnDrilldown;
   me.sendOnPivotDrilldown = data.sendOnPivotDrilldown;
   me.isPivot = me.pivotRank > 0;
   me.isHidden = data.hidden;
   me.axisColor = data.axisColor;
   me.numFmt = data.numFormat;
   me.unit = data.unit;
   me.isAnyGraphAxis = ((me.axisId === 0) || (!me.isHidden && !me.isPivot));
   me.imageUrl = data.imageUrl;
   me.imageUseProxy = !!data.imageUseProxy;

   me._settings = data.axisProperties || {};

   me.map = {};

   if(me.isPivot && data.hasOwnProperty("pivotSelection")) {
       me.pivotSelection = data.pivotSelection;
   }
   me._ranges = null;
   if(ranges) {
      me._ranges = [];
      for(var j=0, jj=ranges.length; j<jj; j++) {
          me._ranges.push(new IDB.Range(ranges[j]));
      }
   }

   me._defaultRanges = null;

//        "axisId":0,
//        "axisCode":"x",
//        "axisName":"City",
//        "pivotRank":-1,
//        "dataType":"STRING",
//        "sendOnDrilldown":true,
//        "sendOnPivotDrilldown":true,
//        "axisColor":15606306,
//        "hidden":false,
//        "ranges":null


};

Object.defineProperties(IDB.Axis.prototype, {
    color:{
        get:function() {
            return (this.axisColor || IDB.rgbx(0));
        }
    },
    label:{
        get:function() {
            return (this.axisName || "");
        }
    },
    ranges:{
       get:function() {
           var r = this._ranges;
           if(r && r.length>0) {return r;}
           if(this.axisId === 0 && this._defaultRanges) {return this._defaultRanges;}
           return [];
       }
    }
});

IDB.Axis.prototype.setDefaultRangeList = function(ranges) {
   this._defaultRanges = ranges;
};

IDB.Axis.prototype.toString = function() {
   var s = ("Axis[axisId=" + this.axisId + ", axisName=" + this.axisName + ", dataType=" + 
            this.dataType + ", isPivot=" + this.isPivot + ", pivotRank=" + this.pivotRank + "]");
   return s;
};

IDB.Axis.prototype.isGraphAxis = function(numericYOnly) {
    return (this.axisId === 0) || (!this.isHidden && !this.isPivot && (!numericYOnly || this.dataTypeIsNumber));
};

IDB.Axis.prototype.getRangeList = function() {
	return (this.ranges || []);
};

IDB.Axis.prototype.get = function(name) {
	var value = this._settings[name];
	
	if ( IDB.isUndef(value) ) {
		IDB.log('Missing value for axis property: "'+name+'".');
	}

	return value;
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.DataValue
/*global IDB */
IDB.DataValue = function(dataType, json) {
    var me = this;
    me.dataType = dataType;
    me._nazFV = ""; //nulls-are-zero formatted value.
    me._value = null;
    me.dtIsN = false; // dataTypeIsNumber
    me.dtIsS = false; // dataTypeIsString
    me.dtIsD = false; // dataTypeIsDatetime
    if(dataType === "STRING") {
        me._value = json.s;
        me._isNothing = (me._value === null || me._value === "");
        me._stringValue = me._isNothing ? "" : me._value;
        me._formattedValue = me._stringValue;
        me._numberValue = me._isNothing ? 0 : NaN;
        me.dtIsS = true;
    }
    else if(dataType === "NUMBER") {
        me._value = json.n;
        me._numberValue = me._value;
        me._isNothing = (me._value === null);
        me._stringValue = me._isNothing ? "" : me._value.toString();
        me._nazFV = json.f; // if json.n is null, this will be 0 with the proper format applied on the server.
        me._formattedValue = me._isNothing ? "" : json.f;
        me.dtIsN = true;
    }
    else if(dataType === "DATETIME") {
//        IDB.log(json, "DATETIME");
        me._value = null;
        me._numberValue = json.n;
        me._isNothing = (me._numberValue === null);
        if(!me._isNothing) {
            me._value = new Date(me._numberValue);
        }
        me._stringValue = me._isNothing ? "" : json.d;
        me._formattedValue = me._isNothing ? "" : json.f;
        me.dtIsD = true;
//        IDB.log(me._value + ", " + me._formattedValue + ", " + me._stringValue);
    }
    else {
        IDB.log("UNKNOWN DATA TYPE: " + dataType);
    }
};

Object.defineProperties(IDB.DataValue.prototype, {
    fv:{get:function(){return this._formattedValue;}},
    formattedValue:{get:function(){return this._formattedValue;}},
    isNothing:{get:function(){return this._isNothing;}},
    numberValue:{get:function(){return this._numberValue;}},
    stringValue:{get:function(){return this._stringValue;}},
    value:{get:function(){return this._value;}}
});

IDB.DataValue.prototype.getFV = function(naz) { // getFormattedValue(nullsAreZero=false)
    var me = this, isNumber = me.dtIsN, isNothing = me._isNothing, fv = me._formattedValue, nazFV = me._nazFV;
    if(!isNothing || !isNumber) {
        return fv;
    }
    // me._isNothing is true
    return (naz ? me._nazFV : ""); 
};

IDB.DataValue.prototype.getNV = function(naz) { // getNumberValue(nullsAreZero=false)
    var me = this, nv = me._numberValue;
    if(!me._isNothing || !me.dtIsN) {
        return nv;
    }
    return naz ? 0 : nv; 
};



IDB.DataValue.prototype.toString = function() {
   return this.stringValue;
};

IDB.DataValue.compare = function(dv1, dv2) {

    // handle case where one or both are null.
    if(!dv1) {
        return dv2 ? -1 : 0;
    }

    if(!dv2) return 1;

    // handle case where one or both contain null
    if(dv1.isNothing) {
        return dv2.isNothing ? 0 : -1;
    }
    if(dv2.isNothing) return 1;

    if(dv1.dataType == "NUMBER" || dv1.dataType == "DATETIME") {
        if(dv1.numberValue == dv2.numberValue) return 0;
        return dv1.numberValue > dv2.numberValue ? 1 : -1;
    }

    var s1 = dv1.stringValue.toLocaleLowerCase();
    var s2 = dv2.stringValue.toLocaleLowerCase();
    if(s1 < s2) return -1;
    return s2 < s1 ? 1 : 0;

};

IDB.DataValue.rcompare = function(dv1, dv2) {
//    IDB.log("rcompare: " + dv1 + ", " + dv2 + ", " + (0-IDB.DataValue.compare(dv1, dv2)));
    return 0 - IDB.DataValue.compare(dv1, dv2);
};

IDB.DataValue.prototype.debugString = function() {
   return "DataValue[dataType=" + this.dataType + ", value=" + this._value + "]";
};////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.Datapoint

/**
 * @param xAxis IDB.Axis
 * @param yAxis IDB.Axis
 * @param dataRow Array of IDB.Datapoint
 * @param datapointRow IDB.DatapointRow
 */
IDB.Datapoint = function(xAxis, yAxis, dataRow, datapointRow) {
   this._xAxis = xAxis;
   this._yAxis = yAxis;
   this._dataRow = dataRow;
   this._datapointRow = datapointRow;
   this._xValue = dataRow[xAxis.axisId];
   this._yValue = dataRow[yAxis.axisId];
   this._axisColor = yAxis.axisColor;
   this._rangeColor = null;
   this._valueOutOfRange = 0;
   this._visualState = 0;
   this.onVisualState = null; 
   this.map = {};
};

Object.defineProperties(IDB.Datapoint.prototype, {
   axisColor:{get:function(){return this._axisColor;}, 
              set:function(rgb){this._axisColor=rgb;}
              },
   rangeColor:{get:function(){return this._rangeColor;}, 
               set:function(rgb){this._rangeColor=rgb;}
              },
   valueOutOfRange:{get:function(){return this._valueOutOfRange;}, 
               set:function(zeroOneOrMinusOne){this._valueOutOfRange=zeroOneOrMinusOne;}
              },

   datapointRow:{get:function(){return this._datapointRow;}},
   dataRow:{get:function(){return this._dataRow;}},
   xAxis:{get:function(){return this._xAxis;}},
   xValue:{get:function(){return this._xValue;}},
   yAxis:{get:function(){return this._yAxis;}},
   yValue:{get:function(){return this._yValue;}}
});

IDB.Datapoint.prototype.setVisualState = function(newState) {
	var oldState = this._visualState;
    this._visualState = newState;

	if ( this.onVisualState && newState != oldState ) {
		this.onVisualState(newState);
	}
};

IDB.Datapoint.prototype.getVisualState = function() {
    return this._visualState;
};

IDB.Datapoint.prototype.getMatchValue = function(matchType) {
    if(matchType == IDB.MT.X_VALUE) return this.xValue.stringValue;
    if(matchType == IDB.MT.Y_VALUE) return this.yValue.stringValue;
    if(matchType == IDB.MT.Y_AXIS_NAME) return this.yAxis.axisName;
    if(matchType == IDB.MT.NONE) return null;
    throw new Error("Invalid matchType: " + matchType);

};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// DatapointManager
/*global IDB */
IDB.DatapointManager = function(graph, displaysMultipleXValues) {
    this.graph = graph;
    this.displaysMultipleXValues = displaysMultipleXValues;
    this.debug = false;
    this.reset();
};

/**
 * Initializes or re-initializes all of the containers that hold datapoints to their initial, empty state.
 */
IDB.DatapointManager.prototype.reset = function() {
    var me = this;
    me.xMap = {};
    me.yMap = {};
    me.yAxisMap = {};
    me.maps = {
        X_VALUE:me.xMap,
        Y_VALUE:me.yMap,
        Y_AXIS_NAME:me.yAxisMap
    };
    me.datapoints = [];
};


IDB.DatapointManager.prototype.addDatapoint = function(dp) {
    this.datapoints.push(dp);
    var x = dp.xValue.stringValue;
    var y = dp.yValue.stringValue;
    var n = dp.yAxis.axisName;
    var arr = this.xMap[x];
    if(!arr) {
        arr = [];
        this.xMap[x] = arr;
    }
    arr.push(dp);
    arr = this.yMap[y];
    if(!arr) {
        arr = [];
        this.yMap[y] = arr;
    }
    arr.push(dp);
    arr = this.yAxisMap[n];
    if(!arr) {
        arr = [];
        this.yAxisMap[n] = arr;
    }
    arr.push(dp);
};


IDB.DatapointManager.prototype.highlightMatchingDatapoints = function(value, matchType, fadeOthers) {

    var me = this, MT = IDB.MT, matches;

    if(matchType === MT.X_VALUE) {
        matches = me.highlightDatapointsWithXValue(value, fadeOthers);
    }
    else if(matchType === MT.Y_VALUE) {
        matches = me.highlightDatapointsWithYValue(value, fadeOthers);
    }
    else if(matchType === MT.Y_AXIS_NAME) {
        matches = me.highlightDatapointsForYAxisWithName(value, fadeOthers);
    }
    else if(matchType === MT.NONE) {
        matches = [];
    }
    
    me.graph.datapointUpdatesCompleted();
    return matches;
};



IDB.DatapointManager.prototype.highlightDatapointsWithXValue = function(value, fadeOthers) {

    var foundAtIndex = -1;
    var matches = [];
    var singleMatch = !this.displaysMultipleXValues;
    var firstMatchFound = false;

    // Note: If there are NO matching values, then nothing should be changed. That's why we don't fade anything
    // until we've found our first match.
    var graph = this.graph;
    var datapoints = this.datapoints;
    var dp = null;
    var oldState = null;
    for(var j=0, jj=datapoints.length; j<jj; j++) {
        dp = datapoints[j];
        oldState = dp.getVisualState();

        var shouldHighlight = (dp.xValue.stringValue == value) && !(singleMatch && firstMatchFound);
        if(shouldHighlight) {
            firstMatchFound = true;
            dp.setVisualState(IDB.VS.HIGHLIGHTED);
            graph.datapointHighlighted(dp, oldState, IDB.MT.X_VALUE);
            matches.push(dp);
            if(foundAtIndex < 0) {
                foundAtIndex = j;
            }
        }
        else if(foundAtIndex > -1) { 
            if(fadeOthers) {
                dp.setVisualState(IDB.VS.FADED);
                graph.datapointFaded(dp, oldState, IDB.MT.X_VALUE);
            }
            else {
                dp.setVisualState(IDB.VS.NORMAL);
                if(oldState == IDB.VS.HIGHLIGHTED) {
                    graph.datapointUnhighlighted(dp);
                }
                else if(oldState == IDB.VS.FADED) {
                    graph.datapointUnfaded(dp);
                }
            }
        }
    }

    if(foundAtIndex > -1) {
        for(j=0; j<foundAtIndex; j++) {
            dp = datapoints[j];
            oldState= dp.getVisualState();
            if(fadeOthers) {
                dp.setVisualState(IDB.VS.FADED);
                graph.datapointFaded(dp, oldState, IDB.MT.X_VALUE);
            }
            else {
                dp.setVisualState(IDB.VS.NORMAL);
                if(oldState == IDB.VS.HIGHLIGHTED) {
                    graph.datapointUnhighlighted(dp);
                }
                else if(oldState == IDB.VS.FADED) {
                    graph.datapointUnfaded(dp);
                }
            }
        }
    }

    return matches;
};


IDB.DatapointManager.prototype.highlightDatapointsWithYValue = function(value, fadeOthers) {

    var foundAtIndex = -1;

    var matches = [];
    var datapoints = this.datapoints;
    var graph = this.graph;
    var dp = null;
    var oldState = null;
    var foundAtDp = null;
    for(var j=0, jj=datapoints.length; j<jj; j++) {
        dp = datapoints[j];
        oldState = dp.getVisualState();
        if(dp.yValue.stringValue == value) {
            dp.setVisualState(IDB.VS.HIGHLIGHTED);
            graph.datapointHighlighted(dp, oldState, IDB.MT.Y_VALUE);
            matches.push(dp);
            if(foundAtIndex < 0) {
                foundAtIndex = j;
                foundAtDp = dp;
                IDB.log(dp, dp.yValue.stringValue + " FOUND AT " + j);
            }
        }
        else if(foundAtIndex > -1) {
            dp.setVisualState(IDB.VS.FADED);
            graph.datapointFaded(dp, oldState, IDB.MT.Y_VALUE);
        }
    }

    if(foundAtIndex > -1) {
        for(j=0; j<foundAtIndex; j++) {
            dp = datapoints[j];
            if(dp === foundAtDp) {
                IDB.log(dp, j + " eek.");
            }
            oldState= dp.getVisualState();
            dp.setVisualState(IDB.VS.FADED);
            graph.datapointFaded(dp, oldState, IDB.MT.Y_VALUE);
        }
    }

    return matches;
};


IDB.DatapointManager.prototype.highlightDatapointsForYAxisWithName = function(axisName, fadeOthers) {

    var graph = this.graph;
    var dataSet = graph.getDataSet();
    var gInfo = graph.getInfo();

    var axis = dataSet.getYGraphAxisWithName(axisName, gInfo.numericYOnly);
    if(!axis) return []; // nothing to do.

    var matches = [];

    var datapoints = this.datapoints;

    // since the axis exists, we assume datapoints exist for it also, unlike
    // in highlightDatapointsWithXValue and highlightDatapointsWithYValue
    for(var j=0, jj=datapoints.length; j<jj; j++) {
        var dp = datapoints[j];
        var oldState = dp.getVisualState();
        if(dp.yAxis.axisName == axisName) {
            dp.setVisualState(IDB.VS.HIGHLIGHTED);
            graph.datapointHighlighted(dp, oldState, IDB.MT.Y_AXIS_NAME);
            matches.push(dp);

        }
        else {
            dp.setVisualState(IDB.VS.FADED);
            graph.datapointFaded(dp, oldState, IDB.MT.Y_AXIS_NAME);
        }
    }

    return matches;
};

IDB.DatapointManager.prototype.highlightDatapoint = function(datapoint, matchType, fadeOthers) {
    var graph = this.graph;
    var datapoints = this.datapoints;

    var foundAtIndex = -1;
    var dp = null;
    var oldState = null;
    for(var j=0, jj=datapoints.length; j<jj; j++) {
        dp = datapoints[j];
        oldState = dp.getVisualState();

        if(foundAtIndex > -1) { // already found.
            if(fadeOthers) {
                dp.setVisualState(IDB.VS.FADED);
                graph.datapointFaded(dp, oldState, IDB.MT.X_VALUE);
            }
            else {
                dp.setVisualState(IDB.VS.NORMAL);
                if(oldState == IDB.VS.HIGHLIGHTED) {
                    graph.datapointUnhighlighted(dp);
                }
                else if(oldState == IDB.VS.FADED) {
                    graph.datapointUnfaded(dp);
                }
            }
        }
        else if(dp == datapoint) {
            dp.setVisualState(IDB.VS.HIGHLIGHTED);
            graph.datapointHighlighted(dp, oldState, IDB.MT.X_VALUE);
            if(foundAtIndex < 0) {
                foundAtIndex = j;
            }
        }
    }

    if(foundAtIndex > -1) {
        for(j=0; j<foundAtIndex; j++) {
            dp = datapoints[j];
            oldState= dp.getVisualState();
            if(fadeOthers) {
                dp.setVisualState(IDB.VS.FADED);
                graph.datapointFaded(dp, oldState, IDB.MT.X_VALUE);
            }
            else {
                dp.setVisualState(IDB.VS.NORMAL);
                if(oldState == IDB.VS.HIGHLIGHTED) {
                    graph.datapointUnhighlighted(dp);
                }
                else if(oldState == IDB.VS.FADED) {
                    graph.datapointUnfaded(dp);
                }
            }
        }
    }
    
    graph.datapointUpdatesCompleted();
    return (foundAtIndex > -1);
};

IDB.DatapointManager.prototype.normalizeAllDatapoints = function() {
	var datapoints = this.datapoints;
	var graph = this.graph;
	var i, dp, oldState;

	for ( i in datapoints ) {
		dp = datapoints[i];
		oldState = dp.getVisualState();
		dp.setVisualState(IDB.VS.NORMAL);

		if ( oldState == IDB.VS.FADED ) {
			graph.datapointUnfaded(dp);
		}
		else if ( oldState == IDB.VS.HIGHLIGHTED ) {
			graph.datapointUnhighlighted(dp);
		}
	}
    
    graph.datapointUpdatesCompleted();
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.DatapointRow

IDB.DatapointRow = function(rowId, axisList, dataRow) {
   this.rowId = rowId;
   this.dataRow = dataRow;
   this.xValue = dataRow[0];
   this.datapoints = [];
   this.map = {};
   this.prev = null;
   this.next = null;
   this._allGraphDatapoints = [];
   this._numericGraphDatapoints = [];
   var xAxis = this.xAxis = axisList[0];
   for(var j=1, jj=axisList.length; j<jj; j++) {
       var yAxis = axisList[j];
       var dp = new IDB.Datapoint(xAxis, yAxis, dataRow, this);
       this.datapoints.push(dp);
       if(yAxis.isGraphAxis(true)) {
            this._allGraphDatapoints.push(dp);
            this._numericGraphDatapoints.push(dp);
       }
       else if(yAxis.isGraphAxis(false)) {
            this._allGraphDatapoints.push(dp);
       }
   }
};

Object.defineProperties(IDB.DatapointRow.prototype, {
    numericGraphDatapoints:{
        get:function() {
            return this._numericGraphDatapoints;
        }
    },
    allGraphDatapoints:{
       get:function() {
           return this._allGraphDatapoints;
       }
    }
}); 

IDB.DatapointRow.prototype.getDatapoint = function(axisId) {
   if(!axisId || axisId < 1) throw new Error("DatapointRow.getDatapoint(): invalid axisId argument: " + axisId);
   return this.datapoints[axisId-1];
};
/**
 * MatchType constants
 */
Object.defineProperty(IDB, "MT", {
    value:{
       X_VALUE:"X_VALUE",
       Y_VALUE:"Y_VALUE",
       Y_AXIS_NAME:"Y_AXIS_NAME",
       NONE:"NONE"
    },
    writable:false,
    enumerable:false,
    configurable:false
});

Object.freeze(IDB.MT);
/**
 * VisualState constants
 */
Object.defineProperty(IDB, "VS", {
    value:{
       NORMAL:0,
       FADED:1,
       HIGHLIGHTED:2
    },
    writable:false,
    enumerable:false,
    configurable:false
});

Object.freeze(IDB.VS);


/*global IDB, $ */
/*====================================================================================================*/
// ._graphContext (GraphContext)
// ._graphContext.datapointGrid (DatapointGrid)
// ._graphContext.datapointGrid.getDatapointRows() (Array/DatapointRow)
// ._graphContext.settings (GraphSettings) 
// .getDataSet() (DataSet)
// .getDataSet().axisList (Array/Axis)             .
IDB.GraphBase = function(chart, $div, graphProperties, dataSet) {
	var me = this, dpGrid = new IDB.DatapointGrid(me, dataSet),
	    context = new IDB.GraphContext(
    		graphProperties,
    		dpGrid,
    		IDB.listener(me, "refreshDisplay"),
    		IDB.listener(me, "datapointClicked"),
    		IDB.listener(me, "datapointMouseOver"),
    		IDB.listener(me, "datapointMouseOut"),
    		IDB.listener(me, "yAxisSelected"),
    		IDB.listener(me, "showWarning")
    	);

    me.gp = function(name) {     // get property - this is a closure so it doesn't need to be invoked on this particular object, but can be passed around. Don't change it.
        return graphProperties.get(name);
    };


    if(!me._absMinW) {
        me._absMinW = 20;
    }

    if(!me._absMinH) {
        me._absMinH = 20;
    }


    me._callingGraphManager = false;

	me._$div = $div;
	me._chart = chart;
	me._graphContext = context;
	me._dataSet = dataSet;
	me._info = new IDB.GraphInfo();
	me._dpm = new IDB.DatapointManager(this, true);

    // subclasses may assign an array of Datapoints to me._relevantDatapoints, which will be
    // returned by the _getRelevantDatapoints() function.
    me._relevantDatapoints = null;

	//TODO: consider moving the creation of Datapoint Grids/Managers to the chart level
	//TODO: consider removing the graph's reference to Chart (in favor of DatapointManager?)

	me._build();
	me._resetDatapointManager();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype._resetDatapointManager = function() {
	var me = this, dpm = me._dpm, datapoints = me._getRelevantDatapoints();
	dpm.reset();
	
    for(var j=0, jj=datapoints.length; j<jj; j++) {
		dpm.addDatapoint(datapoints[j]);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.getDataSet = function() {
	return this._dataSet;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.getLegendAxes = function() {
	return this._dataSet.getGraphAxes(this._info.numericYOnly);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.getInfo = function() {
	return this._info;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype._getRelevantDatapoints = function() {
    var me = this, relevant = me._relevantDatapoints, datapointGrid = me._graphContext.datapointGrid, info = me._info;
    if(relevant) {
        return relevant;
    }
	return datapointGrid.getAllDatapoints(info.numericYOnly);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.resize = function() {
	this._update(this.width(), this.height());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.refreshDisplay = function() {};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.showWarning = function(title, message) {
    this._chart.displayUserMessage(this, message, "warning",  title);
};

/* jshint unused:true */
IDB.GraphBase.prototype.updateCallout = function(dp, $callout) {
/* jshint unused:false */
    $("<div>").appendTo($callout).css({"padding":"5px"}).html("CUSTOM CALLOUT<br>NOT IMPLEMENTED");
};

IDB.GraphBase.prototype.createLegendItems = function() {
    var items = [];

   // $("<div>").appendTo($callout).css({"padding":"5px"}).html("CUSTOM CALLOUT<br>NOT IMPLEMENTED");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.destroy = function() {
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
/* jshint unused:true */
IDB.GraphBase.prototype.highlightMatchingDatapoints = function(value, matchType, fadeOthers) {
	return this._dpm.highlightMatchingDatapoints(value, matchType, fadeOthers);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.highlightDatapoint = function(dp, matchType, fadeOthers) {
	return this._dpm.highlightDatapoint(dp,  matchType, fadeOthers);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.normalizeAllDatapoints = function() {
	this._dpm.normalizeAllDatapoints();
};
/* jshint unused:false */

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.width = function() {
	return this._$div.width();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.height = function() {
	return this._$div.height();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
/* jshint unused:false */
IDB.GraphBase.prototype.datapointHighlighted = function(dp, previousState, matchType) {
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.datapointFaded = function(dp, previousState, matchType) {
};
	
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.datapointUnhighlighted = function(dp) {
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.datapointUnfaded = function(dp) {
};
/* jshint unused:false */
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.datapointUpdatesCompleted = function() {
	this._graphContext.notifyDatapointsUpdated();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.datapointMouseOver = function(dp) {
	var gc = this._graphContext;

    try {
        this._callingGraphManager = true;
		gc.isOutgoingDatapointEvent = true;
        this._chart.datapointMouseOver(this, dp);
    } finally {
        this._callingGraphManager = false;
		gc.isOutgoingDatapointEvent = false;
    }
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.datapointMouseOut = function(dp) {
	var gc = this._graphContext;

    try {
        this._callingGraphManager = true;
		gc.isOutgoingDatapointEvent = true;
        this._chart.datapointMouseOut(this, dp);
    } finally {
        this._callingGraphManager = false;
		gc.isOutgoingDatapointEvent = false;
    }

};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.datapointClicked = function(dp) {
	var gc = this._graphContext;

    try {
        this._callingGraphManager = true;
		gc.isOutgoingDatapointEvent = true;
	    this._chart.datapointClicked(this, dp);
    } finally {
        this._callingGraphManager = false;
		gc.isOutgoingDatapointEvent = false;
    }

};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.yAxisSelected = function(yAxisIndex) {
	var axis = this._graphContext.datapointGrid.getYAxis(yAxisIndex, true);
	this._chart.yAxisSelected(this, axis);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.getAbsMinW = function() {
    return this._absMinW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBase.prototype.getAbsMinH = function() {
    return this._absMinH;
};

/*====================================================================================================*/
IDB.GraphBaseKinetic = function(chart, $div, graphProperties, dataSet) {
	this._stage = new Kinetic.Stage({
		container: $div[0],
		width: $div.width(),
		height: $div.height(),
		draggable: false,
		x: 0,
		y: 0,
		offset: [0, 0]
	});

	this._stage.visible(false); //becomes visible upon first "refreshDisplay()"

	this._stageLayer = new Kinetic.Layer();
	this._stage.add(this._stageLayer);

    this._redrawFunc = null;

	/*this._resizeLabel = $('<p>') //TEMP
		.width(100)
		.css('position', 'absolute')
		.css('font-size', '10px')
		.css('top', '0px')
		.appendTo($div.parent().parent());
		
	this._redrawLabel = $('<p>') //TEMP
		.width(100)
		.css('position', 'absolute')
		.css('font-size', '10px')
		.css('top', '14px')
		.appendTo($div.parent().parent());*/

	IDB.GraphBase.call(this, chart, $div, graphProperties, dataSet);
};

IDB.GraphBaseKinetic.prototype = Object.create(IDB.GraphBase.prototype);
IDB.GraphBaseKinetic.prototype.constructor = IDB.GraphBaseKinetic;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBaseKinetic.prototype.resize = function() {
	//var t0 = window.performance.now();

	var w = this.width();
	var h = this.height();

	this._stage.width(w);
	this._stage.height(h);
	this._stage.position({ x: 0, y: 0 });

	IDB.GraphBase.prototype.resize.call(this);
	
	//var t1 = window.performance.now();
	//console.log("resize: "+(t1-t0)+" ms");

	this.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBaseKinetic.prototype.refreshDisplay = function() {
	IDB.GraphBase.prototype.refreshDisplay.call(this);

	if ( this._redrawFunc ) {
		//console.log('GraphBaseKinetic.refreshDisplay: skip');
		return;
	}

	var thisRef = this;
	
	this._redrawFunc = function() {
		//console.log('GraphBaseKinetic.refreshDisplay: draw');
		//var t0 = window.performance.now();

		thisRef._stage.visible(true);
		thisRef._stage.draw();
		thisRef._redrawFunc = null;
		
		//var t1 = window.performance.now();
		//thisRef._redrawLabel.html('draw: '+(t1-t0)+'ms'); //TEMP
		//console.log('draw: '+(t1-t0)+'ms');
	};

	//console.log('GraphBaseKinetic.refreshDisplay: set');
	setTimeout(this._redrawFunc, 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBaseKinetic.prototype.destroy = function() {
	IDB.GraphBase.prototype.destroy.call(this);
	this._stage.destroy();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBaseKinetic.prototype.highlightMatchingDatapoints = function(value, matchType, fadeOthers) {
	var points = IDB.GraphBase.prototype.highlightMatchingDatapoints
		.call(this, value, matchType, fadeOthers);

	this.refreshDisplay();
	return points;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBaseKinetic.prototype.highlightDatapoint = function(dp, matchType, fadeOthers) {
	IDB.GraphBase.prototype.highlightDatapoint.call(this, dp, matchType, fadeOthers);
	this.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.GraphBaseKinetic.prototype.normalizeAllDatapoints = function() {
	IDB.GraphBase.prototype.normalizeAllDatapoints.call(this);
	this.refreshDisplay();
};

/*====================================================================================================*/
IDB.GraphInfo = function() {
	this.xMatchOnly = false;
	this.numericYOnly = true;
	this.displaysMultipleXValues = true;
	this.calloutStyle = 'axisColors';
	this.legendStyle = IDB.LS.NONE;
};

IDB.GraphInfo.TargetRangeList = [
	new IDB.Range({
		val: 0,
		color: IDB.rgbx('#ff0000'),
		label: 'Below Target'
	}),
	new IDB.Range({
		val: 100,
		color: IDB.rgbx('#00ff00'),
		label: 'Above Target'
	}),
	new IDB.Range({
		val: 200,
		color: IDB.rgbx('#0000ff'),
		label: '2X Target or Above'
	})
];

/*====================================================================================================*/
IDB.Calculator = {};

IDB.Calculator._ = {};

IDB.Calculator._.withFifthsMultipliers = [2.0, 2.5, 5.0, 10.0];
IDB.Calculator._.withoutFifthsMultipliers = [2.5, 5.0, 10.0];

IDB.Calculator.roundOff = function(value, allowFifths) { // (value:Number, allowFifths:Boolean = true):Array {


    var _ = IDB.Calculator._;

    if(isNaN(value) || !isFinite(value) || value <= 0) {
        throw new Error("roundOff(): Invalid argument: " + value);
    }

    var multipliers = allowFifths ? _.withFifthsMultipliers : _.withoutFifthsMultipliers;

    // make sure this is a positive number, otherwise we'll go
    // into an infinite loop.
    if(value <= 0) {
        throw new Error("argument must be greater than zero.");
    }

    // handle the trivial case where the value already equals 1.
    if(value === 1) {
        return [1, 1];
    }


    // calculate the next lowest power of 10 below the value. For example,
    // if the value is 130, the next lowest power of 10 would be 100 (10^2).
    // If the value is 0.000453, the next lowest power of 10 would be
    // 0.0001 (10^-4).

    var nextLowestPowerOf10 = -1;
    var exponent = 1; // int
    var powerOf10, lastPowerOf10;

    if(value > 1) {

        lastPowerOf10 = 1;

        while(true) {
            powerOf10 = Math.pow(10,  exponent);
            if(powerOf10 > value) break; 
            lastPowerOf10 = powerOf10;
            exponent = exponent + 1;
        }

        nextLowestPowerOf10 = lastPowerOf10;
    }
    else { // value < 1

        exponent = -1;

        while(true) {
            powerOf10 = Math.pow(10, exponent);
            if(powerOf10 <= value) {
                nextLowestPowerOf10 = powerOf10;
                break;
            }
            exponent = exponent - 1;
        }
    }

    // now we have the nearest power of 10 that's just below, or possibly
    // equal to our number. We'll use the multipliers to see which "rounded"
    // values are just above and below it. The multipliers array contains
    // 2.0, 2.5, 5.0, 10.0,  so for example, if the power of 10 we're 
    // starting from is 100000, we'll be checking to see what two values
    // in the list 100000, 200000, 250000, 500000, and 1000000 our number
    // falls between.

    // first check for the base case where the value is a power of 10.
    if(value == nextLowestPowerOf10) {
        return [nextLowestPowerOf10, nextLowestPowerOf10];
    }

    var lastRoundValue = nextLowestPowerOf10;
    for(var j=0, jj=multipliers.length; j<jj; j++) {
        var roundValue = nextLowestPowerOf10 * multipliers[j];

        // if we have an exact match, make both elements in the array
        // the same.
        if(roundValue == value) {
            return [roundValue, roundValue];
        }
        else if(roundValue > value) {
            return [lastRoundValue, roundValue];
        }

        lastRoundValue = roundValue;
    }

    // this code should never be reached, but just in case...
    return new [value, value];
};


IDB.Calculator.getDivisionMetrics = function(loVal, hiVal, numDivisions, allowFifths) { //(loVal:Number, hiVal:Number, numDivisions:Number, allowFifths:Boolean = true):Object {
    if(arguments.length < 4) {allowFifths = true;}
//    IDB.log("getDivisionMetrics(): loVal=" + loVal
//        + ", hiVal=" + hiVal + ", numDivisions=" + numDivisions + ", allowFifths=" + allowFifths);

    if(isNaN(loVal) || isNaN(hiVal) || isNaN(numDivisions) 
       || !isFinite(hiVal) || !isFinite(loVal) || !isFinite(numDivisions)) {
        throw new Error("getDivisionMetrics(): one or more arguments are NaN or Infinity (loVal=" + loVal
        + ", hiVal=" + hiVal + ", numDivisions=" + numDivisions + ")");

    }
    if(hiVal <= loVal) throw new Error("getDivisionMetrics(): hiVal must be greater than loVal. (loVal=" + loVal
        + ", hiVal=" + hiVal + ")");

    if(numDivisions < 1) {
        throw new Error("getDivisionMetrics(): numDivisions must be an integer greater than 0: " + numDivisions);
    }

    var overallRange = hiVal - loVal;
//    IDB.log("getDivisionMetrics(): loVal=" + loVal
//        + ", hiVal=" + hiVal + ", overallRange=" + overallRange + ", numDivisions=" + numDivisions);

    var roughDivision = overallRange / numDivisions;

    var roundedDivisions = IDB.Calculator.roundOff(roughDivision, allowFifths);

    var result = new Object();
    var propNames = ['smallDivisions', 'largeDivisions'];
    for(var index = 0, ii=roundedDivisions.length; index<ii; index++) {

        var metrics = new IDB.DivisionMetrics();
        result[propNames[index]] = metrics;

        var roundedDivision = roundedDivisions[index];
        var maxDecimalPlaces = IDB.Calculator.decimalPlaces(roundedDivision);

        metrics.divisionSize = roundedDivision;

        var boundaries = [];
        metrics.boundaries = boundaries;
        var current = 0;
        var lastCurrent = 0;
        if(loVal < 0) {
            if(hiVal < 0) {
                // Find the boundary that's just above or equal to hiVal.
                current = 0;

                // if roundedDivision is very small and loVal is very high, the while loop
                // can take a very long time. This will shorten it.
                if(roundedDivision < 1 && hiVal < -1) {
                    current = Math.floor(hiVal + 1);
                    lastCurrent = current;
                }
                else if(roundedDivision < Math.abs(hiVal)) {
                     var numAbove = Math.floor(Math.abs(hiVal)/roundedDivision);
                     current = 0 - (numAbove * roundedDivision);
                }

                lastCurrent = current;

                // we might still be several roundedDivisions above hiVal, so walk it down.
                while(current >= hiVal) {
                    lastCurrent = current;
                    current -= roundedDivision;
                    current = IDB.Calculator.maybeRound(current, maxDecimalPlaces);
                }
                // we've found the rounded division boundary that's at or immediately
                // above hiVal. now move downwards, unshifting boundary values, until 
                // we're at or below loVal.
                current = lastCurrent;
                while(current > loVal) {
                    boundaries.unshift(current);
                    current -= roundedDivision;
                    current = IDB.Calculator.maybeRound(current, maxDecimalPlaces);
                }
                boundaries.unshift(current);
            }
            else { // hiVal >= 0

                // start at 0 and work backwards until we're below loVal
                current = 0;
                boundaries.unshift(0);
                while(current > loVal) {
                    current -= roundedDivision;
                    current = IDB.Calculator.maybeRound(current, maxDecimalPlaces);
                    boundaries.unshift(current);
                }

                // now add the boundaries above zero, if hiVal is > 0.
                current = 0;
                while(current < hiVal) {
                    current += roundedDivision;
                    current = IDB.Calculator.maybeRound(current, maxDecimalPlaces);
                    boundaries.push(current);
                }
            }
        }
        else if(loVal === 0) {

            current = 0;
            while(current < hiVal) {
                boundaries.push(current);
                current += roundedDivision;
                current = IDB.Calculator.maybeRound(current, maxDecimalPlaces);
            }
            boundaries.push(current);
        }
        else { // loVal is > 0
            // find the first division <= loVal
            current = 0;

            // if roundedDivision is less than one, start at the first whole number
            // below loVal.
            if(roundedDivision < 1 && loVal > 1) {
                current = Math.floor(loVal - 1);
            }
            else if(roundedDivision < loVal) {
                // start at the first multiple of roundedDision <= loVal
                var numBelow = Math.floor(loVal/roundedDivision);
                current = numBelow * roundedDivision;
            }

            lastCurrent = current;

            while(current <= loVal) {
                lastCurrent = current;
                current += roundedDivision;
                current = IDB.Calculator.maybeRound(current, maxDecimalPlaces);
            }

            // lastCurrent is now the division boundary that falls at or immediately
            // below loVal, so start building the list.
            current = lastCurrent;

            while(current < hiVal) {
                boundaries.push(current);
                current += roundedDivision;
                current = IDB.Calculator.maybeRound(current, maxDecimalPlaces);
            }
            boundaries.push(current);
        }
        metrics.validate();

    }

    return result;

};


/**
 * Returns the number of decimal places for n, if and only if its toString() method
 * does not return a string representing the number in exponential notation.
 */
IDB.Calculator.decimalPlaces = function(n) { // (n:Number):int {

    var s = n.toString().toLowerCase();
    var dotIndex = s.indexOf(".");
    if(dotIndex < 0) return 0;
    var eIndex = s.indexOf("e");
    if(eIndex > 0) return 0;
    var lastIndex = s.length - 1;
    return lastIndex - dotIndex

};


/**
 * Rounds to the number of decimal places indicated by "places" only if
 * "places" is between 1 and 20 inclusive. Otherwise n is returned.
 */
IDB.Calculator.maybeRound = function(n, places) { // (n:Number, places:int):Number {
    if(places < 1 || places > 20) return n;
    return Number(n.toFixed(places));
};





/*====================================================================================================*/
IDB.DateUtil = {};

IDB.DateUtil.DEFAULT_UI_STRINGS = {
	sunday:				'Sunday',
	sun:				'Sun',
	monday:				'Monday',
	mon:				'Mon',
	tuesday:			'Tuesday',
	tue:				'Tue',
	wednesday:			'Wednesday',
	wed:				'Wed',
	thursday:			'Thursday',
	thu:				'Thu',
	friday:				'Friday',
	fri:				'Fri',
	saturday:			'Saturday',
	sat:				'Sat',

	january:			'January',
	jan:				'Jan',
	february:			'February',
	feb:				'Feb',
	march:				'March',
	mar:				'Mar',
	april:				'April',
	apr:				'Apr',
	may:				'May',
	june:				'June',
	jun:				'Jun',
	july:				'July',
	jul:				'Jul',
	august:				'August',
	aug:				'Aug',
	september:			'September',
	sep:				'Sep',
	october:			'October',
	oct:				'Oct',
	november:			'November',
	nov:				'Nov',
	december:			'December',
	dec:				'Dec'
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.DateUtil._Init = function() {
	var s = IDB.DateUtil.DEFAULT_UI_STRINGS;

	IDB.DateUtil._MonthsFull = [
		s.january,
		s.february,
		s.march,
		s.april,
		s.may,
		s.june,
		s.july,
		s.august,
		s.september,
		s.october,
		s.november,
		s.december
	];

	IDB.DateUtil._MonthsAbbrev = [
		s.jan,
		s.feb,
		s.mar,
		s.apr,
		s.may,
		s.jun,
		s.jul,
		s.aug,
		s.sep,
		s.oct,
		s.nov,
		s.dec
	];

	IDB.DateUtil._DaysFull = [
		s.sunday,
		s.monday,
		s.tuesday,
		s.wednesday,
		s.thursday,
		s.friday,
		s.saturday
	];

	IDB.DateUtil._DaysAbbrev = [
		s.sun,
		s.mon,
		s.tue,
		s.wed,
		s.thu,
		s.fri,
		s.sat
	];
};

IDB.DateUtil._Init();


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.DateUtil.format = function(date, formatString, truncateMidnight) {
	if ( date == null ) {
		return '';
	}

	//TODO: add "truncateMidnight" functionality

	/*if ( IDB.StringUtil.trimToNull(formatString) == null ) {
		return (truncateMidnight ? truncateMidnight(toTimestamp(date)) : toTimestamp(date));
	}*/

	var s = formatString;
	var year = date.getFullYear()+'';
	var hours = date.getHours() % 12;
	var padFunc = IDB.DateUtil._nullPadLeft;

	s = s.split('a').join('^');
	s = s.split('M').join('�');

	s = s.split('yyyy').join(date.getFullYear());
	s = s.split('yy').join(year.substr(2));

	s = s.split('dd').join(padFunc(date.getDate(),2));
	s = s.split('d').join(date.getDate());

	s = s.split('HH').join(padFunc(date.getHours(),2));
	s = s.split('H').join(date.getHours());

	s = s.split('hh').join(padFunc((hours == 0) ? 12 : hours, 2));
	s = s.split('h').join((hours == 0) ? 12 : hours);

	s = s.split('mm').join(padFunc(date.getMinutes(),2));
	s = s.split('m').join(date.getMinutes());

	s = s.split('ss').join(padFunc(date.getSeconds(),2));
	s = s.split('s').join(date.getSeconds());

	s = s.split('SSS').join(padFunc(date.getMilliseconds(),3));
	s = s.split('SS').join(padFunc(date.getMilliseconds(),2));
	s = s.split('S').join(date.getMilliseconds());

	s = s.split('����').join(IDB.DateUtil._MonthsFull[date.getMonth()]);
	s = s.split('���').join(IDB.DateUtil._MonthsAbbrev[date.getMonth()]);
	s = s.split('��').join(padFunc(date.getMonth()+1,2));
	s = s.split('�').join(date.getMonth()+1);

	s = s.split('EEEE').join(IDB.DateUtil._DaysFull[date.getDay()]);
	s = s.split('E').join(IDB.DateUtil._DaysAbbrev[date.getDay()]);

	s = s.split('^').join(hours >= 12 ? 'PM':'AM');
	return s; //(truncateMidnight ? truncateMidnight(r) : r);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.DateUtil._nullPadLeft = function(value, len) {
	value = value+'';

	while ( value.length < len ) {
		value = '0'+value;
	}

	return value;
};
IDB.DivisionMetrics = function() {
    this.divisionSize = NaN;
    this.boundaries = null;
};

IDB.DivisionMetrics.prototype.validate = function() { // ():void  -- throws Error if invalid.
    var boundaries = this.boundaries;
    if(boundaries === null) throw new Error("DivisionMetrics.validate: boundaries array has not been set, overallRangeSize cannot be calculated.");
    if(boundaries.length < 2) throw new Error("DivisionMetrics.validate: boundaries array contains "
        + boundaries.length + " elements, it should contain at least 2: " + boundaries);

    // make sure elements are in ascending order with no duplicates.
    var lastOne = -1;
    for(var j=0, jj=boundaries.length; j<jj; j++) {
        var thisOne = boundaries[j];
        if(j>0 && (thisOne <= lastOne)) {
            IDB.log("DivisionMetrics.validate(): j=" + j + ", thisOne=" + thisOne + ", lastOne=" + lastOne);
            throw new Error("DivisonMetrics.validate: boundaries array contains invalid data; elements are not in ascending order: "
                + boundaries);
        }
        lastOne = thisOne;
    }
};

IDB.DivisionMetrics.prototype.getOverallRangeSize = function() { //():Number{
    var boundaries = this.boundaries;
    return boundaries[boundaries.length-1] - boundaries[0];
};

/**
 * Returns the index of the boundaries array element that is 0, or -1 if none 
 * of them do. Since a linear search is done each time, it might be a good idea
 * to save this value in a var.
 */
IDB.DivisionMetrics.prototype.getZeroIndex = function() { //():int {
    var boundaries = this.boundaries;
    for(var j=0, jj=boundaries.length; j<jj; j++) {
        if(boundaries[j] === 0) return j;
    }
    return -1;
};

IDB.DivisionMetrics.prototype.getMinBoundary = function() { //():Number {
    return this.boundaries[0];
};

IDB.DivisionMetrics.prototype.getMaxBoundary = function() { //():Number {
    var boundaries = this.boundaries;
    return boundaries[boundaries.length-1];
};

IDB.DivisionMetrics.prototype.toString = function() { //():String {
    return ("DivisionMetrics[divisionSize=" + this.divisionSize
        + ",minBoundary=" + this.getMinBoundary() + ",maxBoundary=" + this.getMaxBoundary() + ",overallRangeSize="
        + this.getOverallRangeSize() + ",zeroIndex=" + this.getZeroIndex() + ",boundaries=" + this.boundaries + "]");
};








/*global IDB, Kinetic */
/*====================================================================================================*/
IDB.KineticUtil = {};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.KineticUtil.setMask = function(group, mask) { //mask must be added to the display list
	var draw = group.drawScene;
	mask.fill(null);
	
	group.drawScene = function(canvas) {
		var c = canvas.context;
		c.save();
        if(Kinetic.version === "5.0.1") {
            c._applyTransform(mask); //Kinetic v5.0.1
        }
        else {
            group.getLayer()._applyTransform(mask, c); //Kinetic v5.1.0 or later
        }
		mask.sceneFunc().call(mask, c);
		c.clip();
		c.reset();
		draw.call(this, canvas);
		c.restore();
	};
};

/*====================================================================================================*/
IDB.MathCache = {};
IDB.MathCache._Cos = {};
IDB.MathCache._Sin = {};
IDB.MathCache._AbsCos = {};
IDB.MathCache._AbsSin = {};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MathCache.cos = function(r) {
	var cache = IDB.MathCache._Cos;
	var result = cache[r];

	if ( IDB.isUndef(result) ) {
		cache[r] = result = Math.cos(r);
	}

	return result;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MathCache.sin = function(r) {
	var cache = IDB.MathCache._Sin;
	var result = cache[r];

	if ( IDB.isUndef(result) ) {
		cache[r] = result = Math.sin(r);
	}

	return result;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MathCache.absCos = function(r) {
	var cache = IDB.MathCache._AbsCos;
	var result = cache[r];

	if ( IDB.isUndef(result) ) {
		cache[r] = result = Math.abs(IDB.MathCache.cos(r));
	}

	return result;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MathCache.absSin = function(r) {
	var cache = IDB.MathCache._AbsSin;
	var result = cache[r];

	if ( IDB.isUndef(result) ) {
		cache[r] = result = Math.abs(IDB.MathCache.sin(r));
	}

	return result;
};

/*====================================================================================================*/
IDB.MathUtil = {};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MathUtil.calculateRotatedBounds = function(width, height, degrees) {
	var rad = IDB.MathUtil.toRadians(degrees);
	var cos = IDB.MathCache.absCos(rad);
	var sin = IDB.MathCache.absSin(rad);

	return {
		width: height*sin + width*cos,
		height: height*cos + width*sin
	};
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MathUtil.clamp = function(val, lo, hi) {
	return (val < lo ? lo : (val > hi ? hi : val));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MathUtil.toRadians = function(degrees) {
	return degrees/180*Math.PI;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MathUtil.modulo = function(a, n) {
    if(isNaN(n) || n <= 0) {
        return NaN;
    }
    return ((a%n)+n)%n;
};

/*====================================================================================================*/
IDB.RangeHelper = {};

		
////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.RangeHelper.getRangeForValue = function(value, sortedRanges, upperBound, extendLowerBound) {
	upperBound = IDB.ifUndef(upperBound, Number.POSITIVE_INFINITY);
	extendLowerBound = IDB.ifUndef(extendLowerBound, false);

    var candidate = null;
	var len = sortedRanges.length;

    for ( var j = 0 ; j < len ; j++ ) {
        var range = sortedRanges[j];

        if ( value < range.val ) {
			return (extendLowerBound && candidate == null ? range : candidate);
        }

        candidate = range;
    }

    if ( candidate != null && value <= upperBound ) {
		return candidate;
	}

    return null;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.RangeHelper.findRangeForValue = function(value, sortedRanges, highestRangeDisplayed) {
    if ( sortedRanges == null || sortedRanges.length == 0 ) {
        return null;
    }

	var range = sortedRanges[0];
    var candidate = null;

    if ( value >= range.val ) {
        candidate = range;
    }

    var upperBound = Number.POSITIVE_INFINITY;
	var len = sortedRanges.length;

    if ( !highestRangeDisplayed ) {
        len = sortedRanges.length-1;
        upperBound = sortedRanges[len].val;
    }

    for ( var j = 0 ; j < len ; j++ ) {
        range = sortedRanges[j];
        
		if ( value < range.val ) {
            return candidate;
        }

        candidate = range;
    }

    if ( candidate != null && value <= upperBound ) {
		return candidate;
	}

    return null;
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ChartParameterInputPanel

IDB.ChartParameterInputPanel = function($gframe, paramSettings, params, callbacks) {
    var me = this;
    if(paramSettings.paramLocation == "floating") {
        me.chartParameterInputPanel =  new IDB.FloatingChartParameterInputPanel($gframe, paramSettings, params, callbacks);
    }
    else {
        me.chartParameterInputPanel =  new IDB.DockedChartParameterInputPanel($gframe, paramSettings, params, callbacks);
    }
};

IDB.ChartParameterInputPanel.prototype.remove = function() {
    this.chartParameterInputPanel.remove();
    this.chartParameterInputPanel = null; 
};

IDB.ChartParameterInputPanel.prototype.update = function() {
    this.chartParameterInputPanel.update();
};

IDB.ChartParameterInputPanel.prototype.adjustDimensions = function(gheight, gwidth) {
    if(this.chartParameterInputPanel.adjustDimensions) {
        return this.chartParameterInputPanel.adjustDimensions(gheight, gwidth);
    }
    else {
        return {height:gheight, width:gwidth};
    }
};

IDB.ChartParameterInputPanel.prototype.resize = function() {
    if(this.chartParameterInputPanel.resize) {
        this.chartParameterInputPanel.resize();
    }
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ChartParameterInputPrompt

IDB.ChartParameterInputPrompt = function($parent, data, callbacks) {
    var me = this, paramInputCallbacks = {}, $div, chartTitle, message;
    me.$parent = $parent;
    me.data = data;
    me.callbacks = callbacks || {};
    if(me.callbacks.chartParameterValuesChanged) {
        paramInputCallbacks.paramValuesChanged = me.callbacks.chartParameterValuesChanged;
    }  

    $div = $("<div>").css({"position":"absolute", "border":"1px solid black", "background-color":"white", "z-index":"100", "padding":"10px"}).appendTo($('#curdash'));
    me.$div = $div;

    chartTitle = data.chartTitle;
    message = chartTitle?IDB.format("param.html5.chart_parameters_required_title", chartTitle):IDB.format("param.html5.chart_parameters_required");

    me.paramInputPanel = new IDB.ParamInputPanel($div, $.extend({styles:{"font-size":"small"}, paramMessage:message, applyLabel:IDB.format("param.html5.show_chart"), preventRefreshOnCommit:true}, data), paramInputCallbacks);
    me.sizeAndPosition(); 
    me.$div.draggable();
    $('.param-control-textbox', $div).on('click', function(evt) {evt.target.focus()});
};


IDB.ChartParameterInputPrompt.prototype.applyStyles = function() {
    this.$div.find(".hasDatepicker").parent().css({display:"inline-flex"});
};

IDB.ChartParameterInputPrompt.prototype.update = function() {
    this.paramInputPanel.update();
};

IDB.ChartParameterInputPrompt.prototype.close = function() {
   this.$div.remove();
   this.paramInputPanel = null;
   this.$div = null; 
};

IDB.ChartParameterInputPrompt.prototype.resize = function() {
    this.sizeAndPosition();
};

IDB.ChartParameterInputPrompt.prototype.sizeAndPosition = function() {
    var me = this;
    me.$div.height(300);
    me.$div.width(400);
    me.$div.position({my:"center", at:"center", collision:"flipfit", of:me.$parent});
    (me.paramInputPanel).sizeAndPosition(me.$div); 
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.DashboardParameterInputPanel

IDB.DashboardParameterInputPanel = function($parent, data, callbacks) {
    var me = this, paramInputCallbacks = {}, $div;
    me.$parent = $parent;
    me.data = data;
    me.callbacks = callbacks || {};
    if(me.callbacks.dashboardParameterValuesChanged) {
        paramInputCallbacks.paramValuesChanged = me.callbacks.dashboardParameterValuesChanged;
    }  
    $div = $("<div>").appendTo(me.$parent);
    me.$div = $div;
    $div.css({"height":"100%", "width":"100%"}); 
 
    me.paramInputPanel = new IDB.ParamInputPanel($div, $.extend({paramMessage:IDB.format('param.html5.dashboard_parameters_required'), numCols:3, preventRefreshOnCommit:true, applyLabel:IDB.format('param.html5.show_dashboard')}, data), paramInputCallbacks);
    $(window).resize(IDB.listener(me, "position"));
};

IDB.DashboardParameterInputPanel.prototype.position = function() {
    this.paramInputPanel.sizeAndPosition(this.$div);
};

IDB.DashboardParameterInputPanel.prototype.applyStyles = function() {
    this.$div.find(".hasDatepicker").parent().css({display:"inline-flex"});
};

IDB.DashboardParameterInputPanel.prototype.update = function() {
    this.paramInputPanel.update();
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.DockedChartParameterInputPanel

IDB.DockedChartParameterInputPanel = function($gframe, paramSettings, params, callbacks) {
    var me = this;
    me.paramSettings = paramSettings;
    me.paramLocation = paramSettings.paramLocation;
    me.$gframe = $gframe;
    me.params = params;
    me.chartParameterValuesChanged = callbacks.chartParameterValuesChanged;
    me.resizeCallback = callbacks.resize;

    me.createFrameDivForParameters();
    me.createParamInputDiv();
    me.layoutFrameForParameters();
    if(me.paramSettings.paramVisibility == "minimizeParams") {
        $(".param-closer", me.$chartParamInputPanel).click();
    }

    var color = me.paramSettings.textColor;
    if(color) {
        $(".param-label", me.$chartParamInputPanel).css({"color":color.css});
    }
};


IDB.DockedChartParameterInputPanel.PARAM_PANES_MAP = {
  right:"east",
  left:"west",
  top:"north",
  bottom:"south"
};

IDB.DockedChartParameterInputPanel.PARAM_LAYOUT_SETTINGS = {
         defaults: {
             togglerClass:"param-toggler",
             resizerClass:"param-resizer",
             buttonClass:"param-button",
             togglerTip_open:"Hide parameters",
             spacing_closed:21, // wider space when closed
             togglerLength_closed:21, // make toggler 'square' - 21x21
             togglerLength_open:0, // NONE - using custom togglers INSIDE panes
             togglerTip_closed:"Show parameters",
             slidable:false,
             resizable:false,
             spacing_open:0,
             fxName: "slide",
             fxSpeed_open:45,
             fxSpeed_close:200,
             fxSettings_open:{easing: "easeInQuint"},
             fxSettings_close:{easing: "easeOutQuint"}
         },
         east: {
             size:0.15,
             togglerAlign_closed:"top"
         },
         west: {
             size:0.15,
             togglerAlign_closed:"top"
         },
         north: {
             size:0.10,
             togglerAlign_closed:"right"
         },
         south: {
             size:0.10,
             togglerAlign_closed:"right"
         }
 };

IDB.DockedChartParameterInputPanel.prototype.createFrameDivForParameters = function() {
    var me = this, $gslice;
    me.$frameForParameters = $("<div></div>").addClass("idb-pf-div");
    me.frameId = me.$gframe.data("frameId");
    $gslice = me.$gframe.parent();
    me.$gframe.detach();
    me.$frameForParameters.appendTo($gslice);
    var dimensionStyles = me.$gframe.css(["max-height", "max-width"]);
    me.$frameForParameters.css(dimensionStyles);
    var height = dimensionStyles['max-height'];
    if(height && height !== "none") {
        me.$frameForParameters.outerHeight(dimensionStyles['max-height']);
    }
    else {
        //
        // This is a bit of hack to get around a warning about a 0 height container in the layout
        // plugin.
        //
        me.$frameForParameters.outerHeight("1px");
    }

    me.$gframe.appendTo(me.$frameForParameters);
};

IDB.DockedChartParameterInputPanel.prototype.createParamInputDiv = function() {
    var me = this, pane, direction, callbacks;
    pane = IDB.DockedChartParameterInputPanel.PARAM_PANES_MAP[me.paramLocation] || "east";
    me.$chartParamInputPanel = $("<div></div>").addClass("ui-layout-" + pane  + " param-pane").css({"overflow":"hidden"}).appendTo(me.$frameForParameters);
    me.$chartParamInputPanel.data({"pane":pane, "isClosed":false});
    $("<span></span>").addClass("param-closer").prependTo(me.$chartParamInputPanel);
    if(me.paramLocation === "top" || me.paramLocation === "bottom") {
        direction = IDB.PositionUtils.Direction.HORIZONTAL;
    }
    else {
        direction = IDB.PositionUtils.Direction.VERTICAL;
    }
    callbacks = {paramValuesChanged:IDB.listener(me,"chartParameterValuesChanged")};
    me.chartParamInputPanel = new IDB.ParamInputPanel(me.$chartParamInputPanel, {params:me.params, direction:direction, styles:{"font-size":"small"}}, callbacks);
};

IDB.DockedChartParameterInputPanel.prototype.layoutFrameForParameters = function() {
    var me = this, pane, layoutSettings, layout;
    me.$gframe.addClass("ui-layout-center");
    pane = me.$chartParamInputPanel.data("pane");
    layoutSettings = $.extend({}, IDB.DockedChartParameterInputPanel.PARAM_LAYOUT_SETTINGS);
    
    if(pane == "north" || pane == "south") {
        layoutSettings[pane].size = me.chartParamInputPanel.$div.outerHeight();
    }
    else {
        layoutSettings[pane].size = me.chartParamInputPanel.$div.outerWidth();
    }

    layoutSettings[pane].onopen = IDB.listener(me, "onPaneOpen");
    layoutSettings[pane].onclose = IDB.listener(me, "onPaneClose");
    layout = me.$frameForParameters.layout(layoutSettings);

    //
    // In general jQuery acts on sets specified by a selector, so if the selector doesn't yield anything
    // jQuery doesn't do anything. The jQuery layout plugin does some checks to ensure the selectors 
    // yield results. This usually isn't an issue, but when the dashboard is detached before the chart
    // is done loading (like if the back button is clicked before the chart is done loading), the
    // jQuery layout plugin throws errors. So, we will check to see if the indicated selector exists 
    // before calling this method on the layout.
    //
    var buttonSelector = "#frame-" + me.frameId + " .param-closer";
    if($(buttonSelector).length > 0) { 
        layout.addCloseBtn(buttonSelector, pane);
    }
};

IDB.DockedChartParameterInputPanel.prototype.onPaneOpen = function(paneName, $element, state, options, layout) {
    this.$chartParamInputPanel.data("isClosed", false);
    if(this.resizeCallback) {
        this.resizeCallback();
    }
};

IDB.DockedChartParameterInputPanel.prototype.onPaneClose = function(paneName, $element, state, options, layout) {
    this.$chartParamInputPanel.data("isClosed", true);
    if(this.resizeCallback) {
        this.resizeCallback();
    }
};

IDB.DockedChartParameterInputPanel.prototype.remove = function() {
    this.$chartParamInputPanel.empty();
    this.chartParamInputPanel.remove();
    this.chartParamInputPanel = null; 
};

IDB.DockedChartParameterInputPanel.prototype.update = function() {
    this.chartParamInputPanel.update();
};

IDB.DockedChartParameterInputPanel.prototype.adjustDimensions = function(gheight, gwidth) {
    var me = this, isClosed = false, hpad=10, vpad=10;
    if(me.$frameForParameters) {
          me.$frameForParameters.css({"max-width":gwidth, "max-height":gheight});
          me.$frameForParameters.outerHeight(gheight);
    }

    if(me.$chartParamInputPanel) {
         //adjust width or height to account for paraminput panel
         // if paramInpuPanel top or bottom, adjust height
         // if paramInputpanel left or right, adjust width
         isClosed = me.$chartParamInputPanel.data("isClosed");
         
         if(me.paramLocation === "top" || me.paramLocation === "bottom") {
             vpad = 5;
             if(isClosed) {
                  gheight -= 21 + 3; // current size of resizer
              }
             else {
                 gheight -= this.$chartParamInputPanel.height();
             }
             me.$chartParamInputPanel.css({"width":gwidth});
         }
         else {
            hpad = 5;
            if(isClosed) {
                gwidth -= 21 + 3;
            }
            else {
                gwidth -= me.$chartParamInputPanel.width();
            }
            me.$chartParamInputPanel.css({"height":gheight});
         }
    }
    
    return {height:gheight, width:gwidth, hpad:hpad, vpad:vpad};
};

IDB.DockedChartParameterInputPanel.prototype.resize = function() {
    var isClosed = this.$chartParamInputPanel.data("isClosed");
    if(!isClosed) {
      this.chartParamInputPanel.resize();
   }
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.FloatingChartParameterInputPanel

IDB.FloatingChartParameterInputPanel = function($gframe, paramSettings, params, callbacks) {
    var me = this, $div, paramInputCallbacks, 
    bgc = paramSettings.backgroundColor, color = paramSettings.textColor;

    me.paramSettings = paramSettings;
    me.$gframe = $gframe;
    me.params = params;
    me.chartParameterValuesChanged = callbacks.chartParameterValuesChanged;


    $div = $("<div class='floating-param-input-panel'>").css({"position":"absolute", "border":"1px solid black",  "z-index":"100", "padding":"10px"}).appendTo($('#curdash'));

    me.$div = $div;
    $div.css("background-color", bgc.css);

    paramInputCallbacks = {paramValuesChanged:IDB.listener(me,"chartParameterValuesChanged")};
    me.chartParamInputPanel = new IDB.ParamInputPanel($div, {styles:{"font-size":"small"}, params:me.params}, paramInputCallbacks);
    me.sizeAndPosition(); 
    me.$div.draggable();

    $('.param-control-textbox', $div).on('click', function(evt) {evt.target.focus()});

    if(color) {
        $(".param-label", $div).css({"color":color.css});
    }

    me.$minimizeButton = $("<span>").addClass("param-closer param-button-close param-button-close-south").appendTo($div);
    me.$restoreButton = $("<span>").addClass("param-button-restore param-toggler-south-closed").appendTo($gframe);
    me.$minimizeButton.click(IDB.listener(me, "minimize"));
    me.$restoreButton.click(IDB.listener(me, "restore"));

   if(me.paramSettings.paramVisibility === "minimizeParams") {
       me.minimize();    
   }
   else {
       me.$restoreButton.hide();
   }
};


IDB.FloatingChartParameterInputPanel.prototype.remove = function() {
    this.$div.remove();
    this.chartParamInputPanel = null;
    this.$div = null; 
};

IDB.FloatingChartParameterInputPanel.prototype.update = function() {
    this.chartParamInputPanel.update();
};

IDB.FloatingChartParameterInputPanel.prototype.applyStyles = function() {
    this.$div.find(".hasDatepicker").parent().css({display:"inline-flex"});
};

IDB.FloatingChartParameterInputPanel.prototype.resize = function() {
    this.size();
};

IDB.FloatingChartParameterInputPanel.prototype.size = function() {
    var me = this, dimensions, height;
    dimensions = me.chartParamInputPanel.getDimensions();
    if(dimensions.height > 0 && dimensions.width > 0) {
        height = Math.max(100, Math.min(dimensions.height, me.$gframe.height()/3));
        me.$div.height(height);
        me.$div.width(dimensions.width);
    }
};


IDB.FloatingChartParameterInputPanel.prototype.position = function() {
    if (IDB.PHONEMODE)
    {
        this.$div.position({my:"right top", at:"right top", collision:"flip", of:this.$gframe});
    }
    else
    {
        this.$div.position({my:"right top", at:"right top", collision:"flipfit", of:this.$gframe});
    }
};

IDB.FloatingChartParameterInputPanel.prototype.sizeAndPosition = function() {
    this.size();
    this.position();
    this.chartParamInputPanel.sizeAndPosition(this.$div);
};

IDB.FloatingChartParameterInputPanel.prototype.minimize = function() {
    this.$div.hide();
    this.$restoreButton.show();
};

IDB.FloatingChartParameterInputPanel.prototype.restore = function() {
    this.$div.show();
    this.$restoreButton.hide();
};
///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.Param

IDB.Param = function($parent, data) {
    var me = this;

    me.$parent = $parent;
    me.data = data;
    me.dataType = data.dataType;
    me.shortDataType = data.dataType.charAt(0).toLowerCase();
    me.paramName = data.paramName;
    me.displayName = data.displayName;
    me.multiVal = !!data.multiVal;
    me.editable = !!data.editable;
    me.refreshOnCommit = !!data.refreshOnCommit;
    me.selections = new IDB.SelectionMap();
    me.cascadeSources = data.cascadeSources || null;
    me.showPrompt = !!data.valuesNeeded;
    me.changeListeners = [];
    me.programmaticChangeListeners = [];
    me.valueFromDefault = !!data.valueFromDefault;
    me.widgetName = "";
    me.addLabel();
    me.addControl();
};

IDB.Param.UNSUPPORTED_CONTROL_TYPES = ["slider", "spinner", "slideSpin"];

IDB.Param.SOURCE_PARAMS           = "S";
IDB.Param.SOURCE_FILTERS          = "F";
IDB.Param.DASHBOARD_PARAMS        = "D";
IDB.Param.DASHBOARD_DEFAULTS      = "Y";
IDB.Param.TARGET_PARAMS           = "T";
IDB.Param.TARGET_DEFAULTS         = "Z";
IDB.Param.SOURCE_DASHBOARD_PARAMS = "R";

IDB.Param.BOTH_SOURCE_PARAMS = "TSRFDZ";
IDB.Param.BOTH_FILTERS       = "TFSRDZ";
IDB.Param.ONLY_SOURCE_PARAMS = "TSRDZ";
IDB.Param.ONLY_FILTERS       = "TFDZ";
IDB.Param.ONLY_TARGET_PARAMS = "TDZ";

IDB.Param.prototype.addLabel = function() {
    var me = this,
        text = ((me.data.required)?"*":"") + me.data.displayName;
    me.$label = $("<td>").appendTo(me.$parent);
    me.$label.text(text);
    me.$label.addClass("param-label"); 
};

IDB.Param.prototype.addControl = function() {
    var me = this, data, isDate, choices, editable, prompt, multiVal, values,
        $option, choice, selected, items, maxLength, showPrompt;

    //
    // Make a copy of the input data because we will be mucking with it.
    //
    data = $.extend({}, me.data);

    isDate = (data.dataType === "Datetime");

    //
    // Unsupported control types are converted to choices if choices are provided by the server.
    //
    if(IDB.Param.UNSUPPORTED_CONTROL_TYPES.indexOf(data.controlType) >= 0 && data.choices) {
        if(isDate) {
            data.value = IDB.DatetimeUtils.normalizeDatetimeString(data.value);
        }

        data.controlType = "choice";
        data.editable = false;
        if(data.value !== 0 && !data.value) {
            data.value = data.min;
        }

        // 
        // Set the value to the nearest choice. This replicates the behavior of the Flex control.
        //
        data.value = me.getValidValue(data.value);
        me.data.value = data.value;
    }

    switch(data.controlType) {
        case "choice":
            choices = data.choices || [];
            if(!data.valuesNeeded) {
                var selection = data.selection || {};
                me.selections.put(selection, choices);
            }
            editable = data.editable || (choices.length === 0 && data.convert);
            
            if(me.dataType === "Datetime") {
                me.choicesHaveMillis = (choices.length > 0) &&  choices.some(function(choice) {return String(choice[0]).length > 19;});
            }

            if(me.multiVal) {
            //    values = !IDB.Param.valueIsMissing(me.data.value)?me.data.value:[];
                if(IDB.isNil(me.data.value)) {
                    values = [];
                }
                else {
                    values = Array.isArray(me.data.value)?me.data.value:[me.data.value];
                }
            }
            else {
                values = [me.data.value];
            }

            if(!values) {
                values = [];
            }

            if(!editable || me.multiVal) {
                me.$control = $("<td><select data-native-menu='true'/></td>").insertAfter(me.$label).find("select");
                if(me.multiVal) {
                    me.$control.attr({multiple:"multiple", size:5});
                }
                prompt = !editable;
                for(var i=0; !data.valuesNeeded && i < choices.length; i++) {
                    choice = choices[i];
                    if(!me.showPrompt) {
                        selected = values.some(function(v) {
                                              return IDB.Param.valuesAreEqual(data.dataType, v, choice[0])})
                                              ?"selected":"";
                        if(selected) {
                            prompt = false;
                        }
                    }
                    else {
                        selected = "";
                    }
                    $option = $("<option " + selected + " />").appendTo(me.$control);
                    $option.attr("value", choice[0]).text(choice[1]);
                }

                if(me.multiVal) {
                     var multiOptions = {
                           selectedList:1000,
                           minWidth:200,
                           classes:"param",
                           height:(choices.length > 5)?175:"auto"};
                     if(editable) {
                         me.$control.multiselecteditable($.extend({value:IDB.Param.normalizeValue(data.dataType, values, me.choicesHaveMillis)}, multiOptions));
                         me.widgetName = "multiselecteditable";
                     }
                     else {
                         me.$control.multiselect(multiOptions);
                         me.widgetName = "multiselect";
                     }
                }
                else {
                    if(prompt || me.showPrompt) {
                        $option = $("<option value='SELECT_PROMPT' disabled selected>").prependTo(me.$control);
                        $option.text(IDB.format("param.html5.select_prompt")); 
                        me.$control.on("change", function(event) {
                                                       var $control = $(event.target);
                                                       if($control.val() !== 'SELECT_PROMPT') {
                                                           $("option[value='SELECT_PROMPT']", $control).remove();
                                                       }
                                                   });
                    }
                }
                var disabled = !!data.valuesNeeded;
                if(disabled) {
                    me.disable(true);
                }
            }
            else {
                me.$control = $("<td><input type='text'/></td>").insertAfter(me.$label).find("input");
                if(choices.length !== 0 || !data.convert) {
                    items = choices.map(function(choice) {return {value:choice[0], label:choice[1]};}); 
                    me.$control.combobox({source:items, editable:true, value:data.value,
                                            equals:function(x,y) {
                                                     return IDB.Param.valuesAreEqual(data.dataType,x,y)}});
                    me.$control.combobox("value", data.value);
                    me.widgetName = "combobox";
                }
                else {
                    me.$control.val(data.value);
                }
            }
            break;
        default:
            me.$control = $("<td><input type='text'/></td>").appendTo(me.$parent).find("input");
            maxLength = data.maxLength || 50;
            me.$control.attr("maxlength", maxLength);
            if(me.multiVal) {
                me.$control.multitextinput({value:data.value, isDate:isDate/*, css:{"font-size":"small"}*/});
                me.widgetName = "multitextinput";
            }
            else {
                me.$control.val(data.value);
 
                //
                // Class to deal with text input in a draggable floating panel not receiving focus.
                //
                me.$control.addClass("param-control-textbox");

                if(isDate) {
                    me.$control.parents('.param-input-panel').css({'z-index':101});
                      me.$control.datepick({
                              showTrigger:'<img src="' + IDB.config.contextRoot + 'html5/images/calendar.gif">',
                              showOnFocus:false,
                              dateFormat:'yyyy-mm-dd',
                              constrainInput:false,
                              changeMonth:false,
                              onSelect:function(dates) {me.$control.trigger("change");}});
                      me.widgetName = "datepick";
                      var $pickerButton =  $(".datepick-trigger", me.$control.parent());
                      $pickerButton.css({position:"relative", "margin-left":"-20px", "top":"3px"}); 
                }
            }

            break;
    }
    me.$control.parent('td').addClass('param-control');
};

Object.defineProperty(IDB.Param.prototype, "value", {
  get: function() {
                    var me = this, v;
                    if(me.$control.is(".idb-combobox-input")) {
                        v = me.$control.combobox("value");
                    }
                    else {
                        if(me.multiVal) {
                            if(me.$control.is(":text")) {
                                v = me.$control.multitextinput("value");
                            }
                            else if(me.$control.is("select")) {
                               if(me.editable) {
                                   v = me.$control.multiselecteditable("value");
                               }
                               else {
                                   v = me.$control.multiselect("value");
                               }
                            }
                            else {
                                v = me.$control.val();
                            }
                        }
                        else {
                            v = me.$control.val();
                        }
                    }
                    if(v === "SELECT_PROMPT") {
                        v = null;
                    }
                    if(me.multiVal && v !== null && !(v instanceof Array)) {
                        v = [v];
                    }
                    return v; 
       },

  set: function(newValue) {
      this.setValue(newValue, this.data.programmaticChange); 
    }
});

IDB.Param.prototype.setValue = function(newValue, triggerChange) {
  var me = this, data = me.data, choice, choices = data.choices;
  if(!IDB.isUndef(newValue)) {
    if(IDB.Param.isValidValue(me.data, newValue)) {
      newValue = me.getValidValue(newValue);
      if(newValue !== null) {
          if(me.$control.is(".idb-combobox-input")) {
              me.$control.combobox("value", newValue);
          }
          else {
              if(me.multiVal && me.$control.is(":text")) {
                  me.$control.multitextinput("value", newValue);
              }
              else if(me.$control.is("select")) {
                  if(me.multiVal) {
                      newValue = IDB.Param.normalizeValue(data.dataType, newValue, me.choicesHaveMillis);
                      if(me.editable) {
                          me.$control.multiselecteditable("value", newValue);
                      }
                      else {
                          me.$control.multiselect("value", newValue);
                      }
                      if(triggerChange) {
                          me.$control.trigger("programmaticChange");
                      }
                  }
                  else {
                    for(var i = 0; choices != null && i < choices.length; i++) {
                        choice = choices[i];
                        if (IDB.Param.valuesAreEqual(data.dataType, newValue, choice[0])) {
                                newValue = choice[0];
                                break;
                            }
                        }
                        me.$control.val(newValue);
                        if(triggerChange) {
                            me.$control.trigger("programmaticChange");
                        }
                    }
              }
              else {
                  me.$control.val(newValue);
                  if(triggerChange) {
                      (me.$control.trigger("programmaticChange"));
              }
          }
      }
      me.data.programmaticChange = false;
    }
    else if(IDB.Param.maybeValidValue(me.data, newValue)) {
        me.data.maybeValue = newValue;
    }
  }
}
};

IDB.Param.prototype.getValidValue = function(newValue) {
    var me = this, data = me.data, isDate;
    if(newValue !== undefined && IDB.Param.isValidValue(data, newValue)) {
        isDate = (data.dataType == "Datetime");

       //
       // Unsupported control types are converted to choices if choices are provided by the server.
       //
        if(IDB.Param.UNSUPPORTED_CONTROL_TYPES.indexOf(data.controlType) >= 0 && data.choices) {
            // 
            // Set the value to the nearest choice. This replicates the behavior of the Flex control.
            //
            var closestValue = IDB.Param.closestValue(data.dataType, newValue, data.min, data.max, data.increment);
            var nearestChoice = IDB.Param.nearestChoice(data.dataType, closestValue, data.choices);
            newValue = nearestChoice?nearestChoice[0]:null;
        }
        return  newValue;
    }
    else {
        return null;
    }
};


IDB.Param.prototype.setParent = function($newParent) {
    var me = this;
    me.$label.detach();
    me.$control.detach();
    me.$label.appendTo($newParent); 
    me.$control.appendTo($newParent); 
    me.$parent = $newParent;
};

IDB.Param.prototype.validate = function() {
    var me = this, missing, valid,
        value = me.value, 
        required = me.data.required,
        dataType = me.data.dataType,
        values;

    missing = IDB.Param.valueIsMissing(value);

    if(required && missing) {
        me.indicateInvalid("param.html5.param_required");
        return false;
    }

    valid = true;

    if(!missing) {
       if(value instanceof Array) {
           values = value;
       }
       else {
           values = [value];
       }
       for(var i=0; i < values.length; i++) { 
           var v = values[i];
           if(dataType == "Number") {
               valid = IDB.NumberUtils.validate(v);
               if(!valid) {
                   me.indicateInvalid("param.html5.invalid_number");
                   return false;
               }
           }
           else if(dataType == "Datetime") {
               valid = IDB.DatetimeUtils.validate(v);
               if(!valid) {
                   me.indicateInvalid("param.html5.invalid_datetime");
                   return false;
               }
           }
           else if(String(v).indexOf(';') != -1) {
               me.indicateInvalid("param.html5.invalid_string");
               return false;
           }
       }
     }
       
     me.indicateValid();
     return true; 
};


IDB.Param.prototype.indicateInvalid = function(messageKey) {
        var me = this;
        me.$label.addClass('invalid-param-label'); 
        me.$control.addClass('invalid-param-control'); 
        me.validationMessage =  IDB.format(messageKey);
        me.$control.attr("title",me.validationMessage);
};
    
IDB.Param.prototype.indicateValid = function() {
        var me = this;
        me.$label.removeClass('invalid-param-label'); 
        me.$control.removeClass('invalid-param-control'); 
        me.validationMessage = null;
        me.$control.removeAttr("title");
};

IDB.Param.prototype.buildParamChoicesRequest = function(selection) {
    var me = this, data = me.data, request = {}, cmd;
    request.selection = selection;
    request.paramName = data.paramName;
    if(data.chartId) {
        cmd = "loadChartParamChoices";    
        request.chartId = data.chartId;
    }
    else {
        cmd = "loadDashboardParamChoices";    
        request.dashId = data.dashId;
    }
    return [cmd, request];
};

IDB.Param.prototype.getSelection = function(selection, callback) {
    var choices = this.selections.get(selection, null);
    if(choices == null) {
        var request = this.buildParamChoicesRequest(selection);
        IDB.CQ.enQ(request[0], [request[1]], function(results) {
                                                // error handling
                                                if(callback != null) {
                                                    callback(results.choices, results.defaultValue); 
                                                 }});
    }
    else {
        if(callback != null) {
            callback(choices);
        }
    }
};


IDB.Param.prototype.maybeAddCascadeSourceListeners = function(paramList) {
    var me = this, cascadeSources = me.data.cascadeSources;
    if(!me.isCascadeTarget()) {
        return;
    }
    
    cascadeSources.forEach(function(cascadeSource) {
                                var param = IDB.Param.getParamByName(paramList, cascadeSource);
                                  param.addChangeListener(me.onCascadeSourceChange.bind(me, paramList), true); 
                                  });
};

IDB.Param.prototype.isCascadeTarget = function() {
    var cascadeSources = this.data.cascadeSources;
    return (cascadeSources && cascadeSources.length > 0);
};

IDB.Param.getParamByName = function(paramList, name) {
    var matches = paramList.filter(function(param) {
                               return param.paramName === name;
                             });
    return (matches.length > 0)?matches[0]:null;
};

IDB.Param.prototype.getCascadeSourceValues = function(paramList) {
    if(!this.isCascadeTarget()) {
        return {};
    }

    var cascadeSourceValues = {};
    this.cascadeSources.forEach(function(cascadeSource) {
                          var param = IDB.Param.getParamByName(paramList, cascadeSource);
                          cascadeSourceValues[cascadeSource] = param.value; 
                        });
    return cascadeSourceValues;
};

IDB.Param.prototype.onCascadeSourceChange = function(paramList, evt) {
    if(!this.isCascadeTarget()) {
       return;
    }

    var cascadeSourceValues = this.getCascadeSourceValues(paramList);

    var valuesNeeded = false;
    for(var cascadeSource in cascadeSourceValues) {
        if(cascadeSourceValues[cascadeSource] == null) { //isNothing
            valuesNeeded = true;
            break;
        }
    }

    if(valuesNeeded) {
        this.disable(true);
        this.value = null;
    }
    else {
        this.getSelection(cascadeSourceValues, this.maybeSetChoices.bind(this, cascadeSourceValues, true, this.data.programmaticChange));
    } 
};

IDB.Param.prototype.maybeSetChoices = function(selection, retainParamValue, triggerChange, choices, defaultValue) {
    var me = this, data = me.data;
    me.data.choices = choices;
    me.data.selection = selection;
    me.selections.put(selection, choices);
    me.data.valuesNeeded = false;
    retainParamValue = !!retainParamValue;

    if(data.maybeValue != null) {
        data.value = data.maybeValue; 
        data.maybeValue = null;
    }
    else if(retainParamValue && !IDB.Param.valueIsMissing(me.value)) { 
        data.value = me.value;
    }
    else if(IDB.Param.valueIsMissing(data.value) && !IDB.Param.valueIsMissing(defaultValue)) {
        data.value = defaultValue;
    }
    
    me.showPrompt = IDB.Param.valueIsMissing(data.value);

    me.$control.closest('td').remove();
    me.addControl();
    for(var i = 0; i < me.changeListeners.length; i++) {
        me.addChangeListenerToControl(me.changeListeners[i]);
    }
    for(var i = 0; i < me.programmaticChangeListeners.length; i++) {
        me.addChangeListenerToControl(me.programmaticChangeListeners[i], true);
    }
    if(triggerChange) {
         me.$control.trigger("programmaticChange"); 
    }
};

IDB.Param.prototype.addChangeListener = function(listener, programmaticChange)  {
    this.changeListeners.push(listener);
    if(programmaticChange) {
        this.programmaticChangeListeners.push(listener);
    }
    this.addChangeListenerToControl(listener, programmaticChange);
};

IDB.Param.prototype.addChangeListenerToControl = function(listener, programmaticChange)  {
    this.$control.on(this.widgetName + "change", listener);
    if(this.widgetName === "datepick") {
        this.$control.on("change", listener);
    }
    if(programmaticChange) {
        this.$control.on("programmaticChange", listener);
    }
};

IDB.Param.prototype.disable = function(disabled)  {
     var ctrl = this.$control;
     if(disabled) {
         (this.widgetName)?ctrl[this.widgetName]("disable"):ctrl.attr("disabled", "disabled");
     }
     else {
         (this.widgetName)?ctrl[this.widgetName]("enable"):ctrl.removeAttr("disabled");
     } 
};

IDB.Param.toValueRequestParameters = function(param, prefix, increment, includeType) {
    var requestStr = prefix + "n" + increment + "=" + encodeURIComponent(param.paramName);
    if(includeType) {
        requestStr += ("&" + prefix + "t" + increment + "=" + encodeURIComponent(IDB.Param.getShortDataType(param.dataType)));        		
            }   
			
    requestStr += IDB.Param.valueToRequestParameters(prefix, increment, param.value);
    return requestStr;
};

IDB.Param.prototype.updateControlValue = function(updated) {
    var me = this;
    if(me.isCascadeTarget() && updated.selection && updated.choices) {
        me.maybeSetChoices(updated.selection, false, false,  updated.choices);
    }
    //me.value = me.maybeValue = updated.value;
    me.value = updated.value;
};


IDB.Param.valueToRequestParameters = function(prefix, increment, value) {
    var requestStr = "";

    if(value == null) {
        return requestStr; 
    }
	
    if(Array.isArray(value)) {
        for(var i = 0; i < value.length; i++) {	
            requestStr += ("&" + prefix + "v" + increment + "." + i + "=" + encodeURIComponent(String(value[i]) ? String(value[i]) : ""));
        }
    }
    else {
	requestStr += ("&" + prefix + "v" + increment + "=" + encodeURIComponent(String(value) ? String(value) : ""));				
   }
			
   return requestStr;
};

IDB.Param.toParamValue = function(obj) {
    return {paramName: obj.paramName,
            dataType: obj.dataType,
            value: obj.value};
};

IDB.Param.toParamValues = function(paramList, excludeDefaults) {
    if(paramList === null) {
        return null;
    }

    var paramValues = [];
    paramList.forEach(function(p) {
                    if(p.value !== null && (!excludeDefaults || !p.valueFromDefault)) {
                                  paramValues.push(IDB.Param.toParamValue(p));}
                    });
    return paramValues;
};

IDB.Param.updateParamValues = function(paramList, paramValues) {
    if(!paramValues || !paramList) {
	    return;
    }
    paramValues.every(function(paramValue) {
                         var results = paramList.filter(function(param) {
                                                           return param.paramName == paramValue.paramName;
                                                       }); 
                         if(results.length > 0) {
                            var param = results[0];
                            param.value = paramValue.value;
                            param.valueFromDefault = !!paramValue.valueFromDefault;
                            param.formattedValue = paramValue.formattedValue;
                            if(paramValue.selection) { 
                                param.selection = paramValue.selection;
                            }
                            if(paramValue.choices) {
                                param.choices = paramValue.choices;
                            }
                         }
                         return true;
                     });
};

IDB.Param.overlayParamSets = function(baseSet, overlay) {
    var overlayLen, baseLen, results, param, found, overlayIndex, baseIndex, baseParam;
    // start by making a copy of the base set
    results = baseSet.concat();

    if(!overlay) {
        return results;
    }
			
    overlayLen = overlay.length;
    baseLen = results.length;
    for(overlayIndex = 0; overlayIndex < overlayLen; overlayIndex++) {
        param = overlay[overlayIndex];
        // does a parameter with this paramName already exist in the base set?
        found = false;
        for(baseIndex = 0; baseIndex < baseLen; baseIndex++) {
            baseParam = results[baseIndex];

            // if we find a parameter with the same name, overlay it with the new one
            if(param.paramName == baseParam.paramName) {
                baseParam.value = param.value;
                baseParam.sourceProperties = param.sourceProperties;
                found = true;
                break;
            }
        }
        if(!found) {
            // if one wasn't found, add it to the list.
            results.push(param);
        }
    }

    return results;
};


IDB.Param.valuesAreEqual = function(dataType, v1, v2) {
    if(v1 === "" && v2 === "") return true;
    switch(dataType) {
        case "Datetime":
            return IDB.DatetimeUtils.areDatetimeStringsEqual(v1, v2);
        case "Number":
            return IDB.NumberUtils.areNumberStringsEqual(v1, v2);
        case "String":
            return IDB.StringUtils.areEqualWithConversion(v1, v2);
    }
};

IDB.Param.isParamCascadeTarget = function(param) {
    var cascadeSources = param.cascadeSources;
    return (cascadeSources && cascadeSources.length > 0);
};

//
// Dates are assumed to be normalized strings, JavaScript dates, or milliseconds after 1/1/1970. 
// 
IDB.Param.calculateDistance = function(dataType, v1, v2) {
    if(dataType == "Datetime") {
       return IDB.DatetimeUtils.dateDifference(v1, v2);
    }
    else {
        return v1 - v2;
    }
};

IDB.Param.nearestChoice = function(dataType, value, choices) {
    var i,choice, distance, nearestItem, nearestDistance;

    if(value !== 0 && !value) {
       return value; 
    }
        
    if(!choices || choices.length === 0) {
        return null;
    }

    nearestDistance = -1;

    for(i = 0; i < choices.length; i++) {
        choice = choices[i];
        distance = Math.abs(IDB.Param.calculateDistance(dataType, choice[0], value));  
        if(nearestDistance === -1 || distance < nearestDistance) {
            nearestDistance = distance;
            nearestItem = choice;
            if(nearestDistance === 0) {
                break;
            }
        }
     }

     return nearestItem || null;
};

//
// This logic is lifted directly from Flex NumericStepper.
//
IDB.Param.closestValue = function(dataType, value, minimum, maximum, stepSize) {
    var isDate, closest, parts, scale;

    isDate = (dataType === "Datetime");

    if(isDate) {
        value = IDB.DatetimeUtils.makeJavaScriptDate(value).getTime(); 
        stepSize = stepSize*IDB.DatetimeUtils.MILLISECONDS_PER_DAY;
        minimum = IDB.DatetimeUtils.makeJavaScriptDate(minimum).getTime();
        maximum = IDB.DatetimeUtils.makeJavaScriptDate(maximum).getTime();
    }

    if(isNaN(value)) {
        return null;
    }

    closest = stepSize * Math.round(value/stepSize);

    //
    // This is done to deal with numbers in exponential format. Not really necessary
    // for our purposes, but I wanted to be faithful to the Flex algorithm.
    //
    parts = (String(1 + stepSize)).split(".");
    if(parts.length === 2) {
       scale = Math.pow(10, parts[1].length);
       closest = Math.round(closest*scale)/scale;
    }

    if(closest > maximum) {
        closest = maximum;
    }
    else if(closest < minimum) {
        closest = minimum;
    }

    return isDate?(new Date(closest)):closest;
};

IDB.Param.applicableDashboardParams = function(params, dashboardParams) {
    var i, ii, j, jj, dashboardParam, param;
    if(!params || !dashboardParams) {
        return false;
    }

    for(i=0, ii=params.length; i < ii; i++) {
        param = params[i];
        for(j=0, jj=dashboardParams.length; j < jj; j++) {
            dashboardParam = dashboardParams[j];
            if(param.paramName === dashboardParam.paramName &&
                   param.dataType === dashboardParam.dataType) {
                return true;
            }
        }
    }

    return false;
};

IDB.Param.setParameterValuesFromFilters = function(params, filters) {
    var applied = false, i, ii, possibleValue;

    if(filters === null || filters.length === 0) {
        return true;
    }
    else if(params === null || params.length === 0) {
        return false;
    }

    for(i=0, ii = params.length; i < ii; i++) {
        var param = params[i]; 
        var filterMatch = filters.filter(function(filter) { 
                                     return filter.axisName == param.displayName;});
        if(filterMatch.length == 0) {
            continue;
        }
        possibleValue = IDB.StringUtils.rtrim(filterMatch[0].value);
        if(IDB.Param.maybeValidValue(param, possibleValue)){
            param.value = param.multiVal?[possibleValue]:possibleValue;
            if(IDB.Param.isParamCascadeTarget(param)) {
                param.maybeValue = param.value;
            }
            param.programmaticChange = true; 
        }
        else {
            continue;
        }
        applied = true;
    }

     return applied;
};

//
// We only attempt to convert Strings to other types. Values can only be strings or numbers.
//
IDB.Param.canConvertItemToType = function(dataType, value) {
   if(IDB.isNil(value)) {
      return true;
   }

   switch(dataType) {
       case "Number":
           return IDB.NumberUtils.validate(value);
       case "String": 
           return true;
       case "Datetime":
           return IDB.DatetimeUtils.validate(value);
   }
};

IDB.Param.canConvertToType = function(dataType, value) {
    value = value instanceof Array?value:[value];
    return value.every(function(item) {return IDB.Param.canConvertItemToType(dataType, item);});
};

IDB.Param.maybeValidValue = function(param, possibleValue) {
    if(IDB.Param.isValidValue(param, possibleValue)) {
       return true;
    }
    
    if(param.controlType == "choice" && IDB.Param.isParamCascadeTarget(param)) {
               return true;
    }
    else {
        return false;
    }
};

IDB.Param.isValidValue = function(param, possibleValue) {
    var minimum, maximum, distance, choices, matches, values;

    if(param.required && (IDB.isNil(possibleValue) || $.trim(possibleValue.toString()).length === 0)) {
        return false;
    }

    if(!param.multiVal && possibleValue instanceof Array) {
        return false;
    }

    if(!IDB.Param.canConvertToType(param.dataType, possibleValue)) {
        return false;
    }

    if(!param.required && (possibleValue !== 0 && !possibleValue)) {
        return true;
    }
    
    switch(param.controlType) {
        case "slider":
        case "spinner":
        case "slideSpin":
            minimum = param.min;
            maximum = param.max;

            distance = IDB.Param.calculateDistance(param.dataType, possibleValue, minimum);
            if(distance < 0) {
                return false;
            }
   
            distance = IDB.Param.calculateDistance(param.dataType, possibleValue, maximum);
            if(distance > 0) {
                return false;
            }

            return true;
        case "choice":
            if(param.editable) {
                return true;
            }

            choices = param.choices || [];
               
            values = (possibleValue instanceof Array)?possibleValue:[possibleValue];

            return values.every(function(value) {
                      return choices.some(function(choice) {return IDB.Param.valuesAreEqual(param.dataType, 
                                                        choice[0], value);})});

        default:
            return !param.maxLength || (String(possibleValue).length <= param.maxLength);
    }
};

IDB.Param.SHORT_DATA_TYPE_MAP = {String:'s', Datetime:'d', Number:'n'};

IDB.Param.getShortDataType = function(dataType) {
    return IDB.Param.SHORT_DATA_TYPE_MAP[dataType];
};

IDB.Param.valueIsMissing = function(value) {
    if(value === undefined) {
        return true;
    }
    var values;
    if(value instanceof Array) {
        values = value;
    }
    else {
        values = [value];
    }

    for(var i=0; i < values.length; i++) {
        var v = values[i];
        var missing = (v === undefined || (v = $.trim(v), (v === "" || v === 'SELECT_PROMPT')));
        if(!missing) {
            return false;
        }
     }
     return true;    
};

IDB.Param.allRefreshOnCommit = function(params) {
    return params.every(function(param) {return param.refreshOnCommit;});
};

IDB.Param.hasHijriDateParameter = function(params) {
    return params.some(function(param) {return param.hasHijriDateFormat;});
};

IDB.Param.normalizeValue = function(dataType, value, retainZeroMillis) {
    if(value === undefined) {
        return value;
    }
    var values;
    if(value instanceof Array) {
        values = value;
    }
    else {
        values = [value];
    }

    for(var i=0; i < values.length; i++) {
        var v = values[i];
        if(typeof(v) == 'string' && v !== "" && dataType == "Datetime") {
            var dt = IDB.DatetimeUtils.parseDatetime(v);
            if(!dt.hours) {
                dt.hours = 0;
                dt.minutes = 0;
                dt.seconds = 0;
            }
            if(!retainZeroMillis && parseInt(dt.milliseconds) === 0) {
                dt.milliseconds = undefined;
            }
            values[i] = IDB.DatetimeUtils.formatDatetime(dt); 
        }
     }

     if(value instanceof Array) {
        return values;
     }
     else {
        return value;
     }
};

IDB.Param.getValueSourceIndicatorForSource = function(source)
{
    var valueSourceIndicator = IDB.Param.BOTH_SOURCE_PARAMS;
    var overrideOnDrilldown = IDB.currentDashboard.data.overrideOnDrilldown;
    if (overrideOnDrilldown !== null)
    {
        valueSourceIndicator = overrideOnDrilldown;
    }
    if (source === "dashboard")
    {
        var dashValueSourceIndicator = valueSourceIndicator.substring(1);
        dashValueSourceIndicator = dashValueSourceIndicator.replace(IDB.Param.DASHBOARD_PARAMS, IDB.Param.TARGET_PARAMS);
        dashValueSourceIndicator = IDB.Param.DASHBOARD_PARAMS + dashValueSourceIndicator;

        return dashValueSourceIndicator;
    }
    else
    {
        return valueSourceIndicator;
    }
};

IDB.Param.getAdditionalRequestParameters = function(drilldownFilters,
                                                    dashValueSourceIndicator,
                                                    dashboardParams,
                                                    drilldownParams
                                                   )
{
    var requestParameters = "";

    dashValueSourceIndicator = IDB.StringUtils.trimToNull(dashValueSourceIndicator);

    if (dashValueSourceIndicator !== null)
    {
        requestParameters += "dvs=" + dashValueSourceIndicator;
    }

    var prefix = IDB.Param.SOURCE_FILTERS.toLowerCase();

    for(var j=0; drilldownFilters !== null && j<drilldownFilters.length; j++)
    {
        if (requestParameters.length > 0) requestParameters += "&";
        var filter = drilldownFilters[j];
        requestParameters += new IDB.Filter(filter.axisName, filter.value, filter.formattedValue).toValueRequestParameters(prefix, j);
    }

    if (drilldownParams !== null)
    {
        var chartDrilldownParams = drilldownParams.chartDrilldownParams;
        var dashDrilldownParams = drilldownParams.dashboardDrilldownParams;
        var param;

        prefix = IDB.Param.SOURCE_PARAMS.toLowerCase();

        for(j = 0; chartDrilldownParams !== null && j < chartDrilldownParams.length; j++)
        { 
            param = chartDrilldownParams[j];
            if (!IDB.Param.valueIsNothingForType(param.value, param.dataType))
            {
                if (requestParameters.length > 0) requestParameters += "&";
                requestParameters += IDB.Param.toValueRequestParameters(param, prefix, j, true);
            }
        }

        prefix = IDB.Param.SOURCE_DASHBOARD_PARAMS.toLowerCase();

        for(j = 0; dashDrilldownParams !== null && j < dashDrilldownParams.length; j++)
        { 
            param = dashDrilldownParams[j];
            if (!IDB.Param.valueIsNothingForType(param.value, param.dataType))
            {					
                if (requestParameters.length > 0) requestParameters += "&";
                requestParameters += IDB.Param.toValueRequestParameters(param, prefix, j, true);
            }
        }
    }

    prefix = IDB.Param.DASHBOARD_PARAMS.toLowerCase();

    for(j = 0; dashboardParams !== null && j < dashboardParams.length; j++)
    {
        param = dashboardParams[j];
        if (!IDB.Param.valueIsNothingForType(param.value, param.dataType))
        {					
            if (requestParameters.length > 0) requestParameters += "&";
            requestParameters += IDB.Param.toValueRequestParameters(param, prefix, j, true);
        }
    }

    return requestParameters;
};

IDB.Param.valueIsNothingForType = function(value, dataType)
{
    if (value === null)
    {
        return true;
    }

    if (dataType !== "String")
    {
        return IDB.Param.valueIsNothing(value);
    }

    if (typeof value === "string" || value instanceof IDB.DataValue)
    {
        return false;
    }
    if (Array.isArray(value))
    {
        return value.length === 0;
    }

    return true;
};

IDB.Param.valueIsNothing = function(value)
{
    if (value === null)
    {
        return true;
    }

    if (value instanceof IDB.DataValue)
    {
        return value._isNothing;
    }

    if (typeof value === "string")
    {
        return IDB.StringUtils.trimToNull(value) === null;
    }

    if (Array.isArray(value))
    {
        for (var v in value)
        {
            v = IDB.StringUtils.trimToNull(v);
            if (v !== null)
            {
                return false;
            }
        }
        return true;
    }

    return true;
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ParamInput


IDB.ParamInput = function($parent, data, callbacks) {
    var me = this, offsetParent, maxWidth, maxHeight, width, height;
    me.$parent = $parent;
    me.callbacks = callbacks || {};
    me.data = data;
    me.params = [];
    me.refreshCallback = callbacks.refreshCallback;
    me.direction = me.data.direction || IDB.PositionUtils.Direction.VERTICAL;
    me.$paramInputTable = $("<table class='param-items' data-touchpunch='false' border='0' cellpadding='0' cellspacing = '17'/>");
    me.$paramInputTable.css({"margin-left":"auto", "margin-right":"auto", "text-align":"right"});
    me.$div = $("<div class='param-input'>").appendTo($parent);
    offsetParent = me.$div.offsetParent();
    maxWidth = offsetParent.width();
    maxHeight = offsetParent.height();
    if(me.direction == IDB.PositionUtils.Direction.HORIZONTAL) {
        me.$paramInputTable.css({"max-width":maxWidth + "px"});
    }
    me.$div.append(me.$paramInputTable);
    me.createParameters(); 
    width = me.$paramInputTable.width();
    height = me.$paramInputTable.height();
    if(width) {
        if(me.direction == IDB.PositionUtils.Direction.VERTICAL &&  height > maxHeight) {
            width += 21; 
        }
        me.$div.width(width); 
    }
};

IDB.ParamInput.prototype.getNumCols = function() {
    var me = this, numCols = me.data.numCols, direction;
    if(!numCols){
        direction = me.direction;
        if(direction == IDB.PositionUtils.Direction.HORIZONTAL) {
           numCols = me.data.params.length;
        }
        else {
            numCols = 1;
        }
    }
    return numCols;
};

IDB.ParamInput.prototype.createParameters = function() {
    var me = this, paramList = me.data.params, numCols, i, paramData, $currRow, param, 
              hasCascadeTargets, maxName = 0;
    paramList.sort(function(a,b) {return a.paramSeqNum-b.paramSeqNum;});

    numCols = me.getNumCols();

    for(i = 0; i < paramList.length; i++) {
        paramData = paramList[i];
        if(i % numCols === 0) {
           $currRow = $("<tr class='param-row'/>").appendTo(me.$paramInputTable);
        }
        param = new IDB.Param($currRow, paramData, me.params);
        me.params.push(param);
        if(param.displayName.length > maxName) {
            maxName = param.displayName.length;
        }
        if(param.isCascadeTarget()) {
            hasCascadeTargets = true;
        }
        param.maybeAddCascadeSourceListeners(me.params);
        if(param.refreshOnCommit) {
            param.addChangeListener(me.refreshCallback);
        }
    }

    if(numCols === 1  && hasCascadeTargets && (me.direction === IDB.PositionUtils.Direction.VERTICAL)) {
        me.$paramInputTable.width(Math.min(100, maxName*6) + 17*3 + 200);   
    }
};


IDB.ParamInput.prototype.validate = function(validationCallback) {
    var valid = true;
    this.params.forEach(function(element, index, array) {
        if(!element.validate()) {
            valid = false;
        }
       });

    if(validationCallback !== null) {
        validationCallback(!valid?[IDB.format("param.html5.invalid_param_values")]:[]);
    }
};

IDB.ParamInput.prototype.updateParameterValues = function() {
     var me = this, paramList = me.data.params, params = me.params;
     params.forEach(function(element, index, array) {paramList[index].value = element.value;}, me);
};

IDB.ParamInput.prototype.updateControlValues = function() {
     var me = this, paramList = me.data.params, params = me.params;
     paramList.forEach(function(element, index) {params[index].updateControlValue(element);}, me);
};

IDB.ParamInput.prototype.setStyles = function(styles) {
    var me = this, tableWidth;
    // 
    // We want to retain the size of the paramInputTable so overflow can be handled with scrolling.
    //
    if(me.direction == IDB.PositionUtils.Direction.HORIZONTAL) {
        tableWidth = me.$paramInputTable.width();
    }
    me.$div.css(styles);

    if(me.direction == IDB.PositionUtils.Direction.HORIZONTAL) {
        me.$paramInputTable.width(tableWidth);
    }
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ParamInputPanel

IDB.ParamInputPanel = function($parent, data, callbacks) {
    var me = this, message, label, $div;
    me.$parent = $parent;
    me.data = data;
    me.preventRefreshOnCommit = !!data.preventRefreshOnCommit;
    me.showHijriFormatWarning = IDB.Param.hasHijriDateParameter(data.params)
                                 && !IDB.MM.isSuppressed("suppressHijriFormatWarning_" + IDB.userdata.userId);
    me.callbacks = callbacks;
    me.styles = data.styles || {};
    me.$div = $("<div class='param-input-panel' style='margin:0 auto;text-align:center'>").appendTo(me.$parent);


    message = data.paramMessage;
    if(message) {
        $div = this.$div;
        me.$messageDiv = $("<div class='param-input-message'/>").appendTo($div);
        me.$messageDiv.text(message);
        me.$messageDiv.css({"width":"100%", "text-align":"center", "font-weight":"bold", "padding-bottom":"5px"});
        me.$messageDiv.css(this.styles);
    }

    me.direction = data.direction || IDB.PositionUtils.Direction.VERTICAL;
    me.paramInput = new IDB.ParamInput(me.$div, data, 
                              !me.preventRefreshOnCommit?{refreshCallback:IDB.listener(me, "applyClicked")}:{});
    me.paramInput.setStyles($.extend({"margin-left":"auto", "margin-right":"auto"}, me.styles));

    if(me.preventRefreshOnCommit || !IDB.Param.allRefreshOnCommit(data.params)) {
        label = data.applyLabel || IDB.format("param.html5.update");
        this.$applyButton = $("<button type='button' data-inline='true'>" + label + "</button>").appendTo(me.$div);
        this.$applyButton.on("click", IDB.listener(me, "applyClicked")); 
        me.$applyButton.css(this.styles);
    }

    if(me.data.jqmStyling) {
        me.$div.trigger("create");
    }
    if(me.direction === IDB.PositionUtils.Direction.HORIZONTAL) {
        me.paramInput.setStyles({"margin-right":"10px", "display":"inline-block", "overflow-y":"hidden", "overflow-x":"auto"});
        if(me.$applyButton) {
            me.$applyButton.css({"display":"inline-block"}); 
        }
    }
    else {
        me.paramInput.setStyles({"margin-bottom":"5px", "overflow-x":"hidden", "overflow-y":"auto"});
    }

    me.$div.css({"position":"absolute", "padding-top":"10px", "padding-bottom":"10px"});
};

IDB.ParamInputPanel.prototype.applyClicked = function() {
    this.paramInput.validate(IDB.listener(this, "validationCallback"));
};


IDB.ParamInputPanel.prototype.validationCallback = function(validationMessages) {
    var me = this;
    if(validationMessages && validationMessages.length > 0) {
        IDB.MessagePopup.showMessage(me.$div, validationMessages[0]);
    }
    else {
       if(me.showHijriFormatWarning) {
         IDB.MM.maybeShowSuppressible("suppressHijriFormatWarning_" + IDB.userdata.userId, 
           IDB.format("param.html5.hijri_date_format_title"), 
           IDB.format("param.html5.hijri_date_format_msg", IDB.DatetimeUtils.CALENDAR), 
           function(checked) {
             me.showHijriFormatWarning = !checked;
             me.finishParamValueUpdates();
           });
       }
       else {
           me.finishParamValueUpdates();
       }
    }
};

IDB.ParamInputPanel.prototype.finishParamValueUpdates = function() {
    this.paramInput.updateParameterValues();
    var paramValuesChanged = this.callbacks.paramValuesChanged;
    if(paramValuesChanged) {
       paramValuesChanged();
    }
};
 
IDB.ParamInputPanel.prototype.update = function() {
    this.paramInput.updateControlValues();
};

IDB.ParamInputPanel.prototype.sizeAndPosition = function(relativeTo) {
    var me = this, w, h, maxHeight, maxWidth; 
    w = relativeTo.width();
    h = relativeTo.height() - 20;
    me.$div.css({"max-width":w, "max-height":h});
    
    if(me.direction === IDB.PositionUtils.Direction.HORIZONTAL) {
        if(me.$applyButton) {
            me.$applyButton.css("position", "static");
        }
    }
   if(h) {
       maxHeight = h;
       if(me.direction === IDB.PositionUtils.Direction.VERTICAL) {
           if(me.$applyButton) {
               maxHeight -= me.$applyButton.outerHeight(true);
           }
           if(me.$messageDiv) {
               maxHeight -= me.$messageDiv.outerHeight(true);
           }
       }
       me.paramInput.setStyles({"max-height":maxHeight});
   }

   if(w && (me.direction === IDB.PositionUtils.Direction.HORIZONTAL)) {
           if(me.$applyButton) {
               maxWidth = w - me.$applyButton.outerWidth(true) - 30;
           }
           me.paramInput.setStyles({"max-width":maxWidth});
   }

    me.$div.position({
          my:"center",
          at:"center",
          of:relativeTo});

    if(me.direction === IDB.PositionUtils.Direction.HORIZONTAL) {
        if(me.$applyButton) {
            me.$applyButton.css("position", "absolute");
            IDB.PositionUtils.center(me.$applyButton, false, true);
        }
    }
};

IDB.ParamInputPanel.prototype.resize = function() {
    this.sizeAndPosition(this.$parent);
};

IDB.ParamInputPanel.prototype.remove = function() {
    this.$div.remove();
    this.$div = null;
};

IDB.ParamInputPanel.prototype.getDimensions = function() {
    return {height:this.$div.outerHeight(), width:this.$div.outerWidth()};
};
/*global IDB, jQuery */

(function($, IDB) {
    $.widget("idb.paramLegend", $.idb.floatingPanel, {
        options:{
           name:"paramLegend",
           legendTitle:"Parameters",
           bgColor:IDB.rgb(0xFFFFFF),
           paramList:[]
        },
        _create:function() {
             var me = this, $e = me.element;
             me._super(); // call _create() in the floatingPanel widget.

             // note: see the idb-param-legend-* classes in idbhtml.css. The styles they apply to the child elements are needed for
             // the ParamLegend to function correctly.
             $e.addClass("idb-param-legend");
             me.$_titlebar = $("<div class=\"idb-param-legend-titlebar idb-header\">").appendTo($e);
             
             me.$_content = $("<div class=\"idb-param-legend-content\">").appendTo($e)
                   .on("click", IDB.listener(me,  "_toggleOpaque"));
             me._buildHeader();
             me._layout();
             me.adjustLocation();
        },
        _setOption:function(key, value) {
            var me = this, options = me.options;
            options[key] = value;
            if(key === "legendTitle") {
                me._setTitle(value);
                return;
            }
            me._super(key, value);
        },
        _setTitle:function(title) {
            var me = this, $titletext = me.$_titletext;
            if(!$titletext) {
               return;
            }
            $titletext.html(title);
        },
//        destroy: function() {
//
//        },

        // private functions
        _buildHeader:function() {
            var me =this, o = me.options, paramList = o.paramList, $header = me.$_titlebar;
            me.$_titletext = $('<div class="idb-param-legend-titletext">').appendTo($header);
            // See http://phrogz.net/CSS/vertical-align/ (Method 2) for how the text gets vertically aligned in the titlebar.
            me._setTitle(o.legendTitle);
        },
        _layout:function() {
            var me = this, $e = me.element, $c = me.$_content, o = me.options, 
               legendItems = (o.paramList || []), lw = 0, cw = 50, lineH = 24, // lineH must agree with idbhtml.css. 
              $t, $tr, $td, $titlebar = me.$_titlebar, $titletext = me.$_titletext, $bg = me.$_bg, legendItem,
              hpad = 13, needScroll, titletextWidth;
            $c.empty();
            $t = $('<table class="idb-param-legend-table">').appendTo($c);

           for(var j=0, jj=legendItems.length; j<jj; j++) {
              legendItem = legendItems[j];
              $tr = $("<tr>").appendTo($t);
              $td = $("<td>").appendTo($tr).text(legendItem.displayName).attr("title", legendItem.displayName);
              lw = Math.max(lw, $td.width());

              $td.addClass('idb-param-legend-label');
              var value = legendItem.formattedValue;
              $td = $("<td>").appendTo($tr).text(value).css({"white-space":"nowrap"}).attr("title", value);
              $td.addClass('idb-param-legend-value');
              cw = Math.max(cw, $tr.width());
           }
          needScroll = legendItems.length > 10;
          if(needScroll) {
             $c.addClass("idb-scroll-y");
             hpad += 12;
          }
          else {
            $c.removeClass("idb-scroll-y");
          }

          // Removing any previously set explict width will cause
          // the value returned by $titletext.width() to be the value
          // based on the actual text width.
          $titletext.css({"width":"auto"});
          titletextWidth = $titletext.width();

//          IDB.log("Header width: " + titletextWidth);

          var headerHeight = 22, /* (20 h) + (2 padding) */
              contentHeight = (Math.min(10, legendItems.length)*(lineH)) + 10, /* padding top and bottom = 10 */
              W = cw+hpad, H = contentHeight + headerHeight; // (5+5+20)
//          IDB.log("cw=" + cw + ", hpad=" + hpad + ", W=" + W + ", H=" + H);

             W = Math.min(250, Math.max(W, titletextWidth+44));
          if(titletextWidth > (W-44)) {
             $titletext.width(W-44);
          }

          $e.width(W).height(H);
          $c.outerHeight(contentHeight).outerWidth(W);
          $titlebar.outerWidth(W);
          $bg.outerWidth(W).outerHeight(H);
          var contentWidth = me.$_content.width();
          if(needScroll) {
              contentWidth -= 12;
          }
    
          //
          // If the legend is of maximum size, adjust.
          // 
          if(W == 250) {
              var lblPct = Math.max(0.2, lw/cw); 
              var labelWidth = contentWidth*(lblPct)-5; 
              var valueWidth = contentWidth*(1-lblPct)-5; 
              $e.find('.idb-param-legend-label').css({maxWidth:labelWidth});
              $e.find('.idb-param-legend-value').css({maxWidth:valueWidth});
          }
        },
        refresh:function() {
            this._layout();
            this.adjustLocation();
        }
    });
})(jQuery, IDB);

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ReportParameterInputPanel

IDB.ReportParameterInputPanel = function($parent, data, callbacks) {
    var me = this, $div;
    me.$parent = $parent;
    me.data = data;
    me.callbacks = callbacks || {};
    $div = $("<div>").appendTo(me.$parent);
    me.$div = $div;
    $div.css({"height":"100%", "width":"100%"}); 
 
    me.paramInputPanel = new IDB.ParamInputPanel($div, $.extend({numCols:3, preventRefreshOnCommit:true}, data), callbacks);
    $(window).resize(IDB.listener(me, "position"));
};

IDB.ReportParameterInputPanel.prototype.position = function() {
    this.paramInputPanel.sizeAndPosition(this.$div);
};

IDB.ReportParameterInputPanel.prototype.applyStyles = function() {
    this.$div.find(".hasDatepicker").parent().css({display:"inline-flex"});
};

IDB.ReportParameterInputPanel.prototype.update = function() {
    this.paramInputPanel.update();
};
///////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.SelectionMap

IDB.SelectionMap = function() {
    this.pairs = [];
    this.equalFunction = IDB.StringUtils.areEqualWithConversion;
};

IDB.SelectionMap.prototype.get = function(selection, defaultChoices) {
    var me = this, pairs = this.pairs;
    for(var i=0; i < pairs.length; i++) {
        if(me.selectionsAreEqual(pairs[i].key, selection)) {
            return pairs[i].value;
        }
    }
    return defaultChoices;
};

IDB.SelectionMap.prototype.put = function(selection, choices) {
    var me = this, pairs = this.pairs, 
        pair = {key:selection, value:choices};

    var idx = -1;
    for(var i=0; i < pairs.length; i++) {
        if(me.selectionsAreEqual(pairs[i].key, selection)) {
            idx = i;
        }
    }
    if(idx != -1) {
        pairs[idx] = pair;
    }
    else {
        idx = pairs.length;
        pairs.push(pair);
    }
    return idx;
};

IDB.SelectionMap.prototype.selectionsAreEqual = function(s1, s2) {
   var me = this;
   var k1 = Object.keys(s1).sort();
   var k2 = Object.keys(s2).sort();
   if(k1.toString() != k2.toString()) {
       return false;
   }

   return k1.every(function(k) {
                      return me.valuesAreEqual(s1[k],s2[k]);
                   }); 
};

IDB.SelectionMap.prototype.valuesAreEqual = function(v1, v2) {
    if(Array.isArray(v1) && Array.isArray(v2)) {
        return IDB.ArrayUtils.equalAsSets(v1,v2,this.equalFunction);
    }
    else {
        return this.equalFunction(v1,v2);
    }
};



////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ArrayUtils
/*global IDB */
IDB.ArrayUtils = {};

IDB.ArrayUtils.diff = function(arr1, arr2, equalFunction) {
    return $(arr1).filter(function(i1,e1) {
                                  return $(arr2).filter(function(i2,e2) {
                                                if(equalFunction != null) {
                                                    return equalFunction(e1,e2);
                                                }
                                                else {
                                                    return e1 == e2;
                                                } 
                                              }).length == 0;
                          }).get();
};

IDB.ArrayUtils.equalAsSets = function(arr1, arr2, equalFunction) {
    if(arr1 == null) {
        return arr2 == null;
    }

    if(arr2 == null) {
        return false;
    }

    if(IDB.ArrayUtils.diff(arr1,arr2, equalFunction).length > 0) {
        return false;
    }

    return IDB.ArrayUtils.diff(arr2, arr1, equalFunction).length == 0;
};

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ChartUtils
/*global IDB, $ */
IDB.ChartUtils = {};

/**
 * Expands macros that are embedded in the str argument.
 */
IDB.ChartUtils.expandMacros = function(str, escapeType, username, contentBaseUrl, chart, dp) {

    if(!str) return str;

    var chartTitle, axes = null, xAxisName, xAxisValue, dataRow;

    chart = chart || null;
    dp = dp || null;
    
    if(username) {
        str = str.replace(/\$\{\s*user\s*}/ig, IDB.ChartUtils.maybeEscape(username, escapeType));
    }

    if(chart) {
        axes = chart.dataSet.axisList;
        chartTitle = chart.chartTitle?chart.chartTitle:"";
        str = str.replace(/\$\{\s*chartid\s*\}/ig, chart.chartId);
        str = str.replace(/\$\{\s*charttitle\s*\}/ig, IDB.ChartUtils.maybeEscape(chartTitle, escapeType));
        str = str.replace(/\$\{\s*uecharttitle\s*\}/ig, chartTitle);
        str = str.replace(/\$\{\s*categoryid\s*\}/ig, chart.categoryId);
    }

    if(dp && axes) {
        xAxisName = dp.xAxis.axisName;
        xAxisValue = dp.xValue.stringValue;
        dataRow = dp.dataRow || null;
        var dataRows = chart.dataSet.getDataRows();
        if(dataRows) {
            str =  IDB.ChartUtils.expandRepeatMacros(str, escapeType, dataRows, chart, dataRow, axes);
         }
        str = str.replace(/\$\{\s*xlabel\s*\}/ig, IDB.ChartUtils.maybeEscape(xAxisName, escapeType));
        str = IDB.ChartUtils.expandValueMacros(str, escapeType, xAxisValue, dataRow, chart, axes);
    }

    if(escapeType === 'html') {
        str = IDB.ChartUtils.expandContentMacros(str, contentBaseUrl);
    }

    if(escapeType === 'url') {
        str = IDB.ChartUtils.resolveContentUrl(str, contentBaseUrl);
    }

    str = str.replace(/\$\{\s*dollar\s*\}/ig, IDB.ChartUtils.maybeEscape("$", escapeType));
    return str;
};

IDB.ChartUtils.expandValueMacros = function(str, escapeType, xAxisValue, dataRow, chart, axes) {
    var axis, pat, re, dv, stringValue, repl;

        repl = IDB.ChartUtils.maybeEscape(xAxisValue, escapeType);
        str = str.replace("${xvalue}", repl); 
        str = str.replace(/\$\{\s*xvalue\s*\}/ig, repl);
        for(var j=0; dataRow !== null && j<axes.length; j++) {
            axis = axes[j];
            dv = dataRow[j];
            if(!dv) continue; // shouldn't happen
            if(!axis) continue; // shouldn't happen
            stringValue = dv.stringValue;
            if(escapeType != 'url') {
                stringValue =  dv.formattedValue;//DataValueFormatter.formatDataValue(dv, axis, chart);
            }
            pat = "\\$\\{\\s*value\\s*:\\s*" + IDB.StringUtils.escapeRegEx(axis.axisName) + "\\s*\\}";
            re = new RegExp(pat, "g"); // case-SENSITIVE
            repl = IDB.ChartUtils.maybeEscape(stringValue, escapeType);
            str = str.replace("${value:" + axis.axisName + "}", repl);
            str = str.replace(re, repl);

            // Expand the uevalue for this axis.
            pat = "\\$\\{\\s*uevalue\\s*:\\s*" + IDB.StringUtils.escapeRegEx(axis.axisName) + "\\s*\\}";
            re = new RegExp(pat, "g"); // case-SENSITIVE
            str = str.replace("${uevalue:" + axis.axisName + "}", stringValue);
            str = str.replace(re, stringValue);
        }
        return str;
};

IDB.ChartUtils.repeatRegExp = /\[\[(\[(.*)\])?(.*?)\]\]/g;

IDB.ChartUtils.expandRepeatMacros = function(str, escapeType, dataRows, chart, dpDataRow, axes) {
    if(!dataRows || dataRows.length === 0) {
        return str;
    }
    while(true) {
        var results = IDB.ChartUtils.repeatRegExp.exec(str);
        if(!results) {
            break;
        } 
        var match = results[0];
	var directives = IDB.StringUtils.pairsStringToMap(results[2], "=", ",");
        var toExpand = results[3];
        var expanded = IDB.ChartUtils.expandRepeatMacro(toExpand, escapeType, directives, dataRows, chart, dpDataRow, axes);
	var idx = results.index;
	str = str.substring(0,idx) + expanded + str.substring(idx+match.length);
    }
		
    return str;			
};

IDB.ChartUtils.expandRepeatMacro = function(str, escapeType, directives, dataRows, chart, dpDataRow, axes) {
    if(!dataRows || dataRows.length === 0) {
        return str;
    }
    var result = "";
    var delim = directives.delim || '|';
    var ndp = (directives.ndp === "true");
    if(directives.inc) {
        ndp = (directives.inc === "others");
    }
    var maxValues = directives.max || -1;
    var valueCount = 0;
    for (var i = 0; i < dataRows.length; i++) {
        var row = dataRows[i];
        if(ndp && dpDataRow == row) {
            continue;
        }
        if(maxValues !== -1 && maxValues <= valueCount) {
            break;
        }
    	if(valueCount > 0) {
            result += delim;
        }
    	valueCount++;
   	result += IDB.ChartUtils.expandValueMacros(str, escapeType, row[0].stringValue, row, chart, axes);
    }
    		
    return result;
}; 





IDB.ChartUtils.maybeEscape = function(str, escapeType) {
    if(!str) return str;
    if(escapeType === 'html') {
        return IDB.StringUtils.escapeHtml(str);
    }
    else if(escapeType === 'url') {
        return encodeURIComponent(str);
    }
    else if(escapeType) {
        throw new Error("ChartUtils.maybeEscape(): Invalid escapeType: '" + escapeType + "'.");
    }
    return str;
};

IDB.ChartUtils.expandContentMacros = function(str, contentBaseUrl) {
    if(!str) return str;
    var ueBaseUrl = contentBaseUrl, 
        baseUrl = IDB.ChartUtils.maybeEscape(ueBaseUrl, "html"),
        pat = "\\$\\{\\s*content\\s*\\}",
        re = new RegExp(pat, "ig");

    str = str.replace(re, baseUrl);

    pat = "\\$\\{\\s*uecontent\\s*\\}";
    re = new RegExp(pat, "ig");
    str = str.replace(re, ueBaseUrl);

    return str;
};

IDB.ChartUtils.resolveContentUrl = function(url, contentBaseUrl) {
    if(!url || !contentBaseUrl) return url;
    var str = url.replace(/^\s*content:\s*\/?/i, contentBaseUrl + "/");
    if(str.indexOf("/") !== 0 && (str.indexOf("://") == -1 || str.indexOf("://") > 8)) {
        str = "http://" + str;
    }
    return str;
};


IDB.ChartUtils.downloadFile = function(url, request) {
    
    var $iframe = $("#idb-download");
    if($iframe.length === 0) {
        $iframe = $("<iframe name='idb-download' id='idb-download'>").appendTo($('body')).hide();
    }
    var $downloadForm = $("<form method='post' target='idb-download'><input type='hidden' name='json'>")
                              .appendTo($('body')).hide();
    $downloadForm.attr({"action":url});
    var $input = $downloadForm.find('input');
    $input.val(JSON.stringify(request));
    $downloadForm.submit().remove();
};


IDB.ColorSet = function(startIndex) {
    if(!startIndex) {startIndex = 0;}
    this._length = IDB.ColorSet.BASIC_COLORS.length;
    this._minusLength = 0 - this._length;
    this._startIndex = startIndex;
    this._cursor = startIndex;
    if(!IDB.ColorSet._cache) {
        IDB.ColorSet._cache = {};
    }
};

IDB.ColorSet.prototype.setStartIndex = function(index) {
    if(index > 0) {
        this._startIndex = index % this._length;
    }
    else if (index < 0) {
        this._startIndex = this._length + (index % this._length);
    }
    else {
       this. _startIndex = 0;
    }
};

IDB.ColorSet.prototype.getColor = function(index) {

    // move to the indicated position to the right or left of _startIndex
    index += this._startIndex;

    // wrap around, if we've gone past the end or before the beginning of the array.
    if(index > this._length) {
        index %= this._length;
    }
    else if(index < 0) {
        index = this._length + (index % this._length);
    }
    var color = IDB.ColorSet.BASIC_COLORS[index];
    // check cache
    var rgb = IDB.ColorSet._cache[color];
    if(!(rgb)) {
        rgb = IDB.rgbx(color);
        IDB.ColorSet._cache[color] = rgb; 
    }

    return rgb;
};

IDB.ColorSet.prototype.getNextColor = function() {
    return this.getColor(this._cursor++);
};

IDB.ColorSet.BASIC_COLORS = [
    //new 22 w/buffer color in position index (0)
    0x008CBD,0x7e9ca8,0xe79f23,0x525762,
    0x017e89,0xd9532f,0x203566,0x77863b,
    0x8a3357,0xa9ccc6,0xc77529,0x1c5f60,
    0x957290,0xc6d2d8,0xfbd895,0xbabcc1,
    0xa1cbd0,0xeeb2a2,0xa7afc2,0xc9ceb3,
    0xc398ab,0xd8e7e3,

    //repeat above (minus index (0)) for X-Axis charts because they begin after position 22
    0x7e9ca8,0xe79f23,0x525762,
    0x017e89,0xd9532f,0x203566,0x77863b,
    0x8a3357,0xa9ccc6,0xc77529,0x1c5f60,
    0x957290,0xc6d2d8,0xfbd895,0xbabcc1,
    0xa1cbd0,0xeeb2a2,0xa7afc2,0xc9ceb3,
    0xc398ab,0xd8e7e3,0xe7c1a1,0xa0c0c0,
    0xc7b9c6,

    //category color reuse
    0x33b49e,0xf1cb4e,0xe57a3c,0xe35b4a,
    0xf7af02,0xffe766,0x8bdad9,0x5da8f3,
    0x2890b9,0x17a58a,0x52e8cb,0xa4ce40,
    0x27ae60,0x00bbce,0x04a2aa,0x1069ad,
    0xae1c1e,0xf26222,0xfc8627,0xfcc322,
    0x86d1cb,0x9ec7d1,0xd0daa8,0xb4b744,
    0x42896c,0x87a3c2,0x5da8f3,0x00887d,
    0xc2d965,0xe8880c,0xffdd4d,0xad6e69,
    0xedaea7,0x4239bc,0x7916ad,0xab17ad,
    0xad196e,0x1069ad,0x1147ad,0x0f9dad,
    0x0ead9a,0xaa3caa,0xfc63fc,0xad189f,
    0xad1a5e,0xc6282f,0xff9182,0xe87c5a,
    0x55897d,0xadc4a0,0xf4eeb4,0x32657b,

    //oldpattern continues here
    0xEE2222,0x228822,0x5555BB,0xFF8855,
    0xBB55BB,0x888822,0x558888,0xBB5555,
    0xFF00CC,0x22BBEE,0xFF4444,0x44AA44,
    0x7777DD,0xFFAA77,0xDD77DD,0xAAAA44,
    0x77AAAA,0xDD7777,0xFF22EE,0x44DDFF,
    0xFF0000,0x336699,0x009900,0xFF9900,
    0x990000,//0xffffff,0x000000,0x888888,
    0x707883,0xFFF200,0x007BC4,0xE65714,
    0x369A3D,0xF4E0C8,0xCDE7F6,0x42B4AC,
    0xBF2E7B,0x3D1D0E,0x7D63A9,0xB0001E,
    0x002455,0xD39815,0xA9D1E7,0x38175A,
    0x00727C,0xEC83AD,0x63001E,0x006A35,
    0x010202,0x586695,0xFFBD00,0x8AB28D,
    0xFFD16E,0x87A2B0,0xC8C866,0xB2A9E7,
    0x629C33,0x815E4A,0xC8EB7D,0xC88DAC,
    0x895227,0x1B75C4,0xCCC8A4,0x424252,
    0xcd5c5c,0xe9967a,0xf08080,0xfa8072,
    0xff4500,0xff0000,0xdc143c,0xb22222,
    0x8b0000,0xc71585,0xffc0cb,0xffb6c1,
    0xff69b4,0xff1493,0xdb7093,0xbdb76b,
    0xf0e68c,0xeee8aa,0xfafad2,0xffffe0,
    0xfffacd,0xffff00,0xffd700,0xffefd5,
    0xffe4b5,0xffdab9,0x00ffff,0x00ffff,
    0x7fffd4,0x40e0d0,0x48d1cc,0x00ced1,
    0x5f9ea0,0x708090,0xe0ffff,0xafeeee,
    0xb0e0e6,0xb0c4de,0x4682b4,0xadd8e6,
    0x87ceeb,0x87cefa,0x00bfff,0x6495ed,
    0x4169e1,0x7b68ee,0x1e90ff,0x0000ff,
    0x0000cd,0x00008b,0x000080,0x191970,
    0xffa07a,0xffa500,0xff8c00,0xff7f50,
    0xff6347,0xff4500,0x7fffd4,0x00fa9a,
    0x00ff7f,0x98fb98,0xadff2f,0x7fff00,
    0x7cfc00,0x00ff00,0x90ee90,0x9acd32,
    0x32cd32,0x3cb371,0x8fbc8f,0x228b22,
    0x2e8b57,0x008000,0x6b8e23,0x808000,
    0x556b2f,0x006400,0x66cdaa,0x40e0d0,
    0x20b2aa,0x008b8b,0x008080,0xe6e6fa,
    0xd8bfd8,0xdda0dd,0xee82ee,0xff00ff,
    0xff00ff,0xda70d6,0xba55d3,0x9932cc,
    0x8a2be2,0x9400d3,0x9370db,0x6a5acd,
    0x800080,0x8b008b,0x483d8b,0x4b0082,
    0xf0fff0,0xf5fffa,0xf0ffff,0xf0f8ff,
    0xf8f8ff,0xf5f5f5,0xfff0f5,0xffe4e1,
    0xfaebd7,0xfff5ee,0xfffafa,0xffffff,
    0xf5f5dc,0xfaf0e6,0xfdf5e6,0xfffaf0,
    0xfffff0,0xdcdcdc,0xd3d3d3,0xc0c0c0,
    0xa9a9a9,0x808080,0x696969,0x2f4f4f,
    0x778899,0x708090,0xfff8dc,0xffebcd,
    0xffe4c4,0xffdead,0xf5deb3,0xf4a460,
    0xdaa520,0xb8860b,0xcd853f,0xd2691e,
    0x800000,0x8b4513,0xa52a2a,0xa0522d,
    0x8b0000,0xdeb887,0xd2b48c,0xbc8f8f
    ];
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.DatetimeUtils

IDB.DatetimeUtils = {};

IDB.DatetimeUtils.datetimeFormat = "yyyy-MM-dd HH:mm:ss.SSS";

IDB.DatetimeUtils.dateFormat = "yyyy-MM-dd";

IDB.DatetimeUtils.dateRegExp = /^(\d{4})-(\d{1,2})-(\d{1,2})$/;

IDB.DatetimeUtils.timeRegExp = /^(\d{1,2}):(\d{1,2})(?::(\d{1,2})(?:.(\d{1,3}))?)?$/;

IDB.DatetimeUtils.DAYS_IN_MONTH = [31,28,31,30,31,30,31,31,30,31,30,31];

IDB.DatetimeUtils.MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24;

IDB.DatetimeUtils.MILLISECONDS_PER_MINUTE = 1000 * 60;

IDB.DatetimeUtils.CALENDAR = "ISO 8601"; // Gregorian

IDB.DatetimeUtils.trim = function(s) {
   if(s === null) return '';
   return s.replace(/^\s*/, '').replace(/\s*$/, '');
};


IDB.DatetimeUtils.isLeapYear = function(year) {            
    return ((year % 400 === 0) || ((year % 4 === 0) && (year % 100 !== 0)));
};

IDB.DatetimeUtils.hasTime = function(dt) {
   return dt.hasOwnProperty('hours');
};

IDB.DatetimeUtils.makeDate = function(dt) {
   if(!IDB.DatetimeUtils.hasTime(dt)) {
       return new Date(dt.year, dt.month, dt.date);
   }
   else {
       var seconds = dt.seconds?dt.seconds:0;
       var milliseconds = dt.milliseconds?dt.milliseconds:0;
        
       return new Date(dt.year, dt.month, dt.date, dt.hours, dt.minutes, seconds, milliseconds);
   }
};

IDB.DatetimeUtils.makeDatetime = function(date, showTime) {
   var dt = {};

   dt.year = date.getFullYear();
   dt.month = date.getMonth();
   dt.date = date.getDate();

   if(showTime) {
      dt.hours = date.getHours();
      dt.minutes = date.getMinutes();
      dt.seconds = date.getSeconds();
      dt.milliseconds = date.getMilliseconds();
   }

   return dt;
};

IDB.DatetimeUtils.parseDatetimeBase = function(dtStr)  {
    dtStr = IDB.DatetimeUtils.trim(dtStr);
            
    if(!dtStr) return null;
            
    var err =  new Error("Invalid datetime: " + dtStr + ". Expected datetime format is " + IDB.DatetimeUtils.datetimeFormat + ".");

    var splitStr = dtStr.split(/\s+/);

    var dateResults = splitStr[0].match(IDB.DatetimeUtils.dateRegExp);
            
    //
    // Might never be less than 4 because of capture groups in results.
    //
    if(dateResults === null || dateResults.length < 4) {
        throw err;
    }
                
    var dt = {};

    try {
           
        var year = dateResults[1];

        if(year < 1 || year > 9999) { 
            throw err; 
        }
        else {
            dt.year = year;
        }
           
        var month = dateResults[2]-1;
    
        if(month < 0 || month > 11) {
            throw err;
        }
        else {
            dt.month = month;
        }

        var day = 1;
       
        if(dateResults.length > 2) {
           day = dateResults[3];
              
           var numDays = IDB.DatetimeUtils.DAYS_IN_MONTH[month];
                 
           if (month == 1 && IDB.DatetimeUtils.isLeapYear(year)) { 
               ++numDays; 
           }
                      
           if (day < 1 || day > numDays) { 
               throw err; 
           }
           else {
               dt.date = day;
          
           }
        } 
          var hours = 0;
          var minutes = 0;
          var seconds = 0;
          var milliseconds = 0;
                 
          if(splitStr.length > 1) {

               var timeResults = splitStr[1].match(IDB.DatetimeUtils.timeRegExp);

               if(timeResults === null || timeResults.length < 3) {
                        throw err;
               }
                                   
               hours = timeResults[1];
               
               if(hours < 0 || hours > 23) {
                   throw err;
               }
               else {
                   dt.hours = hours;
               }

               minutes = timeResults[2];
               if(minutes < 0 || minutes > 59) {
                   throw err;
               }
               else {
                   dt.minutes = minutes;
               }
                   
               if(timeResults[3]) {
                     seconds = timeResults[3];
                     
                     if(seconds < 0 || seconds > 59) {
                         throw err;
                     }
                     else {
                       dt.seconds = seconds;
                     }
               }

               if(timeResults[4]) {
                   milliseconds = timeResults[4];
                   
                   if(milliseconds < 0 || milliseconds > 999) {
                       throw err;
                   }
                   else {
                       dt.milliseconds = milliseconds;
                   }
                   
               }

           }
       }
       catch(e) {
           throw err;
       }

       return dt;
   };

//
// Parses a datetime string in the 'yyyy-MM-dd HH:mm:ss.SSS' format. Returns an object with the appropriate 
// components to construct a Date object.
//
//

IDB.DatetimeUtils.parseDatetime = function(dtStr, timezoneOffset)  {
    var dt = IDB.DatetimeUtils.parseDatetimeBase(dtStr);

    if(!isNaN(timezoneOffset)) {
       dt = IDB.DatetimeUtils.adjustForTimezone(dt, timezoneOffset, -1);
    }

    return dt;
};


//
// Returns a string representation of the date in the 'yyyy-MM-dd HH:mm:ss.SSS' format. If the 'hours' 
// property is present, then the string will have the time component.
//
IDB.DatetimeUtils.formatDatetime = function(dt, timezoneOffsetBase) {
    var date = new Date(dt.year, dt.month, dt.date);

    var y = String(date.getFullYear());
    var M = String(date.getMonth()+1);
    var d = String(date.getDate());

    var showTime = IDB.DatetimeUtils.hasTime(dt);

    if(!isNaN(timezoneOffsetBase)) {

        dt = IDB.DatetimeUtils.adjustForTimezone(dt, timezoneOffsetBase); 
    }

    var dateStr = y + "-";

    dateStr += IDB.DatetimeUtils.padLeft(M, "0", 2);

    dateStr += "-";

    dateStr += IDB.DatetimeUtils.padLeft(d, "0", 2);

    var result = dateStr;

    if(showTime) {
        var H = dt.hours;
        var m = dt.minutes;
        var s = dt.seconds;
        var S = dt.milliseconds;

        var timeStr = " ";

        timeStr += IDB.DatetimeUtils.padLeft(String(H), "0", 2);

        timeStr += ":";

        timeStr += IDB.DatetimeUtils.padLeft(String(m), "0", 2);

        if(s !== undefined) {

            timeStr += ":";

            timeStr += IDB.DatetimeUtils.padLeft(String(s), "0", 2);

            if(S !== undefined) {
                timeStr += ".";

                timeStr += IDB.DatetimeUtils.padLeft(String(S), "0", 3);
            }
        }

        result += timeStr;
    } 

    return result;
};


IDB.DatetimeUtils.padLeft = function(str, ch, num) {
   var result;  

   if(num === 0) {
       return "";
   }

   if(str.length >= num) {
       return str.substr(str.length-num);
   }

   else {
      result = "";

      for(var i = 0; i < num-str.length; i++) {
          result += ch;
      }
      result += str;
   }
   return result;
};

IDB.DatetimeUtils.validate = function(value) {
     try {
         IDB.DatetimeUtils.parseDatetime(value);
     }
     catch(e) {
          return false;
     }

     return true;
};

//
// This method takes a date string and normalizes it to the 'yyyy-MM-dd HH:mm:ss.SSS' format.
//
IDB.DatetimeUtils.normalizeDatetimeString = function(dtStr) {
    if(!dtStr) {
       return dtStr;
    }

    var dt = IDB.DatetimeUtils.parseDatetime(dtStr);

    if(!IDB.DatetimeUtils.hasTime(dt)) {
        dt.hours = 0;
        dt.minutes = 0;
        dt.seconds = 0;
        dt.milliseconds = 0;
    }

    if(!dt.hasOwnProperty("milliseconds")) {
        dt.milliseconds = 0;
    }

    return IDB.DatetimeUtils.formatDatetime(dt);
};

IDB.DatetimeUtils.adjustForTimezone = function(dt, timezoneOffsetBase, orientation) {
    var adjustedMillis;

    var showTime = IDB.DatetimeUtils.hasTime(dt);

    var date = IDB.DatetimeUtils.makeDate(dt);

    if(!isNaN(timezoneOffsetBase)) { 

        if(isNaN(orientation)) {
            orientation = 1;
        }

        orientation = (Math.abs(orientation) != 1)?1:orientation; 


        adjustedMillis = date.getTime() + orientation*((date.getTimezoneOffset() - timezoneOffsetBase)*IDB.DatetimeUtils.MILLISECONDS_PER_MINUTE); 
    }
    else {
        adjustedMillis = date.getTime();
    }

    var newDate = new Date(adjustedMillis); 

    return IDB.DatetimeUtils.makeDatetime(newDate, showTime);
};

IDB.DatetimeUtils.parseDate = function(dtStr)  {
    dtStr = IDB.DatetimeUtils.trim(dtStr);
            
    if(!dtStr) return null;
            
    var err =  new Error("Invalid date: " + dtStr + ". Expected date format is " + IDB.DatetimeUtils.dateFormat + ".");
    var dateResults = dtStr.match(IDB.DatetimeUtils.dateRegExp);
            
    //
    // Might never be less than 4 because of capture groups in results.
    //
    if(dateResults === null || dateResults.length < 4) {
        throw err;
    }
                
    var dt = {};

    try {
           
        var year = dateResults[1];

        if(year < 1 || year > 9999) { 
            throw err; 
        }
        else {
            dt.year = year;
        }
           
        var month = dateResults[2]-1;
    
        if(month < 0 || month > 11) {
            throw err;
        }
        else {
            dt.month = month;
        }

        var day = 1;
       
        if(dateResults.length > 2) {
           day = dateResults[3];
              
           var numDays = IDB.DatetimeUtils.DAYS_IN_MONTH[month];
                 
           if (month == 1 && IDB.DatetimeUtils.isLeapYear(year)) { 
               ++numDays; 
           }
                      
           if (day < 1 || day > numDays) { 
               throw err; 
           }
           else {
               dt.date = day;
          
           }
        } 
       }
       catch(e) {
           throw err;
       }

       return dt;
   };

IDB.DatetimeUtils.isValidDate = function(value) {
     try {
         IDB.DatetimeUtils.parseDate(value);
     }
     catch(e) {
          return false;
     }

     return true;
};

IDB.DatetimeUtils.areDatetimeStringsEqual = function(dateStr1, dateStr2) {
    var d1 = IDB.DatetimeUtils.normalizeDatetimeString(dateStr1);
    var d2 = IDB.DatetimeUtils.normalizeDatetimeString(dateStr2);

    return d1 == d2; 
};


IDB.DatetimeUtils.makeJavaScriptDate = function(value) {
    if(value instanceof Date) {
       return value;
    }
    else if(typeof(value) === 'number') {
        return new Date(value);
    }
    else if(value) {
        return IDB.DatetimeUtils.makeDate(IDB.DatetimeUtils.parseDatetime(value.toString()));
    }
    else {
       return null;
    }
};

IDB.DatetimeUtils.dateDifference = function(d1, d2) {
   var date1 = IDB.DatetimeUtils.makeJavaScriptDate(d1);
   var date2 = IDB.DatetimeUtils.makeJavaScriptDate(d2);
   return date1.getTime() - date2.getTime(); 
};
////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//// IDB.HtmlUtils
///*global IDB */
IDB.HtmlUtils = {};

IDB.HtmlUtils.strToHtml = function(str, expandMacros, removeScript) {
    str = str.replace(/\r\n/g, '\n');
    expandMacros = IDB.isUndef(expandMacros) || expandMacros;
    removeScript = IDB.isUndef(removeScript) || removeScript;

    if(expandMacros) {
        str = IDB.ChartUtils.expandMacros(str, "html", IDB.userdata.username, IDB.userdata.baseUrl);
    }

    var $div = $("<div>").html(str);

    if(removeScript) {
        $div.find("script").remove();
    }

    if(IDB.HtmlUtils.isFlashHtml($div)) {
        IDB.HtmlUtils.toStandardHtml($div);
    }

    return $div;
};


IDB.HtmlUtils.toStandardHtml = function($div) {
    //
    // Remove the textformat elements.
    //
    $div.find("textformat").replaceWith(function() {return $(this).contents();});

    //
    // Replace the font elements with spans with styles.
    //
    $div.find("font").each(function() {
       var $this = $(this),
           color = $this.attr("color"),
           fontFamily = $this.attr("face"),
           fontSize = $this.attr("size"), 
           styles = {},
           $underlined, $bold, $italicized, $contents, $span;

       if(color) {
           styles.color = color;
       }

       if(fontFamily) {
           styles.fontFamily = fontFamily;
       }
 
       if(fontSize) {
           styles.fontSize = fontSize + "px";
       }

       //
       // Replace the underlined, bold and italicized tags with styles.
       //
       $underlined = $this.find("u");
       if($underlined.length > 0) {
           styles.textDecoration = "underline";
           $underlined.replaceWith(function() {return $(this).contents();});
       }

       $bold = $this.find("b");
       if($bold.length > 0) {
           styles.fontWeight = "bold";
           $bold.replaceWith(function() {return $(this).contents();});
       }

       $italicized = $this.find("i");
       if($italicized.length > 0) {
           styles.fontStyle = "italic";
           $italicized.replaceWith(function() {return $(this).contents();});
       }
       
       $contents = $this.contents();
       $span = $("<span>").css(styles).addClass("idbFont");
       $this.replaceWith($span);
       $span.append($contents);

       //
       // Since links are explicitly styled, apply the styles directly to links.
       //
       if($contents.is("a")) {
           $contents.css(styles);
       }
     });

    //
    // Replace the 'align' attribute on paragraphs with the text-align style.
    //
    $div.find("p").each(function() {
         var $this = $(this), 
             align = $this.attr("align"); 
         $this.css({"text-align":align, "margin":0}).removeAttr("align");

         //
         // Set the height on empty paragraphs. Flash seems to translate BR tags to P tags with
         // no content.
         //
         if(IDB.HtmlUtils.isFlashBlankLine($this)) {
             var height = $this.find(".idbFont").css("fontSize");
             if(height) {
                 $this.css({height:height});
             }
         }
    });

     //
     // Put list items in a list container (UL). 
     //
     $div.find(":not(li) + li").each(function() {
        var parent = this.parentElement;
        if(parent.nodeName != "OL" && parent.nodeName != "UL") {
            var $this = $(this);
            var $loc = $this.prev();
            var li = [this];
            $this.nextUntil(":not(li)").each(function() {li.push(this);});
            var $ul = $("<ul>");
            li.forEach(function(v, i, a) {
                var $v = $(v);
                $v.detach(); 
                if(!IDB.HtmlUtils.isFlashBlankLine($v)) {
                    $ul.append(v);
                }
                else {
                    var height = $v.find(".idbFont").css("fontSize");
                    var $newDiv = $("<div>").appendTo($ul);
                    if(height) {
                        $newDiv.css({height:height});
                    }
                    $newDiv.append($v.contents());
                }
            });
            if($loc.length > 0) {
                $loc.after($ul);
            }
            else {
                $parent.find(":first").before($ul);
            }
        }
    });

    //
    // Flash wraps text.
    //
    $div.find('span').css({"word-wrap":"break-word"});
};

//
// An element is considered to be a blank line in Flash if all it contains is an empty font element.
//
IDB.HtmlUtils.isFlashBlankLine = function($element) {
    var $idbFont = $element.children(".idbFont");
    
    if($idbFont.length > 0 && $element.children(":not(.idbFont)").length === 0 && 
                              $idbFont.children().length === 0 && $idbFont.text().trim().length === 0) {
        return true;
    }
    else {
        return false;
    }
};

//
// The HTML is considered Flash HTML if the 'textformat' element is present.
// 
IDB.HtmlUtils.isFlashHtml = function($div) {
    return $div.find("textformat").length > 0;
};

IDB.HtmlUtils.hasVerticalScrollbar = function(divNode) {
    if(divNode.scrollHeight > divNode.clientHeight) {
            return true;
    }
};

IDB.HtmlUtils.removeScript = function($div) {
    $div.find("script").remove(); 
};
IDB.HtmlUtils.getUrlParameter = function(name) {
    if (typeof IDB.HtmlUtils.urlParameters === "undefined")
    {
        IDB.HtmlUtils.urlParameters = {};
        var params = location.search.substring(1).split("&");
        for (var i = 0; i < params.length; i++)
        {
            var param = params[i].split("=");
            IDB.HtmlUtils.urlParameters[param[0]] = param[1];
        }
    }
    var value = IDB.HtmlUtils.urlParameters[name];
    if (typeof value === "undefined")
    {
        return null;
    }
    return value;
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.MessagePopup

IDB.MessagePopup = {};

IDB.MessagePopup.showMessage = function($parent, msg) {
    if(!msg) {
        return;
    }
    
    var $popupDiv = $('<div data-role="popup" class="ui-content" data-overlay-theme="a" data-history="false">').appendTo($parent);

    $('<a href="#" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>').appendTo($popupDiv).on("click", function() {$popupDiv.popup('close')});
    var $msgDiv = $('<div class="dialogMsg"></div>').appendTo($popupDiv);
    $msgDiv.html(document.createTextNode(msg));
    $popupDiv.trigger("create");
    $popupDiv.css({"padding":12, "max-width":"400px"});
    $popupDiv.popup();
    $popupDiv.popup("open");
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.NumberUtils
/*global IDB */
IDB.NumberUtils = {};

IDB.NumberUtils.isNumber = function(value) {
    return !isNaN(+value);
};

IDB.NumberUtils.validate = function(value) {
    return IDB.NumberUtils.isNumber(value);
};

IDB.NumberUtils.areEqualWithConversion = function(val1, val2) {
    if(val1 === val2) {
        return true;
    }
       
    if(val1 === 0 && !val2) {
        return false;
    }

    if(val2 === 0 && !val1) {
        return false;
    }

    return val1 == val2;
};

IDB.NumberUtils.areNumberStringsEqual = function(numStr1, numStr2) {
    if(numStr1 === numStr2) {
        return true;
    }

    if(!numStr1 || !numStr2) {
        return false;
    }

    return IDB.NumberUtils.areEqualWithConversion(Number(numStr1), Number(numStr2));
};

Object.defineProperty(IDB.NumberUtils, "ZEROES", {
   value:"000000000000000000000000000000",
   writable:false,
   configurable:false});

Object.defineProperty(IDB.NumberUtils, "DNF", {  /* Default Number Format */
   value:{"al":"left","ts":",","ut":false,"ds":".","cs":"","pr":-1,"rd":"none","un":true},
   writable:false,
   configurable:false});


/**
 * Converts a String representing a number in scientific notation
 * to a String representing the same number in standard notation. Input strings
 * must be in proper form, for example -1.22e+23, -1.22e23, 23e-5, 2.3e-4, etc.
 */
IDB.NumberUtils.sciToStd = function(s) {//(s:String):String {
            
    s = s.toLowerCase();
    if(s.indexOf("e") < 0) return s;
    var arr= s.split("e");
    var mantissa = arr[0];
    if(Number(mantissa) === 0) return "0";
    var exponent = Number(arr[1]);

    if(exponent === 0) return mantissa; // probably impossible

    var sign = "";
    if(mantissa.indexOf("-") === 0) {
        sign = "-";
        mantissa = mantissa.substring(1);
    }
    var mantissaWhole = mantissa;
    var mantissaFraction = "";
    if(mantissa.indexOf(".") > -1) {
        arr = mantissa.split(".");
        mantissaWhole = arr[0];
        mantissaFraction = arr[1];
    }
    var toShift = Math.abs(exponent);
    //IDB.log("toShift=" + toShift + ", mantissaWhole=" + mantissaWhole);
    if(exponent < 0) { // move decimal to the left

        if(mantissaWhole.length > toShift) {
            return (sign + mantissaWhole.substring(0, mantissaWhole.length-toShift) + "." +
                mantissaWhole.substring(mantissaWhole.length-toShift) + mantissaFraction);
        }
        else if(mantissaWhole.length == toShift) {
            return (sign + "0." + mantissaWhole + mantissaFraction);
        }
        else {
            while(mantissaWhole.length < toShift) {
                mantissaWhole = IDB.NumberUtils.ZEROES + mantissaWhole;
            }
            //IDB.log("toShift=" + toShift + ", mantissaWhole=" + mantissaWhole);
            return sign + "0." + mantissaWhole.substring(mantissaWhole.length-toShift) + mantissaFraction;
        }
    }
    else { // move decimal to the right
        if(mantissaFraction.length > toShift) {
            return sign + mantissaWhole + mantissaFraction.substring(0, toShift) + "." + mantissaFraction.substring(toShift);
        }
        else if(mantissaFraction.length === toShift) {
            return sign + mantissaWhole + mantissaFraction;
        }
        else {
            while(mantissaFraction.length < toShift) {
                mantissaFraction = mantissaFraction + IDB.NumberUtils.ZEROES;
            }
            return (sign + mantissaWhole + mantissaFraction.substring(0, toShift));
        }
    }

};


IDB.NumberUtils.format = function (n, format) {
        //IDB.log(n, "INPUT NUMBER");
        if(n === null || IDB.isUndef(n)) {return "";}
        var alignSymbol = "left";
        var thousandsSeparator = "";
        var useThousandsSeparator = false;
        var decimalSeparator = ".";
        var currencySymbol = "";
        var precision = -1;
        var rounding  =  "none";
        var useNegativeSign = true;

//        map.put("al", alignSymbol);
//        map.put("ts", thousandsSeparator);
//        map.put("ut", useThousandsSeparator);
//        map.put("ds", decimalSeparator);
//        map.put("cs", currencySymbol);
//        map.put("pr", precision);
//        map.put("rd", rounding);
//        map.put("un", useNegativeSign);

        if(format) {
            alignSymbol = format.al;
            thousandsSeparator = format.ts;
            useThousandsSeparator = format.ut;
            decimalSeparator = format.ds;
            currencySymbol = format.cs;
            precision = format.pr;
            rounding = format.rd;
            useNegativeSign = format.un;
        }

        var isNegative = n < 0;
        var numStr = n.toString();
        //IDB.log("numStr = " + numStr);
//        var orig = numStr;
        //DFG iDashboards modifications begin here
        var maxDigits = 20;
        var maxLen = maxDigits;
        if(numStr.toLowerCase().indexOf("e") != -1) {
            if(Math.abs(n) < 1) {
                // fractional numbers always get changed to standard notation
                numStr = IDB.NumberUtils._expandExponents(numStr);
                //IDB.log("NumberUtils._expandExponents converted numStr to " + numStr);
            }
            else {
                // If the number can be expressed in standard notation with less than 20 digits, we'll 
                // change it to standard notation here. However, if useThousandsSeparator is true,
                // the dataFormatter will always format it as standard notation, so save the cycles if it's true.
                if(!useThousandsSeparator) {
                    var stdStr = IDB.NumberUtils._expandExponents(numStr);
                    //IDB.log("NumberUtils.format(): stdStr=" + stdStr);
                    if(stdStr.indexOf("-") != -1) maxLen++;
                    if(stdStr.indexOf(".") != -1) maxLen++;
                    //if(DEBUG) debug("CurrencyFormatter.format(): stdStr.length=" + stdStr.length + ", maxLen=" + maxLen);
                    if(stdStr.length <= maxLen) {
                        numStr = stdStr;
                        //IDB.log("NumberUtils.format(): numStr changed from " + orig + " to " + numStr);
                    }
                }
            }
        }
         //DFG iDashboards modifications end here.

        var numArrTemp = numStr.split(".");
        var numFraction = numArrTemp[1] ? numArrTemp[1].length : 0;

        //IDB.log("CurrencyFormatter.format(): numStr is now " + numStr + ", precision=" + precision + ", numFraction=" + numFraction + ", rounding='" + rounding + "'");

        if (precision <= numFraction) {
            if (rounding != "none") {
                numStr = IDB.NumberUtils._formatRoundingWithPrecision(numStr, rounding, precision);
            }
        }

        //IDB.log("CurrencyFormatter.format(): numStr now " + numStr);

        var numValue = Number(numStr);
        if (Math.abs(numValue) >= 1)
        {
            numArrTemp = numStr.split(".");
            //IDB.log("CurrencyFormatter.format(): numArrTemp=" + numArrTemp + ", useThousandsSeparator=" + useThousandsSeparator);
            var front = (useThousandsSeparator ? IDB.NumberUtils._formatThousands(numArrTemp[0], thousandsSeparator, decimalSeparator) :
                numArrTemp[0]);
            //IDB.log("CurrencyFormatter.format(): front=" + front);
            if(numArrTemp[1]) {
                numStr = front + decimalSeparator + numArrTemp[1];
            }
            else {
                numStr = front;
            }
            //IDB.log("CurrencyFormatter.format(): A) numStr now is " + numStr);
        }
        else if (Math.abs(numValue) > 0)    // DFG  (was >=)
        {
            // if the value is in scientefic notation then the search for '.' 
            // doesnot give the correct result. Adding one to the value forces 
            // the value to normal decimal notation. 
            // As we are dealing with only the decimal portion we need not 
            // worry about reverting the addition
            if (numStr.indexOf("e") != -1)    // DFG - Shouldn't happen, because we changed to std notation above.
            {
                var temp = Math.abs(numValue) + 1;
                numStr = temp.toString();
            }

            // Handle leading zero if we got one give one if not don't
            var position = numStr.indexOf(".");
            var leading = position > 0 ? "0" : "";
            numStr = leading + decimalSeparator +
                     numStr.substring(position + 1);
            //IDB.log("CurrencyFormatter.format(): B) numStr now is " + numStr);
        }
        else {             // DFG
            numStr = "0";  // DFG
        }                  // DFG
        
        numStr = IDB.NumberUtils._formatPrecision(numStr, precision, decimalSeparator);

        //IDB.log("CurrencyFormatter.format(): numStr with precision: " + numStr);

        // If our value is 0, then don't show -0
        if (Number(numStr) === 0)
        {
            isNegative = false; 
        }

        if (isNegative) {
            numStr = IDB.NumberUtils._formatNegative(numStr, useNegativeSign);
        }

//        if (!dataFormatter.isValid)
//        {
//            error = defaultInvalidFormatError;
//            return "";
//        }

        // -- currency --
        var baseVal;
        if (alignSymbol != "right")
        {
            if (isNegative) {
                var nSign = numStr.charAt(0);
                baseVal = numStr.substr(1, numStr.length - 1);
                numStr = nSign + currencySymbol + baseVal;
            }
            else {
                numStr = currencySymbol + numStr;
            }
        } 
        else {
            var lastChar = numStr.charAt(numStr.length - 1);
            if (isNegative && lastChar == ")")
            {
                baseVal = numStr.substr(0, numStr.length - 1);
                numStr = baseVal + currencySymbol + lastChar;
            }
            else
            {
                numStr = numStr + currencySymbol;
            }
        }

        //IDB.log("CurrencyFormatter.format(): Returning " + numStr + "\n");

        return numStr;


};

IDB.NumberUtils._formatDecimal = function(value, decimalSeparator) {//(value:String):String
    var parts = value.split(".");
    return parts.join(decimalSeparator);
};

IDB.NumberUtils._formatRounding = function(value, roundType) {//(value:String, roundType:String):String
    var v = Number(value);
    
    if (roundType != "none")
    {
        if (roundType === "up") {
             v = Math.ceil(v);
        }
        else if (roundType === "down") {
            v = Math.floor(v);
        }
        else if (roundType === "nearest") {
            v = Math.round(v);
        }
        else
        {
            //IDB.log("Invalid roundType: " + roundType + ", value=" + value);
        }
    }

    return v.toString();
};

IDB.NumberUtils._formatNegative = function(value, useSign) {//(value:String, useSign:Boolean):String
    if (useSign)
    {
        if (value.charAt(0) != "-") {value = "-" + value;}
    }
    else if (!useSign)
    {
        if (value.charAt(0) == "-") {
                value = value.substr(1, value.length - 1);
        }
        value = "(" + value + ")";
    }
    else
    {
        //IDB.log("ERROR: NumberUtils._formatNegative(): Invalid value: " + value);
    }
    return value;
};

/**
*  Formats a number by setting its decimal precision by using 
*  the <code>decimalSeparatorTo</code> property as the decimal separator.
*
*  @param value Value to be formatted.
*
*  @param precision Number of decimal points to use.
*
*  @return Formatted number.
*  
*/
IDB.NumberUtils._formatPrecision = function(value, precision, decimalSeparator) { // (value:String, precision:int):String {
    // precision works differently now. Its default value is -1
    // which stands for 'do not alter the number's precision'. If a precision
    // value is set, all numbers will contain that precision. Otherwise, there
    // precision will not be changed.
    
    if (precision === -1) {return value;}
    
    var numArr = value.split(decimalSeparator);
    
    numArr[0] = numArr[0].length === 0 ? "0" : numArr[0];
    
    if (precision > 0) {
        var decimalVal = numArr[1] ? numArr[1] : "";
        var fraction = decimalVal + "000000000000000000000000000000000";
        value = numArr[0] + decimalSeparator + fraction.substr(0, precision);
    }
    else {
        value = numArr[0];
    }
    
    return value.toString();
};


/**
 *  Formats a number by rounding it and setting the decimal precision.
 *  The possible rounding types are defined by
 *  mx.formatters.NumberBaseRoundType.
 *
 *  @param value Value to be rounded.
 *
 *  @param roundType The type of rounding to perform:
 *  NumberBaseRoundType.NONE, NumberBaseRoundType.UP,
 *  NumberBaseRoundType.DOWN, or NumberBaseRoundType.NEAREST.
 *
 *  @param precision int of decimal places to use.
 *
 *  @return Formatted number.
 *
 */
IDB.NumberUtils._formatRoundingWithPrecision = function(value, roundType, precision) {//(value:String, roundType:String, precision:int):String {
    // precision works differently now. Its default value is -1
    // which means 'do not alter the number's precision'. If a precision
    // value is set, all numbers will contain that precision. Otherwise, there
    // precision will not be changed. 

    var v = Number(value);
    
    // If rounding is not present and precision is NaN,
    // leave value untouched.
    if (roundType === "none") 
    {
        if (precision === -1 || isNaN(precision)) {
           return v.toString();
        }
    }
    else
    {
        // If rounding is present but precision is less than 0,
        // then do integer rounding.
        if (precision < 0) { 
           precision = 0;
        }
        
        // Shift decimal right as Math functions
        // perform only integer ceil/round/floor.
        v = v * Math.pow(10, precision);
        
        // Attempt to get rid of floating point errors
        v = Number(v.toString()); 
        if (roundType === "up")
        {
            v = Math.ceil(v);
        }
        else if (roundType === "down")
        {
            v = Math.floor(v);
        }
        else if (roundType == "nearest")
        {
            v = Math.round(v);
        }
        else
        {
            //IDB.log("NumberUtils._formatRoundingWithPrecision(): Invalid roundType: " + roundType);
            return "";
        }
        
        // Shift decimal left to get back decimal to original point.
        v = v / Math.pow(10, precision); 
    }

    return v.toString();
};

/**
 *  Formats a number by using 
 *  the <code>thousandsSeparatorTo</code> property as the thousands separator 
 *  and the <code>decimalSeparatorTo</code> property as the decimal separator.
 *
 *  @param value Value to be formatted.
 *
 *  @return Formatted number.
 *  
 *  @langversion 3.0
 *  @playerversion Flash 9
 *  @playerversion AIR 1.1
 *  @productversion Flex 3
 */
IDB.NumberUtils._formatThousands = function(value, thousandsSeparator, decimalSeparator) { //(value:String):String
    decimalSeparator = decimalSeparator || ".";

    var v = Number(value);
    
    var isNegative= (v < 0);
    
    var numStr = Math.abs(v).toString().toLowerCase();
    
    var e = numStr.indexOf("e");
    if (e != -1) { //deal with exponents
       numStr = IDB.NumberUtils._expandExponents(numStr);
    }
        
    var numArr = numStr.split((numStr.indexOf(decimalSeparator) != -1) ? decimalSeparator : ".");
    var numLen = numArr[0].length;

    if (numLen > 3)
    {
        var numSep = Math.floor(numLen / 3);

        if ((numLen % 3) === 0) {
                numSep--;
        }
        
        var b = numLen;
        var a = b - 3;
        
        var arr = [];
        for (var i = 0; i <= numSep; i++)
        {
            arr[i] = numArr[0].slice(a, b);
            a = Math.max(a - 3, 0);
            b = Math.max(b - 3, 1);
        }
        
        arr.reverse();
        
        numArr[0] = arr.join(thousandsSeparator);
    }
    
    numStr = numArr.join(decimalSeparator);
    
    if (isNegative) {
            numStr = "-" + numStr;
    }
    
    return numStr.toString();
};

IDB.NumberUtils._expandExponents = function(value) { //(value:String):String
    //Break string into component parts
    var regExp = /^([+-])?(\d+).?(\d*)[eE]([-+]?\d+)$/;
    var result = regExp.exec(value);
    var sign = result[1];
    var first = result[2];
    var last =  result[3];
    var exp =  Number(result[4]);
            
    //if we didn't get a good exponent to begin with, return what we can figure out
    if (isNaN(exp))
    {
        return (sign ? sign : "") + (first ? first : "0") + (last ? "." + last : "");
    }
            
    var r = first + last;
            
    var decimal = (exp < 0);
            
    if (decimal)
    {   
        var o = (-1 * (first.length + exp) + 1);
        return (sign ? sign : "") + "0." + new Array(o).join("0") + r;
    }
    else
    {
        var i = exp + first.length;
        if (i >= r.length) {
            return (sign ? sign : "") + r + new Array(i - r.length + 1).join("0");
        }
        else {
            return (sign ? sign : "") + r.substr(0,i) + "." + r.substr(i); 
        }
    }
};


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.PositionUtils
/*global IDB */
IDB.PositionUtils = {};
IDB.PositionUtils.Direction = {HORIZONTAL:"horizontal", VERTICAL:"vertical"};
IDB.PositionUtils.VerticalAlignment = {TOP:"top", MIDDLE:"middle", BOTTOM:"bottom"};
IDB.PositionUtils.HorizontalAlignment = {LEFT:"left", CENTER:"center", RIGHT:"right"};


IDB.PositionUtils.position = function($item, $relativeTo, horizontalAlignment, verticalAlignment) {
    //
    // Set the position style if it isn't already set.
    //
    var position = $item.css("position");
    if(!position) {
        $item.css("position", "absolute");
    }

    //
    // Position relative to the offset parent if not specified.
    //
    if(!$relativeTo) {
        $relativeTo = $item.offsetParent();
    }

    var top = -1;
    switch(verticalAlignment) {
       case IDB.PositionUtils.VerticalAlignment.TOP:
           top = 0 + "px";
           break;
       case IDB.PositionUtils.VerticalAlignment.MIDDLE:
           top = ($relativeTo.height() - $item.height())/2 + "px";
           break;
       case IDB.PositionUtils.VerticalAlignment.BOTTOM:
           top = $relativeTo.height() - $item.height() + "px";
           break;
    }

    if(top != -1) {
        $item.css("top", top);
    }

    var left = -1;
    switch(horizontalAlignment) {
       case IDB.PositionUtils.HorizontalAlignment.LEFT:
           left = 0 + "px";
           break;
       case IDB.PositionUtils.HorizontalAlignment.CENTER:
           left = ($relativeTo.width() - $item.width())/2 + "px";
           break;
       case IDB.PositionUtils.HorizontalAlignment.RIGHT:
           left = $relativeTo.width() - $item.width() + "px";
           break;
    }

    if(left != -1) {
        $item.css("left", left);
    }
};

IDB.PositionUtils.center = function($item, horizontal, vertical) {
    horizontal = horizontal || (horizontal == undefined);
    vertical = vertical || (vertical == undefined);
    var $offsetParent = $item.offsetParent();
    if(vertical) {
        var top = ($offsetParent.outerHeight(true) - $item.outerHeight(true))/2 + "px";
        $item.css({"top":top});
    }

    if(horizontal) {
        var left = ($offsetParent.outerWidth(true) - $item.outerWidth(true))/2 + "px";
        $item.css({"left":left});
    }
};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.ReportUtils
/*global IDB */
IDB.ReportUtils = {};

IDB.ReportUtils.buildReportQueryString = function(map) {
    var requestParameters = "";
    for(var key in map) {
        var value = map[key];
        if(value == null) {
            continue;
        }
        if(requestParameters.length !== 0) {
            requestParameters += "&";
        }
      
        if(Array.isArray(value)) {
            for(var i=0; i < value.length; i++) {	
                if(i > 0) {
                    requestParameters += "&";
                }
                requestParameters += (key + "." + i + "=" + encodeURIComponent(String(value[i]) ? String(value[i]) : ""));
            }
        }
        else {
            requestParameters += (key + "=" + encodeURIComponent(String(value) ? String(value) : ""));
        }
    }

    return requestParameters;
};

IDB.ReportUtils.buildUpdatedReportUrl = function(baseUrl, prefixParamMap, requestParams) {
    for(var prefix in prefixParamMap) {
        var params = prefixParamMap[prefix];
        IDB.ReportUtils.removeParams(requestParams, prefix);
        IDB.ReportUtils.addParams(requestParams, prefix, params); 
    }
    var queryString = IDB.ReportUtils.buildReportQueryString(requestParams);
    return baseUrl + (queryString?("?" + queryString):"");
};

IDB.ReportUtils.removeEntriesInList = function(map, toRemove) {
     for(var key in toRemove) {
            delete map[key];
     }
};

IDB.ReportUtils.removeEntriesMatchingPattern = function(map, toRemove) {
    for(var key in map) {
        if(toRemove.test(key)) {
            delete map[key];
        }
    }
};

IDB.ReportUtils.removeParams = function(map, prefix) {
     var regexp = new RegExp("^"+prefix+"[vnt]\\d(\\.\\d+)?$");
    IDB.ReportUtils.removeEntriesMatchingPattern(map, regexp);
};

IDB.ReportUtils.addParams = function(map, prefix, params) {
    for(var i=0; i < params.length; i++) {
        var param = params[i];
        map[prefix+"n"+i] = param.paramName;
        map[prefix+"t"+i] = IDB.Param.getShortDataType(param.dataType);
        map[prefix+"v"+i] = param.value;
    }
};

IDB.ReportUtils.getDashReportUrl = function(dashId, format, inline)
{
    var s = IDB.config.dashReportURL;
    s = s.replace("$DASH_ID$", dashId);
    s = s.replace("$FORMAT$", format.toLowerCase());
    s = s.replace("$INLINE$", inline ? "1" : "0");
    return s;
};
IDB.ReportUtils.buildLoadDashboardReportURL = function(dashboardBase, urlTemplate, drilldownFilters, drilldownParams, token)
{            

    var valueSourceIndicator = IDB.Param.getValueSourceIndicatorForSource("dashboard");

    var requestParameters = IDB.Param.getAdditionalRequestParameters(drilldownFilters, 
                                                                     valueSourceIndicator, 
                                                                     dashboardBase.dashboardParams, 
                                                                     drilldownParams);

    if (token) {
        if (requestParameters.length > 0) requestParameters += "&";
        requestParameters += "dtk=" + token;    
    }

    var chartDashframes = dashboardBase.dashframes;
    for(var j = 0; j < chartDashframes.length; j++)
    {
        var chartDashframe = chartDashframes[j];
        if (chartDashframe._chart)
        {
            var chartRequestParameters = chartDashframe.buildReportRequestParameters("dashboard");
            if (chartRequestParameters)
            {
                if (requestParameters.length > 0) requestParameters += "&";
                requestParameters += chartRequestParameters;
            }
        }
    }

    var url = urlTemplate;

    if (requestParameters.length > 0)
    {
        if (url.indexOf("?") < 0)
        {
            url = url + "?" + requestParameters;
        }
        else
        {
            url = url + "&" + requestParameters;
        }
    }

    return url;
};
IDB.ReportUtils.getChartReportUrl = function(dashId, chartId, format, inline)
{
    var s = IDB.config.chartReportURL;

    s = s.replace("$DASH_ID$", dashId === -1 ? "" : dashId);
    s = s.replace("$CHART_ID$", chartId);
    s = s.replace("$FORMAT$", format.toLowerCase());
    s = s.replace("$INLINE$", inline ? "1" : "0");
    return s;

};
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
// IDB.StringUtils
/*global IDB */
IDB.StringUtils = {};

IDB.StringUtils.rtrim = function(str) {
    if(!str) return '';
    return str.replace(/\s*$/, ''); 
};

IDB.StringUtils.escapeHtml = function(str) {
   if(!str) return "";
   str = str.split('&').join('&amp;');
   str = str.split('<').join('&lt;');
   str = str.split('>').join('&gt;');
   return str;
};

IDB.StringUtils.escapeRegEx = function(str) {
    if(!str) return str;
    return str.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};

IDB.StringUtils.areEqualWithConversion = function(val1, val2) {
    if(val1 === val2) {
        return true;
    }

    if(val1 === "" && !val2) {
        return false;
    }

    if(val2 === "" && !val1) {
        return false;
    }

    return val1 == val2;
};

IDB.StringUtils.parseTokens = function(input, delimiter, escape) {
	if ( escape && escape.length > 1 ) {
		throw "StringUtils.parseTokens(): Invalid escape: '"+escape+
			"': escape may not be more than one character.";
	}

	if ( !delimiter || delimiter.length > 1 ) {
		throw "StringUtils.parseTokens(): Invalid separator: '"+delimiter+
			"': separator must be exactly one character.";
	}

	if ( delimiter == escape ) {
		throw "StringUtils.parseToken(): separator and escape arguments may not be identical. ("+
			delimiter+")";
	}

	if ( input == null ) {
		return [];
	}

	var len = input.length;
	var currToken = '';
	var j, currChar;
	var tokens = [];

	for ( j = 0 ; j < len; j++ ) {
		currChar = input[j];

		if ( escape == currChar ) {
			if ( j == input.length-1 ) {
				throw "StringUtils.parseTokens(): escape character '"+escape+
					"' must be escaped itself when appearing at the end of the input string.";
			}

			currChar = input[++j];
			currToken += currChar;
			continue;
		}

		if ( currChar == delimiter ) {
			tokens.push(currToken);
			currToken = '';
			continue;
		}

		currToken += currChar;
	}

	tokens.push(currToken);
	return tokens;
};


IDB.StringUtils.pairsStringToMap = function(s, pairsDelimChar, delimChar, escapeChar) {
       pairsDelimChar = pairsDelimChar || ':';
       delimChar = delimChar || '|';
       escapeChar = escapeChar || '\\';

       var pairs = IDB.StringUtils.parseTokens(s, delimChar, escapeChar);
       if (!pairs.length) {
               return {};
       }
       
       var map = {};
       var i, pair;
       
       for ( i = 0 ; i < pairs.length ; ++i ) {
               pair = IDB.StringUtils.parseTokens(pairs[i], pairsDelimChar, escapeChar);
               map[pair[0]] = pair[1];
       }
       return map;
};
IDB.StringUtils.trimToNull = function(s)
{
    if (s === null)
    {
        return null;
    }
    s = s.replace(/^\s+|\s+$/g, "");
    if (s === "")
    {
        return null;
    }
    return s;
};

