'use strict';
IDB.PyramidGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
	this._info.calloutStyle = 'custom';
	this._info.legendStyle = (this._graphContext.settings.getBool('PyramidSumAcross') ?
		IDB.LS.AXES : IDB.LS.X_VALUES);
};

IDB.PyramidGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.PyramidGraph.prototype.constructor = IDB.PyramidGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidGraph.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var isCone = sett.getBool('PyramidConeMode');
	var isFunnel = sett.getBool('PyramidFunnelMode');
	var nameKey = (isCone ? 'ConeName' : (isFunnel ? 'FunnelName' : 'PyramidName'));

	sett.set('GraphModeName', IDB.GraphLang.get(nameKey));

	this._pyramids = new IDB.PyramidSet(gc);
	this._pyramidsView = new IDB.PyramidSetView(this._pyramids);
	this._stageLayer.add(this._pyramidsView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidGraph.prototype._update = function(width, height) {
	this._pyramidsView.setSize(width, height);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidGraph.prototype.updateCallout = function(datapoint, $callout) {
	var gc = this._graphContext;
	var sett = gc.settings;
	var sumAcross = sett.getBool('PyramidSumAcross');
	var dpGoup = datapoint.dpGroup;
	var dpGroupLen = dpGoup.length;
	var dpIndex = datapoint.sliceIndex;
	var maxRows = 11;
	var i, dp, isSelected, name, $table, $row;

	// Truncate (if necessary) and store the datapoint group.

	if ( dpGroupLen > maxRows ) {
		var halfMax = Math.floor(maxRows/2);
		var startI = IDB.MathUtil.clamp(dpIndex-halfMax, 0, dpGroupLen-maxRows);

		dpGoup = dpGoup.slice(startI, startI+maxRows);
		dpGroupLen = dpGoup.length;
		datapoint.dpGroup = dpGoup;
	}

	// Create table and its header row.

	$table = $('<table>')
		.appendTo($callout);

	$('<tr>')
		.append(
			$('<td colspan="4" class="idb-header">')
				.text(datapoint.xValue.formattedValue)
				.css('text-align', 'center')
		)
		.appendTo($table);

	// Create datapoint rows.

	for ( i = 0 ; i < dpGroupLen ; ++i ) {
		dp = dpGoup[i];
		isSelected = (dpIndex == dp.sliceIndex);
		name = (sumAcross ? dp.yAxis.axisName : dp.xValue.formattedValue);

		$row = $('<tr>')
			.appendTo($table);

		if ( isSelected && dpGroupLen > 1 ) {
			$row.addClass('idb-callout-highlighted');
		}

		$('<td>')
			.append(
				$('<div>')
					.html('&nbsp;')
					.css('background-color', dp.axisColor.css)
					.css('width', '15px')
			)
			.appendTo($row);
				
		$('<td>')
			.text(name)
			.appendTo($row);

		$('<td>')
			.text(dp.yValue.formattedValue)
			.appendTo($row);

		$('<td>')
			.text(isSelected ? dp.percStr : '')
			.appendTo($row);
	}
};

/*====================================================================================================*/
IDB.PyramidItem = function(graphContext, datapoints, sumAcrossXValue) {
	this._graphContext = graphContext;
	this._datapoints = datapoints;
	this._sumAcrossXValue = sumAcrossXValue;

	this._prepareData();
	this._buildSlices();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype._prepareData = function() {
	var datapoints = this._datapoints;
	var gc = this._graphContext;
	var sett = gc.settings;
	var graphName = sett.get('GraphModeName');
	var max = 70;
	var hasNeg = false;
	var sum = 0;
	var i, val;
	
	if ( datapoints.length > max ) {
		gc.showWarning(IDB.GraphLang.get('PyramidWarningTitle'),
			IDB.GraphLang.get('PyramidRowsWarning', [max, datapoints.length, graphName]));
		datapoints.splice(max, datapoints.length-max);
	}

	for ( i = 0 ; i < datapoints.length ; ++i ) {
		val = datapoints[i].yValue.numberValue;
				
		if ( val <= 0 ) {
			datapoints.splice(i--, 1);
			
			if ( val < 0 ) {
				hasNeg = true;
			}

			continue;
		}
		
		sum += val;
	}

	var dp = datapoints[0];
	this._titleText = (dp ? dp.yAxis.axisName : '');
	
	if ( dp && sett.getBool('PyramidSumAcross') ) {
		this._titleText = this._sumAcrossXValue;
	}
	
	if ( hasNeg ) {
		gc.showWarning(IDB.GraphLang.get('PyramidWarningTitle'),
			IDB.GraphLang.get('PyramidNegativeWarning', [this._titleText, graphName]));
	}

	this._valueSum = sum;
	
	//TODO: update the DatapointRow for "Sum Across" mode?
};


/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype._buildSlices = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var sliceSpacing = sett.get('PyramidSliceSpacing')/200;
	var sum = this._valueSum;
	var datapoints = this._datapoints;
	var dpLen = datapoints.length;
	var percents = [];
	var buildPos = [];
	var buildSum = 0;
	var i, buildGap, buildTotal, val;

	// Calculate the percentages and positions of each slice (in "area ratio" or "standard" modes).
	
	if ( sett.getBool('PyramidAreaRatios') ) {

		buildGap = 1/dpLen*sliceSpacing;
		buildTotal = 1+buildGap*(dpLen-1);

		var funnelMode = sett.getBool('PyramidFunnelMode');
		var sumPerc = 0;
		var sumAreaPerc = 0;
		var perc, areaPerc;
		
		var getNextAreaPerc = (sett.getBool('Pyramid3dMode') ?
			(funnelMode ? 
				function(p) { return 1-Math.pow(1-p, 1/3); } :
				function(p) { return Math.pow(p, 1/3); }
			) :
			(funnelMode ? 
				function(p) { return 1-Math.sqrt(1-p); } :
				function(p) { return Math.sqrt(p); }
			)
		);
		
		for ( i = 0 ; i < dpLen ; ++i ) {
			val = datapoints[i].yValue.numberValue;
			perc = val/sum;
			percents[i] = perc;

			sumPerc = Math.min(1, sumPerc+perc);
			areaPerc = getNextAreaPerc(sumPerc)-sumAreaPerc;
			sumAreaPerc += areaPerc;

			buildPos[i] = [
				buildSum/buildTotal,
				(buildSum+areaPerc)/buildTotal
			];

			buildSum += areaPerc+buildGap;
		}

	}
	else {

		buildGap = sum/dpLen*sliceSpacing;
		buildTotal = sum+buildGap*(dpLen-1);
		
		for ( i = 0 ; i < dpLen ; ++i ) {
			val = datapoints[i].yValue.numberValue;
			percents[i] = val/sum;

			buildPos[i] = [
				buildSum/buildTotal,
				(buildSum+val)/buildTotal
			];

			buildSum += val+buildGap;
		}

	}

	// Build the slices using the calclated information.
	
	this._slices = [];

	var dp, slice;

	for ( i = 0 ; i < dpLen ; ++i ) {
		dp = datapoints[i];
		perc = percents[i];

		//store info for the custom callouts
		dp.sliceIndex = i;
		dp.dpGroup = datapoints;
		dp.percStr = (perc*100).toFixed(1)+'%';

		slice = new IDB.PyramidSlice(gc, dp, buildPos[i][0], buildPos[i][1], perc);
		this._slices[i] = slice;
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.getSliceCount = function() {
	return this._slices.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.getSlice = function(index) {
	return this._slices[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.getTitleText = function() {
	return this._titleText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.is3dMode = function() {
	return false;
	//return this._graphContext.settings.getBool('Pyramid3dMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.isConeMode = function() {
	return false;
	//return this._graphContext.settings.getBool('PyramidConeMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.isFunnelMode = function() {
	return this._graphContext.settings.getBool('PyramidFunnelMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('PyramidLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.showSliceLabel = function() {
	return this._graphContext.settings.get('PyramidSliceLabelOptions').show;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItem.prototype.getRelativeSliceLabelWidth = function() {
	return this._graphContext.settings.get('PyramidSliceLabelWidth')/100;
};

/*====================================================================================================*/
IDB.PyramidItemView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItemView.prototype._buildKinetic = function() {
	var model = this._model;
	var sliceCount = model.getSliceCount();
	var i, slice, sliceModel;

	this._display = new Kinetic.Group();

	this._sliceHold = new Kinetic.Group();
	this._display.add(this._sliceHold);

	this._slices = [];

	for ( i = 0 ; i < sliceCount ; ++i ) {
		sliceModel = model.getSlice(i);

		if ( model.isConeMode() ) {
			slice = null; //LATER: new IDB.PyramidSliceConeView(sliceModel);
		}
		else if ( model.is3dMode() ) {
			slice = null; //LATER: new IDB.PyramidSlice3DView(sliceModel);
		}
		else {
			slice = new IDB.PyramidSliceView(sliceModel);
		}

		this._sliceHold.add(slice.getDisplay());
		this._slices[i] = slice;
	}

	var lblOpt = model.getLabelOptions();

	if ( lblOpt.show ) {
		lblOpt.align = 'center';

		this._title = new IDB.GraphLabel(lblOpt, model.getTitleText());
		this._display.add(this._title.getDisplay());
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItemView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItemView.prototype.getWidthRatio = function() {
	var model = this._model;
	return 1+(model.showSliceLabel() ? model.getRelativeSliceLabelWidth()*2 : 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItemView.prototype.getFixedHeight = function() {
	return (this._title ? this._title.height+11 : 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidItemView.prototype.setSize = function(width, height) {
	var title = this._title;
	var slices = this._slices;
	var sliceLen = slices.length;
	var sliceW = width/this.getWidthRatio();
	var sliceH = height-this.getFixedHeight();
	var sliceX = sliceW/2;
	var lblY = -99999;
	var i, slice;

	for ( i = 0 ; i < sliceLen ; ++i ) {
		slice = slices[i];
		slice.getDisplay().x(sliceX);
		slice.setSize(sliceW, sliceH);

		lblY = slice.checkLabelOverlap(lblY);
	}
	
	if ( title ) {
		title.y = height-title.height-5;
		title.width = sliceW;
		title.truncate();
	}
};

/*====================================================================================================*/
IDB.PyramidSet = function(graphContext) {
	this._graphContext = graphContext;
	graphContext.addDatapointsUpdatedListener(IDB.listener(this, '_onDatapointsUpdated'));
	
	var sett = graphContext.settings;
	var dpGrid = graphContext.datapointGrid;

	////

	this._isSumAcross = sett.getBool('PyramidSumAcross');
	this._sizeRatio = 1/Math.sqrt(0.75);

	////

	var itemCount = (this._isSumAcross ? dpGrid.getNumXRows() : dpGrid.getNumYCols(true));
	var showCount = Math.min(sett.get('PyramidNumShown'), itemCount);
	var buildItemFunc = IDB.listener(this, '_buildPyramid');
	
	this._datapoints = this._buildDatapoints(itemCount);
	this._pages = new IDB.GraphPages(graphContext, buildItemFunc, itemCount, showCount);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSet.prototype._buildDatapoints = function(itemCount) {
	var datapoints = [];
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var xCount = dpGrid.getNumXRows();
	var yCount = dpGrid.getNumYCols(true);
	var sumAcross = this._isSumAcross;
	var i, j, dp;
	
	for ( i = 0 ; i < itemCount ; ++i ) {
		datapoints[i] = [];
				
		if ( sumAcross ) {
			for ( j = 0 ; j < yCount ; ++j ) {
				dp = dpGrid.getDatapointAt(i, j, true);
				datapoints[i][j] = dp;
			}
		}
		else {
			for ( j = 0 ; j < xCount ; ++j ) {
				dp = dpGrid.getDatapointAt(j, i, true);
				dp.axisColor = dpGrid.getColorForXValue(dp.xValue.stringValue);
				datapoints[i][j] = dp;
			}
		}
	}

	return datapoints;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSet.prototype._buildPyramid = function(index) {
	var dpRow = this._datapoints[index];
	var label = (this._isSumAcross ? dpRow[0].xValue.formattedValue : '');
	return new IDB.PyramidItem(this._graphContext, dpRow, label);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSet.prototype._onDatapointsUpdated = function() {
	var pages = this._pages;
	var itemCount = pages.getItemCount();
	var datapoints = this._datapoints;
	var i, j, dpRow, rowCount;
	
	// Does a visible Pyramid already contain a match?

	for ( i = 0 ; i < itemCount ; ++i ) {
		if ( !pages.getItem(i) ) {
			continue;
		}

		dpRow = datapoints[i];
		rowCount = dpRow.length;

		for ( j = 0 ; j < rowCount ; ++j ){
			if ( dpRow[j].getVisualState() == IDB.VS.HIGHLIGHTED ) {
				return;
			}
		}
	}
	
	// Jump to the first Pyramid with a match.

	for ( i = 0 ; i < itemCount ; ++i ) {
		if ( pages.getItem(i) ) {
			continue;
		}

		dpRow = datapoints[i];
		rowCount = dpRow.length;

		for ( j = 0 ; j < rowCount ; ++j ){
			if ( dpRow[j].getVisualState() == IDB.VS.HIGHLIGHTED ) {
				pages.setCurrentIndex(i);
				return;
			}
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSet.prototype.getGraphPages = function() {
	return this._pages;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSet.prototype.getItemModel = function(index) {
	return this._pages.getItem(index);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSet.prototype.getSizeRatio = function() {
	return this._sizeRatio;
};

/*====================================================================================================*/
IDB.PyramidSetView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSetView.prototype._buildKinetic = function() {
	var model = this._model;
	var buildFunc = IDB.listener(this, '_buildPyramid');
	var layoutFunc = IDB.listener(this, '_updatePyramidLayout');
	
	this._display = new Kinetic.Group();

	this._pagesView = new IDB.GraphPagesView(model.getGraphPages());
	this._pagesView.build(buildFunc, layoutFunc);
	this._display.add(this._pagesView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSetView.prototype._buildPyramid = function(index, itemModel) {
	return new IDB.PyramidItemView(itemModel);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSetView.prototype._updatePyramidLayout = function() {
	var model = this._model;
	var pages = model.getGraphPages();
	var currI = pages.getCurrentIndex();
	var maxI = currI+pages.getShowCount();
	var pv = this._pagesView;

	var itemView = pv.getItemView(currI);
	var ratio = model.getSizeRatio()*itemView.getWidthRatio();
	var fixedH = itemView.getFixedHeight();
	
	var spacing = new IDB.GraphSpacing(pages.getShowCount(), 
		pv.getAvailableWidth(), pv.getAvailableHeight(), ratio, fixedH);
	var itemW = spacing.getItemWidth();
	var itemH = spacing.getItemHeight();

	for ( var i = currI ; i < maxI ; ++i ) {
		itemView = pv.getItemView(i);
		itemView.setSize(itemW, itemH);
		itemView.getDisplay().position(spacing.getPos(i-currI));
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSetView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSetView.prototype.setSize = function(width, height) {
	this._pagesView.setSize(width, height);
};

/*====================================================================================================*/
IDB.PyramidSlice = function(graphContext, datapoint, pos0, pos1, percent) {
	this._graphContext = graphContext;
	this._datapoint = datapoint;
	this._pos0 = pos0;
	this._pos1 = pos1;
	this._percent = percent;

	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype._build = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var dp = this._datapoint;

	this._labelText = null;

	if ( this.getLabelOptions().show ) {
		var showX = sett.getBool('PyramidSliceLabelShowXVal');
		var showY = sett.getBool('PyramidSliceLabelShowYVal');
		var showPerc = sett.getBool('PyramidSliceLabelShowPerc');
		var sumAcross = sett.getBool('PyramidSumAcross');
		var t = '';
				
		if ( showX ) {
			t += (sumAcross ? dp.yAxis.axisName : dp.xValue.formattedValue)+
				(showY || showPerc ? ': ' : '');	
		}
				
		if ( showY ) {
			t += dp.yValue.formattedValue+(showPerc ? ' / ' : '');
		}
				
		if ( showPerc ) {
			var dec = sett.get('PyramidSliceLabelPercDec');
			t += (this._percent*100).toFixed(dec)+'%';
		}
		
		this._labelText = t;		
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.getDatapoint = function() {
	return this._datapoint;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.getPos0 = function() {
	return this._pos0;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.getPos1 = function() {
	return this._pos1;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.getLabelText = function() {
	return this._labelText;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.getAlpha = function() {
	return this._graphContext.settings.getAlpha('PyramidSliceTrans');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('PyramidSliceLabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.isFunnelMode = function() {
	return this._graphContext.settings.getBool('PyramidFunnelMode');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.getRelativeLabelWidth = function() {
	return this._graphContext.settings.get('PyramidSliceLabelWidth')/100;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, '_handleClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.createOverListener = function() {
	return IDB.listener(this, '_handleOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype.createOutListener = function() {
	return IDB.listener(this, '_handleOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype._handleClick = function() {
	this._graphContext.onDatapointClicked(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype._handleOver = function() {
	this._graphContext.onDatapointOver(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSlice.prototype._handleOut = function() {
	this._graphContext.onDatapointOut(this._datapoint);
};

/*====================================================================================================*/
IDB.PyramidSliceView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, '_onDatapointsUpdated'));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSliceView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();
	this._display.on('click', model.createClickListener());
	this._display.on('mouseover', model.createOverListener());
	this._display.on('mouseout', model.createOutListener());
	this._display._touchHandler = new IDB.TouchHandler(this._display);

	this._shape = new Kinetic.Line({
		points: [],
		closed: true,
		fill: model.getDatapoint().axisColor.css,
		opacity: model.getAlpha()
	});
	this._display.add(this._shape);

	var lblOpt = model.getLabelOptions();

	if ( lblOpt.show ) {
		lblOpt.align = 'center';

		this._label = new IDB.GraphLabel(lblOpt, model.getLabelText());
		this._display.add(this._label.getDisplay());

		this._labelLine = new Kinetic.Line({
			stroke: lblOpt.color.css,
			strokeWidth: 1,
			opacity: 0.5
		});
		this._display.add(this._labelLine);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSliceView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSliceView.prototype.checkLabelOverlap = function(prevY) {
	var lbl = this._label;

	if ( !lbl ) {
		return;
	}

	if ( Math.abs(lbl.y-prevY) < lbl.textHeight*0.8 ) {
		lbl.setOverlap(true);
		return prevY;
	}

	lbl.setOverlap(false);
	return lbl.y;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSliceView.prototype.setSize = function(width, height) {
	var model = this._model;
	var lbl = this._label;
	var origPos0 = model.getPos0();
	var origPos1 = model.getPos1();
	var pos0 = origPos0;
	var pos1 = origPos1;

	if ( model.isFunnelMode() ) {
		pos0 = 1-pos0;
		pos1 = 1-pos1;
	}

	var posDiff = pos1-pos0;
	var halfW = width/2;
	var xMidPos = pos0+posDiff/2;
	var yMidPos = origPos0+(origPos1-origPos0)/2;
	var shapeX0 = pos0*halfW;
	var shapeX1 = pos1*halfW;
	var shapeY0 = origPos0*height;
	var shapeY1 = origPos1*height;

	this._shape.points([
		-shapeX0, shapeY0,
		shapeX0, shapeY0,
		shapeX1, shapeY1,
		-shapeX1, shapeY1
	]);

	if ( lbl ) {
		lbl.x = halfW;
		lbl.y = yMidPos*height-lbl.height/2;
		lbl.width = Math.min(lbl.textWidth, width*model.getRelativeLabelWidth()*2);
		lbl.truncate();

		var lineY = Math.round(yMidPos*height);

		this._labelLine.points([
			xMidPos*halfW, lineY,
			halfW+1, lineY
		]);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.PyramidSliceView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getDatapoint().getVisualState();

	this._display.opacity(state == IDB.VS.FADED ? 0.2 : 1.0);

	if ( this._label ) {
		this._label.setHighlight(state == IDB.VS.HIGHLIGHTED);
	}
};

/*====================================================================================================*/

