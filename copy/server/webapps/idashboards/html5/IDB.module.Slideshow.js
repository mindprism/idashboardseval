'use strict';
IDB.SlideshowGraph = function(chart, $div, graphProperties, dataSet) {
    var me = this, labelOptions;
    graphProperties.set('ButtonRowShow', !graphProperties.get('HideButtonRow'));
    graphProperties.set('ButtonRowPos', 0);
    labelOptions = graphProperties.get('GallXLabelOptions');
    labelOptions.align = graphProperties.get('GallXLabelTextAlign');
    labelOptions.show = me._showLabel = graphProperties.get('GallXLabelVisible');
    labelOptions.clickable = true;
    graphProperties.set('XValueLabelOptions', labelOptions);
    graphProperties.set('XLabelShadow', graphProperties.get('GallXLabelShadow'));
    graphProperties.set('XValueLabelPos', (graphProperties.get('GallXLabelStyle') == "below")?0:1);
    graphProperties.set('XLabelFunction', IDB.listener(me, "_labelFunction"));

    me._labelPos = graphProperties.get('GallXLabelStyle'); 
    me._imageUrlTemplate = graphProperties.get("ImageUrlTemplate") || "";
    me._useProxy = !!graphProperties.get("UseProxy");
    me._preserveAspectRatio = graphProperties.get("PreserveAspectRatio");
    me._sizing = graphProperties.get("GallSizing");
    me._delay = graphProperties.get("SSSlideIntervalSeconds")*1000;
    me._slideshowEnabled = graphProperties.get("SSSlideEnabled");
    me._transparency = (100-graphProperties.get("SSImageTransparency"))/100;
    me._maxWidth = graphProperties.get("SSMaxFrameWidth");
    me._maxHeight = graphProperties.get("SSMaxFrameHeight");
    me._vAlign = graphProperties.get("SSFrameVAlign");
    me._autoPlay = graphProperties.get("SSSlideAutoplay");
    me._frameBorderThickness = graphProperties.get("GallFrameBorderVisible")?graphProperties.get("GallFrameBorderThickness"):0;

    me._framePosition = {my:"center", at:"center"};
    switch(me._vAlign) {
        case "middle":
            me._framePosition.my = "center";
            me._framePosition.at = "center";
            break;
        case "top":
            me._framePosition.my = "top";
            me._framePosition.at = "top";
            break;
        case "bottom":
            me._framePosition.my = "bottom";
            me._framePosition.at = "bottom";
    }

    me._currentRow = null;

    IDB.GraphBaseKinetic.call(me, chart, $div, graphProperties, dataSet, null);
};

IDB.SlideshowGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.SlideshowGraph.prototype.constructor = IDB.SlideshowGraph;

//  "Slideshow":{
//    "ImageUrlTemplate":null,
//    "UseProxy":false,
//    "GallSizing":"center",
//    "PreserveAspectRatio":true,
//    "SSImageTransparency":0,
//    "SSMaxFrameWidth":0,
//    "SSMaxFrameHeight":0,
//    "SSFrameVAlign":"middle",
//    "GallFrameBorderVisible":false,
//    "GallFrameBorderThickness":1,
//    "GallFrameBorderColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0},
//    "SSSlideEnabled":true,
//    "SSSlideStepButtons":true,
//    "SSSlidePlayPause":true,
//    "SSSlideButtonHeight":60,
//    "SSSlideIntervalSeconds":10,
//    "SSSlideAutoplay":false,
//    "StickySelect":true,
//    "AutoscrollSpeed":10, NOT IMPLEMENTED
//    "HideButtonRow":false,
//    "XLabelTemplate":null,
//    "GallXLabelVisible":true,
//    "GallXLabelStyle":"below",
//    "GallXLabelOptions":{"show":false,"size":25,"color":{"r":0,"g":0,"b":0,"css":"#000000","i":0},"bold":false}
//    "GallXLabelTextAlign":"center",
//    "GallXLabelShadow":true
//}

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.SlideshowGraph.prototype._build = function() {
    var me = this, I = IDB, gc = me._graphContext, gs = gc.settings, showFunc, url, gp = me.gp,
    showStepButtons, showPlayPauseButton, frameBorderColor;

    me._info.xMatchOnly = true;
    me._info.numericYOnly = false;
    me._info.displaysMultipleXValues = false;
    me._info.calloutStyle = 'none';
    me._info.legendStyle = IDB.LS.NONE;

    me._$contentRectDiv = $("<div class=\"idb-slideshow-rect\">").appendTo(me._$div).css({"position":"absolute", "z-index":-100});
    me._$frameDiv = $("<div class=\"idb-slideshow-frame\">").appendTo(me._$div).css({"position":"absolute"});

    if(me._frameBorderThickness > 0) {
        frameBorderColor = gp("GallFrameBorderColor");
        me._$frameDiv.css({"border-style":"solid", "border-color":frameBorderColor.css, "border-width":me._frameBorderThickness+"px"});
    }

    me._framePosition.of = me._$contentRectDiv;

    showStepButtons = gp("SSSlideStepButtons");
    showPlayPauseButton = gp("SSSlidePlayPause");
    me._$buttonBar = $("<div>").appendTo(me._$frameDiv).slideshowButtonBar({
                                            "forwardCallback":IDB.listener(me, "_stepForward"),
                                            "backCallback":IDB.listener(me, "_stepBackward"),
                                            "playCallback":IDB.listener(me, "_startSlideshow"),
                                            "pauseCallback":IDB.listener(me, "_stopSlideshow"),
                                            "showStepButtons":showStepButtons,
                                            "showPlayPauseButton":showPlayPauseButton,
                                            "buttonHeight":gp("SSSlideButtonHeight"),
                                            "visible":false});

    me._stickySelect = gp('StickySelect') || (me._slideshowEnabled && (me._autoPlay || showStepButtons || showPlayPauseButton)); 
    showFunc = IDB.listener(me, '_showRow');
    me._sr = new IDB.GraphSingleRow(gc, showFunc, me._stickySelect);

    me._srView = new IDB.GraphSingleRowView(me._sr);
    me._stageLayer.add(me._srView.getDisplay());

    me._$frameDiv.on("mouseover", IDB.listener(me, "_onFrameMouseOver"));
    me._$frameDiv.on("mouseout", IDB.listener(me, "_onFrameMouseOut"));
    me._$frameDiv.on("click", IDB.listener(me, "_onFrameClick"));
};

IDB.SlideshowGraph.prototype._stepForward = function() {
    this._srView.stepForward();
};

IDB.SlideshowGraph.prototype._stepBackward = function() {
    this._srView.stepBackward();
};


IDB.SlideshowGraph.prototype._onTimer = function() {
    var me = this;
    if(me._$buttonBar.slideshowButtonBar("mouseIsOverStepButton")) {
        return;
    }
    else {
        me._stepForward(); 
        if(me._currentRow) {
            //
            // This should select a matching data row in other single row graph canvases, without
            // any highlight/fade flickering on the other graph types.
            //

            var dp = me._currentRow.datapoints[0];
            me.datapointMouseOver(dp);
            me.datapointMouseOut(dp);
        }
    }
};

IDB.SlideshowGraph.prototype._startSlideshow = function() {
    var me = this;
    me._stopSlideshow();
    me._timer = window.setInterval(IDB.listener(me, "_onTimer"), me._delay);
};

IDB.SlideshowGraph.prototype._stopSlideshow = function() {
      var me = this;
      if(!IDB.isNil(me._timer)) {
          window.clearInterval(me._timer);
          me._timer = null;
      }
};


IDB.SlideshowGraph.prototype._onFrameClick = function(evt) {
    if($(evt.target).hasClass('idb-slideshow-btn')) {
        return;
    }
    var dp = this._currentRow.datapoints[0];
    this.datapointClicked(dp);
};

IDB.SlideshowGraph.prototype._onFrameMouseOver = function(evt) {
     var me = this, index;
     me._$buttonBar.slideshowButtonBar("option", "visible", me._slideshowEnabled); 
     index = me._sr.getSelectedIndex();
     if(index >= 0) {
         me._sr.onItemMouseOver(index);
     }
};

IDB.SlideshowGraph.prototype._onFrameMouseOut = function(evt) {
     var me = this, index;
     me._$buttonBar.slideshowButtonBar("option", "visible", false); 
     index = me._sr.getSelectedIndex();
     if(index >= 0) {
         me._sr.onItemMouseOut(index);
     }
     else {
         if(!me._stickySelect) {
             if(me._$img) {
                 me._$img.remove();
                 me._$img = null;
             }
         }
     }
};


/*----------------------------------------------------------------------------------------------------*/
IDB.SlideshowGraph.prototype._update = function(width, height) {
    var me = this, $img = me._$img, imgAR = me._imgAR, rect, w, h, oh, position, possibleY;

    me._srView.setSize(width, height);
    rect = me._srView.getContentRect();
    w = rect.width;
    h = rect.height;
    me._$contentRectDiv.css({"top":rect.y+"px", "left":rect.x+"px", "height":h+"px", "width":w+"px"}); 
    w = Math.min(w, me._maxWidth) || w;
    h = Math.min(h, me._maxHeight) || h;

    w -= 2*me._frameBorderThickness;
    h -= 2*me._frameBorderThickness;

    me._$frameDiv.width(w);
    me._$frameDiv.height(h);
    me._$frameDiv.position(me._framePosition);

    if(me._showLabel) {
        position = me._$frameDiv.position();
        if(me._labelPos === "below" && me._vAlign !== "bottom") {
            oh = me._$frameDiv.outerHeight();
            possibleY = oh + position.top + 5;
            if(possibleY < rect.height) {
                me._srView.setLabelY(possibleY);
            }
        }
        else if(me._labelPos == "above" && me._vAlign != "top") {
            possibleY = position.top - 5;
            if(possibleY > rect.y) {
                me._srView.setLabelY(possibleY);
            } 
        }
    }

    me._$buttonBar.slideshowButtonBar("resize", w, h);

    if(!imgAR) {
        return;
    }
    
    me._setImageSizeAndPosition(w, h);
    $img.css({"visibility":"visible"});
};

IDB.SlideshowGraph.prototype._setImageSizeAndPosition = function(w, h) {
    var me = this, imgAR = me._imgAR, $img = me._$img, ar, rect, imgW, imgH, imgTop, imgLeft;

    if(me._sizing === "center") {
        $img.css("max-width", w);
        $img.css("max-height", h);
        $img.position(IDB.center(me._$frameDiv));
    }
    else {
        if(me._preserveAspectRatio) {
            ar = w/h;
            if(imgAR > ar) {
                // expand to fit width
                imgW = w;
                imgH = imgW/imgAR;
                imgLeft = 0;
                imgTop = (h-imgH)/2;
            }
            else {
                // expand to fit height
                imgH = h;
                imgW = imgH*imgAR;
                imgLeft = (w-imgW)/2;
                imgTop = 0;
            }
        }
        else {
            imgW = w;
            imgH = h;
            imgLeft = 0;
            imgTop = 0;
        }
       $img.css({"width":imgW+"px", "height":imgH+"px", "top": imgTop+"px", "left":imgLeft+"px"});
   }
};

IDB.SlideshowGraph.prototype._showRow = function(datapointRow) {
    var me = this, I = IDB, url, $img;
    if(datapointRow === null) {
        if(!me._stickySelect) {
            if(me._$img) {
                me._$img.remove();
                me._$img = null;
            }
        }
        return;
    }
    me._currentRow = datapointRow;
    url = me.getImageUrl(datapointRow);
    me._imgW = 0;
    me._imgH = 0;
    me._imgAR = 0;
    $img = $('<img class="idb-slideshow-img">').css({"position":"absolute","visibility":"hidden"}).attr("src", url).one("load", I.listener(me, "_onLoad")).one("error", I.listener(me, "_onError"));
    $img.css({"opacity":me._transparency});

    if(me._$img) {
        me._$img.replaceWith($img);
    }
    else {
       $img.appendTo(me._$frameDiv);
    }
    me._$img = $img;

    //
    // Start the slideshow if indicated.
    // 
    if(me._slideshowEnabled && me._autoPlay) {
     me._$buttonBar.slideshowButtonBar("startOrStopSlideshow"); 
        me._autoPlay = false;
    }

};

IDB.SlideshowGraph.prototype._onLoad = function(evt) {
    var me = this, $img = me._$img;
    me._imgW = $img.width();
    me._imgH = $img.height();
    me._imgAR = me._imgH ? (me._imgW / me._imgH) : 0;
    IDB.log(evt, "_onLoad(): " + $img.width() + ", " + $img.height());
    me.resize();
};

IDB.SlideshowGraph.prototype._onError = function(evt) {
    var me = this, $img = me._$img,
    url = IDB.config.contextRoot + "html5/images/broken-img.png";
    $img.attr("src", url).one("load", IDB.listener(me, "_onLoad"));
};

IDB.SlideshowGraph.prototype.getImageUrl = function(datapointRow) {
   var me = this, I = IDB, config = I.config, gp = me.gp, url = me._imageUrlTemplate,
       isContentUrl = (url.toLowerCase().indexOf("content:") === 0), 
       axes = me._dataSet.axisList, axis, firstY,
       useProxy = (!isContentUrl && config.proxyEnabled && me._useProxy), chart = me._chart;

       if(url === "") {
           firstY = null;
           for(var j=1; j<axes.length; j++) {
               axis = axes[j];
               if(!axis.isGraphAxis()) continue;
               if(!firstY) firstY = axis;
               if(axis.dataTypeIsString) {
                   url = "${uevalue:" + axis.axisName + "}";
                   break;
                }
           }
        }
        if(url === "") {
            url = "${uevalue:" + firstY.axisName + "}";
        }
      
        return I.resolveUrl(chart.expandMacros(url, 'url', datapointRow.datapoints[0]), useProxy);
};

IDB.SlideshowGraph.prototype._labelFunction  = function(dp) {
    var me = this, gp = me.gp, template = gp("XLabelTemplate"), chart = me._chart;

    if(template) {
        return chart.expandMacros(template, null, dp);
    }
    else {
        return dp.xValue.formattedValue;
    }
};

IDB.SlideshowGraph.prototype.destroy = function() {
    IDB.GraphBaseKinetic.prototype.destroy.call(this);
    this._stopSlideshow();
};



/*====================================================================================================*/

