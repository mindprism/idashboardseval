'use strict';
IDB.GalleryGraph = function(chart, $div, graphProperties, dataSet) {
//    graphProperties._settings.GallScrollDirection = "horizontal";
//    graphProperties._settings.GallHighlightMagnifyPercentage = 80;
//    graphProperties._settings.GallMaxFrameRows = 4;
//    graphProperties._settings.GallMaxFrameCols = 4;
//    graphProperties._settings.GallFadeTransparency = 0;
//    graphProperties._settings.GallHFrameGap = 75;
//    graphProperties._settings.GallXLabelStyle = "hover";
//    graphProperties._settings.XLabelTemplate = "IMAGE: ${value:Image}";
//    graphProperties._settings.GallXLabelTextAlign = "right";

	IDB.GraphBase.call(this, chart, $div, graphProperties, dataSet, null);
    var me = this, info = me._info;
    info.calloutStyle = "none";
    info.legendStyle = "none";
    info.xMatchOnly = true;

};

IDB.GalleryGraph.prototype = Object.create(IDB.GraphBase.prototype);
IDB.GalleryGraph.prototype.constructor = IDB.GalleryGraph;

//  "Gallery":{
// x   "ImageUrlTemplate":null,
// x   "UseProxy":false,
// x   "GallSizing":"center",
// x   "PreserveAspectRatio":true,
// x   "GallMaxFrameWidth":150,
// x   "GallMaxFrameHeight":150,
// x   "GallHFrameGap":10,
// x   "GallVFrameGap":10,
// x   "GallFrameBorderVisible":false,
// x  "GallFrameBorderThickness":1,
// x   "GallFrameBorderColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0,"lum":0.0,"x":{"r":229,"g":229,"b":229,"css":"#e5e5e5","i":15066597}},
// x   "GallMaxFrameRows":0,
// x   "GallMaxFrameCols":0,
//    "AutoscrollSpeed":10,
// x   "GallScrollDirection":"vertical",
// x   "GallHighlightMagnifyPercentage":20,
// x   "GallFadeShrinkPercentage":0,
// x   "GallFadeTransparency":30,
// x   "XLabelTemplate":null,
// x   "GallXLabelVisible":true,
// x   "GallXLabelStyle":"hover",
// x   "GallXLabelOptions":{"show":false,"size":25,"color":{"r":0,"g":0,"b":0,"css":"#000000","i":0,"lum":0.0,"x":{"r":229,"g":229,"b":229,"css":"#e5e5e5","i":15066597}},"bold":false},
// x   "GallXLabelSize":25,
// x   "GallXLabelColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0,"lum":0.0,"x":{"r":229,"g":229,"b":229,"css":"#e5e5e5","i":15066597}},
// x   "GallXLabelTextAlign":"center",
// x   "GallXLabelShadow":true
//  },



////////////////////////////////////////////////////////////////////////////
// Graph API methods

IDB.GalleryGraph.prototype._build = function() {
    //this._$div.text("GalleryGraph not implemented.");
    
    var me = this, I = IDB, config = I.config, resolve = I.resolveUrl, gp = me.gp, dataSet = me._dataSet, chart = me._chart,
        template = me.getUrlTemplate(), isContentUrl = (template.toLowerCase().indexOf("content:") === 0),
        useProxy = (!isContentUrl && config.proxyEnabled && !!gp("UseProxy")),
        graphContext = me._graphContext, 
        dpGrid = graphContext.datapointGrid, dpRows = dpGrid._datapointRows, dpRow, dp, url, n, s, prev = null,
        relevantDatapoints = [],
        num = function(p, min, max) {
            var X = gp(p), N = Math.max(min, Math.min(max, X));
            return isNaN(N) ? min : N;
        }, dpm = me._dpm, labelTemplate = gp("XLabelTemplate"), cbase = "idb-gall-btn-", prevBtnClass = "left", nextBtnClass = "right", preTT = "gall.html5.", postTT = "ScrollToolTip";


    me._dpm.reset();
    me._relevantDatapoints = relevantDatapoints;
    me.num = num;
    me._dpRows = dpRows;

    me._width = 0;
    me._height = 0;
    me._pagePointer = 0;

    me.frameBorderVisible = gp("GallFrameBorderVisible");
    IDB.log("VIS: " + me.frameBorderVisible);
    me.frameBorderThickness = gp("GallFrameBorderThickness");
    me.frameBorderColor = gp("GallFrameBorderColor");
    me._preserveAR = gp("PreserveAspectRatio");
    me._sizing = gp("GallSizing");
    n = num("GallHighlightMagnifyPercentage", 0, 200);
    me._magFactor = (100+n)/100;
    n = num("GallFadeShrinkPercentage", 0, 90);
    me._shrinkFactor = (100-n)/100;
    n = num("GallFadeTransparency", 0, 100);
    me._fadeAlpha = 1.0 - (n/100);
    me._tileWidth = me._frameWidth = num("GallMaxFrameWidth", 10, 1000);
    me._labelSize = n = num("GallXLabelSize", 5, 100);
    me._labelColor = gp("GallXLabelColor");
//    me._labelShadowCSS = "0px /* xxx */ 1px /* aaa */ 0px " + ((me._labelColor.lum > 127) ? "#444444" : "#EEEEEE");
    me._labelShadowCSS = "none";
    me._labelAlign = gp("GallXLabelTextAlign");
    me._labelVisible = gp("GallXLabelVisible");
    me._labelHeight = Math.max(n+7,  Math.min(n*1.2));
    me._labelShadow = gp("GallXLabelShadow");
    me._labelStyle = s = gp("GallXLabelStyle");
    me._frameHeight = n = num("GallMaxFrameHeight", 10, 10000);
    if(s === "none" || s === "hover") {
        me._tileHeight = n;
    }
    else {
        me._tileHeight = n + me._labelHeight;
    }

    me._minHGap = gp("GallHFrameGap");
    me._minVGap = gp("GallVFrameGap");
    me._preserveAR = !!gp("PreserveAspectRatio");
    s = gp("GallScrollDirection");
    me._scrollDir = (s !== "vertical") ? "h" : "v";
    if(me._scrollDir === "v") {
        prevBtnClass = "up";
        nextBtnClass = "down"
    }
    me._asSpeed = num("AutoscrollSpeed", 1, 10);
    n = gp("GallMaxFrameCols") || 10000;
    me._maxCols = Math.max(1, Math.min(10000,  n));
    n = gp("GallMaxFrameRows") || 10000;
    me._maxRows = Math.max(1, Math.min(10000,  n));

    me._numStrips = 1;
    me._stripLength = 1;

    me._datapoints = [];

   IDB.log("URL template: " + template);

   for(var j=0, jj=dpRows.length; j<jj; j++) {
       dpRow = dpRows[j];
       dp = dpRow._allGraphDatapoints[0];
       relevantDatapoints.push(dp);
       dpRow.map.datapoint = dp;
       url = resolve(chart.expandMacros(template, 'url', dp), useProxy);
       dpRow.map.url = url;
       if(!labelTemplate) {
           dpRow.map.labelText = dp.xValue.formattedValue;
       }
       else {
           dpRow.map.labelText = chart.expandMacros(labelTemplate, null,  dp);
       }

       if(prev) {
          prev.next = dpRow;
          dpRow.prev = prev;
       }

       prev = dpRow;

//       I.log("URL: " + url);
   }

   me._$grid = $('<div class="idb-gall-grid">').css({"position":"absolute"}).appendTo(me._$div);
   me._$prev = $('<a class="idb-gall-btn">').idbHide().css({"position":"absolute"}).appendTo(me._$div).addClass(cbase + prevBtnClass).attr({title:IDB.format(preTT + prevBtnClass + postTT)})
      .on("click", I.listener(me, "_onPrev"));
   me._$next = $('<a class="idb-gall-btn">').idbHide().css({"position":"absolute"}).appendTo(me._$div).addClass(cbase + nextBtnClass).attr({title:IDB.format(preTT + nextBtnClass + postTT)})
      .on("click", I.listener(me, "_onNext"));

   //me.resize();
};



IDB.GalleryGraph.prototype._update = function(w, h) {
    if(!w || !h) {
        return;
    }
    var me = this, I = IDB, $grid = me._$grid, $prev = me._$prev, $next = me._$next, gp = me.gp, dataSet = me._dataSet,
        graphContext = me._graphContext,
        dpGrid = graphContext.datapointGrid, rowList = dpGrid._datapointRows, dpRow, dp,
        tw = me._tileWidth, th = me._tileHeight, hgap = me._minHGap, vgap = me._minVGap,
        numCols = 1, numRows = 1, layout = me._getLayout(w,h), maxCols = me._maxCols, maxRows = me._maxRows, numStrips, stripLength,
        sd = me._scrollDir, pagePointer = me._pagePointer;
    IDB.log(layout, "\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\nGalleryGraph._update(" + w + ", " + h + ")");
    if(w === me._width && h === me._height) {
        var a = 1;
    }
    $grid.idbXYWH(layout);
    $next.idbXYWH(layout.next);
    $prev.idbXYWH(layout.prev);

    me._width = w;
    me._height = h;

    if(tw > layout.w) {
        tw = layout.w;
    }
    else {
        while(((((numCols+1)*tw) + (numCols*hgap))) <= layout.w) {
            numCols++;
        }
        numCols = Math.min(numCols, maxCols);
    }

    if(th >= layout.h) {
        th = layout.h;
    }
    else {
        while(((((numRows+1)*th) + (numRows*vgap))) <= layout.h) {
            numRows++;
        }
        numRows = Math.min(numRows, maxRows);
    }

    if("h" == sd) {
        me._numStrips = numStrips = numCols;
        me._stripLength = stripLength = numRows;
    }
    else {
        me._numStrips = numStrips = numRows;
        me._stripLength = stripLength = numCols;
    }

    // See if the entire data set will fit on this page.
    var totalTiles = rowList.length;
    var onePage = totalTiles <= (numRows * numCols); 
    var lpi = onePage ? 0 : me._getLastPageIndex(numStrips,  stripLength, totalTiles);

    if(onePage) {
        me._pagePointer = pagePointer = 0; // start from the beginning.
        // adjust the actual number of displayed rows and columns
        // to reflect the fact that the data set fits on a single page.
        if("h" === sd) {
            // how many columns will we have?
            numCols = Math.ceil(totalTiles / stripLength);
            if(numCols === 1) {
                numRows = Math.min(stripLength, totalTiles);
            }
        }
        else {
            numRows = Math.ceil(totalTiles / stripLength);
            if(numRows === 1) {
                numCols = Math.min(stripLength, totalTiles);
            }
        }
        IDB.log("GalleryGraph._update(): scrollDir=" + sd + ", Adjusted values: numRows=" + numRows + ", numCols=" + numCols + ", numStrips=" + numStrips + ", stripLength=" + stripLength);
    }

    // Now that the final numRows and numCols are known, calculate the margins.
    var lmar = 0;
    var rmar = 0;
    var tmar = 0;
    var bmar = 0;

    if(numCols == 1) {
        lmar = rmar = (layout.w - tw)/2;
    }
    else {
        lmar = rmar = (layout.w - (numCols * tw) - ((numCols-1) * hgap) )   /2;
    }

    if(numRows == 1) {
        tmar = bmar = (layout.h - th)/2;
    }
    else {
        tmar = bmar = (layout.h - (numRows * th) - ((numRows-1) * vgap)) /2;
    }

    $next.idbVisible(pagePointer < lpi);
    $prev.idbVisible(pagePointer > 0);

    IDB.log("GalleryGraph._update(): w=" + w + ", h=" + h + ", numRows=" + numRows + ", numCols=" + numCols 
        + ", lmar=" + lmar + ", rmar=" + rmar + ", tmar=" + tmar + ", bmar=" + bmar);

    var numTiles = numRows * numCols;

    me._hideThumbsOutside(pagePointer, pagePointer + numTiles - 1);

    var rowNum = 0, colNum = 0;
    var firstDpRow= rowList[pagePointer];
    var lastThumb = null;
    var dpRow = null;
    var currY, currX;
    var currIndex = pagePointer;
    var thumb;
    if("h" == sd) { // lay out tiles in columns down.
        for(colNum=0; colNum<numCols; colNum++) {
            currX = lmar + (colNum*tw) + (colNum*hgap);
            for(rowNum=0; rowNum<numRows; rowNum++) {
                currY = tmar + (rowNum*th) + (rowNum*vgap);
                
                if(currIndex > (rowList.length-1)) break;
                dpRow = rowList[currIndex++];
                
                thumb = dpRow.map.thumb;
                if(!thumb) {
                    thumb = new IDB.Thumbnail(me, dpRow);
                }
                thumb.setVisualState(dpRow.map.datapoint.getVisualState());
//                if(thumb.visualState == VisualState.HIGHLIGHTED) {
//                    setChildIndex(thumb, numChildren-1); // Bring it above the other thumbs.
//                }
                thumb.layout(currX, currY, tw, th);
            }
        }
    }
    else {   // lay out tiles in rows across
        for(rowNum=0; rowNum<numRows; rowNum++) {
            currY = tmar + (rowNum*th) + (rowNum*vgap);
            for(colNum=0; colNum<numCols; colNum++) {
                currX = lmar + (colNum*tw) + (colNum*hgap);

                if(currIndex > (rowList.length-1)) break;
                dpRow = rowList[currIndex++];

                thumb = dpRow.map.thumb;
                if(!thumb) {
                    thumb = new IDB.Thumbnail(me, dpRow);
                }
                thumb.setVisualState(dpRow.map.datapoint.getVisualState());
//                if(thumb.visualState == VisualState.HIGHLIGHTED) {
//                    setChildIndex(thumb, numChildren-1); // Bring it above the other thumbs.
//                }
                thumb.layout(currX, currY, tw, th);
            }
        }
    }

//    var stripPointer = getStripPointer(_pagePointer);
//    if(stripPointer != _pagePointer) {
//        pagePointer = stripPointer;
//    }

};

IDB.GalleryGraph.prototype.highlightMatchingDatapoints = function(value, matchType, fadeOthers) {
    var me = this, mine = me._callingGraphManager, pts, dpRow, dp,
        pts = IDB.GraphBase.prototype.highlightMatchingDatapoints.call(me, value, matchType, fadeOthers),
        sp, pp, currPage = me._pagePointer, stripLength = me._stripLength, numStrips = me._numStrips, rowIndex, numTiles,
        isVisible, updateNeeded = false, endOfCurrentPage, endOfAdjustedPage, adjustmentNeeded = false, others;
    IDB.log(pts, "MINE: " + mine);
    if(!pts || !pts.length || mine) {
        return pts;
    }
    dp = pts[0];
    dpRow = dp.datapointRow;
    rowIndex = dpRow.rowId;

    sp = me._getStripPointer(rowIndex);
    pp = me._getPagePointer(rowIndex);

    numTiles = (stripLength * numStrips);
    isVisible = (rowIndex >= currPage) && (rowIndex < (currPage + numTiles));

    if(!isVisible) {
        me._pagePointer = currPage = pp;
        updateNeeded = true;
    }

    if(pts.length === 1 || sp === currPage) {
        if(updateNeeded) {
            me._delayedUpdate();
        }
        return pts;
    }

    endOfCurrentPage = currPage + numTiles - 1;
    endOfAdjustedPage = sp + numTiles - 1;
    others = pts.slice(1);
    while(dp = others.shift()) {
        dpRow = dp.datapointRow;
        rowIndex = dpRow.rowId;
        if(rowIndex > endOfCurrentPage && rowIndex <= endOfAdjustedPage) {
            adjustmentNeeded = true;
            break;
        }
    }

    if(adjustmentNeeded) {
        me._pagePointer = sp;
        updateNeeded = true;
    }

    if(updateNeeded) {
        me._delayedUpdate();
    }

    return pts;
};

////////////////////////////////////////////////////////////////////////////
// Private methods
 
IDB.GalleryGraph.prototype._delayedUpdate = function() {
    var me = this, w = me._width, h = me._height;
    setTimeout(function() {
        me._update(w,h);
    }, 0);
}

IDB.GalleryGraph.prototype._getStripPointer = function(index) {
    var me = this, stripLength = me._stripLength;
//            debug("GalleryGraph.getStripPointer(): index=" + index + ", _stripLength=" + _stripLength + ", rowList.length=" + rowList.length);
    return Math.floor(index / stripLength) * stripLength;
};

IDB.GalleryGraph.prototype._getPagePointer = function(index) {
    var me = this, stripLength = me._stripLength, numStrips = me._numStrips, numTiles = (stripLength*numStrips),
        numPagesBefore = Math.floor(index/numTiles),
        ptr = (numPagesBefore * numTiles),
        lpi = me._lastPageIndex();
    if(ptr > lpi) {
        ptr = lpi;
    }
    return ptr;
};



IDB.GalleryGraph.prototype._hideThumbsOutside = function(beginIndex, endIndex) {
    var me = this, dpRows = me._dpRows, dpRow, thumb, numRows = dpRows.length, j;
    for(var j=0; j<beginIndex; j++) {
        thumb = dpRows[j].map.thumb;
        if(thumb) {
            thumb.setAttached(false);
        }
    }

    for(j=numRows-1; j>endIndex; j--) {
        thumb = dpRows[j].map.thumb;
        if(thumb) {
            thumb.setAttached(false);
        }
    }
};


IDB.GalleryGraph.prototype._getLayout = function(w, h) {
    var me = this, sd = me._scrollDir, x = 0, y = 0, 
        BW = Math.min(32, w), BH = Math.min(32, h), w$ = w/2, h$ = h/2, prevX, prevY, nextX, nextY;
    if(sd === "h") {
        BW = Math.min(12, w$);
        prevY = nextY = h$ - BH/2;
        prevX = 0;
        nextX = w - BW;

        IDB.log("BW=" + BW);
        return {x:BW, y:0, w:w-(2*BW), h:h, next:{x:nextX, y:nextY, w:BW, h:BH}, prev:{x:prevX, y:prevY, w:BW, h:BH}};
    }
    
    BH = Math.min(12, h$);
    prevX = nextX = w$ - BW/2;
    prevY = 0;
    nextY = h - BH;

    return {x:0, y:BH, w:w, h:h-(2*BH), next:{x:nextX, y:nextY, w:BW, h:BH}, prev:{x:prevX, y:prevY, w:BW, h:BH}};
};


IDB.GalleryGraph.prototype._getLastPageIndex = function(numStrips, stripLength, numDataRows) {
    var me = this,  numTiles = numStrips * stripLength,  totalNumStrips, i;

    if(numDataRows === undefined) {
        numDataRows = me._graphContext.datapointGrid._datapointRows.length;
    }
    totalNumStrips = Math.ceil(numDataRows / stripLength);
    i = (totalNumStrips * stripLength) - numTiles;
    return Math.max(0, i);
};

IDB.GalleryGraph.prototype._lastPageIndex = function() {
    var me = this;
    return me._getLastPageIndex(me._numStrips, me._stripLength);
};


IDB.GalleryGraph.prototype.getUrlTemplate = function() {
    var me = this, isContentUrlTemplate = false, gp = me.gp, 
        ds = me._dataSet, axes = ds.getGraphAxes(false),
        url = $.trim(gp("ImageUrlTemplate")), firstY = null, axis;


    if(!url) {
        for(var j=0, jj=axes.length; j<jj; j++) {
            axis = axes[j];
            if(!axis.isAnyGraphAxis) continue;
            if(firstY == null) firstY = axis;
            if(axis.dataTypeIsString) {
                url = "${uevalue:" + axis.axisName + "}";
                break;
            }
        }
        if(!url) {
            url = "${uevalue:" + firstY.axisName + "}";
        }
    }

    return url;
};

IDB.GalleryGraph.prototype._scroll = function(strips) {
    var me = this, I = IDB, toInt = I.toInt, pagePointer = toInt(me._pagePointer + (strips * me._stripLength),  0,  me._lastPageIndex());
    I.log("scroll(" + strips + "): pagePointer = " + pagePointer);
    me._pagePointer = pagePointer;
    me._update(me._width, me._height);

};

////////////////////////////////////////////////////////////////////////////
// Event Handlers

IDB.GalleryGraph.prototype._onPrev = function(e) {
    var me = this, numStrips = me._numStrips;
    IDB.log(e, "_onPrev");
    if(e.ctrlKey) {
        me._scroll(-numStrips);
    }
    else if(e.shiftKey) {
        me._pagePointer = 0;
        me._update(me._width, me._height);
    }
    else {
        me._scroll(-1);
    }

};

IDB.GalleryGraph.prototype._onNext = function(e) {
    IDB.log(e, "_onNext");
    var me = this, numStrips = me._numStrips;
    IDB.log(e, "_onPrev");
    if(e.ctrlKey) {
        me._scroll(numStrips);
    }
    else if(e.shiftKey) {
        me._pagePointer = me._lastPageIndex();
        me._update(me._width, me._height);
    }
    else {
        me._scroll(1);
    }

};


/*global IDB,  $ */

IDB.Thumbnail = function(graph, dpRow) {
    var me = this;
    me._graph = graph;
    me._$grid = graph._$grid;
    me._dpRow = null;
    me._datapoint = null;
    me._x = 0;
    me._y = 0;
    me._width = 0;
    me._height = 0;
    me._attached = false;
    me._loaded = false;
    me._preserveAR = graph._preserveAR;
    me._sizing = graph._sizing;
    me._magFactor = graph._magFactor;
    me._shrinkFactor = graph._shrinkFactor;
    me._fadeAlpha = graph._fadeAlpha;
    me._imgAR = 0;
    me._visualState = IDB.VS.NORMAL;
    me.onVisualState = IDB.listener(me, "setVisualState");
    me._build();
    me.setDpRow(dpRow);
};

IDB.Thumbnail.prototype._build = function() {
   var me = this, I = IDB, listener = I.listener, $div, $frameDiv, $border, bt, $img, $label, graph = me._graph, gp = graph.gp,
       labelStyle = graph._labelStyle, labelVisible = graph._labelVisible, labelHeight = graph._labelHeight,
       frameCSS = {"top":"0px", "left":"0px", "bottom":"0px", "right":"0px"},
       labelColor = graph._labelColor;;

   me._$div = $div = $('<div style="position:absolute">').idbVisible(false);


   me._$border = null;
   me._borderThickness = 0;
   me._borderVisible = graph.frameBorderVisible;
   me._$label = null;
   me._$span = null;
   me._labelHover = false;
   me._labelHeight = labelHeight;
   me._frameVOffset = 0;
   if(labelVisible) {
       me._$label = $label = $('<div style="position:absolute;  z-index:auto; white-space:nowrap; padding:0px;">').css({"color":labelColor.css,
          "font-size":graph._labelSize+"px", "text-align":graph._labelAlign
          });

       if(graph._labelShadow) {
           $label.css({"text-shadow":graph._labelShadowCSS})
       }
       me._$span = $('<span>').appendTo($label);
       if(labelStyle === "above") {
           frameCSS.top = labelHeight + "px";
           me._frameVOffset = labelHeight;
       }
       else if(labelStyle === "below") {
           frameCSS.bottom = labelHeight + "px";
           me._frameVOffset = labelHeight;
       }
       else {
           me._labelHover = true;
           $label.idbHide();
       }
       
   }

//    "GallXLabelVisible":true,
//    "GallXLabelStyle":"hover",
//    "GallXLabelOptions":{"show":false,"size":25,"color":{"r":0,"g":0,"b":0,"css":"#000000","i":0,"lum":0.0,"x":{"r":229,"g":229,"b":229,"css":"#e5e5e5","i":15066597}},"bold":false},
//    "GallXLabelSize":25,
//    "GallXLabelColor":{"r":0,"g":0,"b":0,"css":"#000000","i":0,"lum":0.0,"x":{"r":229,"g":229,"b":229,"css":"#e5e5e5","i":15066597}},
//    "GallXLabelTextAlign":"center",
//    "GallXLabelShadow":true


   if(me._borderVisible) {
       me._borderThickness = graph.frameBorderThickness;
       me._$border = $border = $('<div>').appendTo($div).css({"position":"absolute",  "background-color":"transparent",
           "border-style":"solid", "border-width":me._borderThickness+"px", "border-color":graph.frameBorderColor.css}).css(frameCSS);
   }


   me._$frameDiv = $frameDiv = $('<div style="position:absolute;" data-x="frameDiv">').appendTo($div).css(frameCSS)//.css({"background-color":"red"})
      .on("mouseover", listener(me, "_handleOver"))
      .on("mouseout", listener(me, "_handleOut"))
      .on("click", listener(me, "_handleClick"));



   me._$img = $('<img style="position:absolute">').idbPosAbs().appendTo($frameDiv);

   if($label) {
       $label.appendTo($frameDiv);
   }


   me._labelVisible = false;
   me._labelStyle = labelStyle;
};

IDB.Thumbnail.prototype.setDpRow = function(dpRow) {
    var me = this, I = IDB, $img = me._$img, url = (dpRow ? dpRow.map.url : ""), $span = me._$span, labelText = "";

    if(me._dpRow) {
        me._dpRow.map.datapoint.onVisualState = null;
        me._dpRow.map.thumb = null;
    }

    me._dpRow = dpRow || null;
    me._datapoint = null;

    if(dpRow) {
        me._datapoint = dpRow.map.datapoint;
        me._datapoint.onVisualState = me.onVisualState;
        me._visualState = me._datapoint.getVisualState();
        labelText = dpRow.map.labelText;
        me._dpRow.map.thumb = this;
    }

    if($span) {
        $span.text(labelText);
    }
    
    me._loaded = false;
    me._imgAR = 0;
    $img.one("load", I.listener(me, "_onLoad")).attr({"src":url});

};

IDB.Thumbnail.prototype.layout = function(x, y, w, h) {
    var me = this, $div = me._$div, $frameDiv = me._$frameDiv, frameH = h, $img = me._$img, $label = me._$label, $span = me._$span, ls = me._labelStyle, lW, lH, lX, lY, bt = me._borderThickness;
    me._x = x;
    me._y = y;
    me._width = w;
    me._height = h;
    $div.idbVisible(false).css({"top":y+"px", "left":x+"px", "width":w+"px", "height":h+"px"});
    if(!me._attached) {
        // This must be on the DOM for the jQuery width() and height() calls to return non-zero.
        me.setAttached(true);
    }
    frameH = Math.max(0, h-me._frameVOffset);
    if($label) {
        lW = Math.max(w, $span.width());
        lH = me._labelHeight;
        lX = (w-lW)/2;
        if(ls === "above") {
            lY = 0-lH;
        }
        else if(ls === "below") {
            lY = frameH;
        }
        else {
            lY = (frameH - lH)/2;
        }
        $label.idbXYWH({x:lX, y:lY, w:lW, h:lH});
    }
    me._layoutImage();
    $div.idbVisible(true);
};

IDB.Thumbnail.prototype._layoutImage = function() {
    var me = this, $div = me._$div, $frameDiv = me._$frameDiv, $img = me._$img, w = $frameDiv.width(), h = $frameDiv.height(), AR, imgW = me._imgW, imgH = me._imgH, imgAR = me._imgAR, imgX = 0, imgY = 0,
         bt = me._borderThickness, innerWidth = Math.max(10, w-(2*bt)), innerHeight = Math.max(10, h-(2*bt)), vs = me._visualState, VS = IDB.VS;
    if(!me._loaded || !me._imgAR || !w || !h || !me._attached) {
        return;
    }
    AR = innerWidth/innerHeight;
    if(me._sizing === "stretch") {
        if(me._preserveAR) {
            if(AR > imgAR) { // flatter, fit height
                imgH = innerHeight;
                imgW = imgAR * imgH;
            }
            else {
                imgW = innerWidth;
                imgH = imgW / imgAR;
            }
        }
        else { // fit both dimensions to frame
            imgW = innerWidth;
            imgH = innerHeight;
        }
    }
    else { // sizing == "center"
        if(AR > imgAR) { // flatter
            if(imgH > innerHeight) {  // fit height
                imgH = innerHeight;
                imgW = imgAR * imgH;
            }
        }
        else { 
            if(imgW > innerWidth) { // fit width
                imgW = innerWidth;
                imgH = imgW / imgAR;
            }
        }
    }
    if(vs === VS.HIGHLIGHTED) {
        imgW = imgW * me._magFactor;
        imgH = imgH * me._magFactor;
    }
    else if(vs === VS.FADED) {
        imgW = imgW * me._shrinkFactor;
        imgH = imgH * me._shrinkFactor;
    }
    imgX = (w-imgW)/2;
    imgY = (h-imgH)/2;
    $img.css({"top":imgY+"px", "left":imgX+"px", "width":imgW+"px", "height":imgH+"px"});
};

IDB.Thumbnail.prototype.setAttached = function(b) {
    var me = this, $div = me._$div, $grid = me._$grid;
    b = !!b;
    if(me._attached === b) {
        return;
    }
    if(b) {
        $div.appendTo($grid);
    }
    else {
        $div.detach();
    }
    me._attached = b;
};

IDB.Thumbnail.prototype.setVisualState = function(vs) {
    var me = this, VS = IDB.VS, $div = me._$div, $label = me._$label;
    me._visualState = vs;
    if($label) {
        if(me._labelHover) {
            if(vs === VS.HIGHLIGHTED) {
                $label.idbShow();
            }
            else {
                $label.idbHide();
            }
        }
    }
    $div.css({"opacity":(vs === VS.FADED ? me._fadeAlpha : "1"), "z-index":(vs === VS.HIGHLIGHTED ? "100" : "auto")});
    me._layoutImage();
};

////////////////////////////////////////////////////////////////////////////
// Event handlers
////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.Thumbnail.prototype._handleClick = function() {
    this._graph._graphContext.onDatapointClicked(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Thumbnail.prototype._handleOver = function() {
    this._graph._graphContext.onDatapointOver(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.Thumbnail.prototype._handleOut = function() {
    this._graph._graphContext.onDatapointOut(this._datapoint);
};


IDB.Thumbnail.prototype._onLoad = function() {
    var me = this, $img = me._$img;

    me._imgW = $img.width();
    me._imgH = $img.height();
    me._imgAR = me._imgH ? (me._imgW / me._imgH) : 0;
    me._loaded = true;
    me._layoutImage();
};





/*====================================================================================================*/

