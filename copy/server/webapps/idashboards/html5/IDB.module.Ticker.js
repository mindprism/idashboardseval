'use strict';
IDB.TickerBar = function(graphContext) {
	this._graphContext = graphContext;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBar.prototype._build = function() {
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var rowCount = dpGrid.getNumXRows();
	var rows = [];

	for ( var i = 0 ; i < rowCount ; ++i ) {
		rows[i] = new IDB.TickerBarRow(gc, i);
	}

	this._rows = rows;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBar.prototype.getRowCount = function() {
	return this._rows.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBar.prototype.getRow = function(index) {
	return this._rows[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBar.prototype.getFirstHighlightRowIndex = function() {
	var rows = this._rows;
	var rowCount = rows.length;
	var high = IDB.VS.HIGHLIGHTED;

	for ( var i = 0 ; i < rowCount ; ++i ) {
		if ( rows[i].getVisualState() == high ) {
			return i;
		}
	}

	/*var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var rowCount = this._rows.length;
	var colCount = dpGrid.getNumYCols(false);
	var high = IDB.VS.HIGHLIGHTED;
	var i, j, dp;

	for ( i = 0 ; i < rowCount ; ++i ) {
		for ( j = 0 ; j < colCount ; ++j ) {
			dp = dpGrid.getDatapointAt(i, j, false);

			if ( dp.getVisualState() == high ) {
				return i;
			}
		}
	}*/

	return null;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBar.prototype.isVertical = function() {
	return this._graphContext.settings.getBool('ScrollVert');
};

/*====================================================================================================*/
IDB.TickerBarCell = function(graphContext, colIndex, datapoint) {
	this._graphContext = graphContext;
	this._colIndex = colIndex;
	this._datapoint = datapoint;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype._build = function() {
	var gc = this._graphContext;
	var dp = this._datapoint;
	var colI = this._colIndex;
	var sett = gc.settings;
	var isNum = dp.yAxis.dataTypeIsNumber;
	var lblOpt = sett.get('LabelOptions');

	this._lblOpt = {
		show: true,
		size: lblOpt.size,
		color: (isNum ? dp.rangeColor : dp.axisColor),
		bold: lblOpt.bold
	};

	var arrowVal = sett.get('TickerArrowsList')[colI];
	var showArrow = (isNum && arrowVal[0] == '1');
	var flipArrow = (showArrow && arrowVal[1] == '1');

	this._arrowState = (showArrow ? (flipArrow ? -1 : 1) : 0);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype.getText = function() {
	return this._datapoint.yValue.getFV(true);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype.isPositiveValue = function() {
	var val = this._datapoint.yValue.numberValue;
	return (val == 0 || val == null ? null : (val > 0));
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype.getArrowState = function() {
	return this._arrowState;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype.getLabelOptions = function() {
	return this._lblOpt;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, '_handleClick');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype.createOverListener = function() {
	return IDB.listener(this, '_handleOver');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype.createOutListener = function() {
	return IDB.listener(this, '_handleOut');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype._handleClick = function() {
	this._graphContext.onDatapointClicked(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype._handleOver = function() {
	this._graphContext.onDatapointOver(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCell.prototype._handleOut = function() {
	this._graphContext.onDatapointOut(this._datapoint);
};

/*====================================================================================================*/
IDB.TickerBarCellView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCellView.prototype._buildKinetic = function() {
	var model = this._model;
	var lblOpt = model.getLabelOptions();
	var arrowState = model.getArrowState();

	this._display = new Kinetic.Group();
	this._display.on("click", model.createClickListener());
	this._display.on("mouseover", model.createOverListener());
	this._display.on("mouseout", model.createOutListener());
    this._touchHandler = new IDB.TouchHandler(this._display);

	this._hitArea = new Kinetic.Rect({
		fill: 'rgba(0, 0, 0, 0.001)'
	});
	this._display.add(this._hitArea);

	lblOpt.align = 'right';

	this._label = new IDB.GraphLabel(lblOpt, (model.getText() || '')+' ');
	this._label.y = 5;
	this._label.width = this._label.textWidth+6;
	this._display.add(this._label.getDisplay());

	if ( arrowState != 0 ) {
		this._buildArrow();
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCellView.prototype._buildArrow = function() {
	var model = this._model;
	var arrowState = model.getArrowState();
	var isPos = model.isPositiveValue();
	var size = this._label.textHeight*0.6;
	var halfSize = size/2;

	this._arrowSize = size;

	this._arrow = new Kinetic.Group();
	this._display.add(this._arrow);

	if ( isPos == null ) {
		this._display.visible(false);
		return ;
	}

	var tri = new Kinetic.Line({
		points: [
			0, -halfSize,
			-halfSize, halfSize,
			halfSize, halfSize
		],
		closed: true,
		fill: (isPos ? '#0a0' : '#a00'),
		scaleY: (isPos ? 1 : -1)*arrowState
	});
	this._arrow.add(tri);
	
	var shade = new Kinetic.Line({
		points: [
			-halfSize*0.8, halfSize*0.6,
			-halfSize, halfSize,
			halfSize, halfSize,
			halfSize*0.8, halfSize*0.6
		],
		closed: true,
		fill: 'rgba(0, 0, 0, 0.3)',
		scaleY: tri.scaleY()
	});
	this._arrow.add(shade);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCellView.prototype.setTextWidth = function(width) {
	var arrow = this._arrow;

	this._label.width = width;

	if ( arrow ) {
		arrow.x(width+this._arrowSize/2);
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCellView.prototype.updateHitArea = function(pushL, pushR, height) {
	this._hitArea
		.x(-pushL)
		.width(this.getTrueWidth()+pushL+pushR)
		.height(height);

	if ( this._arrow ) {
		this._arrow.y(height/2);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCellView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCellView.prototype.getTrueWidth = function() {
	return this._label.width+(this._arrow ? this._arrowSize*1.4 : 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarCellView.prototype.getTextWidth = function() {
	return this._label.width;
};

/*====================================================================================================*/
IDB.TickerBarRow = function(graphContext, rowIndex) {
	this._graphContext = graphContext;
	this._rowIndex = rowIndex;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype._build = function() {
	var gc = this._graphContext;
	var rowI = this._rowIndex;
	var dpGrid = gc.datapointGrid;
	var colCount = dpGrid.getNumYCols(false);
	var cells = [];
	var datapoints = [];
	var i, dp;

	for ( i = 0 ; i < colCount ; ++i ) {
		dp = dpGrid.getDatapointAt(rowI, i, false);

		if ( i == 0 ) {
			this._text = dp.xValue.getFV(true);
		}

		cells[i] = new IDB.TickerBarCell(gc, i, dp);
		datapoints[i] = dp;
	}

	this._cells = cells;
	this._datapoints = datapoints;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.getText = function() {
	return this._text;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.getCellCount = function() {
	return this._cells.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.getCell = function(index) {
	return this._cells[index];
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.getVisualState = function() {
	var datapoints = this._datapoints;
	var len = datapoints.length;
	var i, vs;

	for ( i = 0 ; i < len ; ++i ) {
		vs = datapoints[i].getVisualState();
		
		if ( vs != IDB.VS.FADED ) {
			return vs;
		}
	}

	return IDB.VS.FADED;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.getLabelOptions = function() {
	return this._graphContext.settings.get('LabelOptions');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.getColor = function() {
	return this._graphContext.settings.get('BannerColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.getHighlightColor = function() {
	return this._graphContext.settings.get('BannerMouseoverColor');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.showBorder = function() {
	return this._graphContext.settings.getBool('BannerBorder');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRow.prototype.getBorderColor = function() {
	return this._graphContext.settings.get('BannerBorderColor');
};

/*====================================================================================================*/
IDB.TickerBarRowView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype._buildKinetic = function() {
	var model = this._model;
	var cellCount = model.getCellCount();
	var lblOpt = model.getLabelOptions();
	var i, cell;

	this._display = new Kinetic.Group();

	////

	this._banner = new Kinetic.Rect({
		fill: 'rgba(0, 0, 0, 0.0001)'
	});
	this._display.add(this._banner);
	
	this._bg = new Kinetic.Rect({
		fill: model.getColor().css
	});
	this._display.add(this._bg);

	this._high = new Kinetic.Rect({
		fill: model.getHighlightColor().css,
		visible: false
	});
	this._display.add(this._high);

	if ( model.showBorder() ) {
		this._border = new Kinetic.Rect({
			stroke: model.getBorderColor().css,
			strokeWidth: 1,
			x: 0.5,
			y: 0.5
		});
		this._display.add(this._border);
	}
	
	////

	lblOpt.align = 'left';

	this._label = new IDB.GraphLabel(lblOpt, model.getText()+' ');
	this._label.x = 6;
	this._label.y = 5;
	this._label.width = this._label.textWidth+4;
	this._display.add(this._label.getDisplay());
	
	this._cells = [];

	this._cellTextWidths = [
		this._label.width
	];
	
	for ( i = 0 ; i < cellCount ; ++i ) {
		cell = new IDB.TickerBarCellView(model.getCell(i));
		this._display.add(cell.getDisplay());

		this._cells[i] = cell;
		this._cellTextWidths.push(cell.getTextWidth());
	}
	
	this._updateCellWidths();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype._updateCellWidths = function() {
	var cells = this._cells;
	var cellCount = cells.length;
	var lbl = this._label;
	var px = lbl.width;
	var i, cell, pushR, pushL;

	var trueH = lbl.height+lbl.y*2-2;

	for ( i = 0 ; i < cellCount ; ++i ) {
		cell = cells[i];
		cell.getDisplay().x(px);

		px += cell.getTrueWidth();
		
		pushL = (i == 0 ? lbl.width+lbl.x : 0);
		pushR = (i == cellCount-1 ? 5 : 0);
		cell.updateHitArea(pushL, pushR, trueH);
	}
	
	var trueW = px+3;
	
	this._banner
		.width(trueW)
		.height(trueH);
		
	this._bg
		.width(trueW-1)
		.height(trueH-1);

	this._high
		.width(trueW-1)
		.height(trueH-1);
	
	if ( this._border ) {
		this._border
			.width(trueW-2)
			.height(trueH-2);
	}

	this._trueW = trueW;
	this._trueH = trueH;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype.setCellWidths = function(widths) {
	var cells = this._cells;
	var cellCount = cells.length;

	this._label.width = widths[0];
	
	for ( var i = 0 ; i < cellCount ; ++i ) {
		cells[i].setTextWidth(widths[i+1]);
	}
	
	this._updateCellWidths();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype.getTrueWidth = function() {
	return this._trueW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype.getTrueHeight = function() {
	return this._trueH;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype.getCellWidths = function() {
	return this._cellTextWidths;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype.isHighlighted = function() {
	return this._high.visible();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarRowView.prototype._onDatapointsUpdated = function() {
	var state = this._model.getVisualState();

	this._display.opacity(state == IDB.VS.FADED ? 0.333 : 1.0);
	this._high.visible(state == IDB.VS.HIGHLIGHTED);
};

/*====================================================================================================*/
IDB.TickerBarView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarView.prototype._buildKinetic = function() {
	var model = this._model;
	var rowCount = model.getRowCount();
	var isVert = model.isVertical();
	var px = 0;
	var py = 0;
	var rows = [];
	var cellMaxWidths = [];
	var row = null;
	var i, j, cellWidths;

	this._display = new Kinetic.Group();

	for ( i = 0 ; i < rowCount ; ++i ) {
		row = new IDB.TickerBarRowView(model.getRow(i));
		row.getDisplay()
			.x(px)
			.y(py);
		this._display.add(row.getDisplay());

		if ( isVert ) {
			py += row.getTrueHeight();
			cellWidths = row.getCellWidths();
			
			for ( j = 0 ; j < cellWidths.length ; ++j ) {
				cellMaxWidths[j] = Math.max((i == 0 ? 0 : cellMaxWidths[j]), cellWidths[j]);
			}
		}
		else {
			px += row.getTrueWidth();
		}

		rows[i] = row;
	}

	if ( isVert ) {
		for ( i = 0 ; i < rowCount ; ++i ) {
			rows[i].setCellWidths(cellMaxWidths);
		}
		
		this._trueW = row.getTrueWidth();
		this._trueH = py;
	}
	else {
		this._trueW = px;
		this._trueH = row.getTrueHeight();
	}

	this._rows = rows;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarView.prototype.setRowVisibleByBounds = function(size) {
	var model = this._model;
	var rows = this._rows;
	var rowCount = rows.length;
	var isVert = model.isVertical();
	var dx = this._display.x();
	var dy = this._display.y();
	var i, row, rowDisp;
	//var debug = "";
	
	if ( isVert ) {
		for ( i = 0 ; i < rowCount ; ++i ) {
			row = rows[i];
			rowDisp = row.getDisplay();
			rowDisp.visible(dy+rowDisp.y()+row.getTrueHeight() > 0 && dy+rowDisp.y() < size);
			//debug += (rowDisp.visible() ? '#' : 'x');
		}
	}
	else {
		for ( i = 0 ; i < rowCount ; ++i ) {
			row = rows[i];
			rowDisp = row.getDisplay();
			rowDisp.visible(dx+rowDisp.x()+row.getTrueWidth() > 0 && dx+rowDisp.x() < size);
			//debug += (rowDisp.visible() ? '#' : 'x');
		}
	}

	//console.log("visibility: "+debug);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarView.prototype.setAllRowsVisible = function() {
	var rows = this._rows;
	var rowCount = rows.length;
	
	for ( var i = 0 ; i < rowCount ; ++i ) {
		rows[i].getDisplay().visible(true);
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarView.prototype.getTrueWidth = function() {
	return this._trueW;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarView.prototype.getTrueHeight = function() {
	return this._trueH;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerBarView.prototype.getFirstHighlightedPos = function() {
	var model = this._model;
	var rowI = model.getFirstHighlightRowIndex();

	if ( rowI == null ) {
		return null;
	}

	var row = this._rows[rowI];

	if ( model.isVertical() ) {
		return row.getDisplay().y()+row.getTrueHeight()/2;
	}

	return row.getDisplay().x()+row.getTrueWidth()/2;
};

/*====================================================================================================*/
IDB.TickerGraph = function(chart, $div, graphProperties, dataSet) {
	IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
};

IDB.TickerGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.TickerGraph.prototype.constructor = IDB.TickerGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerGraph.prototype._build = function() {
	var gc = this._graphContext;
	var noNumAxis = (gc.datapointGrid.getNumYCols(true) == 0);

	this._info.numericYOnly = false;
	this._info.calloutStyle = 'rangeColors';
	this._info.legendStyle = (noNumAxis ? IDB.LS.NONE : IDB.LS.RANGES);

	this._buildTickerArrowMap();

	var scroll = new IDB.TickerScroll(gc);

	this._scrollView = new IDB.TickerScrollView(scroll);
	this._stageLayer.add(this._scrollView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerGraph.prototype._buildTickerArrowMap = function() {
	var gc = this._graphContext;
	var sett = gc.settings;
	var pairsStr = sett.get('TickerArrows');
	var pairs = IDB.StringUtils.parseTokens(pairsStr, '|', '/');

	if ( !pairs.length ) {
		return null;
	}
	
	var map = {};
	var dpGrid = gc.datapointGrid;
	var axesCount = dpGrid.getNumYCols(false);
	var arrowList = [];
	var i, pair, axis, val;
	
	for ( i = 0 ; i < pairs.length ; ++i ) {
		pair = IDB.StringUtils.parseTokens(pairs[i], '=', '$');
		map[pair[0]] = pair[1];
	}

	//NOTE: The 'TickerArrows' setting has an incorrect format, which requires the following workaround.
	//This format incorrectly includes the X-axis, so all the Y-axes have a value off by one position.
	//The workaround is to parse the format as if the X-axis were the first Y-axis, put the values in
	//an array, then request the setting value via the Y-axis index. This format always includes all the
	//axes, and in the correct order.

	for ( i = 0 ; i < axesCount+1 ; ++i ) {
		if ( i == 0 ) {
			axis = dpGrid.getXAxis();
		}
		else {
			axis = dpGrid.getYAxis(i-1, false);
		}

		val = map[axis.axisName];
		arrowList.push(val || '00');
	}
	
	sett.set('TickerArrowsList', arrowList);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerGraph.prototype._update = function(width, height) {
	this._scrollView.setSize(width, height);
};

/*====================================================================================================*/
IDB.TickerScroll = function(graphContext) {
	this._graphContext = graphContext;
	this._build();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype._build = function() {
	var gc = this._graphContext;

	this._bar = new IDB.TickerBar(gc);

	this._allowIncomingHighlights = (gc.settings.get('DatapointMatchType') != IDB.MT.Y_AXIS_NAME);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype.getBar = function() {
	return this._bar;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype.refreshDisplay = function() {
	this._graphContext.refreshDisplay();
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype.getSpeed = function() {
	return this._graphContext.settings.get('ScrollSpeed');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype.isVertical = function() {
	return this._graphContext.settings.getBool('ScrollVert');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype.isOnceOnly = function() {
	return this._graphContext.settings.getBool('ScrollOnce');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype.shouldCenterDatapoint = function() {
	return (this._allowIncomingHighlights && !this._graphContext.isOutgoingDatapointEvent);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScroll.prototype.hasHighlightedDatapoint = function() {
	var dpGrid = this._graphContext.datapointGrid;
	
	//Shortcut assumes either A) all Normal, or B) mix of Highlighted/Faded.
	var dp = dpGrid.getDatapointAt(0, 0, false);
	return (dp.getVisualState() != IDB.VS.NORMAL);

	/*var rowCount = dpGrid.getNumXRows();
	var colCount = dpGrid.getNumYCols(false);
	var i, j, dp;

	for ( i = 0 ; i < rowCount ; ++i ) {
		for ( j = 0 ; j < colCount ; ++j ) {
			dp = dpGrid.getDatapointAt(i, j, false);
			console.log(' - '+i+' / '+j+': '+dp.getVisualState());

			if ( dp.getVisualState() == IDB.VS.HIGHLIGHTED ) {
				return true;
			}
		}
	}

	return false;*/
};

/*====================================================================================================*/
IDB.TickerScrollView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._buildKinetic = function() {
	var model = this._model;

	this._display = new Kinetic.Group();

	this._barHold = new Kinetic.Group();
	this._display.add(this._barHold);

	var bar0 = new IDB.TickerBarView(model.getBar());
	this._barHold.add(bar0.getDisplay());
	
	var bar1 = new IDB.TickerBarView(model.getBar());
	this._barHold.add(bar1.getDisplay());

	this._bars = [bar0, bar1];
	this._currBarI = 0;
	this._nextBarI = 1;
	this._barWidth = bar0.getTrueWidth();
	this._barHeight = bar0.getTrueHeight();
	this._isVert = model.isVertical();
	this._speed = model.getSpeed();
	this._isOnce = model.isOnceOnly();
	this._barFits = false;
	this._doCenter = null;
	this._firstSizing = true;

	this._setNextAnimTimeout();

	this._scrollH = new IDB.GraphScroll(true);
	this._scrollH.onVisualUpdate(IDB.listener(model, 'refreshDisplay'));
	this._scrollH.onScroll(IDB.listener(this, '_onScrollH'));
	this._scrollH.lineScrollSize = 16;
	this._scrollH.getDisplay().rotation(-90);
	this._display.add(this._scrollH.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._setNextAnimTimeout = function() {
	clearTimeout(this._frameTimeout);
	this._frameTimeout = setTimeout(IDB.listener(this, '_handleFrame'), 35);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._onScrollH = function() {
	var px = this._scrollH.scrollPos;
	this._bars[0].getDisplay().x(-px);
	this._bars[1].getDisplay().x(-px);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._handleFrame = function() {
	var model = this._model;
	var curr = this._bars[this._currBarI];
	var next = this._bars[this._nextBarI];
	var currDisp = curr.getDisplay();
	var nextDisp = next.getDisplay();
	var isVert = this._isVert;
	var speed = this._speed;
	var isPaused = (this._barFits || this._hasHighlight);

	var delta = (isPaused ? 0 : speed);
	var dir = (speed > 0 ? 1 : -1);

	var moved = (isVert ?
		this._updateBarY(currDisp, nextDisp, delta, dir) :
		this._updateBarX(currDisp, nextDisp, delta, dir)
	);

	if ( moved ) {
		currDisp.x(Math.round(currDisp.x()));
		currDisp.y(Math.round(currDisp.y()));
		nextDisp.x(Math.round(nextDisp.x()));
		nextDisp.y(Math.round(nextDisp.y()));

		var maskSize = (isVert ? this._maskH : this._maskW);
		curr.setRowVisibleByBounds(maskSize);
		next.setRowVisibleByBounds(maskSize);
	
		model.refreshDisplay();
	}

	if ( isPaused || !moved ) {
		return;
	}

	var stop = false;

	if ( this._isOnce ) {
		stop = (isVert ?
			this._tryStopY(curr) :
			this._tryStopX(curr)
		);
	}

	if ( stop ) {
		this._speed = 0;

		clearTimeout(this._frameTimeout);
		this._frameTimeout = null;
	}
	else {
		this._setNextAnimTimeout();
	}

	var swap = ((isVert ? nextDisp.y() : nextDisp.x())*dir <= 0);
	
	if ( swap ) {
		this._currBarI = (this._currBarI == 0 ? 1 : 0);
		this._nextBarI = (this._nextBarI == 0 ? 1 : 0);
	}
	
	this._doCenter = null;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._updateBarX = function(currDisp, nextDisp, delta, dir) {
	var doCenter = this._doCenter;
	var barW = this._barWidth;
	var newX;

	if ( doCenter == null ) {
		if ( delta == 0 ) {
			return false;
		}

		newX = currDisp.x()-delta;
	}
	else {
		newX = this._maskW/2-doCenter;

		if ( newX*dir > 0 ) {
			newX -= barW*dir;
		}
	}

	currDisp.x(newX);
	nextDisp.x(newX+barW*dir);
	return true;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._updateBarY = function(currDisp, nextDisp, delta, dir) {
	var doCenter = this._doCenter;
	var barH = this._barHeight;
	var newY;

	if ( doCenter == null ) {
		if ( delta == 0 ) {
			return false;
		}

		newY = currDisp.y()-delta;
	}
	else {
		newY = this._maskH/2-doCenter;

		if ( newY*dir > 0 ) {
			newY -= barH*dir;
		}
	}

	currDisp.y(newY);
	nextDisp.y(newY+barH*dir);
	return true;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._tryStopX = function(currDisp) {
	var maskW = this._maskW;
	var barW = this._barWidth;
	var speed = this._speed;
	
	if ( speed < 0 && currDisp.x() >= 0 ) {
		currDisp.x(0);
		return true;
	}

	if ( speed > 0 && currDisp.x() <= maskW-barW ) {
		currDisp.x(Math.round(maskW-barW));
		return true;
	}

	return false;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._tryStopY = function(nextDisp) {
	var maskH = this._maskH;
	var barH = this._barHeight;
	var speed = this._speed;

	if ( speed < 0 && nextDisp.y() >= 0 ) {
		nextDisp.y(0);
		return true;
	}

	if ( speed > 0 && nextDisp.y() <= maskH-barH ) {
		nextDisp.y(Math.round(maskH-barH));
		return true;
	}

	return false;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype.setSize = function(width, height) {
	var curr = this._bars[this._currBarI];
	var next = this._bars[this._nextBarI];
	var currDisp = curr.getDisplay();
	var nextDisp = next.getDisplay();
	
	var isVert = this._isVert;
	var barW = this._barWidth;
	var barH = this._barHeight;
	var scrollH = this._scrollH;
	var scrollVis = (isVert && barW > width);
	var scrollThick = 16;

	this._barFits = false;

	scrollH.getDisplay().visible(scrollVis);

	var maskH = height-(scrollVis ? scrollThick : 0);
	this._maskW = width;
	this._maskH = maskH;

	this._barHold.clip({
		x: 0,
		y: 0,
		width: width,
		height: maskH
	});

	if ( isVert && barH > maskH ) {
		nextDisp.visible(true);
	}
	else if ( !isVert && barW > width ) {
		nextDisp.visible(true);
	}
	else { //no scrolling animation
		if ( !scrollVis ) {
			currDisp.x(Math.round((width-barW)/2));
		}

		currDisp.y(Math.round((maskH-barH)/2));
		curr.setAllRowsVisible();
		nextDisp.visible(false);
		this._barFits = true;
	}
	
	if ( isVert ) {
		if ( !scrollVis ) {
			var shareX = Math.round((width-barW)/2);
			currDisp.x(shareX);
			nextDisp.x(shareX);
		}
	}
	else {
		var shareY = (maskH-barH)/2;
		currDisp.y(shareY);
		nextDisp.y(shareY);
	}
	
	if ( scrollVis ) {
		scrollH.getDisplay()
			.y(nextDisp.visible() ? height : currDisp.y()+barH+scrollThick);
		scrollH.setSize(scrollThick, width);
		scrollH.pageSize = width;
		scrollH.maxScrollPos = barW-scrollH.pageSize;
		scrollH.scrollPos = -currDisp.x();
		scrollH.refresh();
		this._onScrollH();
	}

	if ( this._isOnce && this._firstSizing && this._speed < 0 ) {
		//position the current bar so that it can scroll to its stopping point

		if ( isVert ) {
			currDisp.y(maskH-barH);
		}
		else {
			currDisp.x(width-barW);
		}
	}

	this._setNextAnimTimeout(); //attempt animation restart
	this._firstSizing = false;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.TickerScrollView.prototype._onDatapointsUpdated = function() {
	var model = this._model;
	this._hasHighlight = model.hasHighlightedDatapoint();

	if ( this._hasHighlight && !this._barFits && model.shouldCenterDatapoint() ) {
		var curr = this._bars[this._currBarI];
		this._doCenter = curr.getFirstHighlightedPos();
	}

	this._setNextAnimTimeout(); //attempt animation restart
};

/*====================================================================================================*/

