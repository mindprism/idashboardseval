'use strict';
IDB.MetricsBox = function(graphContext, datapoint, targetDatapoint, ranges) {
	this._graphContext = graphContext;
	this._datapoint = datapoint;
	this._targetDatapoint = targetDatapoint;
	this._ranges = ranges;

	this._color = datapoint.rangeColor;
	this._isBlank = datapoint.yValue.isNothing;
	
	if ( graphContext.settings.getBool('MetricsTargetMode') ) {
		var targNum = targetDatapoint.yValue.numberValue;
		var val = (targNum == 0 ? 0 : datapoint.yValue.numberValue/targNum*100);
		var range = IDB.RangeHelper.getRangeForValue(val, ranges, Number.POSITIVE_INFINITY, true);

		this._color = (range ? range.color : 0x0);
		this._isBlank = (this._isBlank || targetDatapoint.yValue.isNothing);
	}
};

IDB.MetricsBox.ShapeRectangle = 0;
IDB.MetricsBox.ShapeEllipse = 1;
IDB.MetricsBox.ShapeDiamond = 2;
IDB.MetricsBox.ShapeTriangle = 3;
IDB.MetricsBox.ShapeTriangleInverted = 4;
IDB.MetricsBox.ShapeDollar = 5;
IDB.MetricsBox.ShapeEuro = 6;
IDB.MetricsBox.ShapeYen = 7;
IDB.MetricsBox.ShapePound = 8;
IDB.MetricsBox.ShapeSpade = 9;
IDB.MetricsBox.ShapeClub = 10;
IDB.MetricsBox.ShapeHeart = 11;
IDB.MetricsBox.ShapeMusic = 12;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.setOnDatapointsUpdated = function(callback) {
	this._graphContext.addDatapointsUpdatedListener(callback);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.getDatapoint = function() {
	return this._datapoint;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.getColor = function() {
	return this._color;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.getShape = function() {
	return this._graphContext.settings.get('MetricsShape');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.getShapeSize = function() {
	return this._graphContext.settings.get('MetricsShapeSize');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.showShadow = function() {
	return this._graphContext.settings.getBool('MetricsShadowShow');
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.isBlank = function() {
	return this._isBlank;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.isHorizMode = function() {
	return this._graphContext.settings.getBool('HorizMode');
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.createClickListener = function() {
	return IDB.leftClickListener(this, "_handleClick");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.createOverListener = function() {
	return IDB.listener(this, "_handleOver");
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype.createOutListener = function() {
	return IDB.listener(this, "_handleOut");
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype._handleClick = function() {
	this._graphContext.onDatapointClicked(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype._handleOver = function() {
	this._graphContext.onDatapointOver(this._datapoint);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBox.prototype._handleOut = function() {
	this._graphContext.onDatapointOut(this._datapoint);
};

/*====================================================================================================*/
IDB.MetricsBoxView = function(model) {
	this._model = model;
	model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBoxView.prototype._buildKinetic = function() {
	var model = this._model;
	
	this._origSize = 100;
	this._display = new Kinetic.Group();

	this._hold = new Kinetic.Group();
	this._hold.on("click", model.createClickListener());
	this._hold.on("mouseover", model.createOverListener());
	this._hold.on("mouseout", model.createOutListener());
	this._touchHandler = new IDB.TouchHandler(this._hold);
	this._display.add(this._hold);

	this._bg = new Kinetic.Rect({
		fill: 'rgba(0, 0, 0, 0)'
	});
	this._hold.add(this._bg);
	
	if ( model.isBlank() ) {
		this._shape = this._buildBlank();
	}
	else {
		this._shape = this._buildShape();
	}
	
	this._hold.add(this._shape);

	if ( !this._shapeViz ) {
		this._shapeViz = this._shape;
	}
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBoxView.prototype._buildBlank = function() {
	var config = {
		width: this._origSize,
		height: this._origSize,
		stroke: 'rgba(128, 128, 128, 0.5)',
		strokeWidth: 1,
		strokeScaleEnabled: false
	};

	return new Kinetic.Rect(config);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBoxView.prototype._buildShape = function() {
	var model = this._model;
	var s = this._origSize;
	var col = model.getColor();

	var config = {
		closed: true,
		fill: col.css,
		stroke: IDB.toRGBA(col.x, 0.3),
		strokeWidth: 2,
		strokeEnabled: false,
		strokeScaleEnabled: false,
		shadowEnabled: model.showShadow(),
		shadowOffsetX: 2,
		shadowOffsetY: 2,
		shadowBlur: 0,
		shadowColor: 'rgba(0, 0, 0, 0.2)'
	};

	switch ( model.getShape() ) {
		case IDB.MetricsBox.ShapeRectangle:
			config.width = s;
			config.height = s;
			return new Kinetic.Rect(config);
			
		case IDB.MetricsBox.ShapeEllipse:
			config.offsetX = -s/2;
			config.offsetY = -s/2;
			config.width = s;
			config.height = s;
			return new Kinetic.Ellipse(config);
			
		case IDB.MetricsBox.ShapeDiamond:
			config.points = [
				s/2, 0,
				s, s/2,
				s/2, s,
				0, s/2
			];
			return new Kinetic.Line(config);
			
		case IDB.MetricsBox.ShapeTriangle:
			config.points = [
				s/2, 0,
				s, s,
				0, s
			];
			return new Kinetic.Line(config);
			
		case IDB.MetricsBox.ShapeTriangleInverted:
			config.points = [
				s/2, s,
				s, 0,
				0, 0
			];
			return new Kinetic.Line(config);
			
		case IDB.MetricsBox.ShapeDollar:
			return this._buildCharShape('$', -6, -3, 1.16, 1.14);

		case IDB.MetricsBox.ShapeEuro:
			return this._buildCharShape('\u20ac', 3, -10, 1.03, 1.34);
			
		case IDB.MetricsBox.ShapeYen:
			return this._buildCharShape('\u00a5', 0, -12, 1.0, 1.39);
		
		case IDB.MetricsBox.ShapePound:
			return this._buildCharShape('\u00a3', -1, -10, 1.05, 1.34);
		
		case IDB.MetricsBox.ShapeSpade:
			return this._buildCharShape('\u2660', -6, -35, 1.11, 1.68);
		
		case IDB.MetricsBox.ShapeClub:
			return this._buildCharShape('\u2663', -4, -34, 1.09, 1.66);
		
		case IDB.MetricsBox.ShapeHeart:
			return this._buildCharShape('\u2665', -6, -34, 1.11, 1.66);
		
		case IDB.MetricsBox.ShapeMusic:
			return this._buildCharShape('\u266b', -6, -8, 1.22, 1.24);
	}

	return null;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBoxView.prototype._buildCharShape = function(character, x, y, xs, ys) {
	var model = this._model;
	var s = this._origSize;

	var lblOpt = {
		size: s,
		color: model.getColor(),
		bold: true,
		align: 'center'
	};

	var lbl = new IDB.GraphLabel(lblOpt, character);

	lbl.getDisplayLabel()
		.x(x)
		.y(y)
		.scaleX(s/lbl.getDisplayLabel().width()*xs)
		.scaleY(s/lbl.getDisplayLabel().height()*ys)
		.shadowEnabled(model.showShadow())
		.shadowOffsetX(2)
		.shadowOffsetY(2)
		.shadowBlur(0)
		.shadowColor('rgba(0, 0, 0, 0.2)')
		.shadowOpacity(1);

	this._shapeViz = lbl.getDisplayLabel();
	return lbl.getDisplay();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBoxView.prototype.setSize = function(width, height) {
	var model = this._model;
	var size = model.getShapeSize();
	var s = this._origSize;
	var pad = Math.min(width, height)*(1-size);
	var tempW = width;

	if ( model.isHorizMode() ) {
		width = height;
		height = tempW;
	}

	this._bg
		.width(width)
		.height(height);

	this._shape
		.x(pad/2)
		.y(pad/2)
		.scaleX((width-pad)/s)
		.scaleY((height-pad)/s);

	this._hold
		.offsetX(width/2)
		.offsetY(height/2);
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBoxView.prototype.getDisplay = function() {
	return this._display;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsBoxView.prototype._onDatapointsUpdated = function() {
	var model = this._model;
	var state = model.getDatapoint().getVisualState();
	
	this._shapeViz
		.opacity(state == IDB.VS.FADED ? 0.2 : 1.0)
		.strokeEnabled(model.isBlank() || state == IDB.VS.HIGHLIGHTED)
		.shadowOpacity(state != IDB.VS.FADED ? 1 : 0);
};

/*====================================================================================================*/
IDB.MetricsGraph = function(chart, $div, graphProperties, dataSet) {
    var me = this, gtc = graphProperties.getGraphTypeCode() || "";
	IDB.GraphBaseKinetic.call(me, chart, $div, graphProperties, dataSet, null);
	me._info.legendStyle = IDB.LS.RANGES;
    me._info.calloutStyle = "rangeColors";
    if(gtc.indexOf("Target") !== -1) {
        me._info.calloutStyle = "none";
    }
};

IDB.MetricsGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
IDB.MetricsGraph.prototype.constructor = IDB.MetricsGraph;


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsGraph.prototype._build = function() {
	var gc = this._graphContext;

	if ( this._graphContext.settings.getBool('MetricsTargetMode') ) {
		gc.datapointGrid.setDefaultRangeList(IDB.GraphInfo.TargetRangeList);
	}

	this._hold = new Kinetic.Group();
	this._stageLayer.add(this._hold);

	this._grid = new IDB.CartesianGrid(gc);
	var model = new IDB.MetricsPlot(gc, this._grid.getAxes());
	
	this._gridView = new IDB.CartesianGridView(this._grid, null);
	this._hold.add(this._gridView.getDisplay());

	this._plotView = new IDB.MetricsPlotView(model);
	this._hold.add(this._plotView.getDisplay());
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsGraph.prototype._update = function(width, height) {
	var hold = this._hold;
	var gv = this._gridView;
	var pv = this._plotView;
	var pvd = pv.getDisplay();

	if ( this._graphContext.settings.getBool('HorizMode') ) {
		hold.rotation(90);
		hold.x(width);
		gv.setSize(height, width);
	}
	else {
		hold.rotation(0);
		hold.x(0);
		gv.setSize(width, height);
	}
	
	pvd.x(gv.getPlotPosX());
	pvd.y(gv.getPlotPosY());
	pv.setSize(gv.getPlotWidth(), gv.getPlotHeight());
	
	pvd.clip({
		x: 0,
		y: -3,
		width: gv.getPlotWidth(),
		height: gv.getPlotHeight()+6
	});
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsGraph.prototype.getLegendAxes = function() {
	var gc = this._graphContext;

	if ( !gc.settings.getBool('MetricsTargetMode') ) {
		return IDB.GraphBase.prototype.getLegendAxes.call(this);
	}

	var dpGrid = gc.datapointGrid;
	var yCount = dpGrid.getNumYCols(true);
	var axes = [ dpGrid.getXAxis() ];

	for ( var i = 1 ; i < yCount ; i += 2 ) {
		axes.push(dpGrid.getYAxis(i, true));
	}

	return axes;
};

/*====================================================================================================*/
IDB.MetricsPlot = function(graphContext, yMins, yMaxes) {
	this._graphContext = graphContext;
	this._yMins = yMins;
	this._yMaxes = yMaxes;
	this._boxes = this._buildBoxes();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsPlot.prototype._buildBoxes = function() {
	var boxes = [];
	var gc = this._graphContext;
	var dpGrid = gc.datapointGrid;
	var sett = gc.settings;
	var xAxesCount = dpGrid.getNumXRows();
	var yAxesCount = dpGrid.getNumYCols(true);
	var targ = sett.getBool('MetricsTargetMode');
	var flip = sett.getBool('HorizMode');
	var jInc = (targ ? 2 : 1);
	var dpTarg = null;
	var rList = null;
	var i, j, dp, box, z;
	
	if ( targ ) {
		yAxesCount = Math.floor(yAxesCount/2)*2;
	}
	
	for ( i = 0 ; i < xAxesCount ; ++i ) {
		boxes[i] = [];
		
		for ( j = 0 ; j < yAxesCount ; j += jInc ) {
			dp = dpGrid.getDatapointAt(i, j, true);
			
			if ( targ ) {
				dpTarg = dp;
				dp = dpGrid.getDatapointAt(i, j+1, true);
				rList = dpGrid.getRangeList(j+1, true);
			}
			
			box = new IDB.MetricsBox(gc, dp, dpTarg, rList);
			z = (flip ? yAxesCount-j-1 : j);
			boxes[i][Math.floor(z/jInc)] = box;
		}
	}

	return boxes;
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsPlot.prototype.getBoxXCount = function() {
	return this._boxes.length;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsPlot.prototype.getBoxYCount = function() {
	return (this._boxes.length ? this._boxes[0].length : 0);
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsPlot.prototype.getBox = function(xIndex, yIndex) {
	return this._boxes[xIndex][yIndex];
};

/*====================================================================================================*/
IDB.MetricsPlotView = function(model) {
	this._model = model;
	this._buildKinetic();
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsPlotView.prototype._buildKinetic = function() {
	this._display = new Kinetic.Group();

	this._boxes = [];

	var model = this._model;
	var xCount = model.getBoxXCount();
	var yCount = model.getBoxYCount();
	var i, j, box;

	for ( i = 0 ; i < xCount ; ++i ) {
		this._boxes[i] = [];

		for ( j = 0 ; j < yCount ; ++j ) {
			box = new IDB.MetricsBoxView(model.getBox(i, j));
			this._display.add(box.getDisplay());
			this._boxes[i][j] = box;
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsPlotView.prototype.getDisplay = function() {
	return this._display;
};

/*----------------------------------------------------------------------------------------------------*/
IDB.MetricsPlotView.prototype.setSize = function(width, height) {
	var model = this._model;
	var boxes = this._boxes;
	var xCount = model.getBoxXCount();
	var yCount = model.getBoxYCount();
	var boxW = width/xCount;
	var boxH = height/yCount;
	var i, j, box, px;

	for ( i = 0 ; i < xCount ; ++i ) {
		px = (i+0.5)*boxW;

		for ( j = 0 ; j < yCount ; ++j ) {
			box = boxes[i][j];
			box.setSize(boxW, boxH);
			box.getDisplay()
				.x(px)
				.y(((j+0.5)/yCount)*height);
		}
	}
};

/*====================================================================================================*/

