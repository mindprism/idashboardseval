// Z_DOT file::IDB.module.Area.js z.29193761005683301.2019.04.09.04.38.59.192|file
/* global IDB, Kinetic, Kinetic.Group, jQueryElement, Point*/
/**
 * Kinetic Area module.
 * @module Area
 * @see IDB.AreaAxis
 * @see IDB.AreaAxisView
 * @see IDB.AreaGraph
 * @see IDB.AreaPlot
 * @see IDB.AreaPlotView
 * @see IDB.AreaSegment
 * @see IDB.AreaSegmentView
 */
'use strict';
// Z_DOT ctor AreaAxis(graphContext, cartGridYAxis, stacks, yAxisIndex):AreaAxis      ::IDB.module.Area.js  z.57046761005683301.2019.04.09.04.39.24.075|class
  /**
   * AreaAxis constructor
   * @class AreaAxis
   * @constructor
   * @memberof IDB
   *
   * @param {Object} graphContext
   * @param {Object} cartGridYAxis
   * @param {Object} stacks
   * @param {Number} yAxisIndex
   *
   * @see z.57046761005683301
   * @see AreaAxisView
   * @see module:Area
   */
  IDB.AreaAxis = function(graphContext, cartGridYAxis, stacks, yAxisIndex) {
      /**
       * @var {Object} _grid = new IDB.CartesianGrid(this._graphContext)
       * @memberof IDB.AreaGraph.prototype
       * @private
       */

      this._graphContext = graphContext;
      this._cartGridYAxis = cartGridYAxis;
      this._stacks = stacks;
      this._yAxisIndex = yAxisIndex;
      //
      var yAxisId = graphContext.datapointGrid.getYAxis(yAxisIndex, true).axisId;
      this._stats = cartGridYAxis.getStatsByAxis(yAxisId);
      //
      this._segments = this._buildSegments();
    }; //-IDB.AreaAxis
  // Z_DOT cmd _buildSegments():[IDB.AreaSegment]                               ::IDB.module.Area.js  z.85226961005683301.2019.04.09.04.42.42.258|-!
    /**
     * Build Segments
     * @memberof IDB.AreaAxis
     * @private
     * @method
     * @return {Array<IDB.AreaSegment>} segments
     * @see z.85226961005683301
     */
    IDB.AreaAxis.prototype._buildSegments = function() {
      var segments = [];
      var gc = this._graphContext;
      var sett = gc.settings;
      var dpGrid = gc.datapointGrid;
      var stacks = this._stacks;
      var stats = this._stats;
      var range = stats.max-stats.min;
      var rowCount = dpGrid.getNumXRows();
      var colCount = dpGrid.getNumYCols(true);
      var stackMaxY = colCount-1;
      var yIndex = this._yAxisIndex;
      // noinspection EqualityComparisonWithCoercionJS
      var isLastY = (yIndex == colCount-1);
      var stackedMode = sett.getBool('AreaStackedMode');
      var ratioMode = sett.getBool('AreaRatioMode');
      var showLabels = sett.get('AreaLabelOptions').show;
      var labelStackedSum = sett.getBool('AreaLabelStackedSum');
      var i, dp, val, stackMax, lblText, relY, seg;
      //
      for ( i = 0 ; i < rowCount ; ++i ) {
        dp = dpGrid.getDatapointAt(i, yIndex, true);
        lblText = null;
        //
        if ( stackedMode ) {
          val = stacks[i][yIndex];
        }
        else if ( ratioMode ) {
          stackMax = Math.abs(stacks[i][stackMaxY]);
          // noinspection MagicNumberJS
          val = (0 === stackMax ? 0 : stacks[i][yIndex]/stackMax*100);
        }
        else {
          val = dp.yValue.numberValue;
        }
        //
        if ( showLabels ) {
          if ( labelStackedSum ) {
            lblText = (isLastY ? sett.formatNumber(val) : null);
          }
          else {
            lblText = dp.yValue.getFV(true);
          }
        }
        //
        relY = (val-stats.min)/range;
        //
        seg = new IDB.AreaSegment(gc, dp, relY, lblText);
        segments[i] = seg;
      }
      //
      return segments;
      }; //-IDB.AreaAxis.prototype._buildSegments
  // Z_DOT ppt getSegmentCount():Number                                              ::IDB.module.Area.js  z.05477371005683301.2019.04.09.04.49.37.450|+@.
    /**
     * Segment Count
     * @memberof IDB.AreaAxis
     * @return {Number} this._segments.length
     * @see z.05477371005683301
     */
    IDB.AreaAxis.prototype.getSegmentCount = function() {
      return this._segments.length;
      }; //-IDB.AreaAxis.prototype.getSegmentCount
  // Z_DOT ppt getSegment(index):IDB.AreaSegment                                     ::IDB.module.Area.js  z.47621471005683301.2019.04.09.04.50.12.674|+@.
    /**
     * Segment
     * @memberof IDB.AreaAxis
     * @param {Number} index
     * @return {IDB.AreaSegment} this._segments[index]
     * @see z.47621471005683301
     */
    IDB.AreaAxis.prototype.getSegment = function(index) {
      return this._segments[index];
      }; //-IDB.AreaAxis.prototype.getSegment
  // Z_DOT ppt getRelativeYZero():IDB.AreaSegment                                    ::IDB.module.Area.js  z.53058671005683301.2019.04.09.04.54.45.035|+@.
    /**
     * Relative Y Zero
     * @memberof IDB.AreaAxis
     * @return {Number} (1-(0-this._stats.min)/(this._stats.max-this._stats.min))
     * @see z.53058671005683301
     */
    IDB.AreaAxis.prototype.getRelativeYZero = function() {
      return (1-(0-this._stats.min)/(this._stats.max-this._stats.min));
      }; //-IDB.AreaAxis.prototype.getRelativeYZero
  // Z_DOT ppt getYAxisIndex():Number                                                ::IDB.module.Area.js  z.93776771005683301.2019.04.09.04.56.07.739|+@.
    /**
     * Y Axis Index
     * for 3D mode
     * @memberof IDB.AreaAxis
     * @return {Number} this._yAxisIndex
     * @see z.93776771005683301
     */
    IDB.AreaAxis.prototype.getYAxisIndex = function() {
      return this._yAxisIndex;
    };
  // Z_DOT ppt getLabelOptions():Object                                              ::IDB.module.Area.js  z.51101871005683301.2019.04.09.04.56.50.115|+@.
    /**
     * Label Options
     * @memberof IDB.AreaAxis
     * @return {Object} this._graphContext.settings.get('AreaLabelOptions')
     * @see z.51101871005683301
     */
    IDB.AreaAxis.prototype.getLabelOptions = function() {
      return this._graphContext.settings.get('AreaLabelOptions');
    };
// Z_DOT ctor AreaAxisView(model):AreaAxisView                                        ::IDB.module.Area.js  z.99727681005683301.2019.04.09.05.11.12.799|class
  /**
   * AreaAxisView constructor
   * @class AreaAxisView
   * @constructor
   * @memberof IDB
   *
   * @param {Object} model
   *
   * @see z.99727681005683301
   * @see AreaAxis
   * @see module:Area
   */
  IDB.AreaAxisView = function(model) {
      this._model = model;
      this._buildKinetic();
    }; //-IDB.AreaAxisView
  // Z_DOT cmd _buildKinetic():void          ::IDB.module.Area.js  z.95358291005683301.2019.04.09.05.21.25.359|-!
    /**
     * Build Kinetic
     * @memberof IDB.AreaAxisView
     * @private
     *
     * @see z.95358291005683301
     * @see Kinetic.Group
     */
    IDB.AreaAxisView.prototype._buildKinetic = function() {
      this._display = new Kinetic.Group(undefined);
      this._segments = [];
      this._labels = [];
      //
      var model = this._model;
      var segCount = model.getSegmentCount();
      var lblOpt = model.getLabelOptions();
      var i, segModel, seg, lbl, lblText;
      //
      for ( i = 0 ; i < segCount ; ++i ) {
        segModel = model.getSegment(i);
        //
        seg = new IDB.AreaSegmentView(segModel);
        this._display.add(seg.getDisplay());
        //
        this._segments[i] = seg;
      }
      //
      if ( !lblOpt.show ) {
        return;
      }
      //
      for ( i = 0 ; i < segCount ; ++i ) {
        segModel = model.getSegment(i);
        lblText = segModel.getLabelText();
        //
        if ( !segModel.getActive() || !lblText ) {
          continue;
        }
        //
        lbl = new IDB.GraphLabel(lblOpt, lblText);
        lbl.rotateFromCenter(lblOpt.rotation);
        lbl.getDisplay().listening(false);
        //
        this._segments[i].setLabelRef(lbl);
        this._labels[i] = lbl;
      }
      }; //-IDB.AreaAxisView.prototype._buildKinetic
  // Z_DOT ppt getDisplay():Kinetic.Group    ::IDB.module.Area.js  z.95935391005683301.2019.04.09.05.22.33.959|+@.
    /**
     * Display
     * @memberof IDB.AreaAxisView
     * @return {Kinetic.Group} this._display
     * @see z.95935391005683301
     */
    IDB.AreaAxisView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.AreaAxisView.prototype.getDisplay
  // Z_DOT ppt getLabels():[IDB.GraphLabel]  ::IDB.module.Area.js  z.66762491005683301.2019.04.09.05.23.46.766|+@.
    /**
     * Labels
     * @memberof IDB.AreaAxisView
     * @return {IDB.GraphLabel} this._labels
     * @see z.66762491005683301
     */
    IDB.AreaAxisView.prototype.getLabels = function() {
      return this._labels;
      }; //-IDB.AreaAxisView.prototype.getLabels
  // Z_DOT ppt setSize(width, height):void   ::IDB.module.Area.js  z.17026991005683301.2019.04.09.05.32.42.071|+.
    /**
     * Size
     * @memberof IDB.AreaAxisView
     * @method
     * @param {Number} width
     * @param {Number} height
     * @see z.17026991005683301
     */
    IDB.AreaAxisView.prototype.setSize = function(width, height) {
      var model = this._model;
      var segments = this._segments;
      var segCount = segments.length;
      var segW = width/segCount;
      var lblPos = model.getLabelOptions().position;
      var i, seg, segModel, segDisp, segX, relDot, lbl;
      //
      for ( i = 0 ; i < segCount ; ++i ) {
        seg = segments[i];
        segModel = model.getSegment(i);
        segDisp = seg.getDisplay();
        // noinspection MagicNumberJS
        segX = segW*(i+0.5);
        //
        segDisp.x(segX);
        seg.setSize(segW, height);
        //
        lbl = seg.getLabelRef();
        //
        if ( lbl ) {
          relDot = segModel.getRelativeDotPoint();
          //
          lbl.x = segX+relDot.x*segW;
          lbl.y = IDB.GraphLabel.clampYPosition(lbl, relDot.y*height-lblPos, height);
        }
      }
      }; //-IDB.AreaAxisView.prototype.setSize
// Z_DOT ctor AreaGraph(chart, $div, graphProperties, dataSet):AreaGraph              ::IDB.module.Area.js  z.85997102005683301.2019.04.09.05.36.19.958|class
  /**
   * AreaGraph constructor
   * @class AreaGraph
   * @constructor
   * @memberof IDB
   * @param {Object} chart
   * @param {jQueryElement} $div
   * @param {Object} graphProperties
   * @param {Object} dataSet
   * @augments IDB.GraphBaseKinetic
   *
   * @see z.85997102005683301
   * @see IDB.GraphBaseKinetic
   * @see module:Area
   */
  IDB.AreaGraph = function(chart, $div, graphProperties, dataSet) {
    IDB.GraphBaseKinetic.call(this, chart, $div, graphProperties, dataSet, null);
    this._info.legendStyle = IDB.LS.AXES;
    }; //-IDB.AreaGraph
    IDB.AreaGraph.prototype = Object.create(IDB.GraphBaseKinetic.prototype);
    IDB.AreaGraph.prototype.constructor = IDB.AreaGraph;
  // Z_DOT cmd _build():void                ::IDB.module.Area.js  z.51286302005683301.2019.04.09.05.39.28.215|-!
    /**
     * Build
     * @memberof IDB.AreaGraph
     * @method
     * @private
     * @see z.51286302005683301
     * @see IDB.CartesianGrid
     * @see IDB.AreaPlot
     * @see Kinetic.Group
     * @see IDB.CartesianGridView
     * @see IDB.AreaPlotView
     */
    IDB.AreaGraph.prototype._build = function() {
      /**
       * @var {IDB.CartesianGrid} _grid = new IDB.CartesianGrid(this._graphContext)
       * @memberof IDB.AreaGraph.prototype
       * @private
       */
      this._grid = new IDB.CartesianGrid(this._graphContext);
      /**
       * @var {IDB.AreaPlot} _plot = new IDB.AreaPlot(this._graphContext, this._grid.getAxes())
       * @memberof IDB.AreaGraph.prototype
       * @private
       */
      this._plot = new IDB.AreaPlot(this._graphContext, this._grid.getAxes());
      //
      this._limitHold = new Kinetic.Group(undefined);
      //
      this._gridView = new IDB.CartesianGridView(this._grid, this._limitHold);
      this._stageLayer.add(this._gridView.getDisplay());
      //
      /**
       * @var {IDB.AreaPlotView} _plotView
       * @memberof IDB.AreaGraph.prototype
       */
      this._plotView = new IDB.AreaPlotView(this._plot);
      this._stageLayer.add(this._plotView.getDisplay());
      //
      this._stageLayer.add(this._limitHold);
      }; //-IDB.AreaGraph.prototype._build
  // Z_DOT cmd _update(width, height):void  ::IDB.module.Area.js  z.24788602005683301.2019.04.09.05.44.48.742|-!
    /**
     * Update
     * @memberof IDB.AreaGraph
     * @method
     * @private
     * @see z.24788602005683301
     */
    IDB.AreaGraph.prototype._update = function(width, height) {
      var gv = this._gridView;
      var pv = this._plotView;
      var pvd = pv.getDisplay();
      //
      gv.setSize(width, height);
      //
      pvd.x(gv.getPlotPosX());
      pvd.y(gv.getPlotPosY());
      pv.setSize(gv.getPlotWidth(), gv.getPlotHeight());
      //
      pvd.clip({
        x: 0,
        y: -3,
        width: gv.getPlotWidth(),
        height: gv.getPlotHeight()+6
      });
      }; //-IDB.AreaGraph.prototype._update
// Z_DOT ctor AreaPlot(graphContext, cartesianGridAxes):IDB.AreaPlot                  ::IDB.module.Area.js  z.93007012005683301.2019.04.09.05.51.10.039|class
  /**
   * AreaPlot constructor
   * @class AreaPlot
   * @constructor
   * @memberof IDB
   *
   * @param {Object} graphContext
   * @param {Object} cartesianGridAxes
   *
   * @see z.93007012005683301
   * @see AreaPlotView
   * @see module:Area
   */
  IDB.AreaPlot = function(graphContext, cartesianGridAxes) {
    this._graphContext = graphContext;
    this._cartesianGridAxes = cartesianGridAxes;
    //
    var sett = graphContext.settings;
    var is3D = sett.getBool('Is3dGraph');
    //
    if ( !is3D ) {
      var lo = sett.get('AreaLabelOptions');
      lo.position = sett.get('AreaLabelPos');
      lo.rotation = sett.get('AreaLabelRotation');
    }
    //
    this._clusterMode = (!sett.getBool('AreaStackedMode') && !sett.getBool('AreaRatioMode'));
    this._axes = this._buildAxes();
    //
    if ( !is3D && sett.getBool('AreaSmoothMode') ) {
      this._buildSmoothPoints();
    }
    else {
      this._buildPoints();
    }
    }; //-IDB.AreaPlot
  // Z_DOT cmd _buildAxes():[IDB.AreaAxis]      ::IDB.module.Area.js  z.17298902005683301.2019.04.09.05.49.49.271|-!
    /**
     * Build Axes
     * @memberof IDB.AreaPlot
     * @method
     * @private
     * @see z.17298902005683301
     */
    IDB.AreaPlot.prototype._buildAxes = function() {
      var axes = [];
      var gc = this._graphContext;
      var dpGrid = gc.datapointGrid;
      var cartGridAxes = this._cartesianGridAxes;
      var rowCount = dpGrid.getNumXRows();
      var yCount = dpGrid.getNumYCols(true);
      var stacks = [];
      var i, j, val, yAxisId, seg;
      //
      for ( i = 0 ; i < rowCount ; ++i ) {
        stacks[i] = [];
        //
        for ( j = 0 ; j < yCount ; ++j ) {
          val = dpGrid.getDatapointAt(i, j, true).yValue.numberValue;
          stacks[i][j] = val+(0 === j ? 0 : stacks[i][j-1]);
        }
      }
      //
      for ( i = 0 ; i < yCount ; ++i ) {
        yAxisId = dpGrid.getYAxis(i, true).axisId;
        //
        seg = new IDB.AreaAxis(gc, cartGridAxes.getYGridAxisById(yAxisId), stacks, i);
        axes[i] = seg;
      }
      //
      return axes;
      }; //-IDB.AreaPlot.prototype._buildAxes
  // Z_DOT cmd _buildPoints():void              ::IDB.module.Area.js  z.74708612005683301.2019.04.09.06.01.20.747|-!
    /**
     * Build Points
     * @memberof IDB.AreaPlot
     * @method
     * @private
     * @see z.74708612005683301
     */
    IDB.AreaPlot.prototype._buildPoints = function() {
      var axes = this._axes;
      var gc = this._graphContext;
      var sett = gc.settings;
      var rowCount = gc.datapointGrid.getNumXRows();
      var skipNull = sett.getBool('AreaSkipNulls');
      var clusterMode = this._clusterMode;
      var j, i, axis, x0, x1, x2, seg0, seg1, seg2, p0, p1, p2, prevP0, prevP1, prevP2, zero;
      //
      for ( i = 0 ; i < rowCount ; ++i ) {
        // noinspection MagicNumberJS
        x0 = Math.max(0, i-0.5);
        x1 = i;
        // noinspection MagicNumberJS
        x2 = Math.min(i+0.5, rowCount-1);
        //
        prevP0 = null;
        prevP1 = null;
        prevP2 = null;
        //
        for ( j = 0 ; j < axes.length ; ++j ) {
          axis = axes[j];
          //
          seg0 = axis.getSegment(Math.max(0, i-1));
          seg1 = axis.getSegment(i);
          seg2 = axis.getSegment(Math.min(i+1, rowCount-1));
          //
          if ( skipNull && seg1.valueIsNothing() ) {
            seg1.setActive(false);
            continue;
          }
          //
          p0 = { x: x0, y: 1-seg0.getRelativeY() };
          p1 = { x: x1, y: 1-seg1.getRelativeY() };
          p2 = { x: x2, y: 1-seg2.getRelativeY() };
          //
          p0.y = (p0.y-p1.y)/2+p1.y;
          p2.y = (p2.y-p1.y)/2+p1.y;
          //
          if ( clusterMode || !prevP0 ) {
            zero = axis.getRelativeYZero();
            prevP0 = { x: p0.x, y: zero };
            prevP1 = { x: p1.x, y: zero };
            prevP2 = { x: p2.x, y: zero };
          }
          //
          if ( skipNull ) {
            if ( seg0.valueIsNothing() ) {
              p0 = p1;
              prevP0 = prevP1;
            }
            //
            if ( seg2.valueIsNothing() ) {
              p2 = p1;
              prevP2 = prevP1;
            }
          }
          //
          seg1.setPoints(p0, p1, p2, prevP0, prevP1, prevP2, i);
          //
          prevP0 = p0;
          prevP1 = p1;
          prevP2 = p2;
        }
      }
      }; //-IDB.AreaPlot.prototype._buildPoints
  // Z_DOT cmd _buildSmoothPoints():void        ::IDB.module.Area.js  z.78532812005683301.2019.04.09.06.03.43.587|-!
    /**
     * Build Smooth Points
     * @memberof IDB.AreaPlot
     * @method
     * @private
     * @see z.78532812005683301
     */
    IDB.AreaPlot.prototype._buildSmoothPoints = function() {
      var axes = this._axes;
      var gc = this._graphContext;
      var rowCount = gc.datapointGrid.getNumXRows();
      var j, i, axis, storedPoints, setPoints, zero, seg0, seg1, seg2,
        n0, n1, p0, p1, p2, nextI, max, curve, dot;
      //
      for ( j = 0 ; j < axes.length ; ++j ) {
        axis = axes[j];
        n1 = { x: 0, y: 0 };
        storedPoints = [];
        zero = axis.getRelativeYZero();
        //
        for ( i = 0 ; i < rowCount ; ++i ) {
          nextI = i+1;
          //
          seg0 = axis.getSegment(Math.max(0, nextI-1));
          seg1 = axis.getSegment(nextI);
          seg2 = axis.getSegment(Math.min(nextI+1, rowCount-1));
          //
          if ( nextI < rowCount ) {
            p0 = { x: i,   y: 1-seg0.getRelativeY() };
            p1 = { x: i+1, y: 1-seg1.getRelativeY() };
            p2 = { x: i+2, y: 1-seg2.getRelativeY() };
            //
            n0 = { x: -n1.x, y: -n1.y };
            n1 = { x: (p0.x-p2.x)/7, y: (p0.y-p2.y)/7 };
            //
            // noinspection MagicNumberJS
            max = (0 === i ? 11 : storedPoints.length+10);
            //
            // noinspection MagicNumberJS
            curve = new IDB.LineCurve(20, p0, n0, p1, n1);
            storedPoints = storedPoints.concat(curve.getPoints());
            max = Math.min(max, storedPoints.length);
          }
          else {
            max = storedPoints.length;
          }
          //
          setPoints = [];
          //
          for ( var n = 0 ; n < max ; ++n ) {
            setPoints.push(storedPoints.shift());
          }
          //
          if ( storedPoints[0] ) {
            setPoints.push(storedPoints[0]);
          }
          //
          dot = ((nextI < rowCount ? p0 : p1) || { x : i, y: zero });
          axis.getSegment(i).setCurvePoints(setPoints, dot, zero, i);
        }
      }
      }; //-IDB.AreaPlot.prototype._buildSmoothPoints
  // Z_DOT ppt getAxisCount():Number            ::IDB.module.Area.js  z.19239912005683301.2019.04.09.06.06.33.291|+@.
    /**
     * Axis Count
     * @memberof IDB.AreaPlot
     * @method
     * @return {Number}
     * @see z.19239912005683301
     */
    IDB.AreaPlot.prototype.getAxisCount = function() {
      return this._axes.length;
      }; //-IDB.AreaPlot.prototype.getAxisCount
  // Z_DOT ppt getAxis(index):IDB.AreaAxis      ::IDB.module.Area.js  z.48846122005683301.2019.04.09.06.09.24.884|+@.
    /**
     * Axis
     * @memberof IDB.AreaPlot
     * @method
     * @param {Number} index
     * @return {IDB.AreaAxis}
     * @see z.48846122005683301
     */
    IDB.AreaPlot.prototype.getAxis = function(index) {
      return this._axes[index];
      }; //-IDB.AreaPlot.prototype.getAxis
  // Z_DOT ppt isClusterMode():Boolean          ::IDB.module.Area.js  z.36334322005683301.2019.04.09.06.12.23.363|+@.
    /**
     * Cluster Mode
     * for 3D mode
     * @memberof IDB.AreaPlot
     * @method
     * @return {Boolean} this._clusterMode
     * @see z.36334322005683301
     */
    IDB.AreaPlot.prototype.isClusterMode = function() {
      return this._clusterMode;
      }; //-IDB.AreaPlot.prototype.isClusterMode
// Z_DOT ctor AreaPlotView(model):AreaPlotView                                        ::IDB.module.Area.js  z.53022622005683301.2019.04.09.06.17.02.035|class
  /**
   * AreaPlotView constructor
   * @class AreaPlotView
   * @constructor
   * @memberof IDB
   *
   * @param {Object} model
   *
   * @see z.53022622005683301
   * @see AreaPlot
   * @see module:Area
   *
   */
  IDB.AreaPlotView = function(model) {
    this._model = model;
    this._buildKinetic();
    }; //-IDB.AreaPlotView
  // Z_DOT cmd _buildKinetic():void        ::IDB.module.Area.js  z.53891822005683301.2019.04.09.06.20.19.835|-!
    /**
     * Build Kinetic
     * @memberof IDB.AreaPlotView
     * @method
     * @private
     * @see z.53891822005683301
     * @see Kinetic.Group
     */
    IDB.AreaPlotView.prototype._buildKinetic = function() {
      this._display = new Kinetic.Group(undefined);
      this._axes = [];
      //
      var plotModel = this._model;
      var axisCount = plotModel.getAxisCount();
      var labelHold = new Kinetic.Group(undefined);
      var i, j, axisModel, axis, labels;
      //
      for ( i = 0 ; i < axisCount ; ++i ) {
        axisModel = plotModel.getAxis(i);
        //
        axis = new IDB.AreaAxisView(axisModel);
        labels = axis.getLabels();
        this._display.add(axis.getDisplay());
        //
        this._axes[i] = axis;
        //
        for ( j in labels ) {
          // noinspection JSUnfilteredForInLoop
          labelHold.add(labels[j].getDisplay());
        }
      }
      //
      this._display.add(labelHold);
      }; //-IDB.AreaPlotView.prototype._buildKinetic
  // Z_DOT ppt getDisplay():Kinetic.Group  ::IDB.module.Area.js  z.41387922005683301.2019.04.09.06.22.58.314|+@.
    /**
     * Display
     * @memberof IDB.AreaPlotView
     * @method
     * @return {Kinetic.Group} this._display
     * @see z.41387922005683301
     * @see Kinetic.Group
     */
    IDB.AreaPlotView.prototype.getDisplay = function() {
      return this._display;
    }; //-IDB.AreaPlotView.prototype.getDisplay
  // Z_DOT ppt setSize(width, height):     ::IDB.module.Area.js  z.45342132005683301.2019.04.09.06.25.24.354|+.
    /**
     * Size
     * @memberof IDB.AreaPlotView
     * @method
     * @see z.45342132005683301
     */
    IDB.AreaPlotView.prototype.setSize = function(width, height) {
      var axes = this._axes;
      //
      for ( var i in axes ) {
        axes[i].setSize(width, height);
      }
    };
// Z_DOT ctor AreaSegment(graphContext, datapoint, relativeY, labelText):AreaSegment  ::IDB.module.Area.js  z.63904332005683301.2019.04.09.06.29.00.936|class
  /**
   * Area Segment
   * @class AreaSegment
   * @constructor
   * @memberof IDB
   *
   * @param {Object} graphContext
   * @param {Object} datapoint
   * @param {Number} relativeY
   * @param {String} labelText
   *
   * @see z.63904332005683301
   * @see AreaSegmentView
   * @see module:Area
   */
  IDB.AreaSegment = function(graphContext, datapoint, relativeY, labelText) {
    this._graphContext = graphContext;
    this._datapoint = datapoint;
    this._relativeY = relativeY;
    this._labelText = labelText;
    this._active = true;
    this._linePoints = null;
    this._fillPoints = null;
    this._dotPoint = null;
    }; //-IDB.AreaSegment
  // Z_DOT ppt setPoints(p0, p1, p2, prevP0, prevP1, prevP2, offsetX):void  ::IDB.module.Area.js  z.22133732005683301.2019.04.09.06.35.33.122|+.
    /**
     * Points
     * @memberof IDB.AreaSegment
     * @method
     * @param {Point} p0
     * @param {Point} p1
     * @param {Point} p2
     * @param {Point} prevP0
     * @param {Point} prevP1
     * @param {Point} prevP2
     * @param {Number} offsetX
     * @see z.22133732005683301
     */
    IDB.AreaSegment.prototype.setPoints = function(p0, p1, p2, prevP0, prevP1, prevP2, offsetX) {
      this._linePoints = [
        { x: p0.x-offsetX, y: p0.y },
        { x: p1.x-offsetX, y: p1.y },
        { x: p2.x-offsetX, y: p2.y }
      ];
      //
      this._fillPoints = [
        { x: p0.x-offsetX, y: p0.y },
        { x: p1.x-offsetX, y: p1.y },
        { x: p2.x-offsetX, y: p2.y },
        { x: prevP2.x-offsetX, y: prevP2.y },
        { x: prevP1.x-offsetX, y: prevP1.y },
        { x: prevP0.x-offsetX, y: prevP0.y }
      ];
      //
      this._dotPoint = this._linePoints[1];
      }; //-IDB.AreaSegment.prototype.setPoints
  // Z_DOT ppt setCurvePoints(curvePoints, dotPoint, zero, offsetX):void    ::IDB.module.Area.js  z.59626442005683301.2019.04.09.06.47.42.695|+@.
    /**
     * Curve Points
     * @memberof IDB.AreaSegment
     * @method
     *
     * @param {Array<Point>} curvePoints
     * @param {Point} dotPoint
     * @param {Number} zero
     * @param {Number} offsetX
     * @see z.59626442005683301
     */
    IDB.AreaSegment.prototype.setCurvePoints = function(curvePoints, dotPoint, zero, offsetX) {
      this._dotPoint = { x: dotPoint.x-offsetX, y: dotPoint.y };
      this._linePoints = [];
      this._fillPoints = [];
      var i, cp, p;
      //
      if(!curvePoints || 0 === curvePoints.length) {
          return;
      }
      //
      for ( i in curvePoints ) {
        // noinspection JSUnfilteredForInLoop
        cp = curvePoints[i];
        p = { x: cp.x-offsetX, y: cp.y };
        this._linePoints.push(p);
        this._fillPoints.push(p);
      }
      //
      p = { x: p.x, y: zero };
      this._fillPoints.push(p);
      //
      p = { x: curvePoints[0].x-offsetX, y: zero };
      this._fillPoints.push(p);
      }; //-IDB.AreaSegment.prototype.setCurvePoints
  // Z_DOT ppt setActive(active):void                                       ::IDB.module.Area.js  z.32676542005683301.2019.04.09.06.49.27.623|+.
    /**
     * Active
     * @memberof IDB.AreaSegment
     * @method
     *
     * @param {Boolean} active
     * @see z.32676542005683301
     */
    IDB.AreaSegment.prototype.setActive = function(active) {
      this._active = active;
      }; //-IDB.AreaSegment.prototype.setActive
  // Z_DOT ppt getActive():Boolean                                          ::IDB.module.Area.js  z.78035742005683301.2019.04.09.06.52.33.087|+.
    /**
     * Active
     * @memberof IDB.AreaSegment
     * @method
     * @return {Boolean} this._active
     *
     * @see z.78035742005683301
     */
    IDB.AreaSegment.prototype.getActive = function() {
      return this._active;
      }; //-IDB.AreaSegment.prototype.getActive
  // Z_DOT ppt setOnDatapointsUpdated(callback):void                        ::IDB.module.Area.js  z.15759642005683301.2019.04.09.06.51.35.751|+.
    /**
     * On Datapoints Updated
     * @memberof IDB.AreaSegment
     * @method
     *
     * @param {Function} callback
     * @see z.15759642005683301
     */
    IDB.AreaSegment.prototype.setOnDatapointsUpdated = function(callback) {
      this._graphContext.addDatapointsUpdatedListener(callback);
      }; //-IDB.AreaSegment.prototype.setOnDatapointsUpdated
  // Z_DOT ppt getDatapoint():Point                                         ::IDB.module.Area.js  z.98401942005683301.2019.04.09.06.55.10.489|+.
    /**
     * Datapoint
     * @memberof IDB.AreaSegment
     * @method
     * @return {Point} this._datapoint
     *
     * @see z.98401942005683301
     */
    IDB.AreaSegment.prototype.getDatapoint = function() {
      return this._datapoint;
      }; //-IDB.AreaSegment.prototype.getDatapoint
  // Z_DOT ppt valueIsNothing():Boolean                                     ::IDB.module.Area.js  z.97058252005683301.2019.04.09.07.01.25.079|+@.
    /**
     * Y Value is Nothing
     * @memberof IDB.AreaSegment
     * @method
     * @return {Boolean} this._datapoint.yValue.isNothing
     * @see z.97058252005683301
     */
    IDB.AreaSegment.prototype.valueIsNothing = function() {
      return this._datapoint.yValue.isNothing;
      }; //-IDB.AreaSegment.prototype.valueIsNothing
  // Z_DOT ppt getRelativeY():                                              ::IDB.module.Area.js  z.53019552005683301.2019.04.09.07.06.31.035|+@.
    /**
     * Relative Y
     * @memberof IDB.AreaSegment
     * @method
     * @return {Number} this._relativeY
     * @see z.53019552005683301
     */
    IDB.AreaSegment.prototype.getRelativeY = function() {
      return this._relativeY;
      }; //-IDB.AreaSegment.prototype.getRelativeY
  // Z_DOT ppt getRelativeLinePoints():[Point]                              ::IDB.module.Area.js  z.72866652005683301.2019.04.09.07.07.46.827|+@.
    /**
     * Relative Line Points
     * @memberof IDB.AreaSegment
     * @method
     * @return {Array<Point>}
     * @see z.72866652005683301
     */
    IDB.AreaSegment.prototype.getRelativeLinePoints = function() {
      return this._linePoints;
      }; //-IDB.AreaSegment.prototype.getRelativeLinePoints
  // Z_DOT ppt getRelativeFillPoints():[Point]                              ::IDB.module.Area.js  z.11023752005683301.2019.04.09.07.08.52.011|+@.
    /**
     * Relative Fill Points
     * @memberof IDB.AreaSegment
     * @method
     * @return {Array<Point>}
     * @see z.11023752005683301
     */
    IDB.AreaSegment.prototype.getRelativeFillPoints = function() {
      return this._fillPoints;
      }; //-IDB.AreaSegment.prototype.getRelativeFillPoints
  // Z_DOT ppt getRelativeDotPoint():Point                                  ::IDB.module.Area.js  z.97579752005683301.2019.04.09.07.09.57.579|+@.
    /**
     * Relative Dot Point
     * @memberof IDB.AreaSegment
     * @method
     * @return {Point} this._dotPoint
     * @see z.97579752005683301
     */
    IDB.AreaSegment.prototype.getRelativeDotPoint = function() {
      return this._dotPoint;
      }; //-IDB.AreaSegment.prototype.getRelativeDotPoint
  // Z_DOT ppt getLabelText():String                                        ::IDB.module.Area.js  z.05365852005683301.2019.04.09.07.10.56.350|+@.
    /**
     * Label Text
     * @memberof IDB.AreaSegment
     * @method
     * @return {String} this._labelText
     * @see z.05365852005683301
     */
    IDB.AreaSegment.prototype.getLabelText = function() {
      return this._labelText;
      }; //-IDB.AreaSegment.prototype.getLabelText
  // Z_DOT ppt getFillAlpha():Number                                        ::IDB.module.Area.js  z.32985952005683301.2019.04.09.07.12.38.923|+@.
    /**
     * Fill Alpha
     * @memberof IDB.AreaSegment
     * @method
     * @return {Number} this._graphContext.settings.getAlpha('AreaFillTransparency')
     * @see z.32985952005683301
     */
    IDB.AreaSegment.prototype.getFillAlpha = function() {
      return this._graphContext.settings.getAlpha('AreaFillTransparency');
      }; //-IDB.AreaSegment.prototype.getFillAlpha
  // Z_DOT ppt getThickness():Number                                        ::IDB.module.Area.js  z.11824062005683301.2019.04.09.07.14.02.811|+@.
    /**
     * Thickness
     * @memberof IDB.AreaSegment
     * @method
     * @return {Number} 2
     * @see z.11824062005683301
     */
    IDB.AreaSegment.prototype.getThickness = function() {
      return 2;
      }; //-IDB.AreaSegment.prototype.getThickness
  // Z_DOT ppt getAxisThickness():Number                                    ::IDB.module.Area.js  z.61713162005683301.2019.04.09.07.15.31.716|+@.
    /**
     * Axis Thickness
     * @memberof IDB.AreaSegment
     * @method
     * @return {Number} this._graphContext.settings.get('AreaThickness')/100
     * @see z.61713162005683301
     */
    IDB.AreaSegment.prototype.getAxisThickness = function() {
      // noinspection MagicNumberJS
      return this._graphContext.settings.get('AreaThickness')/100;
      }; //-IDB.AreaSegment.prototype.getAxisThickness
  // Z_DOT cmd createClickListener():Object                                 ::IDB.module.Area.js  z.74826262005683301.2019.04.09.07.17.42.847|+!
    /**
     * Create Click Listener
     * @memberof IDB.AreaSegment
     * @method
     * @return {Object} IDB.leftClickListener(this, "_handleClick")
     * @see z.74826262005683301
     */
    IDB.AreaSegment.prototype.createClickListener = function() {
      return IDB.leftClickListener(this, "_handleClick");
      }; //-IDB.AreaSegment.prototype.createClickListener
  // Z_DOT cmd createOverListener():Object                                  ::IDB.module.Area.js  z.93446362005683301.2019.04.09.07.19.24.439|+!
    /**
     * Create Over Listener
     * @memberof IDB.AreaSegment
     * @method
     * @return {Object} IDB.listener(this, "_handleOver")
     * @see z.93446362005683301
     */
    IDB.AreaSegment.prototype.createOverListener = function() {
      return IDB.listener(this, "_handleOver");
      }; //-IDB.AreaSegment.prototype.createOverListener
  // Z_DOT cmd createOutListener():Object                                   ::IDB.module.Area.js  z.97817462005683301.2019.04.09.07.21.11.879|+!
    /**
     * Create Out Listener
     * @memberof IDB.AreaSegment
     * @method
     * @return {Object} IDB.listener(this, "_handleOut")
     * @see z.97817462005683301
     */
    IDB.AreaSegment.prototype.createOutListener = function() {
      return IDB.listener(this, "_handleOut");
      }; //-IDB.AreaSegment.prototype.createOutListener
  // Z_DOT evt _handleClick():void                                          ::IDB.module.Area.js  z.74091562005683301.2019.04.09.07.21.59.047|-\
    /**
     * Handle Click
     * @memberof IDB.AreaSegment
     * @method
     * @private
     * @see z.74091562005683301
     */
    IDB.AreaSegment.prototype._handleClick = function() {
      this._graphContext.onDatapointClicked(this._datapoint);
      }; //-IDB.AreaSegment.prototype._handleClick
  // Z_DOT evt _handleOver():void                                           ::IDB.module.Area.js  z.51929562005683301.2019.04.09.07.23.12.915|-\
    /**
     * Handle Over
     * @memberof IDB.AreaSegment
     * @method
     * @private
     * @see z.51929562005683301
     */
    IDB.AreaSegment.prototype._handleOver = function() {
      this._graphContext.onDatapointOver(this._datapoint);
      }; //-IDB.AreaSegment.prototype._handleOver
  // Z_DOT evt _handleOut():void                                            ::IDB.module.Area.js  z.78905662005683301.2019.04.09.07.24.10.987|-\
    /**
     * Handle Out
     * @memberof IDB.AreaSegment
     * @method
     * @private
     * @see z.78905662005683301
     */
    IDB.AreaSegment.prototype._handleOut = function() {
      this._graphContext.onDatapointOut(this._datapoint);
      }; //-IDB.AreaSegment.prototype._handleOut
// Z_DOT ctor AreaSegmentView(model):AreaSegmentView                                  ::IDB.module.Area.js  z.82454682005683301.2019.04.09.07.57.25.428|class
  /**
   * Area Segment View
   * @class AreaSegmentView
   * @constructor
   * @memberof IDB
   *
   * @param {Object} model
   *
   * @see z.82454682005683301
   * @see AreaSegment
   * @see module:Area
   */
  IDB.AreaSegmentView = function(model) {
    this._model = model;
    this._buildKinetic();
    }; //-IDB.AreaSegmentView = function(model
  // Z_DOT cmd _buildKinetic():void         ::IDB.module.Area.js  z.13731782005683301.2019.04.09.07.58.33.731|-!
    /**
     * Build Kinetic
     * @memberof IDB.AreaSegmentView
     * @method
     * @private
     * @see z.13731782005683301
     */
    IDB.AreaSegmentView.prototype._buildKinetic = function() {
      var model = this._model;
      //
      this._display = new Kinetic.Group(undefined);
      this._display.on("click", model.createClickListener());
      this._display.on("mouseover", model.createOverListener());
      this._display.on("mouseout", model.createOutListener());
        this._display._touchHandler = new IDB.TouchHandler(this._display);

      if ( !model.getActive() ) {
        return;
      }
      //
      model.setOnDatapointsUpdated(IDB.listener(this, "_onDatapointsUpdated"));
      //
      var dp = model.getDatapoint();
      this._labelRef = null;
      //
      // noinspection JSCheckFunctionSignatures,JSValidateTypes
      this._fill = new Kinetic.Line({
        closed: true,
        fill: dp.axisColor.css,
        opacity: model.getFillAlpha(),
        strokeWidth: 0
      });
      this._display.add(this._fill);
      //
      var lineConfig = {
        points: [],
        stroke: dp.axisColor.css,
        strokeWidth: model.getThickness(),
        lineJoin: 'round',
        lineCap: 'butt'
      };
      //
      // noinspection JSValidateTypes
      this._line = new Kinetic.Line(lineConfig);
      this._display.add(this._line);
      //
      lineConfig.strokeWidth += 1;
      lineConfig.visible = false;
      //
      // noinspection JSValidateTypes
      this._lineHigh = new Kinetic.Line(lineConfig);
      this._display.add(this._lineHigh);
      }; //-IDB.AreaSegmentView.prototype._buildKinetic
  // Z_DOT ppt getDisplay():Kinetic.Group   ::IDB.module.Area.js  z.74783882005683301.2019.04.09.08.00.38.747|+@.
    /**
     * Display
     * @memberof IDB.AreaSegmentView
     * @method
     * @return {Kinetic.Group} this._display
     * @see z.74783882005683301
     */
    IDB.AreaSegmentView.prototype.getDisplay = function() {
      return this._display;
      }; //-IDB.AreaSegmentView.prototype.getDisplay
  // Z_DOT ppt getLabelRef():Object         ::IDB.module.Area.js  z.76457982005683301.2019.04.09.08.02.55.467|+.
    /**
     * Label Ref
     * @memberof IDB.AreaSegmentView
     * @method
     * @return {Object} this._labelRef
     * @see z.76457982005683301
     */
    IDB.AreaSegmentView.prototype.getLabelRef = function() {
      return this._labelRef;
      }; //-IDB.AreaSegmentView.prototype.getLabelRef
  // Z_DOT ppt setLabelRef(labelRef):void   ::IDB.module.Area.js  z.16447092005683301.2019.04.09.08.04.34.461|+.
    /**
     * Label Ref
     * @memberof IDB.AreaSegmentView
     * @method
     * @param {Object} labelRef
     * @see z.16447092005683301
     */
    IDB.AreaSegmentView.prototype.setLabelRef = function(labelRef) {
      this._labelRef = labelRef;
      }; //-IDB.AreaSegmentView.prototype.setLabelRef
  // Z_DOT ppt setSize(width, height):void  ::IDB.module.Area.js  z.69393192005683301.2019.04.09.08.05.39.396|+@.
    /**
     * Size
     * @memberof IDB.AreaSegmentView
     * @method
     * @param {Number} width
     * @param {Number} height
     * @see z.69393192005683301
     */
    IDB.AreaSegmentView.prototype.setSize = function(width, height) {
      var model = this._model;
      //
      if ( !model.getActive() ) {
        return;
      }
      //
      var fillRel = model.getRelativeFillPoints();
      var lineRel = model.getRelativeLinePoints();
      var fillPts = [];
      var linePts = [];
      var i, rp;
      //
      for ( i in fillRel ) {
        // noinspection JSUnfilteredForInLoop
        rp = fillRel[i];
        fillPts.push(rp.x*width, rp.y*height);
      }
      //
      for ( i in lineRel ) {
        // noinspection JSUnfilteredForInLoop
        rp = lineRel[i];
        linePts.push(rp.x*width, rp.y*height);
      }
      //
      this._fill.points(fillPts);
      this._line.points(linePts);
      this._lineHigh.points(linePts);
      }; //-IDB.AreaSegmentView.prototype.setSize
  // Z_DOT evt _onDatapointsUpdated():void  ::IDB.module.Area.js  z.13108192005683301.2019.04.09.08.06.20.131|-\
    /**
     * On Datapoints Updated
     * @memberof IDB.AreaSegmentView
     * @method
     * @see z.13108192005683301
     */
    IDB.AreaSegmentView.prototype._onDatapointsUpdated = function() {
      var state = this._model.getDatapoint().getVisualState();
      // noinspection EqualityComparisonWithCoercionJS
      var isHigh = (state == IDB.VS.HIGHLIGHTED);
      // noinspection EqualityComparisonWithCoercionJS
      var isFade = (state == IDB.VS.FADED);
      //
      // noinspection MagicNumberJS
      this._display.opacity(isFade ? 0.2 : 1.0);
      this._lineHigh.visible(isHigh);
      //
      if  ( this._labelRef ) {
        this._labelRef.visible = !isFade;
      }
      }; //-IDB.AreaSegmentView.prototype._onDatapointsUpdated
//EOF


